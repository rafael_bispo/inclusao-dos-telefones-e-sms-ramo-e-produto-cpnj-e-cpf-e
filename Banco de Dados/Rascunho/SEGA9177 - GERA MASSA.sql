
if object_id('tempdb..#aviso_negativo') is not null   
begin  
 drop table #aviso_negativo   
end  
         
create table #aviso_negativo  
  ( aviso_cancelamento_inadimplencia_id bigint not null )  
  
insert into #aviso_negativo ( aviso_cancelamento_inadimplencia_id )   
select aviso_cancelamento_inadimplencia_id  
  from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb  with (nolock)        
 where situacao = 'A' /* registros ativos no GRID do SEGP1375 */  
 
 if object_id('tempdb..#retorno_negativo') is not null   
begin  
   drop table #retorno_negativo  
end  
  
create table #retorno_negativo   
  ( aviso_cancelamento_inadimplencia_id bigint not null  
  , proposta_id numeric(9) null  
  , proposta_bb numeric(9) null  
  , produto_id int null  
  , num_parcela int null  
  , apolice_id numeric(9) null  
  , ramo_id tinyint null  
  , subramo_id int null  
  , prop_cliente_id int null  
  , usuario varchar(20) null )  
  
insert into #retorno_negativo  
  ( aviso_cancelamento_inadimplencia_id  
  , proposta_id  
  , proposta_bb  
  , produto_id  
  , num_parcela  
  , usuario )   
select cancelamento.aviso_cancelamento_inadimplencia_id  
  , cancelamento.proposta_id  
  , cancelamento.proposta_bb  
  , cancelamento.produto_id  
  , cancelamento.num_parcela  
  , cancelamento.usuario  
  from #aviso_negativo neg  
 inner join seguros_db.dbo.aviso_cancelamento_inadimplencia_tb cancelamento with (nolock)   
 on cancelamento.aviso_cancelamento_inadimplencia_id = neg.aviso_cancelamento_inadimplencia_id       
 where cancelamento.aviso_cancelamento = 'A' /* parcelas avisadas */  
   and cancelamento.dt_envio_aviso is null /* nao enviado */       
   and cancelamento.situacao = 'A' /* registros ativos no GRID do SEGP1375 */     
   and cancelamento.produto_id IN (1225,1206)   
   and cancelamento.retorno = 'NEGATIVO' /* retorno negativo */   
  
 
  select *    
from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aux with (nolock)    
where aux.proposta_id  in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and aux.situacao     = 'a'     
and aux.dt_envio_aviso is not null 

 
use desenv_db
go
alter proc dbo.vaifilhao   
as

update ag
    set situacao = 'p'
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and num_cobranca = (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 

update seguros_db.dbo.aviso_cancelamento_inadimplencia_tb 
set dt_envio_aviso  = null
where proposta_id  in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and situacao     = 'a'     
and dt_envio_aviso is not null 

DELETE 
from evento_seguros_db.dbo.evento_tb 
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and tp_evento_id = 20027    

DELETE 
from interface_dados_db.dbo.SEGA9177_20_processado_tb  
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)

DELETE 
from interface_dados_db.dbo.SEGA9177_10_processado_tb  
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)

DELETE 
from interface_dados_db.dbo.sega9177_processado_tb  
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)


update seguros_db.dbo.agendamento_cobranca_atual_tb
set canc_endosso_id = null
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)


update aviso_cancelamento_inadimplencia_tb
  set num_parcela =  (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = aviso_cancelamento_inadimplencia_tb.proposta_id ) 
  from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aviso_cancelamento_inadimplencia_tb
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)

  
     update ag
    set dt_agendamento = dateadd(month, -1, getdate())
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and num_cobranca = (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 

    update ag
    set situacao = 'a'
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)
and num_cobranca <> (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 
					 


  update seguros_db.dbo.agendamento_cobranca_atual_tb
set canc_endosso_id = null
where proposta_id in (250710480,253376962,253376972,253377012,253377082,253377083,254497368,254497897,254516446,254531194)

  

begin tran
exec dbo.vaifilhao   
commit  
