create  
 table #aviso_negativo  
  ( aviso_cancelamento_inadimplencia_id bigint not null )  
  
insert  
  into #aviso_negativo  
  ( aviso_cancelamento_inadimplencia_id )   
select aviso_cancelamento_inadimplencia_id  
  from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aviso_cancelamento_inadimplencia_tb with (nolock)        
 where situacao = 'A' /* registros ativos no GRID do SEGP1375 */   
  

create    
 table #retorno_negativo   
  ( aviso_cancelamento_inadimplencia_id bigint not null  
  , proposta_id numeric(9) null  
  , proposta_bb numeric(9) null  
  , produto_id int null  
  , num_parcela int null  
  , apolice_id numeric(9) null  
  , ramo_id tinyint null  
  , subramo_id int null  
  , prop_cliente_id int null  
  , usuario varchar(20) null )  
  
insert  
  into #retorno_negativo  
  ( aviso_cancelamento_inadimplencia_id  
  , proposta_id  
  , proposta_bb  
  , produto_id  
  , num_parcela  
  , usuario )   
select aviso_cancelamento_inadimplencia_tb.aviso_cancelamento_inadimplencia_id  
  , aviso_cancelamento_inadimplencia_tb.proposta_id  
  , aviso_cancelamento_inadimplencia_tb.proposta_bb  
  , aviso_cancelamento_inadimplencia_tb.produto_id  
  , aviso_cancelamento_inadimplencia_tb.num_parcela  
  , aviso_cancelamento_inadimplencia_tb.usuario  
  from #aviso_negativo #aviso_negativo  
 inner  
  join seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aviso_cancelamento_inadimplencia_tb with (nolock)   
 on aviso_cancelamento_inadimplencia_tb.aviso_cancelamento_inadimplencia_id = #aviso_negativo.aviso_cancelamento_inadimplencia_id       
 where aviso_cancelamento_inadimplencia_tb.aviso_cancelamento = 'A' /* parcelas avisadas */  
   and aviso_cancelamento_inadimplencia_tb.dt_envio_aviso is null /* nao enviado */       
   and aviso_cancelamento_inadimplencia_tb.situacao = 'A' /* registros ativos no GRID do SEGP1375 */     
   and aviso_cancelamento_inadimplencia_tb.produto_id <> 1225 /* 1225 vai no SEGA9177 */   
   and aviso_cancelamento_inadimplencia_tb.retorno = 'NEGATIVO' /* retorno negativo */  
   
   
   select aux.* from #retorno_negativo #retorno_negativo
   inner join evento_seguros_db.dbo.evento_tb aux with (nolock)  
   on aux.proposta_id  = #retorno_negativo.proposta_id      
   where aux.tp_evento_id = 20027    
   and aux.num_cobranca between #retorno_negativo.num_parcela -1 and #retorno_negativo.num_parcela 
   
   select *
  from interface_dados_db.dbo.sega9200_processado_tb aux with (nolock)    
  inner join #retorno_negativo #retorno_negativo
   on aux.proposta_id     = #retorno_negativo.proposta_id   
   where aux.tp_documento_id = 46    
   and aux.num_cobranca between #retorno_negativo.num_parcela -1 and #retorno_negativo.num_parcela  
  
  select * 
  from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb
where proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
and aviso_cancelamento_inadimplencia_tb.aviso_cancelamento = 'A' /* parcelas avisadas */  
   and aviso_cancelamento_inadimplencia_tb.dt_envio_aviso is null /* nao enviado */       
   and aviso_cancelamento_inadimplencia_tb.situacao = 'A' /* registros ativos no GRID do SEGP1375 */     
   and aviso_cancelamento_inadimplencia_tb.produto_id <> 1225 /* 1225 vai no SEGA9177 */   
   and aviso_cancelamento_inadimplencia_tb.retorno = 'NEGATIVO' /* retorno negativo */   
  

   select * 
  from seguros_db.dbo.agendamento_cobranca_atual_tb
where proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
order by proposta_id
  

  
   
use desenv_db
go
alter proc dbo.vaifilhao   
as

    update ag
    set dt_agendamento = dateadd(month, -1, getdate())
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (254349672,254352775,254353028,254349672,253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
and num_cobranca = (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 

    update ag
    set situacao = 'a'
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (254349672,254352775,254353028,254349672,253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
and num_cobranca <> (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 
					 
					 
    update ag
    set situacao = 'p'
  from seguros_db.dbo.agendamento_cobranca_atual_tb ag
where proposta_id in (254349672,254352775,254353028,254349672,253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
and num_cobranca = (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = ag.proposta_id ) 
					 



 update aviso_cancelamento_inadimplencia_tb
  set num_parcela =  (select max(aux.num_cobranca)     
					  from seguros_db.dbo.agendamento_cobranca_atual_tb aux with (nolock)                                                                                                                               
					 where aux.proposta_id = aviso_cancelamento_inadimplencia_tb.proposta_id ) 
  from seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aviso_cancelamento_inadimplencia_tb
where proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    
and aviso_cancelamento_inadimplencia_tb.aviso_cancelamento = 'A' /* parcelas avisadas */  
   and aviso_cancelamento_inadimplencia_tb.dt_envio_aviso is null /* nao enviado */       
   and aviso_cancelamento_inadimplencia_tb.situacao = 'A' /* registros ativos no GRID do SEGP1375 */     
   and aviso_cancelamento_inadimplencia_tb.produto_id <> 1225 /* 1225 vai no SEGA9177 */   
   and aviso_cancelamento_inadimplencia_tb.retorno = 'NEGATIVO' /* retorno negativo */   
  

update seguros_db.dbo.agendamento_cobranca_atual_tb
set canc_endosso_id = null
where proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    

delete evento_seguros_db.dbo.evento_tb 
where tp_evento_id = 20027    
and proposta_id in(253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    

 delete aux
  from interface_dados_db.dbo.SEGA9200_20_processado_tb aux with (nolock)    
  inner join #retorno_negativo #retorno_negativo
   on aux.proposta_id     = #retorno_negativo.proposta_id   
   where aux.proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    


 delete aux
  from interface_dados_db.dbo.SEGA9200_10_processado_tb aux with (nolock)    
  inner join #retorno_negativo #retorno_negativo
   on aux.proposta_id     = #retorno_negativo.proposta_id   
   where aux.proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    

 delete aux
  from interface_dados_db.dbo.sega9200_processado_tb aux with (nolock)    
  inner join #retorno_negativo #retorno_negativo
   on aux.proposta_id     = #retorno_negativo.proposta_id   
   where aux.tp_documento_id = 46    
   and aux.num_cobranca between #retorno_negativo.num_parcela -1 and #retorno_negativo.num_parcela  
    and aux.proposta_id in (253766992,253767090,253767102,253768046,253768120,253768153,253768199,253768286,253768306,253768351)    


begin tran
exec dbo.vaifilhao   
commit



