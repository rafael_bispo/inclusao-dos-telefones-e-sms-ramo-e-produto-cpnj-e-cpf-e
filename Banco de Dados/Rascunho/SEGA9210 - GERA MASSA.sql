 SELECT proposta.proposta_id,          
       proposta.produto_id,          
       proposta.prop_cliente_id,          
       cancelamento_proposta.endosso_id,          
       cancelamento_proposta.carta_emitida          
  INTO #proposta          
  FROM seguros_db.dbo.proposta_tb proposta with (NOLOCK)          
  INNER JOIN seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta  with (NOLOCK)             
    ON proposta.proposta_id = cancelamento_proposta.proposta_id  
  -- Inicio - AJuste inserindo condi��o para pegar endosso 63 - Marcio.Nogueira - Confitec-rj - 07/10/2015  
  INNER JOIN seguros_db.dbo.endosso_tb endosso_tb with (NOLOCK) on   
   endosso_tb.proposta_id = cancelamento_proposta.proposta_id       
  -- Fim - AJuste inserindo condi��o para pegar endosso 63 - Marcio.Nogueira - Confitec-rj - 07/10/2015               
 WHERE produto_id = 1231          
   AND cancelamento_proposta.tp_cancelamento = '2'        --pedido pelo segurado           
   AND cancelamento_proposta.dt_fim_cancelamento IS NULL            
   AND cancelamento_proposta.endosso_id IS NOT NULL             
   AND cancelamento_proposta.dt_inicio_cancelamento > '20030801'  
   AND endosso_tb.tp_endosso_id = 63 -- AJuste inserindo condi��o para pegar endosso 63 - Marcio.Nogueira - Confitec-rj - 07/10/2015     
          

select top 10 canc.carta_emitida,* 
from seguros_Db..cancelamento_proposta_tb canc
inner join #proposta prop
on prop.proposta_id = canc.proposta_id
INNER JOIN seguros_db.dbo.produto_tp_carta_tb produto_tp_carta  with (NOLOCK)            
ON prop.produto_id = produto_tp_carta.produto_id   
where  produto_tp_carta.tp_carta_id = '14' 
order by canc.dt_inclusao desc


use desenv_db
go
alter proc dbo.vaifilhao
as

update  canc
set carta_emitida = 'n'
from seguros_Db..cancelamento_proposta_tb canc
where proposta_id in (250613430,251744349,253989981,254302915,254482126,254630042,254734589,254869984,255214415,255378442)

delete evento_seguros_db..evento_impressao_tb  
where proposta_id in (250613430,251744349,253989981,254302915,254482126,254630042,254734589,254869984,255214415,255378442)        
and evento_seguros_db..evento_impressao_tb.tp_documento_id = 14   


begin tran
exec desenv_db.dbo.vaifilhao
commit