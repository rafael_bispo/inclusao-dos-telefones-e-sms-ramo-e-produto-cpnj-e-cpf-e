
DECLARE @produto_id INT  = null
SELECT DISTINCT produto_tb.produto_id  
INTO #produto  
FROM seguros_db..produto_tb produto_tb WITH (NOLOCK)  
LEFT JOIN interface_db..layout_produto_tb layout_produto_tb WITH (NOLOCK)  
  ON layout_produto_tb.layout_id = 731  
 AND layout_produto_tb.produto_id = ISNULL(@produto_id,layout_produto_tb.produto_id)  
WHERE (@produto_id IS NOT NULL AND produto_tb.produto_id = @produto_id)  
   OR (@produto_id IS NULL AND layout_produto_tb.produto_id = produto_tb.produto_id)  
  
  
SELECT top 10 proposta_tb.proposta_id,   
                financeiro_proposta_tb.proposta_bb,  
                proposta_tb.produto_id,    
                --apolice_id = certificado_re_tb.certificado_id,  
                apolice_tb.apolice_id,  
                seguro_moeda_id = financeiro_proposta_tb.seguro_moeda_id,   
                num_solicitacao = 0,  
                cep = CASE agencia_tb.agencia_id    
                        WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')    
                        ELSE ISNULL(endereco_corresp_tb.cep, '0')    
                      END  
INTO #SEGA8084_processar  
FROM renovacao_monitorada_db..proposta_tb proposta_tb WITH (NOLOCK)   
JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK)   
  ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id   
LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)     
  ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id       
LEFT JOIN seguros_db..agencia_tb agencia_tb WITH (NOLOCK)    
  ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id      
 AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id       
JOIN seguros_db..apolice_tb apolice_tb WITH (NOLOCK)       --Zoro.Gomes  
  ON proposta_tb.proposta_id = apolice_tb.proposta_id     
JOIN #produto  
  ON #produto.produto_id = proposta_tb.produto_id  
JOIN seguros_db..proposta_tb seguros_proposta  WITH (NOLOCK)  
  ON seguros_proposta.proposta_id = proposta_tb.proposta_id  
WHERE seguros_proposta.canal_id = 1   
  AND proposta_tb.situacao IN ('a', 'i')   
  AND financeiro_proposta_tb.desconto_renov IS NOT NULL --R.FOUREAUX - 14/08/2018 INCLUIDO TRAVA PARA QUE PROPOSTAS N�O CALCULADAS, N�O ENTREM NO PROCESSO DE ENVIO DA CARTA  
  AND EXISTS (SELECT 1   
                FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)  
               WHERE evento_impressao_tb.proposta_id = proposta_tb.proposta_id   
                 AND LEFT(evento_impressao_tb.arquivo_remessa_grf, 8) = 'SEGA8083'  --verifica se foi impresso a carta de aviso de vencimento simples  
                 AND evento_impressao_tb.tp_documento_id = 41)  
  AND NOT EXISTS (SELECT 1     
                  FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)    
                  WHERE proposta_tb.proposta_id = evento_impressao_tb.proposta_id    
                    AND LEFT(evento_impressao_tb.arquivo_remessa_grf, 8) = 'SEGA8084'  
                    AND evento_impressao_tb.tp_documento_id = 42)  
                    
                    group by proposta_tb.proposta_id,   
                financeiro_proposta_tb.proposta_bb,  
                proposta_tb.produto_id,    
                apolice_tb.apolice_id,  
                seguro_moeda_id,
                 financeiro_proposta_tb.seguro_moeda_id,   
                --num_solicitacao,  
                agencia_tb.agencia_id,
                agencia_tb.cep ,
                endereco_corresp_tb.cep ,
                financeiro_proposta_tb.dt_inicio_vigencia   
     ORDER BY  financeiro_proposta_tb.dt_inicio_vigencia DESC             
                
     
 use desenv_db
 go
 
 alter proc dbo.vaifilhao
 as

  UPDATE F
  SET F.dt_inicio_vigencia = DATEADD(year, 3,F.dt_inicio_vigencia), F.dt_fim_vigencia = DATEADD(year, 3,F.dt_fim_vigencia)
  FROM #SEGA8084_processar T    
  INNER JOIN renovacao_monitorada_db..financeiro_proposta_tb F 
  ON T.PROPOSTA_ID = F.PROPOSTA_ID     
  
  
  
  begin tran
	exec dbo.vaifilhao
  commit
   
  
  
  
  -- TESTES
  SELECT DISTINCT proposta_tb.proposta_id,   
                financeiro_proposta_tb.proposta_bb,  
                proposta_tb.produto_id,    
                apolice_tb.apolice_id,  
                seguro_moeda_id = financeiro_proposta_tb.seguro_moeda_id,   
                num_solicitacao = 0,  
                cep = CASE agencia_tb.agencia_id    
                        WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')    
                        WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')    
                        ELSE ISNULL(endereco_corresp_tb.cep, '0')    
                      END  
FROM renovacao_monitorada_db..proposta_tb proposta_tb WITH (NOLOCK)   
JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK)   
  ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id   
LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)     
  ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id       
LEFT JOIN seguros_db..agencia_tb agencia_tb WITH (NOLOCK)    
  ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id      
 AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id       
JOIN seguros_db..apolice_tb apolice_tb WITH (NOLOCK)       --Zoro.Gomes  
  ON proposta_tb.proposta_id = apolice_tb.proposta_id     
JOIN #produto  
  ON #produto.produto_id = proposta_tb.produto_id  
JOIN seguros_db..proposta_tb seguros_proposta  WITH (NOLOCK)  
  ON seguros_proposta.proposta_id = proposta_tb.proposta_id  
WHERE seguros_proposta.canal_id = 1   
  AND proposta_tb.situacao IN ('a', 'i')   
  AND financeiro_proposta_tb.desconto_renov IS NOT NULL --R.FOUREAUX - 14/08/2018 INCLUIDO TRAVA PARA QUE PROPOSTAS N�O CALCULADAS, N�O ENTREM NO PROCESSO DE ENVIO DA CARTA  
  AND EXISTS (SELECT 1   
                FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)  
               WHERE evento_impressao_tb.proposta_id = proposta_tb.proposta_id   
                 AND LEFT(evento_impressao_tb.arquivo_remessa_grf, 8) = 'SEGA8083'  --verifica se foi impresso a carta de aviso de vencimento simples  
                 AND evento_impressao_tb.tp_documento_id = 41)  
  AND NOT EXISTS (SELECT 1     
                  FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)    
                  WHERE proposta_tb.proposta_id = evento_impressao_tb.proposta_id    
                    AND LEFT(evento_impressao_tb.arquivo_remessa_grf, 8) = 'SEGA8084'  
                    AND evento_impressao_tb.tp_documento_id = 42)      
  AND DATEDIFF(DAY, GETDATE(), financeiro_proposta_tb.dt_inicio_vigencia) > 10 -- inclusão da verificação  
             