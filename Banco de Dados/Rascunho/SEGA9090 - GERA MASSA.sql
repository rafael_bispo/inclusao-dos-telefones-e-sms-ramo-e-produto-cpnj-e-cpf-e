SELECT tp_avaliacao_id  
     , nome  
     , origem  
  INTO #tp_avaliacao_tb  
  FROM als_produto_db..tp_avaliacao_tb WITH (NOLOCK)  
 WHERE origem = 'O' 
 
 select top 20 a.*
 into #massa_sega9090
 from seguros_Db..proposta_modular_tb a
inner join seguros_Db..proposta_tb b
on a.proposta_id = b.proposta_id
inner join avaliacao_proposta_tb c
on b.proposta_id = c.proposta_id
inner join #tp_avaliacao_tb d
ON c.tp_avaliacao_id = d.tp_avaliacao_id  
where c.num_avaliacao = (SELECT MAX (av.num_avaliacao)  
                         FROM avaliacao_proposta_tb av WITH (NOLOCK)  
                         WHERE av.proposta_id = c.proposta_id) 
and b.situacao = 's'
order by b.dt_inclusao desc



use desenv_db
go

alter proc dbo.vaifilhao
as

update p
set situacao = 'r'
from seguros_db..proposta_tb p
inner join #massa_sega9090 a
on a.proposta_id = p.proposta_Id

update a
set a.numero_agrupador = null
from seguros_Db..proposta_modular_tb a
inner join #massa_sega9090 b
on a.proposta_id = b.proposta_id

delete imp
 FROM evento_seguros_db..evento_impressao_tb imp 
 inner join #massa_sega9090 proposta_tb
on proposta_tb.proposta_id = imp.proposta_id     
  AND imp.tp_documento_id IN (26,34)  


begin tran
exec desenv_db.dbo.vaifilhao
commit


select a.*
into #proposta_tb
from seguros_db..proposta_tb a
inner join #massa_sega9090 b
on a.proposta_id = b.proposta_id

-- verifica se o update deu certo
SELECT proposta_id          = proposta_tb.proposta_id  
     , situacao             = proposta_tb.situacao  
     , segurado             = cliente_tb.nome  
     , endereco             = endereco_corresp_tb.endereco  
     , bairro               = endereco_corresp_tb.bairro  
     , municipio            = endereco_corresp_tb.municipio  
     , cep                  = endereco_corresp_tb.cep  
     , estado               = endereco_corresp_tb.estado  
     , dt_contratacao       = proposta_tb.dt_contratacao  
     , produto_id           = proposta_tb.produto_id  
     , dt_avaliacao         = avaliacao_proposta_tb.dt_avaliacao  
     , nome                 = tp_avaliacao_tb.nome  
     , origem               = tp_avaliacao_tb.origem  
     , destino              = NULL  
     , tp_doc               = '1'  
     , num_solicitacao      = NULL  
     , tp_avaliacao_id      = tp_avaliacao_tb.tp_avaliacao_id  
     , Tipo_Documento       = 'ORIGINAL'  
     , TipoDocumentoRetorno = '26'  
     , subramo_id           = proposta_tb.subramo_id  
     , arquivo              = NULL   
  FROM #proposta_tb proposta_tb WITH (NOLOCK)  
  JOIN seguros_db..cliente_tb cliente_tb WITH (NOLOCK)  
    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id     
  JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)  
    ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id    
  JOIN seguros_db..avaliacao_proposta_tb avaliacao_proposta_tb WITH (NOLOCK)  
    ON proposta_tb.proposta_id = avaliacao_proposta_tb.proposta_id  
   AND avaliacao_proposta_tb.num_avaliacao = (SELECT MAX (av.num_avaliacao)  
                                                FROM seguros_db..avaliacao_proposta_tb av WITH (NOLOCK)  
                                               WHERE av.proposta_id = proposta_tb.proposta_id)  
  JOIN #tp_avaliacao_tb tp_avaliacao_tb WITH (NOLOCK)  
    ON avaliacao_proposta_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id  
 WHERE (ISNULL(endereco_corresp_tb.emite_documento, 's') = 's'  
         OR (endereco_corresp_tb.emite_documento = 'a'  
         AND endereco_corresp_tb.situacao = 'c'))  
   AND NOT EXISTS (SELECT 1    
                     FROM evento_seguros_db..evento_impressao_tb imp WITH (NOLOCK)        
                    WHERE proposta_tb.proposta_id = imp.proposta_id     
                      AND imp.tp_documento_id IN (26,34)  
                    UNION   
                   SELECT 1  
                     FROM ABSS.evento_seguros_db.dbo.evento_impressao_tb imp2 WITH (NOLOCK)  
                    WHERE proposta_tb.proposta_id = imp2.proposta_id  
                      AND imp2.tp_documento_id IN (26,34))  