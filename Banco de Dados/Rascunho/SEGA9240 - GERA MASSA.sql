   declare @Produto_id INT  =null
  SELECT TOP 20 proposta_tb.proposta_id 
   ,proposta_tb.produto_id  
   ,financeiro_proposta_tb.proposta_bb  
   ,apolice_tb.apolice_id  
   ,41 as tipo_documento  
   ,0 as num_solicitacao  
   ,CASE agencia_tb.agencia_id  
    WHEN 4053  
     THEN ISNULL(agencia_tb.cep, '0')  
    WHEN 3859  
     THEN ISNULL(agencia_tb.cep, '0')  
    WHEN 4207  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4333  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4604  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4499  
     THEN ISNULL(agencia_tb.cep, '')  
    ELSE ISNULL(endereco_corresp_tb.cep, '0')  
    END as cep  
    INTO #TEMP
  FROM renovacao_monitorada_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)  
  INNER JOIN renovacao_monitorada_db.dbo.financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.cliente_tb cliente_tb WITH (NOLOCK) ON proposta_tb.cliente_id = cliente_tb.cliente_id  
  LEFT JOIN renovacao_monitorada_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK) ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.cobertura_proposta_tb cobertura_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = cobertura_proposta_tb.proposta_id  
   LEFT JOIN seguros_db.dbo.agencia_tb agencia_tb WITH (NOLOCK) ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id  
     AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id  
   LEFT JOIN seguros_db.dbo.municipio_tb municipio_tb WITH (NOLOCK) ON agencia_tb.municipio_id = municipio_tb.municipio_id  
      AND agencia_tb.estado = municipio_tb.estado  
  INNER JOIN interface_db.dbo.layout_tb layout_tb WITH (NOLOCK) ON layout_tb.layout_id = 1836  
  INNER JOIN interface_db.dbo.layout_produto_tb layout_produto_tb WITH (NOLOCK) ON layout_produto_tb.layout_id = 1836  
      AND layout_produto_tb.produto_id = proposta_tb.produto_id  
  INNER JOIN seguros_db.dbo.proposta_tb proposta_seguros_tb WITH (NOLOCK) ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id  
       WHERE proposta_tb.dt_envio_grafica IS NULL     --regra do sega9185  
      AND apolice_tb.dt_fim_vigencia >= '20151016' --regra do sega9185    
      AND proposta_tb.produto_id = ISNULL(@Produto_id, layout_produto_tb.produto_id)  
      AND proposta_tb.situacao IN ('a','i')  
      AND ISNULL(proposta_seguros_tb.canal_id, 1) = layout_tb.canal_id 
    GROUP BY proposta_tb.proposta_id 
   ,proposta_tb.produto_id  
   ,financeiro_proposta_tb.proposta_bb  
   ,apolice_tb.apolice_id  
	,agencia_tb.agencia_id 
	,agencia_tb.cep
	,endereco_corresp_tb.cep 
	,financeiro_proposta_tb.dt_inicio_vigencia
      ORDER BY  financeiro_proposta_tb.dt_inicio_vigencia DESC
      
 use desenv_db
 go
 
 alter proc dbo.vaifilhao
 as
 
   
  UPDATE F
  SET f.desconto_renov = 0.00
  FROM #TEMP T    
  INNER JOIN renovacao_monitorada_db..financeiro_proposta_tb F 
  ON T.PROPOSTA_ID = F.PROPOSTA_ID
    where f.desconto_renov IS NULL
 
  UPDATE F
  SET F.dt_inicio_vigencia = DATEADD(MONTH, 7,F.dt_inicio_vigencia), F.dt_fim_vigencia = DATEADD(MONTH, 7,F.dt_fim_vigencia)
  FROM #TEMP T    
  INNER JOIN renovacao_monitorada_db..financeiro_proposta_tb F 
  ON T.PROPOSTA_ID = F.PROPOSTA_ID
  
     delete e
  FROM evento_seguros_db.dbo.evento_impressao_tb e 
  inner join #TEMP T 
  on e.proposta_id = t.proposta_Id
    WHERE t.proposta_id = e.proposta_id  
     AND LEFT(e.arquivo_remessa_grf, 8) = 'SEGA9240'  
     AND e.STATUS <> 'S'  
     AND e.tp_documento_id = 41  

  
  
  begin tran
  exec dbo.vaifilhao
  commit
  
  select f.desconto_renov,*
  from #TEMP T    
  INNER JOIN renovacao_monitorada_db..financeiro_proposta_tb F 
  ON T.PROPOSTA_ID = F.PROPOSTA_ID
  where f.desconto_renov IS  not NULL


--testes
   declare @Produto_id INT  =null
  SELECT TOP 20 proposta_tb.proposta_id  
   ,proposta_tb.produto_id  
   ,financeiro_proposta_tb.proposta_bb  
   ,apolice_tb.apolice_id  
   ,41 as tipo_documento  
   ,0 as num_solicitacao  
   ,CASE agencia_tb.agencia_id  
    WHEN 4053  
     THEN ISNULL(agencia_tb.cep, '0')  
    WHEN 3859  
     THEN ISNULL(agencia_tb.cep, '0')  
    WHEN 4207  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4333  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4604  
     THEN ISNULL(agencia_tb.cep, '')  
    WHEN 4499  
     THEN ISNULL(agencia_tb.cep, '')  
    ELSE ISNULL(endereco_corresp_tb.cep, '0')  
    END as cep  
  FROM renovacao_monitorada_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)  
  INNER JOIN renovacao_monitorada_db.dbo.financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.cliente_tb cliente_tb WITH (NOLOCK) ON proposta_tb.cliente_id = cliente_tb.cliente_id  
  LEFT JOIN renovacao_monitorada_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK) ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
  INNER JOIN renovacao_monitorada_db.dbo.cobertura_proposta_tb cobertura_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = cobertura_proposta_tb.proposta_id  
   LEFT JOIN seguros_db.dbo.agencia_tb agencia_tb WITH (NOLOCK) ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id  
     AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id  
   LEFT JOIN seguros_db.dbo.municipio_tb municipio_tb WITH (NOLOCK) ON agencia_tb.municipio_id = municipio_tb.municipio_id  
      AND agencia_tb.estado = municipio_tb.estado  
  INNER JOIN interface_db.dbo.layout_tb layout_tb WITH (NOLOCK) ON layout_tb.layout_id = 1836  
  INNER JOIN interface_db.dbo.layout_produto_tb layout_produto_tb WITH (NOLOCK) ON layout_produto_tb.layout_id = 1836  
      AND layout_produto_tb.produto_id = proposta_tb.produto_id  
  INNER JOIN seguros_db.dbo.proposta_tb proposta_seguros_tb WITH (NOLOCK) ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id  
       WHERE proposta_tb.dt_envio_grafica IS NULL     --regra do sega9185  
      AND apolice_tb.dt_fim_vigencia >= '20151016' --regra do sega9185    
      AND proposta_tb.produto_id = ISNULL(@Produto_id, layout_produto_tb.produto_id)  
      AND proposta_tb.situacao IN ('a','i')  
      AND ISNULL(proposta_seguros_tb.canal_id, 1) = layout_tb.canal_id 
      AND DATEDIFF(DAY, GETDATE(), financeiro_proposta_tb.dt_inicio_vigencia) >= 10  
      ORDER BY  financeiro_proposta_tb.dt_inicio_vigencia DESC