



SELECT proposta_tb.prop_cliente_id,        
       proposta_tb.proposta_id,        
       endosso_tb.endosso_id,        
       1762 as layout_id,        
       produto_tb.produto_id,        
       proposta_tb.ramo_id  
       --INTO #MASSA9211
   FROM seguros_db..proposta_tb proposta_tb (NOLOCK)        
  JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)        
    ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id        
  JOIN seguros_db..endosso_tb endosso_tb (NOLOCK)        
    ON endosso_tb.proposta_id  = proposta_tb.proposta_id        
  JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb (NOLOCK)        
    ON cancelamento_proposta_tb.proposta_id = endosso_tb.proposta_id        
   AND cancelamento_proposta_tb.endosso_id = endosso_tb.endosso_id        
   AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL        
  JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)        
    ON certificado_tb.proposta_id = proposta_tb.proposta_id        
   AND certificado_tb.certificado_id = (SELECT MAX(certificado_id)       
                                          FROM seguros_db..certificado_tb certificado_tb (NOLOCK)      
                                         WHERE proposta_id = proposta_tb.proposta_id)      
  JOIN seguros_db..produto_tb produto_tb (NOLOCK)        
    ON proposta_tb.produto_id = produto_tb.produto_id        
 WHERE endosso_tb.dt_impressao IS NULL        
   AND endosso_tb.tp_endosso_id = 293        
   AND proposta_tb.situacao  = 'C'        
   AND produto_tb.produto_id  = 1231  
     
     
--MUDAR A SITUA��O DA PROPOSTA NA PROPOSTA_TB PARA W PARA PRESERVAR A MASSA DE TESTES
USE DESENV_DB
GO

ALTER PROC DBO.VAIFILHAO
AS

UPDATE TOP(10) A 
SET A.SITUACAO = 'C'
FROM SEGUROS_DB.DBO.PROPOSTA_TB A
INNER JOIN #MASSA9211 B
ON A.PROPOSTA_ID = B.PROPOSTA_ID
WHERE A.SITUACAO = 'W'

UPDATE A
SET A.SITUACAO = 'W'
FROM SEGUROS_DB.DBO.PROPOSTA_TB A
INNER JOIN #MASSA9211 B
ON A.PROPOSTA_ID = B.PROPOSTA_ID
     
     
BEGIN TRAN

EXEC DESENV_DB.DBO.VAIFILHAO     

COMMIT