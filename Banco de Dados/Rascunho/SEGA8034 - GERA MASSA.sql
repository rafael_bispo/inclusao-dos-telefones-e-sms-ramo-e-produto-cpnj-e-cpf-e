          
    CREATE TABLE #produto            
      (            
        produto_id      INT,            
      )            
              
    CREATE INDEX I_produto ON #produto (produto_id)          
     DECLARE @produto_id INT = NULL          
    INSERT           
      INTO #produto (produto_id)            
    SELECT DISTINCT layout_produto_tb.produto_id            
      FROM interface_db..layout_produto_tb layout_produto_tb with (NOLOCK)           
     WHERE layout_produto_tb.layout_id = 544          
       AND layout_produto_tb.produto_id = ISNULL(@produto_id,layout_produto_tb.produto_id)            
                
    CREATE TABLE #sega8034_processar_tb            
      (            
        proposta_id                    NUMERIC(9,0),          
        endosso_id                     INT,          
        prop_cliente_id                INT,          
        apolice_id                     INT,             
        proposta_bb                    INT,            
        ramo_id                        INT,            
        produto_Id                     INT,            
        layout_id                      INT,            
        tipo_documento                 VARCHAR(10),            
        origem                         CHAR(2),            
        tp_doc                         CHAR(1),            
        num_solicitacao                INT,          
        cep                            VARCHAR(8)          
      )            
              
    --Obtendo a data do sistema               
    SET @Data_sistema = (SELECT dt_operacional            
                         FROM parametro_geral_tb)            
                
    --Selecionando os produtos do Layout             
    SELECT @canal_id = canal_id            
    FROM interface_db..layout_tb             
    WHERE layout_id = 544            
              
    SELECT proposta_tb.proposta_id,          
           proposta_tb.produto_id,          
           proposta_tb.prop_cliente_id,          
           cancelamento_proposta_tb.endosso_id,          
           cancelamento_proposta_tb.carta_emitida          
      INTO #proposta          
      FROM proposta_tb  with (NOLOCK)          
      JOIN cancelamento_proposta_tb  with (NOLOCK)             
        ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id             
     WHERE produto_id IN (SELECT produto_id          
                            FROM #produto )          
       AND cancelamento_proposta_tb.tp_cancelamento = '2'        --pedido pelo segurado           
       AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL            
       AND cancelamento_proposta_tb.endosso_id IS NOT NULL             
       AND cancelamento_proposta_tb.dt_inicio_cancelamento > '20030801'             
              
    CREATE INDEX I_proposta ON #proposta (proposta_id)          
            
            
    /*Inclus�o de data de corte conforme demanda 618692*/        
    DELETE prop        
     FROM #proposta prop         
    INNER JOIN endosso_tb endo with (NOLOCK)         
       ON prop.proposta_id = endo.proposta_id         
      AND prop.endosso_id = endo.endosso_id        
    WHERE dt_pedido_endosso < '20081020'     
          
    /*Exclus�o dos cancelamentos pelos endossos 293, 297 e 350, pois existe carta espec�fica para eles*/    
    DELETE a    
    FROM #proposta a    
    INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)    
     ON a.proposta_id = endosso_tb.proposta_id    
      AND a.endosso_id = endosso_tb.endosso_id    
--  WHERE endosso_tb.tp_endosso_id IN (293, 297, 350)    
    WHERE endosso_tb.tp_endosso_id IN (293, 297, 350, 360, 340)   --Zoro.Gomes - Confitec - Projeto 19426678 - Endosso de Repactua��o PF - 08/12/2017    
    


 SELECT top 50 proposta_cancelamento.produto_id,             
           proposta_cancelamento.proposta_id,         
           proposta_cancelamento.endosso_id,          
           proposta_cancelamento.prop_cliente_id,          
           apolice_id = CASE             
                            WHEN certificado_tb.certificado_id IS NOT NULL            
                                THEN certificado_tb.certificado_id            
                            WHEN (apolice_tb.apolice_id IS NULL AND certificado_tb.certificado_id IS NULL)            
                                THEN proposta_adesao_tb.apolice_id            
                            ELSE apolice_tb.apolice_id             
       END,            
           ramo_id = CASE             
                         WHEN certificado_tb.certificado_id IS NOT NULL            
                             THEN certificado_tb.ramo_id            
                         WHEN (apolice_tb.ramo_id IS NULL AND certificado_tb.certificado_id IS NULL)            
                 THEN proposta_adesao_tb.ramo_id            
                         ELSE apolice_tb.ramo_id             
                     END,            
           tp_doc = '1',            
           origem = null,        
           num_solicitacao = 0,          
           endereco_corresp_tb.cep            
    FROM #proposta proposta_cancelamento  with (NOLOCK)    
    LEFT JOIN certificado_tb  with (NOLOCK)    
      ON proposta_cancelamento.proposta_id = certificado_tb.proposta_id            
     AND certificado_tb.dt_fim_vigencia IS NULL             
    LEFT JOIN apolice_tb with (NOLOCK)            
      ON proposta_cancelamento.proposta_id = apolice_tb.proposta_id            
    --05-12-2011 eduardo.amaral(Nova consultoria) demanda: 1236945 incluindo novo produto 1206    
    LEFT JOIN proposta_adesao_tb  with (NOLOCK)    
      ON proposta_cancelamento.proposta_id = proposta_adesao_tb.proposta_id    
    INNER JOIN proposta_tb  with (NOLOCK)    
      ON proposta_cancelamento.proposta_id = proposta_tb.proposta_id    
    INNER JOIN ramo_tb  with (NOLOCK)    
      ON ISNULL(proposta_adesao_tb.ramo_id, proposta_tb.ramo_id) = ramo_tb.ramo_id    
    --05-12-2011 eduardo.amaral(Nova consultoria)           
    INNER JOIN produto_tp_carta_tb  with (NOLOCK)            
      ON proposta_cancelamento.produto_id = produto_tp_carta_tb.produto_id             
    INNER JOIN endereco_corresp_tb  with (NOLOCK)    
      ON endereco_corresp_tb.proposta_id = proposta_cancelamento.proposta_id              
     AND (ISNULL(endereco_corresp_tb.emite_documento, 's') = 's' OR        
         (endereco_corresp_tb.emite_documento = 'a' and endereco_corresp_tb.situacao IN ('c', 'v')) )   
         INNER JOIN SEGUROS_DB..proposta_fechada_tb  proposta_fechada_tb with  (NOLOCK)        
         ON   proposta_cancelamento.PROPOSTA_ID = proposta_fechada_tb.PROPOSTA_ID
     LEFT JOIN evento_seguros_db..evento_impressao_tb with  (NOLOCK)            
       ON evento_seguros_db..evento_impressao_tb.proposta_id = proposta_cancelamento.proposta_id           
      AND evento_seguros_db..evento_impressao_tb.tp_documento_id = 14            
    WHERE ramo_tb.tp_ramo_id = 1             
   -- isnull(proposta_cancelamento.carta_emitida, 'n') = 'n'            
 
      AND produto_tp_carta_tb.tp_carta_id = '14'                --carta a pedido do segurado            
      AND evento_seguros_db..evento_impressao_tb.proposta_id IS NULL          
      AND endereco_corresp_tb.endereco <> ''  
      
      
         SELECT top 15* FROM #proposta a
   inner join seguros_db..proposta_fechada_tb b
   on a.proposta_Id = b.proposta_Id
   inner join seguros_db..produto_tp_carta_tb c
   on a.produto_id = c.produto_id
   WHERE proposta_bb IS NOT NULL  
   and tp_carta_id = '14'  
   order by b.dt_inclusao desc 


use desenv_db
go

alter proc dbo.vaifilhao
as

update seguros_db..cancelamento_proposta_tb
set carta_emitida = null
where proposta_id in (50845950,50807801,50808200,50808264,50782753,50783264,50783487,50784194,
50784795,50786289,50786443,50788153,50788506,50789172,50789350) 


delete from evento_seguros_db..evento_impressao_tb
where proposta_id in (50845950,50807801,50808200,50808264,50782753,50783264,50783487,50784194,
50784795,50786289,50786443,50788153,50788506,50789172,50789350)   
and tp_documento_id = 14  			
			
			
begin tran
exec desenv_db.dbo.vaifilhao
commit



seguros_db..cancelamento_proposta_tb
where proposta_id in (50845950,50807801,50808200,50808264,50782753,50783264,50783487,50784194,
50784795,50786289,50786443,50788153,50788506,50789172,50789350) 


select * from seguros_db..cancelamento_proposta_tb
where carta_emitida is null