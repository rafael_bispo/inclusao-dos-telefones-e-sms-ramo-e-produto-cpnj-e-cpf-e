--CONSULTA ORIGINAL
    
SELECT TOP 28    
       PROPOSTA_TB.proposta_id  
     , PROP_cliente_id  
     , PROPOSTA_TB.produto_id  
     , SUBRAMO_TB.num_proc_susep                      
     , endosso_id = MAX(ENDOSSO_TB.endosso_id)     
       INTO #PROPOSTAS   
  FROM SEGUROS_DB..PROPOSTA_TB PROPOSTA_TB WITH (NOLOCK)       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.proposta_id = ENDOSSO_TB.proposta_id      
  JOIN SEGUROS_DB..SUBRAMO_TB SUBRAMO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.subramo_id = SUBRAMO_TB.subramo_id      
 WHERE 1 = 1    
   AND PROPOSTA_TB.produto_id in (1225,1231,1198,1205,1243) --R.FOUREAUX - INCLUIDO PRODUTOS 1231,1198 E 1205 - 15/08/2017 // Louise Freitas - Confitec - Adicionando produto 1243  
   AND ENDOSSO_TB.tp_endosso_id = 340      
   AND SUBRAMO_TB.dt_fim_vigencia_sbr IS NULL     
  
   AND NOT EXISTS (SELECT 1     --Zoro.Gomes - Confitec - acerto no filtro de evento-impressao - 22/02/2016  
                     FROM EVENTO_SEGUROS_DB..EVENTO_IMPRESSAO_TB WITH (NOLOCK)      
                    WHERE proposta_id = PROPOSTA_TB.proposta_id      
                      AND endosso_id = (SELECT MAX(endosso_id)       
                                          FROM seguros_db..endosso_tb WITH (NOLOCK)      
                                         WHERE proposta_id = PROPOSTA_TB.proposta_id  
                                           AND tp_endosso_id = 340 )       
                      AND tp_documento_id = 4)   
 GROUP BY PROPOSTA_TB.proposta_id, PROP_cliente_id, PROPOSTA_TB.produto_id, SUBRAMO_TB.num_proc_susep      
    
    
SELECT Via = '1a Via',      
       num_solicitacao = 0,      
       #PROPOSTAS.proposta_id,                       
       #PROPOSTAS.endosso_id,    
       #PROPOSTAS.produto_id,      
       #PROPOSTAS.num_proc_susep,       
       destinatario = CLIENTE_TB.nome,      
       endereco = ENDERECO_CORRESP_TB.endereco,      
       bairro = ENDERECO_CORRESP_TB.bairro,      
       Cep_solicitante  = ENDERECO_CORRESP_TB.cep,      
       cidade_solicitante = ENDERECO_CORRESP_TB.municipio,      
       uf_solicitante  = ENDERECO_CORRESP_TB.estado,      
       produto = PRODUTO_TB.nome,       
       PROPOSTA_ADESAO_TB.apolice_id,      
       PROPOSTA_ADESAO_TB.proposta_bb,      
       CERTIFICADO_TB.certificado_id,      
       ENDOSSO_TB.dt_emissao,      
       max_endosso_id = NULL              --RMarins 04/01/16                  
  INTO #ENDOSSO      
  FROM #PROPOSTAS WITH (NOLOCK)       
  JOIN SEGUROS_DB..CLIENTE_TB CLIENTE_TB WITH (NOLOCK)       
    ON CLIENTE_TB.cliente_id = #PROPOSTAS.PROP_cliente_id                       
  JOIN SEGUROS_DB..PROPOSTA_ADESAO_TB PROPOSTA_ADESAO_TB WITH (NOLOCK)       
    ON PROPOSTA_ADESAO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..CERTIFICADO_TB CERTIFICADO_TB WITH (NOLOCK)       
    ON CERTIFICADO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..ENDERECO_CORRESP_TB ENDERECO_CORRESP_TB WITH (NOLOCK)       
    ON ENDERECO_CORRESP_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..PRODUTO_TB PRODUTO_TB WITH (NOLOCK)       
    ON PRODUTO_TB.produto_id = #PROPOSTAS.produto_id                       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON ENDOSSO_TB.proposta_id = #PROPOSTAS.proposta_id    
   AND ENDOSSO_TB.endosso_id = #PROPOSTAS.endosso_id      
    
    
--CONSULTA PARA GERAR MASSA    
SELECT TOP 28    
       PROPOSTA_TB.proposta_id  
     , PROP_cliente_id  
     , PROPOSTA_TB.produto_id  
     , SUBRAMO_TB.num_proc_susep                      
     , endosso_id = MAX(ENDOSSO_TB.endosso_id)     
  INTO #massa_sega9189
  FROM SEGUROS_DB..PROPOSTA_TB PROPOSTA_TB WITH (NOLOCK)       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.proposta_id = ENDOSSO_TB.proposta_id      
  JOIN SEGUROS_DB..SUBRAMO_TB SUBRAMO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.subramo_id = SUBRAMO_TB.subramo_id      
 WHERE 1 = 1    
   AND PROPOSTA_TB.produto_id in (1225,1231,1198,1205,1243) --R.FOUREAUX - INCLUIDO PRODUTOS 1231,1198 E 1205 - 15/08/2017 // Louise Freitas - Confitec - Adicionando produto 1243  
   AND ENDOSSO_TB.tp_endosso_id = 340      
   AND SUBRAMO_TB.dt_fim_vigencia_sbr IS NULL     
  
   AND EXISTS (SELECT 1     --Zoro.Gomes - Confitec - acerto no filtro de evento-impressao - 22/02/2016  
                     FROM EVENTO_SEGUROS_DB..EVENTO_IMPRESSAO_TB WITH (NOLOCK)      
                    WHERE proposta_id = PROPOSTA_TB.proposta_id      
                      AND endosso_id = (SELECT MAX(endosso_id)       
                                          FROM seguros_db..endosso_tb WITH (NOLOCK)      
                                         WHERE proposta_id = PROPOSTA_TB.proposta_id  
                                           AND tp_endosso_id = 340 )       
                      AND tp_documento_id = 4)   
 GROUP BY PROPOSTA_TB.proposta_id, PROP_cliente_id, PROPOSTA_TB.produto_id, SUBRAMO_TB.num_proc_susep,proposta_tb.dt_inclusao      
    order by proposta_tb.dt_inclusao desc
    
    
    
    
    
--GERAR A MASSA A PARTIR DA PROC


USE DESENV_DB
GO

ALTER PROC DBO.VAIFILHAO
AS

DELETE A
FROM evento_seguros_db..evento_impressao_tb A
INNER JOIN #massa_sega9189 B            
ON A.proposta_id = B.proposta_id  
and A.tp_documento_id = 4
 WHERE A.endosso_id = (SELECT MAX(endosso_id)       
                                          FROM seguros_db..endosso_tb WITH (NOLOCK)      
                                         WHERE proposta_id = A.proposta_id  
                                           AND tp_endosso_id = 340 ) 


begin tran

exec desenv_db.DBO.VAIFILHAO

commit