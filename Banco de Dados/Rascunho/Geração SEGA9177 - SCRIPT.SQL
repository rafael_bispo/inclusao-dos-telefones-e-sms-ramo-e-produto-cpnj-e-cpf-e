USE seguros_db

--1) ABRIR A TRANSACAO
BEGIN TRAN
SELECT @@TRANCOUNT


/* 
proposta_id
43422484
,43418304
*/

--identificando o sega
DECLARE @LAYOUT_ID INT
select @LAYOUT_ID = LAYOUT_ID from interface_db.dbo.layout_tb WHERE nome like 'SEGA9177'
select * from interface_db.dbo.layout_vigencia_tb WHERE layout_id = @LAYOUT_ID 

DECLARE @DATE SMALLDATETIME
SET @DATE = GETDATE()
SELECT @DATE

DECLARE @REMESSA INT
SELECT @REMESSA =  MAX(num_versao)+1  FROM interface_dados_db.dbo.SEGA9177_processado_tb (nolock) --ORDER BY SEGA9177_processado_id DESC
SELECT @REMESSA 



--SET @LAYOUT_ID = 1213


--2) CRIAR A TABELA TEMPORARIO
--delete from ##tmp_pre_SEGA9177_tb
--processando o SEGA

--drop table ##tmp_pre_SEGA9177_tb

CREATE TABLE ##tmp_pre_SEGA9177_tb (       
            tmp_pre_id             INT IDENTITY(1,1), 
            processar_id           INT,               
            registro_id            SMALLINT NULL,     
            registro_dependente_id SMALLINT NULL,     
            remessa                SMALLINT NULL,     
            ordenar_por            INT NULL,          
            arquivo                VARCHAR(8000) )    

SELECT @@TRANCOUNT
--rollback


select a.layout_id, sp_selecao_generica
from interface_db..layout_tb a
inner join interface_db..layout_vigencia_tb b
on a.layout_id = b.layout_id
where a.nome = 'SEGA9177'

--3) EXECUTAR AS PROCEDURES ABAIXO
--SP_SELECAO_GENERICA OBTIDO NO LAYOUT_VIGENCIA_TB COM OS PARAMETROS DA PROCEDURE
EXEC seguros_db..SEGS11924_SPI 1700, null, 'MU-2018-04260', 'n'



--GERAR O ARQUIVO
EXEC interface_db.dbo.exportar_arquivo_generico_spi 1700 , 'MU-2018-04260', 'S'

--PEGAR O ARQUIVO GERADO
select * from ##tmp_pre_SEGA9177_tb order by tmp_pre_id

--delete from interface_dados_db.dbo.SEGA9177_processar_tb where proposta_id = 241509594


SELECT * FROM interface_dados_db.dbo.SEGA9177_processar_tb (nolock)
SELECT * FROM interface_dados_db.dbo.SEGA9177_10_processar_tb (nolock)
SELECT * FROM interface_dados_db.dbo.SEGA9177_20_processar_tb (nolock)

 
-- SP_POS_PROCESSAMENTO_GERACAO_ARQUIVO NO LAYOUT_VIGENCIA_TB COM OS PARAMETROS DA PROCEDURE
EXEC seguros_db..SEGS5392_SPI 1276 , 'SEGA9177.0001',@DATE, @REMESSA, 'S', 'MU-2018-04260'



SELECT * FROM interface_dados_db.dbo.SEGA9177_processado_tb (nolock) where CulturaSeguradaNome is not null

rollback


--SELECT * FROM EVENTO_SEGUROS_DB.DBO.EVENTO_IMPRESSAO_TB EI(NOLOCK)
--where status = 'L' and proposta_id in (
--43422484
--,43418304
--)







