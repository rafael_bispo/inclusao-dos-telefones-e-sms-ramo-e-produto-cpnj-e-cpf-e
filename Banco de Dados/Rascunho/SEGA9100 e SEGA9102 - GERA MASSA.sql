select top 50 a.* 
into #massa9100
from evento_seguros_Db..evento_impressao_tb a WITH (NOLOCK)    
inner join  proposta_tb b WITH (NOLOCK)    
on a.proposta_Id = b.proposta_Id 
WHERE  b.situacao = 'r'  
--and a.destino =  'A' 
--and a.status = 'l'    
--AND dt_geracao_arquivo IS NULL    
AND tp_documento_id = 12    
order by b.dt_inclusao desc

use desenv_db
go

alter proc dbo.vaifilhao
as

update a
set a.status = 'l' , dt_geracao_arquivo = NULL ,a.destino =  'A' 
from evento_seguros_Db..evento_impressao_tb a
inner join #massa9100 b
on a.proposta_id = b.proposta_Id
and a.tp_documento_id = b.tp_documento_id 
and a.num_solicitacao = b.num_solicitacao
AND a.tp_documento_id = 12  
and a.destino is not null

sp_who2 active
begin tran
exec dbo.vaifilhao
commit
rollback

UK001_evento_impressao