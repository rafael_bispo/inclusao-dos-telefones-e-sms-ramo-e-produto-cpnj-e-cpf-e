
--CONSULTA ORIGINAL

SELECT top 10 cancelamento_proposta_tb.proposta_id,   
         segurado = cliente_tb.nome,  
         endereco_corresp_tb.endereco ,  
         endereco_corresp_tb.bairro,  
         endereco_corresp_tb.municipio,  
         endereco_corresp_tb.cep,   
         endereco_corresp_tb.estado,   
         cancelamento_proposta_tb.dt_cancelamento_bb,   
         cancelamento_proposta_tb.tp_cancelamento,   
         proposta_tb.produto_id,   
         cancelamento_proposta_tb.endosso_id,  
         apolice_id = proposta_adesao_tb.apolice_id,  
         certificado_tb.certificado_id,  
         ramo_id = PROPOSTA_TB.RAMO_ID,  
         NULL destino,   
         NULL diretoria_id,   
         '1' tp_doc,   
         NULL  origem,   
         0 num_solicitacao,  
         SUBRAMO_TB.num_proc_susep,  
         proposta_adesao_tb.cont_agencia_id,  
         proposta_adesao_tb.PROPOSTA_BB,  
         nome_corretor = CORRETOR_TB.nome,  
         NOME_PRODUTO = PRODUTO_TB.nome,  
         endosso_tb.dt_emissao,  
         SINISTRO_TB.SINISTRO_ID,
         cliente_tb.ddd_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.telefone_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.e_mail --migracao-documentacao-digital-2a-fase-cartas
    FROM seguros_db..proposta_tb proposta_tb WITH (nolock)  
    JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb WITH (nolock)   
      ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id   
    JOIN seguros_db..certificado_tb certificado_tb WITH (nolock)  
      ON cancelamento_proposta_tb.proposta_id = certificado_tb.proposta_id  
     --AND certificado_tb.dt_fim_vigencia IS NULL   
    JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb WITH (nolock)  
      ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id    
    JOIN seguros_db..subramo_tb subramo_tb WITH (nolock)  
      ON proposta_tb.subramo_id = subramo_tb.subramo_id   
    JOIN seguros_db..produto_tp_carta_tb produto_tp_carta_tb WITH (nolock)  
      ON proposta_tb.produto_id = produto_tp_carta_tb.produto_id   
    JOIN seguros_db..cliente_tb cliente_tb WITH (nolock)  
      ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id   
    JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb WITH (nolock)  
      ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..endosso_tb endosso_tb WITH (nolock)  
   ON cancelamento_proposta_tb.proposta_id = endosso_tb.proposta_id  
  AND cancelamento_proposta_tb.ENDOSSO_ID  = endosso_tb.endosso_id  
 JOIN seguros_db..corretagem_tb corretagem_tb  WITH (nolock)  
   ON corretagem_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..corretor_tb corretor_tb  WITH (nolock)  
   ON corretor_tb.corretor_id = corretagem_tb.corretor_id  
 JOIN seguros_db..produto_tb produto_tb  WITH (nolock)  
   ON produto_tb.produto_id = proposta_tb.produto_id  
 JOIN seguros_db..sinistro_tb sinistro_tb  WITH (nolock)  
   ON sinistro_tb.proposta_id = proposta_tb.proposta_id  
   WHERE cancelamento_proposta_tb.tp_cancelamento = 8           
  AND ENDOSSO_TB.TP_ENDOSSO_ID = 68  
     AND cancelamento_proposta_tb.dt_fim_cancelamento is NULL  
     AND isnull(endereco_corresp_tb.emite_documento, 's') = 's'   
     AND produto_tp_carta_tb.tp_carta_id = '14'                  
     AND cancelamento_proposta_tb.endosso_id IS NOT NULL   
  AND (proposta_tb.produto_id = 1231 or proposta_tb.produto_id = 1225) 
  AND subramo_tb.dt_fim_vigencia_sbr IS NULL  
     AND NOT exists (select 1   
        from evento_seguros_db..evento_impressao_tb imp WITH (nolock)  
                      where proposta_tb.proposta_id = imp.proposta_id  
                        and imp.tp_documento_id = 14) 
order by proposta_tb.dt_inclusao desc        



-- CONSULTA MODIFICADA PARA PEGAR MASSA
SELECT top 10 cancelamento_proposta_tb.proposta_id,   
         segurado = cliente_tb.nome,  
         endereco_corresp_tb.endereco ,  
         endereco_corresp_tb.bairro,  
         endereco_corresp_tb.municipio,  
         endereco_corresp_tb.cep,   
         endereco_corresp_tb.estado,   
         cancelamento_proposta_tb.dt_cancelamento_bb,   
         cancelamento_proposta_tb.tp_cancelamento,   
         proposta_tb.produto_id,   
         cancelamento_proposta_tb.endosso_id,  
         apolice_id = proposta_adesao_tb.apolice_id,  
         certificado_tb.certificado_id,  
         ramo_id = PROPOSTA_TB.RAMO_ID,  
         NULL destino,   
         NULL diretoria_id,   
         '1' tp_doc,   
         NULL  origem,   
         0 num_solicitacao,  
         SUBRAMO_TB.num_proc_susep,  
         proposta_adesao_tb.cont_agencia_id,  
         proposta_adesao_tb.PROPOSTA_BB,  
         nome_corretor = CORRETOR_TB.nome,  
         NOME_PRODUTO = PRODUTO_TB.nome,  
         endosso_tb.dt_emissao,  
         SINISTRO_TB.SINISTRO_ID,
         cliente_tb.ddd_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.telefone_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.e_mail --migracao-documentacao-digital-2a-fase-cartas
         into #massa9183
    FROM seguros_db..proposta_tb proposta_tb WITH (nolock)  
    JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb WITH (nolock)   
      ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id   
    JOIN seguros_db..certificado_tb certificado_tb WITH (nolock)  
      ON cancelamento_proposta_tb.proposta_id = certificado_tb.proposta_id  
     --AND certificado_tb.dt_fim_vigencia IS NULL   
    JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb WITH (nolock)  
      ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id    
    JOIN seguros_db..subramo_tb subramo_tb WITH (nolock)  
      ON proposta_tb.subramo_id = subramo_tb.subramo_id   
    JOIN seguros_db..produto_tp_carta_tb produto_tp_carta_tb WITH (nolock)  
      ON proposta_tb.produto_id = produto_tp_carta_tb.produto_id   
    JOIN seguros_db..cliente_tb cliente_tb WITH (nolock)  
      ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id   
    JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb WITH (nolock)  
      ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..endosso_tb endosso_tb WITH (nolock)  
   ON cancelamento_proposta_tb.proposta_id = endosso_tb.proposta_id  
  AND cancelamento_proposta_tb.ENDOSSO_ID  = endosso_tb.endosso_id  
 JOIN seguros_db..corretagem_tb corretagem_tb  WITH (nolock)  
   ON corretagem_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..corretor_tb corretor_tb  WITH (nolock)  
   ON corretor_tb.corretor_id = corretagem_tb.corretor_id  
 JOIN seguros_db..produto_tb produto_tb  WITH (nolock)  
   ON produto_tb.produto_id = proposta_tb.produto_id  
 JOIN seguros_db..sinistro_tb sinistro_tb  WITH (nolock)  
   ON sinistro_tb.proposta_id = proposta_tb.proposta_id  
   WHERE cancelamento_proposta_tb.tp_cancelamento = 8           
  AND ENDOSSO_TB.TP_ENDOSSO_ID = 68  
     AND cancelamento_proposta_tb.dt_fim_cancelamento is NULL  
     AND isnull(endereco_corresp_tb.emite_documento, 's') = 's'   
     AND produto_tp_carta_tb.tp_carta_id = '14'                  
     AND cancelamento_proposta_tb.endosso_id IS NOT NULL   
  AND (proposta_tb.produto_id = 1231 or proposta_tb.produto_id = 1225) 
  AND subramo_tb.dt_fim_vigencia_sbr IS NULL  
     AND exists (select 1   
        from evento_seguros_db..evento_impressao_tb imp WITH (nolock)  
                      where proposta_tb.proposta_id = imp.proposta_id  
                        and imp.tp_documento_id = 14) 
order by proposta_tb.dt_inclusao desc            



--GERAR A MASSA A PARTIR DA PROC


USE DESENV_DB
GO

ALTER PROC DBO.VAIFILHAO
AS

DELETE A
FROM evento_seguros_db..evento_impressao_tb A
INNER JOIN #massa9183 B            
ON A.proposta_id = B.proposta_id  
and A.tp_documento_id = 14


begin tran

exec desenv_db.DBO.VAIFILHAO

commit