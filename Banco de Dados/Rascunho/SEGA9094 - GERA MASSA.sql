select top 10 proposta_bonificacao_tb.*
into #massaSEGA9094
from seguros_db..proposta_bonificacao_tb proposta_bonificacao_tb(NOLOCK)    
  JOIN seguros_db..proposta_tb proposta_tb(NOLOCK)    
    ON proposta_tb.proposta_id = proposta_bonificacao_tb.proposta_id  
JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
  JOIN seguros_db..sinistro_tb sinistro_tb  
    ON sinistro_tb.proposta_id = proposta_tb.proposta_id  
    WHERE proposta_bonificacao_tb.dt_inclusao > '20091015'    
    and proposta_bonificacao_tb.situacao = 'R'  
    AND proposta_bonificacao_tb.dt_envio_carta IS not NULL  
    order by proposta_bonificacao_tb.dt_inclusao desc
   


use desenv_Db
go

alter proc dbo.vaifilhao
as

update pb
set pb.dt_envio_carta = NULL  
from seguros_Db.dbo.proposta_bonificacao_tb pb
inner join #massaSEGA9094 mass
on pb.proposta_Id = mass.proposta_Id
and pb.lock = mass.lock


begin tran

exec desenv_db.dbo.vaifilhao

commit