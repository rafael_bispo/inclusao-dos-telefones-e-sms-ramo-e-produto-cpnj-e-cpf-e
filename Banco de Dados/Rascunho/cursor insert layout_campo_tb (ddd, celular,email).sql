if (object_id('tempdb..#segas') is not null)
  drop table #segas

create table #segas (id int identity (1,1), nome varchar(10), layout_id INT)

if (object_id('tempdb..#layout_campo_tb') is not null)
  drop table #layout_campo_tb

CREATE TABLE #layout_campo_tb(
	layout_id smallint NOT NULL,
	dt_ini_vigencia smalldatetime NOT NULL,
	registro_id smallint NOT NULL,
	campo_id smallint NOT NULL,
	nome varchar(60) NOT NULL,
	seq_campo smallint NOT NULL,
	descricao varchar(100) NULL,
	constante varchar(60) NULL,
	tp_dado char(2) NOT NULL,
	formato varchar(30) NULL,
	tamanho smallint NOT NULL,
	obrigatorio char(1) NOT NULL,
	dt_inclusao dbo.UD_dt_inclusao NOT NULL,
	dt_alteracao dbo.UD_dt_alteracao NULL,
	usuario dbo.UD_usuario NOT NULL,
	tp_campo char(1) NOT NULL,
	banco_origem varchar(60) NULL,
	tabela_origem varchar(60) NULL,
	coluna_origem varchar(100) NULL,
	banco varchar(60) NULL,
	tabela varchar(60) NULL,
	coluna varchar(100) NULL,
	layout_campo_formato_id int NULL,
	preencher_com_zeros char(1) NULL,
	qtde_casas_decimais int NULL,
	acrescentar_ao_contador int NULL,
	alinhamento char(1) NULL
 ) 
 
 if (object_id('tempdb..#alter_table') is not null)
  drop table #alter_table
  
  create table #alter_table (comando varchar(150))
 

insert into #segas (nome)
select 'SEGA9134' union
select 'SEGA9160' union
select 'SEGA9231' 

update S 
set s.layout_id = b.layout_id
from #segas s
inner join interface_db..layout_tb b
on s.nome = b.nome


declare @sega varchar(10)

DECLARE sega_cursor CURSOR FOR   
SELECT nome
FROM #segas  

OPEN sega_cursor  
  
FETCH NEXT FROM sega_cursor   
INTO @sega

 
WHILE @@FETCH_STATUS = 0  
BEGIN  

	
declare @layout_Id int, @seq_campo int, @campo_id int, @registro_id int
, @dt_ini_vigencia smalldatetime, @tabela varchar(50), @banco varchar(30)

select @layout_Id = layout_id
from interface_db.dbo.layout_tb 
where nome = @sega

select @registro_id = max(registro_id)
from interface_db..layout_tb a
inner join interface_db..layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id not in(select max(registro_id) 
			from interface_db..layout_campo_tb c
			where c.layout_id = b.layout_id)

select @tabela = b.tabela, @banco = b.banco
from interface_db..layout_tb a
inner join interface_db..layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id
group by b.tabela,b.banco

select @seq_campo = max(seq_campo)
from interface_db..layout_tb a
inner join interface_db..layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id
and tp_dado <> 'FL'


select @campo_id = max(campo_id) 
from interface_db..layout_tb a
inner join interface_db..layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id
and tp_dado <> 'FL'

select @dt_ini_vigencia = dt_ini_vigencia 
from interface_db..layout_tb a
inner join interface_db..layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id
group by dt_ini_vigencia

--select @sega as '@sega',@layout_Id as '@layout_Id',@seq_campo as '@seq_campo',@campo_id as '@campo_id',@registro_id as '@registro_id'
--, @dt_ini_vigencia as '@dt_ini_vigencia', @tabela as '@tabela'

--INSERE REGISTROS DA TABELA ORIGINAL
INSERT INTO #layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT a.layout_id, dt_ini_vigencia, registro_id, campo_id, a.nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento
FROM INTERFACE_DB..layout_campo_tb a
inner join #segas S
on a.layout_id = s.layout_id
where tp_dado = 'FL'
and registro_id = @registro_id
and a.layout_id = @layout_Id
AND NOT EXISTS (SELECT * FROM #layout_campo_tb C WHERE a.layout_id = c.layout_id AND c.REGISTRO_ID = a.registro_id)



INSERT INTO #layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT @layout_Id, @dt_ini_vigencia, @registro_id, @campo_id + 1, N'DDD Celular      ', @seq_campo + 1, N'DDD Celular', null, N'NM', N'      ', 4, N'n', getdate(), null, N'MigracaoDocDigital', N'F', @banco, @tabela, N'ddd_celular   ', @banco, @tabela, N'ddd_celular   ', NULL, NULL, NULL, NULL, N'P' UNION ALL
SELECT @layout_Id, @dt_ini_vigencia, @registro_id, @campo_id + 2, N'Celular', @seq_campo + 2, N'Celular', null, N'NM', N'      ', 9, N'n', getdate(), null, N'MigracaoDocDigital', N'F', @banco, @tabela, N'celular   ', @banco, @tabela, N'celular   ', NULL, NULL, NULL, NULL, N'P' UNION ALL
SELECT @layout_Id, @dt_ini_vigencia, @registro_id, @campo_id + 3, N'Email', @seq_campo + 3, N'Email', null, N'AN', N'      ', 60, N'n', getdate(), null , N'MigracaoDocDigital', N'F', @banco, @tabela, N'email   ', @banco, @tabela, N'email   ', NULL, NULL, NULL, NULL, N'P' UNION ALL
SELECT @layout_Id, @dt_ini_vigencia, @registro_id, @campo_id + 4, N'Ag�ncia', @seq_campo + 4, N'Ag�ncia', null, N'NM', N'      ', 4, N'n', getdate(), null , N'MigracaoDocDigital', N'F', @banco, @tabela, N'agencia   ', @banco, @tabela, N'agencia   ', NULL, NULL, NULL, NULL, N'P' 

--INCLUSAO DE REGISTROS PARA ALTER TABLE
insert into #alter_table (comando)
select 'ALTER TABLE ' + RTRIM(LTRIM(@banco)) + '.dbo.' + RTRIM(LTRIM(@tabela)) + ' ADD ddd_celular VARCHAR (4)' UNION ALL
select 'ALTER TABLE ' + RTRIM(LTRIM(@banco)) + '.dbo.' + RTRIM(LTRIM(@tabela)) + ' ADD celular VARCHAR (9)' UNION ALL
select 'ALTER TABLE ' + RTRIM(LTRIM(@banco)) + '.dbo.' + RTRIM(LTRIM(@tabela)) + ' ADD email VARCHAR (60)' UNION ALL
select 'ALTER TABLE ' + RTRIM(LTRIM(@banco)) + '.dbo.' + RTRIM(LTRIM(@tabela)) + ' ADD agencia int' 


delete b
FROM #layout_campo_tb b
where b.tp_dado = 'FL'
and b.registro_id <> @registro_id
and b.layout_id = @layout_Id

update #layout_campo_tb
set coluna_origem = null, coluna = null
where tabela is null 

declare @max_seq_campo int, @max_campo_id int

select @max_seq_campo = max(b.seq_campo) + 1, @max_campo_id = max(b.campo_id ) +1
from interface_db..layout_tb a
inner join #layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id


update b
set b.seq_campo = @max_seq_campo, b.campo_id = @max_campo_id
from interface_db..layout_tb a
inner join #layout_campo_tb b
on a.layout_id = b.layout_id
where a.nome = @sega
and registro_id = @registro_id
and tp_dado = 'FL'

--FAZ UPDATE DO FILLER
--update A
--set A.seq_campo = b.seq_campo, A.campo_id = b.campo_id
--from interface_db..layout_campo_tb a
--inner join #layout_campo_tb b
--on a.layout_id = b.layout_id
--where a.nome = @sega
--and registro_id = @registro_id
--and tp_dado = 'FL'

  
FETCH NEXT FROM sega_cursor   
INTO @sega
END   
CLOSE sega_cursor;  
DEALLOCATE sega_cursor;  


--INSERIR NAS TABELAS FISICAS

-- sem formata��o 
/*
SELECT 'INSERT INTO dbo.layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)'
SELECT 'SELECT',layout_id,',''', (dt_ini_vigencia), ''',',registro_id, ',',campo_id, ',''',nome, ''',',seq_campo, ',''',descricao, ''',',constante, ',''',tp_dado, ''',',formato, ',',tamanho, ',''',obrigatorio, ''',''',dt_inclusao, ''',''',dt_alteracao, ''',''',usuario, ''',''',tp_campo, ''',''',banco_origem, ''',''',tabela_origem, ''',''',coluna_origem, ''',''',banco, ''',''',tabela, ''',''',coluna, ''',',layout_campo_formato_id, ',',preencher_com_zeros, ',',qtde_casas_decimais, ',',acrescentar_ao_contador, ',''',alinhamento, ''''
FROM #layout_campo_tb 
*/

-- com formata��o 
SELECT 'INSERT INTO dbo.layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)'
SELECT CONCAT ('SELECT ',layout_id,',''',  convert(varchar, dt_ini_vigencia ,120), ''',',registro_id, ',',campo_id, ',''',rtrim(nome), ''',',seq_campo, ',''',descricao, ''',',isnull(constante,null), ',''',tp_dado, ''',',formato, ',',tamanho, ',''',obrigatorio, ''',''', convert(varchar, dt_inclusao ,120), ''',''', convert(varchar, dt_alteracao ,120), ''',''',usuario, ''',''',tp_campo, ''',''',banco_origem, ''',''',tabela_origem, ''',''',rtrim(coluna_origem), ''',''',banco, ''',''',tabela, ''',''',rtrim(coluna), ''',',layout_campo_formato_id, ',',preencher_com_zeros, ',',qtde_casas_decimais, ',',acrescentar_ao_contador, ',''',alinhamento, '''')
FROM #layout_campo_tb 
where nome <> 'FILLER'



select * FROM #alter_table  

/*

select * from #layout_campo_tb
where layout_id = 1585
order by registro_id, seq_campo, campo_id

select * from interface_db..layout_campo_tb
where layout_id = 1487
order by registro_id, seq_campo, campo_id


1487
SELECT * FROM #alter_table



*/







