USE seguros_db

--1) ABRIR A TRANSACAO
BEGIN TRAN
SELECT @@TRANCOUNT


/* 
proposta_id
43422484
,43418304
*/

--identificando o sega
DECLARE @LAYOUT_ID INT
select @LAYOUT_ID = LAYOUT_ID from interface_db.dbo.layout_tb WHERE nome like 'SEGA8133'
select * from interface_db.dbo.layout_vigencia_tb WHERE layout_id = @LAYOUT_ID 

DECLARE @DATE SMALLDATETIME
SET @DATE = GETDATE()
SELECT @DATE

DECLARE @REMESSA INT
SELECT @REMESSA =  MAX(num_versao)+1  FROM interface_dados_db.dbo.SEGA8133_processado_tb (nolock) --ORDER BY SEGA8133_processado_id DESC
SELECT @REMESSA 



--SET @LAYOUT_ID = 1213


--2) CRIAR A TABELA TEMPORARIO
--delete from ##tmp_pre_SEGA8133_tb
--processando o SEGA

--drop table ##tmp_pre_SEGA8133_tb

CREATE TABLE ##tmp_pre_SEGA8133_tb (       
            tmp_pre_id             INT IDENTITY(1,1), 
            processar_id           INT,               
            registro_id            SMALLINT NULL,     
            registro_dependente_id SMALLINT NULL,     
            remessa                SMALLINT NULL,     
            ordenar_por            INT NULL,          
            arquivo                VARCHAR(8000) )    

SELECT @@TRANCOUNT
--rollback


select a.layout_id, sp_selecao_generica
from interface_db..layout_tb a
inner join interface_db..layout_vigencia_tb b
on a.layout_id = b.layout_id
where a.nome = 'SEGA8133'


-- USA-SE O PROCESSO DO SEGA8040 PARA POPULAR A TABELA PRINCIPAL (INICIO)
---- TESTES -------------------------------------------------    
 DECLARE @layout_id      INT    
 DECLARE @produto_id     INT    
 DECLARE @usuario        VARCHAR(20)    
 DECLARE @producao       CHAR(1)    
     
 SET @layout_id = 552 --SEGA8040    
 SET @produto_id = NULL    
 SET @usuario = 'producao3'    
 SET @producao = 'N'    
-------------------------------------------------------------    
    
DECLARE @msg              VARCHAR(100)      
    
--- Processar  
EXEC  desenv_db.dbo.SEGS6342_SPI  @layout_id,      
                   @produto_id,      
                   @usuario  
-- USA-SE O PROCESSO DO SEGA8040 PARA POPULAR A TABELA PRINCIPAL (FIM)                   


--3) EXECUTAR AS PROCEDURES ABAIXO
--SP_SELECAO_GENERICA OBTIDO NO LAYOUT_VIGENCIA_TB COM OS PARAMETROS DA PROCEDURE
EXEC seguros_db..SEGS6496_SPI 1099, null, 'MU-2018-04260', 'n'



--GERAR O ARQUIVO
EXEC interface_db.dbo.exportar_arquivo_generico_spi 1099 , 'MU-2018-04260', 'S'

--PEGAR O ARQUIVO GERADO
select * from ##tmp_pre_SEGA8133_tb order by tmp_pre_id

--delete from interface_dados_db.dbo.SEGA8133_processar_tb where proposta_id = 241509594


SELECT * FROM interface_dados_db.dbo.SEGA8133_processar_tb (nolock)
SELECT * FROM interface_dados_db.dbo.SEGA8133_10_processar_tb (nolock)
SELECT * FROM interface_dados_db.dbo.SEGA8133_20_processar_tb (nolock)

 
-- SP_POS_PROCESSAMENTO_GERACAO_ARQUIVO NO LAYOUT_VIGENCIA_TB COM OS PARAMETROS DA PROCEDURE
EXEC seguros_db..SEGS6501_SPI 1099 , 'SEGA8133.0001',@DATE, @REMESSA, 'S', 'MU-2018-04260'



SELECT * FROM interface_dados_db.dbo.SEGA8133_processado_tb (nolock) where CulturaSeguradaNome is not null

rollback


--SELECT * FROM EVENTO_SEGUROS_DB.DBO.EVENTO_IMPRESSAO_TB EI(NOLOCK)
--where status = 'L' and proposta_id in (
--43422484
--,43418304
--)







