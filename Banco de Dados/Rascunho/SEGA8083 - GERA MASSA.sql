DECLARE @layout_id      INT              
DECLARE @produto_id     INT      
DECLARE @usuario        VARCHAR(20)      
DECLARE @producao       CHAR(1)  
  
SET @layout_id     = 730  
SET @produto_id    = NULL  
SET @usuario       = 'DEM 17781082'   
SET @producao      = 'S'   
                

 SELECT DISTINCT   
         produto_tb.produto_id    
       , produto_tb.nome           
    INTO #produto_temp_tb                
    FROM seguros_db..produto_tb produto_tb WITH (NOLOCK)                 
    LEFT JOIN interface_db..layout_produto_tb layout_produto_tb WITH (NOLOCK)                 
      ON layout_produto_tb.layout_id = 730                
     AND layout_produto_tb.produto_id = ISNULL(@produto_id,layout_produto_tb.produto_id)                
   WHERE (@produto_id IS NOT NULL AND produto_tb.produto_id = @produto_id)                
      OR (@produto_id IS NULL AND layout_produto_tb.produto_id = produto_tb.produto_id)    
      
      

 SELECT DISTINCT   
         proposta_tb.proposta_id                 
       , proposta_tb.produto_id      
       , nome_produto = #produto_temp_tb.nome    
       , financeiro_proposta_tb.proposta_bb                 
       , cont_agencia_id = ISNULL(financeiro_proposta_tb.cont_agencia_id, 0)                 
       , proposta_tb.cliente_id   
       , num_solicitacao = 0              
       , cep = CASE agencia_tb.agencia_id                
                   WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')                
                   ELSE ISNULL(endereco_corresp_tb.cep, '0')                
              END     
       , dt_inicio_vigencia = ISNULL(financeiro_proposta_tb.dt_inicio_vigencia, '')     
       , dt_fim_vigencia = ISNULL(financeiro_proposta_tb.dt_fim_vigencia, '')               
  
   -- INTO #SEGA8083_processar_tb                
    FROM renovacao_monitorada_db..proposta_tb proposta_tb WITH (NOLOCK)  
      
    JOIN seguros_db..subramo_tb subramo_tb WITH (NOLOCK)     
      ON proposta_tb.ramo_id = subramo_tb.ramo_id     
     AND proposta_tb.subramo_id = subramo_tb.subramo_id     
     AND proposta_tb.dt_inicio_vigencia_sbr = subramo_tb.dt_inicio_vigencia_sbr   
      
    JOIN #produto_temp_tb WITH (NOLOCK)                 
      ON proposta_tb.produto_id = #produto_temp_tb.produto_id                 
      
    JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK)                 
      ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id                   
  
    JOIN seguros_db..proposta_tb proposta_seguros_tb WITH (NOLOCK)                
      ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id       
     AND proposta_seguros_tb.canal_id = 1        
      
    JOIN seguros_db..apolice_tb apolice_tb WITH (NOLOCK)         
      ON proposta_tb.proposta_id = apolice_tb.proposta_id         
  
    LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)                 
      ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id                 
      
    LEFT JOIN seguros_db..agencia_tb agencia_tb WITH (NOLOCK)                
      ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id                 
     AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id        --Zoro.Gomes - confitec - Acrescentado o banco - 31/08/2015  
      
   WHERE proposta_tb.situacao in ('P','A')   
     AND proposta_tb.dt_envio_grafica IS NULL                 
     AND apolice_tb.dt_fim_vigencia >= '20151016'  --data de corte - Zoro.Gomes  - confitec - 01/09/2015  
     AND NOT EXISTS (SELECT 1 FROM evento_seguros_db..evento_impressao_tb imp WITH (NOLOCK)                
                      WHERE proposta_tb.proposta_id = imp.proposta_id                
                        AND imp.arquivo_remessa_grf LIKE 'SEGA8083%')   
                        
                        
SELECT TOP 50   
         proposta_tb.proposta_id                 
       , proposta_tb.produto_id        
       , nome_produto = #produto_temp_tb.nome              
       , financeiro_proposta_tb.proposta_bb                 
       , cont_agencia_id = ISNULL(financeiro_proposta_tb.cont_agencia_id, 0)                 
       , proposta_tb.cliente_id   
       , evt_imp.num_solicitacao              
       , cep = CASE agencia_tb.agencia_id                
                   WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')                
                   WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')                
                   ELSE ISNULL(endereco_corresp_tb.cep, '0')                
              END     
       , dt_inicio_vigencia = ISNULL(financeiro_proposta_tb.dt_inicio_vigencia, '')    
       , dt_fim_vigencia = ISNULL(financeiro_proposta_tb.dt_fim_vigencia, '')    
	into #MASSA8083
    FROM evento_seguros_db..evento_impressao_tb evt_imp WITH (NOLOCK)                
    JOIN renovacao_monitorada_db..proposta_tb proposta_tb WITH (NOLOCK)                
      ON evt_imp.proposta_id = proposta_tb.proposta_id          
      
    JOIN #produto_temp_tb WITH (NOLOCK)                 
      ON proposta_tb.produto_id = #produto_temp_tb.produto_id                 
      
    JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK)                 
      ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id                             
  
    JOIN seguros_db..proposta_tb proposta_seguros_tb WITH (NOLOCK)                
      ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id       
     AND proposta_seguros_tb.canal_id = 1        
      
    LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)                 
      ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id                 
      
    LEFT JOIN seguros_db..agencia_tb agencia_tb WITH (NOLOCK)                
      ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id              
     AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id        --Zoro.Gomes - confitec - Acrescentado o banco - 31/08/2015  
  
   WHERE evt_imp.dt_solicitacao >='20100101'
     AND evt_imp.tp_documento_id = 41  
     AND evt_imp.status = 'l'                 
     AND evt_imp.dt_geracao_arquivo IS NULL                 
     
     
     
USE DESENV_DB
GO

ALTER PROC DBO.VAIFILHAO
AS

UPDATE EVT_IMP
SET evt_imp.status = 'l', evt_imp.dt_geracao_arquivo = NULL                 
FROM evento_seguros_db..evento_impressao_tb EVT_IMP
INNER JOIN #MASSA8083 MASS
ON EVT_IMP.PROPOSTA_ID = MASS.PROPOSTA_ID
AND EVT_IMP.num_solicitacao = MASS.num_solicitacao

BEGIN TRAN

EXEC DESENV_DB.DBO.VAIFILHAO

ROLLBACK
COMMIT
                         