CREATE PROCEDURE SEGS12928_SPI  
  
AS  
  
/*  
 ------------------------------------------------------------------------------------------------------------  
 DEMANDA  : 18730204 - CRIA��O DE CARTA DE ENDOSSOS PARA OS PRODUTOS MASSIFICADOS -- JOSE.VIANA (confitec)  
    
 DATA  : 27/07/2017  
   
 ------TESTE-------------------------------------------------------------------------------------------------  
   
 EXEC SEGUROS_DB.DBO.SEGS12928_SPI  
   
 SELECT * FROM ##SEGS9220  
  
*/      
BEGIN  
 BEGIN TRY   
  SET NOCOUNT ON  
  IF object_id('tempdb..##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB')>0   
  BEGIN    
   DROP TABLE ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB   
  END  
    
  IF object_id('tempdb..##SEGS9220') > 0   
  BEGIN    
   DROP TABLE ##SEGS9220   
  END  
  
  DECLARE  @msg        VARCHAR(1000);  
  -- INICIO 18730204 - Cria��o de Carta de Endossos para os Produtos Massificados**************************************   
  CREATE TABLE  ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB  
  (  
   TPEMISSAO                      CHAR(1)   NULL  
   ,APOLICE_ID      NUMERIC(9, 0) NULL  
   ,PROPOSTA_ID     NUMERIC(9, 0) NULL  
   ,DT_INICIO_VIGENCIA    SMALLDATETIME NULL  
   ,DT_FIM_VIGENCIA    SMALLDATETIME NULL  
   ,ENDOSSO_ID      INT    NULL  
   ,DT_PEDIDO_ENDOSSO    SMALLDATETIME NULL  
   ,SEGURADORA_COD_SUSEP   NUMERIC(5, 0) NULL  
   ,SUCURSAL_SEGURADORA_ID   NUMERIC(5, 0) NULL  
   ,RAMO_ID      INT    NULL  
   ,APOLICE_ENVIA_CLIENTE   CHAR(1)   NULL  
   ,APOLICE_NUM_VIAS    TINYINT   NULL  
   ,APOLICE_ENVIA_CONGENERE  CHAR(1)   NULL  
   ,DT_EMISSAO      SMALLDATETIME NULL  
   ,PRODUTO_ID      INT    NULL  
   ,TP_ENDOSSO_ID     INT    NULL  
   ,NUM_PROC_SUSEP     VARCHAR(25)  NULL  
   ,IMPRESSAO_LIBERADA    CHAR(1)   NULL  
   ,DESTINO      CHAR(1)   NULL  
   ,NUM_SOLICITACAO    INT    NULL  
   ,DIRETORIA_ID     NUMERIC(9, 0) NULL  
   ,CEP       VARCHAR(10)  NULL  
  
  )  
  CREATE NONCLUSTERED INDEX in_retorno_mq_001 ON ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB (APOLICE_ID, PROPOSTA_ID)  
    
  DELETE TEMP FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB TEMP  -- "SQL Code Guard"  
  
  INSERT INTO ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB  
  (  
   TPEMISSAO       
   ,APOLICE_ID       
   ,PROPOSTA_ID      
   ,DT_INICIO_VIGENCIA     
   ,DT_FIM_VIGENCIA     
   ,ENDOSSO_ID       
   ,DT_PEDIDO_ENDOSSO     
   ,SEGURADORA_COD_SUSEP    
   ,SUCURSAL_SEGURADORA_ID    
   ,RAMO_ID       
   ,APOLICE_ENVIA_CLIENTE    
   ,APOLICE_NUM_VIAS     
   ,APOLICE_ENVIA_CONGENERE   
   ,DT_EMISSAO       
   ,PRODUTO_ID       
   ,TP_ENDOSSO_ID      
   ,NUM_PROC_SUSEP      
   ,IMPRESSAO_LIBERADA     
   ,DESTINO       
   ,NUM_SOLICITACAO     
   ,DIRETORIA_ID      
   ,CEP        
  )     
   --'Endosso 1a VIA                  
  SELECT   
   'E' tpemissao   
   ,apolice.apolice_id  
   ,endosso.proposta_id  
   ,apolice.dt_inicio_vigencia        
   ,apolice.dt_fim_vigencia  
   ,endosso.endosso_id     
   ,endosso.dt_pedido_endosso       
   ,apolice.seguradora_cod_susep       
   ,apolice.sucursal_seguradora_id       
   ,apolice.ramo_id  
   ,produto.apolice_envia_cliente  
   ,produto.apolice_num_vias  
   ,produto.apolice_envia_congenere  
   ,endosso.dt_emissao  
   ,produto.produto_id  
   ,endosso.tp_endosso_id  
   ,apolice.num_proc_susep  
   ,'' impressao_liberada  
   ,NULL 'destino'  
   ,NULL 'num_solicitacao'  
   ,NULL 'diretoria_id'  
   ,endCor.cep   
  -- select  *    
  FROM seguros_db.dbo.endosso_tb     endosso  WITH(NOLOCK)  
  left JOIN seguros_db.dbo.endereco_corresp_tb endCor  WITH(NOLOCK)   
  ON endCor.proposta_id = endosso.proposta_id  
    
  -- Laurent Silva - Alterado a pedido do cliente Ronaldo Casseb - 24/05/2019  
  --AND endosso.dt_emissao > '20190211'  
  AND endosso.dt_emissao > '20190524'  
  -- fim   
  
  AND endosso.dt_impressao IS NULL  
  and endosso.endosso_id > 0  
  and endosso.tp_endosso_id NOT IN (63,90,91,100,101)    
  JOIN seguros_db.dbo.proposta_tb     proposta WITH(NOLOCK)  
  ON proposta.proposta_id   = endCor.proposta_id  
  AND proposta.produto_id IN (109,111,811,1123,1188,1207,1220,1221,1222,1223,1224,1228,1229,1241)  
  JOIN seguros_db.dbo.apolice_tb     apolice  WITH(NOLOCK)   
  ON apolice.proposta_id   = proposta.proposta_id  
  and apolice.ramo_id    = proposta.ramo_id  
  JOIN seguros_db.dbo.produto_tb     produto  WITH(NOLOCK)   
  ON produto.produto_id   = proposta.produto_id  
  where proposta.situacao  NOT IN ('c', 't')  
  
 UNION ALL  
   --'Endosso 2a VIA  
  SELECT   
    
   'E' tpemissao  
    ,a.apolice_id  
    ,a.proposta_id  
    ,a.dt_inicio_vigencia  
    ,a.dt_fim_vigencia  
    ,e.endosso_id  
    ,E.dt_pedido_endosso  
    ,a.seguradora_cod_susep                   
    ,a.sucursal_seguradora_id                   
    ,a.ramo_id                   
    ,pd.apolice_envia_cliente                   
    ,pd.apolice_num_vias                   
    ,pd.apolice_envia_congenere                   
    ,e.dt_emissao                   
    ,pp.produto_id                   
    ,e.tp_endosso_id                  
    ,a.num_proc_susep                 
    ,'' impressao_liberada            
    ,ssv.Destino                   
    ,ssv.num_solicitacao                   
    ,ssv.diretoria_id                   
    ,ec.cep  
    -- SELECT *    
     FROM evento_seguros_db.dbo.evento_impressao_tb   ssv WITH(NOLOCK)  
      JOIN seguros_db.dbo.apolice_tb       a WITH(NOLOCK)   
    ON ssv.proposta_id = a.proposta_id  
     LEFT JOIN seguros_db.dbo.endosso_tb      e WITH(NOLOCK)   
    ON ssv.proposta_id = e.proposta_id  
    AND e.tp_endosso_id NOT IN (63,90,91,100,101)  
     JOIN seguros_db.dbo.ramo_tb        r WITH(NOLOCK)   
    ON a.ramo_id = r.ramo_id  
     JOIN seguros_db.dbo.proposta_tb       pp WITH(NOLOCK)   
    ON ssv.proposta_id = pp.proposta_id  
     JOIN seguros_db.dbo.produto_tb       pd WITH(NOLOCK)   
    ON pp.produto_id = pd.produto_id  
    JOIN seguros_db.dbo.endereco_corresp_tb     ec WITH(NOLOCK)   
    ON ec.proposta_id = pp.proposta_id  
    LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_Tb   ret WITH(NOLOCK)   
    ON pp.proposta_id = ret.proposta_id  
    and ssv.endosso_id = ret.endosso_id   
     WHERE ssv.status = 'l'  
       AND ssv.dt_geracao_arquivo IS NULL           
  
    -- Laurent Silva - Alterado a pedido do cliente Ronaldo Casseb - 24/05/2019  
    --AND e.dt_emissao > '20190211'  
    AND e.dt_emissao > '20190524'  
    -- fim   
  
    AND pp.produto_id IN(109,111,811,1123,1188,1207,1220,1221,1222,1223,1224,1228,1229,1241)  
    AND ssv.tp_documento_id IN (6)  
    AND (pp.origem_proposta_id = 1   
       or pp.origem_proposta_id is null) -- restringe-se a capturar apenas as propostas que tem origem no SEGBR  
    AND pp.situacao <> 't'  
    AND e.endosso_id = ssv.endosso_id  
    AND ((pp.produto_id >= 1000)   
      OR (pp.produto_id < 1000   
         AND (ret.aceite_bb = 'S'   
           or ret.aceite_bb is null)))  
  UNION ALL  
   --Produtos personalizados do ALS            
   --'Endosso 2a VIA           
  SELECT   
   'E' tpemissao  
    ,a.apolice_id  
    ,a.proposta_id  
    ,a.dt_inicio_vigencia  
    ,a.dt_fim_vigencia  
    ,e.endosso_id  
    ,e.dt_pedido_endosso  
    ,a.seguradora_cod_susep  
    ,a.sucursal_seguradora_id  
    ,a.ramo_id  
    ,pd.apolice_envia_cliente  
    ,pd.apolice_num_vias  
    ,pd.apolice_envia_congenere  
    ,e.dt_emissao  
    ,pp.produto_id  
    ,e.tp_endosso_id  
    ,a.num_proc_susep  
    ,'' impressao_liberada    
    ,ssv.Destino  
    ,ssv.num_solicitacao  
    ,ssv.diretoria_id  
    ,ec.cep  
    -- select  *    
    FROM evento_seguros_db.dbo.evento_impressao_tb ssv WITH(NOLOCK)  
    JOIN seguros_db.dbo.apolice_tb     a   WITH(NOLOCK)   
    ON ssv.proposta_id = a.proposta_id  
   LEFT JOIN seguros_db.dbo.endosso_tb    e WITH(NOLOCK)   
    ON ssv.proposta_id = e.proposta_id  
   AND e.tp_endosso_id NOT IN (63,90,91,100,101)  
   JOIN seguros_db.dbo.ramo_tb      r WITH(NOLOCK)   
    ON a.ramo_id = r.ramo_id  
   JOIN seguros_db.dbo.proposta_tb     pp WITH(NOLOCK)   
    ON ssv.proposta_id = pp.proposta_id  
   JOIN seguros_db.dbo.produto_tb     pd WITH(NOLOCK)   
    ON pp.produto_id = pd.produto_id  
   JOIN seguros_db.dbo.endereco_corresp_tb   ec WITH(NOLOCK)   
    ON ec.proposta_id = pp.proposta_id                
    LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_Tb ret WITH(NOLOCK)   
    ON pp.proposta_id = ret.proposta_id  
    WHERE ssv.status = 'l'  
    AND ssv.dt_geracao_arquivo IS NULL   
    AND pp.produto_id IN(109,111,811,1123,1188,1207,1220,1221,1222,1223,1224,1228,1229,1241)   
      
    -- Laurent Silva - Alterado a pedido do cliente Ronaldo Casseb - 24/05/2019  
    --AND e.dt_emissao > '20190211'  
    AND e.dt_emissao > '20190524'  
    -- fim      
      
    AND ssv.tp_documento_id IN (6)  
    AND pp.origem_proposta_id = 2  
    AND pp.situacao <> 't'  
    AND ((pp.produto_id >= 1000)   
      OR (pp.produto_id < 1000   
         AND (ret.aceite_bb = 'S'   
            or ret.aceite_bb is null)))  
    AND e.endosso_id = ssv.endosso_id  
       
   DELETE ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB  
   -- select *   
   FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB with(nolock)  
    JOIN seguros_db.dbo.agendamento_cobranca_atual_tb ac with(nolock)    
    ON  ac.proposta_id  = ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB.proposta_id  
    AND ac.num_endosso  = ISNULL(##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB.endosso_id, 0)    
    JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb with(nolock)  
    ON proposta_fechada_tb.proposta_id   = ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB.proposta_id  
   WHERE 1 = 1  
    AND proposta_fechada_tb.forma_pgto_id = 3  
    AND ISNULL(ac.fl_boleto_registrado,'N') <> 'S'    
  
   IF OBJECT_ID('tempdb..##TEMP_AUX_PROCESSO_RETORNO_MQ_TB')>0    
   BEGIN  
    DROP TABLE ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB  
   END  
     
   CREATE TABLE ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB  
   (  
    PROCESSO_ID      INT IDENTITY(1,1)  
    ,TPEMISSAO      CHAR(1)   NULL   
    ,APOLICE_ID      NUMERIC(9, 0) NULL  
    ,PROPOSTA_ID     NUMERIC(9, 0) NULL  
    ,DT_INICIO_VIGENCIA    SMALLDATETIME NULL  
    ,DT_FIM_VIGENCIA    SMALLDATETIME NULL  
    ,ENDOSSO_ID      INT    NULL  
    ,DT_PEDIDO_ENDOSSO    SMALLDATETIME NULL  
    ,SEGURADORA_COD_SUSEP   NUMERIC(5, 0) NULL  
    ,SUCURSAL_SEGURADORA_ID   NUMERIC(5, 0) NULL  
    ,RAMO_ID      INT    NULL  
    ,APOLICE_ENVIA_CLIENTE   CHAR(1)   NULL  
    ,APOLICE_NUM_VIAS    TINYINT   NULL  
    ,APOLICE_ENVIA_CONGENERE  CHAR(1)   NULL  
    ,DT_EMISSAO      SMALLDATETIME NULL  
    ,PRODUTO_ID      INT    NULL  
    ,TP_ENDOSSO_ID     INT    NULL  
    ,NUM_PROC_SUSEP     VARCHAR(25)  NULL  
    ,IMPRESSAO_LIBERADA    CHAR(1)   NULL  
    ,DESTINO      CHAR(1)   NULL  
    ,NUM_SOLICITACAO    INT    NULL  
    ,DIRETORIA_ID     NUMERIC(9, 0) NULL  
    ,CEP       VARCHAR(10)  NULL
    ,PRODUTO_ID		INT		NULL
    ,RAMO_ID		INT		NULL
    ,CPF_CNPJ		VARCHAR(14)		NULL  
   )  
   CREATE NONCLUSTERED INDEX in_taprmq_001 on ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB(APOLICE_ID,PROPOSTA_ID)  
  
   --DELETE #RMQ FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB  #RMQ  --  "SQL Code Guard"     
   INSERT INTO ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB   
   (  
    TPEMISSAO  
    ,APOLICE_ID  
    ,PROPOSTA_ID  
    ,DT_INICIO_VIGENCIA  
    ,DT_FIM_VIGENCIA  
    ,ENDOSSO_ID  
    ,DT_PEDIDO_ENDOSSO  
    ,SEGURADORA_COD_SUSEP  
    ,SUCURSAL_SEGURADORA_ID  
    ,RAMO_ID  
    ,APOLICE_ENVIA_CLIENTE  
    ,APOLICE_NUM_VIAS  
    ,APOLICE_ENVIA_CONGENERE  
    ,DT_EMISSAO  
    ,PRODUTO_ID  
    ,TP_ENDOSSO_ID  
    ,NUM_PROC_SUSEP  
    ,IMPRESSAO_LIBERADA  
    ,DESTINO  
    ,NUM_SOLICITACAO  
    ,DIRETORIA_ID  
    ,CEP  
   )  
   SELECT    
    a.TPEMISSAO  
    ,a.APOLICE_ID  
    ,a.PROPOSTA_ID  
    ,a.DT_INICIO_VIGENCIA  
    ,a.DT_FIM_VIGENCIA  
    ,a.ENDOSSO_ID  
    ,a.DT_PEDIDO_ENDOSSO  
    ,a.SEGURADORA_COD_SUSEP  
    ,a.SUCURSAL_SEGURADORA_ID  
    ,a.RAMO_ID  
    ,a.APOLICE_ENVIA_CLIENTE  
    ,a.APOLICE_NUM_VIAS  
    ,a.APOLICE_ENVIA_CONGENERE  
    ,a.DT_EMISSAO  
    ,a.PRODUTO_ID  
    ,a.TP_ENDOSSO_ID  
    ,a.NUM_PROC_SUSEP  
    ,a.IMPRESSAO_LIBERADA  
    ,a.DESTINO  
    ,a.NUM_SOLICITACAO  
    ,a.DIRETORIA_ID  
    ,a.CEP  
     FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_9220_TB A  
      
   EXECUTE SEGUROS_DB.DBO.SEGS10192_SPI  
   
   
   
   -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	set ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.produto_id = proposta_tb.produto_id, ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.ramo_id =  proposta_tb.ramo_id,
		##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
  
   CREATE TABLE ##SEGS9220  
   (  
    TPEMISSAO      CHAR(1)   NULL  
    ,APOLICE_ID      NUMERIC(9, 0) NULL  
    ,PROPOSTA_ID     NUMERIC(9, 0) NULL  
    ,DT_INICIO_VIGENCIA    SMALLDATETIME NULL  
    ,DT_FIM_VIGENCIA    SMALLDATETIME NULL  
    ,ENDOSSO_ID      INT    NULL  
    ,DT_PEDIDO_ENDOSSO    SMALLDATETIME NULL  
    ,SEGURADORA_COD_SUSEP   NUMERIC(5, 0) NULL  
    ,SUCURSAL_SEGURADORA_ID   NUMERIC(5, 0) NULL  
    ,RAMO_ID      INT    NULL  
    ,APOLICE_ENVIA_CLIENTE   CHAR(1)   NULL  
    ,APOLICE_NUM_VIAS    TINYINT   NULL  
    ,APOLICE_ENVIA_CONGENERE  CHAR(1)   NULL  
    ,DT_EMISSAO      SMALLDATETIME NULL  
    ,PRODUTO_ID      INT    NULL  
    ,TP_ENDOSSO_ID     INT    NULL  
    ,NUM_PROC_SUSEP     VARCHAR(25)  NULL  
    ,IMPRESSAO_LIBERADA    CHAR(1)   NULL  
    ,DESTINO      CHAR(1)   NULL  
    ,NUM_SOLICITACAO    INT    NULL  
    ,DIRETORIA_ID     NUMERIC(9, 0) NULL  
    ,CEP       VARCHAR(10)  NULL  
    ,FLAG_EMITE      CHAR(1)   NULL  
    ,DESCRICAO      VARCHAR(60)  NULL  
    ,PRODUTO_ID		INT		NULL
    ,RAMO_ID		INT		NULL
    ,CPF_CNPJ		VARCHAR(14)		NULL
   )  
   CREATE NONCLUSTERED INDEX in_sega9220_001 on ##SEGS9220 (APOLICE_ID,PROPOSTA_ID)  
   DELETE #9220 FROM ##SEGS9220 #9220   --  "SQL Code Guard"    
  
   INSERT INTO ##SEGS9220  
   (  
    TPEMISSAO  
    ,APOLICE_ID  
    ,PROPOSTA_ID  
    ,DT_INICIO_VIGENCIA  
    ,DT_FIM_VIGENCIA  
    ,ENDOSSO_ID  
    ,DT_PEDIDO_ENDOSSO  
    ,SEGURADORA_COD_SUSEP  
    ,SUCURSAL_SEGURADORA_ID  
    ,RAMO_ID  
    ,APOLICE_ENVIA_CLIENTE  
    ,APOLICE_NUM_VIAS  
    ,APOLICE_ENVIA_CONGENERE  
    ,DT_EMISSAO  
    ,PRODUTO_ID  
    ,TP_ENDOSSO_ID  
    ,NUM_PROC_SUSEP  
    ,IMPRESSAO_LIBERADA  
    ,DESTINO  
    ,NUM_SOLICITACAO  
    ,DIRETORIA_ID  
    ,CEP  
    ,FLAG_EMITE  
    ,DESCRICAO  
    ,PRODUTO_ID
    ,RAMO_ID
    ,CPF_CNPJ
   )  
   SELECT distinct  
     A.TPEMISSAO  
    ,A.APOLICE_ID  
    ,A.PROPOSTA_ID  
    ,A.DT_INICIO_VIGENCIA  
    ,A.DT_FIM_VIGENCIA  
    ,A.ENDOSSO_ID  
    ,A.DT_PEDIDO_ENDOSSO  
    ,A.SEGURADORA_COD_SUSEP  
    ,A.SUCURSAL_SEGURADORA_ID  
    ,A.RAMO_ID  
    ,A.APOLICE_ENVIA_CLIENTE  
    ,A.APOLICE_NUM_VIAS  
    ,A.APOLICE_ENVIA_CONGENERE  
    ,A.DT_EMISSAO  
    ,A.PRODUTO_ID  
    ,A.TP_ENDOSSO_ID  
    ,A.NUM_PROC_SUSEP  
    ,A.IMPRESSAO_LIBERADA  
    ,A.DESTINO  
    ,A.NUM_SOLICITACAO  
    ,A.DIRETORIA_ID  
    ,A.CEP  
    ,'S' -- FLAG_EMITE  
    ,tpe.descricao 
    ,A.produto_id
    ,A.ramo_id
    ,A.cpf_cnpj
   from ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB A    
   JOIN seguros_db.dbo.tp_endosso_tb  tpe WITH(NOLOCK)   
   on tpe.tp_endosso_id = A.tp_endosso_id  
   
 END TRY  
  BEGIN CATCH          
   SET @Msg = ISNULL(ERROR_PROCEDURE(),'')    
     + ' - Linha '      + CONVERT(VARCHAR(15),ISNULL(ERROR_LINE(),0))    
     + ' - '            + ISNULL(ERROR_MESSAGE(),'')    
     + ' - Atividade: ' + @Msg;  
   RAISERROR (@Msg, 16, 1) ;   
  END CATCH  
END   
  
  