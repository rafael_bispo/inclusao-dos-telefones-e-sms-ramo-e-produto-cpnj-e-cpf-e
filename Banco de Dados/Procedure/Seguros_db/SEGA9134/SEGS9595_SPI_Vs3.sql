ALTER PROCEDURE SEGS9595_SPI (
    @USUARIO   VARCHAR(20),      
    @NUM_VERSAO   INT      
 )  
AS  
  
BEGIN TRY

	SET NOCOUNT ON      
	    
	INSERT INTO INTERFACE_DADOS_DB..SEGA9134_PROCESSADO_TB (  
	   SEGA9134_PROCESSADO_ID,  
	   NUM_VERSAO,  
	   CLIENTE_ID,  
	   PROPOSTA_ID,  
	   ENDOSSO_ID,  
	   LAYOUT_ID,  
	   PRODUTO_ID,  
	   DT_INCLUSAO,  
	   DT_ALTERACAO,  
	   USUARIO,  
	   NUM_SOLICITACAO )  
	SELECT   
	 SEGA9134_PROCESSAR_ID,  
	 @NUM_VERSAO,  
	 CLIENTE_ID,  
	 PROPOSTA_ID,  
	 ENDOSSO_ID,  
	 LAYOUT_ID,  
	 PRODUTO_ID,  
	 GETDATE(),  
	 NULL,  
	 @USUARIO,  
	 NUM_SOLICITACAO  
	FROM INTERFACE_DADOS_DB..SEGA9134_PROCESSAR_TB  
	  
	INSERT INTO INTERFACE_DADOS_DB..SEGA9134_10_PROCESSADO_TB (  
	 SEGA9134_10_PROCESSADO_ID,  
	 SEGA9134_PROCESSADO_ID,  
	 PROPOSTA_ID,  
	 COD_BARRAS,  
	 NOME_DESTINATARIO,  
	 ENDERECO_CORRESP,  
	 BAIRRO,  
	 MUNICIPIO,  
	 ESTADO,  
	 CEP,  
	 DT_INCLUSAO,  
	 DT_ALTERACAO,  
	 USUARIO  
	)  
	SELECT  
	 SEGA9134_10_processar_id,  
	 SEGA9134_PROCESSAR_ID,  
	 PROPOSTA_ID,  
	 COD_BARRAS,  
	 NOME_DESTINATARIO,  
	 ENDERECO_CORRESP,  
	 BAIRRO,  
	 MUNICIPIO,  
	 ESTADO,  
	 CEP,  
	 GETDATE(),  
	 NULL,  
	 @USUARIO  
	FROM INTERFACE_DADOS_DB..SEGA9134_10_PROCESSAR_TB   
	  
	INSERT INTO INTERFACE_DADOS_DB..SEGA9134_20_PROCESSADO_TB (  
	 SEGA9134_20_PROCESSADO_ID,  
	 SEGA9134_PROCESSADO_ID,  
	 PROPOSTA_ID,  
	 NOME_SEGURADO,  
	 APOLICE_ID,  
	 PROPOSTA_BB,  
	 CERTIFICADO_ID,  
	 NOME_PRODUTO,  
	 DT_INI_VIGENCIA,  
	 DT_PED_ENDOSSO, 
	 NUM_OPERACAO_CREDITO, 
	 DT_INCLUSAO,  
	 DT_ALTERACAO,  
	 USUARIO,
	 num_processo_susep,   --JOAO.MACHADO
	 ddd_celular, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 celular, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 email, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 cpf_cnpj) -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	SELECT  
	 SEGA9134_20_PROCESSAR_ID,  
	 SEGA9134_PROCESSAR_ID,  
	 PROPOSTA_ID,  
	 NOME_SEGURADO,  
	 APOLICE_ID,  
	 PROPOSTA_BB,  
	 CERTIFICADO_ID,  
	 NOME_PRODUTO,  
	 DT_INI_VIGENCIA,  
	 DT_PED_ENDOSSO, 
	 NUM_OPERACAO_CREDITO, 
	 GETDATE(),  
	 NULL,  
	 @USUARIO,
	 num_processo_susep, --JOAO.MACHADO
	 ddd_celular, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 celular, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 email, -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	 cpf_cnpj -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	FROM INTERFACE_DADOS_DB..SEGA9134_20_PROCESSAR_TB  
	       
	-- Excluindo os registros das tabelas processar      
	DELETE FROM INTERFACE_DADOS_DB..SEGA9134_20_PROCESSAR_TB      
	DELETE FROM INTERFACE_DADOS_DB..SEGA9134_10_PROCESSAR_TB      
	DELETE FROM INTERFACE_DADOS_DB..SEGA9134_PROCESSAR_TB           
	      
	SET NOCOUNT OFF      
	      
	RETURN      
END TRY
      
BEGIN CATCH   

	DECLARE @ErrorMessage NVARCHAR(4000)                  
	DECLARE @ErrorSeverity INT                  
	DECLARE @ErrorState INT  
              
	SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                  
		   @ErrorSeverity = ERROR_SEVERITY(),                  
		   @ErrorState = ERROR_STATE()                  
	   	      
	RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)                                 

END CATCH

GO

