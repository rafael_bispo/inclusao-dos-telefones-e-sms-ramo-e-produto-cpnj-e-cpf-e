CREATE PROCEDURE SEGS9591_SPI (
 @usuario VARCHAR(20)       
)       
AS       
  
BEGIN TRY   
      
 SET NOCOUNT ON       
              
 INSERT INTO interface_dados_db..SEGA9134_20_processar_tb (       
     SEGA9134_processar_id,       
     proposta_id,       
     nome_segurado,       
     apolice_id,       
     proposta_bb,       
     certificado_id,       
     nome_produto,       
     dt_ini_vigencia,       
     dt_ped_endosso,    
     num_operacao_credito,  
     dt_inclusao,       
     dt_alteracao,       
     usuario,  
     num_processo_susep, -- JOAO.MACHADO (INCLUINDO SUBRAMO)       
     ddd_celular,
     celular,
     email)
 SELECT SEGA9134_processar_tb.SEGA9134_processar_id,       
     SEGA9134_processar_tb.proposta_id,       
     cliente_tb.nome,       
     proposta_adesao_tb.apolice_id,       
     proposta_adesao_tb.proposta_bb,       
     certificado_tb.certificado_id,       
     produto_tb.nome,       
     proposta_adesao_tb.dt_inicio_vigencia,       
     cancelamento_proposta_tb.dt_cancelamento_bb,    
     CASE LEN(questionario_proposta_tb.texto_resposta) WHEN 9  
    THEN CONVERT(NUMERIC(9,0),questionario_proposta_tb.texto_resposta)  
    ELSE CONVERT(NUMERIC(17,0),questionario_proposta_tb.texto_resposta)    --Zoro.Gomes - Confitec - Tratamento para o produto 1225 - 22/12/2014  
     END,  
     getDate(),       
     NULL,       
     @usuario,  
     subramo_tb.num_proc_susep, -- JOAO.MACHADO (INCLUINDO SUBRAMO)  
     isnull(cliente_tb.ddd_1,'') as ddd_celular,
     isnull(cliente_tb.telefone_1,'') as Celular,
     isnull(cliente_tb.e_mail,'') as email
   FROM interface_dados_db..SEGA9134_processar_tb SEGA9134_processar_tb WITH (NOLOCK)       
   JOIN proposta_adesao_tb WITH (NOLOCK)       
  ON proposta_adesao_tb.proposta_id = SEGA9134_processar_tb.proposta_id       
   JOIN endosso_tb WITH (NOLOCK)       
  ON endosso_tb.proposta_id = SEGA9134_processar_tb.proposta_id       
    AND endosso_tb.endosso_id = SEGA9134_processar_tb.endosso_id      
   JOIN cliente_tb WITH (NOLOCK)       
  ON SEGA9134_processar_tb.cliente_id = cliente_tb.cliente_id       
   JOIN cancelamento_proposta_tb WITH (NOLOCK)       
  ON SEGA9134_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id       
    AND cancelamento_proposta_tb.endosso_id = endosso_tb.endosso_id       
    AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL     
   JOIN questionario_proposta_tb WITH (NOLOCK)        
  ON questionario_proposta_tb.proposta_id = proposta_adesao_tb.proposta_id        
    AND questionario_proposta_tb.pergunta_id = 7677         
   JOIN certificado_tb WITH (NOLOCK)       
  ON certificado_tb.proposta_id = SEGA9134_processar_tb.proposta_id       
    AND certificado_id = (SELECT MAX(certificado_id)       
          FROM certificado_tb      (nolock) 
         WHERE proposta_id = SEGA9134_processar_tb.proposta_id)      
   JOIN produto_tb WITH (NOLOCK)       
  ON SEGA9134_processar_tb.produto_id = produto_tb.produto_id      
   JOIN SEGUROS_DB..PROPOSTA_TB PROPOSTA_TB WITH(NOLOCK) -- JOAO.MACHADO (INCLUINDO SUBRAMO)  
  ON PROPOSTA_TB.PROPOSTA_ID = SEGA9134_processar_tb.PROPOSTA_ID  
   JOIN SEGUROS_DB..SUBRAMO_TB SUBRAMO_TB WITH(NOLOCK)   
  ON SUBRAMO_TB.SUBRAMO_ID = PROPOSTA_TB.SUBRAMO_ID  
  ORDER BY SEGA9134_processar_tb.SEGA9134_processar_id     
  

---- Vinicius.Dario Confitec - Novo Prestamista - Concatenando nome do produto com plano 22/04/2020 inicio  
UPDATE interface_dados_db..SEGA9134_20_processar_tb 
SET nome_produto = ISNULL(plano_tb.nome_fantasia,'')
FROM interface_dados_db..SEGA9134_20_processar_tb SEGA9134_20_processar_tb   with(nolock)
INNER JOIN seguros_db.dbo.proposta_tb proposta_tb with(nolock)  
on proposta_tb.proposta_id = SEGA9134_20_processar_tb.proposta_id  
INNER JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb with(nolock)  
on SEGA9134_20_processar_tb.proposta_id = escolha_plano_tb.proposta_id 
inner join seguros_db.dbo.plano_tb plano_tb with(nolock)
on  plano_tb.produto_id = escolha_plano_tb.produto_id
and plano_tb.plano_id = escolha_plano_tb.plano_id 
where proposta_tb.produto_id = 1243  
---- Vinicius.Dario Confitec - Novo Prestamista - Concatenando nome do produto com plano 22/04/2020 fim  
  
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update SEGA9134_20_processar_tb
	set SEGA9134_20_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db.dbo.SEGA9134_20_processar_tb SEGA9134_20_processar_tb with(nolock)
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = SEGA9134_20_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id  
		
	-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
 update SEGA9134_20_processar_tb	
    set SEGA9134_20_processar_tb.agencia = agencia_tb.agencia_id
   from interface_dados_db.dbo.SEGA9134_20_processar_tb SEGA9134_20_processar_tb with(nolock)
  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with(NOLOCK)          
	 ON proposta_adesao_tb.proposta_id = SEGA9134_20_processar_tb.proposta_id          
  INNER JOIN seguros_db.dbo.agencia_tb (NOLOCK)          
	 ON proposta_adesao_tb.cont_agencia_id = agencia_tb.agencia_id           
    AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id     
    
   -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
  update SEGA9134_20_processar_tb	
	 set SEGA9134_20_processar_tb.agencia = agencia_tb.agencia_id
    from interface_dados_db.dbo.SEGA9134_20_processar_tb SEGA9134_20_processar_tb with(nolock)
   INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb with(NOLOCK)          
	  ON proposta_fechada_tb.proposta_id = SEGA9134_20_processar_tb.proposta_id          
   INNER JOIN seguros_db.dbo.agencia_tb (NOLOCK)          
	  ON proposta_fechada_tb.cont_agencia_id = agencia_tb.agencia_id           
     AND agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id   
    
    
        
 SET NOCOUNT OFF       
        
 RETURN       
  
END TRY    
BEGIN CATCH     
  
 DECLARE @ErrorMessage NVARCHAR(4000)                    
 DECLARE @ErrorSeverity INT                    
 DECLARE @ErrorState INT    
                
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                    
     @ErrorSeverity = ERROR_SEVERITY(),                    
     @ErrorState = ERROR_STATE()                    
             
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState)                                   
  
END CATCH  
  
  



