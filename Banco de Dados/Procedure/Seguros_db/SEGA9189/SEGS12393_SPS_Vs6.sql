CREATE PROCEDURE SEGS12393_SPS
    
AS                      
    
/*      
    
EXEC DESENV_DB..SEGA9189_SPS      
    
DROP TABLE #PROPOSTAS    
DROP TABLE #ENDOSSO    
    
*/      
    
SET NOCOUNT ON                  
DECLARE @tp_documento_id INT      
    
SELECT @tp_documento_id = ISNULL(tp_documento_id, 0)       
  FROM evento_seguros_db..documento_tb WITH (NOLOCK)      
 WHERE arquivo_saida = 'SEGA9189'      
    
SELECT TOP 28    
       PROPOSTA_TB.proposta_id  
     , PROP_cliente_id  
     , PROPOSTA_TB.produto_id  
     , SUBRAMO_TB.num_proc_susep                      
     , endosso_id = MAX(ENDOSSO_TB.endosso_id)
     , PROPOSTA_TB.ramo_id
  INTO #PROPOSTAS     
  FROM SEGUROS_DB..PROPOSTA_TB PROPOSTA_TB WITH (NOLOCK)       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.proposta_id = ENDOSSO_TB.proposta_id      
  JOIN SEGUROS_DB..SUBRAMO_TB SUBRAMO_TB WITH (NOLOCK)      
    ON PROPOSTA_TB.subramo_id = SUBRAMO_TB.subramo_id      
 WHERE 1 = 1    
   AND PROPOSTA_TB.produto_id in (1225,1231,1198,1205,1243) --R.FOUREAUX - INCLUIDO PRODUTOS 1231,1198 E 1205 - 15/08/2017 // Louise Freitas - Confitec - Adicionando produto 1243
   AND ENDOSSO_TB.tp_endosso_id = 340      
   AND SUBRAMO_TB.dt_fim_vigencia_sbr IS NULL     
  
   AND NOT EXISTS (SELECT 1     --Zoro.Gomes - Confitec - acerto no filtro de evento-impressao - 22/02/2016  
                     FROM EVENTO_SEGUROS_DB..EVENTO_IMPRESSAO_TB WITH (NOLOCK)      
                    WHERE proposta_id = PROPOSTA_TB.proposta_id      
                      AND endosso_id = (SELECT MAX(endosso_id)       
                                          FROM seguros_db..endosso_tb WITH (NOLOCK)      
                                         WHERE proposta_id = PROPOSTA_TB.proposta_id  
                                           AND tp_endosso_id = 340 )       
                      AND tp_documento_id = @tp_documento_id)   
 GROUP BY PROPOSTA_TB.proposta_id, PROP_cliente_id, PROPOSTA_TB.produto_id, SUBRAMO_TB.num_proc_susep,PROPOSTA_TB.ramo_id      
    
--SELECT * FROM #PROPOSTAS      
    
SELECT Via = '1a Via',      
       num_solicitacao = 0,      
       #PROPOSTAS.proposta_id,                       
       #PROPOSTAS.endosso_id,    
       #PROPOSTAS.produto_id,      
       #PROPOSTAS.num_proc_susep,       
       destinatario = CLIENTE_TB.nome,      
       endereco = ENDERECO_CORRESP_TB.endereco,      
       bairro = ENDERECO_CORRESP_TB.bairro,      
       Cep_solicitante  = ENDERECO_CORRESP_TB.cep,      
       cidade_solicitante = ENDERECO_CORRESP_TB.municipio,      
       uf_solicitante  = ENDERECO_CORRESP_TB.estado,      
       produto = PRODUTO_TB.nome,       
       PROPOSTA_ADESAO_TB.apolice_id,      
       PROPOSTA_ADESAO_TB.proposta_bb,      
       CERTIFICADO_TB.certificado_id,      
       ENDOSSO_TB.dt_emissao,      
       max_endosso_id = NULL,              --RMarins 04/01/16 
       ISNULL(cliente_tb.ddd_1,'') AS ddd_celular, --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       ISNULL(cliente_tb.telefone_1,'') AS celular, --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       ISNULL(cliente_tb.e_mail,'') AS email,    --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS                   
       #PROPOSTAS.ramo_id,
       CAST('' AS VARCHAR(14)) as cpf_cnpj
  INTO #ENDOSSO      
  FROM #PROPOSTAS WITH (NOLOCK)       
  JOIN SEGUROS_DB..CLIENTE_TB CLIENTE_TB WITH (NOLOCK)       
    ON CLIENTE_TB.cliente_id = #PROPOSTAS.PROP_cliente_id                       
  JOIN SEGUROS_DB..PROPOSTA_ADESAO_TB PROPOSTA_ADESAO_TB WITH (NOLOCK)       
    ON PROPOSTA_ADESAO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..CERTIFICADO_TB CERTIFICADO_TB WITH (NOLOCK)       
    ON CERTIFICADO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..ENDERECO_CORRESP_TB ENDERECO_CORRESP_TB WITH (NOLOCK)       
    ON ENDERECO_CORRESP_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..PRODUTO_TB PRODUTO_TB WITH (NOLOCK)       
    ON PRODUTO_TB.produto_id = #PROPOSTAS.produto_id                       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON ENDOSSO_TB.proposta_id = #PROPOSTAS.proposta_id    
   AND ENDOSSO_TB.endosso_id = #PROPOSTAS.endosso_id        --RMarins 04/01/16    
-- WHERE 1 = 1    
--   AND NOT EXISTS (SELECT 1     --Zoro.Gomes - Confitec - acerto no filtro de evento-impressao - 19/02/2016  
--                   FROM EVENTO_SEGUROS_DB..EVENTO_IMPRESSAO_TB EVENTO_IMPRESSAO_TB WITH (NOLOCK)      
--                  WHERE EVENTO_IMPRESSAO_TB.proposta_id = #PROPOSTAS.proposta_id      
--                    AND EVENTO_IMPRESSAO_TB.endosso_id = #PROPOSTAS.endosso_id      
--                    AND EVENTO_IMPRESSAO_TB.tp_documento_id = @tp_documento_id)          
--    
UNION       
               
SELECT Via  = '2a Via',      
       EVENTO_IMPRESSAO_TB.num_solicitacao,      
       #PROPOSTAS.proposta_id,                       
       #PROPOSTAS.endosso_id,    
       #PROPOSTAS.produto_id,      
       #PROPOSTAS.num_proc_susep,       
       destinatario = CLIENTE_TB.nome,      
       endereco = ENDERECO_CORRESP_TB.endereco,      
       bairro = ENDERECO_CORRESP_TB.bairro,      
       Cep_solicitante = ENDERECO_CORRESP_TB.cep,      
       cidade_solicitante = ENDERECO_CORRESP_TB.municipio,      
       uf_solicitante = ENDERECO_CORRESP_TB.estado,      
       produto = PRODUTO_TB.nome,       
       PROPOSTA_ADESAO_TB.apolice_id,      
       PROPOSTA_ADESAO_TB.proposta_bb,      
       CERTIFICADO_TB.certificado_id,      
       ENDOSSO_TB.dt_emissao,      
       max_endosso_id = NULL,              --RMarins 04/01/16     
       ISNULL(cliente_tb.ddd_1,'') AS ddd_celular, --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       ISNULL(cliente_tb.telefone_1,'') AS celular, --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       ISNULL(cliente_tb.e_mail,'') AS email,    --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS                   
	   #PROPOSTAS.ramo_id,
       CAST('' AS VARCHAR(14)) as cpf_cnpj
  FROM #PROPOSTAS    
  JOIN SEGUROS_DB..CLIENTE_TB CLIENTE_TB WITH (NOLOCK)       
    ON CLIENTE_TB.cliente_id = #PROPOSTAS.PROP_cliente_id                       
  JOIN SEGUROS_DB..PROPOSTA_ADESAO_TB PROPOSTA_ADESAO_TB WITH (NOLOCK)       
    ON PROPOSTA_ADESAO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..CERTIFICADO_TB CERTIFICADO_TB WITH (NOLOCK)       
    ON CERTIFICADO_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..ENDERECO_CORRESP_TB ENDERECO_CORRESP_TB WITH (NOLOCK)       
    ON ENDERECO_CORRESP_TB.proposta_id = #PROPOSTAS.proposta_id                       
  JOIN SEGUROS_DB..PRODUTO_TB PRODUTO_TB WITH (NOLOCK)       
    ON PRODUTO_TB.produto_id = #PROPOSTAS.produto_id                       
  JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)      
    ON ENDOSSO_TB.proposta_id = #PROPOSTAS.proposta_id    
   AND ENDOSSO_TB.endosso_id = #PROPOSTAS.endosso_id        --RMarins 04/01/16    
  JOIN EVENTO_SEGUROS_DB..EVENTO_IMPRESSAO_TB EVENTO_IMPRESSAO_TB WITH (NOLOCK)      
    ON EVENTO_IMPRESSAO_TB.proposta_id  = #PROPOSTAS.proposta_id      
   AND EVENTO_IMPRESSAO_TB.endosso_id  = #PROPOSTAS.endosso_id      
 WHERE 1 = 1    
   AND EVENTO_IMPRESSAO_TB.tp_documento_id = @tp_documento_id      
   AND EVENTO_IMPRESSAO_TB.status  = 'L'      
                  
UPDATE #ENDOSSO      
   SET MAX_ENDOSSO_ID = (SELECT MAX(ENDOSSO_TB.ENDOSSO_ID)     
                           FROM SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)       
                          WHERE 1 = 1    
                            AND ENDOSSO_TB.PROPOSTA_ID = #ENDOSSO.PROPOSTA_ID    
                            AND ENDOSSO_TB.ENDOSSO_ID < #ENDOSSO.endosso_id    
                            AND ENDOSSO_TB.TP_ENDOSSO_ID in (336,359))  --R.FOUREAUX - INCLUSAO DO ENDOSSO 359    
  FROM #ENDOSSO      
    
UPDATE #ENDOSSO      
   SET DT_EMISSAO = (SELECT MAX(ENDOSSO_TB.DT_PEDIDO_ENDOSSO)     
                       FROM SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (NOLOCK)       
                      WHERE 1 = 1    
                        AND ENDOSSO_TB.PROPOSTA_ID = #ENDOSSO.PROPOSTA_ID    
                        AND ENDOSSO_TB.ENDOSSO_ID = #ENDOSSO.MAX_ENDOSSO_ID)    
  FROM #ENDOSSO    
    
    
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 26/03/2020 Inicio
update #ENDOSSO
set #ENDOSSO.produto = ISNULL(plano_tb.nome_fantasia,'')
from #ENDOSSO #ENDOSSO 
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
on proposta_tb.proposta_id = #ENDOSSO.proposta_id
inner join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with(nolock)
on escolha_plano_tb.proposta_id = proposta_tb.proposta_id
and escolha_plano_tb.produto_id = proposta_tb.produto_id
inner join seguros_db.dbo.plano_tb plano_tb with(nolock)
on  plano_tb.produto_id = escolha_plano_tb.produto_id
and plano_tb.plano_id = escolha_plano_tb.plano_id
where proposta_tb.produto_id = 1243
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 26/03/2020 Fim 


-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #ENDOSSO
set #ENDOSSO.CPF_CNPJ = CASE 
							WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
							WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
							ELSE cliente_tb.cpf_cnpj
						END 
from #ENDOSSO #ENDOSSO
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #ENDOSSO.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
	
  
SELECT Via,      
       num_solicitacao,      
       DESTINATARIO,      
       endereco,      
       bairro,      
       Cep_solicitante,      
       cidade_solicitante,      
       uf_solicitante,      
       produto,       
       APOLICE_ID,      
       PROPOSTA_BB,      
       CERTIFICADO_ID,      
       num_proc_susep,      
       dt_emissao,      
       proposta_id,                       
       produto_id,      
       ENDOSSO_ID,
       ddd_celular,--migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       celular,--migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       email,     --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
       ramo_id,
       cpf_cnpj
 FROM  #ENDOSSO       
    
SET NOCOUNT OFF          
  
  
  

