
ALTER PROCEDURE dbo.SEGS11566_SPI
-- PROCEDURE DE INSER��O NA SEGA9200_20_PROCESSAR_TB  
      
    @usuario VARCHAR(20)                                  
      
AS                                  
-----------------------------------------------------------------------------------------------      
-- PARÂMETROS DE ENTRADA PARA TESTE      
/*       
EXEC SEGS10012_SPI 1636,1208,'PRODOCAO01'       
SET @usuario = 'PRODOCAO01'       
*/       
-----------------------------------------------------------------------------------------------       
-----------------------------------------------------------                                  
-- Procedure Detalhe 20 - Segurado SEGA9200             --                                  
-----------------------------------------------------------                                  
      
SET NOCOUNT ON                                  
        
DECLARE @ErrorMessage NVARCHAR(4000)      
DECLARE @ErrorSeverity INT      
DECLARE @ErrorState INT          
      
IF @@ERROR <> 0                                    
BEGIN                                      
    SELECT       
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),      
      @ErrorSeverity = ERROR_SEVERITY(),      
      @ErrorState = ERROR_STATE()      
      
    GOTO error                                      
END       
  
SELECT DISTINCT SEGA9200.SEGA9200_processar_id,  
       SEGA9200.proposta_id,  
       SEGA9200.apolice_id apolice_id,  
       ISNULL(SEGA9200.proposta_bb, 0) AS proposta_bb,  
       c.nome nome_segurado, 
       c.ddd_1 as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
       c.telefone_1 as celular, --migracao-documentacao-digital-2a-fase-cartas
       c.e_mail as email,     --migracao-documentacao-digital-2a-fase-cartas
       p.produto_id, --migracao-documentacao-digital-2a-fase-cartas
       p.ramo_id, --migracao-documentacao-digital-2a-fase-cartas
       prod.nome nome_produto, 
       CAST('' AS VARCHAR(18)) as cpf_cnpj, 
       --rodrigo.moreira - Nova Consultoria - 04/02/2015
       --ce.certificado_id,  
       ISNULL(ce.certificado_id,0) AS certificado_id,
       aca.num_cobranca,  
       aca.dt_agendamento as dt_vencimento,  
       aca.val_cobranca as val_premio,  
       ISNULL(REPLACE(CONVERT(VARCHAR(10),aca_proxima.dt_agendamento,103),'/',''),'') AS dt_vencimento_proxima_parcela,  
       dt_inclusao = GETDATE(),  
       usuario = @usuario  
  INTO #SEGA9200_20  
  FROM interface_dados_db.dbo.SEGA9200_processar_tb SEGA9200 WITH (NOLOCK)  
 INNER JOIN seguros_db.dbo.proposta_tb p WITH(NOLOCK)  
    ON p.proposta_id = SEGA9200.proposta_id  
 INNER JOIN seguros_db.dbo.produto_tb prod WITH(NOLOCK)  
    ON prod.produto_id = p.produto_id  
 INNER JOIN seguros_db.dbo.cliente_tb c WITH(NOLOCK)  
    ON c.cliente_id = p.prop_cliente_id  
 --rodrigo.moreira - Nova Consultoria - 04/02/2015
 --INNER JOIN seguros_db.dbo.certificado_tb ce WITH (NOLOCK)  
   LEFT JOIN seguros_db.dbo.certificado_tb ce WITH (NOLOCK)   
    ON ce.proposta_id = p.proposta_id  
   AND ce.dt_inicio_vigencia = (SELECT TOP 1 ce2.dt_inicio_vigencia  
                                  FROM seguros_db.dbo.certificado_tb ce2 WITH (NOLOCK)  
                                 WHERE ce2.proposta_id = ce.proposta_id  
                                 ORDER BY ce2.dt_inicio_vigencia DESC)  
 INNER JOIN seguros_db.dbo.agendamento_cobranca_atual_tb aca WITH (NOLOCK)  
    ON aca.proposta_id = SEGA9200.proposta_id  
   AND aca.num_cobranca = SEGA9200.num_cobranca  
  LEFT JOIN seguros_db.dbo.agendamento_cobranca_atual_tb aca_proxima WITH (NOLOCK)  
    ON aca_proxima.proposta_id = SEGA9200.proposta_id  
   AND aca_proxima.num_cobranca = SEGA9200.num_cobranca + 1  

   -- SBRJ0015154 - Altera��o do nome do Produto para exibi��o do Nome do Plano para o Produto 1237 - RMarins 08/11/2019
 UPDATE #SEGA9200_20   
    SET nome_produto = plano_tb.nome      
   FROM #SEGA9200_20
   JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK) 
     ON #SEGA9200_20.proposta_id = proposta_tb.proposta_id
   JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH (NOLOCK) 
     ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id
   JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)                                  
     ON escolha_plano_tb.plano_id = plano_tb.plano_id                                                            
    AND escolha_plano_tb.produto_id = plano_tb.produto_id                                                            
    AND escolha_plano_tb.dt_inicio_vigencia = plano_tb.dt_inicio_vigencia 
  WHERE proposta_tb.produto_id = 1237 
    AND plano_tb.plano_id NOT IN (1,2,3)
	AND (escolha_plano_tb.endosso_id IS NULL OR escolha_plano_tb.endosso_id = 0)  
   -- SBRJ0015154 - Fim altera��o - RMarins 08/11/2019     
   
   -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #SEGA9200_20
	set #SEGA9200_20.CPF_CNPJ = CASE 
									WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
									WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
									ELSE cliente_tb.cpf_cnpj
								END 
	from #SEGA9200_20 #SEGA9200_20
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #SEGA9200_20.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
  
IF @@ERROR <> 0  
BEGIN  
    SELECT  
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),  
      @ErrorSeverity = ERROR_SEVERITY(),  
      @ErrorState = ERROR_STATE()  
  
    GOTO error  
END  
  
INSERT INTO interface_dados_db..SEGA9200_20_processar_tb                                  
    (SEGA9200_processar_id,      
    proposta_id,      
    apolice_id,      
    proposta_bb,      
    nome_segurado,      
    nome_produto,      
    certificado_id,    
    num_cobranca,    
    dt_vencimento,    
    val_premio,    
    dt_vencimento_proxima_parcela,    
    dt_inclusao,    
    usuario,
    ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
    celular, --migracao-documentacao-digital-2a-fase-cartas
    email,     --migracao-documentacao-digital-2a-fase-cartas
    produto_id, --migracao-documentacao-digital-2a-fase-cartas
    ramo_id) --migracao-documentacao-digital-2a-fase-cartas          
SELECT SEGA9200_processar_id,      
    proposta_id,      
    apolice_id,      
    proposta_bb,      
    nome_segurado,      
    nome_produto,      
    certificado_id,    
    num_cobranca,    
    dt_vencimento,    
    val_premio,   
    dt_vencimento_proxima_parcela,  
    dt_inclusao,    
    usuario,
    isnull(ddd_celular,''), --migracao-documentacao-digital-2a-fase-cartas
    isnull(celular,''), --migracao-documentacao-digital-2a-fase-cartas
    isnull(email,''),     --migracao-documentacao-digital-2a-fase-cartas   
    produto_id, --migracao-documentacao-digital-2a-fase-cartas
    ramo_id --migracao-documentacao-digital-2a-fase-cartas           
 FROM #SEGA9200_20                           
  
IF @@ERROR <> 0  
BEGIN  
    SELECT  
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),  
      @ErrorSeverity = ERROR_SEVERITY(),  
      @ErrorState = ERROR_STATE()  
  
    GOTO error  
END  
  
SET NOCOUNT OFF  
  
RETURN  
  
error:  
    RAISERROR (@ErrorMessage,  
               @ErrorSeverity,  
               @ErrorState )  
  







