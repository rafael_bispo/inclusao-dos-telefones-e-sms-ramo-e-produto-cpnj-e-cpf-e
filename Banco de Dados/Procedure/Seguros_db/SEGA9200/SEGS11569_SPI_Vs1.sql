CREATE PROCEDURE dbo.SEGS11569_SPI  
    
    @usuario VARCHAR(20),        
    @num_versao INT        
        
AS        
        
SET NOCOUNT ON        
    
        
DECLARE @ErrorMessage NVARCHAR(4000)    
DECLARE @ErrorSeverity INT    
DECLARE @ErrorState INT      
      
    
--Copiando dados para SEGA9200_processado_tb      
INSERT INTO interface_dados_db.dbo.SEGA9200_processado_tb    
  ( SEGA9200_processar_id,    
  proposta_id,    
  apolice_id,    
  proposta_bb,    
  ramo_id,    
  produto_Id,    
  prop_cliente_id,    
  tp_documento_id,    
  tipo_documento,    
  num_solicitacao,            
  num_versao,    
  dt_inclusao,    
  usuario,  
  num_cobranca)        
SELECT  SEGA9200_processar_id,    
     proposta_id,    
  apolice_id,    
  proposta_bb,    
  ramo_id,    
  produto_Id,    
  prop_cliente_id,    
  tp_documento_id,    
  tipo_documento,    
  num_solicitacao,            
        @num_versao as num_versao,    
     GETDATE() as dt_inclusao,    
     @usuario as usuario,  
  num_cobranca       
  FROM interface_dados_db.dbo.SEGA9200_processar_tb        
        
IF @@ERROR <> 0                                  
BEGIN                                    
    SELECT     
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY(),    
      @ErrorState = ERROR_STATE()    
    
    GOTO error                                    
END       
    
INSERT INTO interface_dados_db.dbo.SEGA9200_10_processado_tb        
   ( SEGA9200_10_processar_id,     
     SEGA9200_processar_id,    
        proposta_id,    
        nome_destinatario,    
     endereco_destino,    
     bairro_destino,    
     municipio_destinatario,    
        UF_destinatario,    
     CEP_destinatario,    
      tipo_documento,      
     gerencia_destino,    
     cod_retorno,      
        dt_inclusao,      
        usuario,  
        num_cobranca)      
SELECT  SEGA9200_10_processar_id,    
  SEGA9200_processar_id,    
        proposta_id,    
        nome_destinatario,    
     endereco_destino,    
     bairro_destino,    
     municipio_destinatario,    
        UF_destinatario,    
     CEP_destinatario,    
      tipo_documento,      
     gerencia_destino,    
     cod_retorno,    
  GETDATE() as dt_inclusao,        
        @usuario as usuario,  
        num_cobranca      
  FROM interface_dados_db.dbo.SEGA9200_10_processar_tb        
        
IF @@ERROR <> 0                                  
BEGIN                                    
    SELECT     
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY(),    
      @ErrorState = ERROR_STATE()    
    
    GOTO error                                    
END     
    
INSERT INTO interface_dados_db.dbo.SEGA9200_20_processado_tb     
     ( SEGA9200_20_processar_id,    
       SEGA9200_processar_id,    
       proposta_id,    
       apolice_id,    
    proposta_bb,    
    nome_segurado,
    ddd_celular,
    celular,
    email,
    produto_id,
    ramo_id,    
    nome_produto, 
    cpf_cnpj,   
 certificado_id,  
 num_cobranca,  
 dt_vencimento,  
 val_premio,  
 dt_vencimento_proxima_parcela,  
    dt_inclusao,    
       usuario)    
          
SELECT SEGA9200_20_processar_id,    
       SEGA9200_processar_id,    
       proposta_id,    
       apolice_id,    
    proposta_bb,    
    nome_segurado,
    ddd_celular,
    celular,
    email,
    produto_id,
    ramo_id,        
    nome_produto, 
    cpf_cnpj,  
 certificado_id,  
 num_cobranca,  
 dt_vencimento,  
 val_premio,  
 dt_vencimento_proxima_parcela,  
    GETDATE() as dt_inclusao,        
       @usuario as usuario       
  FROM interface_dados_db.dbo.SEGA9200_20_processar_tb        
        
IF @@ERROR <> 0                                  
BEGIN                                    
    SELECT     
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY(),    
      @ErrorState = ERROR_STATE()    
    
    GOTO error                                    
END     
    
-- Excluindo os registros das tabelas processar        
DELETE FROM interface_dados_db.dbo.SEGA9200_10_processar_tb     
DELETE FROM interface_dados_db.dbo.SEGA9200_20_processar_tb       
DELETE FROM interface_dados_db.dbo.SEGA9200_processar_tb        
        
IF @@ERROR <> 0                                  
BEGIN                                    
    SELECT     
      @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),    
      @ErrorSeverity = ERROR_SEVERITY(),    
      @ErrorState = ERROR_STATE()    
    
    GOTO error                                    
END       
        
SET NOCOUNT OFF        
        
RETURN        
        
error:                                    
    RAISERROR (@ErrorMessage,    
               @ErrorSeverity,    
               @ErrorState )  
  

