CREATE PROCEDURE dbo.SEGS13428_SPI

        @usuario  VARCHAR(20)
	  , @num_remessa INT    

 AS  
     --R.FOUREAUX - CRIA��O SEGA9231 - 27/07/2017
 BEGIN
  
SET NOCOUNT ON   
  
/*--- TESTE --------------              
 DECLARE @usuario VARCHAR(20)                          
 SET @usuario    = 'R.FOUREAUX'      
-- FIM TESTE ----------*/   

----INCLUS�O NA TABELA PROCESSADO_TB 
BEGIN TRY 

	INSERT INTO interface_dados_db.dbo.sega9231_processado_tb 
			  ( sega9231_processar_id
			  , produto_id
			  , proposta_id
			  , proposta_bb
			  , ramo_id
			  , apolice_id
			  , layout_id
			  , num_versao
			  , tipo_documento
			  , num_solicitacao
			  , dt_inclusao
			  , dt_alteracao
			  , usuario
			  , endosso_id
			  , prop_cliente_id
			  , origem )

		SELECT sega9231_processar_id
		     , produto_id
			 , proposta_id
			 , proposta_bb
			 , ramo_id
			 , apolice_id
			 , layout_id
			 , ISNULL(num_versao, @num_remessa) as num_versao
			 , tipo_documento
			 , num_solicitacao
			 , getdate() as dt_inclusao
			 , dt_alteracao
			 , @usuario
			 , endosso_id
			 , prop_cliente_id
			 , origem
		  FROM interface_dados_db.dbo.sega9231_processar_tb  
 
 ----INCLUS�O NA TABELA 10_PROCESSADO_TB    
 
		 INSERT INTO interface_dados_db.dbo.sega9231_10_processado_tb 
		           ( sega9231_10_processar_id
				   , sega9231_processar_id
				   , num_endosso
				   , proposta_id 
				   , nome_destinatario 
				   , endereco_destino 
				   , municipio_destino 
				   , estado_destino 
				   , cep_destino 
				   , cod_retorno 
				   , gerencia_destino 
				   , tipo_documento 
				   , dt_inclusao 
				   , usuario 
				   , bairro )
			  SELECT sega9231_10_processar_id
			       , sega9231_processar_id
				   , num_endosso
				   , proposta_id 
				   , nome_destinatario 
				   , endereco_destino 
				   , municipio_destino 
				   , estado_destino 
				   , cep_destino 
				   , cod_retorno 
				   , gerencia_destino 
				   , tipo_documento 
				   , getdate() as dt_inclusao
				   , @usuario 
				   , bairro 
				FROM interface_dados_db.dbo.sega9231_10_processar_tb  
 
				----INCLUS�O NA TABELA 20_PROCESSADO_TB    
				INSERT INTO interface_dados_db.dbo.sega9231_20_processado_tb 
				          ( sega9231_20_processar_id
						  , sega9231_processar_id
						  , proposta_id
						  , ramo_id
						  , apolice_id
						  , proposta_bb
						  , numero_certificado
						  , cod_Processo_susep
						  , nome_segurado
						  , endereco_segurado
						  , municipio_segurado
						  , estado_segurado
						  , CEP_segurado
						  , nome_produto
						  , dt_cancelamento
						  , ddd_celular -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , celular -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , email -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno       
						  , agencia -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , dt_inclusao
						  , usuario )  
					 SELECT sega9231_20_processar_id
					      , sega9231_processar_id
						  , proposta_id
						  , ramo_id
						  , apolice_id
						  , proposta_bb
						  , numero_certificado
						  , cod_Processo_susep
						  , nome_segurado
						  , endereco_segurado
						  , municipio_segurado
						  , estado_segurado
						  , CEP_segurado
						  , nome_produto
						  , dt_cancelamento
						  , ddd_celular -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , celular -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , email -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno       
						  , agencia -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
						  , getdate() as dt_inclusao
						  , @usuario   
					   FROM interface_dados_db.dbo.sega9231_20_processar_tb 
  
  --copiado da procedure SEGS13435_SPU que nao vai existir mais.
				  UPDATE cancelamento_proposta      
					SET carta_emitida = 's'
					  , dt_alteracao =  GETDATE()
					  , usuario = @usuario                       
				   FROM seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta     
				   JOIN interface_dados_db.dbo.sega9231_processar_tb sega9231     
					 ON cancelamento_proposta.proposta_id = sega9231.proposta_id  
					AND cancelamento_proposta.endosso_id = sega9231.endosso_id    
				  WHERE sega9231.tipo_documento = 'ORIGINAL'      


END TRY                                      
                  
BEGIN CATCH    
  
 DECLARE @ErrorMessage NVARCHAR(4000)          
 DECLARE @ErrorSeverity INT          
 DECLARE @ErrorState INT        
            
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                  
     @ErrorSeverity = ERROR_SEVERITY(),           
     @ErrorState = ERROR_STATE()           
  
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState )    
END CATCH    
  
 END
  
  





