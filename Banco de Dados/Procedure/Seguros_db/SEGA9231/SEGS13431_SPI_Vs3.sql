CREATE PROCEDURE SEGS13431_SPI
        
   @Usuario          VARCHAR(20)        
        
AS 
       
--R.FOUREAUX - CRIA��O SEGA9231 - 31/07/2017
    
BEGIN
	    
SET NOCOUNT ON        
        
/*--- TESTE --------------              
 DECLARE @usuario VARCHAR(20)                          
 SET @usuario    = 'DEM 18319298'      
-- FIM TESTE ----------*/          

BEGIN TRY 
        
CREATE TABLE #sega9231_processar_tb        
		   ( sega9231_processar_id          INT NULL
		   , proposta_id                    NUMERIC(9,0) NULL
		   , produto_id                     INT NULL
		   , ramo_id                        INT NULL
		   , apolice_id                     INT NULL
		   , proposta_bb                    INT NULL
		   , segurado                       VARCHAR(255) NULL
		   , endereco                       VARCHAR(60) NULL
		   , municipio                      VARCHAR(60) NULL
		   , cep_formatado                  VARCHAR(9) NULL
		   , estado                         VARCHAR(2) NULL
		   , numero_certificado             NUMERIC(9,0) NULL
		   , cod_Processo_susep             VARCHAR(20) NULL
		   , nome                           VARCHAR(60) NULL
		   , dt_cancelamento             SMALLDATETIME NULL
		   , tp_cancelamento                CHAR(1) NULL
		   , origem                         CHAR(2) NULL
		   , ddd_celular					VARCHAR(4) NULL -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		   , celular						VARCHAR(9) NULL -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		   , email							VARCHAR(60) NULL -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		   , agencia						INT NULL -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		   )            
        
		INSERT INTO #sega9231_processar_tb        
				  ( sega9231_processar_id
				  , proposta_id
				  , produto_id
				  , segurado
				  , endereco
				  , proposta_bb
				  , municipio
				  , cep_formatado
				  , estado
				  , dt_cancelamento
				  , tp_cancelamento
				  , apolice_id
				  , ramo_id
				  , origem )        
    
			SELECT sega9231.sega9231_processar_id
			     , cancelamento_proposta.proposta_id
				 , sega9231.produto_id
				 , sega9231_10.nome_destinatario as segurado
				 , sega9231_10.endereco_destino as endereco
				 , sega9231.proposta_bb
				 , sega9231_10.municipio_destino as municipio
				 , sega9231_10.cep_destino as cep_formatado
				 , sega9231_10.estado_destino as estado
				 , cancelamento_proposta.dt_inicio_cancelamento
				 , cancelamento_proposta.tp_cancelamento
				 , sega9231.apolice_id
				 , sega9231.ramo_id
				 , sega9231.origem               
			  FROM interface_dados_db.dbo.sega9231_processar_tb sega9231 with (NOLOCK)        
		INNER JOIN interface_dados_db.dbo.sega9231_10_processar_tb sega9231_10 with (NOLOCK)        
			    ON sega9231_10.sega9231_processar_id = sega9231.sega9231_processar_id         
		INNER JOIN seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta with(NOLOCK)         
			    ON sega9231.proposta_id = cancelamento_proposta.proposta_id        
			   AND sega9231.endosso_id = cancelamento_proposta.endosso_id         
		
    
			-- Atualiza nome      
			UPDATE sega9231_processar_tb        
			   SET nome = produto.nome    
			  FROM #sega9231_processar_tb sega9231_processar_tb     
		INNER JOIN seguros_db.dbo.produto_tb produto         
				ON produto.produto_id = sega9231_processar_tb.produto_id  
			 WHERE 1 = 1       

			-- Atualizando numero certificado 
			UPDATE sega9231_processar_tb
			   SET numero_certificado  = certificado.certificado_id
			  FROM seguros_db.dbo.certificado_tb certificado  
		INNER JOIN #sega9231_processar_tb sega9231_processar_tb
		        ON sega9231_processar_tb.proposta_id = certificado.proposta_id 
			 WHERE 1 = 1 

			-- Atualizando numero certificado C�digo processo Susep -------
 			 UPDATE #sega9231_processar_tb
			    SET cod_processo_susep = subramo_tb.num_proc_susep
			   FROM #sega9231_processar_tb #sega9231_processar_tb
		 INNER JOIN seguros_db.dbo.proposta_tb proposta_tb
			     ON #sega9231_processar_tb.proposta_id = proposta_tb.proposta_id
		 INNER JOIN seguros_db.dbo.subramo_tb subramo_tb 
		         ON proposta_tb.ramo_id = subramo_tb.ramo_id
				AND proposta_tb.subramo_id = subramo_tb.subramo_id
				WHERE subramo_tb.dt_fim_vigencia_sbr is null -- Thiago Silva - Confitec - SBRJ009671 - 24/07/2020 

-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 23/03/2020 Inicio
update #sega9231_processar_tb
set #sega9231_processar_tb.nome = ISNULL(plano_tb.nome_fantasia,'')
from #sega9231_processar_tb #sega9231_processar_tb
inner join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with(nolock)
on escolha_plano_tb.proposta_id = #sega9231_processar_tb.proposta_id
and escolha_plano_tb.produto_id = #sega9231_processar_tb.produto_id
inner join seguros_db.dbo.plano_tb plano_tb with(nolock)
on  plano_tb.produto_id = escolha_plano_tb.produto_id
and plano_tb.plano_id = escolha_plano_tb.plano_id
where #sega9231_processar_tb.produto_id = 1243
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 23/03/2020 Fim 		


-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
 update #sega9231_processar_tb	
    set #sega9231_processar_tb.ddd_celular = isnull(cliente_tb.ddd_1,''),
		#sega9231_processar_tb.celular = isnull(cliente_tb.telefone_1,''),
		#sega9231_processar_tb.email = isnull(cliente_tb.e_mail,'')
   from #sega9231_processar_tb #sega9231_processar_tb
  INNER JOIN seguros_db.dbo.proposta_tb proposta_tb with(NOLOCK)          
	 ON proposta_tb.proposta_id = #sega9231_processar_tb.proposta_id          
  INNER JOIN seguros_db.dbo.cliente_tb cliente_tb with (NOLOCK)          
	 ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id

-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
 update #sega9231_processar_tb	
    set #sega9231_processar_tb.agencia = agencia_id
   from #sega9231_processar_tb #sega9231_processar_tb
  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with(NOLOCK)          
	 ON proposta_adesao_tb.proposta_id = #sega9231_processar_tb.proposta_id          
  INNER JOIN seguros_db.dbo.agencia_tb (NOLOCK)          
	 ON proposta_adesao_tb.cont_agencia_id = agencia_tb.agencia_id           
    AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id     
    
   -- RAFAEL MARTINS -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
  update #sega9231_processar_tb	
	 set #sega9231_processar_tb.agencia = agencia_id.agencia_id
    from #sega9231_processar_tb #sega9231_processar_tb
   INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb with(NOLOCK)          
	  ON proposta_fechada_tb.proposta_id = #sega9231_processar_tb.proposta_id          
   INNER JOIN seguros_db.dbo.agencia_tb (NOLOCK)          
	  ON proposta_fechada_tb.cont_agencia_id = agencia_tb.agencia_id           
     AND agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id  


 --SELECT * FROM #sega9231_processar_tb
       
				--Inserindo na tabela f�sica         
				INSERT INTO interface_dados_db.dbo.sega9231_20_processar_tb      
						  ( sega9231_processar_id        
						  , ramo_id        
						  , apolice_id        
						  , proposta_id        
						  , proposta_bb        
						  , nome_segurado        
						  , endereco_segurado        
						  , municipio_segurado        
						  , cep_segurado        
						  , estado_segurado        
						  , numero_certificado
						  , cod_Processo_susep	        
						  , nome_produto        
						  , dt_cancelamento
						  , ddd_celular
						  , celular
						  , email        
						  , agencia
						  , dt_inclusao        
						  , usuario )        
        
					 SELECT sega9231_processar_id        
						  , ramo_id        
						  , apolice_id        
						  , proposta_id
						  , proposta_bb        
						  , LEFT(segurado,60)        
						  , LEFT(endereco,60)        
						  , LEFT(municipio,60)        
						  , LEFT(cep_formatado,9)        
						  , LEFT(estado,2)        
						  , numero_certificado
						  , cod_Processo_susep
						  , LEFT(ISNULL(nome,''),60)
						  , dt_cancelamento
						  , ddd_celular
						  , celular
						  , email        
						  , agencia
						  , GETDATE()        
						  , @usuario        
					   FROM #sega9231_processar_tb 
					   
					   DROP TABLE #sega9231_processar_tb       
             

END TRY                                      
                  
BEGIN CATCH    
  
 DECLARE @ErrorMessage NVARCHAR(4000)          
 DECLARE @ErrorSeverity INT          
 DECLARE @ErrorState INT        
            
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                  
     @ErrorSeverity = ERROR_SEVERITY(),           
     @ErrorState = ERROR_STATE()           
  
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState )    
END CATCH      
      
END 









