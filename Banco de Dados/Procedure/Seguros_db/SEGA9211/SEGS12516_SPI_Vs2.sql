CREATE PROCEDURE dbo.SEGS12516_SPI (  
  @usuario  VARCHAR(20)            
)            
AS            
                        
SET NOCOUNT ON                    
                        
DECLARE @msg VARCHAR(100)            
  
-- TESTE  
-- DECLARE @usuario VARCHAR(100)            
-- set @usuario = 'TESTE'  
   
BEGIN TRY   
  
  
 INSERT INTO interface_dados_db..SEGA9211_20_processar_tb (            
       SEGA9211_processar_id,            
       proposta_id,            
       nome_segurado,
       ddd_celular,
       celular,
       email,            
       apolice_id,            
       proposta_bb,  
       cod_processo_susep,            
       certificado_id,            
       nome_produto,            
       dt_cancelamento,            
       num_operacao_credito,            
       dt_inclusao,                             
       usuario)   
SELECT SEGA9211_processar_tb.SEGA9211_processar_id,            
       SEGA9211_processar_tb.proposta_id,            
       cliente_tb.nome nome_segurado,            
       ISNULL(cliente_tb.ddd_1,'') as ddd_celular,
       ISNULL(cliente_tb.telefone_1,'') as celular,
       ISNULL(cliente_tb.e_mail,'') as email,      
       proposta_adesao_tb.apolice_id,            
       proposta_adesao_tb.proposta_bb,  
       Apolice_Tb.num_proc_susep ,            
       certificado_tb.certificado_id,            
       produto_tb.nome nome_produto,            
       cancelamento_proposta_tb.dt_cancelamento_bb dt_cancelamento,                 
       num_operacao_credito = CASE WHEN produto_tb.produto_id <> 1231                                
                                   THEN CONVERT(NUMERIC(9,0),questionario_proposta_tb.texto_resposta)      
                                   ELSE CONVERT(NUMERIC(17,0),questionario_proposta_tb.texto_resposta)      
                              END ,      
        GETDATE() as dt_inclusao,                           
       @usuario as usuario                   
  FROM interface_dados_db..SEGA9211_processar_tb SEGA9211_processar_tb (NOLOCK)            
  JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb (NOLOCK)            
    ON proposta_adesao_tb.proposta_id = SEGA9211_processar_tb.proposta_id            
  JOIN seguros_db..questionario_proposta_tb questionario_proposta_tb (NOLOCK)            
    ON questionario_proposta_tb.proposta_id = proposta_adesao_tb.proposta_id            
   AND questionario_proposta_tb.pergunta_id = 7677            
  JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb (NOLOCK)            
    ON SEGA9211_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id            
   AND cancelamento_proposta_tb.endosso_id = SEGA9211_processar_tb.endosso_id        
   AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL            
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)            
    ON SEGA9211_processar_tb.cliente_id = cliente_tb.cliente_id            
  JOIN seguros_db..certificado_tb certificado_tb (NOLOCK)            
    ON certificado_tb.proposta_id = SEGA9211_processar_tb.proposta_id            
   AND certificado_tb.certificado_id = (SELECT MAX(certificado_id)           
                                          FROM seguros_db..certificado_tb certificado_tb (NOLOCK)          
                                         WHERE proposta_id = SEGA9211_processar_tb.proposta_id)          
  JOIN seguros_db..produto_tb produto_tb (NOLOCK)            
  ON SEGA9211_processar_tb.produto_id = produto_tb.produto_id   
  JOIN  Seguros_db..Apolice_Tb Apolice_Tb (NOLOCK) ON   
  Apolice_Tb.apolice_id = proposta_adesao_tb.apolice_id AND   
  Apolice_Tb.ramo_id = proposta_adesao_tb.ramo_id      
  
  -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update SEGA9133_20_processar_tb
	set SEGA9133_20_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db..SEGA9211_20_processar_tb SEGA9211_20_processar_tb with(nolock)
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = SEGA9211_20_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id            
    
    
END TRY                                        
                    
BEGIN CATCH      
    
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
              
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                    
     @ErrorSeverity = ERROR_SEVERITY(),             
     @ErrorState = ERROR_STATE()             
    
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState )      
END CATCH      
  

