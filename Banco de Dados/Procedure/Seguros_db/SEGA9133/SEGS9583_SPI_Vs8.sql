CREATE PROCEDURE SEGS9583_SPI (  
  @usuario  VARCHAR(20)          
)          
AS          
                      
SET NOCOUNT ON                  
                      
DECLARE @msg VARCHAR(100)          
              
INSERT INTO interface_dados_db.dbo.SEGA9133_20_processar_tb (          
       SEGA9133_processar_id,          
       proposta_id,          
       nome_segurado,          
       apolice_id,          
       proposta_bb,          
       certificado_id,          
       nome_produto,          
       dt_cancelamento,          
       num_operacao_credito,
       ddd_celular,
       celular,
       email, 
       ramo_id,
	   produto_id,
	   cpf_cnpj,         
       dt_inclusao,          
       dt_alteracao,          
       usuario)          
SELECT SEGA9133_processar_tb.SEGA9133_processar_id,          
       SEGA9133_processar_tb.proposta_id,          
       cliente_tb.nome,          
       proposta_adesao_tb.apolice_id,          
       proposta_adesao_tb.proposta_bb,          
       certificado_tb.certificado_id,          
       produto_tb.nome,          
       cancelamento_proposta_tb.dt_cancelamento_bb,          
--       CONVERT(NUMERIC(9,0),questionario_proposta_tb.texto_resposta),       
       num_operacao_credito = CASE WHEN produto_tb.produto_id <> 1225    --DEMANDA: 17919477 - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma - Zoro.Gomes - Confitec - 03/06/2014        
                                   THEN CONVERT(NUMERIC(9,0),questionario_proposta_tb.texto_resposta)    
                                   ELSE CONVERT(NUMERIC(17,0),questionario_proposta_tb.texto_resposta)    
                              END ,  
       isnull(cliente_tb.ddd_1,''),
       isnull(cliente_tb.telefone_1,''),
       isnull(cliente_tb.e_mail,''),
       NULL AS ramo_id,
	   NULL AS produto_id,
	   NULL AS cpf_cnpj,                          
       GETDATE(),          
       NULL,          
       @usuario          
  FROM interface_dados_db..SEGA9133_processar_tb SEGA9133_processar_tb (NOLOCK)          
  JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb (NOLOCK)          
    ON proposta_adesao_tb.proposta_id = SEGA9133_processar_tb.proposta_id          
  JOIN seguros_db.dbo.questionario_proposta_tb questionario_proposta_tb (NOLOCK)          
    ON questionario_proposta_tb.proposta_id = proposta_adesao_tb.proposta_id          
   AND questionario_proposta_tb.pergunta_id = 7677          
  JOIN seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta_tb (NOLOCK)          
    ON SEGA9133_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id          
   AND cancelamento_proposta_tb.endosso_id = SEGA9133_processar_tb.endosso_id      
   AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL          
  JOIN seguros_db.dbo.cliente_tb cliente_tb (NOLOCK)          
    ON SEGA9133_processar_tb.cliente_id = cliente_tb.cliente_id          
  JOIN seguros_db.dbo.certificado_tb certificado_tb (NOLOCK)          
    ON certificado_tb.proposta_id = SEGA9133_processar_tb.proposta_id          
   AND certificado_tb.certificado_id = (SELECT MAX(certificado_id)         
                                          FROM seguros_db.dbo.certificado_tb certificado_tb (NOLOCK)        
                                         WHERE proposta_id = SEGA9133_processar_tb.proposta_id)        
  JOIN seguros_db.dbo.produto_tb produto_tb (NOLOCK)          
    ON SEGA9133_processar_tb.produto_id = produto_tb.produto_id          
 ORDER BY SEGA9133_processar_tb.SEGA9133_processar_id          
                  
 
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 24/03/2020 Inicio
update SEGA9133_20_processar_tb
set SEGA9133_20_processar_tb.nome_produto = ISNULL(plano_tb.nome_fantasia,'')
from interface_dados_db..SEGA9133_20_processar_tb SEGA9133_20_processar_tb with(nolock)
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
on proposta_tb.proposta_id = SEGA9133_20_processar_tb.proposta_id
inner join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with(nolock)
on escolha_plano_tb.proposta_id = SEGA9133_20_processar_tb.proposta_id
and escolha_plano_tb.produto_id = proposta_tb.produto_id
inner join seguros_db.dbo.plano_tb plano_tb with(nolock)
on  plano_tb.produto_id = escolha_plano_tb.produto_id
and plano_tb.plano_id = escolha_plano_tb.plano_id
where proposta_tb.produto_id = 1243
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 24/03/2020 Fim 	

-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update SEGA9133_20_processar_tb
set SEGA9133_20_processar_tb.produto_id = proposta_tb.produto_id, SEGA9133_20_processar_tb.ramo_id =  proposta_tb.ramo_id,
    SEGA9133_20_processar_tb.CPF_CNPJ = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from interface_dados_db.dbo.SEGA9133_20_processar_tb SEGA9133_20_processar_tb with(nolock)
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
on proposta_tb.proposta_id = SEGA9133_20_processar_tb.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
        
                
IF @@ERROR <> 0                    
BEGIN                      
    SELECT @msg = 'Erro ao inserir registros em interface_dados_db..SEGA9133_20_processar_tb'                    
    GOTO ERROR                      
END                    
                  
SET NOCOUNT OFF                    
                      
RETURN                    
                    
ERROR:                      
  
   -- Confitec � sql2012 � 07/08/2015 16:11:47 - Inicio --  
    --raiserror 55555 @msg           
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:47 - Fim --  
  
  
  
  

