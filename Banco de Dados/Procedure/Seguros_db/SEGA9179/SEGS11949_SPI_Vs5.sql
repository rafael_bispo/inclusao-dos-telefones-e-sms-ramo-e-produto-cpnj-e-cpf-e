CREATE PROCEDURE SEGS11949_SPI  
                                    
    @usuario VARCHAR(20)                                    
                                    
AS                                    
                                    
/*-----------------------------------------------------------------------------------------------------                
 DEMANDA: 17919477 - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma         
 SEGA9179 -- Certificado BB Seguro Prestamista PJ (1225)      
 30 - Detalhe do Arquivo - Dados do Grupo Segurado      
*/-----------------------------------------------------------------------------------------------------                
                             
SET NOCOUNT ON                                    
            
BEGIN TRY      
BEGIN TRANSACTION    
   INSERT INTO Interface_dados_db..SEGA9179_30_processar_tb(        
          SEGA9179_processar_id            
        , proposta_id            
        , nome_segurado            
        , dt_nascimento      
        , cpf       
        , usuario    
        , endereco_segurado    
        , bairro_segurado    
        , municipio_segurado    
        , uf_segurado    
        , cep_segurado    
        , tel_segurado)           
  
   SELECT SEGA9179_processar_id       
        , SEGA9179.proposta_id       
        , ISNULL(CLIENTE_CTR.NM_CLI,'')      
        , ISNULL(PESSOA_FISICA_TB.dt_nascimento, '19000101')    
        , cpf = CASE WHEN LEN(LTRIM(RTRIM(c.cpf_cnpj))) = 11 --CPF        
                     THEN SUBSTRING(LTRIM(c.cpf_cnpj),1,3) + '.' + SUBSTRING(LTRIM(c.cpf_cnpj),4,3) + '.' +        
                          SUBSTRING(LTRIM(c.cpf_cnpj),7,3) + '-' + SUBSTRING(LTRIM(c.cpf_cnpj),10,2)        
                     ELSE SUBSTRING(LTRIM(c.cpf_cnpj),1,2) + '.' + SUBSTRING(LTRIM(c.cpf_cnpj),3,3) + '.' +        
                          SUBSTRING(LTRIM(c.cpf_cnpj),6,3) + '/' + SUBSTRING(LTRIM(c.cpf_cnpj),9,4) + '-' +        
                          SUBSTRING(LTRIM(c.cpf_cnpj),13,2)        
                END        
        , @usuario      
        , ISNULL(CLIENTE_CTR.TX_LGR_CRSD,'')    
        , ISNULL(CLIENTE_CTR.TX_BAI_CRSD,'')    
        , ISNULL(CLIENTE_CTR.TX_MUN_CRSD,'')    
        , ISNULL(CLIENTE_CTR.SG_UF_CRSD,'')    
--        , ISNULL(CLIENTE_CTR.NR_CEP_CRSD,'')    
        , CASE            
            WHEN CLIENTE_CTR.NR_CEP_CRSD IS NULL THEN ''  
            ELSE SUBSTRING(RIGHT('00000000' + CONVERT(VARCHAR, CLIENTE_CTR.NR_CEP_CRSD), 8), 1, 5) + '-' +   
                 SUBSTRING(RIGHT('00000000' + CONVERT(VARCHAR, CLIENTE_CTR.NR_CEP_CRSD), 8), 6, 3)            
          END   
        , ISNULL(CLIENTE_CTR.NR_TEL_CLI,'')    
  
     FROM Interface_dados_db.dbo.SEGA9179_processar_tb SEGA9179 WITH(NOLOCK)                                        
    INNER JOIN seguros_db..PROPOSTA_COMPLEMENTAR_TB PC WITH (NOLOCK)      
       ON SEGA9179.proposta_id = PC.proposta_id    
    INNER JOIN seguros_db..CLIENTE_TB C WITH (NOLOCK)      
       ON C.CLIENTE_ID = PC.PROP_CLIENTE_ID    
    INNER JOIN seguros_db..PESSOA_FISICA_TB PESSOA_FISICA_TB WITH (NOLOCK)      
       ON C.CLIENTE_ID = PESSOA_FISICA_TB.PF_CLIENTE_ID    
    INNER JOIN seguros_db..PROPOSTA_PROCESSO_SUSEP_TB PROPOSTA_PROCESSO_SUSEP_TB WITH (NOLOCK)      
       ON SEGA9179.proposta_id = PROPOSTA_PROCESSO_SUSEP_TB.proposta_id      
    INNER JOIN ALS_OPERACAO_DB..CLIENTE_CTR CLIENTE_CTR WITH(NOLOCK)             
       ON CLIENTE_CTR.CD_PRD       = PROPOSTA_PROCESSO_SUSEP_TB.COD_PRODUTO    
      AND CLIENTE_CTR.CD_MDLD      = PROPOSTA_PROCESSO_SUSEP_TB.COD_MODALIDADE    
      AND CLIENTE_CTR.CD_ITEM_MDLD = PROPOSTA_PROCESSO_SUSEP_TB.COD_ITEM_MODALIDADE    
      AND CLIENTE_CTR.NR_CTR_SGRO  = PROPOSTA_PROCESSO_SUSEP_TB.NUM_CONTRATO_SEGURO    
      AND CLIENTE_CTR.NR_VRS_EDS   = (SELECT MAX(NR_VRS_EDS)    
                                        FROM ALS_OPERACAO_DB..CLIENTE_CTR AUX    
                                       WHERE AUX.CD_PRD          = PROPOSTA_PROCESSO_SUSEP_TB.COD_PRODUTO    
                                            AND AUX.CD_MDLD      = PROPOSTA_PROCESSO_SUSEP_TB.COD_MODALIDADE    
                                            AND AUX.CD_ITEM_MDLD = PROPOSTA_PROCESSO_SUSEP_TB.COD_ITEM_MODALIDADE    
                                            AND AUX.NR_CTR_SGRO  = PROPOSTA_PROCESSO_SUSEP_TB.NUM_CONTRATO_SEGURO)    
      AND CLIENTE_CTR.CD_CLI = C.cod_mci    
    WHERE pc.dt_fim_vigencia IS NULL      
    

-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update SEGA9179_30_processar_tb
	set SEGA9179_30_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db.dbo.SEGA9179_30_processar_tb SEGA9179_30_processar_tb with(nolock)
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = SEGA9179_30_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
  
   SET NOCOUNT OFF                                      
COMMIT         
END TRY      
      
BEGIN CATCH                                        
    
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
    SELECT             
       @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),            
       @ErrorSeverity = ERROR_SEVERITY(),            
       @ErrorState = ERROR_STATE()            
       
    RAISERROR (    
       @ErrorMessage,            
       @ErrorSeverity,            
       @ErrorState )      
    
ROLLBACK    
END CATCH                     
    
  
  