CREATE PROCEDURE SEGS11957_SPD
              
    @usuario VARCHAR(20),              
    @num_versao INT              
              
AS              
      
/*-----------------------------------------------------------------------------------------------------                
 DEMANDA: 17919477 - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma         
 SEGA9179 -- Certificado BB Seguro Prestamista PJ (1225)      
 Grava��o dos dados na tabela de processados      
*/-----------------------------------------------------------------------------------------------------                
                
BEGIN TRY     
      
   --Copiando dados para SEGA9179_processado_tb            
   INSERT INTO Interface_dados_db..SEGA9179_processado_tb (       
          SEGA9179_processar_id              
        , proposta_id                        
        , proposta_bb                        
        , apolice_id                         
        , certificado_id                     
        , grupo_ramo                         
        , agencia_id                         
        , nome_agencia                       
        , prazo_seguro                       
        , dt_inicio_vigencia                 
        , dt_fim_vigencia                    
        , ramo_id                            
        , produto_id                         
        , seguradora_cod_susep               
        , sucursal_seguradora_id             
        , tp_documento_id                    
        , tipo_documento                     
        , endosso_id                         
        , layout_id                          
        , num_solicitacao                    
        , usuario )                           
   SELECT SEGA9179_processar_id          
        , proposta_id                        
        , proposta_bb                        
        , apolice_id                         
        , certificado_id                     
        , grupo_ramo                         
        , agencia_id                         
        , nome_agencia                       
        , prazo_seguro                       
        , dt_inicio_vigencia                 
        , dt_fim_vigencia                    
        , ramo_id                            
        , produto_id                         
        , seguradora_cod_susep               
        , sucursal_seguradora_id             
        , tp_documento_id                    
        , tipo_documento                     
        , endosso_id                         
        , layout_id                          
        , num_solicitacao                    
        , usuario                            
     FROM Interface_dados_db..SEGA9179_processar_tb              
              
   --Copiando dados para SEGA9179_10_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_10_processado_tb (             
          SEGA9179_10_processar_id      
        , SEGA9179_processar_id         
        , proposta_id                   
        , endosso_id                    
        , nome_destinatario             
        , endereco_destinatario         
        , bairro_destinatario           
        , municipio_destinatario        
        , UF_destinatario               
        , CEP_destinatario              
        , cod_retorno                   
        , descricao_documento           
        , usuario                       
        , gerencia_solicitante          
        , destino                       
        , tipo_documento )               
   SELECT SEGA9179_10_processar_id        
        , SEGA9179_processar_id         
        , proposta_id                   
        , endosso_id                    
        , nome_destinatario             
        , endereco_destinatario         
        , bairro_destinatario           
        , municipio_destinatario        
        , UF_destinatario               
        , CEP_destinatario              
        , cod_retorno    
        , descricao_documento           
        , usuario                       
        , gerencia_solicitante          
        , destino                       
        , tipo_documento                
     FROM Interface_dados_db..SEGA9179_10_processar_tb              
                 
   --Copiando dados para SEGA9179_20_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_20_processado_tb (             
          SEGA9179_20_processar_id             
        , SEGA9179_processar_id                
        , proposta_id                          
        , proposta_bb                          
        , apolice_id                           
        , cod_agencia                          
        , nome_agencia                         
        , nome_estipulante                     
        , cnpj_estipulante                     
        , nome_corretor                        
        , usuario                              
        , prazo_seguro                         
        , numero_certificado                   
        , numero_processo_susep                
        , nome_proponente                      
        , cnpj_proponente                      
        , dt_emissao_certificado               
        , endereco_destinatario                
        , bairro_destinatario                  
        , municipio_destinatario               
        , UF_destinatario                      
        , CEP_destinatario                     
        , cod_susep_corretor                   
        , grupo_ramo                           
        , dt_inicio_vigencia_certificado       
        , dt_fim_vigencia_certificado  
        , telefone_estipulante
		,n_contrato_financiamento -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019
		,linha_credito  ) -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019     
   SELECT SEGA9179_20_processar_id        
        , SEGA9179_processar_id                 
        , proposta_id                           
        , proposta_bb                           
        , apolice_id                            
        , cod_agencia                           
        , nome_agencia                          
        , nome_estipulante                      
        , cnpj_estipulante                      
        , nome_corretor                         
        , usuario                               
        , prazo_seguro                          
        , numero_certificado                    
        , numero_processo_susep                 
        , nome_proponente                       
        , cnpj_proponente                       
        , dt_emissao_certificado                
        , endereco_destinatario                 
        , bairro_destinatario                   
        , municipio_destinatario                
        , UF_destinatario                       
        , CEP_destinatario                      
        , cod_susep_corretor                    
        , grupo_ramo                            
        , dt_inicio_vigencia_certificado        
        , dt_fim_vigencia_certificado   
        , telefone_estipulante
		,n_contrato_financiamento -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019
		,linha_credito  -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019
     FROM Interface_dados_db..SEGA9179_20_processar_tb              
              
   --Copiando dados para SEGA9179_30_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_30_processado_tb (             
          SEGA9179_30_processar_id       
        , SEGA9179_processar_id          
        , proposta_id                    
        , nome_segurado                  
        , dt_nascimento                  
        , cpf                            
        , usuario                        
        , endereco_segurado    
        , bairro_segurado    
        , municipio_segurado    
        , uf_segurado    
        , cep_segurado    
        , tel_segurado
		,perc_participacao_socio -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019   
		, cpf_cnpj)        
  
   SELECT SEGA9179_30_processar_id        
        , SEGA9179_processar_id  
        , proposta_id                    
        , nome_segurado                  
        , dt_nascimento                  
        , cpf                            
        , usuario     
        , endereco_segurado    
        , bairro_segurado    
        , municipio_segurado    
        , uf_segurado    
        , cep_segurado    
        , tel_segurado
		,perc_participacao_socio   -- Carlos.Lima - Confitec Circular 365 Sala �gil - 09/09/2019   
		, cpf_cnpj     
     FROM Interface_dados_db..SEGA9179_30_processar_tb              
                 
   --Copiando dados para SEGA9179_40_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_40_processado_tb(             
          SEGA9179_40_processar_id         
        , SEGA9179_processar_id            
        , proposta_id                      
        , nome_cobertura                   
        , val_capital_segurado             
        , usuario                          
        , val_premio                        
        , dt_inicio_vigencia_cobertura  
        , dt_fim_vigencia_cobertura)    
  
   SELECT SEGA9179_40_processar_id        
        , SEGA9179_processar_id            
        , proposta_id      
        , nome_cobertura                   
        , val_capital_segurado             
        , usuario                          
        , val_premio   
        , dt_inicio_vigencia_cobertura  
        , CONVERT(VARCHAR,dt_fim_vigencia_cobertura,112)    
     FROM Interface_dados_db..SEGA9179_40_processar_tb              
                 
   --Copiando dados para SEGA9179_50_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_50_processado_tb (             
          SEGA9179_50_processar_id                 
        , SEGA9179_processar_id                    
        , proposta_id                              
        , endosso_id                               
        , moeda                                    
        , val_premio_liquido                       
        , val_iof                                  
        , val_premio_bruto                         
        , periodicidade                            
        , num_parcela                              
        , val_primeira_parcela                     
        , descricao_forma_pgto_1_parcela           
        , banco_id                                 
        , descricao_banco                          
        , agencia_id                               
        , descricao_agencia                        
        , val_demais_parcela                       
        , descricao_forma_pgto_demais_parcela      
        , dia_debito                               
        , num_cartao                               
        , usuario                                  
        , uf_agencia                               
        , conta_corrente_deb                    
        , dt_validade_cartao )                      
   SELECT SEGA9179_50_processar_id        
        , SEGA9179_processar_id                    
        , proposta_id                              
        , endosso_id                               
        , moeda                                    
        , val_premio_liquido                       
        , val_iof                                  
        , val_premio_bruto                         
        , periodicidade                            
        , num_parcela                              
        , val_primeira_parcela                     
        , descricao_forma_pgto_1_parcela           
        , banco_id                                 
        , descricao_banco                          
        , agencia_id                               
        , descricao_agencia                        
        , val_demais_parcela                       
        , descricao_forma_pgto_demais_parcela      
        , dia_debito                               
        , num_cartao                               
        , usuario         
        , uf_agencia                               
        , conta_corrente_deb                    
        , dt_validade_cartao                       
     FROM Interface_dados_db..SEGA9179_50_processar_tb              
                  
    --Copiando dados para SEGA9179_51_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_51_processado_tb (             
          SEGA9179_51_processar_id            
        , SEGA9179_processar_id               
        , proposta_id                 
        , Moeda                               
        , premio_liquido_total                
        , iof                                 
        , premio_bruto_total                  
        , periodicidade                       
        , numero_parcelas                     
        , valor_primeira_parcela              
        , forma_pgto_primeira                 
        , numero_banco                        
        , nome_banco                          
        , codigo_agencia                      
        , nome_agencia                        
        , uf_agencia                          
        , conta_corrente_debito               
        , valor_demais_parcelas               
        , forma_pgto_demais_parcelas          
        , dia_pgto_debito_conta               
        , numero_cartao_credito_pgto          
        , validade_cartao_credito_pgto        
        , usuario    )
--      , MesAno_refer_primeira_Parcela       --Zoro.Gomes - Confitec - Novos campos - 29/12/2014  
--      , MesAno_refer_segunda_Parcela          --Retirados em 19/03/2015 e foram para o registro 52
--      , MesAno_refer_demais_parcelas_inicial  
--      , MesAno_refer_demais_parcelas_final    
--      , Valor_segunda_parcela          )       
   SELECT SEGA9179_51_processar_id        
        , SEGA9179_processar_id               
        , proposta_id                         
        , Moeda                               
        , premio_liquido_total                
        , iof                                 
        , premio_bruto_total                  
        , periodicidade                       
        , numero_parcelas                     
        , valor_primeira_parcela              
        , forma_pgto_primeira                 
        , numero_banco                        
        , nome_banco                          
        , codigo_agencia                      
        , nome_agencia                        
        , uf_agencia                          
        , conta_corrente_debito               
        , valor_demais_parcelas               
        , forma_pgto_demais_parcelas          
        , dia_pgto_debito_conta               
        , numero_cartao_credito_pgto          
        , validade_cartao_credito_pgto        
        , usuario          
--      , MesAno_refer_primeira_Parcela       --Zoro.Gomes - Confitec - Novos campos - 29/12/2014  
--      , MesAno_refer_segunda_Parcela          --Retirados em 19/03/2015 e foram para o registro 52
--      , MesAno_refer_demais_parcelas_inicial  
--      , MesAno_refer_demais_parcelas_final    
--      , Valor_segunda_parcela            
     FROM Interface_dados_db..SEGA9179_51_processar_tb              
               
   --Copiando dados para SEGA9179_52_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9179_52_processado_tb (             
          SEGA9179_52_processar_id                             
        , SEGA9179_processar_id                                
        , numero_proposta_ab                                   
        , numero_parcelas_quitadas                             
        , premio_quitado_liquido_total                         
        , premio_quitado_iof                                   
        , premio_quitado_bruto_total                           
        , novo_valor_moeda    
        , novo_valor_premio_liquido_total                      
        , novo_valor_iof                                       
        , novo_valor_premio_bruto_total_quitado                
        , novo_valor_periodicidade                             
        , novo_valor_numero_parcelas                           
        , novo_valor_valor_demais_parcelas                     
        , novo_valor_forma_pagamento_demais_parcelas           
        , pgto_debito_conta_numero_banco                       
        , pgto_debito_conta_nome_banco                         
        , pgto_debito_conta_codigo_agencia                     
        , pgto_debito_conta_nome_agencia                       
        , pgto_debito_conta_uf_agencia                         
        , pgto_debito_conta_corrente         
        , pgto_cartao_credito_numero_cartao                    
        , pgto_cartao_credito_validade_cartao                  
        , usuario     
	    , MesAno_refer_primeira_Parcela          --Zoro.Gomes - Confitec - Novos campos vindos do registro 51 - 19/03/2015  
		, MesAno_refer_segunda_Parcela         
		, MesAno_refer_demais_parcelas_inicial 
		, MesAno_refer_demais_parcelas_final   
		, Valor_primeira_parcela               
		, Valor_segunda_parcela                
        , Valor_demais_parcelas )
                                         
   SELECT      
          SEGA9179_52_processar_id                             
        , SEGA9179_processar_id                                
        , numero_proposta_ab                                   
        , numero_parcelas_quitadas                             
        , premio_quitado_liquido_total                         
        , premio_quitado_iof                                   
        , premio_quitado_bruto_total                           
        , novo_valor_moeda                                     
        , novo_valor_premio_liquido_total                      
        , novo_valor_iof                                       
        , novo_valor_premio_bruto_total_quitado                
        , novo_valor_periodicidade                             
        , novo_valor_numero_parcelas                           
        , novo_valor_valor_demais_parcelas                     
        , novo_valor_forma_pagamento_demais_parcelas           
        , pgto_debito_conta_numero_banco                       
        , pgto_debito_conta_nome_banco                         
        , pgto_debito_conta_codigo_agencia                     
        , pgto_debito_conta_nome_agencia                       
        , pgto_debito_conta_uf_agencia                         
        , pgto_debito_conta_corrente                           
        , pgto_cartao_credito_numero_cartao                    
        , pgto_cartao_credito_validade_cartao                  
        , usuario   
	    , MesAno_refer_primeira_Parcela          --Zoro.Gomes - Confitec - Novos campos vindos do registro 51 - 19/03/2015  
		, MesAno_refer_segunda_Parcela         
		, MesAno_refer_demais_parcelas_inicial 
		, MesAno_refer_demais_parcelas_final   
		, Valor_primeira_parcela               
		, Valor_segunda_parcela                
        , Valor_demais_parcelas 
     FROM Interface_dados_db..SEGA9179_52_processar_tb              
                   
   --Copiando dados para SEGA9179_60_processado_tb            --Zoro.Gomes - Confitec - Novos campos - 29/12/2014  
   INSERT INTO Interface_dados_db..SEGA9179_60_processado_tb (             
          SEGA9179_60_processar_id                             
        , SEGA9179_processar_id                                
        , numero_proposta_ab                                   
        , moeda_seguro_restituir  
        , premio_seguro_restituir  
        , usuario )                                             
   SELECT      
          SEGA9179_60_processar_id        
        , SEGA9179_processar_id                                
        , numero_proposta_ab                                   
        , moeda_seguro_restituir  
        , premio_seguro_restituir  
        , usuario                                              
     FROM Interface_dados_db..SEGA9179_60_processar_tb              
  
  
   -- Excluindo os registros das tabelas processar   
   DELETE FROM Interface_dados_db..SEGA9179_10_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_20_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_30_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_40_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_50_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_51_processar_tb      
   DELETE FROM Interface_dados_db..SEGA9179_52_processar_tb    
   DELETE FROM Interface_dados_db..SEGA9179_60_processar_tb    
   DELETE FROM Interface_dados_db..SEGA9179_processar_tb                
      
END TRY                                        
                       
BEGIN CATCH                                        
    
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
    SELECT             
       @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),            
       @ErrorSeverity = ERROR_SEVERITY(),            
       @ErrorState = ERROR_STATE()            
       
    RAISERROR (    
       @ErrorMessage,            
       @ErrorSeverity,            
       @ErrorState )      
END CATCH                     
    
  
  



