CREATE PROCEDURE SEGS12184_SPS    
  
AS  

BEGIN TRY  
  SELECT cancelamento_proposta_tb.proposta_id,   
         segurado = cliente_tb.nome,  
         endereco_corresp_tb.endereco ,  
         endereco_corresp_tb.bairro,  
         endereco_corresp_tb.municipio,  
         endereco_corresp_tb.cep,   
         endereco_corresp_tb.estado,   
         cancelamento_proposta_tb.dt_cancelamento_bb,   
         cancelamento_proposta_tb.tp_cancelamento,   
         proposta_tb.produto_id,   
         cancelamento_proposta_tb.endosso_id,  
         apolice_id = proposta_adesao_tb.apolice_id,  
         certificado_tb.certificado_id,  
         ramo_id = PROPOSTA_TB.RAMO_ID,  
         NULL destino,   
         NULL diretoria_id,   
         '1' tp_doc,   
         NULL  origem,   
         0 num_solicitacao,  
         SUBRAMO_TB.num_proc_susep,  
         proposta_adesao_tb.cont_agencia_id,  
         proposta_adesao_tb.PROPOSTA_BB,  
         nome_corretor = CORRETOR_TB.nome,  
         NOME_PRODUTO = PRODUTO_TB.nome,  
         endosso_tb.dt_emissao,  
         SINISTRO_TB.SINISTRO_ID,
         cliente_tb.ddd_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.telefone_1, --migracao-documentacao-digital-2a-fase-cartas
         cliente_tb.e_mail, --migracao-documentacao-digital-2a-fase-cartas
         proposta_tb.produto_id, --migracao-documentacao-digital-2a-fase-cartas
         proposta_tb.ramo_id, --migracao-documentacao-digital-2a-fase-cartas
         CAST('' AS VARCHAR(14)) as cpf_cnpj
         INTO #CARTA
    FROM seguros_db..proposta_tb proposta_tb WITH (nolock)  
    JOIN seguros_db..cancelamento_proposta_tb cancelamento_proposta_tb WITH (nolock)   
      ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id   
    JOIN seguros_db..certificado_tb certificado_tb WITH (nolock)  
      ON cancelamento_proposta_tb.proposta_id = certificado_tb.proposta_id  
     --AND certificado_tb.dt_fim_vigencia IS NULL   
    JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb WITH (nolock)  
      ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id    
    JOIN seguros_db..subramo_tb subramo_tb WITH (nolock)  
      ON proposta_tb.subramo_id = subramo_tb.subramo_id   
    JOIN seguros_db..produto_tp_carta_tb produto_tp_carta_tb WITH (nolock)  
      ON proposta_tb.produto_id = produto_tp_carta_tb.produto_id   
    JOIN seguros_db..cliente_tb cliente_tb WITH (nolock)  
      ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id   
    JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb WITH (nolock)  
      ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..endosso_tb endosso_tb WITH (nolock)  
   ON cancelamento_proposta_tb.proposta_id = endosso_tb.proposta_id  
  AND cancelamento_proposta_tb.ENDOSSO_ID  = endosso_tb.endosso_id  
 JOIN seguros_db..corretagem_tb corretagem_tb  WITH (nolock)  
   ON corretagem_tb.proposta_id = proposta_tb.proposta_id  
 JOIN seguros_db..corretor_tb corretor_tb  WITH (nolock)  
   ON corretor_tb.corretor_id = corretagem_tb.corretor_id  
 JOIN seguros_db..produto_tb produto_tb  WITH (nolock)  
   ON produto_tb.produto_id = proposta_tb.produto_id  
 JOIN seguros_db..sinistro_tb sinistro_tb  WITH (nolock)  
   ON sinistro_tb.proposta_id = proposta_tb.proposta_id  
   WHERE cancelamento_proposta_tb.tp_cancelamento = 8           
     --AND cancelamento_proposta_tb.carta_emitida = 'n'  
  AND ENDOSSO_TB.TP_ENDOSSO_ID = 68  
     AND cancelamento_proposta_tb.dt_fim_cancelamento is NULL  
     AND isnull(endereco_corresp_tb.emite_documento, 's') = 's'   
     AND produto_tp_carta_tb.tp_carta_id = '14'                  
     AND cancelamento_proposta_tb.endosso_id IS NOT NULL   
--MARCIO.NOGUEIRA - CONFITEC-RJ - Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3 - Produto 1231     
  AND (proposta_tb.produto_id = 1231 or proposta_tb.produto_id = 1225) 
  AND subramo_tb.dt_fim_vigencia_sbr IS NULL  
     AND not exists (select 1   
        from evento_seguros_db..evento_impressao_tb imp WITH (nolock)  
                      where proposta_tb.proposta_id = imp.proposta_id  
                        and imp.tp_documento_id = 14)  
  
   UNION ALL  
  SELECT cancelamento_proposta_tb.proposta_id,   
         segurado = cliente_tb.nome,   
         endereco_corresp_tb.endereco ,   
         endereco_corresp_tb.bairro,   
         endereco_corresp_tb.municipio,   
         endereco_corresp_tb.cep,    
         endereco_corresp_tb.estado,    
         cancelamento_proposta_tb.dt_cancelamento_bb,   
         cancelamento_proposta_tb.tp_cancelamento,    
         proposta_tb.produto_id,   
         cancelamento_proposta_tb.endosso_id,   
         apolice_id = proposta_adesao_tb.apolice_id,  
         certificado_tb.certificado_id,  
         ramo_id = PROPOSTA_TB.ramo_id,  
         evento_impressao_tb.destino,   
         evento_impressao_tb.diretoria_id,   
         '3' tp_doc   
         ,NULL origem   
         ,evento_impressao_tb.num_solicitacao   
   ,SUBRAMO_TB.num_proc_susep  
   ,proposta_adesao_tb.cont_agencia_id  
   ,proposta_adesao_tb.PROPOSTA_BB  
   ,nome_corretor = CORRETOR_TB.nome  
   ,NOME_PRODUTO = PRODUTO_TB.nome  
   ,endosso_tb.dt_emissao  
   ,SINISTRO_TB.SINISTRO_ID
   ,cliente_tb.ddd_1 --migracao-documentacao-digital-2a-fase-cartas
   ,cliente_tb.telefone_1 --migracao-documentacao-digital-2a-fase-cartas
   ,cliente_tb.e_mail --migracao-documentacao-digital-2a-fase-cartas  
   ,proposta_tb.produto_id --migracao-documentacao-digital-2a-fase-cartas
   ,proposta_tb.ramo_id --migracao-documentacao-digital-2a-fase-cartas
   ,CAST('' AS VARCHAR(14)) as cpf_cnpj
    FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (nolock)  
    JOIN SEGUROS_DB..proposta_tb proposta_tb WITH (nolock)  
      ON evento_impressao_tb.proposta_id = proposta_tb.proposta_id   
    JOIN SEGUROS_DB..certificado_tb certificado_tb WITH (nolock)  
      ON proposta_tb.proposta_id = certificado_tb.proposta_id  
     --AND certificado_tb.dt_fim_vigencia IS NULL   
    JOIN SEGUROS_DB..proposta_adesao_tb proposta_adesao_tb WITH (nolock)  
      ON proposta_tb.proposta_id = proposta_adesao_tb.proposta_id    
    JOIN SEGUROS_DB..cancelamento_proposta_tb cancelamento_proposta_tb WITH (nolock)  
      ON proposta_tb.proposta_id = cancelamento_proposta_tb.proposta_id   
    JOIN SEGUROS_DB..cliente_tb cliente_tb WITH (nolock)  
      ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id   
    JOIN SEGUROS_DB..endereco_corresp_tb endereco_corresp_tb WITH (nolock)  
      ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
 JOIN SEGUROS_DB..ENDOSSO_TB ENDOSSO_TB WITH (nolock)  
   ON cancelamento_proposta_tb.PROPOSTA_ID = ENDOSSO_TB.PROPOSTA_ID  
  AND cancelamento_proposta_tb.ENDOSSO_ID  = ENDOSSO_TB.ENDOSSO_ID   
 JOIN SEGUROS_DB..SUBramo_tb SUBramo_tb WITH (nolock)  
      ON PROPOSTA_TB.SUBramo_id = SUBramo_tb.SUBramo_id   
 JOIN SEGUROS_DB..CORRETAGEM_TB CORRETAGEM_TB WITH (nolock)  
   ON CORRETAGEM_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID  
 JOIN SEGUROS_DB..CORRETOR_TB CORRETOR_TB WITH (nolock)  
   ON CORRETOR_TB.corretor_id = CORRETAGEM_TB.corretor_id  
 JOIN SEGUROS_DB..PRODUTO_TB PRODUTO_TB WITH (nolock)  
   ON PRODUTO_TB.PRODUTO_ID = PROPOSTA_TB.PRODUTO_ID  
 JOIN SEGUROS_DB..SINISTRO_TB SINISTRO_TB WITH (nolock)  
   ON SINISTRO_TB.PROPOSTA_ID = PROPOSTA_TB.PROPOSTA_ID  
   WHERE cancelamento_proposta_tb.tp_cancelamento = 8  
     AND isnull(endereco_corresp_tb.emite_documento, 's') = 's'   
     AND evento_impressao_tb.status = 'l'   
  AND ENDOSSO_TB.TP_ENDOSSO_ID = 68  
     AND evento_impressao_tb.dt_geracao_arquivo is NULL   
     AND evento_impressao_tb.tp_documento_id = 14         
     AND cancelamento_proposta_tb.dt_fim_cancelamento is NULL  
  AND SUBRAMO_TB.dt_fim_vigencia_sbr IS NULL       
  --MARCIO.NOGUEIRA - CONFITEC-RJ - Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3 - Produto 1231
    AND (proposta_tb.produto_id = 1231 or proposta_tb.produto_id = 1225)
   ORDER BY endereco_corresp_tb.cep, proposta_tb.produto_id  
   
   -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #CARTA
set #CARTA.CPF_CNPJ = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from #CARTA #CARTA
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #CARTA.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
   
   
   SELECT proposta_id,   
         segurado,
         endereco ,  
         bairro,  
         municipio,  
         cep,   
         estado,   
         dt_cancelamento_bb,   
         tp_cancelamento,   
         produto_id,   
         endosso_id,  
         apolice_id,
         certificado_id,  
         ramo_id,
         destino,   
         diretoria_id,   
         tp_doc,   
         origem,   
         num_solicitacao,  
         num_proc_susep,  
         cont_agencia_id,  
         PROPOSTA_BB,  
         nome_corretor,
         NOME_PRODUTO,
         dt_emissao,  
         SINISTRO_ID,
         ddd_1,
         telefone_1, 
         e_mail, 
         produto_id, 
         ramo_id, 
         cpf_cnpj
    FROM #CARTA
   
   
    

END TRY 

BEGIN CATCH                                    
	DECLARE @ErrorMessage NVARCHAR(4000)        
	DECLARE @ErrorSeverity INT        
	DECLARE @ErrorState INT      
	SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),        
		   @ErrorSeverity = ERROR_SEVERITY(),        
		   @ErrorState = ERROR_STATE()        

	RAISERROR (@ErrorMessage,        
		       @ErrorSeverity,        
			   @ErrorState )  
END CATCH              



