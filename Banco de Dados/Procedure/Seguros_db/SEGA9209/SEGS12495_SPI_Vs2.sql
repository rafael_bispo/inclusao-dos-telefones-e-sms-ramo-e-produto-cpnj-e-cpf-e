CREATE PROCEDURE dbo.SEGS12495_SPI  
        
   @Usuario          VARCHAR(20)        
        
AS        
        
begin try  
        
SET NOCOUNT ON        
        
CREATE TABLE #SEGA9209_processar_tb        
  (        
    SEGA9209_10_processar_id       INT,         
    SEGA9209_processar_id          INT,         
    produto_id                     INT,          
    nome_produto       varchar(60),    
    apolice_id                     INT,         
    endosso_id        int,    
    proposta_id                    NUMERIC(9,0),              
    proposta_bb                    INT,        
    num_parcela        int,    
    dt_vencimento       smalldatetime,  
    val_parcela        numeric(15,2),
    ddd_celular  varchar(4), --migracao-documentacao-digital-2a-fase-cartas
    celular varchar(9), --migracao-documentacao-digital-2a-fase-cartas
    email varchar(60), --migracao-documentacao-digital-2a-fase-cartas 
    ramo_id int, --migracao-documentacao-digital-2a-fase-cartas   
    cpf_cnpj varchar(14)   
  )        
      
DECLARE @Data_sistema    SMALLDATETIME        
        
--Obtendo a data do sistema          
SELECT @Data_sistema = dt_operacional        
FROM seguros_db..parametro_geral_tb  
        
INSERT INTO #SEGA9209_processar_tb        
(        
 SEGA9209_10_processar_id   
 ,SEGA9209_processar_id                
 ,produto_id        
 ,nome_produto    
 ,apolice_id      
 ,endosso_id  
 ,proposta_id       
 ,proposta_bb     
 ,num_parcela      
 ,dt_vencimento  
 ,val_parcela            
)        
        
SELECT SEGA9209_10_processar_tb.SEGA9209_10_processar_id   
    ,SEGA9209_10_processar_tb.SEGA9209_processar_id       
       ,SEGA9209_processar_tb.produto_id        
       ,null -- nome_produto  
       ,SEGA9209_processar_tb.apolice_id        
       ,null -- endosso_id  
       ,SEGA9209_processar_tb.proposta_id         
       ,SEGA9209_processar_tb.proposta_bb  
       ,SEGA9209_processar_tb.num_cobranca  
       ,SEGA9209_processar_tb.dt_agendamento  
       ,SEGA9209_processar_tb.val_cobranca                               
FROM interface_dados_db.dbo.SEGA9209_processar_tb SEGA9209_processar_tb (NOLOCK)        
INNER JOIN interface_dados_db.dbo.SEGA9209_10_processar_tb SEGA9209_10_processar_tb (NOLOCK)        
  ON SEGA9209_10_processar_tb.SEGA9209_processar_id = SEGA9209_processar_tb.SEGA9209_processar_id         
        
CREATE INDEX I001_SEGA9209_processar ON #SEGA9209_processar_tb (proposta_id)      
CREATE INDEX I002_SEGA9209_processar ON #SEGA9209_processar_tb (produto_id)    
      
UPDATE #SEGA9209_processar_tb        
   SET nome_produto = produto_tb.nome    
  FROM #SEGA9209_processar_tb      
INNER JOIN seguros_db..produto_tb (NOLOCK)         
    ON produto_tb.produto_id = #SEGA9209_processar_tb.produto_id   
    
-- Atualizando o contatos do segurado --------------------------------------------  
	  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
	  UPDATE #SEGA9209_processar_tb
	  SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,'')
	  , email = isnull(cliente_tb.e_mail,''), ramo_Id = proposta_tb.ramo_id    
	  FROM #SEGA9209_processar_tb (NOLOCK)  
	  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
		ON #SEGA9209_processar_tb.proposta_id = proposta_tb.proposta_id  
	  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
		ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id    
		
		
		-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		update #SEGA9209_processar_tb
		set #SEGA9209_processar_tb.CPF_CNPJ = CASE 
													WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
													WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
													ELSE cliente_tb.cpf_cnpj
												END 
		from #SEGA9209_processar_tb #SEGA9209_processar_tb
		inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
			on proposta_tb.proposta_id = #SEGA9209_processar_tb.proposta_id
		inner join seguros_db..cliente_tb cliente_tb with(nolock)
			on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
		left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
			on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
		left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
			on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id  
          
        
--Inserindo na tabela f�sica         
INSERT INTO interface_dados_db.dbo.SEGA9209_20_processar_tb        
  (        
    SEGA9209_10_processar_id    
    ,SEGA9209_processar_id      
    ,produto_id        
 ,nome_produto    
 ,numero_apolice      
 ,numero_endosso  
 ,proposta_id       
 ,proposta_bb     
 ,num_prim_parcela      
 ,dt_venc_prim_parcela  
 ,valor_receb_prim_parcela     
 ,dt_emissao_carta  
 ,dt_inclusao        
 ,usuario
 ,ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
 ,celular  --migracao-documentacao-digital-2a-fase-cartas
 ,email   --migracao-documentacao-digital-2a-fase-cartas     
 ,ramo_id   --migracao-documentacao-digital-2a-fase-cartas 
 ,cpf_cnpj             
  )        
        
SELECT SEGA9209_10_processar_id  
  ,SEGA9209_processar_id        
  ,produto_id        
   ,nome_produto    
   ,isnull(apolice_id,0)      
   ,isnull(endosso_id,0)   
   ,proposta_id       
   ,proposta_bb     
   ,num_parcela    
   ,replace(convert(varchar,dt_vencimento,104),'.','')   
   ,val_parcela   
   ,replace(convert(varchar,@Data_sistema,104),'.','')    
   ,GETDATE()        
   ,@usuario
   ,ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
   ,celular  --migracao-documentacao-digital-2a-fase-cartas
   ,email   --migracao-documentacao-digital-2a-fase-cartas     
   ,ramo_id   --migracao-documentacao-digital-2a-fase-cartas       
   ,cpf_cnpj                       
FROM #SEGA9209_processar_tb        
        
Drop table #SEGA9209_processar_tb        
        
SET NOCOUNT OFF        
        
RETURN                          
                          
end try  
                      
begin catch     
    
declare @errormessage nvarchar(4000)            
declare @errorseverity int            
declare @errorstate int      
      
   select    
                        @errormessage = error_procedure() + ' - linha ' + convert(varchar(15),error_line()) + ' - ' + error_message(),            
                        @errorseverity = error_severity(),            
                        @errorstate = error_state()            
      
            raiserror (    
                        @errormessage,            
                        @errorseverity,            
                        @errorstate )      
    
end catch               
      
      
  
  

