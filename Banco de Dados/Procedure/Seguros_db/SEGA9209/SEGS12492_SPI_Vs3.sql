CREATE PROCEDURE SEGS12492_SPI  
@layout_id    INT,    
@arquivo_remessa_grf VARCHAR(50),    
@dt_geracao_arquivo  SMALLDATETIME,    
@num_remessa   INT,    
@producao    CHAR(1),    
@usuario    VARCHAR(20)    
  
AS  
  
begin try  
   
SET NOCOUNT ON   
  
-- declare @msg    varchar(500)  
declare @qtdelinhas as integer   
  
select @qtdelinhas = count(1) from ##tmp_pre_SEGA9209_tb   
  
-- 14/08/2015  
IF NOT EXISTS(SELECT versao                  
                        FROM controle_proposta_db..arquivo_versao_tb (NOLOCK)                  
                       WHERE layout_id = @layout_id   
                         AND versao = @num_remessa)  
begin    
  --- necessario ter dados na controle_proposta_db.dbo.arquivo_versao_tb    
  --- para incluir na  controle_proposta_db..arquivo_versao_gerado_tb   
 insert controle_proposta_db.dbo.arquivo_versao_tb   
 (layout_id, versao, dt_processamento, dt_inclusao, usuario)  
 values  
 (@layout_id ,@num_remessa, @dt_geracao_arquivo, getdate(), @usuario )  
  
  
  insert into controle_proposta_db..arquivo_versao_gerado_tb   
  ( layout_id, versao, qtd_registros, qtd_linhas, dt_inclusao, usuario )   
  values   
   (@layout_id ,@num_remessa , @qtdelinhas - 2 ,@qtdelinhas , getdate() ,@usuario )   
end  
  
--------------------------------------------------------------------    
--Objetivo: Incluir em evento_impress�o_tb os documentos, em lote,    
--          da tabela #evento_impressao.    
--------------------------------------------------------------------    
  
-- cristovao.rodrigues 28/07/2015  
-- obs. ja existe atualizacao do evento na evento_seguros_db..evento_impressao_tb para as propostas,   
-- vinculado a um outro SEGA gerado  
--  
------ EXEC seguros_db..sp_helptext SEGS6765_SPI @layout_id,    
--EXEC desenv_db.dbo.SEGS_PosProcessamento_EventoImpressao_SPI  ---- 28/07/2015 comentada  
--     @layout_id,    
--     @arquivo_remessa_grf,  
--     @dt_geracao_arquivo,  
--     @num_remessa,  
--     @producao,  
--     @usuario  
  
---AKIO OKUNO - 23/09/2015 - VAIDA��O DE CERTIFICADOS J� EMITIDOS - INICIO  
  
CREATE TABLE #evento_impressao            
  (proposta_id INT,            
   proposta_bb NUMERIC(9,0),            
   usuario_aprovacao VARCHAR(10),            
   qtd_vias INT,            
   num_remessa INT,            
   endosso_id INT,            
   num_solicitacao INT,            
   sinistro_id NUMERIC(11,0),            
   num_cobranca INT,            
   dt_aprovacao SMALLDATETIME,            
   tipo_emissao INT,            
   apolice_id INT)            
               
INSERT INTO #evento_impressao            
     (endosso_id,            
      proposta_id,            
      num_solicitacao,            
      sinistro_id,            
      usuario_aprovacao,            
      dt_aprovacao,            
      qtd_vias,            
      num_cobranca,            
      num_remessa)            
SELECT 0,  
      proposta_id,            
      0,            
      sinistro_id = NULL,            
      usuario_aprovacao = CAST(NULL AS VARCHAR(20)),            
      dt_aprovacao = CAST(NULL AS SMALLDATETIME),            
      qtd_vias = NULL,            
      num_cobranca,            
      @num_remessa            
 FROM Interface_dados_db..SEGA9209_processar_tb WITH (NOLOCK)      
  
  
EXEC evento_seguros_db..evento_impressao_generica_spi 0     --@diretoria_id,                
                                                    , Null    -- @usuario_solicitante,            
                                                    , 13    -- @tp_documento_id,            
                                                    , 'i'    -- @status,            
                                                    , Null    -- @motivo,            
                                                    , 0     -- @sub_grupo_id,            
                                                    , 'T'    -- @documento -- 'T' DE CARTA,          
                                                    , Null    -- @dt_solicitacao,            
                 , 'C'    -- @destino,           
                                                    , ''    -- @local_destino,            
                                                    , ''    -- @local_contato,            
                                                    , @dt_geracao_arquivo            
                                                    , @arquivo_remessa_grf  
                                                    , @usuario  
                                                    , @producao            
  
  
  
UPDATE evento_seguros_db..evento_impressao_tb            
  SET dt_geracao_arquivo = @dt_geracao_arquivo           
    , arquivo_remessa_grf = @arquivo_remessa_grf       
    , status = 'i'     
    , dt_alteracao = GETDATE()  
    , usuario = @usuario     --select *        
 FROM evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)            
 JOIN #evento_impressao         
   ON evento_impressao_tb.proposta_id = #evento_impressao.proposta_id            
  and evento_impressao_tb.Num_Cobranca = #evento_impressao.Num_Cobranca   
     
WHERE evento_impressao_tb.num_solicitacao <> 0            
  
---AKIO OKUNO - 23/09/2015 - VAIDA��O DE CERTIFICADOS J� EMITIDOS - FIM]  
  
  
-- Transferindo as informa��es processadas -------------------------------------------    
--Processado    
  
insert into interface_dados_db.dbo.SEGA9209_processado_tb  
 ( SEGA9209_processado_id      
  ,proposta_id         
  ,proposta_bb         
  ,cliente_id         
  ,apolice_id         
  ,agencia_id    
  ,cont_banco_id       
  ,nome_agencia        
  ,ramo_id          
  ,produto_id         
  ,seguradora_cod_susep      
  ,sucursal_seguradora_id      
  ,layout_id         
  ,num_cobranca        
  ,dt_agendamento        
  ,val_cobranca        
  ,remessa          
  ,dt_inclusao  
  ,usuario   
 )  
select   
 SEGA9209_processar_id      
 ,proposta_id         
 ,proposta_bb         
 ,cliente_id         
 ,apolice_id         
 ,agencia_id   
 ,cont_banco_id        
 ,nome_agencia        
 ,ramo_id          
 ,produto_id         
 ,seguradora_cod_susep      
 ,sucursal_seguradora_id      
 ,layout_id         
 ,num_cobranca        
 ,dt_agendamento        
 ,val_cobranca  
 ,@num_remessa          
 ,getdate()  
 ,@usuario   
from interface_dados_db.dbo.SEGA9209_processar_tb  
  
  
--Detalhe 10    
insert into interface_dados_db.dbo.SEGA9209_10_Processado_TB  
 (SEGA9209_10_processado_id     
 ,SEGA9209_processado_id       
 ,proposta_id         
 ,nome_segurado        
 ,endereco         
 ,bairro          
 ,cidade          
 ,UF            
 ,CEP           
 ,numero_agencia        
 ,nome_agencia        
 ,endereco_agencia       
 ,bairro_agencia        
 ,cidade_agencia        
 ,UF_agencia          
 ,CEP_agencia       
 ,dt_inclusao  
 ,usuario  
 ,produto_id --- 20/08/2015  
 ,cod_retorno )  
select   
 SEGA9209_10_processar_id   
 ,SEGA9209_processar_id    
 ,proposta_id       
 ,nome_segurado      
 ,endereco       
 ,bairro        
 ,cidade        
 ,UF         
 ,CEP         
 ,numero_agencia      
 ,nome_agencia      
 ,endereco_agencia     
 ,bairro_agencia      
 ,cidade_agencia      
 ,UF_agencia       
 ,CEP_agencia       
 ,getdate()  
 ,@usuario  
 ,produto_id --- 20/08/2015  
 ,cod_retorno    
from interface_dados_db.dbo.SEGA9209_10_Processar_TB  
  
--Detalhe 20    
insert into interface_dados_db.dbo.SEGA9209_20_Processado_TB   
 (  
 SEGA9209_20_processado_id   
 ,SEGA9209_10_processado_id    
 ,produto_id       
 ,nome_produto      
 ,numero_apolice      
 ,numero_endosso      
 ,proposta_id      
 ,proposta_bb      
 ,num_prim_parcela     
 ,dt_venc_prim_parcela    
 ,valor_receb_prim_parcela   
 ,dt_emissao_carta     
 ,dt_inclusao  
 ,usuario
 ,ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
 ,celular  --migracao-documentacao-digital-2a-fase-cartas
 ,email   --migracao-documentacao-digital-2a-fase-cartas        
 ,ramo_id --migracao-documentacao-digital-2a-fase-cartas 
 ,cpf_cnpj )
select   
 SEGA9209_20_processar_id   
 ,SEGA9209_10_processar_id    
 ,produto_id       
 ,nome_produto      
 ,numero_apolice      
 ,numero_endosso      
 ,proposta_id       
 ,proposta_bb       
 ,num_prim_parcela     
 ,dt_venc_prim_parcela    
 ,valor_receb_prim_parcela   
 ,dt_emissao_carta     
 ,getdate()  
 ,@usuario
 ,ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
 ,celular  --migracao-documentacao-digital-2a-fase-cartas
 ,email   --migracao-documentacao-digital-2a-fase-cartas   
 ,ramo_id --migracao-documentacao-digital-2a-fase-cartas   
 ,cpf_cnpj 
from interface_dados_db.dbo.SEGA9209_20_Processar_TB    
  
  
--LIMPA AS TABELAS PROCESSAR    
delete interface_dados_db.dbo.SEGA9209_20_Processar_TB    
delete interface_dados_db.dbo.SEGA9209_10_Processar_TB    
delete interface_dados_db.dbo.SEGA9209_Processar_TB    
  
SET NOCOUNT OFF    
  
RETURN                      
                      
end try  
                      
begin catch     
    
declare @errormessage nvarchar(4000)            
declare @errorseverity int            
declare @errorstate int      
      
   select    
                        @errormessage = error_procedure() + ' - linha ' + convert(varchar(15),error_line()) + ' - ' + error_message(),            
                        @errorseverity = error_severity(),            
                        @errorstate = error_state()            
      
            raiserror (    
                        @errormessage,            
                        @errorseverity,            
                        @errorstate )      
    
end catch               
      
      
  
  

