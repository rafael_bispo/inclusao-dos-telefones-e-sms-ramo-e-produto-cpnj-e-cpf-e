CREATE PROCEDURE SEGS10187_SPI  
  
    @usuario VARCHAR(20),  
 @produto_id INT = null  
AS                              
  
-----------------------------------------------------------                              
-- Procedure Detalhe 20 - Segurado SEGA9155             --                              
-----------------------------------------------------------                              
  
SET NOCOUNT ON                              
    
DECLARE @msg VARCHAR(100)     
  
SELECT *  
  INTO #SEGA9155_PROCESSAR  
  FROM interface_dados_db..SEGA9155_processar_tb  
 WHERE (@produto_id IS NULL or produto_id = @produto_id)  
  
  
-- Vida  
SELECT SEGA9155.SEGA9155_processar_id,  
    SEGA9155.proposta_id,  
    SEGA9155.ramo_id,  
    SEGA9155.produto_id,  
    SEGA9155.apolice_id,  
    SEGA9155.ano_documento ano_exercicio,  
    c.nome,  
    c.cpf_cnpj,
    ISNULL(c.ddd_1,'') as ddd_celular,
    ISNULL(c.telefone_1,'') as celular,
    ISNULL(c.e_mail,'') as email,     
    ISNULL(endereco_corresp.endereco, '') endereco,  
    ISNULL(endereco_corresp.bairro, '') bairro,     
       ISNULL(endereco_corresp.municipio, '') cidade,        
       ISNULL(endereco_corresp.estado, '') UF,  
    ISNULL(endereco_corresp.cep, '') CEP,  
    getdate() dt_geracao,  
    'n' identificado_envio,  
    'AB' tipo_ramo,  
    1 numero_via,  
    CAST('' as text) OBS,
    CAST('' AS VARCHAR(14)) as cpf_cnpj  
  INTO #SEGA9155_20  
  FROM #SEGA9155_PROCESSAR SEGA9155(NOLOCK)  
  JOIN proposta_tb p(NOLOCK)  
 ON p.proposta_id = SEGA9155.proposta_id  
  JOIN ramo_tb ramo(NOLOCK)  
 ON ramo.ramo_id = SEGA9155.ramo_id  
   AND ramo.tp_ramo_id = 1 -- vida  
  JOIN cliente_tb c (NOLOCK)  
    ON c.cliente_id = p.prop_cliente_id     
  JOIN endereco_corresp_tb endereco_corresp(NOLOCK)  
 ON endereco_corresp.proposta_id = p.proposta_id  
  
IF @@ERROR <> 0       
BEGIN                                  
    SELECT @msg = 'Erro ao Criar a tabela #SEGA9155_20'                       
    GOTO error                                  
END       


-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #SEGA9155_20
	set #SEGA9155_20.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #SEGA9155_20 #SEGA9155_20
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #SEGA9155_20.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id

IF @@ERROR <> 0       
BEGIN                                  
    SELECT @msg = 'Erro ao incluir cpf_cnpj'                       
    GOTO error                                  
END                   
  
SELECT a.proposta_id, YEAR(a.dt_agendamento) AS ano, count(a.num_cobranca) qtd_cobranca  
  INTO #propostas_ano  
  FROM #SEGA9155_PROCESSAR SEGA9155  
  JOIN agendamento_cobranca_atual_tb a  
 ON a.proposta_id = sega9155.proposta_id  
 WHERE sega9155.ano_documento = 2011  
   AND a.canc_endosso_id IS NULL  
 GROUP BY a.proposta_id, YEAR(a.dt_agendamento)  
HAVING YEAR(a.dt_agendamento) BETWEEN '2009' AND '2010'  
  
IF @@ERROR <> 0                                
BEGIN                                  
    SELECT @msg = 'Erro ao Criar registros na tabela #propostas_ano'                       
    GOTO error                                  
END                       
   
-- Deletar as propostas   
DELETE #propostas_ano  
  FROM #propostas_ano propAno  
  JOIN agendamento_cobranca_atual_tb a  
 ON a.proposta_id = propAno.proposta_id  
   AND YEAR(a.dt_agendamento) = propAno.ano  
   AND a.canc_endosso_id IS NULL  
where (a.dt_baixa IS NULL or   
    DATEDIFF(m,a.dt_baixa,getdate()) < 1)   
  
IF @@ERROR <> 0                                
BEGIN                                  
    SELECT @msg = 'Erro ao Deletar as propostas n�o pagas da tabela #propostas_ano'  
    GOTO error                                  
END      
  
-- Cursor para gerar a OBS  
DECLARE @frase varchar(100)  
DECLARE @proposta_id INT  
DECLARE proposta_id_cursor CURSOR STATIC FOR  
SELECT proposta_id FROM #propostas_ano group by proposta_id  
  
DECLARE @proposta_id_aux INT  
  
OPEN proposta_id_cursor  
  
FETCH NEXT FROM proposta_id_cursor INTO  @proposta_id  
WHILE @@FETCH_STATUS = 0  
BEGIN  
  
 SET @frase = ''   
  
     SELECT @frase = @frase + ', ' + CAST(ano AS VARCHAR(4))  
       FROM #propostas_ano  
   WHERE proposta_id = @proposta_id  
   ORDER BY ano   
 UPDATE #SEGA9155_20   
 SET OBS = SUBSTRING(@frase, 2, LEN(@frase)) + ' e'  
 where proposta_id = @proposta_id   
  
 FETCH NEXT FROM proposta_id_cursor INTO  @proposta_id  
END  
CLOSE proposta_id_cursor  
DEALLOCATE proposta_id_cursor  
  
INSERT INTO interface_dados_db..SEGA9155_20_processar_tb                              
       (SEGA9155_processar_id,  
  proposta_id,  
  apolice_id,  
  cod_ramo,  
  cod_produto,  
  ano_exercicio,  
  estipulante,  
  cnpj_cpf,
  ddd_celular,
  celular,
  email, 
  endereco,  
  bairro,  
  cidade,  
  estado,  
  cep,  
  observacao,  
  dt_inclusao,  
  usuario)       
SELECT  SEGA9155_processar_id,  
  proposta_id,  
  apolice_id,  
  ramo_id,  
  produto_id,  
  ano_exercicio,  
  nome,  
  cpf_cnpj,
  ddd_celular,
  celular,
  email,  
  endereco,  
  bairro,  
  cidade,  
  uf,  
  cep,  
  obs,  
  getdate() dt_inclusao,  
        @usuario usuario    
  FROM #SEGA9155_20  
  
IF @@ERROR <> 0                                
BEGIN                                  
    SELECT @msg = 'Erro ao inserir registros em interface_dados_db..SEGA9155_20_processar_tb'                                
    GOTO error                                  
END  
  
SET NOCOUNT OFF                                
  
RETURN                                
  
error:                                  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Inicio --  
    --raiserror 55555 @msg   
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Fim --  
  
  
  
  
  

