CREATE PROCEDURE SEGS10183_SPI  
      
    @usuario VARCHAR(20),      
    @num_versao INT      
      
AS      
      
SET NOCOUNT ON      
      
DECLARE @msg VARCHAR(100)    
--Copiando dados para SEGA9155_processado_tb    
INSERT INTO interface_dados_db..SEGA9155_processado_tb  
      (SEGA9155_processar_id,  
  proposta_id,  
  produto_id,  
  apolice_id,  
  ramo_id,  
  seguradora_cod_susep,  
  sucursal_seguradora_id,  
  ano_documento,  
  tp_documento_id,  
  descricao_documento,  
  layout_id,  
  num_solicitacao,  
  num_versao,  
  dt_inclusao,  
  usuario)      
SELECT  SEGA9155_processar_id,  
  proposta_id,  
  produto_id,  
  apolice_id,  
  ramo_id,  
  seguradora_cod_susep,  
  sucursal_seguradora_id,  
  ano_documento,  
  tp_documento_id,  
  descricao_documento,  
  layout_id,  
  num_solicitacao,  
     @num_versao as num_versao,  
     GETDATE() as dt_inclusao,  
     @usuario as usuario  
  FROM interface_dados_db..SEGA9155_processar_tb      
      
IF @@ERROR <> 0      
BEGIN      
    SELECT @msg = 'Erro na inclus�o em SEGA9155_processado_tb'      
    GOTO error      
END  
  
INSERT INTO interface_dados_db..SEGA9155_10_processado_tb      
      (SEGA9155_10_processar_id,  
    SEGA9155_processar_id,  
    proposta_id,  
    nome_destinatario,  
    endereco_destinatario,  
    municipio_destinatario,  
    UF_destinatario,  
    CEP_destinatario,  
    cod_retorno,  
    tipo_documento,  
    descricao_documento,  
    dt_inclusao,  
    usuario)  
     
SELECT SEGA9155_10_processar_id,  
    SEGA9155_processar_id,  
    proposta_id,  
    nome_destinatario,  
    endereco_destinatario,  
    municipio_destinatario,  
    UF_destinatario,  
    CEP_destinatario,  
    cod_retorno,  
    tipo_documento,  
    descricao_documento,  
    GETDATE() dt_inclusao,  
    @usuario as usuario  
 FROM interface_dados_db..SEGA9155_10_processar_tb      
      
IF @@ERROR <> 0      
BEGIN      
    SELECT @msg = 'Erro na inclus�o em SEGA9155_10_processado_tb'      
    GOTO error      
END  
  
--Copiando dados para SEGA9155_20_processado_tb      
INSERT INTO interface_dados_db..SEGA9155_20_processado_tb      
    (SEGA9155_20_processar_id,  
  SEGA9155_processar_id,  
  proposta_id,  
  apolice_id,  
  cod_ramo,  
  cod_produto,  
  ano_exercicio,  
  estipulante,  
  cnpj_cpf,
  ddd_celular,
  celular,
  email, 
  cpf_cnpj,
  endereco,  
  bairro,  
  cidade,  
  estado,  
  cep,  
  observacao,  
  dt_inclusao,  
  usuario)     
      
SELECT  SEGA9155_20_processar_id,  
  SEGA9155_processar_id,  
  proposta_id,  
  apolice_id,  
  cod_ramo,  
  cod_produto,  
  ano_exercicio,  
  estipulante,  
  cnpj_cpf,
  ddd_celular,
  celular,
  email,  
  cpf_cnpj,
  endereco,  
  bairro,  
  cidade,  
  estado,  
  cep,  
  observacao,  
     GETDATE() as dt_inclusao,      
        @usuario as usuario     
  FROM interface_dados_db..SEGA9155_20_processar_tb      
      
IF @@ERROR <> 0      
BEGIN      
    SELECT @msg = 'Erro na inclus�o em SEGA9155_20_processado_tb'      
    GOTO error      
END  
   
-- Excluindo os registros das tabelas processar      
DELETE FROM interface_dados_db..SEGA9155_10_processar_tb   
DELETE FROM interface_dados_db..SEGA9155_20_processar_tb  
DELETE FROM interface_dados_db..SEGA9155_processar_tb      
      
IF @@ERROR <> 0      
BEGIN      
    SELECT @msg = 'Erro excluindo os registros das tabelas processar'      
    GOTO error      
END      
      
SET NOCOUNT OFF  
      
RETURN      
      
error:          
  
   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Fim --  
  
  
  
  
  

