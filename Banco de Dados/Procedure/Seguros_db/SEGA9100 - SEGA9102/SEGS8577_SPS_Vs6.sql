CREATE PROCEDURE Segs8577_sps @num_remessa_9100 VARCHAR(04),  
                              @num_remessa_9102 VARCHAR(04),  
                              @usuario          UD_USUARIO,  
                              @data             DATETIME,  
                              @cFinalizaArquivo CHAR(2) = 'N'  
AS  
    DECLARE @msg VARCHAR(200)  
  
    SET NOCOUNT ON  
  
    IF @cFinalizaArquivo = 'N'  --Rafael Inacio - RF00285030 - Gravar registros na base ap�s gera��o do arquivo f�sico
      BEGIN  
          --exclus�o de tabelas tempor�rias, se existirem                                
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '#TEMP_PRODUTO')  
            DROP TABLE #TEMP_PRODUTO  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##evento_proposta_2aVia')  
            DROP TABLE ##evento_proposta_2aVia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##evento_proposta_1aVia')  
            DROP TABLE ##evento_proposta_1aVia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '#tp_avaliacao_tb')  
            DROP TABLE #tp_avaliacao_tb  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '#proposta_tb_1')  
            DROP TABLE #proposta_tb_1  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '#evento_impressao_tb')  
            DROP TABLE #evento_impressao_tb  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##retorno_Fonte_Arquivo')  
            DROP TABLE ##retorno_Fonte_Arquivo  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ObtemDadosProduto')  
            DROP TABLE ##ObtemDadosProduto  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ObtemDadosProposta')  
            DROP TABLE ##ObtemDadosProposta  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ObtemDadosAgencia')  
            DROP TABLE ##ObtemDadosAgencia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##Propostas_Produto_arqBB')  
            DROP TABLE ##Propostas_Produto_arqBB  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ObtemEnderecoAgencia')  
            DROP TABLE ##ObtemEnderecoAgencia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##GravaLogAgencia')  
            DROP TABLE ##GravaLogAgencia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ObtemPropostaBB')  
            DROP TABLE ##ObtemPropostaBB  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##Dados_Agencia')  
            DROP TABLE ##Dados_Agencia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_B1')  
            DROP TABLE ##DETALHE_Segurado_B1  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_B2')  
            DROP TABLE ##DETALHE_Segurado_B2  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_B3')  
            DROP TABLE ##DETALHE_Segurado_B3  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##retorno_Fonte_Arquivo_temp')  
            DROP TABLE ##retorno_Fonte_Arquivo_temp  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_B1')  
            DROP TABLE ##DETALHE_CtrlDoc_B1  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_B2')  
            DROP TABLE ##DETALHE_CtrlDoc_B2  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_B3')  
            DROP TABLE ##DETALHE_CtrlDoc_B3  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##evento_impressao')  
            DROP TABLE ##evento_impressao  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ImpressaoSegundaVia')  
            DROP TABLE ##ImpressaoSegundaVia  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##arquivo')  
            DROP TABLE ##arquivo  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_SEGA9100')  
            DROP TABLE ##DETALHE_CtrlDoc_SEGA9100  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_SEGA9102')  
            DROP TABLE ##DETALHE_CtrlDoc_SEGA9102  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_CtrlDoc_SEGA8134')  
            DROP TABLE ##DETALHE_CtrlDoc_SEGA8134  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_SEGA9100')  
            DROP TABLE ##DETALHE_Segurado_SEGA9100  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_SEGA9102')  
            DROP TABLE ##DETALHE_Segurado_SEGA9102  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##DETALHE_Segurado_SEGA8134')  
            DROP TABLE ##DETALHE_Segurado_SEGA8134  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##arquivo_9100')  
            DROP TABLE ##arquivo_9100  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##arquivo_9102')  
            DROP TABLE ##arquivo_9102  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##arquivo_8134')  
            DROP TABLE ##arquivo_8134  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##corretor_independ')  
            DROP TABLE ##corretor_independ  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ESTATISTICAS')  
            DROP TABLE ##ESTATISTICAS  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ESTATISTICAS_1')  
            DROP TABLE ##ESTATISTICAS_1  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##ESTATISTICAS_2')  
            DROP TABLE ##ESTATISTICAS_2  
  
          SELECT DISTINCT produto_id = produto_tb.produto_id  
          INTO   #TEMP_PRODUTO  
          FROM   produto_tb WITH (NOLOCK)  
                 INNER JOIN item_produto_tb WITH (NOLOCK)  
                         ON produto_tb.produto_id = item_produto_tb.produto_id  
                 INNER JOIN ramo_tb WITH (NOLOCK)  
                         ON ramo_tb.ramo_id = item_produto_tb.ramo_id  
                 INNER JOIN produto_tp_carta_tb WITH (NOLOCK)  
                         ON produto_tb.produto_id = produto_tp_carta_tb.produto_id  
          WHERE  produto_tb.ativo = 's'  
                 AND ramo_tb.tp_ramo_id = 2  
                 AND item_produto_tb.participacao = 'm'  
                 AND produto_tp_carta_tb.tp_carta_id = 12 --Recusa                                                        
                 AND produto_tp_carta_tb.dt_fim_vigencia IS NULL  
  
          --Recusa T�cnica                                
          SELECT tp_avaliacao_id,  
                 nome,  
                 origem  
          INTO   #tp_avaliacao_tb  
          FROM   als_produto_db..tp_avaliacao_tb WITH (NOLOCK)  
          WHERE  origem = 'T'  
  
          --proposta_tb, para as 2 primeiras querys                       
          SELECT proposta_id,  
                 situacao,  
                 prop_cliente_id,  
                 produto_id,  
                 dt_contratacao,  
                 subramo_id, 
                 ramo_id --migracao-documentacao-digital-2a-fase-cartas 
          INTO   #proposta_tb_1  
          FROM   proposta_tb WITH (NOLOCK)  
          WHERE  situacao = 'r'  
  
          --evento_impressao_tb, para as 2 ultimas querys                                
          SELECT destino,  
                 num_solicitacao,  
                 proposta_id,  
                 LEFT(arquivo_remessa_grf, 08) AS arquivo  
          INTO   #evento_impressao_tb  
          FROM   evento_seguros_db..evento_impressao_tb WITH (NOLOCK)  
          WHERE  status = 'l'  
                 AND dt_geracao_arquivo IS NULL  
                 AND tp_documento_id = 12  
  
          ----------------------------------------------------------------                                
          -------- Fonte de dados dos Arquivos  --------                              
          --- a fonte de dados abaixo ser� usada para a gera��o dos                                 
          --- arquivos SEGA9100, SEGA9102, SEGA8134                                
          ----------------------------------------------------------------                                
          --# VIDA                                
          SELECT proposta_tb.proposta_id,  
                 proposta_tb.situacao,  
                 segurado   = cliente_tb.nome,  
                 endereco_corresp_tb.endereco,  
                 endereco_corresp_tb.bairro,  
                 endereco_corresp_tb.municipio,  
                 endereco_corresp_tb.cep,  
                 endereco_corresp_tb.estado,  
                 proposta_tb.dt_contratacao,  
                 proposta_tb.produto_id,  
                 avaliacao_proposta_tb.dt_avaliacao,  
                 tp_avaliacao_tb.nome,  
                 tp_avaliacao_tb.origem,  
                 NULL       destino,  
                 '1'        tp_doc,  
                 NULL       num_solicitacao,  
                 tp_avaliacao_tb.tp_avaliacao_id,  
                 'ORIGINAL' Tipo_Documento,  
                 '12'       TipoDocumentoRetorno,  
                 proposta_tb.subramo_id,  
                 NULL       arquivo,
                 isnull(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
                 proposta_tb.ramo_id, --migracao-documentacao-digital-2a-fase-cartas
                 CAST('' AS VARCHAR(14)) as cpf_cnpj
          INTO   ##retorno_Fonte_Arquivo_temp  
          FROM   #proposta_tb_1 proposta_tb WITH (NOLOCK)  
                 INNER JOIN cliente_tb WITH (NOLOCK)  
                         ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
                 INNER JOIN endereco_corresp_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
                 INNER JOIN avaliacao_proposta_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = avaliacao_proposta_tb.proposta_id  
                            AND avaliacao_proposta_tb.num_avaliacao = (SELECT Max (av.num_avaliacao)  
                                                                       FROM   avaliacao_proposta_tb av WITH (NOLOCK)  
                                                                       WHERE  av.proposta_id = proposta_tb.proposta_id)  
                 INNER JOIN #tp_avaliacao_tb tp_avaliacao_tb WITH (NOLOCK)  
   ON avaliacao_proposta_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id  
          WHERE  proposta_tb.produto_id IN ( 11, 15, 121, 135,  
                                             716, 722, 14 ) --flow 487560 inclusao dos produtos 722 e 14                               
                 AND ( Isnull(endereco_corresp_tb.emite_documento, 's') = 's'  
                        OR ( endereco_corresp_tb.emite_documento = 'a'  
                             AND endereco_corresp_tb.situacao IN ( 'c', 'v' ) ) )  
                 AND NOT EXISTS(SELECT 1  
                                FROM   evento_seguros_db..evento_impressao_tb imp WITH (NOLOCK)  
                                WHERE  proposta_tb.proposta_id = imp.proposta_id  
                                       AND imp.tp_documento_id IN ( 12, 34, 35 ))  
                 AND ( avaliacao_proposta_tb.dt_avaliacao >= '20040501'  
                       AND proposta_tb.produto_id NOT IN ( 722, 14 )  
                        OR ( avaliacao_proposta_tb.dt_avaliacao >= '20080901'  
                             AND proposta_tb.produto_id IN ( 722, 14 ) ) )  
                 /* INICIO    
                 FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
                 Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
                 */  
                 AND Isnull(endereco_corresp_tb.endereco, '') <> ''  
                 AND Isnull(endereco_corresp_tb.cep, '00000000') <> '00000000'  
                 AND Isnull(endereco_corresp_tb.estado, 'xx') <> 'xx'  
          /* FIM    
          FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
          Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
          */  
          UNION  
          --#RE                                
          SELECT proposta_tb.proposta_id,  
                 proposta_tb.situacao,  
                 segurado   = cliente_tb.nome,  
                 endereco_corresp_tb.endereco,  
                 endereco_corresp_tb.bairro,  
                 endereco_corresp_tb.municipio,  
                 endereco_corresp_tb.cep,  
                 endereco_corresp_tb.estado,  
                 proposta_tb.dt_contratacao,  
                 proposta_tb.produto_id,  
                 avaliacao_proposta_tb.dt_avaliacao,  
                 tp_avaliacao_tb.nome,  
                 tp_avaliacao_tb.origem,  
                 NULL       destino,  
                 '2'        tp_doc,  
                 NULL       num_solicitacao,  
                 tp_avaliacao_tb.tp_avaliacao_id,  
                 'ORIGINAL' Tipo_Documento,  
                 '12'       TipoDocumentoRetorno,  
                 proposta_tb.subramo_id,  
                 NULL       arquivo, 
                 isnull(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
                 proposta_tb.ramo_id, --migracao-documentacao-digital-2a-fase-cartas 
                 CAST('' AS VARCHAR(14)) as cpf_cnpj
          FROM   #proposta_tb_1 proposta_tb WITH (NOLOCK)  
                 INNER JOIN cliente_tb WITH (NOLOCK)  
                         ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
                 INNER JOIN endereco_corresp_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
                 INNER JOIN avaliacao_proposta_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = avaliacao_proposta_tb.proposta_id  
                            AND avaliacao_proposta_tb.num_avaliacao = (SELECT Max (av.num_avaliacao)  
                                                                       FROM   avaliacao_proposta_tb av WITH (NOLOCK)  
                                                                       WHERE  av.proposta_id = proposta_tb.proposta_id)  
                 INNER JOIN #tp_avaliacao_tb tp_avaliacao_tb WITH (NOLOCK)  
                         ON avaliacao_proposta_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id  
                 INNER JOIN #TEMP_PRODUTO T  
                         ON proposta_tb.produto_id = T.produto_id  
          WHERE  ( endereco_corresp_tb.emite_documento IS NULL  
                    OR endereco_corresp_tb.emite_documento = 'a' )  
                 AND NOT EXISTS (SELECT 1  
                                 FROM   evento_seguros_db..evento_impressao_tb imp WITH (NOLOCK)  
                                 WHERE  proposta_tb.proposta_id = imp.proposta_id  
                                        AND imp.tp_documento_id IN ( 12, 34, 35 ))  
                 AND avaliacao_proposta_tb.dt_avaliacao >= '20040420'  
                 /* INICIO    
                 FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
                 Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
                 */  
                 AND Isnull(endereco_corresp_tb.endereco, '') <> ''  
                 AND Isnull(endereco_corresp_tb.cep, '00000000') <> '00000000'  
                 AND Isnull(endereco_corresp_tb.estado, 'xx') <> 'xx'  
          /* FIM    
          FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
          Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
          */  
          UNION  
          --#VIDA - 2�VIA                                
          SELECT proposta_tb.proposta_id,  
                 proposta_tb.situacao,  
                 segurado  = cliente_tb.nome,  
                 endereco_corresp_tb.endereco,  
                 endereco_corresp_tb.bairro,  
                 endereco_corresp_tb.municipio,  
                 endereco_corresp_tb.cep,  
                 endereco_corresp_tb.estado,  
                 proposta_tb.dt_contratacao,  
                 proposta_tb.produto_id,  
                 avaliacao_proposta_tb.dt_avaliacao,  
                 tp_avaliacao_tb.nome,  
                 tp_avaliacao_tb.origem,  
                 #evento_impressao_tb.destino,  
                 '3'       tp_doc,  
                 #evento_impressao_tb.num_solicitacao,  
                 tp_avaliacao_tb.tp_avaliacao_id,  
                 'SEGUNDA' Tipo_Documento,  
                 '12'      TipoDocumentoRetorno,  
                 proposta_tb.subramo_id,  
                 #evento_impressao_tb.arquivo,
                 isnull(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
                 proposta_tb.ramo_id, --migracao-documentacao-digital-2a-fase-cartas  
                 CAST('' AS VARCHAR(14)) as cpf_cnpj
          FROM   #evento_impressao_tb WITH (NOLOCK)  
                 INNER JOIN proposta_tb WITH (NOLOCK)  
                         ON #evento_impressao_tb.proposta_id = proposta_tb.proposta_id  
                 INNER JOIN cliente_tb WITH (NOLOCK)  
                         ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
                 INNER JOIN endereco_corresp_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
                 INNER JOIN avaliacao_proposta_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = avaliacao_proposta_tb.proposta_id  
                            AND avaliacao_proposta_tb.num_avaliacao = (SELECT Max (av.num_avaliacao) - 1  
                                                                       FROM   avaliacao_proposta_tb av WITH (NOLOCK)  
                                                                       WHERE  av.proposta_id = proposta_tb.proposta_id)  
                 INNER JOIN #tp_avaliacao_tb tp_avaliacao_tb WITH (NOLOCK)  
                         ON avaliacao_proposta_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id  
          WHERE  proposta_tb.produto_id IN ( 11, 15, 121, 135,  
                                             716, 722, 14 )  
                 /* INICIO    
                 FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
                 Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
                 */  
                 AND Isnull(endereco_corresp_tb.endereco, '') <> ''  
                 AND Isnull(endereco_corresp_tb.cep, '00000000') <> '00000000'  
                 AND Isnull(endereco_corresp_tb.estado, 'xx') <> 'xx'  
          /* FIM    
          FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
          Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
          */  
          UNION  
          --'#RE - 2�VIA                                
          SELECT proposta_tb.proposta_id,  
                 proposta_tb.situacao,  
                 segurado  = cliente_tb.nome,  
                 endereco_corresp_tb.endereco,  
                 endereco_corresp_tb.bairro,  
                 endereco_corresp_tb.municipio,  
                 endereco_corresp_tb.cep,  
                 endereco_corresp_tb.estado,  
                 proposta_tb.dt_contratacao,  
                 proposta_tb.produto_id,  
                 avaliacao_proposta_tb.dt_avaliacao,  
                 tp_avaliacao_tb.nome,  
                 tp_avaliacao_tb.origem,  
                 #evento_impressao_tb.destino,  
                 '4'       tp_doc,  
                 #evento_impressao_tb.num_solicitacao,  
                 tp_avaliacao_tb.tp_avaliacao_id,  
                 'SEGUNDA' Tipo_Documento,  
                 '12'      TipoDocumentoRetorno,  
                 proposta_tb.subramo_id,  
                 #evento_impressao_tb.arquivo,
                 isnull(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
                 isnull(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
                 proposta_tb.ramo_id, --migracao-documentacao-digital-2a-fase-cartas
                 CAST('' AS VARCHAR(14)) as cpf_cnpj
          FROM   #evento_impressao_tb WITH (NOLOCK)  
                 INNER JOIN proposta_tb WITH (NOLOCK)  
                         ON #evento_impressao_tb.proposta_id = proposta_tb.proposta_id  
                 INNER JOIN cliente_tb WITH (NOLOCK)  
                         ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
                 INNER JOIN endereco_corresp_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id  
                 INNER JOIN avaliacao_proposta_tb WITH (NOLOCK)  
                         ON proposta_tb.proposta_id = avaliacao_proposta_tb.proposta_id  
                            AND avaliacao_proposta_tb.num_avaliacao = (SELECT Max (av.num_avaliacao) - 1  
                                                                       FROM   avaliacao_proposta_tb av WITH (NOLOCK)  
                                                                       WHERE  av.proposta_id = proposta_tb.proposta_id)  
                 INNER JOIN #tp_avaliacao_tb tp_avaliacao_tb WITH (NOLOCK)  
                         ON avaliacao_proposta_tb.tp_avaliacao_id = tp_avaliacao_tb.tp_avaliacao_id  
                 INNER JOIN #TEMP_PRODUTO T  
                         ON proposta_tb.produto_id = T.produto_id  
                            /* INICIO    
                            FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
                            Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
                            */  
                            AND Isnull(endereco_corresp_tb.endereco, '') <> ''  
                            AND Isnull(endereco_corresp_tb.cep, '00000000') <> '00000000'  
                            AND Isnull(endereco_corresp_tb.estado, 'xx') <> 'xx'  
  
          /* FIM    
          FLOW CORRETIVO N� 1312040 - EDSON GOMES - GPTI - 27/10/2009    
          Adi��o de filtro para n�o enviar � Gr�fica propostas sem endere�o de correspond�ncia.    
          */  
          
          
          -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
		update ##retorno_Fonte_Arquivo_temp
		set ##retorno_Fonte_Arquivo_temp.CPF_CNPJ = CASE 
													WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
													WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
													ELSE cliente_tb.cpf_cnpj
												END 
		from ##retorno_Fonte_Arquivo_temp ##retorno_Fonte_Arquivo_temp
		inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
			on proposta_tb.proposta_id = ##retorno_Fonte_Arquivo_temp.proposta_id
		inner join seguros_db..cliente_tb cliente_tb with(nolock)
			on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
		left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
			on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
		left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
			on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
		          
          IF @@ROWCOUNT = 0  
            BEGIN  
                SET NOCOUNT OFF  
  
                SELECT 0,  
                       0,  
                       0,  
                       0  
  
                RETURN  
            END  
  
          ---CRIA��O DE TABELA ##retorno_Fonte_Arquivo                                
          ---esta tabela armazenar� fonte de dados dos arquivos SEGA9100 e SEGA9102, ou SEGA8134                                
          SELECT TOP 0 *,  
                       Space(25) num_proc_susep  
          INTO   ##retorno_Fonte_Arquivo  
          FROM   ##retorno_Fonte_Arquivo_temp  
  
          INSERT INTO ##retorno_Fonte_Arquivo  
          SELECT A.*,  
                 CASE  
                   WHEN subramo_tb.num_proc_susep IS NULL  
                         OR subramo_tb.num_proc_susep = '0' THEN produto_tb.num_proc_susep  
                   ELSE subramo_tb.num_proc_susep  
                 END AS num_proc_susep  
          FROM   ##retorno_Fonte_Arquivo_temp A  
                 INNER JOIN corretagem_tb WITH (NOLOCK)  
                         ON corretagem_tb.proposta_id = A.proposta_id  
                 INNER JOIN corretor_tb WITH (NOLOCK)  
                         ON corretor_tb.corretor_id = corretagem_tb.corretor_id  
                            AND corretor_tb.flag_corretor_bb = 'S'  
                 INNER JOIN subramo_tb WITH (NOLOCK)  
                         ON subramo_tb.subramo_id = A.subramo_id  
                            AND ( dt_fim_vigencia_sbr IS NULL  
                                   OR dt_fim_vigencia_sbr >= @data )  
                 INNER JOIN produto_tb WITH (NOLOCK)  
                         ON produto_tb.produto_id = A.produto_id  
  
          -- obten��o dos dados do produto (Sub ObtemDadosProduto, programa SEGP0746)                                
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                                
          SELECT DISTINCT produto_tb.produto_id,  
                          produto_tb.nome,  
                          arquivo_produto_tb.origem  
          -- arquivo_produto_tb.arquivo                                
          INTO   ##ObtemDadosProduto  
          FROM   arquivo_produto_tb WITH (NOLOCK)  
                 INNER JOIN produto_tb WITH (NOLOCK)  
                         ON arquivo_produto_tb.produto_id = produto_tb.produto_id  
          WHERE  ( arquivo_produto_tb.produto_id IN (SELECT DISTINCT produto_id  
                                                     FROM   ##retorno_Fonte_Arquivo)  
                    OR produto_tb.produto_anterior_id IN (SELECT DISTINCT produto_id  
                                                          FROM   ##retorno_Fonte_Arquivo) )  
  
          --- para registros que n�o est�o na tabela criada acima                    
          INSERT INTO ##ObtemDadosProduto  
          SELECT DISTINCT produto_id,  
                          nome,  
                          ''--, ''                                 
          FROM   produto_tb WITH (NOLOCK)  
          WHERE  produto_id IN (SELECT DISTINCT produto_id  
                                FROM   ##retorno_Fonte_Arquivo)  
                 AND produto_id NOT IN (SELECT produto_id  
                                        FROM   ##ObtemDadosProduto)  
  
          ----obten��o de dados das propostas (sub ObtemDadosProposta, programa SEGP0746)                                
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                               
          SELECT ramo_id,  
                 proposta_id  
          INTO   ##ObtemDadosProposta  
          FROM   proposta_basica_tb WITH (NOLOCK)  
          WHERE  proposta_id IN (SELECT DISTINCT proposta_id  
                                 FROM   ##retorno_Fonte_Arquivo)  
  
          --- para registros que n�o est�o na tabela criada acima                                
          INSERT INTO ##ObtemDadosProposta  
          SELECT ramo_id,  
                 proposta_id  
          FROM   proposta_adesao_tb WITH (NOLOCK)  
          WHERE  proposta_id IN (SELECT DISTINCT proposta_id  
                                 FROM   ##retorno_Fonte_Arquivo)  
                 AND proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                         FROM   ##ObtemDadosProposta)  
  
          --- para registros que n�o est�o na tabela criada acima                                
          INSERT INTO ##ObtemDadosProposta  
          SELECT 0,  
                 proposta_id  
          FROM   ##retorno_Fonte_Arquivo WITH (NOLOCK)  
          WHERE  proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                     FROM   ##ObtemDadosProposta)  
  
          ---cria��o de ##Propostas_Produto_arqBB                                
          SELECT DISTINCT proposta_id  
          INTO   ##Propostas_Produto_arqBB  
     FROM   ##retorno_Fonte_Arquivo  
          WHERE  produto_id IN (SELECT produto_id  
                                FROM   ##ObtemDadosProduto  
                                WHERE  Upper(origem) = Ltrim(Rtrim('BB')))  
  
          ----obten��o de dados da agencia (sub ObtemDadosAgencia, programa SEGP0746)                                
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                                
          SELECT proposta_adesao_tb.proposta_id,  
                 proposta_adesao_tb.cont_agencia_id,  
                 agencia_tb.nome,  
                 seguros_db.dbo.Calcula_dv_cc_ou_agencia(proposta_adesao_tb.cont_agencia_id) DV  
          INTO   ##ObtemDadosAgencia  
          FROM   proposta_adesao_tb WITH (NOLOCK)  
                 INNER JOIN agencia_tb WITH (NOLOCK)  
                         ON proposta_adesao_tb.cont_agencia_id = agencia_tb.agencia_id  
                            AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id  
          WHERE  proposta_adesao_tb.proposta_id IN (SELECT DISTINCT proposta_id  
                                                    FROM   ##Propostas_Produto_arqBB)  
  
          --- para registros que n�o est�o na tabela criada acima                                
          INSERT INTO ##ObtemDadosAgencia  
          SELECT proposta_fechada_tb.proposta_id,  
                 proposta_fechada_tb.cont_agencia_id,  
                 agencia_tb.nome,  
                 seguros_db.dbo.Calcula_dv_cc_ou_agencia(proposta_fechada_tb.cont_agencia_id) DV  
          FROM   proposta_fechada_tb WITH (NOLOCK)  
                 INNER JOIN agencia_tb WITH (NOLOCK)  
                         ON proposta_fechada_tb.cont_agencia_id = agencia_tb.agencia_id  
                            AND agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id  
          WHERE  proposta_fechada_tb.proposta_id IN (SELECT DISTINCT proposta_id  
                                                     FROM   ##Propostas_Produto_arqBB)  
                 AND proposta_fechada_tb.proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                                             FROM   ##ObtemDadosAgencia)  
  
          --- para registros que n�o est�o na tabela criada acima         
          INSERT INTO ##ObtemDadosAgencia  
          SELECT proposta_id,  
                 '0000',  
                 '',  
                 seguros_db.dbo.Calcula_dv_cc_ou_agencia('0000') DV  
          FROM   ##retorno_Fonte_Arquivo WITH (NOLOCK)  
          WHERE  proposta_id NOT IN (SELECT proposta_id  
                                     FROM   ##ObtemDadosAgencia)  
  
          ----obten��o de dados da PropostaBB (sub ObtemPropostaBB, programa SEGP0746)                                
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                                 
          SELECT pf.proposta_bb,  
                 p.proposta_id,  
                 p.ramo_id,  
                 p.subramo_id  
          INTO   #proposta_fechada  
          FROM   proposta_tb p WITH (NOLOCK)  
                 JOIN proposta_fechada_tb pf WITH (NOLOCK)  
                   ON p.proposta_id = pf.proposta_id  
          WHERE  p.produto_id IN ( 718, 719 )  
  
          ----obten��o de dados da PropostaBB (sub ObtemPropostaBB, programa SEGP0746)                                  
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                                    
          SELECT p.proposta_bb,  
                 p.proposta_id  
          INTO   ##ObtemPropostaBB  
          FROM   #proposta_fechada p WITH (NOLOCK)  
          WHERE  p.ramo_id IN (SELECT ramo_id  
                               FROM   #proposta_fechada WITH (NOLOCK)  
                               WHERE  proposta_id IN (SELECT DISTINCT proposta_id  
                                                      FROM   ##Propostas_Produto_arqBB))  
                 AND p.subramo_id IN (SELECT subramo_id  
                                      FROM   #proposta_fechada WITH (NOLOCK)  
                                      WHERE  proposta_id IN (SELECT DISTINCT proposta_id  
                                                             FROM   ##Propostas_Produto_arqBB))  
  
          --para registros que n�o foram encontrados acima                                
          INSERT INTO ##ObtemPropostaBB  
          SELECT 0 proposta_bb,  
                 p.proposta_id  
          FROM   proposta_tb p WITH (NOLOCK)  
                 INNER JOIN proposta_fechada_tb pf WITH (NOLOCK)  
                         ON p.proposta_id = pf.proposta_id  
          WHERE  p.produto_id IN ( 718, 719 )  
                 AND p.proposta_id IN (SELECT DISTINCT proposta_id  
                                       FROM   ##Propostas_Produto_arqBB)  
                 AND p.proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                           FROM   ##ObtemPropostaBB)  
  
          --- para registros que n�o est�o na tabela criada acima                                
          INSERT INTO ##ObtemPropostaBB  
          SELECT proposta_bb,  
                 proposta_id  
          FROM   proposta_adesao_tb WITH (NOLOCK)  
          WHERE  proposta_id IN (SELECT DISTINCT proposta_id  
                                 FROM   ##Propostas_Produto_arqBB)  
                 AND proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                         FROM   ##ObtemPropostaBB)  
  
          --- para registros que n�o est�o na tabela criada acima                                
          INSERT INTO ##ObtemPropostaBB  
          SELECT Isnull(proposta_fechada_tb.proposta_bb, 0) proposta_bb,  
                 proposta_fechada_tb.proposta_id  
          FROM   proposta_fechada_tb WITH (NOLOCK)  
                 INNER JOIN ##retorno_Fonte_Arquivo B  
                         ON proposta_fechada_tb.proposta_id = B.proposta_id  
          WHERE  proposta_fechada_tb.proposta_id IN (SELECT DISTINCT proposta_id  
                                                     FROM   ##Propostas_Produto_arqBB)  
                 AND proposta_fechada_tb.proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                                             FROM   ##ObtemPropostaBB)  
                 AND B.dt_avaliacao >= '20081001'  
  
          ----obten��o de dados de Endere�o/Agencia (sub ObtemEnderecoAgencia, programa SEGP0746)                                
          -- OBS: foi mantida a forma de desenvolvimento usada na vers�o anterior de SEGP0746                                
          SELECT b.endereco,  
                 b.cep,  
                 b.estado,  
                 b.municipio_id,  
                 c.nome Nome_Municipio,  
                 a.proposta_id  
          INTO   ##ObtemEnderecoAgencia  
          FROM   proposta_adesao_tb a WITH (NOLOCK),  
                 agencia_tb b WITH (NOLOCK),  
                 municipio_tb c WITH (NOLOCK)  
          WHERE  a.proposta_id IN (SELECT DISTINCT proposta_id  
                                   FROM   ##Propostas_Produto_arqBB)  
                 AND a.cont_agencia_id = b.agencia_id  
                 AND b.banco_id = a.cont_banco_id  
                 AND ( c.municipio_id = b.municipio_id  
                       AND c.estado = b.estado )  
                 AND ( ( b.endereco IS NOT NULL  
                         AND Ltrim(Rtrim(b.endereco)) <> '' )  
                       AND b.cep IS NOT NULL  
                       AND b.municipio_id <> 999 )  
          UNION  
          SELECT b.endereco,  
                 b.cep,  
                 b.estado,  
                 b.municipio_id,  
                 c.nome Nome_Municipio,  
                 a.proposta_id  
          FROM   proposta_fechada_tb a WITH (NOLOCK),  
                 agencia_tb b WITH (NOLOCK),  
                 municipio_tb c WITH (NOLOCK)  
          WHERE  a.proposta_id IN (SELECT DISTINCT proposta_id  
                                   FROM   ##Propostas_Produto_arqBB)  
                 AND a.cont_agencia_id = b.agencia_id  
                 AND b.banco_id = a.cont_banco_id  
                 AND ( c.municipio_id = b.municipio_id  
                       AND c.estado = b.estado )  
                 AND ( ( b.endereco IS NOT NULL  
                         AND Ltrim(Rtrim(b.endereco)) <> '' )  
                       AND b.cep IS NOT NULL  
                       AND b.municipio_id <> 999 )  
  
          --tabela de Logs de endere�o - AGENCIA                  
          SELECT 'Dados de Endere�o e/ou Cidade e/ou CEP da Agencia no.'  
                 + CONVERT(VARCHAR(10), Isnull(b.agencia_id, ''))  
                 + ', incompleto(s) para a proposta '  
                 + CONVERT(VARCHAR(15), Isnull(A.proposta_id, ''))  
                 + ' ' LogEnder,  
                 A.proposta_id  
          INTO   ##GravaLogAgencia  
          FROM   proposta_adesao_tb a WITH (NOLOCK),  
                 agencia_tb b WITH (NOLOCK),  
                 municipio_tb c WITH (NOLOCK)  
          WHERE  a.proposta_id IN (SELECT DISTINCT proposta_id  
                                   FROM   ##Propostas_Produto_arqBB)  
                 AND a.cont_agencia_id = b.agencia_id  
                 AND b.banco_id = a.cont_banco_id  
                 AND ( c.municipio_id = b.municipio_id  
                       AND c.estado = b.estado )  
                 AND NOT ( ( b.endereco IS NOT NULL  
                             AND Ltrim(Rtrim(b.endereco)) <> '' )  
                           AND b.cep IS NOT NULL  
                           AND b.municipio_id <> 999 )  
          UNION  
          SELECT 'Dados de Endere�o e/ou Cidade e/ou CEP da Agencia no.'  
                 + CONVERT(VARCHAR(10), Isnull(b.agencia_id, ''))  
                 + ', incompleto(s) para a proposta '  
                 + CONVERT(VARCHAR(15), Isnull(A.proposta_id, ''))  
                 + ' ' LogEnder,  
                 A.proposta_id  
          FROM   proposta_fechada_tb a WITH (NOLOCK),  
                 agencia_tb b WITH (NOLOCK),  
                 municipio_tb c WITH (NOLOCK)  
          WHERE  a.proposta_id IN (SELECT DISTINCT proposta_id  
                                   FROM   ##Propostas_Produto_arqBB)  
                 AND a.cont_agencia_id = b.agencia_id  
                 AND b.banco_id = a.cont_banco_id  
                 AND ( c.municipio_id = b.municipio_id  
                       AND c.estado = b.estado )  
                 AND NOT ( ( b.endereco IS NOT NULL  
                             AND Ltrim(Rtrim(b.endereco)) <> '' )  
                           AND b.cep IS NOT NULL  
                           AND b.municipio_id <> 999 )  
  
          --tabela de Logs de endere�o - SEGURADO                  
          INSERT INTO ##GravaLogAgencia  
          SELECT 'Dados de Endere�o e/ou Cidade e/ou CEP incompleto(s) para a proposta '  
                 + CONVERT(VARCHAR(15), Isnull(A.proposta_id, ''))  
                 + ' ' LogEnder,  
                 A.proposta_id  
          FROM   ##retorno_Fonte_Arquivo A  
          WHERE  a.proposta_id IN (SELECT DISTINCT proposta_id  
                                   FROM   ##Propostas_Produto_arqBB)  
                 AND ( Isnull(endereco, '') = ''  
                        OR Isnull(cep, '') = '' )  
  
          --excluir propostas que n�o tem endere�os validados                                
          DELETE FROM ##retorno_Fonte_Arquivo  
          WHERE  proposta_id IN (SELECT proposta_id  
                                 FROM   ##GravaLogAgencia)  
  
          --##dados_agencia                             
          --##ObtemDadosAgencia   -> campos: cont_agencia_id,nome,DV, proposta_id                                
          --##ObtemPropostaBB   -> campos: proposta_bb                                
          --##ObtemEnderecoAgencia -> campos: endereco,Nome_Municipio,cep,estado                                
          SELECT B.proposta_id,  
                 B.cont_agencia_id,  
                 B.nome        NomeAgenciaCorretor,  
                 B.dv          DVCodAgencia,  
                 C.proposta_bb NumeroProposta,  
                 D.endereco,  
                 D.nome_municipio,  
                 D.cep,  
                 D.estado  
          INTO   ##Dados_Agencia  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##ObtemDadosAgencia B  
                         ON A.proposta_id = B.proposta_id  
                 INNER JOIN ##ObtemPropostaBB C  
                         ON A.proposta_id = C.proposta_id  
                 INNER JOIN ##ObtemEnderecoAgencia D  
                         ON A.proposta_id = D.proposta_id  
  
          --- para atender casos em que Origemproduto <> 'BB'                                
          INSERT INTO ##Dados_Agencia  
          SELECT proposta_id,  
                 cont_agencia_id     = 0,  
                 NomeAgenciaCorretor = '',  
                 DVCodAgencia        = '0',  
                 NumeroProposta      = 0,  
                 endereco            = '',  
                 nome_municipio      = '',  
                 cep                 = '',  
                 estado              = ''  
          FROM   ##retorno_Fonte_Arquivo  
          WHERE  proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                     FROM   ##Propostas_Produto_arqBB)  
                 AND proposta_id NOT IN (SELECT DISTINCT proposta_id  
                                         FROM   ##Dados_Agencia)  
  
          --SEGA9100                            
          --detalhe 10: Controle do documento - destino: Cliente/Proponente                                
          SELECT '10'                 TipoRegistroContDoc,  
                 '1'                  Linha_Arquivo,  
                 IDENTITY(INT, 1, 1)  sequencialContDoc,  
                 A.proposta_id        NumeroPropostaABContDoc,  
                 NomeDestinatario     = CASE  
                                      WHEN p.produto_id = 1167 THEN 'Caixa de Previd�ncia dos Funcion�rios do Banco do Brasil'  
                                      ELSE A.segurado  
                                    END,  
                 EnderecoDestinatario = CASE  
                                          WHEN p.produto_id = 1167 THEN 'Praia de Botafogo,501 4 andar A/C M�rcio Ventura'  
                                          ELSE A.endereco + ' ' + A.bairro  
                                        END,  
                 MunicipioDestino     = CASE  
                                      WHEN p.produto_id = 1167 THEN 'Rio de Janeiro'  
                                      ELSE A.municipio  
                                    END,  
                 UFdestino            = CASE  
                               WHEN p.produto_id = 1167 THEN 'RJ'  
                               ELSE A.estado  
                             END,  
                 CepDestino           = CASE  
                                WHEN p.produto_id = 1167 THEN '22250040'  
                                ELSE A.cep  
                              END,  
                 CodigoRetorno        = A.TipoDocumentoRetorno  
                                 + RIGHT('000000000' + CONVERT(VARCHAR(15), A.proposta_id), 9)  
                                 + CONVERT(VARCHAR(2), RIGHT(Datepart(year, @data), 2))  
                                 + RIGHT('000' + CONVERT(VARCHAR(3), Datepart(y, @data)), 3)  
                                 + RIGHT('0000' + CONVERT(VARCHAR(15), B.cont_agencia_id), 4),  
                 Space(60)            GerenciaDestino,  
                 A.Tipo_Documento     TipoDocumento,  
                 Space(09)            NumeroEndosso,  
                 'CLIENTE'            Destino  
          INTO   ##DETALHE_CtrlDoc_SEGA9100  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##Dados_Agencia B  
                         ON A.proposta_id = B.proposta_id  
                 INNER JOIN proposta_tb P  
                         ON A.proposta_id = P.proposta_id  
          WHERE  ( A.destino IS NULL  
                    OR Upper(Ltrim(Rtrim(A.destino))) = 'C' )  
          ORDER  BY A.cep,  
                    A.produto_id,  
                    A.proposta_id  
  
          --detalhe 20 do arquivo SEGA9100  --- detalhe: Segurado                                
          SELECT '20'                        TipoRegistroSegurado,  
                 '2'                         Linha_Arquivo,  
                 IDENTITY(INT, 1, 1)         sequencialSegurado,  
                 A.proposta_id               NumeroPropostaABSegurado,  
                 A.segurado                  NomeSegurado,  
                 A.endereco + ' ' + A.bairro EnderecoSegurado,  
                 A.municipio                 MunicipioSegurado,  
                 A.estado                    UFSegurado,  
                 A.cep                       CepSegurado,  
                 C.nome                      NomeProduto,  
                 A.nome                      MotivoRecusa,  
                 B.NumeroProposta            NumeroPropostaBB,  
                 B.cont_agencia_id           CodigoAgencia,  
                 B.DVCodAgencia              DVCodAgencia,  
                 B.NomeAgenciaCorretor       NomeAgenciaCorretor,  
                 A.num_proc_susep,
                 A.ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
                 A.celular, --migracao-documentacao-digital-2a-fase-cartas
                 A.email, --migracao-documentacao-digital-2a-fase-cartas
                 A.produto_id, --migracao-documentacao-digital-2a-fase-cartas
                 A.ramo_id, --migracao-documentacao-digital-2a-fase-cartas
                 A.cpf_cnpj
          INTO   ##DETALHE_Segurado_SEGA9100  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##Dados_Agencia B  
                         ON A.proposta_id = B.proposta_id  
                 INNER JOIN ##ObtemDadosProduto C  
                         ON A.produto_id = C.produto_id  
          WHERE  ( A.destino IS NULL  
                    OR Upper(Ltrim(Rtrim(A.destino))) = 'C' )  
          ORDER  BY A.cep,  
                    A.produto_id,  
                    A.proposta_id  
  
          ---SEGA9102                              
          --detalhe 10: Controle do documento - destino: ag�ncia                                
          SELECT '10'                 TipoRegistroContDoc,  
                 '1'                  Linha_Arquivo,  
                 IDENTITY(INT, 1, 1)  sequencialContDoc,  
                 A.proposta_id        NumeroPropostaABContDoc,  
                 NomeDestinatario     = CASE  
                                      WHEN A.produto_id = 1167 THEN 'Caixa de Previd�ncia dos Funcion�rios do Banco do Brasil'  
                                      ELSE 'AG�NCIA ' + B.NomeAgenciaCorretor  
                                    END,  
                 EnderecoDestinatario = CASE  
                                          WHEN A.produto_id = 1167 THEN 'Praia de Botafogo,501 4 andar A/C M�rcio Ventura'  
                                          ELSE B.endereco  
                                        END,  
                 MunicipioDestino     = CASE  
                                      WHEN A.produto_id = 1167 THEN 'Rio de Janeiro'  
                                      ELSE B.nome_municipio  
                                    END,  
                 UFdestino            = CASE  
                               WHEN A.produto_id = 1167 THEN 'RJ'  
                               ELSE B.estado  
                             END,  
                 CepDestino           = CASE  
                                WHEN A.produto_id = 1167 THEN '22250040'  
                                ELSE B.cep  
                              END,  
                 CodigoRetorno        = A.TipoDocumentoRetorno  
                                 + RIGHT('000000000' + CONVERT(VARCHAR(15), A.proposta_id), 9)  
                                 + CONVERT(VARCHAR(2), RIGHT(Datepart(year, @data), 2))  
                                 + RIGHT('000' + CONVERT(VARCHAR(3), Datepart(y, @data)), 3)  
                                 + RIGHT('0000' + CONVERT(VARCHAR(15), B.cont_agencia_id), 4),  
                 Space(60)            GerenciaDestino,  
                 A.Tipo_Documento     TipoDocumento,  
                 Space(09)            NumeroEndosso,  
                 'AGENCIA'            Destino  
          INTO   ##DETALHE_CtrlDoc_SEGA9102  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##Dados_Agencia B  
                         ON A.proposta_id = B.proposta_id  
          WHERE  ( A.destino IS NULL  
                    OR Upper(Ltrim(Rtrim(A.destino))) = 'A' )  
          ORDER  BY B.cep,  
                    A.produto_id,  
                    A.proposta_id  
  
          -- detalhe 20 do arquivo SEGA9102     --- detalhe: Segurado                                          
          SELECT '20'                        TipoRegistroSegurado,  
                 '2'                         Linha_Arquivo,  
                 IDENTITY(INT, 1, 1)         sequencialSegurado,  
                 A.proposta_id               NumeroPropostaABSegurado,  
                 A.segurado                  NomeSegurado,  
                 A.endereco + ' ' + A.bairro EnderecoSegurado,  
                 A.municipio                 MunicipioSegurado,  
                 A.estado                    UFSegurado,  
                 A.cep                       CepSegurado,  
                 C.nome                      NomeProduto,  
                 A.nome                      MotivoRecusa,  
                 B.NumeroProposta            NumeroPropostaBB,  
                 B.cont_agencia_id           CodigoAgencia,  
                 B.DVCodAgencia              DVCodAgencia,  
                 B.NomeAgenciaCorretor       NomeAgenciaCorretor,  
                 A.num_proc_susep,
                 A.ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
                 A.celular, --migracao-documentacao-digital-2a-fase-cartas
                 A.email, --migracao-documentacao-digital-2a-fase-cartas
                 A.produto_id, --migracao-documentacao-digital-2a-fase-cartas
                 A.ramo_id, --migracao-documentacao-digital-2a-fase-cartas  
                 A.cpf_cnpj
          INTO   ##DETALHE_Segurado_SEGA9102  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##Dados_Agencia B  
                         ON A.proposta_id = B.proposta_id  
                 INNER JOIN ##ObtemDadosProduto C  
                         ON A.produto_id = C.produto_id  
          WHERE  ( A.destino IS NULL  
                    OR Upper(Ltrim(Rtrim(A.destino))) = 'A' )  
          ORDER  BY B.cep,  
                    A.produto_id,  
                    A.proposta_id  
  
          --CRIA��O DE LINHAS DE DETALHE DO ARQUIVO                                                       
          ---inser��o em arquivo final 9100 - ##arquivo_9100                                
          SELECT RIGHT('00000'  
                       + CONVERT(VARCHAR(05), Linha_Arquivo), 05) Linha_Arquivo,  
                 CONVERT(VARCHAR(06), sequencialContDoc)          ID_Detalhe,  
                 Linha_registro                                   = LEFT(TipoRegistroContDoc  
                                       + RIGHT('000000' + CONVERT(VARCHAR(06), sequencialContDoc), 6)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaABContDoc), 9)  
                                       + LEFT(NomeDestinatario + Space(60), 60)  
                                       + LEFT(EnderecoDestinatario + Space(60), 60)  
                                       + LEFT(MunicipioDestino + Space(60), 60)  
                                       + LEFT(UFdestino + Space(02), 02)  
                                       + LEFT(CepDestino, 05) + '-'  
                                       + RIGHT(CepDestino, 03)  
                                       + CONVERT(VARCHAR(20), Codigoretorno)  
                                       + GerenciaDestino  
                                       + LEFT(TipoDocumento + Space(10), 10)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroEndosso), 9)  
                                       + LEFT(Destino + Space(10), 10) + Space(527), 527)  
          INTO   ##arquivo_9100  
          FROM   ##DETALHE_CtrlDoc_SEGA9100  
  
          INSERT INTO ##arquivo_9100  
          SELECT RIGHT('00000'  
                       + CONVERT(VARCHAR(05), Linha_Arquivo), 05) Linha_Arquivo,  
                 CONVERT(VARCHAR(06), sequencialSegurado)         ID_Detalhe,  
                 Linha_registro                                   = LEFT(TipoRegistroSegurado  
                        + RIGHT('000000' + CONVERT(VARCHAR(06), sequencialSegurado), 06)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaABSegurado), 09)  
                                       + LEFT(NomeSegurado + Space(60), 60)  
                                       + LEFT(EnderecoSegurado + Space(60), 60)  
                                       + LEFT(MunicipioSegurado + Space(60), 60)  
                                       + LEFT(UFSegurado + Space(02), 02)  
                                       + LEFT(CepSegurado, 05) + '-'  
                                       + RIGHT(CepSegurado, 03)  
                                       + LEFT(NomeProduto + Space(60), 60)  
                                       + LEFT(Motivorecusa + Space(60), 60)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaBB), 09)  
                                       + RIGHT('0000' + CONVERT(VARCHAR(04), CodigoAgencia), 4)  
                                       + LEFT(DVCodAgencia + ' ', 01)  
                                       + LEFT(NomeAgenciaCorretor + Space(60), 60)  
                                       + LEFT(num_proc_susep + Space(25), 25)  
                                       + LEFT(ddd_celular + Space(4), 4)  
                                       + LEFT(celular + Space(9), 9)  
                                       + LEFT(email + Space(60), 60)  
                                       + LEFT(produto_id + Space(5), 5)  
                                       + LEFT(ramo_id + Space(2), 2)  
                                       + LEFT(cpf_cnpj + Space(14), 14)   
                                       + Space(536), 536)  
          FROM   ##DETALHE_Segurado_SEGA9100  
  
          ---inser��o em arquivo final 9102 - ##arquivo_9102                                
          SELECT RIGHT('00000'  
                       + CONVERT(VARCHAR(05), Linha_Arquivo), 05) Linha_Arquivo,  
                 CONVERT(VARCHAR(06), sequencialContDoc)          ID_Detalhe,  
                 Linha_registro                                   = LEFT(TipoRegistroContDoc  
                                       + RIGHT('000000' + CONVERT(VARCHAR(06), sequencialContDoc), 6)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaABContDoc), 9)  
                                       + LEFT(NomeDestinatario + Space(60), 60)  
                                       + LEFT(EnderecoDestinatario + Space(60), 60)  
                                       + LEFT(MunicipioDestino + Space(60), 60)  
                                       + LEFT(UFdestino + Space(02), 02)  
                                       + LEFT(CepDestino, 05) + '-'  
                                       + RIGHT(CepDestino, 03)  
                                       + CONVERT(VARCHAR(20), Codigoretorno)  
                                       + GerenciaDestino  
                                       + LEFT(TipoDocumento + Space(10), 10)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroEndosso), 9)  
                                       + LEFT(Destino + Space(10), 10) + Space(527), 527)  
          INTO   ##arquivo_9102  
          FROM   ##DETALHE_CtrlDoc_SEGA9102  
  
          INSERT INTO ##arquivo_9102  
          SELECT RIGHT('00000'  
                       + CONVERT(VARCHAR(05), Linha_Arquivo), 05) Linha_Arquivo,  
                 CONVERT(VARCHAR(06), sequencialSegurado)         ID_Detalhe,  
                 Linha_registro                                   = LEFT(TipoRegistroSegurado  
                                       + RIGHT('000000' + CONVERT(VARCHAR(06), sequencialSegurado), 06)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaABSegurado), 09)  
                                       + LEFT(NomeSegurado + Space(60), 60)  
                                       + LEFT(EnderecoSegurado + Space(60), 60)  
                                       + LEFT(MunicipioSegurado + Space(60), 60)  
                                       + LEFT(UFSegurado + Space(02), 02)  
                                       + LEFT(CepSegurado, 05) + '-'  
                                       + RIGHT(CepSegurado, 03)  
                                       + LEFT(NomeProduto + Space(60), 60)  
                                       + LEFT(Motivorecusa + Space(60), 60)  
                                       + RIGHT('000000000' + CONVERT(VARCHAR(09), NumeroPropostaBB), 09)  
                                       + RIGHT('0000' + CONVERT(VARCHAR(04), CodigoAgencia), 4)  
                                       + LEFT(DVCodAgencia + ' ', 01)  
                                       + LEFT(NomeAgenciaCorretor + Space(60), 60)  
                                       + LEFT(num_proc_susep + Space(25), 25)
                                       + LEFT(ddd_celular + Space(4), 4)  
                                       + LEFT(celular + Space(9), 9)  
                                       + LEFT(email + Space(60), 60)  
                                       + LEFT(produto_id + Space(5), 5) 
                                       + LEFT(ramo_id + Space(2), 2)  
                                       + LEFT(cpf_cnpj + Space(14), 14)    
                                       + Space(536), 536)  
          FROM   ##DETALHE_Segurado_SEGA9102  
  
          ---CRIA��O DA TABELA ##evento_proposta_1aVia                                
          SELECT TOP 0 999999999         proposta_id,  
                       'XXXXXXXXXXXXXXX' Arquivo  
          INTO   ##evento_proposta_1aVia  
  
          --------------------------------------------------------------                                
          ---CRIA��O DA TABELA ##evento_proposta_2aVia                                
          SELECT TOP 0 999999999  proposta_id,  
                       999999999  num_solicitacao,  
                       'xxxxxxxx' Arquivo,  
                       'S'        Destino  
          INTO   ##evento_proposta_2aVia  
  
          --------------------------------------------------------------                                
          ---- SEGA9100, SEGA9102                                             
          --- 1� Via                                
          INSERT INTO ##evento_proposta_1aVia  
          SELECT DISTINCT Substring(A.LINHA_REGISTRO, 9, 9) proposta_id,  
                          'SEGA9100'                        Arquivo  
          FROM   ##arquivo_9100 A  
                 INNER JOIN ##retorno_Fonte_Arquivo B  
                         ON Substring(A.LINHA_REGISTRO, 9, 9) = B.proposta_id  
          WHERE  B.tp_doc IN ( 1, 2 )  
          UNION  
          SELECT DISTINCT Substring(A.LINHA_REGISTRO, 9, 9) proposta_id,  
                          'SEGA9102'                        Arquivo  
          FROM   ##arquivo_9102 A  
                 INNER JOIN ##retorno_Fonte_Arquivo B  
                         ON Substring(A.LINHA_REGISTRO, 9, 9) = B.proposta_id  
          WHERE  B.tp_doc IN ( 1, 2 )  
  
          --- 2� Via                                
          INSERT INTO ##evento_proposta_2aVia  
          SELECT DISTINCT Substring(B.LINHA_REGISTRO, 9, 9) proposta_id,  
                          A.num_solicitacao,  
                          'SEGA9100'                        Arquivo,  
                          A.destino  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##arquivo_9100 B  
                         ON A.proposta_id = Substring(B.LINHA_REGISTRO, 9, 9)  
          WHERE  A.tp_doc IN ( 3, 4 )  
          UNION  
          SELECT DISTINCT Substring(B.LINHA_REGISTRO, 9, 9) proposta_id,  
                          A.num_solicitacao,  
                          'SEGA9102'                        Arquivo,  
                          A.destino  
          FROM   ##retorno_Fonte_Arquivo A  
                 INNER JOIN ##arquivo_9102 B  
                         ON A.proposta_id = Substring(B.LINHA_REGISTRO, 9, 9)  
          WHERE  A.tp_doc IN ( 3, 4 )  
  
          DECLARE @QT_Registros INT  
          DECLARE @QT_Registros_9100 INT  
  
          --- quantidade de registros processados em SEGA9100                                
          SELECT @QT_Registros_9100 = Count(*)  
          FROM   ##arquivo_9100  
  
          DECLARE @QT_Registros_9102 INT  
  
          --- quantidade de registros processados em SEGA9102                                
          SELECT @QT_Registros_9102 = Count(*)  
          FROM   ##arquivo_9102  
  
          --- INSER��O DE HEADER - ##arquivo_9100                                
          INSERT INTO ##arquivo_9100  
          SELECT '0',  
                 '0',  
                 LEFT('01SEGA9100'  
                      + RIGHT('0000' + @num_remessa_9100, 04)  
                      + RIGHT('00' + CONVERT(VARCHAR(2), Datepart(DAY, @data)), 2)  
                      + RIGHT('00' + CONVERT(VARCHAR(2), Datepart(MONTH, @data)), 2)  
                      + RIGHT('0000' + CONVERT(VARCHAR(4), Datepart(YEAR, @data)), 4)  
                      + Space(427), 427)  
  
          --- INSER��O DE HEADER - ##arquivo_9102                                
          INSERT INTO ##arquivo_9102  
          SELECT '0',  
                 '0',  
                 LEFT('01SEGA9102'  
                      + RIGHT('0000' + @num_remessa_9102, 04)  
                      + RIGHT('00' + CONVERT(VARCHAR(2), Datepart(DAY, @data)), 2)  
                      + RIGHT('00' + CONVERT(VARCHAR(2), Datepart(MONTH, @data)), 2)  
                      + RIGHT('0000' + CONVERT(VARCHAR(4), Datepart(YEAR, @data)), 4)  
                      + Space(427), 427)  
  
          --- INSER��O DE TRAILLER NO ARQUIVO FINAL - ##arquivo_9100                                
          INSERT INTO ##arquivo_9100  
          SELECT CONVERT(VARCHAR(06), @QT_Registros_9100 + 1),  
                 CONVERT(VARCHAR(06), @QT_Registros_9100 + 1),  
                 LEFT('99'  
                      + RIGHT('000000' + CONVERT(VARCHAR(06), @QT_Registros_9100), 06)  
                      + Space(427), 427)  
  
          --- INSER��O DE TRAILLER NO ARQUIVO FINAL - ##arquivo_9102       
          INSERT INTO ##arquivo_9102  
          SELECT CONVERT(VARCHAR(06), @QT_Registros_9102 + 1),  
                 CONVERT(VARCHAR(06), @QT_Registros_9102 + 1),  
                 LEFT('99'  
                      + RIGHT('000000' + CONVERT(VARCHAR(06), @QT_Registros_9102), 06)  
                      + Space(427), 427)  
  
          --- quantidade de linhas e registros do arquivo                                
          DECLARE @QT_linhas_9100 INT  
          DECLARE @QT_linhas_9102 INT  
          DECLARE @QT_linhas_8134 INT  
  
          SELECT @QT_linhas_9100 = Count(*)  
          FROM   ##arquivo_9100  
  
          SELECT @QT_linhas_9102 = Count(*)  
          FROM   ##arquivo_9102  
  
          SELECT @QT_Registros_9100 = @QT_linhas_9100 - 2  
  
          SELECT @QT_Registros_9102 = @QT_linhas_9102 - 2  
  
          --- cria��o de estatistica de proposta: qtde de propostas processadas 9100 E 9102                                
          SELECT Count(*)           AS QT_PROPOSTAS,  
                 @QT_Registros_9100 AS QT_REGISTROS_9100,  
                 @QT_Registros_9102 AS QT_REGISTROS_9102,  
                 @QT_linhas_9100    AS QT_LINHAS_9100,  
                 @QT_linhas_9102    AS QT_LINHAS_9102
          INTO   ##ESTATISTICAS_1  
          FROM   ##retorno_Fonte_Arquivo  
  
          ---retorno das estat�sticas                                                          
          SELECT *  
          FROM   ##ESTATISTICAS_1 --(Linha Indevida. Adriano Ribeiro (16/11/2015) // Linha Retornada em: 15/12/2015                              
      END  
  
    --Rafael Inacio                          
    IF @cFinalizaArquivo = 'S'  --Rafael Inacio - RF00285030 - Gravar registros na base ap�s gera��o do arquivo f�sico
      BEGIN  
          
    SELECT @QT_Registros_9100 = QT_REGISTROS_9100,  
           @QT_Registros_9102 = QT_REGISTROS_9102,  
           @QT_linhas_9100    = QT_LINHAS_9100,  
           @QT_linhas_9102    = QT_LINHAS_9102  
    FROM   ##ESTATISTICAS_1  
  
          --cria��o de tabelas tempor�rias, se n�o existir                                
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##evento_impressao_atualiza_Batch')  
            DROP TABLE ##evento_impressao_atualiza_Batch  
  
          --TABELA TEMPORARIA USADA NA PROC: SEGS4309_SPI                                
          SELECT TOP 0 proposta_id,  
                       proposta_bb,  
                       endosso_id,  
                       diretoria_id,  
     usuario_aprovacao,  
                       dt_aprovacao,  
                       usuario_solicitante,  
                       dt_solicitacao,  
                       destino,  
                       tp_documento_id,  
                       status,  
                       motivo,  
                       sub_grupo_id,  
                       documento,  
                       dt_inclusao,  
                       local_destino,  
                       local_contato,  
                       usuario,  
                       dt_geracao_arquivo,  
                       arquivo_remessa_grf,  
                       qtd_vias,  
                       num_cobranca,  
                       sinistro_id  
          INTO   ##evento_impressao_atualiza_Batch  
          FROM   evento_seguros_db..evento_impressao_TB  
  
          IF EXISTS(SELECT *  
                    FROM   tempdb..SYSOBJECTS  
                    WHERE  NAME LIKE '##avaliacao_proposta_tb_Batch')  
            DROP TABLE ##avaliacao_proposta_tb_Batch  
  
          --TABELA TEMPORARIA USADA NA PROC: SEGS4310_SPI                                
          SELECT TOP 0 proposta_id,  
                       dt_avaliacao,  
                       num_avaliacao,  
                       tp_avaliacao_id,  
                       parecer,  
                       resultado,  
                       cod_cid10_item,  
                       cid10_grupo_id,  
                       usuario  
          INTO   ##avaliacao_proposta_tb_Batch  
          FROM   avaliacao_proposta_tb  
  
          ---atualiza��es de tabelas                                
          BEGIN TRANSACTION  
  
          DECLARE @proposta INT  
          DECLARE @Arquivo_1aVia VARCHAR(08)  
          DECLARE C1 CURSOR FOR  
            SELECT proposta_id,  
                   Arquivo  
            FROM   ##evento_proposta_1aVia  
  
          OPEN C1  
  
          FETCH NEXT FROM C1 INTO @proposta, @Arquivo_1aVia  
  
          WHILE @@FETCH_STATUS = 0  
            BEGIN  
                IF @Arquivo_1aVia = 'SEGA9100'  
                  BEGIN  
                      EXEC ('SEGS4309_SPI ' + @proposta + ', 0, null, null, ''C'', 12,''i'',null, 0, ''R'', '''', '''',''' + @usuario + ''', ''' + @data + ''', ''' + 'SEGA9100' + '.' + @num_remessa_9100 + '''')  
  
                      IF @@ERROR <> 0  
                        BEGIN  
                            RAISERROR('Erro ao executar procedure: SEGS4309_SPI - 1',16,1)  
  
                            ROLLBACK TRANSACTION  
  
                            RETURN  
                        END  
                  END  
  
                IF @Arquivo_1aVia = 'SEGA9102'  
                  BEGIN  
                      EXEC ('SEGS4309_SPI ' + @proposta + ', 0, null, null, ''A'', 12,''i'',null, 0, ''R'', '''', '''',''' + @usuario + ''', ''' + @data + ''', ''' + 'SEGA9102' + '.' + @num_remessa_9102 + '''')  
  
                      IF @@ERROR <> 0  
                        BEGIN  
                            RAISERROR('Erro ao executar procedure: SEGS4309_SPI - 1',16,1)  
  
                            ROLLBACK TRANSACTION  
  
                            RETURN  
                        END  
                  END  
  
                EXEC ('situacao_proposta_spu ' + @proposta + ',''s'',''' + @usuario + '''')  
  
                IF @@ERROR <> 0  
                  BEGIN  
                      RAISERROR('Erro ao executar procedure: situacao_proposta_spu',16,1)  
  
                      ROLLBACK TRANSACTION  
  
                      RETURN  
                  END  
  
                EXEC ('SEGS4310_SPI ' + @proposta + ',''' + @data + ''',005,'' '', '''',0,'' '',1,''' + @usuario + '''')  
  
                IF @@ERROR <> 0  
                  BEGIN  
                      RAISERROR('Erro ao executar procedure: SEGS4310_SPI',16,1)  
  
                      ROLLBACK TRANSACTION  
  
                      RETURN  
                  END  
  
                FETCH NEXT FROM C1 INTO @proposta, @Arquivo_1aVia  
            END  
  
          CLOSE C1  
  
        DEALLOCATE c1  
  
          ---inser��o em lote das tarefas realizadas na proc 'evento_seguros_db..SEGS4309_SPI'                                
          INSERT INTO evento_seguros_db..evento_impressao_tb  
                      (proposta_id,  
                       proposta_bb,  
                       endosso_id,  
                       diretoria_id,  
                       usuario_aprovacao,  
                       dt_aprovacao,  
                       usuario_solicitante,  
                       dt_solicitacao,  
                       destino,  
                       tp_documento_id,  
                       status,  
                       motivo,  
                       sub_grupo_id,  
                       documento,  
                       dt_inclusao,  
                       local_destino,  
                       local_contato,  
                       usuario,  
                       dt_geracao_arquivo,  
                       arquivo_remessa_grf,  
                       qtd_vias,  
                       num_cobranca,  
                       sinistro_id)  
          SELECT proposta_id,  
                 proposta_bb,  
                 endosso_id,  
                 diretoria_id,  
                 usuario_aprovacao,  
                 dt_aprovacao,  
                 usuario_solicitante,  
                 dt_solicitacao,  
                 destino,  
                 tp_documento_id,  
                 status,  
                 motivo,  
                 sub_grupo_id,  
                 documento,  
                 dt_inclusao,  
                 local_destino,  
                 local_contato,  
                 usuario,  
                 dt_geracao_arquivo,  
                 arquivo_remessa_grf,  
                 qtd_vias,  
                 num_cobranca,  
                 sinistro_id  
          FROM   ##evento_impressao_atualiza_Batch  
  
          IF @@ERROR <> 0  
            BEGIN  
                RAISERROR('Erro ao inserir dados na evento_impressao_tb',16,1)  
  
                ROLLBACK TRANSACTION  
  
                RETURN  
            END  
  
          --******************************************************                                
          --inser��o em lote das tarefas realizadas na proc 'SEGS4310_SPI'                                
          --******************************************************                                
          INSERT INTO avaliacao_proposta_tb  
                      (proposta_id,  
                       dt_avaliacao,  
                       num_avaliacao,  
                       tp_avaliacao_id,  
                       parecer,  
                       resultado,  
                       cod_cid10_item,  
                       cid10_grupo_id,  
                       usuario,  
                       dt_inclusao)  
          SELECT DISTINCT proposta_id,  
                          dt_avaliacao,  
                          num_avaliacao,  
                          tp_avaliacao_id,  
                          parecer,  
                          resultado,  
                          cod_cid10_item,  
                          cid10_grupo_id,  
                          usuario,  
                          @data  
          FROM   ##avaliacao_proposta_tb_Batch  
  
          IF @@ERROR <> 0  
            BEGIN  
                RAISERROR('Erro ao inserir a avalia��o da proposta',16,1)  
  
                ROLLBACK TRANSACTION  
  
                RETURN  
            END  
  
          DECLARE @solicitacao INT  
          DECLARE @destino CHAR(1)  
  
          WHILE EXISTS (SELECT num_solicitacao  
                        FROM   ##evento_proposta_2aVia)  
            BEGIN  
                SELECT @solicitacao = Min(num_solicitacao)  
                FROM   ##evento_proposta_2aVia  
  
                SELECT @destino = destino  
                FROM   ##evento_proposta_2aVia  
                WHERE  num_solicitacao = @solicitacao  
  
                ---- SEGA9100, SEGA9102         
                IF @destino = 'C' --- cliente - SEGA9100, SEGA9102                                
                  BEGIN  
                      EXEC ('evento_seguros_db..evento_impressao_geracao_spu ' + @solicitacao + ',''' + 'SEGA9100' + '.' + @num_remessa_9100 + ''',''' + @usuario + '''')  
  
                      IF @@ERROR <> 0  
                        BEGIN  
                            RAISERROR('Erro ao executar procedure: evento_seguros_db..evento_impressao_geracao_spu',16,1)  
  
                            ROLLBACK TRANSACTION  
  
                            RETURN  
                        END  
                  END  
  
                DELETE FROM ##evento_proposta_2aVia  
                WHERE  num_solicitacao = @solicitacao  
            END  
  
          --- Atualizando dados em arquivo_versao_gerado_tb                                                         
          EXEC ('controle_proposta_db..arquivo_versao_gerado_spi ''' + 'SEGA9100' + ''', ' + @num_remessa_9100 + ', ' + @QT_Registros_9100 + ', ''' + @data + ''', ' + @QT_linhas_9100 + ', ''' + @usuario + '''')  
  
          EXEC ('controle_proposta_db..arquivo_versao_gerado_spi ''' + 'SEGA9102' + ''', ' + @num_remessa_9102 + ', ' + @QT_Registros_9102 + ', ''' + @data + ''', ' + @QT_linhas_9102 + ', ''' + @usuario + '''')  
  
          IF @@ERROR <> 0  
            BEGIN  
                -- RAISERROR('Erro ao executar procedure: controle_proposta_db..arquivo_versao_gerado_spi', 16, 1)                                
                -- ROLLBACK TRANSACTION                                
                -- RETURN                                
                SET @msg = CONVERT(VARCHAR(1000), '55555 ') + ' | '  
                           + CONVERT(VARCHAR(1000), @msg)  
  
                RAISERROR (@msg,16,1)  
  
                ROLLBACK TRANSACTION  
  
                RETURN  
            END  
  
          COMMIT TRAN  
      END   
  


