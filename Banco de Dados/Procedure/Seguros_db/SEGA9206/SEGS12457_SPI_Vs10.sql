CREATE PROCEDURE SEGS12457_SPI  
            
    @usuario VARCHAR(20)            
            
AS            
            
/*-----------------------------------------------------------------------------------------------------                    
  DEMANDA: 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3         
  SEGA9206 -- Certificado BB Seguro Prestamista PJ (1231)          
  20 - Detalhe do arquivo - Dados do Segurado           
*/-----------------------------------------------------------------------------------------------------                    
                                             
SET NOCOUNT ON                                        
                
BEGIN TRY          
      
   INSERT INTO Interface_dados_db..SEGA9206_20_processar_tb (                                       
          SEGA9206_processar_id              
        , proposta_id              
        , apolice_id              
        , proposta_bb              
        , numero_certificado               
        , numero_processo_susep          
        , grupo_ramo  
        , dt_inclusao  -- Inclus�o Data da altera��o - Marcio.Nogueira - 11/08/2015 - Demanda 18319298          
        , dt_inicio_vigencia_certificado              
        , dt_fim_vigencia_certificado                        
        , dt_emissao_certificado          
        , nome_proponente          
        , cnpj_proponente          
        , cod_agencia                            
        , nome_agencia                           
        , prazo_seguro          
        , cod_susep_corretor                     
        , nome_corretor                          
        , nome_estipulante                       
        , cnpj_estipulante                       
        , endereco_destinatario          
        , bairro_destinatario          
        , municipio_destinatario          
        , UF_destinatario          
        , CEP_destinatario          
        , usuario      
        , telefone_estipulante)      
                                         
   SELECT SEGA9206.SEGA9206_processar_id              
        , SEGA9206.proposta_id              
        , SEGA9206.apolice_id              
        , f.proposta_bb            
        , ct.certificado_id          
        , pd.num_proc_susep           
        , grupo_ramo = rr.grupo_ramo + SUBSTRING('00' + CONVERT(VARCHAR(2), LTRIM(rr.ramo_id)),  LEN(CONVERT(VARCHAR(2), LTRIM(rr.ramo_id)))+ 1,2)          
        , getdate()  -- Inclus�o Data da altera��o - Marcio.Nogueira - 11/08/2015 - Demanda 18319298  
        , f.dt_inicio_vigencia             
        , f.dt_fim_vigencia           
        , ct.dt_emissao          
        , ce.nome          
        , cnpj_proponente = CASE WHEN LEN(LTRIM(RTRIM(ce.cpf_cnpj))) = 11 --CPF            
                  THEN SUBSTRING(LTRIM(ce.cpf_cnpj),1,3) + '.' + SUBSTRING(LTRIM(ce.cpf_cnpj),4,3) + '.' +            
                       SUBSTRING(LTRIM(ce.cpf_cnpj),7,3) + '-' + SUBSTRING(LTRIM(ce.cpf_cnpj),10,2)            
                 ELSE SUBSTRING(LTRIM(ce.cpf_cnpj),1,2) + '.' + SUBSTRING(LTRIM(ce.cpf_cnpj),3,3) + '.' +            
                      SUBSTRING(LTRIM(ce.cpf_cnpj),6,3) + '/' + SUBSTRING(LTRIM(ce.cpf_cnpj),9,4) + '-' +            
                       SUBSTRING(LTRIM(ce.cpf_cnpj),13,2)            
             END            
        , ISNULL(f.cont_agencia_id, 0)             
        , ISNULL(ag.nome, '')           
        , SEGA9206.prazo_seguro          
        , cod_susep_corretor = CASE WHEN c.corretor_id IS NULL           
                                    THEN ''            
                                    ELSE SUBSTRING(CAST(c.corretor_id AS VARCHAR(9)), 1, 2) + '.' +            
                                         SUBSTRING(CAST(c.corretor_id AS VARCHAR(9)), 3, 6) + '-' +            
                                         SUBSTRING(CAST(c.corretor_id AS VARCHAR(9)), 9, 1)            
                               END            
        , ISNULL(c.nome, '')           
       , nome_estipulante = ''          
        , cnpj_estipulante = ''          
        , endereco_destinatario = ''      
        , bairro_destinatario = ''      
        , municipio_destinatario = ''      
        , UF_destinatario = ''      
        , CEP_destinatario = ''      
        , usuario = @usuario              
        , telefone_estipulante = ''                 
     FROM Interface_dados_db.dbo.SEGA9206_processar_tb SEGA9206 WITH (NOLOCK)                                        
    INNER JOIN seguros_db.dbo.proposta_tb p WITH (NOLOCK)          
       ON p.proposta_id = SEGA9206.proposta_id          
    INNER JOIN seguros_db.dbo.proposta_adesao_tb f WITH (NOLOCK)              
       ON f.proposta_id = p.proposta_id           
    INNER JOIN seguros_db.dbo.ramo_tb rr WITH (NOLOCK)              
       ON rr.ramo_id = f.ramo_id          
    INNER JOIN seguros_db.dbo.certificado_tb ct WITH (NOLOCK)              
       ON ct.proposta_id = p.proposta_id          
    INNER JOIN seguros_db.dbo.cliente_tb ce WITH (NOLOCK)              
       ON ce.cliente_id = p.prop_cliente_id               
     LEFT JOIN seguros_db.dbo.agencia_tb ag WITH (NOLOCK)              
       ON ag.agencia_id = f.deb_agencia_id      
      AND ag.banco_id = CASE WHEN f.deb_banco_id <> 999      
                                     THEN f.deb_banco_id      
                                     ELSE 1      
                                END        
      LEFT JOIN seguros_db.dbo.municipio_tb mun WITH (NOLOCK)          
       ON ag.municipio_id = mun.municipio_id          
      AND ag.estado = mun.estado          
    INNER JOIN seguros_db.dbo.endereco_corresp_tb ec WITH (NOLOCK)                  
       ON ec.proposta_id = p.proposta_id                   
     LEFT JOIN seguros_db.dbo.corretagem_tb co WITH (NOLOCK)              
       ON co.proposta_id = p.proposta_id              
      AND co.dt_inicio_corretagem <= getdate()              
      AND (co.dt_fim_corretagem >= getdate() or co.dt_fim_corretagem is null)              
     LEFT JOIN seguros_db.dbo.corretor_tb c WITH (NOLOCK)              
       ON c.corretor_id = co.corretor_id              
     JOIN seguros_db.dbo.produto_tb pd WITH (NOLOCK)          
       ON pd.produto_id = SEGA9206.produto_id           
        
        
   --atualiza dados do estipulante          
   UPDATE Interface_dados_db..SEGA9206_20_processar_tb           
      SET nome_estipulante = ISNULL(cliente_tb.nome ,'')          
        , cnpj_estipulante = CASE WHEN cliente_tb.cpf_cnpj IS NOT NULL          
                                  THEN SUBSTRING(LTRIM(cliente_tb.cpf_cnpj),1,2) + '.' + SUBSTRING(LTRIM(cliente_tb.cpf_cnpj),3,3) + '.' +             
                                       SUBSTRING(LTRIM(cliente_tb.cpf_cnpj),6,3) + '/' + SUBSTRING(LTRIM(cliente_tb.cpf_cnpj),9,4) + '-' +             
                                       SUBSTRING(LTRIM(cliente_tb.cpf_cnpj),13,2)            
                                  ELSE ''          
                              END           
        , endereco_destinatario  = ISNULL(RTRIM(endereco_cliente_tb.endereco), '')        
        , bairro_destinatario    = ISNULL(endereco_cliente_tb.bairro,'')         
        , municipio_destinatario = ISNULL(municipio_tb.nome, '')       
        , UF_destinatario        = ISNULL(endereco_cliente_tb.estado, '')       
        , CEP_destinatario       = CASE            
               WHEN ISNULL(endereco_cliente_tb.cep, '') = '' OR LEN(RTRIM(endereco_cliente_tb.cep)) <> 8 THEN ''         
               ELSE SUBSTRING(endereco_cliente_tb.cep, 1, 5) + '-' + SUBSTRING(endereco_cliente_tb.cep, 6, 3)            
           END       
        , telefone_estipulante = ISNULL(cliente_tb.ddd_1,'') + '-' + ISNULL(cliente_tb.telefone_1,'')                     
             
     FROM seguros_db.dbo.representacao_proposta_tb representacao_proposta_tb WITH (NOLOCK)           
     JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)           
       ON cliente_tb.cliente_id = representacao_proposta_tb.est_cliente_id           
             
     JOIN seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)          
       ON apolice_tb.proposta_id = representacao_proposta_tb.proposta_id          
             
     JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK)          
       ON proposta_adesao_tb.apolice_id = apolice_tb.apolice_id          
      AND proposta_adesao_tb.ramo_id = apolice_tb.ramo_id          
      AND proposta_adesao_tb.sucursal_seguradora_id = apolice_tb.sucursal_seguradora_id          
      AND proposta_adesao_tb.seguradora_cod_susep = apolice_tb.seguradora_cod_susep          
             
     JOIN Interface_dados_db..SEGA9206_20_processar_tb SEGA9206_20          
       ON SEGA9206_20.proposta_id = proposta_adesao_tb.proposta_id          
              
     JOIN seguros_db.dbo.endereco_cliente_tb endereco_cliente_tb WITH (NOLOCK)          
       ON endereco_cliente_tb.cliente_id = representacao_proposta_tb.est_cliente_id           
          
     LEFT JOIN seguros_db.dbo.municipio_tb municipio_tb WITH (NOLOCK)          
       ON municipio_tb.municipio_id = endereco_cliente_tb.municipio_id          
      AND municipio_tb.estado = endereco_cliente_tb.estado          
      
   --Zoro.Gomes - Confitec - Somente obter a contratacao - O endosso passou a ser feito no SEGA9212 - 06/06/2016  
   --atualiza a data de emissao para as propostas com endosso          
--   UPDATE SEGA9206_20          
--      SET dt_emissao_certificado   = endosso_tb.dt_emissao          
--        , DT_INICIO_VIGENCIA_CERTIFICADO = EDS_CTR_SGRO.DT_SLCT_EDS      
--        , DT_FIM_VIGENCIA_CERTIFICADO  = CTR_SGRO.DT_FIM_VGC_CTR      
--     FROM Interface_dados_db.dbo.SEGA9206_20_processar_tb SEGA9206_20          
--     JOIN Interface_dados_db.dbo.SEGA9206_processar_tb SEGA9206           
--       ON SEGA9206_20.SEGA9206_processar_id = SEGA9206.SEGA9206_processar_id          
--     JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)          
--       ON SEGA9206.proposta_id = endosso_tb.proposta_id          
--      AND SEGA9206.endosso_id = endosso_tb.endosso_id          
--     JOIN seguros_db.dbo.proposta_processo_susep_tb proposta_processo_susep_tb WITH (NOLOCK)          
--    on proposta_processo_susep_tb.proposta_id = SEGA9206.proposta_id      
--     JOIN ALS_OPERACAO_DB..EDS_CTR_SGRO EDS_CTR_SGRO WITH (NOLOCK)          
--    ON EDS_CTR_SGRO.CD_PRD    = PROPOSTA_PROCESSO_SUSEP_TB.COD_PRODUTO      
--   AND EDS_CTR_SGRO.CD_MDLD    = PROPOSTA_PROCESSO_SUSEP_TB.COD_MODALIDADE      
--   AND EDS_CTR_SGRO.CD_ITEM_MDLD   = PROPOSTA_PROCESSO_SUSEP_TB.COD_ITEM_MODALIDADE      
--   AND EDS_CTR_SGRO.NR_CTR_SGRO   = PROPOSTA_PROCESSO_SUSEP_TB.NUM_CONTRATO_SEGURO      
--   AND EDS_CTR_SGRO.NR_SLCT_EDS_SGRA  = SEGA9206.ENDOSSO_ID      
--  JOIN ALS_OPERACAO_DB..CTR_SGRO CTR_SGRO WITH (NOLOCK)          
--    ON EDS_CTR_SGRO.CD_PRD    = CTR_SGRO.CD_PRD      
--   AND EDS_CTR_SGRO.CD_MDLD    = CTR_SGRO.CD_MDLD      
--   AND EDS_CTR_SGRO.CD_ITEM_MDLD   = CTR_SGRO.CD_ITEM_MDLD      
--   AND EDS_CTR_SGRO.NR_CTR_SGRO   = CTR_SGRO.NR_CTR_SGRO      
--   AND EDS_CTR_SGRO.NR_VRS_EDS   = CTR_SGRO.NR_VRS_EDS      
--    WHERE endosso_tb.endosso_id > 0          
    
  ----Atualiza Data inicio e fim de vigencia   
   update Sega9206_20_Processar_Tb  
   Set Dt_Inicio_Vigencia_Apolice                         = APolice_Tb.dt_inicio_vigencia --Proposta_Adesao_Tb.Dt_Inicio_Vigencia --Marcio.Nogueira - 11/08/2015 - Demanda 18319298  
     , Dt_Fim_VIgencia_Apolice                            = APolice_Tb.dt_fim_vigencia --Proposta_Adesao_Tb.Dt_Fim_Vigencia --Marcio.Nogueira - 11/08/2015 - Demanda 18319298     
  From Interface_Dados_Db..Sega9206_20_Processar_tb Sega9206_20_Processar_Tb  
   Join Seguros_Db..Proposta_Adesao_Tb Proposta_Adesao_Tb   
   on Proposta_Adesao_Tb.Proposta_ID  = Sega9206_20_Processar_Tb.Proposta_id    
   join  Seguros_Db..APolice_Tb APolice_Tb on   
   Proposta_Adesao_Tb.Apolice_ID = APolice_Tb.Apolice_ID  
   and Proposta_Adesao_Tb.ramo_id = APolice_Tb.ramo_id  
   and Proposta_Adesao_Tb.seguradora_cod_susep = APolice_Tb.seguradora_cod_susep   
  
-- Incluindo Texto da linha de Cr�dito - Marcio.Nogueira - 27/20/2015  
Select proposta_processo_susep_tb.proposta_id,PRTD_RSCO_CTR.CD_PRD,PRTD_RSCO_CTR.CD_MDLD,PRTD_RSCO_CTR.CD_ITEM_MDLD,PRTD_RSCO_CTR.NR_CTR_SGRO,PRTD_RSCO_CTR.NR_VRS_EDS,TX_CMPT_RPST_CTR,cast(Null as Varchar(100)) NM_LNH_OPR  
,convert(int,substring(TX_CMPT_RPST_CTR,1,9)) [CD_PRD_OPR]  
,convert(int,substring(TX_CMPT_RPST_CTR,11,9))[CD_MDLD_OPR]  
,convert(int,substring(TX_CMPT_RPST_CTR,21,9))[CD_LNH_OPR]  
into #Linha_Credito  
From Seguros_db.dbo.Proposta_tb Proposta_tb with(nolock)   
 join Seguros_db.dbo.proposta_processo_susep_tb proposta_processo_susep_tb with(nolock)   
   On Proposta_tb.proposta_id = proposta_processo_susep_tb.proposta_id  
Join als_operacao_db.dbo.PRTD_RSCO_CTR PRTD_RSCO_CTR with(nolock)   
   on PRTD_RSCO_CTR.cd_prd = proposta_processo_susep_tb.cod_produto  
  and PRTD_RSCO_CTR.cd_mdld = proposta_processo_susep_tb.cod_modalidade  
  and PRTD_RSCO_CTR.cd_item_mdld = proposta_processo_susep_tb.cod_item_modalidade  
  and PRTD_RSCO_CTR.nr_ctr_sgro = proposta_processo_susep_tb.num_contrato_seguro  
  and PRTD_RSCO_CTR.nr_vrs_eds = 1    
 INNER JOIN interface_dados_db..sega9206_20_processar_tb AS Sega9206_20_Processar_Tb ON  ---voltar para processar  
      Sega9206_20_Processar_Tb.proposta_id = proposta_tb.proposta_id   
where PRTD_RSCO_CTR.NR_QSTN = 899   
  and PRTD_RSCO_CTR.NR_QST = 7677  
  and proposta_tb.produto_id = 1231  
  and len(TX_CMPT_RPST_CTR) = 29  
   
   
Update Linha_Credito  
  Set Linha_Credito.NM_LNH_OPR = PRD_LNH_CRED.NM_LNH_OPR  
From #Linha_Credito Linha_Credito  
Join als_produto_db.dbo.PRD_LNH_CRED PRD_LNH_CRED with(nolock)  
   on PRD_LNH_CRED.CD_PRD = Linha_Credito.CD_PRD  
  and PRD_LNH_CRED.CD_MDLD = Linha_Credito.CD_MDLD  
  and PRD_LNH_CRED.CD_ITEM_MDLD = Linha_Credito.CD_ITEM_MDLD  
  and PRD_LNH_CRED.CD_PRD_OPR = Linha_Credito.CD_PRD_OPR  
  and PRD_LNH_CRED.CD_MDLD_OPR = Linha_Credito.CD_MDLD_OPR  
  and PRD_LNH_CRED.CD_LNH_OPR = Linha_Credito.CD_LNH_OPR  
-- Incluindo Texto da linha de Cr�dito - Marcio.Nogueira - 27/20/2015  
  
--Marcio.Nogueira - 19/08/2015 - Demanda 18319298 temporaria NUMERO CONTRATO FINANCIAMENTO---CTR_SGRO.nr_ctr_sgro        
SELECT  distinct proposta_tb.proposta_id, prtd_rsco_ctr.TX_DCR_ATB_CTR, prtd_rsco_ctr.TX_CMPT_RPST_CTR,CTR_SGRO.NR_VRS_EDS --Inser��o campo (CTR_SGRO.NR_VRS_EDS) de Vers�o de Endosso - Marcio.Nogueira - 04/09/2015 - Demanda 18319298  
into  #questionario  
FROM  seguros_db..proposta_processo_susep_tb AS proposta_processo_susep_tb INNER JOIN  
      seguros_db..proposta_tb AS proposta_tb WITH (nolock) ON   
      proposta_tb.proposta_id = proposta_processo_susep_tb.proposta_id INNER JOIN  
      seguros_db..produto_tb AS produto_tb ON   
      produto_tb.produto_id = proposta_tb.produto_id INNER JOIN  
      seguros_db..avaliacao_proposta_tb AS avaliacao_proposta_tb ON   
      avaliacao_proposta_tb.proposta_id = proposta_tb.proposta_id INNER JOIN  
      als_operacao_db..CTR_SGRO AS CTR_SGRO ON   
      CTR_SGRO.CD_PRD = proposta_processo_susep_tb.cod_produto AND   
      CTR_SGRO.CD_MDLD = proposta_processo_susep_tb.cod_modalidade AND   
      CTR_SGRO.CD_ITEM_MDLD = proposta_processo_susep_tb.cod_item_modalidade AND   
      CTR_SGRO.NR_CTR_SGRO = proposta_processo_susep_tb.num_contrato_seguro INNER JOIN  
      als_operacao_db..PRTD_RSCO_CTR AS prtd_rsco_ctr ON   
      prtd_rsco_ctr.CD_ITEM_MDLD = CTR_SGRO.CD_ITEM_MDLD AND   
      CTR_SGRO.CD_PRD = prtd_rsco_ctr.CD_PRD AND   
      CTR_SGRO.CD_MDLD = prtd_rsco_ctr.CD_MDLD AND   
      CTR_SGRO.NR_VRS_EDS = prtd_rsco_ctr.NR_VRS_EDS AND  
      proposta_processo_susep_tb.num_contrato_seguro = prtd_rsco_ctr.NR_CTR_SGRO   
      INNER JOIN interface_dados_db..sega9206_20_processar_tb AS Sega9206_20_Processar_Tb ON  ---voltar para processar  
      Sega9206_20_Processar_Tb.proposta_id = proposta_tb.proposta_id  
WHERE (CTR_SGRO.CD_ITEM_MDLD = 7733)AND  
      (prtd_rsco_ctr.NR_QST = 7677)   
                    
 --Marcio.Nogueira - 19/08/2015 - Demanda 18319298 update NUMERO CONTRATO FINANCIAMENTO---CTR_SGRO.nr_ctr_sgro       
 update Sega9206_20_Processar_Tb    
   Set   
     n_contrato_financiamento     = TX_DCR_ATB_CTR --Marcio.Nogueira - 17/08/2015 - Demanda 18319298 ALTERANDO NUMERO CONTRATO FINANCIAMENTO---CTR_SGRO.nr_ctr_sgro  
    ,linha_credito                = isnull(CAST(CONVERT(int,substring (#questionario.TX_CMPT_RPST_CTR,1,charindex ('.',#questionario.TX_CMPT_RPST_CTR)-1))AS VARCHAR)  +'/'+       
                              CAST(CONVERT(int,substring (#questionario.TX_CMPT_RPST_CTR,charindex ('.',#questionario.TX_CMPT_RPST_CTR)+1,charindex ('.',#questionario.TX_CMPT_RPST_CTR)-1))AS VARCHAR)+'/'+          
                              CAST(CONVERT(int,substring (#questionario.TX_CMPT_RPST_CTR,(charindex ('.',#questionario.TX_CMPT_RPST_CTR)+ charindex ('.',#questionario.TX_CMPT_RPST_CTR))+1,10))AS VARCHAR),0) ---Nova formata��o da linha de credito          
 
                                    + ISNULL (#Linha_Credito.NM_LNH_OPR,'') -- Incluindo Texto da linha de Cr�dito - Marcio.Nogueira - 27/20/2015  
   
 FROM Interface_Dados_Db..Sega9206_20_Processar_Tb Sega9206_20_Processar_Tb WITH (nolock) INNER JOIN ---voltar para   
 #questionario ON Sega9206_20_Processar_Tb.proposta_id = #QUESTIONARIO.proposta_id AND   
 #QUESTIONARIO.nr_vrs_eds = 1 --Inser��o campo (NR_VRS_EDS) de Vers�o de Endosso filtro = 1 (Contrata��o) - Marcio.Nogueira - 04/09/2015 - Demanda 18319298  
 left join #Linha_Credito on   
 #Linha_Credito.proposta_id = Sega9206_20_Processar_Tb.proposta_id -- Incluindo Texto da linha de Cr�dito  
 ----- fim ajuste marcio.nogueira - 19/08/2015 - Demanda 18319298 ALTERANDO NUMERO CONTRATO FINANCIAMENTO---CTR_SGRO.nr_ctr_sgro    
  
 --Zoro.Gomes - confitec - Acerto de Nulo - 07/06/2016  
 update Interface_Dados_Db..Sega9206_20_Processar_Tb    
    Set n_contrato_financiamento = ISNULL(n_contrato_financiamento,'')  
      , linha_credito = ISNULL(linha_credito,'')  
      
      
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update Sega9206_20_Processar_Tb
	set Sega9206_20_Processar_Tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db.dbo.Sega9206_20_Processar_Tb Sega9206_20_Processar_Tb with(nolock)
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = Sega9206_20_Processar_Tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
		
                                          
END TRY          
          
BEGIN CATCH                                            
        
 DECLARE @ErrorMessage NVARCHAR(4000)                
 DECLARE @ErrorSeverity INT                
 DECLARE @ErrorState INT              
    SELECT                 
       @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                
       @ErrorSeverity = ERROR_SEVERITY(),                
       @ErrorState = ERROR_STATE()                
           
    RAISERROR (        
       @ErrorMessage,                
       @ErrorSeverity,                
       @ErrorState )          
             
END CATCH                         
    
  
  
  
  
  
  