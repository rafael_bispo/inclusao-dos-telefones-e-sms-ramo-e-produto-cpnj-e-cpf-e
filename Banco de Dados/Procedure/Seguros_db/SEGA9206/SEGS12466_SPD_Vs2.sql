CREATE PROCEDURE SEGS12466_SPD  
                  
    @usuario VARCHAR(20),                  
    @num_versao INT                  
                  
AS                  
          
/*-----------------------------------------------------------------------------------------------------                    
 DEMANDA: 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma - Fase 3         
 SEGA9206 -- Certificado BB Seguro Prestamista PJ (1231)          
 Grava��o dos dados na tabela de processados          
*/-----------------------------------------------------------------------------------------------------                    
                    
BEGIN TRY         
          
   --Copiando dados para SEGA9206_processado_tb                
   INSERT INTO Interface_dados_db..SEGA9206_processado_tb (           
          SEGA9206_processar_id                  
        , proposta_id                            
        , proposta_bb                            
        , apolice_id                             
        , certificado_id                         
        , grupo_ramo                             
        , agencia_id                             
        , nome_agencia                           
        , prazo_seguro                           
        , dt_inicio_vigencia                     
        , dt_fim_vigencia  
        , dt_inclusao  --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                       
        , ramo_id                                
        , produto_id                             
        , seguradora_cod_susep                   
        , sucursal_seguradora_id                 
        , tp_documento_id                        
        , tipo_documento                         
        , endosso_id                             
        , layout_id                              
        , num_solicitacao                        
        , usuario )                               
   SELECT SEGA9206_processar_id              
        , proposta_id                            
        , proposta_bb                            
        , apolice_id                             
        , certificado_id                         
        , grupo_ramo                             
        , agencia_id                             
        , nome_agencia                           
        , prazo_seguro                           
        , dt_inicio_vigencia                     
        , dt_fim_vigencia  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                       
        , ramo_id                                
        , produto_id                             
        , seguradora_cod_susep                   
        , sucursal_seguradora_id                 
        , tp_documento_id                        
        , tipo_documento                         
        , endosso_id                             
        , layout_id                              
        , num_solicitacao                        
        , usuario                                
     FROM Interface_dados_db..SEGA9206_processar_tb                  
                  
   --Copiando dados para SEGA9206_10_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_10_processado_tb (                 
          SEGA9206_10_processar_id          
        , SEGA9206_processar_id             
        , proposta_id                       
        , endosso_id                        
        , nome_destinatario                 
        , endereco_destinatario             
        , bairro_destinatario               
        , municipio_destinatario            
        , UF_destinatario                   
        , CEP_destinatario                  
        , cod_retorno                       
        , descricao_documento               
        , usuario                           
        , gerencia_solicitante              
        , destino                           
        , tipo_documento  
        , dt_inclusao ) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                  
   SELECT SEGA9206_10_processar_id            
        , SEGA9206_processar_id             
        , proposta_id                       
        , endosso_id                        
        , nome_destinatario                 
        , endereco_destinatario             
        , bairro_destinatario               
        , municipio_destinatario            
        , UF_destinatario                   
        , CEP_destinatario                  
        , cod_retorno        
        , descricao_documento               
        , usuario                           
        , gerencia_solicitante              
        , destino                           
        , tipo_documento  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                               
     FROM Interface_dados_db..SEGA9206_10_processar_tb                  
                     
   --Copiando dados para SEGA9206_20_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_20_processado_tb (                 
          SEGA9206_20_processar_id                 
        , SEGA9206_processar_id                    
        , proposta_id                              
        , proposta_bb                              
        , apolice_id                               
        , cod_agencia                              
        , nome_agencia                             
        , nome_estipulante                         
        , cnpj_estipulante                         
        , nome_corretor                            
        , usuario                                  
        , prazo_seguro                             
        , numero_certificado                       
        , numero_processo_susep                    
        , nome_proponente                          
        , cnpj_proponente                          
        , dt_emissao_certificado                   
        , endereco_destinatario                    
        , bairro_destinatario                      
        , municipio_destinatario                   
        , UF_destinatario                          
        , CEP_destinatario                         
        , cod_susep_corretor                       
        , grupo_ramo                               
        , dt_inicio_vigencia_certificado           
        , dt_fim_vigencia_certificado      
        , telefone_estipulante    
        , n_contrato_financiamento   
		, linha_credito  
        , dt_inicio_vigencia_apolice  
		, dt_fim_vigencia_apolice  
		, cpf_cnpj
		, dt_inclusao )--Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929          
   SELECT SEGA9206_20_processar_id            
        , SEGA9206_processar_id                     
        , proposta_id                               
        , proposta_bb                               
        , apolice_id                                
        , cod_agencia                               
        , nome_agencia                              
        , nome_estipulante                          
        , cnpj_estipulante                          
        , nome_corretor                             
        , usuario                                   
        , prazo_seguro                              
        , numero_certificado                        
        , numero_processo_susep                     
        , nome_proponente                           
        , cnpj_proponente                           
        , dt_emissao_certificado                    
        , endereco_destinatario                     
        , bairro_destinatario                       
        , municipio_destinatario                    
        , UF_destinatario                           
        , CEP_destinatario                          
        , cod_susep_corretor                        
        , grupo_ramo                                
        , dt_inicio_vigencia_certificado            
        , dt_fim_vigencia_certificado       
        , telefone_estipulante  
        , n_contrato_financiamento   
		, linha_credito  
        , dt_inicio_vigencia_apolice  
		, dt_fim_vigencia_apolice  
		, cpf_cnpj
		, dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929       
     FROM Interface_dados_db..SEGA9206_20_processar_tb                  
       
                  
   --Copiando dados para SEGA9206_30_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_30_processado_tb (                 
          SEGA9206_30_processar_id           
        , SEGA9206_processar_id              
        , proposta_id                        
        , nome_segurado                      
        , dt_nascimento  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                       
        , cpf                                
        , usuario                            
        , endereco_segurado        
        , bairro_segurado        
        , municipio_segurado        
        , uf_segurado        
        , cep_segurado        
        , tel_segurado)                   
   SELECT SEGA9206_30_processar_id            
        , SEGA9206_processar_id      
        , proposta_id                        
        , nome_segurado                      
        , dt_nascimento  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                       
        , cpf                                
        , usuario         
        , endereco_segurado        
        , bairro_segurado        
        , municipio_segurado        
        , uf_segurado        
        , cep_segurado        
        , tel_segurado               
     FROM Interface_dados_db..SEGA9206_30_processar_tb                  
                     
   --Copiando dados para SEGA9206_40_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_40_processado_tb(                 
          SEGA9206_40_processar_id             
        , SEGA9206_processar_id                
        , proposta_id                          
        , nome_cobertura                       
        , val_capital_segurado                 
        , usuario                              
        , val_premio                            
        , dt_inicio_vigencia_cobertura      
        , dt_fim_vigencia_cobertura  
        , dt_inclusao) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                               
   SELECT SEGA9206_40_processar_id            
        , SEGA9206_processar_id                
        , proposta_id          
        , nome_cobertura                       
        , val_capital_segurado                 
        , usuario                              
        , val_premio       
        , dt_inicio_vigencia_cobertura      
        , CONVERT(VARCHAR,dt_fim_vigencia_cobertura,112)  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                             
     FROM Interface_dados_db..SEGA9206_40_processar_tb                  
                   
   --Copiando dados para SEGA9206_50_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_50_processado_tb (                 
          SEGA9206_50_processar_id                     
        , SEGA9206_processar_id                        
        , proposta_id                                  
        , endosso_id                                   
        , moeda                                        
        , val_premio_liquido                           
        , val_iof                                      
        , val_premio_bruto                             
        , periodicidade                                
        , num_parcela                                  
        , val_primeira_parcela                         
        , descricao_forma_pgto_1_parcela               
        , banco_id         
        , descricao_banco                              
        , agencia_id                                   
        , descricao_agencia                                    
        , usuario                                      
        , uf_agencia                                   
        , conta_corrente_deb  
        , dt_inclusao) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                       
                                   
   SELECT SEGA9206_50_processar_id            
        , SEGA9206_processar_id                        
        , proposta_id                                  
        , endosso_id                                   
        , moeda                                        
        , val_premio_liquido                           
        , val_iof                                      
        , val_premio_bruto                             
        , periodicidade                                
        , num_parcela                                  
        , val_primeira_parcela                         
        , descricao_forma_pgto_1_parcela               
        , banco_id                                     
        , descricao_banco                              
        , agencia_id                                   
        , descricao_agencia                                                                                                     
        , usuario             
        , uf_agencia                                   
        , conta_corrente_deb  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                                                              
   FROM Interface_dados_db..SEGA9206_50_processar_tb                  
  
   --Copiando dados para SEGA9206_51_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_51_processado_tb (                 
          SEGA9206_51_processar_id                
        , SEGA9206_processar_id                   
        , proposta_id                     
        , Moeda                                   
        , premio_liquido_total                    
        , iof                                     
        , premio_bruto_total                      
        , periodicidade                           
        , numero_parcelas                         
        , valor_primeira_parcela                  
        , forma_pgto_primeira                     
        , numero_banco                            
        , nome_banco                              
        , codigo_agencia                          
        , nome_agencia                            
        , uf_agencia                              
        , conta_corrente_debito                                                                    
        , usuario  
        , dt_inclusao ) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                   
   SELECT SEGA9206_51_processar_id            
        , SEGA9206_processar_id                   
        , proposta_id                             
        , Moeda                                   
        , premio_liquido_total                    
        , iof                                     
        , premio_bruto_total                      
        , periodicidade                           
        , numero_parcelas                         
        , valor_primeira_parcela                  
        , forma_pgto_primeira                     
        , numero_banco                            
        , nome_banco                              
        , codigo_agencia                          
        , nome_agencia                            
        , uf_agencia                              
        , conta_corrente_debito                                                              
        , usuario  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                   
   FROM Interface_dados_db..SEGA9206_51_processar_tb                  
                   
   --Copiando dados para SEGA9206_52_processado_tb                  
   INSERT INTO Interface_dados_db..SEGA9206_52_processado_tb (                 
          SEGA9206_52_processar_id                                 
        , SEGA9206_processar_id                                    
        , numero_proposta_ab                                       
        , numero_parcelas_quitadas                                 
        , premio_quitado_liquido_total                             
        , premio_quitado_iof                                       
        , premio_quitado_bruto_total                               
        , novo_valor_moeda        
        , novo_valor_premio_liquido_total                          
        , novo_valor_iof                                           
        , novo_valor_premio_bruto_total_quitado                    
        , novo_valor_periodicidade                                 
        , novo_valor_numero_parcelas                                                                  
        , usuario  
        , dt_inclusao) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                                                          
   SELECT          
          SEGA9206_52_processar_id                                 
        , SEGA9206_processar_id                                    
        , numero_proposta_ab                                       
        , numero_parcelas_quitadas                                 
        , premio_quitado_liquido_total                             
        , premio_quitado_iof                                       
        , premio_quitado_bruto_total                               
        , novo_valor_moeda                                         
        , novo_valor_premio_liquido_total                          
        , novo_valor_iof                                           
        , novo_valor_premio_bruto_total_quitado                    
        , novo_valor_periodicidade                                 
        , novo_valor_numero_parcelas                                                          
        , usuario  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                            
     FROM Interface_dados_db..SEGA9206_52_processar_tb                  
                       
   --Copiando dados para SEGA9206_60_processado_tb              
   INSERT INTO Interface_dados_db..SEGA9206_60_processado_tb (                 
          SEGA9206_60_processar_id                                 
        , SEGA9206_processar_id                                    
        , numero_proposta_ab                                       
        , moeda_seguro_restituir      
        , premio_seguro_restituir      
        , usuario  
        , dt_inclusao) --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                                                           
   SELECT          
          SEGA9206_60_processar_id            
        , SEGA9206_processar_id                                    
        , numero_proposta_ab                                       
        , moeda_seguro_restituir      
        , premio_seguro_restituir      
        , usuario  
        , dt_inclusao --Inclus�o Dt_inclusao - Marcio.Nogueira - 13/08/2015 - Demanda 1831929                                                                       
     FROM Interface_dados_db..SEGA9206_60_processar_tb      
      
      
   -- Excluindo os registros das tabelas processar       
     
   DELETE FROM Interface_dados_db..SEGA9206_10_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_20_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_30_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_40_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_50_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_51_processar_tb          
   DELETE FROM Interface_dados_db..SEGA9206_52_processar_tb        
   DELETE FROM Interface_dados_db..SEGA9206_60_processar_tb        
   DELETE FROM Interface_dados_db..SEGA9206_processar_tb   
     
     
     
     
     
END TRY                                            
                           
BEGIN CATCH                                            
        
 DECLARE @ErrorMessage NVARCHAR(4000)                
 DECLARE @ErrorSeverity INT                
 DECLARE @ErrorState INT              
    SELECT                 
       @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                
       @ErrorSeverity = ERROR_SEVERITY(),                
       @ErrorState = ERROR_STATE()                
           
    RAISERROR (        
       @ErrorMessage,                
       @ErrorSeverity,                
       @ErrorState )          
END CATCH                         
        
      
      
    
  
  