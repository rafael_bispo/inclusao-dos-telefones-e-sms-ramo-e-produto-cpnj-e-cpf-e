CREATE PROCEDURE SEGS8220_SPI  
  
@layout_id    INT,  
@produto_id   INT,  
@usuario      VARCHAR(20),  
@producao     CHAR(1) = 'N'  
      
AS      
      
SET NOCOUNT ON  
SET XACT_ABORT ON   
    
---- TESTES -------------------------------------------------    
-- DECLARE @layout_id      INT    
-- DECLARE @produto_id     INT    
-- DECLARE @usuario        VARCHAR(20)    
-- DECLARE @producao       CHAR(1)    
--     
-- SET @layout_id = 1456  
-- SET @produto_id = NULL    
-- SET @usuario = 'producao3'    
-- SET @producao = 'N'    
-------------------------------------------------------------    
    
-- AB-ABS    
DECLARE @empresa AS CHAR(3)    
DECLARE @msg  VARCHAR(100)  
    
SELECT @empresa = CASE WHEN trata_ABS =  's' THEN 'AB' ELSE 'ABS' END    
FROM parametro_geral_tb    
    
--Cria as tabelas tempor�rias   
SELECT * INTO ##interface_SEGA9088_processar_tb FROM interface_dados_db..SEGA9088_processar_tb WHERE 1=2   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na cria��o de ##interface_SEGA9088_processar_tb'  
     GOTO error  
  END  
  
SELECT * INTO ##interface_SEGA9088_10_processar_tb FROM interface_dados_db..SEGA9088_10_processar_tb WHERE 1=2   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na cria��o de ##interface_SEGA9088_10_processar_tb'  
     GOTO error  
  END  
  
SELECT * INTO ##interface_SEGA9088_20_processar_tb FROM interface_dados_db..SEGA9088_20_processar_tb WHERE 1=2   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na cria��o de ##interface_SEGA9088_20_processar_tb'  
     GOTO error  
  END  
  
-- Processar  
EXEC SEGS8221_SPI @layout_id,@produto_id,@usuario  
      
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na carga de SEGA9088_processar_tb'  
     GOTO error  
  END  
  
-- Detalhe 10  
EXEC SEGS8222_SPI @usuario      
    
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro na carga de SEGA9088_10_processar_tb'      
     GOTO error                        
  END      
    
-- Detalhe 20  
EXEC SEGS8223_SPI @usuario  
    
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na carga de SEGA9088_20_processar_tb'      
     GOTO error  
  END  
    
-- Exclui propostas ainda n�o consolidadas AB/ABS  
DELETE FROM ##interface_SEGA9088_processar_tb   
 WHERE NumeroPropostaAB IS NULL OR NumeroPropostaBB IS NULL OR NumeroProcSusep IS NULL   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na exclus�o de ##interface_SEGA9088_processar_tb'  
     GOTO error  
  END  
  
UPDATE ##interface_SEGA9088_processar_tb   
   SET MotivoRecusa = CASE ISNULL(MotivoRecusa,' ') WHEN ' ' THEN ' ' ELSE MotivoRecusa END,  
       CodigoAgencia = CASE ISNULL(CodigoAgencia,0) WHEN 0 THEN 0 ELSE CodigoAgencia END,  
       DVCodAgencia = CASE ISNULL(DVCodAgencia,' ') WHEN ' ' THEN ' ' ELSE DVCodAgencia END,  
       NomeAgenciaCorretor = CASE ISNULL(NomeAgenciaCorretor,' ') WHEN ' ' THEN ' ' ELSE NomeAgenciaCorretor END,  
       NomeSegurado = CASE ISNULL(NomeSegurado,' ') WHEN ' ' THEN ' ' ELSE NomeSegurado END,  
       CepDestino = CASE ISNULL(CepDestino,' ') WHEN ' ' THEN ' ' ELSE CepDestino END,  
       NomeProduto = CASE ISNULL(NomeProduto,' ') WHEN ' ' THEN ' ' ELSE NomeProduto END  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na atualiza��o de ##interface_SEGA9088_processar_tb'  
     GOTO error  
  END  
  
DELETE FROM ##interface_SEGA9088_10_processar_tb   
 WHERE NOT EXISTS (SELECT ##interface_SEGA9088_10_processar_tb.SEGA9088_processar_id   
                     FROM ##interface_SEGA9088_10_processar_tb, ##interface_SEGA9088_processar_tb   
                    WHERE ##interface_SEGA9088_10_processar_tb.SEGA9088_processar_id=  
                          ##interface_SEGA9088_processar_tb.SEGA9088_processar_id)   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na exclus�o de ##interface_SEGA9088_10_processar_tb'  
     GOTO error  
  END  
  
UPDATE ##interface_SEGA9088_10_processar_tb   
   SET NomeDestinatario = CASE ISNULL(NomeDestinatario,' ') WHEN ' ' THEN ' ' ELSE NomeDestinatario END,  
       EnderecoDestinatario = CASE ISNULL(EnderecoDestinatario,' ') WHEN ' ' THEN ' ' ELSE EnderecoDestinatario END,  
       MunicipioDestino = CASE ISNULL(MunicipioDestino,' ') WHEN ' ' THEN ' ' ELSE MunicipioDestino END,  
       UFdestino = CASE ISNULL(UFdestino,' ') WHEN ' ' THEN ' ' ELSE UFdestino END,  
       CepDestino = CASE ISNULL(CepDestino,' ') WHEN ' ' THEN ' ' ELSE CepDestino END  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na atualiza��o de ##interface_SEGA9088_10_processar_tb'  
     GOTO error  
  END  
  
DELETE FROM ##interface_SEGA9088_20_processar_tb   
 WHERE NOT EXISTS (SELECT ##interface_SEGA9088_20_processar_tb.SEGA9088_processar_id   
                     FROM ##interface_SEGA9088_20_processar_tb, ##interface_SEGA9088_processar_tb   
                    WHERE ##interface_SEGA9088_20_processar_tb.SEGA9088_processar_id=  
                          ##interface_SEGA9088_processar_tb.SEGA9088_processar_id)   
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na exclus�o de ##interface_SEGA9088_20_processar_tb'  
     GOTO error  
  END  
  
UPDATE ##interface_SEGA9088_20_processar_tb   
   SET NomeSegurado = CASE ISNULL(NomeSegurado,' ') WHEN ' ' THEN ' ' ELSE NomeSegurado END,  
       EnderecoSegurado = CASE ISNULL(EnderecoSegurado,' ') WHEN ' ' THEN ' ' ELSE EnderecoSegurado END,  
       MunicipioSegurado = CASE ISNULL(MunicipioSegurado,' ') WHEN ' ' THEN ' ' ELSE MunicipioSegurado END,  
       UFSegurado = CASE ISNULL(UFSegurado,' ') WHEN ' ' THEN ' ' ELSE UFSegurado END,  
       CepSegurado = CASE ISNULL(CepSegurado,' ') WHEN ' ' THEN ' ' ELSE CepSegurado END,  
       NomeProduto = CASE ISNULL(NomeProduto,' ') WHEN ' ' THEN ' ' ELSE NomeProduto END,  
       Motivorecusa = CASE ISNULL(Motivorecusa,' ') WHEN ' ' THEN ' ' ELSE Motivorecusa END,  
       CodigoAgencia = CASE ISNULL(CodigoAgencia,0) WHEN 0 THEN 0 ELSE CodigoAgencia END,  
       DVCodAgencia = CASE ISNULL(DVCodAgencia,' ') WHEN ' ' THEN ' ' ELSE DVCodAgencia END,  
       NomeAgenciaCorretor = CASE ISNULL(NomeAgenciaCorretor,' ') WHEN ' ' THEN ' ' ELSE NomeAgenciaCorretor END  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na atualiza��o de ##interface_SEGA9088_20_processar_tb'  
     GOTO error  
  END  
      
-- Atualiza situacao da proposta  
EXEC SEGS8224_SPU @usuario,@producao  
  
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro na atualiza��o da situa��o da proposta'      
     GOTO error                        
  END      
  
-- Grava nas tabelas SEGA9088  
UPDATE interface_dados_db..SEGA9088_processar_tb SET processar_id=NULL  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na carga de SEGA9088_processar_tb'  
     GOTO error  
  END  
  
INSERT INTO interface_dados_db..SEGA9088_processar_tb   
     ( NumeroPropostaAB  
     , produto_id  
     , MotivoRecusa  
     , NumeroPropostaBB  
     , NomeSegurado  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , var_destino  
     , destino  
     , CepDestino  
     , NomeProduto  
     , NumeroEndosso  
     , Tipo_Documento  
     , num_solicitacao  
     , proposta_id  
     , proposta_bb  
     , layout_id  
     , tp_doc  
     , dt_inclusao  
     , usuario  
  , NumeroProcSusep   
     , processar_id  
     )  
SELECT DISTINCT NumeroPropostaAB  
     , produto_id  
     , MotivoRecusa  
     , NumeroPropostaBB  
     , NomeSegurado  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , var_destino  
     , destino  
     , CepDestino  
     , NomeProduto  
     , NumeroEndosso  
     , Tipo_Documento  
     , num_solicitacao  
     , proposta_id  
     , proposta_bb  
     , layout_id  
     , tp_doc  
     , dt_inclusao  
     , usuario   
  , NumeroProcSusep   
     , SEGA9088_processar_id  
  FROM ##interface_SEGA9088_processar_tb WITH(NOLOCK)      
  ORDER BY CepDestino,      
           produto_id,      
           proposta_id      
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na carga de SEGA9088_processar_tb'  
     GOTO error  
  END  
  
INSERT INTO interface_dados_db..SEGA9088_10_processar_tb   
     ( SEGA9088_processar_id  
     , tipo_registro  
     , sequencial  
     , NumeroPropostaAB  
     , NomeDestinatario  
     , EnderecoDestinatario  
     , MunicipioDestino  
     , UFdestino  
     , CepDestino  
     , CodigoRetorno  
     , GerenciaDestino  
     , Tipo_Documento  
     , NumeroEndosso  
     , Destino  
     , num_solicitacao  
     , layout_id  
     , dt_inclusao  
     , usuario  
     )  
SELECT SEGA9088_processar_tb.SEGA9088_processar_id  
     , SEGA9088_10_processar_tb.tipo_registro  
     , SEGA9088_10_processar_tb.sequencial  
     , SEGA9088_10_processar_tb.NumeroPropostaAB  
     , SEGA9088_10_processar_tb.NomeDestinatario  
     , SEGA9088_10_processar_tb.EnderecoDestinatario  
     , SEGA9088_10_processar_tb.MunicipioDestino  
     , SEGA9088_10_processar_tb.UFdestino  
     , SEGA9088_10_processar_tb.CepDestino  
     , SEGA9088_10_processar_tb.CodigoRetorno  
     , SEGA9088_10_processar_tb.GerenciaDestino  
     , SEGA9088_10_processar_tb.Tipo_Documento  
     , SEGA9088_10_processar_tb.NumeroEndosso  
     , SEGA9088_10_processar_tb.Destino  
     , SEGA9088_10_processar_tb.num_solicitacao  
     , SEGA9088_10_processar_tb.layout_id  
     , SEGA9088_10_processar_tb.dt_inclusao  
     , SEGA9088_10_processar_tb.usuario   
  FROM ##interface_SEGA9088_10_processar_tb SEGA9088_10_processar_tb WITH(NOLOCK)  
  JOIN interface_dados_db..SEGA9088_processar_tb SEGA9088_processar_tb WITH(NOLOCK)  
    ON SEGA9088_10_processar_tb.SEGA9088_processar_id=SEGA9088_processar_tb.processar_id  
 ORDER BY sequencial  
  
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro na carga de SEGA9088_10_processar_tb'      
     GOTO error                        
  END      
    
INSERT INTO interface_dados_db..SEGA9088_20_processar_tb   
     ( SEGA9088_processar_id  
     , sequencial  
     , layout_id  
     , NumeroPropostaAB  
     , NomeSegurado  
     , EnderecoSegurado  
     , MunicipioSegurado  
     , UFSegurado  
     , CepSegurado  
     , NomeProduto  
     , Motivorecusa  
     , NumeroPropostaBB  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , tipo_registro  
     , dt_inclusao  
     , usuario  
	 , NumeroProcSusep   
	 , ddd_celular --migracao-documentacao-digital-2a-fase-cartas
	 , celular --migracao-documentacao-digital-2a-fase-cartas
	 , email --migracao-documentacao-digital-2a-fase-cartas  
	 , produto_id --migracao-documentacao-digital-2a-fase-cartas  
	 , ramo_id --migracao-documentacao-digital-2a-fase-cartas  
     )  
SELECT SEGA9088_processar_tb.SEGA9088_processar_id  
     , SEGA9088_20_processar_tb.sequencial  
     , SEGA9088_20_processar_tb.layout_id  
     , SEGA9088_20_processar_tb.NumeroPropostaAB  
     , SEGA9088_20_processar_tb.NomeSegurado  
     , SEGA9088_20_processar_tb.EnderecoSegurado  
     , SEGA9088_20_processar_tb.MunicipioSegurado  
     , SEGA9088_20_processar_tb.UFSegurado  
     , SEGA9088_20_processar_tb.CepSegurado  
     , SEGA9088_20_processar_tb.NomeProduto  
     , SEGA9088_20_processar_tb.Motivorecusa  
     , SEGA9088_20_processar_tb.NumeroPropostaBB  
     , SEGA9088_20_processar_tb.CodigoAgencia  
     , SEGA9088_20_processar_tb.DVCodAgencia  
     , SEGA9088_20_processar_tb.NomeAgenciaCorretor  
     , SEGA9088_20_processar_tb.num_proc_susep  
     , SEGA9088_20_processar_tb.tipo_registro  
     , SEGA9088_20_processar_tb.dt_inclusao  
     , SEGA9088_20_processar_tb.usuario  
	 , SEGA9088_20_processar_tb.NumeroProcSusep 
	 , SEGA9088_20_processar_tb.ddd_celular --migracao-documentacao-digital-2a-fase-cartas
	 , SEGA9088_20_processar_tb.celular --migracao-documentacao-digital-2a-fase-cartas
	 , SEGA9088_20_processar_tb.email --migracao-documentacao-digital-2a-fase-cartas    
	 , SEGA9088_20_processar_tb.produto_id --migracao-documentacao-digital-2a-fase-cartas  
	 , SEGA9088_20_processar_tb.ramo_id --migracao-documentacao-digital-2a-fase-cartas  
  FROM ##interface_SEGA9088_20_processar_tb SEGA9088_20_processar_tb WITH(NOLOCK)  
  JOIN interface_dados_db..SEGA9088_processar_tb SEGA9088_processar_tb WITH(NOLOCK)  
    ON SEGA9088_20_processar_tb.SEGA9088_processar_id=SEGA9088_processar_tb.processar_id  
 ORDER BY sequencial  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro na carga de SEGA9088_20_processar_tb'      
     GOTO error  
  END  
    
--Exclui as tabelas tempor�rias   
DROP TABLE ##interface_SEGA9088_processar_tb   
DROP TABLE ##interface_SEGA9088_10_processar_tb   
DROP TABLE ##interface_SEGA9088_20_processar_tb   
      
/* Executa em ABS */  
-------------------------------------------------------------------------------------    
IF @empresa = 'AB'    
 BEGIN    
    EXEC ABSS.seguros_db.dbo.SEGS8220_SPI @layout_id,@produto_id,@usuario,@producao  
   IF @@ERROR <> 0  
  BEGIN  
  SELECT @msg = 'Erro na carga de SEGA9088 (ABS)'      
        GOTO error  
     END  
     
    -- Agrupando para gerar somente um arquivo de cada SEGA  
    UPDATE interface_dados_db..SEGA9088_processar_tb SET processar_id=NULL  
    IF @@ERROR <> 0  
     BEGIN  
        SELECT @msg = 'Erro na carga de SEGA9088_processar_tb (ABS)'  
        GOTO error  
     END  
  
    INSERT INTO interface_dados_db..SEGA9088_processar_tb   
          (layout_id,NumeroPropostaAB,produto_id,MotivoRecusa,NumeroPropostaBB,CodigoAgencia,DVCodAgencia,  
           NomeAgenciaCorretor,num_proc_susep,destino,NumeroEndosso,Tipo_Documento,num_solicitacao,tp_doc,NomeSegurado,  
           CepDestino,NomeProduto,var_destino,proposta_id,proposta_modular_id,proposta_bb,proposta_modular_bb,  
           dt_inclusao,dt_alteracao,usuario,NumeroProcSusep,processar_id)   
    SELECT layout_id,NumeroPropostaAB,produto_id,MotivoRecusa,NumeroPropostaBB,CodigoAgencia,DVCodAgencia,  
           NomeAgenciaCorretor,num_proc_susep,destino,NumeroEndosso,Tipo_Documento,num_solicitacao,tp_doc,NomeSegurado,  
           CepDestino,NomeProduto,var_destino,proposta_id,proposta_modular_id,proposta_bb,proposta_modular_bb,  
           dt_inclusao,dt_alteracao,usuario,NumeroProcSusep,SEGA9088_processar_id    
    FROM ABSS.interface_dados_db.dbo.SEGA9088_processar_tb   
   IF @@ERROR <> 0  
  BEGIN  
  SELECT @msg = 'Erro na carga de SEGA9088_processar_tb (ABS)'      
        GOTO error  
     END  
     
    INSERT INTO interface_dados_db..SEGA9088_10_processar_tb   
          (SEGA9088_processar_id,tipo_registro,sequencial,layout_id,NumeroPropostaAB,NomeDestinatario,  
           EnderecoDestinatario,MunicipioDestino,UFdestino,CepDestino,CodigoRetorno,NumeroEndosso,Tipo_Documento,Destino,  
           num_solicitacao,GerenciaDestino,dt_inclusao,dt_alteracao,usuario)   
    SELECT SEGA9088_processar_tb.SEGA9088_processar_id,SEGA9088_10_processar_tb.tipo_registro,  
           SEGA9088_10_processar_tb.sequencial,SEGA9088_10_processar_tb.layout_id,  
           SEGA9088_10_processar_tb.NumeroPropostaAB,SEGA9088_10_processar_tb.NomeDestinatario,  
           SEGA9088_10_processar_tb.EnderecoDestinatario,SEGA9088_10_processar_tb.MunicipioDestino,  
           SEGA9088_10_processar_tb.UFdestino,SEGA9088_10_processar_tb.CepDestino,SEGA9088_10_processar_tb.CodigoRetorno,  
           SEGA9088_10_processar_tb.NumeroEndosso,SEGA9088_10_processar_tb.Tipo_Documento,  
           SEGA9088_10_processar_tb.Destino,SEGA9088_10_processar_tb.num_solicitacao,  
           SEGA9088_10_processar_tb.GerenciaDestino,SEGA9088_10_processar_tb.dt_inclusao,  
           SEGA9088_10_processar_tb.dt_alteracao,SEGA9088_10_processar_tb.usuario  
      FROM ABSS.interface_dados_db.dbo.SEGA9088_10_processar_tb SEGA9088_10_processar_tb   
      JOIN interface_dados_db..SEGA9088_processar_tb SEGA9088_processar_tb   
        ON SEGA9088_10_processar_tb.SEGA9088_processar_id=SEGA9088_processar_tb.processar_id  
   IF @@ERROR <> 0  
  BEGIN  
  SELECT @msg = 'Erro na carga de SEGA9088_10_processar_tb (ABS)'      
        GOTO error  
     END  
     
    INSERT INTO interface_dados_db..SEGA9088_20_processar_tb   
          (SEGA9088_processar_id,tipo_registro,sequencial,layout_id,NumeroPropostaAB,NomeSegurado,EnderecoSegurado,  
           MunicipioSegurado,UFSegurado,CepSegurado,NomeProduto,Motivorecusa,NumeroPropostaBB,CodigoAgencia,DVCodAgencia,  
           NomeAgenciaCorretor,num_proc_susep,dt_inclusao,dt_alteracao,usuario,NumeroProcSusep,ddd_celular,celular,email,produto_id, ramo_id)   
    SELECT SEGA9088_processar_tb.SEGA9088_processar_id,SEGA9088_20_processar_tb.tipo_registro,  
           SEGA9088_20_processar_tb.sequencial,SEGA9088_20_processar_tb.layout_id,  
           SEGA9088_20_processar_tb.NumeroPropostaAB,SEGA9088_20_processar_tb.NomeSegurado,  
           SEGA9088_20_processar_tb.EnderecoSegurado,SEGA9088_20_processar_tb.MunicipioSegurado,  
           SEGA9088_20_processar_tb.UFSegurado,SEGA9088_20_processar_tb.CepSegurado,SEGA9088_20_processar_tb.NomeProduto,  
           SEGA9088_20_processar_tb.Motivorecusa,SEGA9088_20_processar_tb.NumeroPropostaBB,  
           SEGA9088_20_processar_tb.CodigoAgencia,SEGA9088_20_processar_tb.DVCodAgencia,  
           SEGA9088_20_processar_tb.NomeAgenciaCorretor,SEGA9088_20_processar_tb.num_proc_susep,  
           SEGA9088_20_processar_tb.dt_inclusao,SEGA9088_20_processar_tb.dt_alteracao,SEGA9088_20_processar_tb.usuario,  
           SEGA9088_20_processar_tb.NumeroProcSusep,
           SEGA9088_20_processar_tb.ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
		   SEGA9088_20_processar_tb.celular, --migracao-documentacao-digital-2a-fase-cartas
		   SEGA9088_20_processar_tb.email, --migracao-documentacao-digital-2a-fase-cartas    
		   SEGA9088_20_processar_tb.produto_id, --migracao-documentacao-digital-2a-fase-cartas  
		   SEGA9088_20_processar_tb.ramo_id --migracao-documentacao-digital-2a-fase-cartas     
      FROM ABSS.interface_dados_db.dbo.SEGA9088_20_processar_tb SEGA9088_20_processar_tb   
      JOIN interface_dados_db..SEGA9088_processar_tb SEGA9088_processar_tb   
        ON SEGA9088_20_processar_tb.SEGA9088_processar_id=SEGA9088_processar_tb.processar_id  
   IF @@ERROR <> 0  
  BEGIN  
  SELECT @msg = 'Erro na carga de SEGA9088_20_processar_tb (ABS)'      
        GOTO error  
     END  
     
    -- Retornando os valores    
    SELECT COUNT(*) FROM interface_dados_db..SEGA9088_processar_tb WITH(NOLOCK)     
 END    
-------------------------------------------------------------------------------------    
  
SET NOCOUNT OFF  
  
RETURN  
  
error:  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:45 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:45 - Fim --  
  
  
  
  
  
  
  

