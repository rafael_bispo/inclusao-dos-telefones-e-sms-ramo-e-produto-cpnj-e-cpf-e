CREATE PROCEDURE SEGS8223_SPI  
  
@usuario VARCHAR(20)  
  
AS  
  
SET NOCOUNT ON         
SET XACT_ABORT ON   
  
-- TESTES --------------------------------------    
-- DECLARE @usuario  VARCHAR(20)    
-- SET @usuario = 'producao3'         
------------------------------------------------    
    
DECLARE @msg  VARCHAR(100)  
  
-- Criando temporaria para tratamento dos registros -----------------------------------------------  
CREATE TABLE #SEGA9088_20_processar_tb        
  (        
    SEGA9088_processar_id  INT  
  , TipoRegistro           CHAR(2)  
  , sequencial             INT  
  , NumeroPropostaAB       VARCHAR(19)  
  , NomeSegurado           VARCHAR(60)  
  , EnderecoSegurado       VARCHAR(91)  
  , MunicipioSegurado      VARCHAR(60)  
  , UFSegurado             CHAR(2)  
  , CepSegurado            VARCHAR(9)  
  , NomeProduto            VARCHAR(60)  
  , Motivorecusa           VARCHAR(60)  
  , NumeroPropostaBB       VARCHAR(21)  
  , CodigoAgencia          NUMERIC(4, 0)  
  , DVCodAgencia           CHAR(1)  
  , NomeAgenciaCorretor    VARCHAR(60)  
  , num_proc_susep         VARCHAR(25)  
  , layout_id              INT  
  , proposta_id            NUMERIC(9,0)  
  , proposta_bb            NUMERIC(9,0)  
  , NumeroProcSusep        VARCHAR(53)
  , ddd_celular			   VARCHAR(4)--migracao-documentacao-digital-2a-fase-cartas	  
  , celular				   VARCHAR(9)--migracao-documentacao-digital-2a-fase-cartas
  , email				   VARCHAR(60)--migracao-documentacao-digital-2a-fase-cartas  
  , produto_id			   INT--migracao-documentacao-digital-2a-fase-cartas
  , ramo_id				   INT--migracao-documentacao-digital-2a-fase-cartas  
  )  
    
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao criar #SEGA9088_processar_tb'  
     GOTO error  
 END  
  
-- Detalhe do segurado ----------------------------------------------------------------------------  
SELECT sega9088_processar_id = SEGA9088_processar_tb.sega9088_processar_id  
     , TipoRegistro          = '20'  
     , sequencial            = IDENTITY(INT, 1, 1)  
     , proposta_id           = SEGA9088_processar_tb.proposta_id  
     , proposta_bb           = SEGA9088_processar_tb.proposta_bb  
     , NomeSegurado          = SEGA9088_processar_tb.NomeSegurado  
     , EnderecoSegurado      = endereco_corresp_tb.endereco + ' ' + endereco_corresp_tb.bairro  
     , MunicipioSegurado     = endereco_corresp_tb.municipio  
     , UFSegurado            = endereco_corresp_tb.estado  
     , CepSegurado           = RIGHT('00000000' + CONVERT(VARCHAR(8),endereco_corresp_tb.cep),8)  
     , MotivoRecusa          = SEGA9088_processar_tb.MotivoRecusa  
     , CodigoAgencia         = SEGA9088_processar_tb.CodigoAgencia  
     , DVCodAgencia          = SEGA9088_processar_tb.DVCodAgencia  
     , NomeAgenciaCorretor   = SEGA9088_processar_tb.NomeAgenciaCorretor  
     , num_proc_susep        = SEGA9088_processar_tb.num_proc_susep  
     , layout_id             = SEGA9088_processar_tb.layout_id  
     , '' as ddd_celular --migracao-documentacao-digital-2a-fase-cartas
	 , '' as celular --migracao-documentacao-digital-2a-fase-cartas
	 , '' as email   --migracao-documentacao-digital-2a-fase-cartas   
	 , '' as produto_id   --migracao-documentacao-digital-2a-fase-cartas   
	 , '' as ramo_id   --migracao-documentacao-digital-2a-fase-cartas   
  INTO #SEGA9088_20_PROCESSAR  
  FROM ##interface_SEGA9088_processar_tb SEGA9088_processar_tb  with (nolock)   
  JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb  with (nolock)   
    ON endereco_corresp_tb.proposta_id = SEGA9088_processar_tb.proposta_id  
 ORDER BY CepSegurado  
        , SEGA9088_processar_tb.produto_id  
        , SEGA9088_processar_tb.proposta_id  
  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao criar temporaria #SEGA9088_20_PROCESSAR'  
     GOTO error  
 END  
  
  
INSERT INTO #SEGA9088_20_processar_tb        
    (      
       SEGA9088_processar_id  
     , TipoRegistro  
     , sequencial  
     , proposta_id  
     , proposta_bb  
     , NomeSegurado  
     , EnderecoSegurado  
     , MunicipioSegurado  
     , UFSegurado  
     , CepSegurado  
     , Motivorecusa  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , layout_id 
     , ddd_celular --migracao-documentacao-digital-2a-fase-cartas  
     , celular --migracao-documentacao-digital-2a-fase-cartas  
     , email   --migracao-documentacao-digital-2a-fase-cartas       
     , produto_id --migracao-documentacao-digital-2a-fase-cartas       
     , ramo_id --migracao-documentacao-digital-2a-fase-cartas       
     )  
SELECT sega9088_processar_id  
     , TipoRegistro  
     , sequencial  
     , proposta_id  
     , proposta_bb  
     , NomeSegurado  
     , EnderecoSegurado  
     , MunicipioSegurado  
     , UFSegurado  
     , CepSegurado  
     , MotivoRecusa  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , layout_id 
     , ddd_celular --migracao-documentacao-digital-2a-fase-cartas  
     , celular --migracao-documentacao-digital-2a-fase-cartas  
     , email   --migracao-documentacao-digital-2a-fase-cartas 
     , produto_id --migracao-documentacao-digital-2a-fase-cartas       
     , ramo_id --migracao-documentacao-digital-2a-fase-cartas             
FROM #SEGA9088_20_PROCESSAR  
  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao inserir na tabela #SEGA9088_processar_tb'  
     GOTO error  
 END  
  
-- Atualizando o nome do produto para o nome do modulo --------------------------------------------  
UPDATE #SEGA9088_20_processar_tb  
   SET NomeProduto = modulo_tb.nome  
  FROM #SEGA9088_20_processar_tb  with (nolock)   
  JOIN seguros_db..proposta_modular_tb proposta_modular_tb  with (nolock)   
 ON #SEGA9088_20_processar_tb.proposta_id = proposta_modular_tb.proposta_id  
  JOIN seguros_db..modulo_tb modulo_tb  with (nolock)   
    ON modulo_tb.modulo_id = proposta_modular_tb.modulo_id  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao atualizar o nome do modulo'  
     GOTO error  
 END  
  
-- Atualizando a proposta BB modular e o n�mero SUSEP de acordo com o agrupador da proposta ------------------------  
UPDATE ##interface_SEGA9088_processar_tb  
   SET proposta_modular_bb = proposta_adesao_tb.proposta_bb,  
       num_proc_susep_modular = CASE WHEN subramo_tb.num_proc_susep IS NULL OR subramo_tb.num_proc_susep = '0'  
                                     THEN produto_tb.num_proc_susep  
                                     ELSE subramo_tb.num_proc_susep  
                                END  
  FROM ##interface_SEGA9088_processar_tb SEGA9088_processar_tb  with (nolock)   
  JOIN seguros_db..proposta_modular_tb proposta_modular_tb  with (nolock)   
 ON SEGA9088_processar_tb.proposta_id = proposta_modular_tb.proposta_id  
  JOIN ABSS.seguros_db.dbo.proposta_modular_tb proposta_modular2_tb  with (nolock)   
 ON proposta_modular_tb.numero_agrupador = proposta_modular2_tb.numero_agrupador  
   AND proposta_modular_tb.proposta_id <> proposta_modular2_tb.proposta_id  
  JOIN ABSS.seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb  with (nolock)   
 ON proposta_adesao_tb.proposta_id = proposta_modular2_tb.proposta_id  
  JOIN ABSS.seguros_db.dbo.proposta_tb proposta_tb  with (nolock)   
 ON proposta_tb.proposta_id = proposta_modular2_tb.proposta_id  
  LEFT JOIN ABSS.seguros_db.dbo.subramo_tb subramo_tb  with (nolock)       
    ON subramo_tb.subramo_id = proposta_tb.subramo_id      
   AND dt_fim_vigencia_sbr IS NULL   
  JOIN ABSS.seguros_db.dbo.produto_tb produto_tb  with (nolock)   
    ON produto_tb.produto_id = proposta_tb.produto_id  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao atualizar a proposta BB modular e o n�mero SUSEP modular'  
     GOTO error  
 END  
  
-- Acumular as propostas de acordo com o agrupador da proposta ------------------------------------  
UPDATE #SEGA9088_20_processar_tb        
   SET NumeroPropostaAB = SEGA9088_processar_tb.NumeroPropostaAB  
     , NumeroPropostaBB = RTRIM(LTRIM(RIGHT('000000000' + CONVERT(VARCHAR(9),SEGA9088_processar_tb.proposta_modular_bb),9))) + ' , ' + RTRIM(LTRIM(RIGHT('000000000' + CONVERT(VARCHAR(9),SEGA9088_processar_tb.proposta_bb),9)))  
     , NumeroProcSusep  = RTRIM(LTRIM(RIGHT(SPACE(25) + CONVERT(VARCHAR(25),SEGA9088_processar_tb.num_proc_susep_modular),25))) + ' , ' + RTRIM(LTRIM(RIGHT(SPACE(25) + CONVERT(VARCHAR(25),SEGA9088_processar_tb.num_proc_susep),25)))  
  FROM #SEGA9088_20_processar_tb  
  JOIN ##interface_SEGA9088_processar_tb SEGA9088_processar_tb  with (nolock)   
 ON SEGA9088_processar_tb.proposta_id = #SEGA9088_20_processar_tb.proposta_id  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao atualizar as propostas'  
     GOTO error  
 END  
  
UPDATE ##interface_SEGA9088_processar_tb  
   SET NumeroPropostaBB = #SEGA9088_20_processar_tb.NumeroPropostaBB  
     , NumeroProcSusep = #SEGA9088_20_processar_tb.NumeroProcSusep  
  FROM #SEGA9088_20_processar_tb  
  JOIN ##interface_SEGA9088_processar_tb SEGA9088_processar_tb  with (nolock)   
 ON SEGA9088_processar_tb.proposta_id = #SEGA9088_20_processar_tb.proposta_id  
  
IF @@ERROR <> 0  
 BEGIN  
    SELECT @msg = 'Erro ao atualizar ##interface_SEGA9088_processar_tb'  
     GOTO error  
 END  
 
     -- Atualizando o contatos do segurado --------------------------------------------    
  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS  
UPDATE SEGA9088  
   SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,''), email = isnull(cliente_tb.e_mail,'')      
     , produto_id = proposta_tb.produto_id, ramo_id = proposta_tb.ramo_id
  FROM #SEGA9088_20_processar_tb SEGA9088   
  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)    
    ON SEGA9088.proposta_id = proposta_tb.proposta_id    
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)    
    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
    
IF @@ERROR <> 0    
 BEGIN    
    SELECT @msg = 'Erro ao atualizar dados de contato do segurado'    
     GOTO error    
 END 
  
-- DADOS DO SEGURADO ------------------------------------------------------------------------------  
INSERT INTO ##interface_SEGA9088_20_processar_tb  
    (  SEGA9088_processar_id  
     , sequencial  
     , layout_id  
     , NumeroPropostaAB  
     , NomeSegurado  
    , EnderecoSegurado  
     , MunicipioSegurado  
     , UFSegurado  
     , CepSegurado  
     , NomeProduto  
     , Motivorecusa  
     , NumeroPropostaBB  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , tipo_registro  
     , dt_inclusao  
     , usuario  
	 , NumeroProcSusep
	 , ddd_celular --migracao-documentacao-digital-2a-fase-cartas
	 , celular --migracao-documentacao-digital-2a-fase-cartas
	 , email   --migracao-documentacao-digital-2a-fase-cartas
	 , produto_id --migracao-documentacao-digital-2a-fase-cartas       
     , ramo_id --migracao-documentacao-digital-2a-fase-cartas        
     )  
SELECT SEGA9088_processar_id  
     , sequencial  
     , layout_id  
     , NumeroPropostaAB  
     , NomeSegurado  
     , EnderecoSegurado  
     , MunicipioSegurado  
     , UFSegurado  
     , CepSegurado = LEFT(CepSegurado, 5) + '-' + RIGHT(CepSegurado, 3)  
     , NomeProduto  
     , Motivorecusa  
     , NumeroPropostaBB  
     , CodigoAgencia  
     , DVCodAgencia  
     , NomeAgenciaCorretor  
     , num_proc_susep  
     , TipoRegistro  
     , dt_inclusao = GETDATE()  
     , usuario = @usuario  
     , NumeroProcSusep 
     , ddd_celular --migracao-documentacao-digital-2a-fase-cartas
	 , celular --migracao-documentacao-digital-2a-fase-cartas
	 , email   --migracao-documentacao-digital-2a-fase-cartas
	 , produto_id --migracao-documentacao-digital-2a-fase-cartas       
     , ramo_id --migracao-documentacao-digital-2a-fase-cartas        
  FROM #SEGA9088_20_processar_tb  with (nolock)   
 ORDER BY sequencial  
  
      
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro ao incluir em ##interface_SEGA9088_20_processar_tb'      
     GOTO error                        
  END   
 
    
SET NOCOUNT OFF        
  
RETURN  
  
error:  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:45 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:45 - Fim --  
  
  
  
  
  

