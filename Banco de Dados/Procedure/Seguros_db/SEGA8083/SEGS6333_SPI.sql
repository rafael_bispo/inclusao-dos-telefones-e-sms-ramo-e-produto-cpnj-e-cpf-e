CREATE PROCEDURE SEGS6333_SPI  
            
@usuario        VARCHAR(20)              
                
AS                
                
/*--------------------------------------    
--PARA TESTES    
DECLARE @usuario        VARCHAR(20)        
SET @usuario       = 'DEM 17781082'     
*/-------------------------------------        
                  
SET NOCOUNT ON    
    
BEGIN TRY    
  --Selecionando dados                
  SELECT DISTINCT proposta_ab = SEGA8083_processar_tb.proposta_id,          
                  SEGA8083_processar_tb.SEGA8083_processar_id,       
                  SEGA8083_processar_tb.proposta_bb,          
                  apolice = CAST('' AS VARCHAR(9)),            
                  SEGA8083_10_processar_tb.segurado,          
                  SEGA8083_10_processar_tb.endereco,          
                  SEGA8083_10_processar_tb.municipio,          
                  SEGA8083_10_processar_tb.estado,          
                  SEGA8083_10_processar_tb.cep,          
                  endereco_risco = LEFT(ISNULL(endereco_risco_tb.endereco, '') + ' ' + ISNULL(endereco_risco_tb.bairro, ''),60),            
                  municipio_risco = ISNULL(endereco_risco_tb.municipio, ''),            
                  estado_risco = ISNULL(endereco_risco_tb.estado, ''),            
                  cep_risco = CASE endereco_risco_tb.cep            
                                   WHEN NULL THEN '0'            
                                   ELSE SUBSTRING(endereco_risco_tb.cep, 1, 5) + '-' + SUBSTRING(endereco_risco_tb.cep, 6, 3)            
                              END,            
                  nome_produto = SEGA8083_processar_tb.nome_produto,          
                  dt_inicio_vigencia = ISNULL(SEGA8083_processar_tb.dt_inicio_vigencia, ''),        
                  dt_fim_vigencia = ISNULL(SEGA8083_processar_tb.dt_fim_vigencia, ''),        
                  num_proc_susep = ISNULL(subramo_tb.num_proc_susep, '') ,          
                  endereco_risco_tb.end_risco_id,   --Zoro.Gomes - confitec - Tratamento de duplicidade de endereco de risco - 12/02/2016    
      CAST('' AS VARCHAR(4)) as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas  
      CAST('' AS VARCHAR(9)) as celular, --migracao-documentacao-digital-2a-fase-cartas  
      CAST('' AS VARCHAR(60)) as email, --migracao-documentacao-digital-2a-fase-cartas  
      CAST('' AS VARCHAR(14)) as cpf_cnpj
    INTO #SEGA8083_20_processar_tb                
    FROM interface_dados_db..SEGA8083_processar_tb SEGA8083_processar_tb WITH (NOLOCK)                 
    JOIN interface_dados_db..SEGA8083_10_processar_tb SEGA8083_10_processar_tb WITH (NOLOCK)          
      ON SEGA8083_10_processar_tb.SEGA8083_processar_id = SEGA8083_processar_tb.SEGA8083_processar_id                  
    
    LEFT JOIN seguros_db..endereco_risco_tb endereco_risco_tb WITH (NOLOCK)                
      ON endereco_risco_tb.proposta_id = SEGA8083_processar_tb.proposta_id          
    
    JOIN seguros_db..proposta_tb proposta_tb WITH (NOLOCK)                
      ON proposta_tb.proposta_id = SEGA8083_processar_tb.proposta_id          
    
    JOIN seguros_db..subramo_tb subramo_tb WITH (NOLOCK)                   
      ON proposta_tb.ramo_id = subramo_tb.ramo_id                   
     AND proposta_tb.subramo_id = subramo_tb.subramo_id                   
     AND proposta_tb.dt_inicio_vigencia_sbr = subramo_tb.dt_inicio_vigencia_sbr     
       
       -- Atualizando o contatos do segurado --------------------------------------------    
   --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS  
   UPDATE #SEGA8083_20_processar_tb  
   SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,''), email = isnull(cliente_tb.e_mail,'')      
   FROM #SEGA8083_20_processar_tb (NOLOCK)    
   JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)    
  ON #SEGA8083_20_processar_tb.proposta_ab = proposta_tb.proposta_id    
   JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)    
  ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id   
  
  -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #SEGA8083_20_processar_tb
	set #SEGA8083_20_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #SEGA8083_20_processar_tb #SEGA8083_20_processar_tb
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #SEGA8083_20_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id 
           
       
  --Zoro.Gomes - confitec - Tratamento de duplicidade de endereco de risco - 12/02/2016    
  SELECT DISTINCT proposta_ab    
    INTO #prop_sem_repeticao    
    FROM #SEGA8083_20_processar_tb    
    
   ALTER TABLE #prop_sem_repeticao ADD end_risco_id INT    
    
  UPDATE #prop_sem_repeticao    
     SET end_risco_id = (SELECT MAX(end_risco_id)    
                           FROM seguros_db..endereco_risco_tb WITH (NOLOCK)    
                          WHERE proposta_id = #prop_sem_repeticao.proposta_ab )    
                   
  --Populando tabela referente ao detalhe 20    
  INSERT INTO interface_dados_db..SEGA8083_20_processar_tb (SEGA8083_processar_id,          
                                                            proposta_ab,          
                                                            proposta_bb,          
                                                            apolice,          
                                                            nome_segurado,          
                                                    endereco_segurado,          
                                                            municipio_segurado,          
                                                            uf_segurado,          
                                                            cep_segurado,          
                                                            endereco_risco,          
                                                            municipio_risco,          
                                                            uf_risco,          
                                                            cep_risco,          
                                                            nome_produto,          
                                                            data_vencimento_seguro,         
                                                            data_inicio_seguro,         
                                                            numero_processo_susep,         
                                                            dt_inclusao,          
                                                            usuario,  
                                                            ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas  
														    celular,  --migracao-documentacao-digital-2a-fase-cartas  
														    email,   --migracao-documentacao-digital-2a-fase-cartas                  
														    cpf_cnpj)
  SELECT SEGA8083_processar_id,          
         #SEGA8083_20_processar_tb.proposta_ab,          
         proposta_bb,          
         apolice,          
         segurado,          
         endereco,         
         municipio,          
         estado,          
         cep,          
         endereco_risco = LEFT(ISNULL(endereco_risco, ''), 60),            
         municipio_risco = ISNULL(municipio, ''),            
         estado_risco = ISNULL(estado, ''),            
         cep_risco,            
         nome_produto,          
         dt_fim_vigencia = ISNULL(dt_inicio_vigencia, ''), --O processo original(SEGP0746) executa com in�cio de vig�ncia --ISNULL(dt_fim_vigencia, ''),             
         dt_inicio_vigencia = ISNULL(dt_inicio_vigencia, ''),             
         num_proc_susep = ISNULL(num_proc_susep, ''),             
         dt_inclusao = GETDATE(),                
         @usuario,  
         ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas  
		 celular,  --migracao-documentacao-digital-2a-fase-cartas  
		 email,  --migracao-documentacao-digital-2a-fase-cartas                  
		 cpf_cnpj
   FROM #SEGA8083_20_processar_tb                
   JOIN #prop_sem_repeticao  ----Zoro.Gomes - confitec - Tratamento de duplicidade de endereco de risco - 12/02/2016    
     ON #prop_sem_repeticao.proposta_ab = #SEGA8083_20_processar_tb.proposta_ab      
    AND #prop_sem_repeticao.end_risco_id = #SEGA8083_20_processar_tb.end_risco_id    
    
  ORDER BY LEFT(RIGHT('00000000'+cep,8),5)+'-'+RIGHT(cep,3)                  
                  
  SET NOCOUNT OFF                             
END TRY                                        
                      
BEGIN CATCH                                          
   DECLARE @ErrorMessage NVARCHAR(4000)              
   DECLARE @ErrorSeverity INT              
   DECLARE @ErrorState INT            
   SELECT @ErrorMessage = 'Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),              
       @ErrorSeverity = ERROR_SEVERITY(),              
       @ErrorState = ERROR_STATE()              
        
   RAISERROR (@ErrorMessage,              
           @ErrorSeverity,              
        @ErrorState )        
  END CATCH                    
    
    
  