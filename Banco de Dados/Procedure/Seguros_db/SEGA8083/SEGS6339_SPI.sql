CREATE PROCEDURE SEGS6339_SPI  
          
@usuario          VARCHAR(20)        
        
AS              
        
SET NOCOUNT ON         
        
DECLARE @msg              VARCHAR(100)        
              
INSERT INTO interface_dados_db..SEGA8083_20_processado_tb (SEGA8083_processar_id,          
                                                          proposta_ab,          
                                                          proposta_bb,          
                                                          apolice,          
                                                          nome_segurado,          
                                                          endereco_segurado,          
                                                          municipio_segurado,          
                                                          uf_segurado,          
                                                          cep_segurado,          
                                                          endereco_risco,          
                                                          municipio_risco,          
                                                          uf_risco,          
                                                          cep_risco,          
                                                          nome_produto,          
                                                          data_vencimento_seguro,          
                                                          numero_processo_susep,          
                                                          dt_inclusao,          
                                                          usuario,    
                                                          data_inicio_seguro,
                                                          ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
														  celular,  --migracao-documentacao-digital-2a-fase-cartas
														  email,  --migracao-documentacao-digital-2a-fase-cartas                  
														  cpf_cnpj)
SELECT SEGA8083_processar_id,          
       proposta_ab,          
       proposta_bb,          
       apolice,          
       nome_segurado,          
       endereco_segurado,          
       municipio_segurado,          
       uf_segurado,          
       cep_segurado,          
       endereco_risco,          
       municipio_risco,          
       uf_risco,          
       cep_risco,          
       nome_produto,          
       data_vencimento_seguro,          
       numero_processo_susep,          
       dt_inclusao = GETDATE(),                
       @usuario,    
       data_inicio_seguro,
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
	   celular,  --migracao-documentacao-digital-2a-fase-cartas
	   email,  --migracao-documentacao-digital-2a-fase-cartas                   
	   cpf_cnpj
FROM interface_dados_db..SEGA8083_20_processar_tb (NOLOCK)               
              
IF @@ERROR <> 0        
 BEGIN                          
    SELECT @msg = 'Erro na inclus�o em SEGA8083_20_processado_tb'        
     GOTO error                          
  END        
        
SET NOCOUNT OFF        
        
--Retornando os dados----------------------------------------------------------------------------                
        
RETURN                          
                          
error:                          
  
   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Inicio --  
    --raiserror 55555 @msg        
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Fim --  
                            
        
      
    
    
  
  
  
  
  
  