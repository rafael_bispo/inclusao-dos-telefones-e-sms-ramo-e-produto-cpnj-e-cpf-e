CREATE PROCEDURE SEGS5507_SPI
  
  @layout_id  INT,  
  @usuario    VARCHAR(20)  
  
AS  
  
--GERA SEGA8177 DETALHE 21  
  
--APOLICE--  
   
-- SubRamo
-- Certificado
-- Constante Proposta
-- N�mero da Proposta
-- Constante Ap�lice
-- N�mero da Ap�lice
-- Constante ramo
-- Grupo de Ramo(2Chr) + C�digo do ramo + nome do ramo
-- Constante renova
-- N�mero da certificado anterior
-- Constante forma de pagamento
-- Forma de pagamento
-- Data in�cio de vig�ncia
-- Data final de vig�ncia

INSERT INTO interface_dados_db..SEGA8177_21_processar_tb    
      (layout_id,
       SEGA8177_processar_id,
       subramo_id,
       certificado_id,
       constante_proposta,
       proposta_id,
       constante_apolice,
       apolice_id,
       constante_ramo,
       detalhe_ramo,
       constante_renova,
       certificado_anterior_id,
       constante_forma_pgto,
       forma_pgto,
       dt_inicio_vigencia_seg,
       dt_fim_vigencia_seg,
       usuario,  
       dt_inclusao,
	   --Demanda SD01372169 - IN�CIO
	   produto_id,
	   nome_produto,
	   num_processo_susep)
	   --Demanda SD01372169 - FIM
SELECT @Layout_id,
       SEGA8177_processar_tb.SEGA8177_processar_id,
       SEGA8177_processar_tb.subramo_id,
       SEGA8177_processar_tb.certificado_id,
       constante_proposta = 'Proposta N�',
       SEGA8177_processar_tb.proposta_id,
       constante_apolice = 'Apolice N�',
       SEGA8177_processar_tb.apolice_id,
       constante_ramo = 'Ramo',
       detalhe_ramo = SUBSTRING(RIGHT('00' + CONVERT(VARCHAR, ISNULL(ramo_tb.grupo_ramo, 0)), 2) + RIGHT('00' + CONVERT(VARCHAR, SEGA8177_processar_tb.ramo_id), 2) + ' ' + ramo_tb.nome, 1, 45),
       constante_renova = 'Renova Certificado N�',
       certificado_anterior_id = ISNULL((SELECT certificado_tb.certificado_id
                                           FROM proposta_adesao_tb proposta_adesao_anterior (NOLOCK)
                                          INNER JOIN proposta_tb (NOLOCK)
                                             ON proposta_tb.proposta_id = proposta_adesao_anterior.proposta_id
                                          INNER JOIN certificado_tb (NOLOCK)
                                             ON certificado_tb.proposta_id = proposta_adesao_anterior.proposta_id
                                          WHERE proposta_tb.produto_id = 1123
                                            AND proposta_adesao_anterior.proposta_bb = proposta_adesao_tb.proposta_bb_anterior), 0),
       constante_forma_pgto = 'Forma Pagto.',
       forma_pgto = forma_pgto_tb.nome,
       dt_inicio_vigencia_seg = dbo.DataExtenso_fn(proposta_adesao_tb.dt_inicio_vigencia),
       dt_fim_vigencia_seg = CASE WHEN proposta_adesao_tb.dt_fim_vigencia IS NULL
                                  THEN ''
                                  ELSE dbo.DataExtenso_fn(proposta_adesao_tb.dt_fim_vigencia)
                             END,
       usuario = @Usuario,
       dt_inclusao = GETDATE(),
	   --Demanda SD01372169 - IN�CIO
	   produto_tb.produto_id,
	   produto_tb.nome,
	   Coalesce(apolice_tb.num_proc_susep, certificado_re_tb.num_proc_susep, subramo_tb.num_proc_susep, produto_tb.num_proc_susep) As num_processo_susep
	   --Demanda SD01372169 - FIM
  FROM interface_dados_db..SEGA8177_processar_tb SEGA8177_processar_tb (NOLOCK)
 INNER JOIN proposta_adesao_tb (NOLOCK)
    ON proposta_adesao_tb.proposta_id = SEGA8177_processar_tb.proposta_id
 INNER JOIN forma_pgto_tb (NOLOCK)
    ON forma_pgto_tb.forma_pgto_id = proposta_adesao_tb.forma_pgto_id
 INNER JOIN ramo_tb (NOLOCK)
    ON ramo_tb.ramo_id = SEGA8177_processar_tb.ramo_id
--JOAO.MACHADO (1229) EMPRESARIAL 2.0
--Demanda SD01372169 - IN�CIO
 INNER JOIN dbo.proposta_tb proposta_tb
	On SEGA8177_processar_tb.proposta_id = proposta_tb.proposta_id
 INNER JOIN dbo.produto_tb produto_tb
	On proposta_tb.produto_id = produto_tb.produto_id
 LEFT JOIN dbo.apolice_tb apolice_tb
	On proposta_adesao_tb.apolice_id = apolice_tb.apolice_id
	And proposta_adesao_tb.ramo_id = apolice_tb.ramo_id
 LEFT JOIN dbo.certificado_re_tb certificado_re_tb
	On SEGA8177_processar_tb.proposta_id = certificado_re_tb.proposta_id
 LEFT JOIN dbo.subramo_tb subramo_tb
	On proposta_adesao_tb.ramo_id = subramo_tb.ramo_id
	And proposta_tb.subramo_id = subramo_tb.subramo_id
--Demanda SD01372169 - FIM
union
SELECT @Layout_id,
       SEGA8177_processar_tb.SEGA8177_processar_id,
       SEGA8177_processar_tb.subramo_id,
       SEGA8177_processar_tb.certificado_id,
       constante_proposta = 'Proposta N�',
       SEGA8177_processar_tb.proposta_id,
       constante_apolice = 'Apolice N�',
       SEGA8177_processar_tb.apolice_id,
       constante_ramo = 'Ramo',
       detalhe_ramo = SUBSTRING(RIGHT('00' + CONVERT(VARCHAR, ISNULL(ramo_tb.grupo_ramo, 0)), 2) + RIGHT('00' + CONVERT(VARCHAR, SEGA8177_processar_tb.ramo_id), 2) + ' ' + ramo_tb.nome, 1, 45),
       constante_renova = 'Renova Certificado N�',
       certificado_anterior_id = 0,
       constante_forma_pgto = 'Forma Pagto.',
       forma_pgto = forma_pgto_tb.nome,
       dt_inicio_vigencia_seg = dbo.DataExtenso_fn(PROPOSTA_FECHADA_TB.dt_inicio_vig),
       dt_fim_vigencia_seg = CASE WHEN PROPOSTA_FECHADA_TB.dt_fim_vig IS NULL
                                  THEN ''
                                  ELSE dbo.DataExtenso_fn(PROPOSTA_FECHADA_TB.dt_fim_vig)
                             END,
       usuario = @Usuario,
       dt_inclusao = GETDATE(),
	   --Demanda SD01372169 - IN�CIO
	   produto_tb.produto_id,
	   produto_tb.nome,
	   Coalesce(apolice_tb.num_proc_susep, certificado_re_tb.num_proc_susep, subramo_tb.num_proc_susep, produto_tb.num_proc_susep) As num_processo_susep
	   --Demanda SD01372169 - FIM
  FROM interface_dados_db..SEGA8177_processar_tb SEGA8177_processar_tb (NOLOCK)
 INNER JOIN PROPOSTA_FECHADA_TB (NOLOCK)
    ON PROPOSTA_FECHADA_TB.proposta_id = SEGA8177_processar_tb.proposta_id
 INNER JOIN forma_pgto_tb (NOLOCK)
    ON forma_pgto_tb.forma_pgto_id = PROPOSTA_FECHADA_TB.forma_pgto_id
 INNER JOIN ramo_tb (NOLOCK)
    ON ramo_tb.ramo_id = SEGA8177_processar_tb.ramo_id
--Demanda SD01372169 - IN�CIO
 INNER JOIN dbo.proposta_tb proposta_tb
	On SEGA8177_processar_tb.proposta_id = proposta_tb.proposta_id
 INNER JOIN dbo.produto_tb produto_tb
	On proposta_tb.produto_id = produto_tb.produto_id
 LEFT JOIN dbo.apolice_tb apolice_tb
	On proposta_tb.proposta_id = apolice_tb.proposta_id
 LEFT JOIN dbo.certificado_re_tb certificado_re_tb
	On SEGA8177_processar_tb.proposta_id = certificado_re_tb.proposta_id
 LEFT JOIN dbo.subramo_tb subramo_tb
	On apolice_tb.ramo_id = subramo_tb.ramo_id
	And proposta_tb.subramo_id = subramo_tb.subramo_id
--Demanda SD01372169 - FIM
 ORDER BY SEGA8177_processar_tb.proposta_id,
          SEGA8177_processar_tb.SEGA8177_processar_id

--Demanda SD01372169 - IN�CIO
Update interface_dados_db.dbo.SEGA8177_21_processar_tb
	Set num_processo_susep =	Left(num_processo_susep, 5) + '.' +
								Right(Left(num_processo_susep, 11), 6) + '/' + 
								Right(Left(num_processo_susep, 15), 4) + '-' + 
								Right(Left(num_processo_susep, 17), 2)
Where Len(Rtrim(Ltrim(num_processo_susep))) = 17
--Demanda SD01372169 - FIM

RETURN




GO


