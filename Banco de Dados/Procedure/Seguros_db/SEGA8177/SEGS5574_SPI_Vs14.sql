CREATE PROCEDURE SEGS5574_SPI
    @layout_id INT,          
    @arquivo_remessa_grf VARCHAR(50),            
    @dt_geracao_arquivo SMALLDATETIME,            
    @remessa INT,            
    @producao CHAR(1),            
    @usuario VARCHAR(20)            
/*
		DECLARE @dt SMALLDATETIME; SET @dt = getdate();
		EXECUTE seguros_db..SEGS5574_SPI @layout_id		      = 1213		 
									   , @arquivo_remessa_grf = 'SEGA8177'
									   , @dt_geracao_arquivo  = @dt 
									   , @remessa    		  =  1000
									   , @usuario			  = 'DEM 18402795'
									   , @producao			  = 'N'
*/          
AS            

DECLARE  @error_message nvarchar (4000),                
            @error_severity int ,                 
            @error_state int;                
BEGIN TRY    
            
	DECLARE @motivo VARCHAR(60),            
			@sub_grupo_id INT,            
			@diretoria_id TINYINT,            
			@usuario_solicitante VARCHAR(20),            
			@tp_documento_id SMALLINT,            
			@status CHAR(1),            
			@documento CHAR(1),                
			@local_destino VARCHAR(60),            
			@local_contato VARCHAR(60),            
			@dt_solicitacao SMALLDATETIME,            
			@destino CHAR(1)            
            
	SET @motivo = NULL            
	SET @sub_grupo_id = 0            
	SET @diretoria_id = NULL            
	SET @usuario_solicitante = NULL            
	SET @tp_documento_id = 22            
	SET @status = 'i'            
	SET @documento = 'C'            
	SET @local_destino = ''            
	SET @local_contato = ''            
	SET @dt_solicitacao = getDate()            
	SET @destino = 'C'            
            
	SET NOCOUNT ON            
            
	--Inserindo 1ª Via             
          
	 SELECT SEGA8177_processar_tb.proposta_id,            
			num_cobranca = NULL,            
			SEGA8177_processar_tb.endosso_id,            
			proposta_adesao_tb.proposta_bb,            
			sinistro_id = NULL,            
			usuario_aprovacao = CAST(NULL AS VARCHAR(20)),            
			dt_aprovacao = CAST(NULL AS SMALLDATETIME),            
			qtd_vias = NULL,            
			num_solicitacao = NULL            
	   INTO #evento_impressao            
	   FROM interface_dados_db..SEGA8177_processar_tb SEGA8177_processar_tb  with (nolock)           
	  INNER JOIN proposta_adesao_tb with (nolock)            
		 ON proposta_adesao_tb.proposta_id = SEGA8177_processar_tb.proposta_id          
	  WHERE SEGA8177_processar_tb.via <> 2    
	union
	 SELECT SEGA8177_processar_tb.proposta_id,            
			num_cobranca = NULL,            
			SEGA8177_processar_tb.endosso_id,            
			proposta_fechada_tb.proposta_bb,            
			sinistro_id = NULL,            
			usuario_aprovacao = CAST(NULL AS VARCHAR(20)),            
			dt_aprovacao = CAST(NULL AS SMALLDATETIME),            
			qtd_vias = NULL,            
			num_solicitacao = NULL            
	   FROM interface_dados_db..SEGA8177_processar_tb SEGA8177_processar_tb with (nolock)            
	  INNER JOIN proposta_fechada_tb with (nolock)             
		 ON proposta_fechada_tb.proposta_id = SEGA8177_processar_tb.proposta_id          
	  WHERE SEGA8177_processar_tb.via <> 2           
            
	EXEC evento_seguros_db.dbo.evento_impressao_generica_spi @diretoria_id,            
															 @usuario_solicitante,            
															 @tp_documento_id,            
															 @status,            
															 @motivo,            
															 @sub_grupo_id,            
															 @documento,            
															 @dt_solicitacao,            
															 @destino,            
															 @local_destino,            
															 @local_contato,            
															 @dt_geracao_arquivo,            
															 @arquivo_remessa_grf,            
						   @usuario,            
															 @producao             
   
	-- 2° via             
	SELECT evento_impressao_tb.num_solicitacao            
	  INTO #evento_impressao2            
	  FROM interface_dados_db..SEGA8177_processar_tb SEGA8177_processar_tb  with (nolock)            
	 INNER JOIN evento_seguros_db..evento_impressao_tb evento_impressao_tb with (nolock)                 
		ON SEGA8177_processar_tb.proposta_id = evento_impressao_tb.proposta_id              
	 WHERE SEGA8177_processar_tb.via = 2          
	   AND evento_impressao_tb.status = 'L'              
	   AND evento_impressao_tb.dt_geracao_arquivo IS NULL              
	   AND evento_impressao_tb.tp_documento_id IN (@tp_documento_id)           
            
	IF @producao = 'S'            
	BEGIN            
		UPDATE evento_seguros_db..evento_impressao_tb            
		SET dt_geracao_arquivo = @dt_geracao_arquivo,            
			arquivo_remessa_grf = @arquivo_remessa_grf,            
			dt_alteracao = GETDATE(),            
			dt_solicitacao = @dt_solicitacao,    
			status = 'i',
			usuario = @usuario            
		FROM #evento_impressao2            
		JOIN evento_seguros_db..evento_impressao_tb evento_impressao_tb with (nolock)              
		  ON #evento_impressao2.num_solicitacao = evento_impressao_tb.num_solicitacao            
		WHERE #evento_impressao2.num_solicitacao > 0            
	END            
	ELSE            
	BEGIN            
		UPDATE seguros_temp_db..evento_impressao_tb            
		SET dt_geracao_arquivo = @dt_geracao_arquivo,            
			arquivo_remessa_grf = @arquivo_remessa_grf,            
			dt_alteracao = GETDATE(),     
			dt_solicitacao = @dt_solicitacao,  
			status = 'i',         
			usuario = @usuario        
		FROM #evento_impressao2            
		JOIN seguros_temp_db..evento_impressao_tb evento_impressao_tb  with (nolock)             
		  ON #evento_impressao2.num_solicitacao = evento_impressao_tb.num_solicitacao            
		WHERE #evento_impressao2.num_solicitacao > 0            
	END            
            
	-- transferindo as informações processadas -------------------------------------------------            
          
	INSERT INTO interface_dados_db..SEGA8177_processado_tb          
		  (SEGA8177_processado_id,           
		   proposta_id,          
		   apolice_id,          
		   certificado_id,          
		   ramo_id,          
		   subramo_id,          
		   endosso_id,          
		   agencia_id,          
		   via,          
		   layout_id,          
		   remessa,          
		   destino,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario)          
	SELECT SEGA8177_processar_id,           
		   proposta_id,          
		   apolice_id,          
		   certificado_id,          
		   ramo_id,          
		   subramo_id,          
		   endosso_id,          
		   agencia_id,          
		   via,          
		   layout_id,          
		   @remessa,          
		   destino,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db..SEGA8177_processar_tb with (nolock)            
          
	INSERT INTO interface_dados_db..SEGA8177_10_processado_tb          
		  (SEGA8177_processado_id,           
		   proposta_id,          
		   layout_id,                   
		   remessa,                     
		   subramo_id,                    
		   certificado_id,                
		   endereco,                 
		   bairro,                     
		   municipio,                     
		   estado,                     
		   cep,                        
		   tipo_documento,                
		   codigo_barras,                
		   dt_inclusao,                 
		   dt_alteracao,                  
		   usuario)          
	SELECT SEGA8177_processar_id,           
		   proposta_id,          
		   layout_id,                   
		   @remessa,                     
		   subramo_id,                    
		   certificado_id,          
		   endereco,                 
		   bairro,                     
		   municipio,                     
		   estado,                     
		   cep,                        
		   tipo_documento,                
		   codigo_barras,                
		   GETDATE(),                 
		   dt_alteracao,                  
		   usuario          
	  FROM interface_dados_db..SEGA8177_10_processar_tb with (nolock)            
          
	INSERT INTO interface_dados_db..SEGA8177_20_processado_tb          
		  (SEGA8177_processado_id,           
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   cost_certificado,          
		   num_certificado,          
		   const_segurado,          
		   nome_segurado,          
		   const_cpf_cnpj,          
		   cpf_cnpj,          
		   const_local_risco,          
		   endereco,          
		   const_bairro,          
		   bairro,          
		   const_municipio,          
		   municipio,          
		   const_cep,          
		   cep,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario)          
	SELECT SEGA8177_processar_id,           
		   proposta_id,          
		   layout_id,          
		   @remessa,       
		   subramo_id,          
		   certificado_id,          
		   cost_certificado,          
		   num_certificado,          
		   const_segurado,          
		   nome_segurado,          
		   const_cpf_cnpj,          
		   cpf_cnpj,          
		   const_local_risco,          
		   endereco,          
		   const_bairro,          
		   bairro,          
		   const_municipio,          
		   municipio,          
		   const_cep,          
		   cep,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db..SEGA8177_20_processar_tb with (nolock)            
          
	INSERT INTO interface_dados_db..SEGA8177_21_processado_tb          
		  (SEGA8177_processado_id,           
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   constante_proposta,          
		   proposta_id,          
		   constante_apolice,          
		   apolice_id,          
		   constante_ramo,          
		   detalhe_ramo,          
		   constante_renova,          
		   certificado_anterior_id,          
		   constante_forma_pgto,          
		   forma_pgto,          
		   dt_inicio_vigencia_seg,          
		   dt_fim_vigencia_seg,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario,
		   --Demanda SD01372169 - IN�CIO
		   produto_id,
		   nome_produto,
		   num_processo_susep)
		   --Demanda SD01372169 - FIM
	SELECT SEGA8177_processar_id,           
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   constante_proposta,          
		   proposta_id,          
		   constante_apolice,          
		   apolice_id,          
		   constante_ramo,          
		   detalhe_ramo,          
		   constante_renova,          
		   certificado_anterior_id,          
		   constante_forma_pgto,          
		   forma_pgto,          
		   dt_inicio_vigencia_seg,          
		   dt_fim_vigencia_seg,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario,
		   --Demanda SD01372169 - IN�CIO
		   produto_id,
		   nome_produto,
		   num_processo_susep
		   --Demanda SD01372169 - FIM		             
	  FROM interface_dados_db..SEGA8177_21_processar_tb  with (nolock)            
          
	INSERT INTO interface_dados_db..SEGA8177_22_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   subramo_id,        
		   certificado_id,          
		   tp_cobertura_id,          
		   nome,          
		   moeda_cobertura,          
		   val_is,          
		   usuario,          
		   dt_inclusao,          
		   dt_alteracao,
		   val_premio_cob  --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS          
		   )
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   tp_cobertura_id,          
		   nome,          
		   moeda_cobertura,          
		   val_is,          
		   usuario,          
		   GETDATE(),          
		   dt_alteracao, 
		   val_premio_cob  --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS          	   
	  FROM interface_dados_db..SEGA8177_22_processar_tb with (nolock)            
          
	INSERT INTO interface_dados_db..SEGA8177_23_processado_tb          
		  (SEGA8177_processado_id,          
		   SEGA8177_23_processar_id,
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   descricao_franquia,          
		   detalhe_franquia,          
		   usuario,          
		   dt_inclusao,          
		   dt_alteracao)
	SELECT SEGA8177_processar_id,          
		   SEGA8177_23_processar_id,
		   proposta_id,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   descricao_franquia,          
		   detalhe_franquia,                 
		   usuario,          
		   GETDATE(),          
		   dt_alteracao
	  FROM interface_dados_db..SEGA8177_23_processar_tb with (nolock)            
        
	INSERT INTO interface_dados_db..SEGA8177_24_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   certificado_id,          
		   subramo_id,          
		   moeda_premio_tarifa,          
		   val_premio_tarifa,          
		   moeda_desconto,          
		   desconto_progres,          
		   moeda_desconto_renov,          
		   desconto_renov,          
		   moeda_desconto_shopping,          
		   desconto_shopping,          
		   moeda_desconto_fidelidade,          
		   desconto_fidelidade,          
		   moeda_premio_liquido,          
		   premio_liquido,          
		   moeda_adicional_fracionamento,          
		   val_juros,          
		   moeda_custo,          
		   custo_certificado,          
		   moeda_iof,          
		   val_iof,          
		   moeda_premio_bruto,          
		   val_premio_bruto,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario,
		   --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS
		   moeda_custo_assist,
		   val_custo_assist
		   --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS
			)  	   
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   layout_id,          
		   @remessa,          
		   certificado_id,          
		   subramo_id,          
		   moeda_premio_tarifa,          
		   val_premio_tarifa,          
		   moeda_desconto,          
		   desconto_progres,          
		   moeda_desconto_renov,          
		   desconto_renov,          
		   moeda_desconto_shopping,          
		   desconto_shopping,          
		   moeda_desconto_fidelidade,          
		   desconto_fidelidade,          
		   moeda_premio_liquido,          
		   premio_liquido,          
		   moeda_adicional_fracionamento,          
		   val_juros,          
		   moeda_custo,          
		   custo_certificado,          
		   moeda_iof,          
		   val_iof,          
		   moeda_premio_bruto,          
		   val_premio_bruto,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario ,
		   --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS
		   moeda_custo_assist,
		   val_custo_assist
		   --FLOWBR18402795--GUILHERMECRUZ--CONFITEC SISTEMAS	   
	  FROM interface_dados_db..SEGA8177_24_processar_tb   with (nolock)          
          
	INSERT INTO interface_dados_db..SEGA8177_25_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   const_pagamento,          
		   const_prim_parcela,          
		   moeda_val_prim_parcela,          
		   val_prim_parcela,          
		   const_demais_parcelas,          
		   moeda_demais_parcelas,          
		   val_demais_parcelas,          
		   const_vencimento,          
		   prim_vencimento,          
		   seg_vencimento,          
		   ter_vencimento,          
		   qua_vencimento,          
		   qui_vencimento,          
		   sex_vencimento,          
		   set_vencimento,
		   --INICIO DEMANDA 19471884 - CORRE��O DO PARCELAMENTO DO SEGA8177 - APINHEIRO - 20/12/2016 - CONFITEC SP
		   oit_vencimento,          
		   non_vencimento,          
		   dec_vencimento,
		   --FIM DEMANDA 19471884 - CORRE��O DO PARCELAMENTO DO SEGA8177 - APINHEIRO - 20/12/2016 - CONFITEC SP  
		   --Demanda SD02351074 - IN�CIO 
		   dec_prim_vencimento,	
		   dec_seg_vencimento,	
		   dec_ter_vencimento,	
		   dec_qua_vencimento,	
		   dec_qui_vencimento,	
		   dec_sex_vencimento,	
		   dec_set_vencimento,	
		   dec_oit_vencimento,	
		   --Demanda SD02351074 - FIM    		           
		   usuario,          
		   dt_inclusao,          
		   dt_alteracao)          
	SELECT SEGA8177_processar_id,          
		  proposta_id,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   const_pagamento,          
		   const_prim_parcela,          
		   moeda_val_prim_parcela,          
		   val_prim_parcela,          
		   const_demais_parcelas,          
		   moeda_demais_parcelas,          
		   val_demais_parcelas,          
		   const_vencimento,          
		   prim_vencimento,          
		   seg_vencimento,          
		   ter_vencimento,          
		   qua_vencimento,          
		   qui_vencimento,          
		   sex_vencimento,          
		   set_vencimento,
		   --INICIO DEMANDA 19471884 - CORRE��O DO PARCELAMENTO DO SEGA8177 - APINHEIRO - 20/12/2016 - CONFITEC SP
		   oit_vencimento,          
		   non_vencimento,          
		   dec_vencimento,
		   --FIM DEMANDA 19471884 - CORRE��O DO PARCELAMENTO DO SEGA8177 - APINHEIRO - 20/12/2016 - CONFITEC SP  
		   --Demanda SD02351074 - IN�CIO 
		   dec_prim_vencimento,	
		   dec_seg_vencimento,	
		   dec_ter_vencimento,	
		   dec_qua_vencimento,	
		   dec_qui_vencimento,	
		   dec_sex_vencimento,	
		   dec_set_vencimento,	
		   dec_oit_vencimento,	
		   --Demanda SD02351074 - FIM        
		   usuario,          
		   GETDATE(),          
		   dt_alteracao          
	  FROM interface_dados_db..SEGA8177_25_processar_tb  with (nolock)           
            
	INSERT INTO interface_dados_db..SEGA8177_26_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   nome_corretor,          
		   corretor_id,          
		   constante_cobertura,          
		   descricao_bem,          
		   emissao_apolice,          
		   constante_construcao,          
		   descricao_construcao,          
		   constante_atividade,          
		   descricao_atividade,
		   --Demanda SD01639731 - IN�CIO
		   constante_assistencia,
		   descricao_assistencia,
		   constante_objeto_seguro,
		   descricao_objeto_seguro,
		   --Demanda SD01639731 - FIM     		       
		   dt_inclusao,          
		   usuario,
		   num_processo_susep) --JOAO.MACHADO
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   nome_corretor,          
		   corretor_id,          
		   constante_cobertura,          
		   descricao_bem,          
		   emissao_apolice,          
		   constante_construcao,          
		   descricao_construcao,         
		   constante_atividade,          
		   descricao_atividade,
		   --Demanda SD01639731 - IN�CIO
		   constante_assistencia,
		   descricao_assistencia,
		   constante_objeto_seguro,
		   descricao_objeto_seguro,
		   --Demanda SD01639731 - FIM		          
		   GETDATE(),          
		   usuario,
		   num_processo_susep --JOAO.MACHADO
	  FROM interface_dados_db..SEGA8177_26_processar_tb  with (nolock)           
          
	INSERT INTO interface_dados_db..SEGA8177_30_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   linha_digitavel,          
		   local_pagamento,          
		   vencimento,          
		   cedente,          
		   codigo_agencia_conta_corrente,          
		   data_documento,          
		   numero_documento,          
		   especie_documento,          
		   aceite,          
		   data_processamento,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario,          
		   ramo_linha_dig,          
		   apolice_linha_dig,          
		   endosso_linha_dig,          
		   parcela_linha_dig,          
		   nosso_numero_linha_dig,          
		   dt_vencimento_linha_dig,          
		   valor_linha_dig,          
		   agencia_linha_dig,          
		   carteira_linha_dig,  
		   convenio_linha_dig,     
		   conta_linha_dig
		   --- cristovao.rodrigues 28/10/2016 19368999/19370584 - cobrança registrada AB e ABS
		   ,cpf_cnpj_cedente	
		   ,endereco_cedente	
		   ,bairro_cedente		
		   ,municipio_cedente	
		   ,uf_cedente			
		   ,cep_endereco_cedente
		   )          
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   linha_digitavel,          
		   local_pagamento,          
		   vencimento,          
		   cedente,          
		   codigo_agencia_conta_corrente,          
		   data_documento,          
		   numero_documento,          
		   especie_documento,          
		   aceite,          
		   data_processamento,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario,          
		   ramo_linha_dig,          
		   apolice_linha_dig,          
		   endosso_linha_dig,          
		   parcela_linha_dig,          
		   nosso_numero_linha_dig,          
		   dt_vencimento_linha_dig,          
		   valor_linha_dig,          
		   agencia_linha_dig,          
		   carteira_linha_dig,          
		   convenio_linha_dig,          
		   conta_linha_dig     
		   --- cristovao.rodrigues 28/10/2016 19368999/19370584 - cobrança registrada AB e ABS
		   ,cpf_cnpj_cedente	
		   ,endereco_cedente	
		   ,bairro_cedente		
		   ,municipio_cedente	
		   ,uf_cedente			
		   ,cep_endereco_cedente
		         
	  FROM interface_dados_db..SEGA8177_30_processar_tb  with (nolock)           
          
	INSERT INTO interface_dados_db..SEGA8177_31_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   nosso_numero,          
		   numero_conta,          
		   carteira,          
		   especie,          
		   quantidade,          
		   valor_unitario,          
		   valor_documento,          
		   codigo_barra_boleto,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario)          
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   nosso_numero,          
		   numero_conta,          
		   carteira,          
		   especie,          
		   quantidade,          
		   valor_unitario,          
		   valor_documento,          
		   codigo_barra_boleto,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db..SEGA8177_31_processar_tb  with (nolock)           
          
	INSERT INTO interface_dados_db..SEGA8177_33_processado_tb          
		  (SEGA8177_processado_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   remessa,          
		   subramo_id,          
		   certificado_id,          
		   nome_segurado,          
		   endereco_cobranca,          
		   municipio_cobranca_UF_CEP,          
		   dt_inclusao,          
		   dt_alteracao,          
		   usuario)          
	SELECT SEGA8177_processar_id,          
		   proposta_id,          
		   num_cobranca,          
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   nome_segurado,          
		   endereco_cobranca,          
		   municipio_cobranca_UF_CEP,          
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db..SEGA8177_33_processar_tb  with (nolock)           
        
	INSERT INTO interface_dados_db..SEGA8177_34_processado_tb          
		  (SEGA8177_processado_id,        
		   proposta_id,        
		   layout_id,        
		   remessa,        
		   subramo_id,             
		   certificado_id,         
		   seq_beneficiario,        
		   constante_beneficiario,   
		   nome_beneficiario,        
		   cpf_cnpj_beneficiario,                          
		   perc_participacao,                          
		   dt_inclusao,     
		   dt_alteracao,     
		   usuario)        
	SELECT SEGA8177_processar_id,          
		   proposta_id,            
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   seq_beneficiario,        
		   constante_beneficiario,        
		   nome_beneficiario,        
		   cpf_cnpj_beneficiario,                          
		   perc_participacao,             
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db..SEGA8177_34_processar_tb      with (nolock)       

	--Demanda SD01570189 - IN�CIO
	INSERT INTO interface_dados_db.dbo.SEGA8177_35_processado_tb          
		  (SEGA8177_processado_id,        
		   proposta_id,        
		   layout_id,        
		   remessa,        
		   subramo_id,             
		   certificado_id,         
		   seq_linha,	
		   texto_exigencia,
		   dt_inclusao,     
		   dt_alteracao,     
		   usuario)        
	SELECT SEGA8177_processar_id,          
		   proposta_id,            
		   layout_id,          
		   @remessa,          
		   subramo_id,          
		   certificado_id,          
		   seq_linha,	
		   texto_exigencia,            
		   GETDATE(),          
		   dt_alteracao,          
		   usuario          
	  FROM interface_dados_db.dbo.SEGA8177_35_processar_tb With (Nolock)     
	--Demanda SD01570189 - FIM
          
	DELETE FROM interface_dados_db..SEGA8177_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_10_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_20_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_21_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_22_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_23_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_24_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_25_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_26_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_30_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_31_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_33_processar_tb         
	DELETE FROM interface_dados_db..SEGA8177_34_processar_tb            
	DELETE FROM interface_dados_db..SEGA8177_35_processar_tb	--Demanda SD01570189
            
	DROP TABLE #evento_impressao            
	DROP TABLE #evento_impressao2            
            
END TRY                
                
BEGIN CATCH                
                
Select      @error_message  = error_message(),                
            @error_severity = error_severity() ,                 
            @error_state    = error_state()                
                
RAISERROR (@error_message ,@error_severity, @error_state)                  
                
END CATCH




