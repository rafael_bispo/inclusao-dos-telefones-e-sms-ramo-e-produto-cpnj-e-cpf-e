CREATE PROCEDURE dbo.SEGS5594_SPI
    @layout_id INT,        
    @arquivo_remessa_grf VARCHAR(50),          
    @dt_geracao_arquivo SMALLDATETIME,          
    @remessa INT,          
    @producao CHAR(1),          
    @usuario VARCHAR(20)        
AS        

DECLARE  @error_message  nvarchar (4000),                
         @error_severity int ,                 
         @error_state    int;                

BEGIN TRY    
        
	DECLARE @motivo VARCHAR(60),          
			@sub_grupo_id INT,          
			@diretoria_id TINYINT,          
			@usuario_solicitante VARCHAR(20),          
			@tp_documento_id SMALLINT,          
			@status CHAR(1),          
			@documento CHAR(1),              
			@local_destino VARCHAR(60),          
			@local_contato VARCHAR(60),          
			@dt_solicitacao SMALLDATETIME,          
			@destino CHAR(1)          
          
	SET @motivo = NULL          
	SET @sub_grupo_id = 0          
	SET @diretoria_id = NULL          
	SET @usuario_solicitante = NULL          
	SET @tp_documento_id = 22          
	SET @status = 'i'          
	SET @documento = 'C'          
	SET @local_destino = ''          
	SET @local_contato = ''          
	SET @dt_solicitacao = getDate()          
	SET @destino = 'C'          
          
	SET NOCOUNT ON          
          
	--Inserindo 1� Via           
        
	 SELECT SEGA8178_processar_tb.proposta_id,          
			num_cobranca = NULL,          
			SEGA8178_processar_tb.endosso_id,          
			proposta_fechada_tb.proposta_bb,          
			sinistro_id = NULL,          
			usuario_aprovacao = CAST(NULL AS VARCHAR(20)),          
			dt_aprovacao = CAST(NULL AS SMALLDATETIME),          
			qtd_vias = NULL,          
			num_solicitacao = NULL          
	   INTO #evento_impressao          
	   FROM interface_dados_db..SEGA8178_processar_tb SEGA8178_processar_tb  With(Nolock)      
	  INNER JOIN proposta_fechada_tb With(Nolock)        
		 ON proposta_fechada_tb.proposta_id = SEGA8178_processar_tb.proposta_id        
	  WHERE SEGA8178_processar_tb.via <> 2          
          
	EXEC evento_seguros_db.dbo.evento_impressao_generica_spi @diretoria_id,          
															 @usuario_solicitante,          
															 @tp_documento_id,          
															 @status,          
															 @motivo,          
															 @sub_grupo_id,          
															 @documento,          
															 @dt_solicitacao,          
															 @destino,          
															 @local_destino,          
															 @local_contato,          
															 @dt_geracao_arquivo,          
															 @arquivo_remessa_grf,          
															 @usuario,          
															 @producao           
        
	-- 2� via           
	SELECT evento_impressao_tb.num_solicitacao          
	  INTO #evento_impressao2          
	  FROM interface_dados_db..SEGA8178_processar_tb SEGA8178_processar_tb  With(Nolock)      
	 INNER JOIN evento_seguros_db..evento_impressao_tb evento_impressao_tb  With(Nolock)           
		ON SEGA8178_processar_tb.proposta_id = evento_impressao_tb.proposta_id            
	 WHERE SEGA8178_processar_tb.via = 2        
	   AND evento_impressao_tb.status = 'L'            
	   AND evento_impressao_tb.dt_geracao_arquivo IS NULL            
	   AND evento_impressao_tb.tp_documento_id IN (@tp_documento_id)         
          
	IF @producao = 'S'          
	BEGIN          
        
		UPDATE evento_impressao_tb          
		   SET dt_geracao_arquivo = @dt_geracao_arquivo,          
			   arquivo_remessa_grf = @arquivo_remessa_grf,          
			   dt_alteracao = GETDATE(),          
			   dt_solicitacao = @dt_solicitacao,    
			   usuario = @usuario,
			   status = @status          
		  FROM #evento_impressao2          
		 INNER JOIN evento_seguros_db.dbo.evento_impressao_tb evento_impressao_tb With(Nolock)         
			ON #evento_impressao2.num_solicitacao = evento_impressao_tb.num_solicitacao        
		 WHERE #evento_impressao2.num_solicitacao > 0          
        
	END          
	ELSE          
	BEGIN          
        
		UPDATE evento_impressao_tb          
		   SET dt_geracao_arquivo = @dt_geracao_arquivo,          
			   arquivo_remessa_grf = @arquivo_remessa_grf,          
			   dt_alteracao = GETDATE(),          
			   dt_solicitacao = @dt_solicitacao,    
			   usuario = @usuario,
			   status = @status            
		   FROM #evento_impressao2 
		  INNER JOIN seguros_temp_db.dbo.evento_impressao_tb evento_impressao_tb  With(Nolock)        
			 ON #evento_impressao2.num_solicitacao = evento_impressao_tb.num_solicitacao          
		   WHERE #evento_impressao2.num_solicitacao > 0          
        
	END          
          
	-- transferindo as informa��es processadas -------------------------------------------------          
        
	INSERT INTO interface_dados_db..SEGA8178_processado_tb        
		  (SEGA8178_processar_id,        
		   proposta_id,        
		   apolice_id,        
		   ramo_id,        
		   subramo_id,        
		   produto_id,        
		   endosso_id,        
		   agencia_id,        
		   cliente_id,        
		   via,        
		   layout_id,        
		   remessa,        
		   dt_inclusao,        
		   usuario)        
	SELECT SEGA8178_processar_id,         
		   proposta_id,        
		   apolice_id,        
		   ramo_id,        
		   subramo_id,        
		   produto_id,        
		   endosso_id,        
		   agencia_id,        
		   cliente_id,        
		   via,        
		   layout_id,        
		   @remessa,        
		   GETDATE(),        
		   usuario        
	  FROM interface_dados_db..SEGA8178_processar_tb  With(Nolock)      
        
	INSERT INTO interface_dados_db..SEGA8178_10_processado_tb        
		  (SEGA8178_processar_id,        
		   layout_id,        
		   remessa,        
		   proposta_id,        
		   nome_destinatario,        
		   endereco_destinatario,        
		   bairro_destino,        
		   municipio_destino,        
		   UF_destino,        
		   CEP_destino,        
		   constante_AC,        
		   aos_cuidados_de,        
		   const_ref,        
		   nome_segurado,        
		   codigo_barras,        
		   numero_AR,        
		   dt_inclusao,        
		   usuario)        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   nome_destinatario,        
		   endereco_destinatario,        
		   bairro_destino,        
		   municipio_destino,        
		   UF_destino,        
		   CEP_destino,        
		   constante_AC,        
		   aos_cuidados_de,        
		   const_ref,        
		   nome_segurado,        
		   codigo_barras,        
		   numero_AR,        
		   GETDATE(),        
		   usuario        
	  FROM interface_dados_db..SEGA8178_10_processar_tb With(Nolock)       
        
	INSERT INTO interface_dados_db..SEGA8178_20_processado_tb        
		  (SEGA8178_processar_id,        
		   layout_id,     
		   remessa,        
		   proposta_id,        
		   num_apolice,        
		   descricao_documento,        
		   constante_segurado,        
		   nome_segurado,        
		   const_CPF_CNPJ,       
		   CPF_CNPJ,        
		   const_endereco,        
		   endereco_segurado,        
		   bairro_segurado,        
		   const_municipio,        
		   municipio_UF,        
		   const_CEP,        
		   CEP_segurado,        
		   codigo_produto,        
		   nome_produto,        
		   endereco_risco,        
		   dt_inicio_vigencia,        
		   dt_fim_vigencia,        
		   limite_responsabilidade,        
		   num_endosso,        
		   proposta_renovacao,        
		   proposta_BB,        
		   nome_agencia,        
		   nome_agencia_cobranca,        
		   val_iof,        
		   val_custo_apolice,        
		   val_juros,        
		   val_premio_liquido,        
		   val_premio_bruto,        
		   const_qt_parcelas,        
		   quantidade_parcelas,        
		   const_parcela_1,        
		   val_parcela_1,        
		   const_demais_parcelas,        
		   val_demais_parcelas,        
		   const_ultima_parcela,        
		   val_ultima_parcela,        
		   dt_vencimento_1,        
		   dt_vencimento_2,        
		   dt_vencimento_3,        
		   dt_vencimento_4,        
		   dt_vencimento_5,        
		   dt_vencimento_6,        
		   dt_vencimento_7,        
		   dt_vencimento_8,        
		   dt_vencimento_9,        
		   dt_vencimento_10,        
		   dt_vencimento_11,        
		   dt_vencimento_12,        
		   forma_cobranca,        
		   cod_ramo,        
		   nome_ramo,        
		   cod_SUSEP_corretor,        
		   nome_corretor,        
		   inspetoria,        
		   observacao_1,        
		   observacao_2,        
		   observacao_3,        
		   observacao_4,        
		   observacao_5,        
		   observacao_6,        
		   atividade_principal,        
		   grupo_ramo,        
		   cod_agencia_contrante,        
		   nome_agencia_contrante,        
		   municipio_risco,        
		   estado_risco,        
		   const_processo_susep,        
		   cod_susep,        
		   dt_contratacao,        
		   dt_inclusao,        
		   usuario,        
		   nome_cobertura ,        
		   vl_is_cobertura ,        
		   texto_franquia_cobertura,  
		   nome_beneficiario,  
		   cpf_cnpj_beneficiario,
		   -- Demanda SD01544345 - IN�CIO
		   dt_emissao_apolice
		   -- Demanda SD01544345 - FIM
	)        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   num_apolice,        
		   descricao_documento,        
		   constante_segurado,        
		   nome_segurado,        
		   const_CPF_CNPJ,        
		   CPF_CNPJ,        
		   const_endereco,        
		   endereco_segurado,        
		   bairro_segurado,        
		   const_municipio,        
		   municipio_UF,        
		   const_CEP,        
		   CEP_segurado,        
		   codigo_produto,        
		   nome_produto,        
		   endereco_risco,        
		   dt_inicio_vigencia,        
		   dt_fim_vigencia,        
		   limite_responsabilidade,        
		   num_endosso,        
		   proposta_renovacao,        
		   proposta_BB,        
		   nome_agencia,        
		   nome_agencia_cobranca,        
		   val_iof,        
		   val_custo_apolice,        
		   val_juros,        
		   val_premio_liquido,        
		   val_premio_bruto,        
		   const_qt_parcelas,        
		   quantidade_parcelas,        
		   const_parcela_1,        
		   val_parcela_1,        
		   const_demais_parcelas,        
		   val_demais_parcelas,        
		   const_ultima_parcela,        
		   val_ultima_parcela,        
		   dt_vencimento_1,        
		   dt_vencimento_2,        
		   dt_vencimento_3,        
		   dt_vencimento_4,        
		   dt_vencimento_5,        
		   dt_vencimento_6,        
		   dt_vencimento_7,        
		   dt_vencimento_8,        
		   dt_vencimento_9,        
		   dt_vencimento_10,        
		   dt_vencimento_11,        
		   dt_vencimento_12,        
		   forma_cobranca,        
		   cod_ramo,        
		   nome_ramo,        
		   cod_SUSEP_corretor,        
		   nome_corretor,        
		   inspetoria,        
		   observacao_1,        
		   observacao_2,        
		   observacao_3,        
		   observacao_4,        
		   observacao_5,        
		   observacao_6,        
		   atividade_principal,        
		   grupo_ramo,        
		   cod_agencia_contrante,        
		   nome_agencia_contrante,        
		   municipio_risco,        
		   estado_risco,        
		   const_processo_susep,        
		   cod_susep,        
		   dt_contratacao,        
		   GETDATE(),        
		   usuario,        
		   nome_cobertura ,        
		   vl_is_cobertura ,        
		   texto_franquia_cobertura,  
		   nome_beneficiario,  
		   cpf_cnpj_beneficiario,
		   -- Demanda SD01544345 - IN�CIO
		   dt_emissao_apolice
		   -- Demanda SD01544345 - FIM
	  FROM interface_dados_db..SEGA8178_20_processar_tb   With(Nolock)     
        
	
	--18402795 leonardo.santos 20/04/2015
INSERT INTO interface_dados_db.dbo.SEGA8178_23_processado_tb                
			(SEGA8178_processar_id      ,  
			SEGA8178_23_processar_id   ,          
			layout_id                  ,            
			proposta_id                ,            
			num_apolice                ,            
			const_val_premio_cobertura ,
			val_premio_cobertura       ,
			const_custo_assist         ,
			val_custo_assit            ,
			const_nome_cosseg          ,
			nome_cosseg                ,
			const_percentual_cosseg    ,
			percentual_cosseg          ,
			const_processo_susep_cosseg,
			processo_susep_cosseg      ,
			const_cnpj_cosseg          ,
			cnpj_cosseg                ,
			dt_inclusao                ,
			usuario                    
			)            
	 SELECT SEGA8178_processar_id      ,            
			SEGA8178_23_processar_id   ,
			layout_id                  ,            
			proposta_id                ,            
			num_apolice                ,            
			const_val_premio_cobertura ,
			val_premio_cobertura       ,
			const_custo_assist         ,
			val_custo_assit            ,
			const_nome_cosseg          ,
			nome_cosseg                ,
			const_percentual_cosseg    ,
			percentual_cosseg          ,
			const_processo_susep_cosseg,
			processo_susep_cosseg      ,
			const_cnpj_cosseg          ,
			cnpj_cosseg                ,
			dt_inclusao = GETDATE()    ,
			usuario                    
		FROM interface_dados_db.dbo.SEGA8178_23_processar_tb With(Nolock)


	INSERT INTO interface_dados_db..SEGA8178_21_processado_tb        
	      (SEGA8178_processar_id,        
		   layout_id,        
		   remessa,        
		   proposta_id,        
		   seq_linha,        
		   tp_cobertura_id,        
		   texto_cobertura,        
		   dt_inclusao,        
		   usuario)        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   seq_linha,        
		   tp_cobertura_id,        
		   texto_cobertura,        
		   GETDATE(),        
		   usuario        
	  FROM interface_dados_db..SEGA8178_21_processar_tb   With(Nolock)     
        
	INSERT INTO interface_dados_db..SEGA8178_22_processado_tb        
		  (SEGA8178_processar_id,        
		   layout_id,        
		   remessa,        
		   proposta_id,        
		   seq_linha,        
		   seq_clausula,        
		   texto_clausula,        
		   dt_inclusao,        
		   usuario)        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   seq_linha,        
		   seq_clausula,        
		   texto_clausula,        
		   GETDATE(),        
		   usuario        
	  FROM interface_dados_db..SEGA8178_22_processar_tb With(Nolock)  
        
	INSERT INTO interface_dados_db..SEGA8178_61_processado_tb        
		  (SEGA8178_processar_id,        
		   layout_id,        
		   remessa,        
		   proposta_id,        
		   pergunta_id,        
		   questionario_id,        
		   subramo,        
		   apolice,        
		   texto,        
		   dt_inclusao,        
		   usuario)        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   pergunta_id,        
		   questionario_id,        
		   subramo,        
		   apolice,        
		   texto,        
		   GETDATE(),        
		   usuario        
	  FROM interface_dados_db..SEGA8178_61_processar_tb  With(Nolock) 
  
	INSERT INTO interface_dados_db..SEGA8178_62_processado_tb        
		  (SEGA8178_processar_id,        
		   layout_id,        
		   remessa,        
		   proposta_id,        
		   num_cobranca,        
		   linha_digitavel,        
		   local_pagamento,        
		   dt_vencimento,        
		   cedente,        
		   agencia_conta,        
		   dt_documento,        
		   num_documento,        
		   especie_documento,        
		   aceite,        
		   dt_processamento,        
		   nosso_numero,        
		   num_conta,        
		   carteira,        
		   especie,        
		   quantidade,        
		   val_unitario,        
		   val_documento,        
		   linha_1,        
		   linha_2,        
		   linha_3,        
		   linha_4,        
		   linha_5,        
		   sacado_1,        
		   sacado_2,        
		   sacado_3,        
		   cod_barras_boleto,        
		   produto,        
		   dt_inclusao,        
		   usuario,        
		   ramo_linha_dig,        
		   apolice_linha_dig,        
		   endosso_linha_dig,        
		   parcela_linha_dig,        
		   nosso_numero_linha_dig,        
		   dt_vencimento_linha_dig,        
		   valor_linha_dig,        
		   agencia_linha_dig,        
		   carteira_linha_dig,        
		   convenio_linha_dig,        
		   conta_linha_dig
		   -- cristovao.rodrigues 28/10/2016 19368999/19370584 - cobran�a registrada AB e ABS
		   , cpf_cnpj_cedente	
		   , endereco_cedente	
		   , bairro_cedente		
		   , municipio_UF_cedente
		   , cep_endereco_cedente 
		   )        
	SELECT SEGA8178_processar_id,        
		   layout_id,        
		   @remessa,        
		   proposta_id,        
		   num_cobranca,        
		   linha_digitavel,        
		   local_pagamento,        
		   dt_vencimento,        
		   cedente,        
		   agencia_conta,        
		   dt_documento,        
		   num_documento,        
		   especie_documento,        
		   aceite,        
		   dt_processamento,        
		   nosso_numero,        
		   num_conta,        
		   carteira,        
		   especie,        
		   quantidade,        
		   val_unitario,        
		   val_documento,        
		   linha_1,        
		   linha_2,        
		   linha_3,        
		   linha_4,        
		   linha_5,        
		   sacado_1,        
		   sacado_2,        
		   sacado_3,        
		   cod_barras_boleto,        
		   produto,        
		   GETDATE(),        
		   usuario,        
		   ramo_linha_dig,        
		   apolice_linha_dig,        
		   endosso_linha_dig,        
		   parcela_linha_dig,        
		   nosso_numero_linha_dig,        
		   dt_vencimento_linha_dig,        
		   valor_linha_dig,        
		   agencia_linha_dig,        
		   carteira_linha_dig,        
		   convenio_linha_dig,        
		   conta_linha_dig
		   -- cristovao.rodrigues 28/10/2016 19368999/19370584 - cobran�a registrada AB e ABS
		   , cpf_cnpj_cedente	
		   , endereco_cedente	
		   , bairro_cedente		
		   , municipio_UF_cedente
		   , cep_endereco_cedente 		           
	  FROM interface_dados_db..SEGA8178_62_processar_tb With(Nolock)   
        
	DELETE FROM interface_dados_db..SEGA8178_10_processar_tb          
	DELETE FROM interface_dados_db..SEGA8178_20_processar_tb          
	DELETE FROM interface_dados_db..SEGA8178_21_processar_tb          
	DELETE FROM interface_dados_db..SEGA8178_22_processar_tb           
	DELETE FROM interface_dados_db..SEGA8178_23_processar_tb  
	DELETE FROM interface_dados_db..SEGA8178_61_processar_tb          
	DELETE FROM interface_dados_db..SEGA8178_62_processar_tb        
	DELETE FROM interface_dados_db..SEGA8178_processar_tb          
          
	DROP TABLE #evento_impressao          
	DROP TABLE #evento_impressao2          

END TRY                
                
BEGIN CATCH                
	SELECT @error_message  = error_message(),                
		   @error_severity = error_severity() ,                 
		   @error_state    = error_state()                
	RAISERROR (@error_message ,@error_severity, @error_state)                  
END CATCH      




