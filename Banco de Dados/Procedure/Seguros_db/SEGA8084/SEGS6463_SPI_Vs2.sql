
CREATE PROCEDURE dbo.SEGS6463_SPI
  
@usuario          VARCHAR(20)  
  
AS  
  
SET NOCOUNT ON   
  
DECLARE @msg              VARCHAR(100)  
  
INSERT INTO interface_dados_db..SEGA8084_20_processado_tb  
   (SEGA8084_processar_id,
    SEGA8084_20_processar_id,
    proposta_id,
    proposta_bb,
    apolice_id,
    nome_segurado,
    endereco_segurado,
    municipio_segurado,
    uf_segurado,
    cep_segurado,
    endereco_risco,
    municipio_risco,
    uf_risco,
    cep_risco,
    nome_produto,
    dt_vencimento_seguro,
    dt_inicio_vigencia,
    dt_fim_vigencia,
    num_processo_susep,
    cpf_cnpj,
    dt_inclusao,
    usuario)
  
SELECT SEGA8084_processar_id,
       SEGA8084_20_processar_id,
       proposta_id,
       proposta_bb,
       apolice_id,
       nome_segurado,
       endereco_segurado,
       municipio_segurado,
       uf_segurado,
       cep_segurado,
       endereco_risco,
       municipio_risco,
       uf_risco,
       cep_risco,
       nome_produto,
       dt_vencimento_seguro,
       dt_inicio_vigencia,
       dt_fim_vigencia,
       num_processo_susep,
       cpf_cnpj,
       dt_inclusao = GETDATE(),
       usuario = @usuario
FROM interface_dados_db..SEGA8084_20_processar_tb
  
IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na inclus�o em SEGA8084_20_processado_tb'  
     GOTO error                    
  END  
  
SET NOCOUNT OFF  
  
RETURN                    
                    
error:                    

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Inicio --
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Fim --




