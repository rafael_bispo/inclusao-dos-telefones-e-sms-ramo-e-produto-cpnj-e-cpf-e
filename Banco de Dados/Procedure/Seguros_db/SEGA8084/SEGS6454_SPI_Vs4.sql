CREATE PROCEDURE SEGS6454_SPI
  
@usuario    VARCHAR(20)  
  
AS  
  
DECLARE @msg             VARCHAR(100)  
  
SET NOCOUNT ON   
  
SET @msg = ''   
  
SELECT DISTINCT SEGA8084_processar_tb.SEGA8084_processar_id,  
                SEGA8084_processar_tb.proposta_id,   
                SEGA8084_processar_tb.proposta_bb,  
                SEGA8084_processar_tb.apolice_id,  
                segurado = CASE agencia_tb.agencia_id  
                               WHEN 4053 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               WHEN 3859 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               WHEN 4207 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               WHEN 4333 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               WHEN 4604 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               WHEN 4499 THEN 'Ao gerente de contas de: ' + cliente_tb.nome  
                               ELSE cliente_tb.nome  
                           END,  
                endereco = LEFT(  
                            RTRIM(LTRIM(  
                                        CASE agencia_tb.agencia_id  
                                          WHEN 4053 THEN ISNULL(agencia_tb.endereco, '')  
                                          WHEN 3859 THEN ISNULL(agencia_tb.endereco, '')  
                                          WHEN 4207 THEN ISNULL(agencia_tb.endereco, '')  
                                          WHEN 4333 THEN ISNULL(agencia_tb.endereco, '')  
                                          WHEN 4604 THEN ISNULL(agencia_tb.endereco, '')  
                                          WHEN 4499 THEN ISNULL(agencia_tb.endereco, '')  
                                          ELSE ISNULL(endereco_corresp_tb.endereco, '')  
                                        END  
                                   ))   
                            + ' ' +  
                            RTRIM(LTRIM(  
                                        CASE agencia_tb.agencia_id  
                                          WHEN 4053 THEN ISNULL(agencia_tb.bairro, '')  
                                          WHEN 3859 THEN ISNULL(agencia_tb.bairro, '')  
                                          WHEN 4207 THEN ISNULL(agencia_tb.bairro, '')  
                                          WHEN 4333 THEN ISNULL(agencia_tb.bairro, '')  
                                          WHEN 4604 THEN ISNULL(agencia_tb.bairro, '')  
                                          WHEN 4499 THEN ISNULL(agencia_tb.bairro, '')  
                                          ELSE ISNULL(endereco_corresp_tb.bairro, '')  
                                        END  
                                   ))  
                            + SPACE(60)  
                           ,60),  
                municipio = CASE agencia_tb.agencia_id  
                              WHEN 4053 THEN ISNULL(municipio_tb.nome, '')  
                              WHEN 3859 THEN ISNULL(municipio_tb.nome, '')  
                              WHEN 4207 THEN ISNULL(municipio_tb.nome, '')  
                              WHEN 4333 THEN ISNULL(municipio_tb.nome, '')  
                              WHEN 4604 THEN ISNULL(municipio_tb.nome, '')  
                              WHEN 4499 THEN ISNULL(municipio_tb.nome, '')  
                              ELSE ISNULL(endereco_corresp_tb.municipio, '')  
                            END,  
                estado = LEFT(  
                              CASE agencia_tb.agencia_id  
                                WHEN 4053 THEN ISNULL(agencia_tb.estado, '')  
                                WHEN 3859 THEN ISNULL(agencia_tb.estado, '')  
                                WHEN 4207 THEN ISNULL(agencia_tb.estado, '')  
                                WHEN 4333 THEN ISNULL(agencia_tb.estado, '')  
                                WHEN 4604 THEN ISNULL(agencia_tb.estado, '')  
                                WHEN 4499 THEN ISNULL(agencia_tb.estado, '')  
                                ELSE ISNULL(endereco_corresp_tb.estado, '')  
                              END  
                              + SPACE(2)  
                         ,60),  
                cep = LEFT(  
                           CASE agencia_tb.agencia_id  
                             WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')  
                             WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')  
                             WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')  
                             WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')  
                             WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')  
                             WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')  
                             ELSE ISNULL(endereco_corresp_tb.cep, '0')  
                           END  
                          ,5)  
                      + '-' +   
                      RIGHT(  
                            CASE agencia_tb.agencia_id  
                              WHEN 4053 THEN ISNULL(agencia_tb.cep, '0')  
                              WHEN 3859 THEN ISNULL(agencia_tb.cep, '0')  
                              WHEN 4207 THEN ISNULL(agencia_tb.cep, '0')  
                              WHEN 4333 THEN ISNULL(agencia_tb.cep, '0')  
                              WHEN 4604 THEN ISNULL(agencia_tb.cep, '0')  
                              WHEN 4499 THEN ISNULL(agencia_tb.cep, '0')  
                              ELSE ISNULL(endereco_corresp_tb.cep, '0')  
                            END  
                           ,3),  
                nome_produto = renovacao_monitorada_db..proposta_tb.nome_produto,  
                dt_cobranca_dia = ISNULL(financeiro_proposta_tb.data_cobranca_dia, 0),   
                dt_inicio_vigencia = ISNULL(financeiro_proposta_tb.dt_inicio_vigencia, ''),   
                dt_fim_vigencia = ISNULL(financeiro_proposta_tb.dt_fim_vigencia, ''),   
                num_proc_susep = ISNULL(subramo_tb.num_proc_susep, ''),  
                endereco_risco = CAST('' AS VARCHAR(60)),  
                municipio_risco = CAST('' AS VARCHAR(60)),  
                estado_risco = CAST('' AS VARCHAR(2)),  
                cep_risco = CAST('' AS VARCHAR(9)),
                cpf_cnpj = CAST('' AS VARCHAR(14)) 
INTO #dado_segurado  
FROM interface_dados_db..SEGA8084_processar_tb SEGA8084_processar_tb (NOLOCK)  
JOIN renovacao_monitorada_db..proposta_tb (NOLOCK)   
  ON renovacao_monitorada_db..proposta_tb.proposta_id = SEGA8084_processar_tb.proposta_id  
JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb (NOLOCK)   
  ON renovacao_monitorada_db..proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id   
JOIN subramo_tb (NOLOCK)   
  ON renovacao_monitorada_db..proposta_tb.ramo_id = subramo_tb.ramo_id   
 AND renovacao_monitorada_db..proposta_tb.subramo_id = subramo_tb.subramo_id   
 AND renovacao_monitorada_db..proposta_tb.dt_inicio_vigencia_sbr = subramo_tb.dt_inicio_vigencia_sbr   
JOIN renovacao_monitorada_db..cliente_tb cliente_tb (NOLOCK)   
  ON renovacao_monitorada_db..proposta_tb.cliente_id = cliente_tb.cliente_id   
LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)   
  ON renovacao_monitorada_db..proposta_tb.proposta_id = endereco_corresp_tb.proposta_id   
LEFT JOIN seguros_db..agencia_tb agencia_tb (NOLOCK)  
  ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id   
LEFT JOIN seguros_db..municipio_tb municipio_tb (NOLOCK)  
  ON agencia_tb.municipio_id = municipio_tb.municipio_id   
 AND agencia_tb.estado = municipio_tb.estado   
  
IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na sele��o dos dados de controle do segurado'  
     GOTO error  
  END  
  
--ObterEnderecoRiscoRenovacao  
UPDATE #dado_segurado  
SET endereco_risco = LEFT(  
                       ISNULL(renovacao_monitorada_db..endereco_risco_tb.endereco, '')  
                       + ' ' + ISNULL(renovacao_monitorada_db..endereco_risco_tb.bairro, '')  
                       + SPACE(60)  
                     ,60),  
    municipio_risco = ISNULL(renovacao_monitorada_db..endereco_risco_tb.municipio, '') ,   
    estado_risco = LEFT(ISNULL(renovacao_monitorada_db..endereco_risco_tb.estado, '') + SPACE(2),2) ,   
    cep_risco = LEFT(ISNULL(renovacao_monitorada_db..endereco_risco_tb.cep, '0'),5)  
                + '-' + RIGHT(ISNULL(renovacao_monitorada_db..endereco_risco_tb.cep, '0'),3)  
FROM renovacao_monitorada_db..endereco_risco_tb (NOLOCK)   
JOIN #dado_segurado  
  ON renovacao_monitorada_db..endereco_risco_tb.proposta_id = #dado_segurado.proposta_id  
  
IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na obten��o dos dados de risco'  
     GOTO error  
  END  
  
  
  -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #dado_segurado
set #dado_segurado.CPF_CNPJ = CASE 
									WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
									WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
									ELSE cliente_tb.cpf_cnpj
								END 
from #dado_segurado #dado_segurado
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #dado_segurado.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
  
  IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na obter cpf_cnpj'  
     GOTO error  
  END  
  
  
  
INSERT INTO interface_dados_db..SEGA8084_20_processar_tb  
   (SEGA8084_processar_id,  
    proposta_id,  
    proposta_bb,  
    apolice_id,  
    nome_segurado,  
    endereco_segurado,  
    municipio_segurado,  
    uf_segurado,  
    cep_segurado,  
    endereco_risco,  
    municipio_risco,  
    uf_risco,  
    cep_risco,  
    nome_produto,  
    dt_vencimento_seguro,  
    dt_inicio_vigencia,  
    dt_fim_vigencia,  
    num_processo_susep,
    cpf_cnpj,  
    dt_inclusao,  
    usuario)  
  
SELECT SEGA8084_processar_id,  
       proposta_id,  
       proposta_bb,  
       apolice_id,  
       CONVERT(VARCHAR(60),segurado),
       endereco,  
       municipio,  
       estado,  
       cep,  
       endereco_risco,  
       municipio_risco,  
       estado_risco,  
       cep_risco,  
       nome_produto,  
       dt_inicio_vigencia,  
       dt_inicio_vigencia,  
       dt_fim_vigencia,  
       num_proc_susep,  
       cpf_cnpj,  
       dt_inclusao = GETDATE(),  
       usuario = @usuario  
FROM #dado_segurado  
  
IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na inclus�o dos dados de segurado'  
     GOTO error                    
  END  
  
SET NOCOUNT OFF  
  
RETURN                    
                    
error:                    

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Inicio --
    --raiserror 55555 @msg
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Fim --






