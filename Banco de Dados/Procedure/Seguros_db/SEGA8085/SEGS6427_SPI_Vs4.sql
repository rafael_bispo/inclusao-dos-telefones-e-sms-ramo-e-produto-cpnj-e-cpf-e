ALTER PROCEDURE DBO.SEGS6427_SPI

@Usuario   VARCHAR(20)

AS

/*----------------------
--PARA TESTES
DECLARE @usuario    VARCHAR(20)
SET @usuario    = 'DEM 17833806' 
*/
SET NOCOUNT ON

BEGIN TRY
   --Criando tempor�ria de trabalho
   CREATE TABLE #SEGA8085_20_processar_tb(SEGA8085_20_processar_id INT IDENTITY,
                                          SEGA8085_processar_id    INT,
                                          proposta_ab  NUMERIC(9,0),
                                          proposta_id  NUMERIC(9,0),
                                          proposta_bb  NUMERIC(9,0),
                                          num_apolice  NUMERIC(9,0),
                                          segurado   VARCHAR(255),
                                          endereco_destino  VARCHAR(60),
                                          endereco_bairro_destino VARCHAR(90),              
                                          bairro_destino  CHAR(30),                
                                          municipio_destino VARCHAR(60),                
                                          estado_destino  VARCHAR(2),              
                                          cep_destino  VARCHAR(8),               
                                          cep_destino_formatado VARCHAR(9),
                                          endereco_risco        VARCHAR(60),
                                          endereco_bairro_risco VARCHAR(60),
                                          bairro_risco          VARCHAR(60),
                                          municipio_risco       VARCHAR(60),
                                          estado_risco          VARCHAR(2),
                                          cep_risco_formatado   VARCHAR(9),
                                          nome_produto          VARCHAR(60),
                                          dt_inicio_vigencia    SMALLDATETIME,
                                          dt_fim_vigencia       SMALLDATETIME,
                                          num_proc_susep    VARCHAR(30),
                                          cep_risco         VARCHAR(8),
                                          cpf_cnpj			VARCHAR(14))
  
   DECLARE @tp_documento_id              SMALLINT,
           @documento_id                 SMALLINT,
           @ROWCOUNT                     INT,
           @Data_sistema                 SMALLDATETIME,
           @canal_id                     INT,
           @msg                          VARCHAR(80),
           @contrato_postagem_atual      INT,
           @contrato_postagem_id         INT,
           @faixa_final                  INT,
           @tp_servico                   VARCHAR(2),              
           @num_maximo_postagem          VARCHAR(15),
           @incluir_proposta_postagem    INT
   
   SET @msg = ''
   
   --Obtendo a data do sistema
   SET @Data_sistema = (SELECT dt_operacional
                        FROM seguros_db..parametro_geral_tb)
   
   --Buscando o tipo do documento do produto  
   SELECT @tp_documento_id = ISNULL(tp_documento_id, 0),
           @documento_id = documento_id
   FROM evento_seguros_db..documento_tb WITH (NOLOCK)
   WHERE arquivo_saida = 'SEGA8085'

   SELECT @tp_documento_id = ISNULL(@tp_documento_id, 0)
   SELECT @documento_id = ISNULL(@documento_id, 0)
                 
   --Populando tempor�ria
   INSERT INTO #SEGA8085_20_processar_tb(SEGA8085_processar_id,
                                         proposta_ab,
                                         proposta_id,
                                         proposta_bb,
                                         num_apolice,
                                         segurado,
                                         endereco_destino,
                                         endereco_bairro_destino,
                                         bairro_destino,
                                         municipio_destino,
                                         estado_destino,
                                         cep_destino,               
                                         cep_destino_formatado,
                                         endereco_risco,              
                                         endereco_bairro_risco,          
                                         bairro_risco,                  
                                         municipio_risco,                 
                                         estado_risco,              
                                         cep_risco_formatado,           
                                         nome_produto,                 
                                         dt_inicio_vigencia,          
                                         dt_fim_vigencia,          
                                         num_proc_susep)              
                 
   SELECT DISTINCT SEGA8085_processar_tb.SEGA8085_processar_id,
                   SEGA8085_processar_tb.proposta_ab,
                   SEGA8085_processar_tb.proposta_id,
   
                   --emi_re_proposta_tb.proposta_bb, 
                   --certificado_re_tb.certificado_id AS apolice_id,
   
                   financeiro_proposta_tb.proposta_bb, --Zoro.Gomes - confitec - Retirar a emi - 18/09/2015
                   apolice_tb.apolice_id,  --Zoro.Gomes - confitec - 18/09/2015
   
                   SEGA8085_10_processar_tb.segurado,
                   SEGA8085_10_processar_tb.endereco_destino,
                   SEGA8085_10_processar_tb.endereco_bairro_destino,
                   SEGA8085_10_processar_tb.bairro_destino,
                   SEGA8085_10_processar_tb.municipio_destino,
                   SEGA8085_10_processar_tb.estado_destino,
                   cep = RIGHT(('00000000' + ISNULL(RTRIM(LTRIM(CONVERT(VARCHAR(8),SEGA8085_10_processar_tb.cep_destino))),'')),8),
                   SEGA8085_10_processar_tb.cep_destino_formatado,
                   endereco_risco = CAST('' AS VARCHAR(60)),
                   endereco_bairro_risco = CAST('' AS VARCHAR(60)),
                   bairro_risco = CAST('' AS VARCHAR(60)),                 
                   municipio_risco = CAST('' AS VARCHAR(60)),                 
                   estado_risco = CAST('' AS VARCHAR(2)),             
                   cep_risco_formatado = CAST('' AS VARCHAR(9)),
                   proposta_tb.nome_produto,
                   dt_inicio_vigencia = ISNULL(financeiro_proposta_tb.dt_inicio_vigencia, ''),
                   dt_fim_vigencia = ISNULL(financeiro_proposta_tb.dt_fim_vigencia, ''),
                   num_proc_susep = ISNULL(subramo_tb.num_proc_susep, '')       
   
   FROM interface_dados_db..SEGA8085_processar_tb SEGA8085_processar_tb WITH (NOLOCK)
   JOIN interface_dados_db..SEGA8085_10_processar_tb SEGA8085_10_processar_tb WITH (NOLOCK)
     ON SEGA8085_10_processar_tb.SEGA8085_processar_id = SEGA8085_processar_tb.SEGA8085_processar_id
   JOIN renovacao_monitorada_db..proposta_tb proposta_tb WITH (NOLOCK)               
     ON proposta_tb.proposta_id = SEGA8085_processar_tb.proposta_id
   JOIN renovacao_monitorada_db..financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK)
     ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id               
   JOIN seguros_db..subramo_tb subramo_tb WITH (NOLOCK)               
     ON proposta_tb.ramo_id = subramo_tb.ramo_id               
    AND proposta_tb.subramo_id = subramo_tb.subramo_id               
    AND proposta_tb.dt_inicio_vigencia_sbr = subramo_tb.dt_inicio_vigencia_sbr               
   JOIN renovacao_monitorada_db..cliente_tb cliente_tb WITH (NOLOCK)               
     ON SEGA8085_processar_tb.cliente_id = cliente_tb.cliente_id               
   LEFT JOIN renovacao_monitorada_db..endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)               
     ON SEGA8085_processar_tb.proposta_id = endereco_corresp_tb.proposta_id              

   --JOIN certificado_re_tb WITH (NOLOCK))               --Zoro.Gomes - confitec - 18/09/2015          
   --  ON renovacao_monitorada_db..proposta_tb.proposta_id_renovada = certificado_re_tb.proposta_id               
   --JOIN emi_re_proposta_tb WITH (NOLOCK))     --Zoro.Gomes - confitec - Retirar a emi - 18/09/2015          
   --  ON renovacao_monitorada_db..proposta_tb.proposta_id_renovada = emi_re_proposta_tb.proposta_id               
   -- AND emi_re_proposta_tb.tp_registro = '02'               
   -- AND emi_re_proposta_tb.canal_venda = 5               
   -- AND emi_re_proposta_tb.arquivo LIKE 'SEG410%'              
   
   JOIN seguros_db..apolice_tb apolice_tb WITH (NOLOCK)               --Zoro.Gomes - confitec - 18/09/2015          
     ON proposta_tb.proposta_id_renovada = apolice_tb.proposta_id               
   
   LEFT JOIN seguros_db..agencia_tb agencia_tb WITH (NOLOCK)              
   --  ON renovacao_monitorada_db..financeiro_proposta_tb.cont_agencia_id = SEGA8085_processar_tb.agencia_id   --Zoro.Gomes - Confitec - 18/09/2015
     ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id   
    AND financeiro_proposta_tb.cont_banco_id = agencia_tb.banco_id     
   
   LEFT JOIN seguros_db..municipio_tb municipio_tb WITH (NOLOCK)
     ON agencia_tb.municipio_id = municipio_tb.municipio_id
    AND agencia_tb.estado = municipio_tb.estado
   ORDER BY cep
   
   
   --ObterEnderecoRiscoRenovacao        
   UPDATE #SEGA8085_20_processar_tb
   SET endereco_risco = ISNULL(endereco, ''),
       bairro_risco = ISNULL(bairro, ''),
       municipio_risco = ISNULL(municipio, ''),
       estado_risco = ISNULL(estado, ''),
       cep_risco = RIGHT(('00000000' + ISNULL(RTRIM(LTRIM(CONVERT(VARCHAR(8),cep))), '')),8)
   FROM renovacao_monitorada_db..endereco_risco_tb WITH (NOLOCK)
   JOIN #SEGA8085_20_processar_tb
     ON renovacao_monitorada_db..endereco_risco_tb.proposta_id = #SEGA8085_20_processar_tb.proposta_id
   
   --Formatando os campos
   
   UPDATE #SEGA8085_20_processar_tb
   SET endereco_bairro_destino = LEFT((endereco_destino + ' ' + bairro_destino),60),
       endereco_bairro_risco = LEFT((endereco_risco + ' ' + bairro_risco),60),        
       cep_destino_formatado = LEFT(cep_destino,5) + '-' + RIGHT(cep_destino,3),        
       cep_risco_formatado = LEFT(cep_risco,5) + '-' + RIGHT(cep_risco,3)
       
       
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #SEGA8085_20_processar_tb
	set #SEGA8085_20_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #SEGA8085_20_processar_tb #SEGA8085_20_processar_tb
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #SEGA8085_20_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id       
       
       
   
   --Inserindo na tabela sega8085_processar_tb no banco interface_dados_db
   INSERT INTO interface_dados_db..SEGA8085_20_processar_tb(SEGA8085_processar_id,
                                                            proposta_ab,          
                                                            proposta_bb,          
                                                            num_apolice,          
                                                            segurado,              
                                                            endereco_destino,                
                                                            endereco_bairro_destino,              
                                                            bairro_destino,                
                                                            municipio_destino,                
                                                            estado_destino,              
                                                            cep_destino,               
                                                            cep_destino_formatado,              
                                                            endereco_bairro_risco,          
                                                            bairro_risco,                  
                                                            municipio_risco,                 
                                                            estado_risco,              
                                                            cep_risco_formatado,           
                                                            nome_produto,                 
                                                            dt_inicio_vigencia,          
                                                            dt_fim_vigencia,          
                                                            num_proc_susep,  
                                                            cpf_cnpj,          
                                                            dt_inclusao,              
                                                            dt_alteracao,              
                                                            usuario)                
                 
   SELECT SEGA8085_processar_id,
          proposta_ab,
          proposta_bb,
          num_apolice,
          segurado,
          endereco_destino,
          endereco_bairro_destino,
          bairro_destino,
          municipio_destino,
          estado_destino,
          cep_destino,
          cep_destino_formatado,
          endereco_bairro_risco,
          bairro_risco,
          municipio_risco,
          estado_risco,
          cep_risco_formatado,
          nome_produto,
          dt_inicio_vigencia,
          dt_fim_vigencia,
          num_proc_susep,
          cpf_cnpj, 
          GETDATE(),
          NULL,
          @Usuario
   FROM #SEGA8085_20_processar_tb
   
  SET NOCOUNT OFF                           
END TRY                                    
                
BEGIN CATCH                                      
 DECLARE @ErrorMessage NVARCHAR(4000)          
 DECLARE @ErrorSeverity INT          
 DECLARE @ErrorState INT        
 SELECT @ErrorMessage = 'Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),          
     @ErrorSeverity = ERROR_SEVERITY(),          
     @ErrorState = ERROR_STATE()          
  
 RAISERROR (@ErrorMessage,          
         @ErrorSeverity,          
      @ErrorState )    
END CATCH                
     

