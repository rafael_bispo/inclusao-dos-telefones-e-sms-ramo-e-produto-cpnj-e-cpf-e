
CREATE PROCEDURE dbo.SEGS6438_SPD
                
AS                
                
SET NOCOUNT ON                 
                
DECLARE @msg              VARCHAR(100)                
        
DELETE FROM interface_dados_db..SEGA8085_10_processar_tb        
DELETE FROM interface_dados_db..SEGA8085_20_processar_tb        
DELETE FROM interface_dados_db..SEGA8085_21_processar_tb        
DELETE FROM interface_dados_db..SEGA8085_22_processar_tb        
DELETE FROM interface_dados_db..SEGA8085_processar_tb        
  
IF @@ERROR <> 0                
 BEGIN                                  
    SELECT @msg = 'Erro em apagar os dados da SEGA8085_processar_tb, SEGA8085_10_processar_tb,  SEGA8085_20_processar_tb, SEGA8085_21_processar_tb e SEGA8085_22_processar_tb.'                
     GOTO error                                  
  END                
                
SET NOCOUNT OFF                
                
RETURN                                  
                                  
error:               

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Inicio --
    --raiserror 55555 @msg                  
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec � sql2012 � 07/08/2015 16:11:39 - Fim --
             
             
            
            
          
        
      
    
  






