CREATE PROCEDURE SEGS10306_SPI (
  @usuario  VARCHAR(20)        
)        
AS        
                    
SET NOCOUNT ON                
                    
DECLARE @msg VARCHAR(100)        
            
INSERT INTO interface_dados_db..SEGA9160_20_processar_tb (        
       SEGA9160_processar_id,        
       proposta_id,        
       nome_segurado,        
       apolice_id,        
       proposta_bb,        
       certificado_id,        
       nome_produto,        
       dt_cancelamento,        
       num_operacao_credito,
       ddd_celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       email, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno        
       dt_inclusao,        
       dt_alteracao,        
       usuario)        
SELECT SEGA9160_processar_tb.SEGA9160_processar_id,        
       SEGA9160_processar_tb.proposta_id,        
       cliente_tb.nome,        
       proposta_adesao_tb.apolice_id,        
       proposta_adesao_tb.proposta_bb,        
       certificado_tb.certificado_id,        
       produto_tb.nome,        
       ISNULL(cancelamento_proposta_tb.dt_cancelamento_bb, cancelamento_proposta_tb.dt_inicio_cancelamento),
       CONVERT(NUMERIC(9,0),questionario_proposta_tb.texto_resposta),        
       isnull (cliente_tb.ddd_1,'') as ddd_celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       isnull (cliente_tb.telefone_1,'') as celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       isnull (cliente_tb.e_mail,'') as email,   -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       GETDATE(),        
       NULL,        
       @usuario        
  FROM interface_dados_db..SEGA9160_processar_tb SEGA9160_processar_tb (NOLOCK)        
  JOIN proposta_adesao_tb (NOLOCK)        
    ON proposta_adesao_tb.proposta_id = SEGA9160_processar_tb.proposta_id        
  JOIN questionario_proposta_tb (NOLOCK)        
    ON questionario_proposta_tb.proposta_id = proposta_adesao_tb.proposta_id        
   AND questionario_proposta_tb.pergunta_id = 7677        
  JOIN cancelamento_proposta_tb (NOLOCK)        
    ON SEGA9160_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id        
   AND cancelamento_proposta_tb.endosso_id = SEGA9160_processar_tb.endosso_id    
   AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL        
  JOIN cliente_tb (NOLOCK)        
    ON SEGA9160_processar_tb.cliente_id = cliente_tb.cliente_id        
  JOIN certificado_tb (NOLOCK)        
    ON certificado_tb.proposta_id = SEGA9160_processar_tb.proposta_id        
   AND certificado_tb.certificado_id = (SELECT MAX(certificado_id)       
                                          FROM certificado_tb (NOLOCK)      
                                         WHERE proposta_id = SEGA9160_processar_tb.proposta_id)      
  JOIN produto_tb (NOLOCK)        
    ON SEGA9160_processar_tb.produto_id = produto_tb.produto_id        
 ORDER BY SEGA9160_processar_tb.SEGA9160_processar_id      
 
 
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
 update SEGA9160_20_processar_tb	
    set SEGA9160_20_processar_tb.agencia = agencia_tb.agencia_id
   from interface_dados_db.dbo.SEGA9160_20_processar_tb SEGA9160_20_processar_tb with(nolock)
  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb with(NOLOCK)          
	 ON proposta_adesao_tb.proposta_id = SEGA9160_20_processar_tb.proposta_id          
  INNER JOIN seguros_db.dbo.agencia_tb agencia_tb(NOLOCK)          
	 ON proposta_adesao_tb.cont_agencia_id = agencia_tb.agencia_id           
    AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id     
    
   -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
  update SEGA9160_20_processar_tb	
	 set SEGA9160_20_processar_tb.agencia = agencia_tb.agencia_id
    from interface_dados_db.dbo.SEGA9160_20_processar_tb SEGA9160_20_processar_tb with(nolock)
   INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb with(NOLOCK)          
	  ON proposta_fechada_tb.proposta_id = SEGA9160_20_processar_tb.proposta_id          
   INNER JOIN seguros_db.dbo.agencia_tb agencia_tb (NOLOCK)          
	  ON proposta_fechada_tb.cont_agencia_id = agencia_tb.agencia_id           
     AND agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id  
                
IF @@ERROR <> 0                  
BEGIN                    
    SELECT @msg = 'Erro ao inserir registros em interface_dados_db..SEGA9160_20_processar_tb'                  
    GOTO ERROR                    
END                  
                
SET NOCOUNT OFF                  
                    
RETURN                  
                  
ERROR:                    

   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Inicio --
    --raiserror 55555 @msg         
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec � sql2012 � 07/08/2015 16:11:34 - Fim --
        
        
      
      
    
    
  




