CREATE PROCEDURE dbo.SEGS10310_SPI
(
    @usuario   VARCHAR(20),      
    @num_versao INT      
 )  
AS  
  
SET NOCOUNT ON      
  
DECLARE @MSG VARCHAR(100)      
  
INSERT INTO interface_dados_db..SEGA9160_processado_tb 
(  
       SEGA9160_processado_id,  
       num_versao,  
       cliente_id,  
       proposta_id,  
       endosso_id,  
       layout_id,  
       produto_id,  
       num_solicitacao,   
       dt_inclusao,  
       dt_alteracao,  
       usuario)  
SELECT SEGA9160_processar_id,  
       @num_versao,  
       cliente_id,  
       proposta_id,  
       endosso_id,  
       layout_id,  
       produto_id,  
       num_solicitacao,
       GETDATE(),  
       NULL,  
       @usuario 
  FROM interface_dados_db..SEGA9160_processar_tb  
  
IF @@ERROR <> 0      
BEGIN      
    SELECT @MSG = 'Erro na inclus�o em SEGA9160_processado_tb'      
    GOTO ERROR      
END   
  
INSERT INTO interface_dados_db..SEGA9160_10_processado_tb (  
       SEGA9160_10_processado_id,  
       SEGA9160_processado_id,  
       proposta_id,  
       cod_barras,  
       nome_destinatario,  
       endereco_corresp,  
       bairro,  
       municipio,  
       estado,  
       cep,  
       dt_inclusao,  
       dt_alteracao,  
       usuario)  
SELECT SEGA9160_10_processar_id,  
       SEGA9160_processar_id,  
       proposta_id,  
       cod_barras,  
       nome_destinatario,  
       endereco_corresp,  
       bairro,  
       municipio,  
       estado,  
       cep,  
       GETDATE(),  
       NULL,  
       @usuario  
  FROM interface_dados_db..SEGA9160_10_processar_tb  
  
IF @@ERROR <> 0      
BEGIN      
    SELECT @MSG = 'Erro na inclus�o em SEGA9160_10_processado_tb'      
    GOTO ERROR      
END   
  
INSERT INTO interface_dados_db..SEGA9160_20_processado_tb (  
       SEGA9160_20_processado_id,  
       SEGA9160_processado_id,  
       proposta_id,  
       nome_segurado,  
       apolice_id,  
       proposta_bb,  
       certificado_id,  
       nome_produto,  
       dt_cancelamento,  
       num_operacao_credito, 
       ddd_celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       email, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno 
       agencia, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno        
       dt_inclusao,  
       dt_alteracao,  
       usuario)  
SELECT SEGA9160_20_processar_id,  
       SEGA9160_processar_id,  
       proposta_id,  
       nome_segurado,  
       apolice_id,  
       proposta_bb,  
       certificado_id,  
       nome_produto,  
       dt_cancelamento,  
       num_operacao_credito, 
       ddd_celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       celular, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
       email, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno 
       agencia, -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno     
       GETDATE(),  
       NULL,  
       @usuario  
  FROM interface_dados_db..SEGA9160_20_processar_tb  
   
IF @@ERROR <> 0      
BEGIN      
    SELECT @MSG = 'Erro na inclus�o em SEGA9160_20_processado_tb'      
    GOTO ERROR      
END   
     
      
-- Excluindo os registros das tabelas processar      
DELETE FROM interface_dados_db..SEGA9160_20_processar_tb      
DELETE FROM interface_dados_db..SEGA9160_10_processar_tb      
DELETE FROM interface_dados_db..SEGA9160_processar_tb      
      
IF @@ERROR <> 0      
BEGIN      
    SELECT @MSG = 'Erro excluindo os registros das tabelas processar'      
    GOTO ERROR      
END      
      
SET NOCOUNT OFF      
      
RETURN      
      
ERROR:  
	--ISABELI SILVA - CONFITEC SP - FLOW 18313521 MIGRA��O SQL SERVER - 2016-06-14        
    --RAISERROR 55555 @MSG   
	RAISERROR(@MSG, 16,1)

  
  


  

