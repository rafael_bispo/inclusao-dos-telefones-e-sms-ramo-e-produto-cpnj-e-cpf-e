/*-----------------------------------------------------------------------------------------------------            
 Projeto C00198309 - Nova Carta de Renova��o     
 SEGA9240 - BB Seguro Residencial � Nova Carta de Renova��o - Baseado no SEGA9186.
 Descri��o: Procedure de p�s processamento.
 Autor: Elson Lima - 08/11/2019       
*/-----------------------------------------------------------------------------------------------------  
CREATE PROCEDURE SEGS14500_SPI        
     @usuario     VARCHAR(20)      
   , @num_versao  INT    
AS
-- BLOCO DE TESTE   
/*    
  BEGIN TRAN  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14500_SPI @Usuario = 'teste' @num_versao =''
  ROLLBACK    
*/          
BEGIN    
   
BEGIN TRY    
      
   SET NOCOUNT ON       
         
   INSERT INTO interface_dados_db.dbo.SEGA9240_processado_tb (      
          SEGA9240_processado_id ,      
          produto_id ,      
          proposta_id ,      
          proposta_bb ,      
          apolice_id ,      
          layout_id ,      
          num_versao ,      
          tipo_documento,      
          num_solicitacao,      
          usuario)      
   SELECT SEGA9240_processar_id ,      
          produto_id ,      
          proposta_id ,      
          proposta_bb ,      
          apolice_id ,      
          layout_id ,      
          @num_versao ,      
          tipo_documento ,      
          num_solicitacao ,      
          @usuario      
   FROM interface_dados_db.dbo.SEGA9240_processar_tb      
       
   INSERT INTO interface_dados_db.dbo.SEGA9240_10_processado_tb (      
          SEGA9240_processado_id ,      
          proposta_id ,      
          nome_destinatario ,      
          endereco_destinatario ,      
          nome_municipio ,      
          uf_destinatario ,      
          CEP_destino ,      
          codigo_retorno ,      
          gerencia_destino,      
          tipo_documento,      
          numero_AR,      
          usuario,    
          identificador )      
   SELECT SEGA9240_processar_id ,      
          proposta_id ,      
          nome_destinatario ,      
          endereco_destinatario ,      
          nome_municipio ,      
          uf_destinatario ,      
          CEP_destino ,      
          codigo_retorno ,      
          gerencia_destino,      
          tipo_documento,      
          numero_AR,      
          @usuario,      
          identificador    
   FROM interface_dados_db.dbo.SEGA9240_10_processar_tb      
       
   INSERT INTO interface_dados_db.dbo.SEGA9240_20_processado_tb (      
          SEGA9240_processado_id ,      
          proposta_id,      
          proposta_bb,      
          apolice_id,      
          nome_segurado,      
          endereco_segurado,      
          municipio_segurado,      
          uf_segurado,      
          CEP_segurado,      
          endereco_risco,      
          municipio_risco,                   
          UF_risco,                            
          CEP_risco,                  
          nome_produto,                     
          dt_vencimento_seguro,                        
          dt_inicio_vigencia,                   
          dt_fim_vigencia,                      
          num_processo_susep,   
          tipo_assistencia_id, --demanda 18707852 - edilson silva - 07/03/2016
		  nome_assistencia,
		  texto_periodo_renovacao, --c00198309-frente-sega-carta-de-renovacao
		  cpf_cnpj,
          usuario  )      
   SELECT SEGA9240_processar_id ,      
          proposta_id,      
          proposta_bb,      
          apolice_id,      
          nome_segurado,      
          endereco_segurado,      
          municipio_segurado,      
          uf_segurado,      
          CEP_segurado,      
          endereco_risco,      
          municipio_risco,                   
          UF_risco,                            
          CEP_risco,                  
          nome_produto,                     
          dt_vencimento_seguro,                        
          dt_inicio_vigencia,                   
          dt_fim_vigencia,                      
          num_processo_susep,    
          tipo_assistencia_id,--demanda 18707852 - edilson silva - 07/03/2016 
          nome_assistencia,	
		  texto_periodo_renovacao, --C00198309-frente-sega-carta-de-renovacao
		  cpf_cnpj,		  
          @usuario       
     FROM interface_dados_db.dbo.SEGA9240_20_processar_tb      
       
   INSERT INTO interface_dados_db.dbo.SEGA9240_21_processado_tb (      
          SEGA9240_processado_id ,      
          proposta_id,      
          cod_cobertura ,      
          nome_cobertura ,              
          moeda_cobertura ,              
          val_is_cobertura,
		  texto_franquia,
          usuario  )      
   SELECT SEGA9240_processar_id ,      
          proposta_id,              
		  cod_cobertura ,      
          nome_cobertura ,              
          moeda_cobertura ,              
          val_is_cobertura,
		  texto_franquia,
          @usuario       
   FROM interface_dados_db.dbo.SEGA9240_21_processar_tb      
       
   INSERT INTO interface_dados_db.dbo.SEGA9240_22_processado_tb (      
          SEGA9240_processado_id ,      
          proposta_id,      
          moeda_valor,      
          val_1_parcela,      
          venc_1_parcela,      
          moeda_demais_parcelas,      
          val_demais_parcelas,      
          venc_demais_parcelas,      
          texto_demais_parcelas,      
          texto_venc_demais_parcelas,      
          val_total_pago,      
          qtd_parcelas,      
          usuario,
          ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
          celular, --migracao-documentacao-digital-2a-fase-cartas
          email,--migracao-documentacao-digital-2a-fase-cartas       
          produto_id,--migracao-documentacao-digital-2a-fase-cartas       
          ramo_id )--migracao-documentacao-digital-2a-fase-cartas       
   SELECT SEGA9240_processar_id ,      
          proposta_id,      
          moeda_valor,      
          val_1_parcela,      
          venc_1_parcela,      
          moeda_demais_parcelas,      
          val_demais_parcelas,      
          venc_demais_parcelas,      
          texto_demais_parcelas,      
          texto_venc_demais_parcelas,      
          val_total_pago,      
          qtd_parcelas,      
          @usuario,
          ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
          celular, --migracao-documentacao-digital-2a-fase-cartas
          email, --migracao-documentacao-digital-2a-fase-cartas             
          produto_id,--migracao-documentacao-digital-2a-fase-cartas       
          ramo_id --migracao-documentacao-digital-2a-fase-cartas       
     FROM interface_dados_db.dbo.SEGA9240_22_processar_tb      
         
   SET NOCOUNT OFF      
         
END TRY                                      
                 
BEGIN CATCH                                        
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),            
     @ErrorSeverity = ERROR_SEVERITY(),            
     @ErrorState = ERROR_STATE()            
    
 RAISERROR (@ErrorMessage,            
         @ErrorSeverity,            
      @ErrorState )      
END CATCH                  
END
GO

      
       
    
  



