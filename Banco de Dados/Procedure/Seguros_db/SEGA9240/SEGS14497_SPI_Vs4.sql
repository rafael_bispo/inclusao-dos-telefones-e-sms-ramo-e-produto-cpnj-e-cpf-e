/*-----------------------------------------------------------------------------------------------------            
 Projeto C00198309 - Nova Carta de Renova��o     
 SEGA9240 - BB Seguro Residencial � Nova Carta de Renova��o - Baseado no SEGA9186.
 Descri��o: Procedure para o detalhe 20 - Dados do Segurado
 Autor: Elson Lima - 08/11/2019             
*/-----------------------------------------------------------------------------------------------------   
CREATE PROCEDURE SEGS14497_SPI 
@Usuario VARCHAR(20)
-- BLOCO DE TESTE   
/*    
  BEGIN TRAN  
    IF @@TRANCOUNT > 0 EXEC seguros_db.dbo.SEGS14497_SPI @Usuario = 'teste'
  ROLLBACK    
*/
AS
BEGIN
	DECLARE @msg VARCHAR(100)

	BEGIN TRY
		SET NOCOUNT ON

		--Selecionando as propostas -------------------------------------------------------------------------------------------    
		CREATE TABLE #Prop_Cliente (
			sega9240_processar_id INT NULL
			,proposta_id NUMERIC(9) NOT NULL
			,nome_produto VARCHAR(60) NULL
			,proposta_bb NUMERIC(9) NULL
			,cliente_id INT NOT NULL
			,dt_fim_vigencia SMALLDATETIME NOT NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,num_proc_susep VARCHAR(60) NULL
			,segurado VARCHAR(85) NOT NULL
			,endereco VARCHAR(200) NULL
			,bairro VARCHAR(30) NULL
			,municipio VARCHAR(30) NULL
			,estado VARCHAR(2) NULL
			,cep VARCHAR(8) NULL
			,seguro_moeda_id VARCHAR(4) NULL
			,apolice_id NUMERIC NULL
			)

		INSERT INTO #Prop_Cliente (
			sega9240_processar_id
			,proposta_id
			,nome_produto
			,proposta_bb
			,cliente_id
			,segurado
			,dt_fim_vigencia
			,dt_inicio_vigencia
			,num_proc_susep
			,endereco
			,bairro
			,municipio
			,estado
			,cep
			,seguro_moeda_id
			,apolice_id
			)
		SELECT DISTINCT sega9240_processar_id
			,proposta_tb.proposta_id
			,proposta_tb.nome_produto
			,financeiro_proposta_tb.proposta_bb
			,proposta_tb.cliente_id
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN cliente_tb.nome
				ELSE 'Ao gerente de contas de: ' + cliente_tb.nome
				END AS segurado
			,ISNULL(apolice_tb.dt_fim_vigencia, '') AS dt_fim_vigencia
			,ISNULL(apolice_tb.dt_inicio_vigencia, '') AS dt_inicio_vigencia
			,ISNULL(apolice_tb.num_proc_susep, '') AS num_proc_susep
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.endereco, '')
				ELSE ISNULL(agencia_tb.endereco, '')
				END AS endereco
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.bairro, '')
				ELSE ISNULL(agencia_tb.bairro, '')
				END AS bairro
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.municipio, '')
				ELSE ISNULL(municipio_tb.nome, '')
				END AS municipio
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.estado, '')
				ELSE ISNULL(agencia_tb.estado, '')
				END AS estado
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.cep, '0')
				ELSE ISNULL(agencia_tb.cep, '0')
				END AS cep
			,ISNULL((
					SELECT TOP (1) sigla
					FROM seguros_db.dbo.moeda_tb moeda_tb WITH (NOLOCK)
					WHERE moeda_id = financeiro_proposta_tb.seguro_moeda_id
					ORDER BY seguro_moeda_id
					), '') AS seguro_moeda_id
			,apolice_tb.apolice_id
		FROM interface_dados_db.dbo.sega9240_processar_tb sega9240_processar_tb WITH (NOLOCK)
		INNER JOIN renovacao_monitorada_db.dbo.proposta_tb proposta_tb WITH (NOLOCK) ON sega9240_processar_tb.proposta_id = proposta_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.cliente_tb cliente_tb WITH (NOLOCK) ON proposta_tb.cliente_id = cliente_tb.cliente_id
		LEFT JOIN SEGUROS_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK) ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.cobertura_proposta_tb cobertura_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = cobertura_proposta_tb.proposta_id
		LEFT JOIN seguros_db.dbo.agencia_tb agencia_tb WITH (NOLOCK) ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id
		LEFT JOIN seguros_db.dbo.municipio_tb municipio_tb WITH (NOLOCK) ON agencia_tb.municipio_id = municipio_tb.municipio_id
			AND agencia_tb.estado = municipio_tb.estado
		INNER JOIN interface_db.dbo.layout_tb layout_tb WITH (NOLOCK) ON layout_tb.layout_id = sega9240_processar_tb.Layout_id
		INNER JOIN interface_db.dbo.layout_produto_tb layout_produto_tb WITH (NOLOCK) ON layout_produto_tb.layout_id = sega9240_processar_tb.Layout_id
			AND layout_produto_tb.produto_id = proposta_tb.produto_id
		INNER JOIN seguros_db.dbo.proposta_tb proposta_seguros_tb WITH (NOLOCK) ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id
		WHERE proposta_tb.produto_id = ISNULL(sega9240_processar_tb.Produto_id, layout_produto_tb.produto_id)
			AND proposta_tb.situacao IN (
				'a'
				,'i'
				)
			AND ISNULL(proposta_seguros_tb.canal_id, 1) = layout_tb.canal_id
		
		UNION
		
		-- GERA��O DE 2� VIA          
		SELECT DISTINCT sega9240_processar_ID
			,proposta_tb.proposta_id
			,proposta_tb.nome_produto
			,financeiro_proposta_tb.proposta_bb
			,proposta_tb.cliente_id
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN cliente_tb.nome
				ELSE 'Ao gerente de contas de: ' + cliente_tb.nome
				END AS segurado
			,ISNULL(apolice_tb.dt_fim_vigencia, '') AS dt_fim_vigencia
			,ISNULL(apolice_tb.dt_inicio_vigencia, '') AS dt_inicio_vigencia
			,ISNULL(apolice_tb.num_proc_susep, '') AS num_proc_susep
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.endereco, '')
				ELSE ISNULL(agencia_tb.endereco, '')
				END AS endereco
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.bairro, '')
				ELSE ISNULL(agencia_tb.bairro, '')
				END AS bairro
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.municipio, '')
				ELSE ISNULL(municipio_tb.nome, '')
				END AS municipio
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.estado, '')
				ELSE ISNULL(agencia_tb.estado, '')
				END AS estado
			,CASE CHARINDEX(CONVERT(CHAR(4), agencia_tb.agencia_id), '4053,3859,4207,4333,4604,4499')
				WHEN 0
					THEN ISNULL(endereco_corresp_tb.cep, '0')
				ELSE ISNULL(agencia_tb.cep, '0')
				END AS cep
			,ISNULL((
					SELECT TOP (1) sigla
					FROM seguros_db.dbo.moeda_tb moeda_tb WITH (NOLOCK)
					WHERE moeda_id = financeiro_proposta_tb.seguro_moeda_id
					ORDER BY seguro_moeda_id
					), '') AS seguro_moeda_id
			,apolice_tb.apolice_id
		FROM interface_dados_db.dbo.sega9240_processar_tb sega9240_processar_tb WITH (NOLOCK)
		INNER JOIN evento_seguros_db.dbo.evento_impressao_tb evento_impressao_tb WITH (NOLOCK) ON sega9240_processar_tb.proposta_id = evento_impressao_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.proposta_tb proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = evento_impressao_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.financeiro_proposta_tb financeiro_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = financeiro_proposta_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.apolice_tb apolice_tb WITH (NOLOCK) ON proposta_tb.proposta_id = apolice_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.cliente_tb cliente_tb WITH (NOLOCK) ON proposta_tb.cliente_id = cliente_tb.cliente_id
		LEFT JOIN SEGUROS_DB.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK) ON proposta_tb.proposta_id = endereco_corresp_tb.proposta_id
		INNER JOIN renovacao_monitorada_db.dbo.cobertura_proposta_tb cobertura_proposta_tb WITH (NOLOCK) ON proposta_tb.proposta_id = cobertura_proposta_tb.proposta_id
		LEFT JOIN seguros_db.dbo.agencia_tb agencia_tb WITH (NOLOCK) ON financeiro_proposta_tb.cont_agencia_id = agencia_tb.agencia_id
		LEFT JOIN seguros_db.dbo.municipio_tb municipio_tb WITH (NOLOCK) ON agencia_tb.municipio_id = municipio_tb.municipio_id
			AND agencia_tb.estado = municipio_tb.estado
		INNER JOIN interface_db.dbo.layout_tb layout_tb WITH (NOLOCK) ON layout_tb.layout_id = sega9240_processar_tb.Layout_id
		INNER JOIN interface_db.dbo.layout_produto_tb layout_produto_tb WITH (NOLOCK) ON layout_produto_tb.layout_id = sega9240_processar_tb.Layout_id
			AND layout_produto_tb.produto_id = proposta_tb.produto_id
		INNER JOIN seguros_db.dbo.proposta_tb proposta_seguros_tb WITH (NOLOCK) ON proposta_seguros_tb.proposta_id = proposta_tb.proposta_id
		WHERE evento_impressao_tb.STATUS = 'l'
			AND evento_impressao_tb.dt_geracao_arquivo IS NULL
			AND evento_impressao_tb.tp_documento_id = CAST(sega9240_processar_tb.tipo_documento AS VARCHAR(2))
			AND proposta_tb.produto_id = ISNULL(sega9240_processar_tb.Produto_id, layout_produto_tb.produto_id)
			AND ISNULL(proposta_seguros_tb.canal_id, 1) = layout_tb.canal_id
		ORDER BY cep

		--Dados auxiliares ------------------------------------------------------------------------------------------------          
		CREATE TABLE #Prop_SEGA9240 (
			sega9240_processar_id INT NULL
			,proposta_id NUMERIC(9) NOT NULL
			,nome_produto VARCHAR(60) NULL
			,proposta_bb NUMERIC NULL
			,cliente_id INT NOT NULL
			,dt_fim_vigencia SMALLDATETIME NOT NULL
			,dt_inicio_vigencia SMALLDATETIME NULL
			,num_proc_susep VARCHAR(60) NULL
			,segurado VARCHAR(85) NOT NULL
			,endereco VARCHAR(200) NULL
			,bairro VARCHAR(30) NULL
			,municipio VARCHAR(30) NULL
			,estado VARCHAR(2) NULL
			,cep VARCHAR(9) NULL
			,seguro_moeda_id VARCHAR(4) NULL
			,apolice_id NUMERIC NULL
			,endereco_bairro VARCHAR(60) NULL
			,CEP_destino VARCHAR(9) NULL
			,endereco_bairro_risco VARCHAR(60) NULL
			,endereco_risco VARCHAR(60) NULL
			,bairro_risco VARCHAR(60) NULL
			,municipio_risco VARCHAR(60) NULL
			,UF_risco VARCHAR(2) NULL
			,CEP_risco VARCHAR(9) NULL
			,dt_fim_vigencia_aux SMALLDATETIME NULL
			,dt_fim_vigencia_aux_formatada VARCHAR(10) NULL
			,dt_vencimento_parcela VARCHAR(10) NULL
			,tipo_assistencia_id NUMERIC(2) NULL
			,nome_assistencia VARCHAR(60) NULL
			,periodo_renovacao NUMERIC(1) NULL
			,texto_periodo_renovacao VARCHAR(30) NULL
			,cpf_cnpj VARCHAR(14) NULL
			)

		INSERT INTO #Prop_SEGA9240 (
			sega9240_processar_id
			,proposta_id
			,nome_produto
			,proposta_bb
			,cliente_id
			,dt_fim_vigencia
			,dt_inicio_vigencia
			,num_proc_susep
			,segurado
			,endereco
			,bairro
			,municipio
			,estado
			,cep
			,seguro_moeda_id
			,apolice_id
			,endereco_bairro
			,CEP_destino
			,endereco_bairro_risco
			,endereco_risco
			,bairro_risco
			,municipio_risco
			,UF_risco
			,CEP_risco
			,dt_fim_vigencia_aux
			,dt_fim_vigencia_aux_formatada
			,dt_vencimento_parcela
			,tipo_assistencia_id
			,nome_assistencia
			,periodo_renovacao
			,texto_periodo_renovacao
			)
		SELECT sega9240_processar_id
			,proposta_id
			,nome_produto
			,proposta_bb
			,cliente_id
			,dt_fim_vigencia
			,dt_inicio_vigencia
			,num_proc_susep
			,segurado
			,endereco
			,bairro
			,municipio
			,estado
			,cep
			,seguro_moeda_id
			,apolice_id
			,CAST(NULL AS VARCHAR(60)) AS endereco_bairro
			,CAST(NULL AS VARCHAR(9)) AS CEP_destino
			,CAST(NULL AS VARCHAR(60)) AS endereco_bairro_risco
			,CAST(NULL AS VARCHAR(60)) AS endereco_risco
			,CAST(NULL AS VARCHAR(60)) AS bairro_risco
			,CAST(NULL AS VARCHAR(60)) AS municipio_risco
			,CAST(NULL AS VARCHAR(2)) AS UF_risco
			,CAST(NULL AS VARCHAR(9)) AS CEP_risco
			,CAST(NULL AS SMALLDATETIME) AS dt_fim_vigencia_aux
			,CAST(NULL AS VARCHAR(10)) AS dt_fim_vigencia_aux_formatada
			,CAST(NULL AS VARCHAR(10)) AS dt_vencimento_parcela
			,CAST(NULL AS NUMERIC(2)) AS tipo_assistencia_id -- Demanda 18707852 - 11/02/2016 - Edilson Silva
			,CAST(NULL AS VARCHAR(60)) AS nome_assistencia
			,CAST(NULL AS NUMERIC(1)) AS periodo_renovacao
			,CAST(NULL AS VARCHAR(30)) AS texto_periodo_renovacao
		FROM #Prop_Cliente

		--Obtendo os dados do endere�o de risco ---------------------------------------------------------------------------          
		UPDATE #Prop_SEGA9240
		SET endereco_risco = ISNULL(endereco_risco_tb.endereco, '')
			,bairro_risco = ISNULL(endereco_risco_tb.bairro, '')
			,municipio_risco = ISNULL(endereco_risco_tb.municipio, '')
			,UF_risco = ISNULL(endereco_risco_tb.estado, '')
			,CEP_risco = ISNULL(endereco_risco_tb.cep, 0)
		FROM renovacao_monitorada_db.dbo.endereco_risco_tb endereco_risco_tb WITH (NOLOCK)
		INNER JOIN #Prop_SEGA9240 prop ON prop.proposta_id = endereco_risco_tb.proposta_id
		--Adriano Ribeiro - inc: 4881950 - 22/03/2016 - Inicio    
		WHERE endereco_risco_tb.end_risco_id = (
				SELECT MAX(A.end_risco_id)
				FROM renovacao_monitorada_db.dbo.endereco_risco_tb A WITH (NOLOCK)
				WHERE A.proposta_id = prop.proposta_id
				)

		--Adriano Ribeiro - inc: 4881950 - 22/03/2016 - Fim  
		--Atualizando e formatando demais campos --------------------------------------------------------------------------          
		UPDATE #Prop_SEGA9240
		SET endereco_bairro = LEFT(#Prop_SEGA9240.endereco + ' ' + #Prop_SEGA9240.bairro + SPACE(60), 60)
			,CEP_destino = SUBSTRING(#Prop_SEGA9240.CEP, 1, 5) + '-' + SUBSTRING(#Prop_SEGA9240.CEP, 6, 3)
			,endereco_bairro_risco = LEFT(#Prop_SEGA9240.endereco_risco + ' ' + #Prop_SEGA9240.bairro_risco + SPACE(60), 60)
			,CEP_risco = SUBSTRING(#Prop_SEGA9240.CEP_risco, 1, 5) + '-' + SUBSTRING(#Prop_SEGA9240.CEP_risco, 6, 3)

		-- Demanda 18707852 - 11/02/2016 - Edilson Silva - inicio  
		--Tipo assistencia  
		UPDATE pr
		SET pr.tipo_assistencia_id = CASE pl.tipo_assistencia_id
				WHEN 44
					THEN 01
				WHEN 45
					THEN 02
				WHEN 46
					THEN 03
				WHEN 47
					THEN 04
				WHEN 48
					THEN 05
				ELSE NULL
				END
		FROM assistencia_db.dbo.proposta_assistencia_atual_tb p WITH (NOLOCK)
		INNER JOIN #prop_sega9240 pr WITH (NOLOCK) ON pr.proposta_id = p.proposta_id
		INNER JOIN assistencia_db.dbo.plano_assistencia_tb pl WITH (NOLOCK) ON pl.plano_assistencia_id = p.plano_assistencia_id
		WHERE pl.tipo_assistencia_id IN (
				44
				,45
				,46
				,47
				,48
				)

		UPDATE #Prop_SEGA9240   
		SET TIPO_ASSISTENCIA_ID = 00   
		WHERE TIPO_ASSISTENCIA_ID IS NULL

		--Update para preenchimento do nome da assistencia(Modificacao solicitada no layout do SEGA9240)
			UPDATE PROP
			   SET PROP.NOME_ASSISTENCIA = TIPO_ASSISTENCIA_TB.NOME 
			  FROM #Prop_SEGA9240 PROP (NOLOCK)  
		INNER JOIN ASSISTENCIA_USS_DB.DBO.MOVIMENTO_ASSISTENCIA_TB M (NOLOCK)  
		        ON M.PROPOSTA_ID = PROP.PROPOSTA_ID  
		INNER JOIN ASSISTENCIA_USS_DB.DBO.TP_ASSISTENCIA_TB TIPO_ASSISTENCIA_TB (NOLOCK) 
		        ON TIPO_ASSISTENCIA_TB.TP_ASSISTENCIA_ID = M.TP_ASSISTENCIA_ID 
		     WHERE M.MOVIMENTO_ASSISTENCIA_ID = 
	   (SELECT MAX(MOVIMENTO_ASSISTENCIA_ID) FROM ASSISTENCIA_USS_DB.DBO.MOVIMENTO_ASSISTENCIA_TB MA WITH (NOLOCK)WHERE MA.PROPOSTA_ID = M.PROPOSTA_ID)

		
		--C00198309-frente-sega-carta-de-renovacao - Altera��o de escopo  - In�cio
		UPDATE PROP
		   SET PROP.PERIODO_RENOVACAO = DATEDIFF(YEAR,APOLICE_TB.DT_INICIO_VIGENCIA,APOLICE_TB.DT_FIM_VIGENCIA)
		  FROM SEGUROS_DB.DBO.APOLICE_TB APOLICE_TB WITH (NOLOCK)
	INNER JOIN #PROP_SEGA9240 PROP ON PROP.PROPOSTA_ID = APOLICE_TB.PROPOSTA_ID 
	     WHERE 1 = 1
		
		UPDATE PROP
		   SET PROP.TEXTO_PERIODO_RENOVACAO = CASE PROP.PERIODO_RENOVACAO
		  WHEN 1
			THEN '1 (um) ano'
		  WHEN 2
			THEN '2 (dois) anos'
		  WHEN 3
			THEN '3 (tr�s) anos'
		  WHEN 4
			THEN '4 (quatro) anos'
		  WHEN 5
			THEN '5 (cinco) anos'
		  ELSE ''
		  END
		       ,PROP.DT_FIM_VIGENCIA = DATEADD(YEAR,PROP.PERIODO_RENOVACAO,PROP.DT_INICIO_VIGENCIA)
		FROM SEGUROS_DB.DBO.APOLICE_TB APOLICE_TB WITH (NOLOCK)
		INNER JOIN #PROP_SEGA9240 PROP ON PROP.PROPOSTA_ID = APOLICE_TB.PROPOSTA_ID 
		WHERE 1 = 1
		
		--C00198309-frente-sega-carta-de-renovacao - Altera��o de escopo  - In�cio
		
	    -- PRJ0016832 - VIG�NCIA PLURIANUAL - AJUSTE PARA RESGATAR A VIG�NCIA RECEBIDA DO BB
		update a
		   set a.dt_inicio_vigencia = b.dt_inicio_vigencia
		     , a.dt_fim_vigencia = b.dt_fim_vigencia 
		  from #prop_sega9240 a
		 inner join renovacao_monitorada_db.dbo.apolice_tb b
		    on a.proposta_id = b.proposta_id 
		 -- FIM - VIG�NCIA PLURIANUAL
		 
		 
		 
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #prop_sega9240
	set #prop_sega9240.CPF_CNPJ = CASE 
										WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
										WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
										ELSE cliente_tb.cpf_cnpj
									END 
	from #prop_sega9240 #prop_sega9240
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #prop_sega9240.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
		
		-- Demanda 18707852 - 11/02/2016 - Edilson Silva - Fim        
		--Inserindo dados na base--------------------------------------------------------------------------------------------          
		INSERT INTO interface_dados_db.dbo.SEGA9240_20_processar_tb (
			sega9240_processar_id
			,proposta_id
			,proposta_bb
			,apolice_id
			,nome_segurado
			,endereco_segurado
			,municipio_segurado
			,uf_segurado
			,CEP_segurado
			,endereco_risco
			,municipio_risco
			,UF_risco
			,CEP_risco
			,nome_produto
			,dt_vencimento_seguro
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,num_processo_SUSEP
			,usuario
			,tipo_assistencia_id
			,nome_assistencia
			,texto_periodo_renovacao
			,cpf_cnpj
			) -- Demanda 18707852 - 11/02/2016 - Edilson Silva            
		SELECT sega9240_processar_id
			,proposta_id
			,proposta_bb
			,apolice_id
			,LEFT(segurado, 60)
			,endereco_bairro
			,LEFT(municipio, 60) --municipio_segurado          
			,estado
			,CEP_destino
			,ISNULL(endereco_bairro_risco, '') --25/08/2014        
			,ISNULL(LEFT(municipio_risco, 60), '')
			,ISNULL(UF_risco, '')
			,ISNULL(CEP_risco, '')
			,LEFT(nome_produto, 60)
			,dt_inicio_vigencia AS dt_vencimento_seguro
			,dt_inicio_vigencia
			,dt_fim_vigencia
			,LEFT(num_proc_susep, 30)
			,@usuario
			,tipo_assistencia_id -- Demanda 18707852 - 11/02/2016 - Edilson Silva 
			,ISNULL(nome_assistencia,'')
			,ISNULL(texto_periodo_renovacao,'')
			,cpf_cnpj
		FROM #Prop_SEGA9240

		SET NOCOUNT OFF
	END TRY

	BEGIN CATCH
		SELECT @msg = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()

		RAISERROR (
				@msg
				,1
				,1
				)

		RETURN
	END CATCH
END
GO





