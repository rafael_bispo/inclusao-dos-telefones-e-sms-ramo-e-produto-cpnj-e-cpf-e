CREATE PROCEDURE SEGS6764_SPI  
    
@usuario VARCHAR(20)     
      
AS      
    
SET NOCOUNT ON    
    
DECLARE @msg AS VARCHAR(100)    
    
         
INSERT INTO interface_dados_db..SEGA8037_20_processado_tb    
       (SEGA8037_20_processar_id,    
       SEGA8037_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_cliente,    
       endereco,    
       municipio,    
       estado,    
       cep,    
       cont_agencia_id,    
       agencia_nome,    
       dv_agencia,    
       nome_produto,    
       ramo_id,    
       dt_inclusao,    
       usuario,  
       dt_cancelamento,  
       proposta_bb,
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
       celular,  --migracao-documentacao-digital-2a-fase-cartas
       email,  --migracao-documentacao-digital-2a-fase-cartas          
       produto_id,   --migracao-documentacao-digital-2a-fase-cartas          
       cpf_cnpj
       )    
SELECT SEGA8037_20_processar_id,    
       SEGA8037_processar_id,    
       proposta_id,            
       endosso_id,        
       nome_cliente,        
       endereco,       
       municipio,        
       estado,        
       cep,    
       cont_agencia_id,      
       agencia_nome,      
       dv_agencia,    
       nome_produto,    
       ramo_id,    
       GETDATE(),    
       usuario = @usuario,  
       dt_cancelamento,  
       proposta_bb,
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
       celular,  --migracao-documentacao-digital-2a-fase-cartas
       email, --migracao-documentacao-digital-2a-fase-cartas
       produto_id,  --migracao-documentacao-digital-2a-fase-cartas         
       cpf_cnpj
FROM interface_dados_db..SEGA8037_20_processar_tb    
    
    
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro na inclus�o em SEGA8037_20_processado_tb'      
     GOTO error                        
  END      
      
SET NOCOUNT OFF      
      
RETURN                        
                        
error:                        
  
   -- Confitec � sql2012 � 07/08/2015 16:11:41 - Inicio --  
    --raiserror 55555 @msg    
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:41 - Fim --  
  
  
  
  
  
  

