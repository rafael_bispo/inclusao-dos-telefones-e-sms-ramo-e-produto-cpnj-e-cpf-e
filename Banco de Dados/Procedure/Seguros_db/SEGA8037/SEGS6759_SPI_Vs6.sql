CREATE PROCEDURE SEGS6759_SPI  
    
@Usuario AS VARCHAR(20)    
    
AS    
    
SET NOCOUNT ON    
    
DECLARE @msg VARCHAR(4000)    
    
-- Selecionando os dados dos documentos ------------------------------------------------------        
SELECT SEGA8037_processar_tb.SEGA8037_processar_id,    
       SEGA8037_processar_tb.proposta_id,         
       endosso_id = ISNULL(SEGA8037_processar_tb.endosso_id,0),    
       canc_endosso_id = ISNULL(SEGA8037_processar_tb.canc_endosso_id,0),    
       nome_produto = produto_tb.nome,        
       SEGA8037_processar_tb.ramo_id,         
       nome_cliente = cliente_tb.nome,        
       endereco_corresp_tb.endereco,        
       endereco_corresp_tb.bairro,        
       endereco_corresp_tb.municipio,        
       endereco_corresp_tb.cep,        
       endereco_corresp_tb.estado,    
       dt_cancelamento = ISNULL(SEGA8037_processar_tb.dt_cancelamento,''),        
       proposta_bb = ISNULL(SEGA8037_processar_tb.proposta_bb,0),       
       SEGA8037_processar_tb.cont_agencia_id,    
       nome_agencia = agencia_tb.nome,      
       isnull(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
       isnull(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
       isnull(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
       SEGA8037_processar_tb.produto_id, --migracao-documentacao-digital-2a-fase-cartas 
       CAST('' AS VARCHAR(14)) as cpf_cnpj
INTO #documento_temp2    
      
FROM interface_dados_db..SEGA8037_processar_tb SEGA8037_processar_tb (NOLOCK)    
JOIN seguros_db..produto_tb produto_tb(NOLOCK)    
  ON produto_tb.produto_id = SEGA8037_processar_tb.produto_id     
JOIN seguros_db..cliente_tb cliente_tb(NOLOCK)        
  ON cliente_tb.cliente_id = SEGA8037_processar_tb.prop_cliente_id      
JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb(NOLOCK)    
  ON endereco_corresp_tb.proposta_id = SEGA8037_processar_tb.proposta_id    
JOIN seguros_db..agencia_tb agencia_tb(NOLOCK)    
  ON agencia_tb.agencia_id = SEGA8037_processar_tb.cont_agencia_id    
 AND agencia_tb.banco_id = SEGA8037_processar_tb.cont_banco_id    
    
IF @@ERROR <> 0    
 BEGIN    
    SELECT @msg = 'Erro na inclus�o em #documento_temp2'    
     GOTO error    
  END    
    
-- SBRJ0015154 - Altera��o do nome do Produto para exibi��o do Nome do Plano para o Produto 1237 - RMarins 12/11/2019  
 UPDATE #documento_temp2     
    SET nome_produto = plano_tb.nome        
   FROM #documento_temp2  
   JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)   
     ON #documento_temp2.proposta_id = proposta_tb.proposta_id  
   JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH (NOLOCK)   
     ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
   JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)                                    
     ON escolha_plano_tb.plano_id = plano_tb.plano_id                                                              
    AND escolha_plano_tb.produto_id = plano_tb.produto_id                                                              
    AND escolha_plano_tb.dt_inicio_vigencia = plano_tb.dt_inicio_vigencia   
  WHERE proposta_tb.produto_id = 1237   
    AND plano_tb.plano_id NOT IN (1,2,3)  
 AND (escolha_plano_tb.endosso_id IS NULL OR escolha_plano_tb.endosso_id = 0)    
-- SBRJ0015154 - Fim altera��o - RMarins 12/11/2019    

-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #documento_temp2
	set #documento_temp2.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #documento_temp2 #documento_temp2
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #documento_temp2.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
		
    
--Populando tabela com os documentos cancelados -----------------------------------------------------        
INSERT INTO interface_dados_db..SEGA8037_20_processar_tb    
(    SEGA8037_processar_id,    
     proposta_id,            
     endosso_id,        
     nome_cliente,        
     endereco,       
     municipio,        
     estado,        
     cep,    
     cont_agencia_id,      
     agencia_nome,      
     dv_agencia,    
     nome_produto,    
     ramo_id,    
     dt_cancelamento,    
     proposta_bb,      
     usuario,      
     dt_inclusao,
     ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
     celular,  --migracao-documentacao-digital-2a-fase-cartas
     email, --migracao-documentacao-digital-2a-fase-cartas
     produto_id,  --migracao-documentacao-digital-2a-fase-cartas          
     cpf_cnpj
)      
SELECT SEGA8037_processar_id,    
       proposta_id,     
       endosso_id,    
       nome_cliente,      
       endereco = LEFT(endereco + ' ' + bairro,200), /* Jessica.Adao - Confitec Sistemas - 20181112 - Sprint 11: Aumento caracter endereco */      
       municipio,      
       estado,      
       SUBSTRING(cep, 1, 5) + '-' + SUBSTRING(cep, 6, 3),    
       cont_agencia_id,      
       nome_agencia,      
       seguros_db.dbo.calcula_dv_cc_ou_agencia(cont_agencia_id),    
       nome_produto,      
       ramo_id,    
       dt_cancelamento,    
       proposta_bb,    
       @usuario,      
       GETDATE(),
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
	   celular,  --migracao-documentacao-digital-2a-fase-cartas
       email,  --migracao-documentacao-digital-2a-fase-cartas           
       produto_id,  --migracao-documentacao-digital-2a-fase-cartas    
       cpf_cnpj
FROM #documento_temp2      
ORDER BY proposta_id,      
         endosso_id,      
         canc_endosso_id    
    
    
IF @@ERROR <> 0    
 BEGIN    
    SELECT @msg = 'Erro na inclus�o em interface_dados_db..SEGA8037_20_processar_tb'    
     GOTO error    
  END    
    
SET NOCOUNT OFF    
    
RETURN    
    
error:    
    
   -- Confitec � sql2012 � 07/08/2015 16:11:41 - Inicio --    
    --raiserror 55555 @msg    
    Declare @msgRaiserror nvarchar(1000)    
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)     
    
    RAISERROR (@msgRaiserror, 16, 1)    
    
   -- Confitec � sql2012 � 07/08/2015 16:11:41 - Fim --    
    
    
    
    
    
    
  
  
  

