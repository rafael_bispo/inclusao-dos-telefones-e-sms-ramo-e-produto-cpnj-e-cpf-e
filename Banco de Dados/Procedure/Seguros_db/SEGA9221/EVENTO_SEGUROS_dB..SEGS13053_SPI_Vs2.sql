CREATE PROCEDURE dbo.SEGS13053_SPI (  
 @usuario VARCHAR(20)  
 , @num_versao INT  
 )  
AS  
/*  
 Bruno Schoralick Pinto - 19/08/2016  
 Demanda: 18234489 - Novos produtos para substituir Ouro Vida e outros  
 Procedure para processamento do endosso de arrependimento 350 referente ao SEGA9221  
 L�gica desenvolvida para os novos produtos (1235,1236,1237)  
 evento_seguros_db  
*/  
-- BLOCO DE TESTE  
/*  
  BEGIN TRAN    
 IF @@TRANCOUNT > 0 EXEC evento_seguros_db.dbo.SEGS13053_SPI @usuario = teste', @num_versao = 1          
  ROLLBACK    
*/  
BEGIN  
 BEGIN TRY  
  SET NOCOUNT ON  
  
  INSERT INTO interface_dados_db.dbo.SEGA9221_processado_tb (  
   SEGA9221_processado_id  
   , num_versao  
   , cliente_id  
   , proposta_id  
   , endosso_id  
   , layout_id  
   , produto_id  
   , dt_inclusao  
   , dt_alteracao  
   , usuario  
   , num_solicitacao  
   )  
  SELECT SEGA9221_processar_id  
   , @num_versao  
   , cliente_id  
   , proposta_id  
   , endosso_id  
   , layout_id  
   , produto_id  
   , GETDATE()  
   , NULL  
   , @usuario  
   , num_solicitacao  
  FROM interface_dados_db.dbo.SEGA9221_processar_tb SEGA9221_processar_tb WITH (NOLOCK)  
  
  INSERT INTO interface_dados_db.dbo.SEGA9221_10_processado_tb (  
   SEGA9221_10_processado_id  
   , SEGA9221_processado_id  
   , proposta_id  
   , cod_barras  
   , nome_destinatario  
   , endereco_corresp  
   , bairro  
   , municipio  
   , estado  
   , cep  
   , dt_inclusao  
   , dt_alteracao  
   , usuario  
   )  
  SELECT SEGA9221_10_processar_id  
   , SEGA9221_processar_id  
   , proposta_id  
   , cod_barras  
   , nome_destinatario  
   , endereco_corresp  
   , bairro  
   , municipio  
   , estado  
   , cep  
   , GETDATE()  
   , NULL  
   , @usuario  
  FROM interface_dados_db.dbo.SEGA9221_10_processar_tb SEGA9221_10_processar_tb WITH (NOLOCK)  
  
  INSERT INTO interface_dados_db.dbo.SEGA9221_20_processado_tb (  
   SEGA9221_20_processado_id  
   , SEGA9221_processado_id  
   , proposta_id  
   , nome_segurado  
   , apolice_id  
   , proposta_bb  
   , certificado_id  
   , nome_produto  
   , dt_ini_vigencia  
   , dt_ped_endosso  
   , num_operacao_credito  
   , dt_inclusao  
   , dt_alteracao  
   , usuario  
   , num_processo_susep
   , ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
   , celular  --migracao-documentacao-digital-2a-fase-cartas
   , email   --migracao-documentacao-digital-2a-fase-cartas
   , produto_id
   , ramo_id 
   )  
  SELECT SEGA9221_20_processar_id  
   , SEGA9221_processar_id  
   , proposta_id  
   , nome_segurado  
   , apolice_id  
   , proposta_bb  
   , certificado_id  
   , nome_produto  
   , dt_ini_vigencia  
   , dt_ped_endosso  
   , num_operacao_credito  
   , GETDATE()  
   , NULL  
   , @usuario  
   , num_processo_susep
   , ddd_celular  --migracao-documentacao-digital-2a-fase-cartas
   , celular  --migracao-documentacao-digital-2a-fase-cartas
   , email   --migracao-documentacao-digital-2a-fase-cartas   
   , produto_id
   , ramo_id 
  FROM interface_dados_db.dbo.SEGA9221_20_processar_tb SEGA9221_20_processar_tb WITH (NOLOCK)  
  
  -- excluindo os registros das tabelas processar          
  DELETE a  
  FROM interface_dados_db.dbo.SEGA9221_20_processar_tb a  
  WHERE 1=1  
  
  DELETE a  
  FROM interface_dados_db.dbo.SEGA9221_10_processar_tb a  
  WHERE 1=1  
  
  DELETE a  
  FROM interface_dados_db.dbo.SEGA9221_processar_tb a  
  WHERE 1=1  
  
  SET NOCOUNT OFF  
  
  RETURN  
 END TRY  
  
 BEGIN CATCH  
  DECLARE @ErrorMessage NVARCHAR(4000)  
   , @ErrorSeverity INT  
   , @ErrorState INT  
  
  SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()  
   , @ErrorSeverity = ERROR_SEVERITY()  
   , @ErrorState = ERROR_STATE()  
  
  RAISERROR (  
    @ErrorMessage  
    , @ErrorSeverity  
    , @ErrorState  
    )  
 END CATCH  
END  
GO



