CREATE PROCEDURE SEGS13048_SPI (@usuario VARCHAR(20))  
AS  
/*  
 Bruno Schoralick Pinto - 18/08/2016  
 Demanda: 18234489 - Novos produtos para substituir Ouro Vida e outros  
 Procedure "interface_dados_db" para processamento do endosso de arrependimento 350 referente ao SEGA9221  
 L?gica desenvolvida para os novos produtos (1235,1236,1237)  
 interface_dados_db  
  */  
  
-- BLOCO DE TESTE  
/*  
 BEGIN TRAN  
 IF @@TRANCOUNT > 0 EXEC interface_dados_db.dbo.SEGS13048_SPI @usuario = 'TESTE'  
 ROLLBACK    
*/  
BEGIN  
 BEGIN TRY  
  SET NOCOUNT ON  
  
  IF OBJECT_ID('tempdb..#SEGA9221_processar') IS NOT NULL  
  BEGIN  
   DROP TABLE #SEGA9221_processar  
  END  
  
  CREATE TABLE #SEGA9221_processar (  
   ID INT NOT NULL IDENTITY(1, 1) PRIMARY KEY  
   , SEGA9221_processar_id INT NULL  
   , proposta_id NUMERIC(9, 0) NULL  
   , nome_produto VARCHAR(60) NULL  
   , dt_ped_endosso SMALLDATETIME NULL  
   , num_operacao_credito NUMERIC(17, 0) NULL  
   , dt_inclusao DATETIME NULL  
   , dt_alteracao DATETIME NULL  
   , usuario VARCHAR(20) NULL  
   , num_processo_susep VARCHAR(30) NULL  
   , cliente_id INT NULL
   , produto_id INT NULL
   , ramo_id INT NULL
   )  
  
  INSERT INTO #SEGA9221_processar (  
   SEGA9221_processar_id  
   , proposta_id  
   , nome_produto  
   , dt_ped_endosso  
   , num_operacao_credito  
   , dt_inclusao  
   , dt_alteracao  
   , usuario  
   , num_processo_susep  
   , cliente_id
   , produto_id
   , ramo_id 
   )  
  SELECT SEGA9221_processar_tb.SEGA9221_processar_id  
   , SEGA9221_processar_tb.proposta_id  
            /* SBRJ0015154 - FaceLift - Revitalizacao dos produtos NPV - inicio inclusao de novos planos 4, 5 e 6 */  
   --, produto_tb.nome  
            , CASE WHEN SEGA9221_processar_tb.produto_id = 1237 AND ISNULL(escolha_plano_tb.plano_id, 0) IN (4,5,6) THEN  
                        plano_tb.nome  
                   ELSE produto_tb.nome  
              END AS nome_produto  
            /* SBRJ0015154 - FaceLift - Revitalizacao dos produtos NPV - inicio inclusao de novos planos 4, 5 e 6 */  
   , cancelamento_proposta_tb.dt_cancelamento_bb  
   , NULL   
   , GETDATE()  
   , NULL  
   , @usuario  
   , subramo_tb.num_proc_susep  
   , SEGA9221_processar_tb.cliente_id 
   , proposta_tb.produto_id
   , proposta_tb.ramo_id 
  FROM interface_dados_db.dbo.SEGA9221_processar_tb SEGA9221_processar_tb WITH (NOLOCK)  
  INNER JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)  
   ON SEGA9221_processar_tb.produto_id = produto_tb.produto_id  
  INNER JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)  
   ON proposta_tb.proposta_id = SEGA9221_processar_tb.proposta_id  
    AND proposta_tb.produto_id = produto_tb.produto_id  
  INNER JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)  
   ON endosso_tb.proposta_id = SEGA9221_processar_tb.proposta_id  
    AND endosso_tb.endosso_id = SEGA9221_processar_tb.endosso_id  
  INNER JOIN seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta_tb WITH (NOLOCK)  
   ON SEGA9221_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id  
    AND cancelamento_proposta_tb.endosso_id = endosso_tb.endosso_id  
    AND cancelamento_proposta_tb.dt_fim_cancelamento IS NULL  
  INNER JOIN seguros_db.dbo.subramo_tb subramo_tb WITH (NOLOCK)  
   ON subramo_tb.subramo_id = proposta_tb.subramo_id  
    AND subramo_tb.dt_fim_vigencia_sbr IS NULL  
        /* SBRJ0015154 - FaceLift - Revitalizacao dos produtos NPV - inicio inclusao de novos planos 4, 5 e 6 */  
         LEFT JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb  
            ON escolha_plano_tb.proposta_id = SEGA9221_processar_tb.proposta_id  
         LEFT JOIN seguros_db.dbo.plano_tb plano_tb  
            ON plano_tb.produto_id = escolha_plano_tb.produto_id  
                AND plano_tb.plano_id = escolha_plano_tb.plano_id  
        /* SBRJ0015154 - FaceLift - Revitalizacao dos produtos NPV - inicio inclusao de novos planos 4, 5 e 6 */  
                    
  INSERT INTO interface_dados_db.dbo.SEGA9221_20_processar_tb (  
   SEGA9221_processar_id  
   , proposta_id  
   , nome_segurado  
   , apolice_id  
   , proposta_bb  
   , certificado_id  
   , nome_produto  
   , dt_ini_vigencia  
   , dt_ped_endosso  
   , num_operacao_credito  
   , dt_inclusao  
   , dt_alteracao  
   , usuario  
   , num_processo_susep
   , ddd_celular
   , celular
   , email  
   )  
  SELECT a.SEGA9221_processar_id  
   , a.proposta_id  
   , cliente_tb.nome  
   , proposta_adesao_tb.apolice_id AS apolice_id  
   , proposta_adesao_tb.proposta_bb AS proposta_bb  
   , certificado_tb.certificado_id  
   , a.nome_produto  
   , proposta_adesao_tb.dt_inicio_vigencia AS dt_inicio_vigencia  
   , a.dt_ped_endosso  
   , a.num_operacao_credito  
   , a.dt_inclusao  
   , a.dt_alteracao  
   , a.usuario  
   , a.num_processo_susep
   , isnull(cliente_tb.ddd_1,'') AS ddd_celular
   , isnull(cliente_tb.telefone_1,'') AS celular
   , isnull(cliente_tb.e_mail,'') as email  
   , a.produto_id -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
   , a.ramo_id -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
  FROM #SEGA9221_processar a  
  INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK)  
   ON proposta_adesao_tb.proposta_id = a.proposta_id  
  INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)  
   ON cliente_tb.cliente_id = a.cliente_id  
  INNER JOIN seguros_db.dbo.certificado_tb certificado_tb WITH (NOLOCK)  
   ON certificado_tb.proposta_id = a.proposta_id  
    AND certificado_tb.certificado_id = (  
     SELECT MAX(certificado_id)  
     FROM seguros_db.dbo.certificado_tb c  
     WHERE c.proposta_id = a.proposta_id  
     )  
    
  UNION  
    
  SELECT b.SEGA9221_processar_id  
   , b.proposta_id  
   , cliente_tb.nome  
   , apolice_tb.apolice_id AS apolice_id  
   , proposta_fechada_tb.proposta_bb AS proposta_bb  
   , null  
   , b.nome_produto  
   , proposta_fechada_tb.dt_inicio_vig AS dt_inicio_vigencia  
   , b.dt_ped_endosso  
   , b.num_operacao_credito  
   , b.dt_inclusao  
   , b.dt_alteracao  
   , b.usuario  
   , b.num_processo_susep
   , isnull(cliente_tb.ddd_1,'') AS ddd_celular
   , isnull(cliente_tb.telefone_1,'') AS celular
   , isnull(cliente_tb.e_mail,'') as email  
   , b.produto_id -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
   , b.ramo_id -- Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
  FROM #SEGA9221_processar b  
  INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK)  
   ON proposta_fechada_tb.proposta_id = b.proposta_id  
  INNER JOIN seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)  
   ON cliente_tb.cliente_id = b.cliente_id  
  INNER JOIN seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)  
   ON apolice_tb.proposta_id = b.proposta_id  
  
  SET NOCOUNT OFF  
  
  RETURN  
 END TRY  
  
 BEGIN CATCH  
  DECLARE @ErrorMessage NVARCHAR(4000)  
   , @ErrorSeverity INT  
   , @ErrorState INT  
  
  SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()  
   , @ErrorSeverity = ERROR_SEVERITY()  
   , @ErrorState = ERROR_STATE()  
  
  RAISERROR (  
    @ErrorMessage  
    , @ErrorSeverity  
    , @ErrorState  
    )  
 END CATCH  
END  
  
GO




