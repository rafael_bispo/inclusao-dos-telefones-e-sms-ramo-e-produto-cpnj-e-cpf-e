CREATE PROCEDURE SEGS12508_SPI  
          
   @Usuario          VARCHAR(20)          
          
AS          
          
  /*-------------------------------------------------------------------------  
  Demanda 18319298 - BB Cr�dito Protegido Empresa Fora do Cronograma  
  Produto 1231 - sega9210 - Carta de Cancelamento a Pedido do Segurado  
  sega9210_20_Processar_tb  
 --------------------------------------------------------------------------*/   
  
          
SET NOCOUNT ON          
          
DECLARE @msg              VARCHAR(100)          
  
/*--- TESTE --------------                
 DECLARE @usuario VARCHAR(20)                            
 SET @usuario    = 'DEM 18319298'        
-- FIM TESTE ----------*/            
  
BEGIN TRY   
          
CREATE TABLE #sega9210_processar_tb          
  (          
    sega9210_processar_id          INT ,           
    proposta_id                    NUMERIC(9,0),                
    produto_id                     INT,            
    ramo_id                        INT,          
    apolice_id                     INT,           
    proposta_bb                    INT,          
    segurado                       VARCHAR(255),          
    endereco                       VARCHAR(60),            
    municipio                      VARCHAR(60),            
    cep_formatado                  VARCHAR(9),          
    estado                         VARCHAR(2),          
    numero_certificado             NUMERIC(9,0),  
    cod_Processo_susep             VARCHAR(20),  
    nome                           VARCHAR(60),          
    dt_cancelamento             SMALLDATETIME,           
    tp_cancelamento                CHAR(1),          
    origem                         CHAR(2),
    prop_cliente_id				   INT,
    ddd_celular					   VARCHAR(4),
    celular						   VARCHAR(9),
    email						   VARCHAR(60),
    cpf_cnpj					   VARCHAR(14)
  )          
        
DECLARE @Data_sistema    SMALLDATETIME          
          
--Obtendo a data do sistema            
SET @Data_sistema = (SELECT dt_operacional          
                     FROM seguros_db.dbo.parametro_geral_tb)          
          
          
INSERT INTO #sega9210_processar_tb          
  (          
 sega9210_processar_id,                   
 proposta_id,          
 produto_id,          
 segurado,          
 endereco,          
 proposta_bb,          
 municipio,          
 cep_formatado,          
 estado,           
 dt_cancelamento,           
 tp_cancelamento,           
 apolice_id,          
 ramo_id,          
 origem,
 prop_cliente_Id                 
  )          
          
SELECT sega9210.sega9210_processar_id,          
       cancelamento_proposta.proposta_id,           
       sega9210.produto_id,          
       segurado = sega9210_10.nome_destinatario,          
       endereco = sega9210_10.endereco_destino,          
       sega9210.proposta_bb,          
       municipio = sega9210_10.municipio_destino,          
       cep_formatado = sega9210_10.cep_destino,           
       estado = sega9210_10.estado_destino,           
       --cancelamento_proposta.dt_inicio_cancelamento,  -- Marcio.Nogueira - altera��o de data de cancelamento - Demanda 18319298 - Confitec-RJ  
       cancelamento_proposta.dt_cancelamento_bb, -- Marcio.Nogueira - altera��o de data de cancelamento - Demanda 18319298 - Confitec-RJ                 
       cancelamento_proposta.tp_cancelamento,           
       sega9210.apolice_id,          
       sega9210.ramo_id,          
       sega9210.origem,
       sega9210.prop_cliente_id                   
FROM interface_dados_db.dbo.sega9210_processar_tb sega9210 with (NOLOCK)          
INNER JOIN interface_dados_db.dbo.sega9210_10_processar_tb sega9210_10 with (NOLOCK)          
  ON sega9210_10.sega9210_processar_id = sega9210.sega9210_processar_id           
INNER JOIN seguros_db.dbo.cancelamento_proposta_tb cancelamento_proposta with(NOLOCK)           
  ON sega9210.proposta_id = cancelamento_proposta.proposta_id          
 AND sega9210.endosso_id = cancelamento_proposta.endosso_id           
WHERE sega9210.tipo_documento = 'ORIGINAL'     
      AND cancelamento_proposta.dt_fim_cancelamento is null    
                   
UNION ALL         
--#VIDA - 2�VIA           
SELECT sega9210.sega9210_processar_id,          
       cancelamento_proposta.proposta_id,           
       sega9210.produto_id,          
       segurado = sega9210_10.nome_destinatario,          
       endereco = sega9210_10.endereco_destino,          
       sega9210.proposta_bb,          
       municipio = sega9210_10.municipio_destino,          
       cep_formatado = sega9210_10.cep_destino,           
       estado = sega9210_10.estado_destino,         
       --cancelamento_proposta.dt_inicio_cancelamento,  -- Marcio.Nogueira - altera��o de data de cancelamento - Demanda 18319298 - Confitec-RJ  
       cancelamento_proposta.dt_cancelamento_bb, -- Marcio.Nogueira - altera��o de data de cancelamento - Demanda 18319298 - Confitec-RJ           
       cancelamento_proposta.tp_cancelamento,            
       sega9210.apolice_id,          
       sega9210.ramo_id,          
       sega9210.origem,    
       sega9210.prop_cliente_id      
 FROM interface_dados_db.dbo.sega9210_processar_tb sega9210 with(NOLOCK)        
INNER JOIN interface_dados_db..sega9210_10_processar_tb sega9210_10 with(NOLOCK)          
   ON sega9210_10.sega9210_processar_id = sega9210.sega9210_processar_id           
INNER JOIN seguros_db.dbo.cancelamento_proposta_tb  cancelamento_proposta with(NOLOCK)          
   ON sega9210.proposta_id = cancelamento_proposta.proposta_id           
  AND sega9210.endosso_id = cancelamento_proposta.endosso_id           
WHERE sega9210.tipo_documento = 'SEGUNDA'  AND cancelamento_proposta.dt_fim_cancelamento is null    
  
  
-- Atualiza nome        
UPDATE #sega9210_processar_tb          
   SET nome = produto.nome      
  FROM #sega9210_processar_tb        
 INNER JOIN seguros_db.dbo.produto_tb produto           
    ON produto.produto_id = #sega9210_processar_tb.produto_id           
  
-- Atualizando numero certificado   
update #sega9210_processar_tb  
set numero_certificado                   = certificado_id  
from seguros_db.dbo.certificado_tb certificado  inner join  
#sega9210_processar_tb on  
#sega9210_processar_tb.proposta_id = certificado.proposta_id   
  
-- Atualizando numero certificado C�digo processo Susep -------  
update #sega9210_processar_tb  
set cod_processo_susep = apolice.num_proc_susep  
From Seguros_db..apolice_Tb apolice    
inner join #sega9210_processar_tb  on   
Apolice.apolice_id = #sega9210_processar_tb.apolice_id   
where apolice.ramo_id = #sega9210_processar_tb.ramo_id   

--Atualizando dados do cliente
update sega
set sega.ddd_celular = isnull(cliente_tb.ddd_1,''), sega.celular = isnull(cliente_tb.telefone_1,''), sega.email = isnull(cliente_tb.e_mail,'')   
From Seguros_db.dbo.cliente_Tb cliente_tb
inner join #sega9210_processar_tb sega 
on cliente_tb.cliente_id= sega.prop_cliente_id

-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update sega
set sega.cpf_cnpj = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from #sega9210_processar_tb sega 
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
on proposta_tb.proposta_id = sega.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
   
         
--Inserindo na tabela f�sica           
INSERT INTO  interface_dados_db.dbo.sega9210_20_processar_tb          
  (          
    sega9210_processar_id          
    ,ramo_id          
    ,apolice_id          
    ,proposta_id          
    ,proposta_bb          
    ,nome_segurado          
    ,endereco_segurado          
    ,municipio_segurado          
    ,cep_segurado          
    ,estado_segurado          
    ,numero_certificado  
	,cod_Processo_susep           
    ,nome_produto          
    ,dt_cancelamento
    ,ddd_celular
    ,celular
    ,email          
    ,dt_inclusao          
    ,usuario          
  )          
          
SELECT sega9210_processar_id          
       ,ramo_id          
       ,apolice_id          
       ,proposta_id  
       ,proposta_bb          
       ,LEFT(segurado,60)          
       ,LEFT(endereco,60)          
       ,LEFT(municipio,60)          
       ,LEFT(cep_formatado,9)          
       ,LEFT(estado,2)          
       ,numero_certificado  
    ,cod_Processo_susep  
       ,LEFT(ISNULL(nome,''),60)  
       ,dt_cancelamento 
       ,ddd_celular
	   ,celular
	   ,email 
       ,GETDATE()          
       ,@usuario          
FROM #sega9210_processar_tb          
               
  
END TRY                                        
                    
BEGIN CATCH      
    
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
              
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                    
     @ErrorSeverity = ERROR_SEVERITY(),             
     @ErrorState = ERROR_STATE()             
    
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState )      
END CATCH        
        
    
  

