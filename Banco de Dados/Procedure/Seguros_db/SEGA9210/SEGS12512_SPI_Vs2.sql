CREATE PROCEDURE dbo.SEGS12512_SPI  
  
@usuario  VARCHAR(20),    
@num_versao INT      
  
AS    
    
SET NOCOUNT ON     
    
DECLARE @msg              VARCHAR(100)    
  
/*--- TESTE --------------                
 DECLARE @usuario VARCHAR(20)                            
 SET @usuario    = 'DEM 18319298'        
-- FIM TESTE ----------*/     
  
----INCLUS�O NA TABELA PROCESSADO_TB   
BEGIN TRY   
   
INSERT INTO interface_dados_db..sega9210_processado_tb (    
   sega9210_processar_id,  
   produto_id,  
   proposta_id,  
   proposta_bb,  
   ramo_id,  
   apolice_id,  
   layout_id,  
   num_versao,  
   tipo_documento,  
   num_solicitacao,  
   dt_inclusao,  
   dt_alteracao,  
   usuario,  
   endosso_id,  
   prop_cliente_id,  
   origem  
   )  
  
SELECT      
 sega9210_processar_id,  
 produto_id,  
 proposta_id,  
 proposta_bb,  
 ramo_id,  
 apolice_id,  
 layout_id,  
 num_versao,  
 tipo_documento,  
 num_solicitacao,  
 dt_inclusao = getdate(),  
 dt_alteracao,  
 @usuario,  
 endosso_id,  
 prop_cliente_id,  
 origem  
FROM interface_dados_db..sega9210_processar_tb    
   
 ----INCLUS�O NA TABELA 10_PROCESSADO_TB      
   
 INSERT INTO interface_dados_db..sega9210_10_processado_tb (    
 sega9210_10_processar_id,    
 sega9210_processar_id,    
 num_endosso,  
 proposta_id ,  
 nome_destinatario ,  
 endereco_destino ,  
 municipio_destino ,  
 estado_destino ,  
 cep_destino ,  
 cod_retorno ,  
 gerencia_destino ,  
 tipo_documento ,  
 dt_inclusao ,  
 usuario)  
   
 SELECT        
 sega9210_10_processar_id,    
 sega9210_processar_id,     
 num_endosso,  
 proposta_id ,  
 nome_destinatario ,  
 endereco_destino ,  
 municipio_destino ,  
 estado_destino ,  
 cep_destino ,  
 cod_retorno ,  
 gerencia_destino ,  
 tipo_documento ,  
 dt_inclusao= getdate(),    
 @usuario    
FROM interface_dados_db..sega9210_10_processar_tb    
   
  ----INCLUS�O NA TABELA 20_PROCESSADO_TB      
 INSERT INTO interface_dados_db..sega9210_20_processado_tb (    
 sega9210_20_processar_id,    
 sega9210_processar_id,    
 proposta_id,    
 ramo_id,    
 apolice_id,    
 proposta_bb,  
 numero_certificado,  
 cod_Processo_susep,    
 nome_segurado,    
 endereco_segurado,    
 municipio_segurado,    
 estado_segurado,    
 CEP_segurado,      
 nome_produto,    
 dt_cancelamento, 
 ddd_celular,
 celular,
 email, 
 cpf_cnpj,  
 dt_inclusao,    
 usuario     
)    
SELECT    
 sega9210_20_processar_id,    
 sega9210_processar_id,    
 proposta_id,    
 ramo_id,    
 apolice_id,    
 proposta_bb,  
 numero_certificado,  
 cod_Processo_susep,      
 nome_segurado,    
 endereco_segurado,    
 municipio_segurado,    
 estado_segurado,    
 CEP_segurado,     
 nome_produto,    
 dt_cancelamento, 
 ddd_celular,
 celular,
 email, 
 cpf_cnpj,  
 dt_inclusao = getdate(),    
 @usuario     
FROM interface_dados_db..sega9210_20_processar_tb   
    
  
END TRY                                        
                    
BEGIN CATCH      
    
 DECLARE @ErrorMessage NVARCHAR(4000)            
 DECLARE @ErrorSeverity INT            
 DECLARE @ErrorState INT          
              
 SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                    
     @ErrorSeverity = ERROR_SEVERITY(),             
     @ErrorState = ERROR_STATE()             
    
 RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState )      
END CATCH      
    
    
    
    
  

