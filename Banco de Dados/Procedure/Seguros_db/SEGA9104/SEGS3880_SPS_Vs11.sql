﻿CREATE PROCEDURE SEGS3880_SPS
  
@origem   AS CHAR(2) = 'BB'  
AS  
  
SET NOCOUNT ON  
  
--22/06/2007 - emaior --------------------------------     
DECLARE @dt_sistema   SMALLDATETIME     
     
SELECT @dt_sistema = dt_operacional     
  FROM parametro_geral_tb  
------------------------------------------------------  
  
--Selecionando as propostas para enviar 2ª Via--------(07/12/2005 - Ana Paula - Stefanini)--------------------------------------  
 SELECT   proposta_id, num_solicitacao  
 INTO  #segunda_via_temp_tb   
 FROM   evento_seguros_db..evento_impressao_tb evt_imp (NOLOCK)  
 WHERE   evt_imp.status = 'l'   
 AND   evt_imp.dt_geracao_arquivo IS NULL   
 AND   evt_imp.tp_documento_id = 13  
  
  
-- Selecionando os documentos que emitem carta de cancelamento---------------------------------------------  
 SELECT   documento_cancelado_tb.endosso_id,   
   documento_cancelado_tb.canc_endosso_id,   
   dt_cancelamento = ISNULL(documento_cancelado_tb.dt_cancelamento,''),  
   documento_cancelado_tb.carta_emitida,  
   proposta_tb.proposta_id,  
   proposta_tb.prop_cliente_id,  
   proposta_tb.produto_id,  
   proposta_tb.ramo_id,  
   corretor_id =  (SELECT MAX(corretor_id)  
     FROM corretagem_tb  (NOLOCK)  
     WHERE proposta_id = documento_cancelado_tb.proposta_id  
     AND dt_inicio_corretagem < = dt_cancelamento  
     AND (dt_fim_corretagem > = dt_cancelamento OR dt_fim_corretagem IS NULL)),  
   num_solicitacao = 0,  
                        ident_inadimplencia_bb_primeira_parc,  
                        ident_inadimplencia_bb_demais_parc  
 INTO   #documentos_temp1  
 FROM   documento_cancelado_tb  
 INNER JOIN  proposta_tb  (NOLOCK)  
 
 ON   documento_cancelado_tb.proposta_id = proposta_tb.proposta_id 
 /*IM00028341-Correção de procedure para SEGA9104-inadimplencia_SEGP0746  /  Carlos Gomes */
	inner join endosso_tb endosso
	on endosso.proposta_id = proposta_tb.proposta_id 
	and endosso.endosso_id = documento_cancelado_tb.endosso_id
/*IM00028341-Correção de procedure para SEGA9104-inadimplencia_SEGP0746  /  Carlos Gomes */
 
 INNER JOIN  parametro_inadimplencia_tb  (NOLOCK)  
 ON   parametro_inadimplencia_tb.ramo_id = proposta_tb.ramo_id  
 AND   parametro_inadimplencia_tb.subramo_id = proposta_tb.subramo_id  
 AND   parametro_inadimplencia_tb.produto_id = proposta_tb.produto_id  
 AND   parametro_inadimplencia_tb.dt_inicio_vigencia_sbr = parametro_inadimplencia_tb.dt_inicio_vigencia_sbr  
 WHERE   parametro_inadimplencia_tb.emite_carta_cancelamento = 'S'  
 AND   parametro_inadimplencia_tb.dt_fim_vigencia IS NULL  
        AND             proposta_tb.situacao = 'c'  
  
  
  UNION   
  
 SELECT   documento_cancelado_tb.endosso_id,   
   documento_cancelado_tb.canc_endosso_id,   
   dt_cancelamento = ISNULL(documento_cancelado_tb.dt_cancelamento,''),  
   documento_cancelado_tb.carta_emitida,  
   proposta_tb.proposta_id,  
   proposta_tb.prop_cliente_id,  
   proposta_tb.produto_id,  
   proposta_tb.ramo_id,  
   corretor_id =  (SELECT MAX(corretor_id)  
     FROM corretagem_tb  (NOLOCK)  
     WHERE proposta_id = documento_cancelado_tb.proposta_id  
     AND dt_inicio_corretagem < = dt_cancelamento  
     AND (dt_fim_corretagem > = dt_cancelamento OR dt_fim_corretagem IS NULL)),  
   #segunda_via_temp_tb.num_solicitacao,  
                        ident_inadimplencia_bb_primeira_parc,  
                        ident_inadimplencia_bb_demais_parc  
 FROM   #segunda_via_temp_tb   
 INNER JOIN documento_cancelado_tb  
 ON  documento_cancelado_tb.proposta_id = #segunda_via_temp_tb.proposta_id  
 INNER JOIN  proposta_tb  (NOLOCK)  
 ON   documento_cancelado_tb.proposta_id = proposta_tb.proposta_id  
 
 /*IM00028341-Correção de procedure para SEGA9104-inadimplencia_SEGP0746  /  Carlos Gomes */
	inner join endosso_tb endosso
	on endosso.proposta_id = proposta_tb.proposta_id 
	and endosso.endosso_id = documento_cancelado_tb.endosso_id
 /*IM00028341-Correção de procedure para SEGA9104-inadimplencia_SEGP0746  /  Carlos Gomes */
 
 INNER JOIN  parametro_inadimplencia_tb  (NOLOCK)  
 ON   parametro_inadimplencia_tb.ramo_id = proposta_tb.ramo_id  
 AND   parametro_inadimplencia_tb.subramo_id = proposta_tb.subramo_id  
 AND   parametro_inadimplencia_tb.produto_id = proposta_tb.produto_id  
 AND   parametro_inadimplencia_tb.dt_inicio_vigencia_sbr = parametro_inadimplencia_tb.dt_inicio_vigencia_sbr  
 WHERE   parametro_inadimplencia_tb.emite_carta_cancelamento = 'S'  
 AND   parametro_inadimplencia_tb.dt_fim_vigencia IS NULL  
        AND             proposta_tb.situacao = 'c'  
 AND  documento_cancelado_tb.carta_emitida = 'n'  
  
  
--vbarbosa - 01/06/2006  
--Removendo do filtro documentos de produtos que tenham cancelamento identificado pelo BB, que não emitem carta.  
  
DELETE FROM #documentos_temp1  
WHERE EXISTS(SELECT 1  
             FROM cobranca_inadimplente_tb  
             WHERE cobranca_inadimplente_tb.proposta_id = #documentos_temp1.proposta_id  
               AND ((ident_inadimplencia_bb_primeira_parc = 'S' AND cobranca_inadimplente_tb.num_cobranca = 1)  
                     OR (ident_inadimplencia_bb_demais_parc = 'S' AND cobranca_inadimplente_tb.num_cobranca > 1)  
                   )  
             )  
  
-- Selecionando os dados dos documentos --------------------------------------------------------------------  
 SELECT   #documentos_temp1.proposta_id,   
   endosso_id = ISNULL(#documentos_temp1.endosso_id,0),   
   canc_endosso_id = ISNULL(#documentos_temp1.canc_endosso_id,0),  
   produto_tb.produto_id,   
   nome_produto = produto_tb.nome,  
   ramo_tb.ramo_id,   
   proposta_adesao_tb.apolice_id,   
   nome_cliente = cliente_tb.nome,  
   endereco_corresp_tb.endereco,  
   endereco_corresp_tb.bairro,  
   endereco_corresp_tb.municipio,  
   endereco_corresp_tb.cep,  
   endereco_corresp_tb.estado,  
   tp_ramo = CASE   
    WHEN ramo_tb.tp_ramo_id = 1 THEN 'VIDA'  
    ELSE 'RE'  
   END,   
   dt_cancelamento = ISNULL(#documentos_temp1.dt_cancelamento,''),  
   proposta_bb = ISNULL(proposta_adesao_tb.proposta_bb,0),   
   tp_proposta = 1,   
   tp_emissao_documento = 'ORIGINAL',  
   cont_agencia_id = proposta_adesao_tb.cont_agencia_id,  
   nome_agencia = agencia_tb.nome,  
   #documentos_temp1.num_solicitacao, --07/12/2005 - Ana Paula - Stefanini  
   ISNULL(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
   ISNULL(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
   ISNULL(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
   CAST('' AS VARCHAR(14)) as cpf_cnpj
 INTO   #documento_temp2  
  
 FROM   #documentos_temp1  
 INNER JOIN  produto_tb (NOLOCK)  
 ON   produto_tb.produto_id = #documentos_temp1.produto_id  
  
 INNER JOIN  proposta_adesao_tb (NOLOCK)  
 ON   proposta_adesao_tb.proposta_id = #documentos_temp1.proposta_id  
  
 INNER JOIN  ramo_tb (NOLOCK)  
 ON   ramo_tb.ramo_id = #documentos_temp1.ramo_id  
  
 INNER JOIN  cliente_tb (NOLOCK)  
 ON   cliente_tb.cliente_id = #documentos_temp1.prop_cliente_id  
  
 INNER JOIN endereco_corresp_tb (NOLOCK)  
 ON   endereco_corresp_tb.proposta_id = #documentos_temp1.proposta_id  
  
 INNER JOIN  agencia_tb (NOLOCK)  
 ON   agencia_tb.agencia_id = proposta_adesao_tb.cont_agencia_id  
 AND   agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id  
  
 WHERE   #documentos_temp1.carta_emitida = 'n'  
 -- Documentos cujo endereço do documento se encontra válido.  
 AND   (endereco_corresp_tb.emite_documento = 'a' OR endereco_corresp_tb.emite_documento IS NULL)  
 --Verificando datas início e fim de vigência em proposta_adesao_tb - 22/06/2007 - emaior  
  AND   proposta_adesao_tb.dt_inicio_vigencia <= @dt_sistema      
  AND  (proposta_adesao_tb.dt_fim_vigencia >= @dt_sistema OR proposta_adesao_tb.dt_fim_vigencia IS NULL)  
 -- Origem do documento (Corretor Independente ou não)    
 AND   @origem = CASE WHEN corretor_id = 100067199 then 'BB' --Corretor BB    
   ELSE 'CI'    
   END    
 --documentos já cancelados  
 AND NOT EXISTS (SELECT  1   
   FROM  evento_seguros_db..evento_impressao_tb evento_impressao_tb  (NOLOCK)  
   WHERE  #documentos_temp1.proposta_id = evento_impressao_tb.proposta_id  
   AND  evento_impressao_tb.tp_documento_id in (13))  
   
   UNION   
   
 SELECT   #documentos_temp1.proposta_id,   
   endosso_id = ISNULL(#documentos_temp1.endosso_id,0),   
   canc_endosso_id = ISNULL(#documentos_temp1.canc_endosso_id,0),  
   produto_tb.produto_id,   
   nome_produto = produto_tb.nome,  
   ramo_tb.ramo_id,   
   apolice_tb.apolice_id,     
   nome_cliente = cliente_tb.nome,  
   endereco_corresp_tb.endereco,  
   endereco_corresp_tb.bairro,  
   endereco_corresp_tb.municipio,  
   endereco_corresp_tb.cep,  
   endereco_corresp_tb.estado,  
   tp_ramo = CASE   
    WHEN ramo_tb.tp_ramo_id = 1 THEN 'VIDA'  
    ELSE 'RE'  
   END,   
   dt_cancelamento = ISNULL(#documentos_temp1.dt_cancelamento,''),  
   proposta_bb = ISNULL(proposta_fechada_tb.proposta_bb,0),   
   tp_proposta = 2,   
   tp_emissao_documento = 'ORIGINAL',  
   cont_agencia_id = proposta_fechada_tb.cont_agencia_id ,  
   nome_agencia = agencia_tb.nome,  
   #documentos_temp1.num_solicitacao, --07/12/2005 - Ana Paula - Stefanini  
   ISNULL(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
   ISNULL(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
   ISNULL(cliente_tb.e_mail,'') as email, --migracao-documentacao-digital-2a-fase-cartas
   CAST('' AS VARCHAR(14)) as cpf_cnpj
 FROM   #documentos_temp1  
  
 INNER JOIN  produto_tb  (NOLOCK)  
 ON   produto_tb.produto_id = #documentos_temp1.produto_id  
  
 INNER JOIN  proposta_fechada_tb  (NOLOCK)  
 ON   proposta_fechada_tb.proposta_id = #documentos_temp1.proposta_id  
  
 INNER JOIN  apolice_tb  (NOLOCK)  
 ON   apolice_tb.proposta_id = #documentos_temp1.proposta_id  
  
 INNER JOIN  ramo_tb  (NOLOCK)  
 ON   ramo_tb.ramo_id = #documentos_temp1.ramo_id  
  
 INNER JOIN  cliente_tb  (NOLOCK)  
 ON   cliente_tb.cliente_id = #documentos_temp1.prop_cliente_id  
  
 INNER JOIN  endereco_corresp_tb  (NOLOCK)  
 ON   endereco_corresp_tb.proposta_id = #documentos_temp1.proposta_id  
  
 INNER JOIN  agencia_tb  (NOLOCK)  
 ON   agencia_tb.agencia_id = proposta_fechada_tb.cont_agencia_id  
 AND   agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id  
   
 WHERE   #documentos_temp1.carta_emitida = 'n'  
 -- Documentos cujo endereço do documento se encontra válido.  
 AND   (endereco_corresp_tb.emite_documento = 'a' OR endereco_corresp_tb.emite_documento IS NULL)  
 --Verificando datas início e fim de vigência em apolice_tb - 22/06/2007 - emaior  
 AND   apolice_tb.dt_inicio_vigencia <= @dt_sistema      
  AND  (apolice_tb.dt_fim_vigencia >= @dt_sistema OR apolice_tb.dt_fim_vigencia IS NULL)  
 -- Origem do documento (Corretor Independente ou não)    
 AND   @origem = CASE WHEN corretor_id = 100067199 then 'BB' --Corretor BB    
   ELSE 'CI'    
   END    
 --documentos já cancelados  
 AND NOT EXISTS (SELECT  1   
   FROM  evento_seguros_db..evento_impressao_tb evento_impressao_tb  (NOLOCK)  
   WHERE  #documentos_temp1.proposta_id = evento_impressao_tb.proposta_id  
   AND  evento_impressao_tb.tp_documento_id in (13))  
   
   
   -- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #documento_temp2
set #documento_temp2.CPF_CNPJ = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from #documento_temp2 #documento_temp2
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #documento_temp2.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id

  
  
--Populando tabela com os documentos cancelado e com os dados para registro de impressão----------------  
  
 INSERT INTO #documento (proposta_id,  
                         proposta_bb,  
                         endosso_id,  
                         canc_endosso_id,  
                         nome_cliente,  
                         endereco,  
                         bairro,  
                         municipio,  
                         estado,  
                         cep,  
                         cont_agencia_id,  
                         agencia_nome,  
                         produto_id,  
                         nome_produto,  
                         ramo_id,  
                         tp_ramo,  
                         apolice_id,   
                         dt_cancelamento,  
                         tp_proposta,  
                         tp_emissao_documento,  
                         dt_solicitacao,  
                         destino,  
                         tp_documento_id,  
                         status,  
                         documento,  
                         local_destino,  
                         local_contato,  
                         dt_geracao_arquivo,  
						 num_solicitacao,
						 ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
						 celular,--migracao-documentacao-digital-2a-fase-cartas
						 email, --migracao-documentacao-digital-2a-fase-cartas
						 cpf_cnpj)
     SELECT  #documento_temp2.proposta_id,  
  proposta_bb,  
  #documento_temp2.endosso_id,  
  canc_endosso_id,     
  nome_cliente,  
  endereco,  
  bairro,  
  municipio,  
  estado,  
  cep,  
  cont_agencia_id,  
  nome_agencia,  
  produto_id,  
  nome_produto,  
  ramo_id,  
  tp_ramo,  
  apolice_id,  
  dt_cancelamento,  
  tp_proposta,  
  tp_emissao_documento,  
  dt_solicitacao = NULL,  
  destino = 'C',  
  tp_documento_id = 13, --documento de cancelamento  
  status = 'i',  
  documento = 'T',  
  local_destino = '',  
  local_contato = '',  
  dt_geracao_arquivo = GETDATE(),  
  num_solicitacao,
  ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
  celular,--migracao-documentacao-digital-2a-fase-cartas
  email,  --migracao-documentacao-digital-2a-fase-cartas
  cpf_cnpj
  FROM  #documento_temp2  
  
  
  
----------------------------- PRODUTOS VIDA -------------------------------------------  
  
  
  
  
  
  
SELECT   
 A.PROPOSTA_ID, b.endosso_id, a.produto_id, a.ramo_id, a.prop_cliente_id, dt_inicio_cancelamento  
INTO     #PROP_CANCE_INADI  
FROM   
 seguros_db..proposta_tb A (NOLOCK)   
 INNER JOIN seguros_db..cancelamento_proposta_tb B (NOLOCK) ON B.PROPOSTA_ID = A.PROPOSTA_ID  
WHERE   
 A.PRODUTO_ID IN (11,121,135,136,716,721)   
 AND B.TP_cancelamento = 9  
 AND A.SITUACAO = 'c'  
 AND B.dt_fim_cancelamento is null  
 AND B.dt_inicio_cancelamento >= '20070801'  
 AND (B.carta_emitida = 'n' or B.carta_emitida is null)   
  
  
  
  
delete from #PROP_CANCE_INADI   
where exists (  
 select 1   
 from seguros_db..sinistro_tb s (nolock),  
 seguros_db..sinistro_cobertura_tb sc(nolock)  
 where s.sinistro_id = sc.sinistro_id  
 and #PROP_CANCE_INADI.proposta_id = s.proposta_id  
 and sc.tp_cobertura_id not in (4,359)   
 and sc.dt_fim_vigencia is null)  
  
  
insert into #documento(proposta_id,  
 proposta_bb,  
 endosso_id,  
 canc_endosso_id,  
 nome_cliente,  
 endereco,  
 bairro,  
 municipio,  
 estado,  
 cep,  
 cont_agencia_id,  
 agencia_nome,  
 produto_id,  
 nome_produto,  
 ramo_id,  
 tp_ramo,  
 apolice_id,  
 dt_cancelamento,  
 tp_proposta,  
 tp_emissao_documento,  
 dt_solicitacao,  
 destino,  
 tp_documento_id,  
 status,  
 documento,  
 local_destino,  
 local_contato,  
 dt_geracao_arquivo,  
 num_cobranca,  
 arquivo_remessa_grf,
 num_solicitacao,
 ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
 celular, --migracao-documentacao-digital-2a-fase-cartas
 email --migracao-documentacao-digital-2a-fase-cartas
 )  
  
SELECT inad.proposta_id,  
proposta_adesao_tb.proposta_bb,         
endosso_id = ISNULL(inad.endosso_id,0),  
0 as canc_endosso_id,  
nome_cliente = cliente_tb.nome,  
endereco_corresp_tb.endereco,  
endereco_corresp_tb.bairro,  
endereco_corresp_tb.municipio,  
endereco_corresp_tb.estado,  
endereco_corresp_tb.cep,  
proposta_adesao_tb.cont_agencia_id,  
nome_agencia = agencia_tb.nome,  
inad.produto_id,  
nome_produto = produto_tb.nome,  
inad.ramo_id,  
tp_ramo = CASE WHEN ramo_tb.tp_ramo_id = 1  
                           THEN 'VIDA'  
                           ELSE 'RE'  
   END,  
proposta_adesao_tb.apolice_id,   
dt_cancelamento = inad.dt_inicio_cancelamento,  
tp_proposta = 1,  
tp_emissao_documento = 'ORIGINAL',  
dt_solicitacao = null,  
destino = 'C',  
tp_documento_id = 13,  
status = 'i',  
documento = 'T',  
local_destino = '',  
local_contato = '',  
dt_geracao_arquivo = getdate(),  
null as num_cobranca,  
null as arquivo_remessa_grf,
0 as num_solicitacao, 
ISNULL(cliente_tb.ddd_1,'') as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
ISNULL(cliente_tb.telefone_1,'') as celular, --migracao-documentacao-digital-2a-fase-cartas
ISNULL(cliente_tb.e_mail,'') as email --migracao-documentacao-digital-2a-fase-cartas
from #PROP_CANCE_INADI inad  
JOIN seguros_db..produto_tb produto_tb(NOLOCK) ON produto_tb.produto_id = inad.produto_id  
JOIN seguros_db..proposta_adesao_tb proposta_adesao_tb(NOLOCK) ON proposta_adesao_tb.proposta_id = inad.proposta_id  
JOIN seguros_db..ramo_tb ramo_tb(NOLOCK) ON ramo_tb.ramo_id = inad.ramo_id  
JOIN seguros_db..cliente_tb cliente_tb(NOLOCK) ON cliente_tb.cliente_id = inad.prop_cliente_id  
JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb(NOLOCK) ON endereco_corresp_tb.proposta_id = inad.proposta_id  
JOIN seguros_db..agencia_tb agencia_tb(NOLOCK) ON agencia_tb.agencia_id = proposta_adesao_tb.cont_agencia_id  
AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id  
  

-- RAFAEL MARTINS - Inclusão dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #documento
set #documento.CPF_CNPJ = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from #documento #documento
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #documento.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id

  
  
  
--Retornando os dados da tabela temporária --------------------------------------------  
  
SELECT   *   
FROM   #documento  
ORDER BY #documento.proposta_id,  
         #documento.endosso_id,  
         #documento.canc_endosso_id  
  
SET NOCOUNT OFF  
  
  
  
  
  
  
  
  
  
  
  
  
  





