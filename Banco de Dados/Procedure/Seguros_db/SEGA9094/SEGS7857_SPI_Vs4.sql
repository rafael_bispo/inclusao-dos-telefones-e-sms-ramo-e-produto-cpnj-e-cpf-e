CREATE PROCEDURE SEGS7857_SPI  
  
@usuario     VARCHAR(20),  
@num_remessa INT     
    
AS  
  
SET NOCOUNT ON  
  
-- TESTES --------------------------------  
-- DECLARE @usuario     VARCHAR(20)  
-- DECLARE @num_remessa INT  
-- SET @usuario = 'PRODUCAO3'  
-- SET @num_remessa = 1  
------------------------------------------  
  
DECLARE @msg  VARCHAR(100)  
    
INSERT INTO interface_dados_db..SEGA9094_10_processado_tb      
           (SEGA9094_processar_id,      
            endosso_id,        
            proposta_id,  
            sinistro_id,  
            NomeSegurado,  
            EnderecoSegurado,  
            BairroSegurado,  
            MunicipioSegurado,  
            UFSegurado,  
            CepSegurado,   
            sequencial,      
            tipo_registro,      
            layout_id,  
            num_remessa,  
            dt_inclusao,  
            usuario,  
			cod_tratamento_retorno, 
			ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
			celular,--migracao-documentacao-digital-2a-fase-cartas
			email, --migracao-documentacao-digital-2a-fase-cartas    
			produto_id,
			ramo_id,
			cpf_cnpj)
SELECT SEGA9094_processar_id,      
       endosso_id,        
       proposta_id,  
       sinistro_id,          
       NomeSegurado,  
       EnderecoSegurado,  
       BairroSegurado,  
       MunicipioSegurado,  
       UFSegurado,  
       CepSegurado,  
       sequencial,      
       tipo_registro,  
       layout_id,  
       @num_remessa,  
       GETDATE(),  
       @usuario,  
       cod_tratamento_retorno,
       ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
       celular,--migracao-documentacao-digital-2a-fase-cartas
       email,--migracao-documentacao-digital-2a-fase-cartas  
       produto_id,
	   ramo_id,
	   cpf_cnpj
FROM interface_dados_db..sega9094_10_processar_tb  
    
IF @@ERROR <> 0    
 BEGIN                      
    SELECT @msg = 'Erro na inclus�o em SEGA9094_10_processado_tb'    
     GOTO error                      
  END    
    
SET NOCOUNT OFF    
    
RETURN                      
                      
error:                      
  
   -- Confitec � sql2012 � 07/08/2015 16:11:44 - Inicio --  
    --raiserror 55555 @msg    
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:44 - Fim --  
  
  
  
  
  
  

