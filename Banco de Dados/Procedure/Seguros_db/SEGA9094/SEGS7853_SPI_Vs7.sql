CREATE PROCEDURE SEGS7853_SPI  
        
@usuario  VARCHAR(20)    
        
AS        
    
SET NOCOUNT ON        
        
DECLARE @msg  VARCHAR(100)        
    
-- TESTES --------------------------    
-- DECLARE @usuario  VARCHAR(20)    
-- SET @usuario = 'PRODUCAO3'    
------------------------------------    
    
-- MONTANDO DETALHE ------------------------------------------------------------------------        
-- DETALHE SEGURADO    
-- DROP TABLE #DETALHE_10_SEGA9094    
    
-- Seleciona e passa pra variavel @tp_documento_id            
-------------------------------------------------------------------------------------            
DECLARE @tp_documento_id  INT              
SELECT @tp_documento_id = evento_seguros_db..documento_tb.tp_documento_id            
FROM evento_seguros_db..documento_tb            
JOIN interface_db..layout_tb (NOLOCK)            
  ON evento_seguros_db..documento_tb.arquivo_saida = interface_db..layout_tb.nome            
WHERE interface_db..layout_tb.layout_id = 1467    
-------------------------------------------------------------------------------------       
    
SELECT SEGA9094_processar_id,        
       '10' TipoRegistro,    
       NomeSegurado = segurado,    
       IDENTITY(INT, 1, 1) Sequencial,    
       a.endosso_id,    
       a.proposta_id,    
       a.sinistro_id,    
       EnderecoSegurado = E.endereco,    
       BairroSegurado = E.bairro,    
       MunicipioSegurado = e.municipio,    
       UFSegurado = e.estado,    
       CepSegurado = RIGHT('00000000' + CONVERT(VARCHAR(8),Cep),8),    
       layout_id,    
       cod_tratamento_retorno =  RIGHT('00' + CONVERT(VARCHAR(2),@tp_documento_id),2)        
       + RIGHT('000000000' + CONVERT(VARCHAR(9),a.proposta_id), 9)               
       + dbo.Obter_Data_Juliana_fn(getdate())               
--     Gabriel C�mara - Confitec - 18/04/2012 - INC000003504400    
--     + RIGHT('0000' + CONVERT(VARCHAR(4), pa.cont_agencia_id), 4)    
--     Gabriel C�mara - Confitec - 17/08/2012 - INC000003669891    
--       + isnull(RIGHT('0000' + CONVERT(VARCHAR(4), pa.cont_agencia_id), 4),pf.cont_agencia_id)    
     + RIGHT('0000' + CONVERT(VARCHAR(4), isnull(pa.cont_agencia_id,pf.cont_agencia_id)), 4),
       CAST('' AS VARCHAR(4)) as ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
	   CAST('' AS VARCHAR(9)) as celular,--migracao-documentacao-digital-2a-fase-cartas
       CAST('' AS VARCHAR(60)) as email, --migracao-documentacao-digital-2a-fase-cartas    
       null as produto_id,
       null as ramo_id,
       CAST('' AS VARCHAR(14)) as cpf_cnpj
  INTO #DETALHE_10_SEGA9094    
  FROM interface_dados_db..SEGA9094_processar_tb A (NOLOCK)    
  JOIN endereco_corresp_tb E (NOLOCK)    
    ON E.proposta_id = A.proposta_id    
  LEFT JOIN proposta_adesao_tb pa (NOLOCK)    
    ON pa.proposta_id = e.proposta_id    
  LEFT JOIN proposta_fechada_tb pf (NOLOCK)    
    ON pf.proposta_id = e.proposta_id    
 ORDER BY a.endosso_id,    
       a.proposta_id    
        
IF @@ERROR <> 0        
 BEGIN                   
    SELECT @msg = 'Erro na inclus�o em #DETALHE_10_SEGA9094'        
     GOTO error        
 END    
 
 
    -- Atualizando o contatos do segurado --------------------------------------------  
  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
  UPDATE #DETALHE_10_SEGA9094
  SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,''), email = isnull(cliente_tb.e_mail,'')    
  FROM #DETALHE_10_SEGA9094 (NOLOCK)  
  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
	ON #DETALHE_10_SEGA9094.proposta_id = proposta_tb.proposta_id  
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id 
    
IF @@ERROR <> 0        
 BEGIN                   
    SELECT @msg = 'Erro ao atualizar #DETALHE_10_SEGA9094 dados de contato'        
     GOTO error        
 END   
 
 
 
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
update #DETALHE_10_SEGA9094
set #DETALHE_10_SEGA9094.produto_id = proposta_tb.produto_id, #DETALHE_10_SEGA9094.ramo_id =  proposta_tb.ramo_id,
    #DETALHE_10_SEGA9094.CPF_CNPJ = CASE 
											WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
											WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
											ELSE cliente_tb.cpf_cnpj
										END 
from #DETALHE_10_SEGA9094 #DETALHE_10_SEGA9094
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #DETALHE_10_SEGA9094.proposta_id
inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
     
     
     IF @@ERROR <> 0        
 BEGIN                   
    SELECT @msg = 'Erro ao atualizar #DETALHE_10_SEGA9094 produto_id, ramo_id e cpf_cnpj'        
     GOTO error        
 END   
        
        
/* CONTROLE DOCUMENTO */    
-- delete from interface_dados_db..SEGA9094_10_processar_tb      
INSERT INTO interface_dados_db..SEGA9094_10_processar_tb        
           (SEGA9094_processar_id,        
            endosso_id,          
            proposta_id,    
            sinistro_id,            
            NomeSegurado,    
            EnderecoSegurado,    
            BairroSegurado,    
            MunicipioSegurado,    
            UFSegurado,    
            CepSegurado,    
            layout_id,        
            sequencial,        
            tipo_registro,        
            dt_inclusao,        
            usuario,    
			cod_tratamento_retorno,
			ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
			celular,--migracao-documentacao-digital-2a-fase-cartas
			email,--migracao-documentacao-digital-2a-fase-cartas      
			produto_id,
			ramo_id,
			cpf_cnpj)
SELECT #DETALHE_10_SEGA9094.SEGA9094_processar_id,      
       endosso_id = RIGHT('000000000' + #DETALHE_10_SEGA9094.endosso_id, 9),          
       NumeroPropostaAB = #DETALHE_10_SEGA9094.Proposta_id,     
       sinistro_id,       
       NomeDestinatario = #DETALHE_10_SEGA9094.NomeSegurado,        
       EnderecoDestinatario = #DETALHE_10_SEGA9094.EnderecoSegurado,     
       BairroSegurado = #DETALHE_10_SEGA9094.BairroSegurado,       
       MunicipioDestino = #DETALHE_10_SEGA9094.MunicipioSegurado,     
       UFdestino = #DETALHE_10_SEGA9094.UFSegurado,        
	   CepDestino = LEFT(#DETALHE_10_SEGA9094.CepSegurado, 05) + '-' + RIGHT(#DETALHE_10_SEGA9094.CepSegurado, 03),     
       layout_id = #DETALHE_10_SEGA9094.layout_id,        
       sequencial = #DETALHE_10_SEGA9094.sequencial,        
       tipo_registro = #DETALHE_10_SEGA9094.TipoRegistro,        
       GETDATE(),        
       usuario = @usuario,    
	   cod_tratamento_retorno = #DETALHE_10_SEGA9094.cod_tratamento_retorno,
	   ddd_celular = #DETALHE_10_SEGA9094.ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
	   celular = #DETALHE_10_SEGA9094.celular,--migracao-documentacao-digital-2a-fase-cartas
	   email = #DETALHE_10_SEGA9094.email, --migracao-documentacao-digital-2a-fase-cartas        
	   #DETALHE_10_SEGA9094.produto_id,
	   #DETALHE_10_SEGA9094.ramo_id,
	   #DETALHE_10_SEGA9094.cpf_cnpj
  FROM #DETALHE_10_SEGA9094 (NOLOCK)        
 ORDER BY sequencial        
        
IF @@ERROR <> 0    
 BEGIN                   
    SELECT @msg = 'Erro na inclus�o em SEGA9094_10_processar_tb'        
     GOTO error        
 END    
    
SET NOCOUNT OFF    
    
RETURN        
        
error:        
  
   -- Confitec � sql2012 � 07/08/2015 16:11:44 - Inicio --  
    --raiserror 55555 @msg        
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:44 - Fim --  
    
    
    
    
  
  
  
  
  

