CREATE PROCEDURE Segs12032_sps
AS
    /**********************************************************************************
    
    Demanda 18242260 - Eduardo.Maior - Confitec Sistemas - 07/08/2014
    
    Objetivo: Migracao de produtos e ramos do SEGA8055RE (Produtos Rural/Habitacional)
    
    SEGA9181 - Apolice Rural/Habitacional
    
    ***********************************************************************************/
    SET NOCOUNT ON

  BEGIN TRY
      DECLARE @CODERRO INT
      DECLARE @ERRORMESSAGE NVARCHAR(4000)
      DECLARE @ERRORPROCEDURE NVARCHAR(4000)
      DECLARE @ERRORSEVERITY INT
      DECLARE @ERRORSTATE INT

      IF EXISTS (SELECT 1
                 FROM   tempdb.dbo.sysobjects
                 WHERE  NAME LIKE '#TEMP_AUX_PROCESSO_RETORNO_MQ_TB')
        BEGIN
            DROP TABLE #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
        END

      IF EXISTS (SELECT 1
                 FROM   tempdb.dbo.sysobjects
                 WHERE  NAME LIKE '##SEGA9181')
        BEGIN
            DROP TABLE ##SEGA9181
        END

      -- APOLICE (1a VIA)
      SELECT TOP 1000 tpemissao = 'A',
                      apolice_tb.apolice_id,
                      apolice_tb.proposta_id,
                      apolice_tb.dt_inicio_vigencia,
                      apolice_tb.dt_fim_vigencia,
                      endosso_id = 0,
                      dt_pedido_endosso = '19000101',
                      apolice_tb.seguradora_cod_susep,
                      apolice_tb.sucursal_seguradora_id,
                      apolice_tb.ramo_id,
                      produto_tb.apolice_envia_cliente,
                      produto_tb.apolice_num_vias,
                      produto_tb.apolice_envia_congenere,
                      apolice_tb.dt_emissao,
                      proposta_tb.produto_id,
                      tp_endosso_id = 0,
                      apolice_tb.num_proc_susep,
                      proposta_fechada_tb.impressao_liberada,
                      destino = CONVERT(CHAR(1), NULL),
                      num_solicitacao = NULL,
                      diretoria_id = NULL,
                      endereco_corresp_tb.cep,
                      CAST('' AS VARCHAR(14)) as cpf_cnpj
      INTO   #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
      FROM   seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)
             JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
               ON apolice_tb.proposta_id = proposta_tb.proposta_id
             JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)
               ON proposta_tb.produto_id = produto_tb.produto_id
             JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK)
               ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id
             JOIN seguros_db.dbo.sega9181_produto_ramo_tb SEGA9181 WITH (NOLOCK)
               ON SEGA9181.produto_id = proposta_tb.produto_id
                  AND SEGA9181.ramo_id = proposta_tb.ramo_id
             JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)
               ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id
             LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_tb avaliacao_retorno_bb_tb WITH (NOLOCK)
                    ON proposta_tb.proposta_id = avaliacao_retorno_bb_tb.proposta_id
      WHERE  apolice_tb.dt_impressao IS NULL
             AND ( proposta_fechada_tb.impressao_liberada = 's'
                    OR ( proposta_fechada_tb.impressao_liberada = 'n'
                         AND produto_tb.apolice_envia_cliente = 's' ) )
             AND apolice_tb.dt_emissao >= '20000701'
             AND proposta_tb.situacao = 'i'
             AND ( avaliacao_retorno_bb_tb.aceite_bb = 'S'
                    OR avaliacao_retorno_bb_tb.aceite_bb IS NULL )
      UNION
      -- APOLICE (2a VIA)
      SELECT TOP 1000 tpemissao = 'A',
                      apolice_tb.apolice_id,
                      apolice_tb.proposta_id,
                      apolice_tb.dt_inicio_vigencia,
                      apolice_tb.dt_fim_vigencia,
                      endosso_id = 0,
                      dt_pedido_endosso = '19000101',
                      apolice_tb.seguradora_cod_susep,
                      apolice_tb.sucursal_seguradora_id,
                      apolice_tb.ramo_id,
                      produto_tb.apolice_envia_cliente,
                      produto_tb.apolice_num_vias,
                      produto_tb.apolice_envia_congenere,
                      apolice_tb.dt_emissao,
                      proposta_tb.produto_id,
                      tp_endosso_id = 0,
                      apolice_tb.num_proc_susep,
                      impressao_liberada = '',
                      evento_impressao_tb.destino,
                      evento_impressao_tb.num_solicitacao,
                      evento_impressao_tb.diretoria_id,
                      endereco_corresp_tb.cep,
                      CAST('' AS VARCHAR(14)) as cpf_cnpj
      FROM   evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)
             JOIN seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)
               ON evento_impressao_tb.proposta_id = apolice_tb.proposta_id
             JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
               ON apolice_tb.proposta_id = proposta_tb.proposta_id
             JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)
               ON proposta_tb.produto_id = produto_tb.produto_id
             JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH (NOLOCK)
               ON apolice_tb.proposta_id = proposta_fechada_tb.proposta_id
             JOIN seguros_db.dbo.sega9181_produto_ramo_tb SEGA9181 WITH (NOLOCK)
               ON SEGA9181.produto_id = proposta_tb.produto_id
                  AND SEGA9181.ramo_id = proposta_tb.ramo_id
             JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)
               ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id
             LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_tb avaliacao_retorno_bb_tb WITH (NOLOCK)
                    ON proposta_tb.proposta_id = avaliacao_retorno_bb_tb.proposta_id
      WHERE  evento_impressao_tb.status = 'l'
             AND evento_impressao_tb.dt_geracao_arquivo IS NULL
             AND evento_impressao_tb.tp_documento_id = 4
             AND proposta_tb.situacao <> 't'
             AND ( avaliacao_retorno_bb_tb.aceite_bb = 'S'
                    OR avaliacao_retorno_bb_tb.aceite_bb IS NULL )
      UNION
      -- APOLICE (2a VIA) PARA PROPOSTA ADESO -- DEMANDA 18657619 - PAULO PELEGRINI - 09/03/2015
      SELECT TOP 1000 tpemissao = 'A',
                      apolice_tb.apolice_id,
                      proposta_adesao_tb.proposta_id,
                      proposta_adesao_tb.dt_inicio_vigencia,
                      proposta_adesao_tb.dt_fim_vigencia,
                      endosso_id = 0,
                      dt_pedido_endosso = '19000101',
                      apolice_tb.seguradora_cod_susep,
                      apolice_tb.sucursal_seguradora_id,
                      apolice_tb.ramo_id,
                      produto_tb.apolice_envia_cliente,
                      produto_tb.apolice_num_vias,
                      produto_tb.apolice_envia_congenere,
                      apolice_tb.dt_emissao,
                      proposta_tb.produto_id,
                      tp_endosso_id = 0,
                      apolice_tb.num_proc_susep,
                      impressao_liberada = '',
                      evento_impressao_tb.destino,
                      evento_impressao_tb.num_solicitacao,
                      evento_impressao_tb.diretoria_id,
                      endereco_corresp_tb.cep,
                      CAST('' AS VARCHAR(14)) as cpf_cnpj
      FROM   evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)
             JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH (NOLOCK)
               ON evento_impressao_tb.proposta_id = proposta_adesao_tb.proposta_id
             JOIN seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)
               ON proposta_adesao_tb.apolice_id = apolice_tb.apolice_id
                  AND proposta_adesao_tb.ramo_id = apolice_tb.ramo_id
             JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
               ON proposta_adesao_tb.proposta_id = proposta_tb.proposta_id
             JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)
               ON proposta_tb.produto_id = produto_tb.produto_id
             JOIN seguros_db.dbo.sega9181_produto_ramo_tb SEGA9181 WITH (NOLOCK)
               ON SEGA9181.produto_id = proposta_tb.produto_id
                  AND SEGA9181.ramo_id = proposta_tb.ramo_id
             JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)
               ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id
             LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_tb avaliacao_retorno_bb_tb WITH (NOLOCK)
                    ON proposta_tb.proposta_id = avaliacao_retorno_bb_tb.proposta_id
      WHERE  evento_impressao_tb.status = 'l'
             AND evento_impressao_tb.dt_geracao_arquivo IS NULL
             AND evento_impressao_tb.tp_documento_id = 4
             AND proposta_tb.situacao <> 't'
             AND ( avaliacao_retorno_bb_tb.aceite_bb = 'S'
                    OR avaliacao_retorno_bb_tb.aceite_bb IS NULL )
             AND proposta_tb.produto_id IN ( 8, 300, 227, 156,
                                             228, 229, 701 )
      -- FIM DEMANDA 18657619 - PAULO PELEGRINI - 09/03/2015
      UNION
      -- ENDOSSO (1a VIA)
      SELECT TOP 1000 tpemissao = 'E',
                      apolice_tb.apolice_id,
                      apolice_tb.proposta_id,
                      apolice_tb.dt_inicio_vigencia,
                      apolice_tb.dt_fim_vigencia,
                      endosso_tb.endosso_id,
                      endosso_tb.dt_pedido_endosso,
                      apolice_tb.seguradora_cod_susep,
                      apolice_tb.sucursal_seguradora_id,
                      apolice_tb.ramo_id,
                      produto_tb.apolice_envia_cliente,
                      produto_tb.apolice_num_vias,
                      produto_tb.apolice_envia_congenere,
                      endosso_tb.dt_emissao,
                      proposta_tb.produto_id,
                      endosso_tb.tp_endosso_id,
                      apolice_tb.num_proc_susep,
                      impressao_liberada = '',
                      destino = CONVERT(CHAR(1), NULL),
                      num_solicitacao = NULL,
                      diretoria_id = NULL,
                      endereco_corresp_tb.cep,
                      CAST('' AS VARCHAR(14)) as cpf_cnpj
      FROM   seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)
             JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
               ON apolice_tb.proposta_id = endosso_tb.proposta_id
                  AND endosso_tb.tp_endosso_id NOT IN ( 63, 90, 91, 100, 101 )
             JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
               ON endosso_tb.proposta_id = proposta_tb.proposta_id
             JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)
               ON proposta_tb.produto_id = produto_tb.produto_id
             JOIN seguros_db.dbo.sega9181_produto_ramo_tb SEGA9181 WITH (NOLOCK)
               ON SEGA9181.produto_id = proposta_tb.produto_id
                  AND SEGA9181.ramo_id = proposta_tb.ramo_id
             JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)
               ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id
             LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_tb avaliacao_retorno_bb_tb WITH (NOLOCK)
                    ON proposta_tb.proposta_id = avaliacao_retorno_bb_tb.proposta_id
      WHERE  endosso_tb.dt_impressao IS NULL
             AND endosso_tb.dt_emissao >= '20000701'
             AND proposta_tb.situacao NOT IN ( 'c', 't' )
             AND ( avaliacao_retorno_bb_tb.aceite_bb = 'S'
                    OR avaliacao_retorno_bb_tb.aceite_bb IS NULL )
      UNION
      -- ENDOSSO (2a VIA)
      SELECT TOP 1000 'tpemissao' = 'E',
                      apolice_tb.apolice_id,
                      apolice_tb.proposta_id,
                      apolice_tb.dt_inicio_vigencia,
                      apolice_tb.dt_fim_vigencia,
                      endosso_id = Isnull(endosso_tb.endosso_id, 0),
                      dt_pedido_endosso = Isnull(endosso_tb.dt_pedido_endosso, ''),
                      apolice_tb.seguradora_cod_susep,
                      apolice_tb.sucursal_seguradora_id,
                      apolice_tb.ramo_id,
                      produto_tb.apolice_envia_cliente,
                      produto_tb.apolice_num_vias,
                      produto_tb.apolice_envia_congenere,
                      dt_emissao = Isnull(endosso_tb.dt_emissao, ''),
                      proposta_tb.produto_id,
                      tp_endosso_id = Isnull(endosso_tb.tp_endosso_id, 0),
                      num_proc_susep = Isnull(apolice_tb.num_proc_susep, 0),
                      impressao_liberada = '',
                      evento_impressao_tb.destino,
                      evento_impressao_tb.num_solicitacao,
                      evento_impressao_tb.diretoria_id,
                      endereco_corresp_tb.cep,
                      CAST('' AS VARCHAR(14)) as cpf_cnpj
      FROM   evento_seguros_db..evento_impressao_tb evento_impressao_tb WITH (NOLOCK)
             JOIN seguros_db.dbo.apolice_tb apolice_tb WITH (NOLOCK)
               ON evento_impressao_tb.proposta_id = apolice_tb.proposta_id
             LEFT JOIN seguros_db.dbo.endosso_tb endosso_tb WITH (NOLOCK)
                    ON evento_impressao_tb.proposta_id = endosso_tb.proposta_id
                       AND endosso_tb.tp_endosso_id NOT IN ( 63, 90, 91, 100, 101 )
                       AND endosso_tb.endosso_id = evento_impressao_tb.endosso_id
             JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)
               ON evento_impressao_tb.proposta_id = proposta_tb.proposta_id
             JOIN seguros_db.dbo.produto_tb produto_tb WITH (NOLOCK)
               ON proposta_tb.produto_id = produto_tb.produto_id
             JOIN seguros_db.dbo.sega9181_produto_ramo_tb SEGA9181 WITH (NOLOCK)
               ON SEGA9181.produto_id = proposta_tb.produto_id
                  AND SEGA9181.ramo_id = proposta_tb.ramo_id
             JOIN seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)
               ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id
             LEFT JOIN seguros_db.dbo.avaliacao_retorno_bb_tb avaliacao_retorno_bb_tb WITH (NOLOCK)
                    ON proposta_tb.proposta_id = avaliacao_retorno_bb_tb.proposta_id
      WHERE  evento_impressao_tb.status = 'l'
             AND evento_impressao_tb.dt_geracao_arquivo IS NULL
             AND evento_impressao_tb.tp_documento_id = 6
             AND proposta_tb.situacao <> 't'
             AND ( avaliacao_retorno_bb_tb.aceite_bb = 'S'
                    OR avaliacao_retorno_bb_tb.aceite_bb IS NULL )

      -- cristovao.rodrigues 22/09/2016 - 19368999 - cobrana registrada AB e ABS
      --select top 20 * from ##TEMP_AUX_PROCESSO_RETORNO_MQ_9201_TB
      DELETE #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
      -- select proposta_adesao_tb.forma_pgto_id, agendamento_cobranca_tb.fl_boleto_registrado, *
      FROM   #TEMP_AUX_PROCESSO_RETORNO_MQ_TB #TEMP_AUX_PROCESSO_RETORNO_MQ_TB WITH(nolock)
             INNER JOIN seguros_db.dbo.agendamento_cobranca_tb agendamento_cobranca_tb WITH(nolock)
                     ON agendamento_cobranca_tb.proposta_id = #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
                        AND agendamento_cobranca_tb.num_endosso = Isnull(#TEMP_AUX_PROCESSO_RETORNO_MQ_TB.endosso_id, 0)
             INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb WITH(nolock)
                     ON proposta_fechada_tb.proposta_id = #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
      WHERE  1 = 1
             AND proposta_fechada_tb.forma_pgto_id = 3
             AND Isnull(agendamento_cobranca_tb.fl_boleto_registrado, 'N') <> 'S'

      DELETE #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
      -- select proposta_adesao_tb.forma_pgto_id, agendamento_cobranca_tb.fl_boleto_registrado, *
      FROM   #TEMP_AUX_PROCESSO_RETORNO_MQ_TB #TEMP_AUX_PROCESSO_RETORNO_MQ_TB WITH(nolock)
             INNER JOIN seguros_db.dbo.agendamento_cobranca_tb agendamento_cobranca_tb WITH(nolock)
                     ON agendamento_cobranca_tb.proposta_id = #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
                        AND agendamento_cobranca_tb.num_endosso = Isnull(#TEMP_AUX_PROCESSO_RETORNO_MQ_TB.endosso_id, 0)
             INNER JOIN seguros_db.dbo.proposta_adesao_tb proposta_adesao_tb WITH(nolock)
                     ON proposta_adesao_tb.proposta_id = #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
      WHERE  1 = 1
             AND proposta_adesao_tb.forma_pgto_id = 3
             AND Isnull(agendamento_cobranca_tb.fl_boleto_registrado, 'N') <> 'S'
             
             
             -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	set #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #TEMP_AUX_PROCESSO_RETORNO_MQ_TB #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id

      -- cristovao.rodrigues 22/09/2016 - 19368999 - cobrana registrada AB e ABS
      -- Copiado da procedure SEGS4024_SPS : Demanda 12005250 - Nilton Confitec
      EXEC seguros_db.dbo.Segs10192_spi @TMPLOCAL = 1

      -- Criando temporaria para o SEGP0665
      -- Considerar no SEGA9181 somente as emissoes dos ultimos 60 dias.
      SELECT FLAG_EMITE = 'S',
             *
      INTO   ##SEGA9181
      FROM   #TEMP_AUX_PROCESSO_RETORNO_MQ_TB
      WHERE  proposta_id NOT IN ( 021301147, 022643501 )
             AND Datediff(DAY, dt_emissao, Getdate()) <= 60

      SET NOCOUNT OFF
  END TRY

  BEGIN CATCH
      SELECT @CODERRO = Error_number(),
             @ERRORMESSAGE = Error_message(),
             @ERRORSEVERITY = Error_severity(),
             @ERRORSTATE = Error_state(),
             @ERRORPROCEDURE = Error_procedure()

      RAISERROR(@ERRORMESSAGE,@ERRORSEVERITY,@ERRORSTATE)
  END CATCH 

