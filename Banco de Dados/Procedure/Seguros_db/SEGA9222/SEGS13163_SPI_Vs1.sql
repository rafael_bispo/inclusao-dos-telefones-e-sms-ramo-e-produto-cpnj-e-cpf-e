alter PROCEDURE SEGS13163_SPI  
  
@usuario VARCHAR(20)      
      
AS      
-----------------------------------------------------------                                  
-- Procedure Detalhe 30 - Segurado SEGA9222              --                                  
-- teste
-- exec SEGS13163_SPI 'teste'     
-----------------------------------------------------------                                  
    
BEGIN TRY
	
SET NOCOUNT ON                                  
        
DECLARE @dt_sistema SMALLDATETIME            
DECLARE @msg VARCHAR(100)             
   
    
SELECT distinct SEGA9222.SEGA9222_processar_id,        
       ISNULL(cliente_tb.nome, '') as razao_social, 
       ISNULL(cliente_tb.cpf_cnpj, '') as cnpj,
       ISNULL(endereco_corresp_tb.endereco, '') as Endereco_corresp, 
       ISNULL(endereco_corresp_tb.bairro, '') as bairro,
       ISNULL(endereco_corresp_tb.municipio, '') as cidade,
       ISNULL(endereco_corresp_tb.estado, '') as UF,
       CASE      
          WHEN endereco_corresp_tb.cep IS NULL THEN ''      
          ELSE SUBSTRING(endereco_corresp_tb.cep, 1, 5) + '-' + SUBSTRING(endereco_corresp_tb.cep, 6, 3)      
       END AS CEP,  
       ISNULL(endereco_corresp_tb.telefone, 0)telefone,
	   ISNULL(endereco_corresp_tb.DDD, 0)DDD
  INTO #SEGA9222_30         
  FROM interface_dados_db..sega9222_processar_tb sega9222 (NOLOCK)                                  
 INNER JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)        
ON proposta_tb.proposta_id = sega9222.proposta_id        
 INNER JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)        
    ON cliente_tb.cliente_id = proposta_tb.prop_cliente_id        
 INNER JOIN seguros_db..endereco_corresp_tb endereco_corresp_tb (NOLOCK)        
    ON endereco_corresp_tb.proposta_id = proposta_tb.proposta_id         
 INNER JOIN seguros_db..proposta_fechada_tb proposta_fechada_tb (NOLOCK)        
    ON proposta_fechada_tb.proposta_id = proposta_tb.proposta_id        
 INNER JOIN seguros_db..endosso_tb endosso_tb (NOLOCK)        
    ON endosso_tb.proposta_id = sega9222.proposta_id       
 INNER JOIN seguros_db..tp_endosso_tb tp_endosso_tb (NOLOCK)        
    ON tp_endosso_tb.tp_endosso_id = endosso_tb.tp_endosso_id    

  

INSERT INTO interface_dados_db..sega9222_30_processar_tb                                  
   (SEGA9222_processar_id,
	razao_social,
	cnpj,
	endereco_corresp,
	bairro,
	cidade,
	uf,
	cep,
	telefone,
	DDD,
	dt_inclusao,
	dt_alteracao,
	usuario)        
SELECT SEGA9222_processar_id,
	razao_social,
	cnpj,
	endereco_corresp,
	bairro,
	cidade,
	uf,
	cep,
	telefone,
	DDD,
	GETDATE(),
	NULL,
	@usuario
  FROM #SEGA9222_30      
  
  
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update sega9222_30_processar_tb
	set sega9222_30_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db.dbo.sega9222_30_processar_tb sega9222_30_processar_tb with(nolock)
	inner join interface_dados_db.dbo.sega9222_processar_tb sega9222_processar_tb with(nolock)
	on sega9222_processar_tb.SEGA9222_processar_id = sega9222_30_processar_tb.SEGA9222_processar_id 
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = sega9222_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id  
                                  
                                
                           
SET NOCOUNT OFF                                    
                                      
RETURN                                    
                                    
END TRY

BEGIN CATCH                                                
    SELECT @msg = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE()                                        
                                            

   -- Confitec  sql2012  07/08/2015 16:11:34 - Inicio --
    --raiserror 444444 @msg  
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '444444 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec  sql2012  07/08/2015 16:11:34 - Fim --
  
END CATCH    
  
  

