CREATE PROCEDURE SEGS13171_SPI    
    @usuario VARCHAR(20),          
    @num_versao INT          
          
AS    
        
          
SET NOCOUNT ON          
 /*  
begin tran   
 exec SEGS13171_SPI 'teste', 1  
rollback  
*/           
DECLARE @msg VARCHAR(100)          
      
BEGIN TRY   	  
	  
--Copiando dados para SEGA9222_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9222_processado_tb      
      (SEGA9222_processar_id,
		proposta_id,
		proposta_bb,
		apolice_id,
		ramo_id,
		produto_id,
		seguradora_cod_susep,
		sucursal_seguradora_id,
		tp_documento_id,
		tipo_documento,
		endosso_id,
		layout_id,
		num_solicitacao,
		num_versao,
		dt_inclusao,
		dt_alteracao,
		usuario)          
SELECT SEGA9222_processar_id,
		proposta_id,
		proposta_bb,
		apolice_id,
		ramo_id,
		produto_id,
		seguradora_cod_susep,
		sucursal_seguradora_id,
		tp_documento_id,
		tipo_documento,
		endosso_id,
		layout_id,
		num_solicitacao,
        @num_versao,		
       getdate() dt_inclusao,
	   NULL,	   
       @usuario as usuario         
  FROM interface_dados_db.dbo.SEGA9222_processar_tb           
                  

INSERT INTO interface_dados_db..SEGA9222_10_processar_tb(
		SEGA9222_processar_id,
		proposta_id,
		endosso_id,
		nome_destinatario,
		endereco_destinatario,
		municipio_destinatario,
		UF_destinatario,
		CEP_destinatario,
		cod_retorno,
		tipo_documento,
		descricao_documento,
		dt_inclusao,
		dt_alteracao,
		usuario,
		GrupoRamo_RamoID)
SELECT  
		SEGA9222_processar_id,
		proposta_id,
		endosso_id,
		nome_destinatario,
		endereco_destinatario,
		municipio_destinatario,
		UF_destinatario,
		CEP_destinatario,
		cod_retorno,
		tipo_documento,
		descricao_documento,
        getdate() dt_inclusao,
		NULL,
        @usuario as usuario,
	    GrupoRamo_RamoID		
 FROM interface_dados_db.dbo.SEGA9222_10_processar_tb       


          
--Copiando dados para SEGA9222_20_processado_tb          
INSERT INTO interface_dados_db.dbo.SEGA9222_20_processado_tb          
      (SEGA9222_processar_id,
		proposta_id,
		proposta_bb,
		apolice_id,
		cod_agencia,
		nome_agencia,
		prazo_seguro,
		dt_inicio_vigencia,
		dt_fim_vigencia,
		dt_inclusao,
		dt_alteracao,
		usuario)          
SELECT  SEGA9222_processar_id,
		proposta_id,
		proposta_bb,
		apolice_id,
		cod_agencia,
		nome_agencia,
		prazo_seguro,
		dt_inicio_vigencia,
		dt_fim_vigencia,
        getdate() dt_inclusao,
		NULL,
        @usuario as usuario      
 FROM interface_dados_db.dbo.SEGA9222_20_processar_tb       
          
      
--Copiando dados para SEGA9222_30_processado_tb          
INSERT INTO interface_dados_db.dbo.SEGA9222_30_processado_tb                  
		(SEGA9222_processar_id,
		razao_social,
		CNPJ,
		Endereco_corresp,
		Bairro,
		Cidade,
		UF,
		CEP,
		Telefone,
		cpf_cnpj,
		dt_inclusao,
		dt_alteracao,
		usuario)      
SELECT SEGA9222_processar_id,
		razao_social,
		CNPJ,
		Endereco_corresp,
		Bairro,
		Cidade,
		UF,
		CEP, 
		Telefone,    
		cpf_cnpj,
        getdate() dt_inclusao,
        NULL,    
        @usuario as usuario         
  FROM interface_dados_db.dbo.SEGA9222_30_processar_tb     
    
       
          
--Copiando dados para SEGA9222_30_processado_tb          
INSERT INTO interface_dados_db.dbo.SEGA9222_40_processado_tb          
      (SEGA9222_processar_id,
		Qtd_funcionarios,
		Nome_cob_bas_func,
		Cap_glob_cob_bas,
		Prem_liq_men_cob_bas,
		dt_inclusao,
		dt_alteracao,
		usuario)     
SELECT 	SEGA9222_processar_id,
		Qtd_funcionarios,
		Nome_cob_bas_func,
		Cap_glob_cob_bas,
		Prem_liq_men_cob_bas,       
        getdate() dt_inclusao,
        NULL,          
        @usuario as usuario        
  FROM interface_dados_db.dbo.SEGA9222_40_processar_tb        
          
       
        
        
--Copiando dados para SEGA9222_50_processado_tb          
INSERT INTO interface_dados_db.dbo.SEGA9222_50_processado_tb         
      (SEGA9222_processar_id,
		Nome_cob_adic_func,
		Cap_glob_cob_adic,
		Prem_liq_men_cob_adic,
		dt_inclusao,
		dt_alteracao,
		usuario)          
SELECT 	SEGA9222_processar_id,
		Nome_cob_adic_func,
		Cap_glob_cob_adic,
		Prem_liq_men_cob_adic,     
        getdate() dt_inclusao,   
        NULL,
        @usuario as usuario        
  FROM interface_dados_db.dbo.SEGA9222_50_processar_tb            
          
     
      
--Copiando dados para SEGA9222_60_processado_tb          
INSERT INTO interface_dados_db.dbo.SEGA9222_60_processado_tb          
      ( SEGA9222_processar_id,
		Qtd_soc_dir,
		Nome_cob_bas_soc_dir,
		Cap_glob_cob_bas,
		Prem_liq_men_cob_bas,
		dt_inclusao,
		dt_alteracao,
		usuario)           
SELECT  SEGA9222_processar_id,
		Qtd_soc_dir,
		Nome_cob_bas_soc_dir,
		Cap_glob_cob_bas,
		Prem_liq_men_cob_bas,
       getdate() dt_inclusao,
       NULL,     
       @usuario as usuario         
  FROM interface_dados_db.dbo.SEGA9222_60_processar_tb      
          

  
 INSERT INTO interface_dados_db.dbo.SEGA9222_70_processado_tb  
			 ( SEGA9222_processar_id,
				Nome_cob_adic_soc_dir,
				Cap_glob_cob_adic,
				Prem_liq_men_cob_adic,
				dt_inclusao,
				dt_alteracao,
				usuario)  
 SELECT SEGA9222_processar_id,
	Nome_cob_adic_soc_dir,
	Cap_glob_cob_adic,
	Prem_liq_men_cob_adic,
	getdate() dt_inclusao,
	NULL,   
    @usuario as usuario   
  FROM interface_dados_db.dbo.SEGA9222_70_processar_tb     
  

     
  
 INSERT INTO interface_dados_db.dbo.SEGA9222_80_processado_tb  
			 ( 	SEGA9222_processar_id,
				Prem_liq_men,
				IOF,
				Prem_bruto_men,
				Periodicidade,
				Forma_pgto,
				Dia_vencimento,
				Banco,
				Nome_agencia_deb,
				conta_corrente_debito,
				dt_inclusao,
				dt_alteracao,
				usuario)  
 SELECT     	SEGA9222_processar_id,
				Prem_liq_men,
				IOF,
				Prem_bruto_men,
				Periodicidade,
				Forma_pgto,
				Dia_vencimento,
				Banco,
				Nome_agencia_deb,
				conta_corrente_debito,
				getdate() dt_inclusao,
				NULL,   
				@usuario as usuario   
  FROM interface_dados_db.dbo.SEGA9222_80_processar_tb     
  


            
-- Excluindo os registros das tabelas processar          
DELETE FROM interface_dados_db.dbo.SEGA9222_10_processar_tb       
DELETE FROM interface_dados_db.dbo.SEGA9222_20_processar_tb         
DELETE FROM interface_dados_db.dbo.SEGA9222_30_processar_tb          
DELETE FROM interface_dados_db.dbo.SEGA9222_40_processar_tb          
DELETE FROM interface_dados_db.dbo.SEGA9222_50_processar_tb 
DELETE FROM interface_dados_db.dbo.SEGA9222_60_processar_tb 
DELETE FROM interface_dados_db.dbo.SEGA9222_70_processar_tb 
DELETE FROM interface_dados_db.dbo.SEGA9222_80_processar_tb 
DELETE FROM interface_dados_db.dbo.SEGA9222_processar_tb          
          
      
          
SET NOCOUNT OFF          
          
RETURN          
          
END TRY

BEGIN CATCH                                                
    SELECT @msg = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE()                                        
                                            

   -- Confitec  sql2012  07/08/2015 16:11:34 - Inicio --
    --raiserror 444444 @msg  
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '444444 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec  sql2012  07/08/2015 16:11:34 - Fim --
  
END CATCH    

