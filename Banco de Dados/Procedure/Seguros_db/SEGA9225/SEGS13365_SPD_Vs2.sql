CREATE PROCEDURE dbo.SEGS13365_SPD  
    
@usuario     VARCHAR(20),      
@num_remessa INT         
        
AS        
  
BEGIN  
BEGIN TRY        
SET NOCOUNT ON         
         
  
  
INSERT INTO interface_dados_db.dbo.sega9225_processado_tb (SEGA9225_processar_id  
            ,proposta_id  
            ,num_cotacao_seguro  
            ,num_versao_cotacao  
            ,cod_produto  
            ,cod_modalidade  
            ,cod_item_modalidade  
            ,num_contrato_seguro  
            ,produto_id  
            ,MotivoRecusa  
            ,NumeroPropostaBB  
            ,CodigoAgencia  
            ,DVCodAgencia  
            ,NomeAgenciaCorretor  
            ,num_proc_susep  
            ,destino  
            ,NumeroEndosso  
            ,Tipo_Documento  
            ,num_remessa  
            ,layout_id  
            ,tp_doc  
            ,NomeSegurado  
            ,CepDestino  
            ,NomeProduto  
            ,dt_inclusao  
            ,usuario)    
      
SELECT SEGA9225_processar_id  
   ,proposta_id  
   ,num_cotacao_seguro  
   ,num_versao_cotacao  
   ,cod_produto  
   ,cod_modalidade  
   ,cod_item_modalidade  
   ,num_contrato_seguro  
   ,produto_id  
   ,MotivoRecusa  
   ,NumeroPropostaBB  
   ,CodigoAgencia  
   ,DVCodAgencia  
   ,NomeAgenciaCorretor  
   ,num_proc_susep  
   ,destino  
   ,NumeroEndosso  
   ,Tipo_Documento  
   ,@num_remessa  
   ,layout_id  
   ,tp_doc  
   ,NomeSegurado  
   ,CepDestino  
   ,NomeProduto  
   ,GETDATE()  
   ,@usuario      
 FROM interface_dados_db.dbo.sega9225_processar_tb WITH (NOLOCK)    
   
        
INSERT INTO interface_dados_db.dbo.SEGA9225_10_processado_tb (SEGA9225_10_processar_id  
             ,SEGA9225_processar_id  
             ,tipo_registro  
             ,sequencial  
             ,NumeroPropostaAB  
             ,NomeDestinatario  
             ,EnderecoDestinatario  
             ,MunicipioDestino  
             ,UFdestino  
             ,CepDestino  
             ,Codigoretorno  
             ,NumeroEndosso  
             ,Tipo_Documento  
             ,destino  
             ,Nome_endosso  
             ,Data_endosso  
             --,num_solicitacao  
             ,layout_id  
             ,num_remessa  
             ,GerenciaDestino  
             ,dt_inclusao  
             ,usuario)      
SELECT   
  SEGA9225_10_processar_id  
  ,SEGA9225_processar_id  
  ,tipo_registro  
  ,sequencial  
  ,NumeroPropostaAB  
  ,NomeDestinatario  
  ,EnderecoDestinatario  
  ,MunicipioDestino  
  ,UFdestino  
  ,CepDestino  
  ,Codigoretorno  
  ,NumeroEndosso  
  ,Tipo_Documento  
  ,destino  
  ,Nome_endosso  
  ,Data_endosso  
  --,num_solicitacao  
  ,layout_id  
  ,@num_remessa  
  ,GerenciaDestino  
        ,GETDATE()      
        ,@usuario      
FROM interface_dados_db.dbo.SEGA9225_10_processar_tb WITH (NOLOCK)    
        
    
    
          
INSERT INTO interface_dados_db.dbo.SEGA9225_20_processado_tb ( SEGA9225_20_processar_id  
                ,SEGA9225_processar_id  
                ,tipo_registro  
                ,sequencial  
                ,NumeroPropostaAB  
                ,NomeSegurado  
                ,EnderecoSegurado  
                ,MunicipioSegurado  
                ,UFSegurado  
                ,CepSegurado  
                ,NomeProduto  
                ,Motivorecusa  
                ,NumeroPropostaBB  
                ,CodigoAgencia  
                ,DVCodAgencia  
                ,NomeAgenciaCorretor  
                ,num_proc_susep  
                ,apolice_id  
                ,cotacao_id  
                ,layout_id  
                ,num_remessa  
                ,dt_inclusao  
                ,usuario
                ,ddd_celular --migracao-documentacao-digital-2a-fase-cartas
                ,celular --migracao-documentacao-digital-2a-fase-cartas
                ,email--migracao-documentacao-digital-2a-fase-cartas 
                ,produto_id
                ,ramo_id)
                      
 SELECT SEGA9225_20_processar_id  
  ,SEGA9225_processar_id  
  ,tipo_registro  
  ,sequencial  
  ,NumeroPropostaAB  
  ,NomeSegurado  
  ,EnderecoSegurado  
  ,MunicipioSegurado  
  ,UFSegurado  
  ,CepSegurado  
  ,NomeProduto  
  ,Motivorecusa  
  ,NumeroPropostaBB  
  ,CodigoAgencia  
  ,DVCodAgencia  
  ,NomeAgenciaCorretor  
  ,num_proc_susep  
  ,apolice_id  
  ,cotacao_id  
  ,layout_id  
  ,@num_remessa      
  ,GETDATE()      
  ,@usuario	
  ,ddd_celular --migracao-documentacao-digital-2a-fase-cartas
  ,celular --migracao-documentacao-digital-2a-fase-cartas
  ,email  --migracao-documentacao-digital-2a-fase-cartas   
  ,produto_id
  ,ramo_id  
   FROM interface_dados_db.dbo.SEGA9225_20_processar_tb WITH (NOLOCK)    
              
    
  DELETE SEGA9225_10_processar_tb FROM interface_dados_db.dbo.SEGA9225_10_processar_tb SEGA9225_10_processar_tb  
  DELETE SEGA9225_20_processar_tb FROM interface_dados_db.dbo.SEGA9225_20_processar_tb SEGA9225_20_processar_tb  
  DELETE SEGA9225_processar_tb FROM interface_dados_db.dbo.SEGA9225_processar_tb SEGA9225_processar_tb  
    
    
    
  SET NOCOUNT OFF    
    
  RETURN  
END TRY  
  
BEGIN CATCH    
 DECLARE @ErrorMessage NVARCHAR(4000)  
  ,@ErrorSeverity INT  
  ,@ErrorState INT  
    
  SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()  
   ,@ErrorSeverity = ERROR_SEVERITY()  
   ,@ErrorState = ERROR_STATE()  
     
   RAISERROR(  
    @ErrorMessage  
    ,@ErrorSeverity  
    ,@ErrorState  
    )  
END CATCH  
END  

