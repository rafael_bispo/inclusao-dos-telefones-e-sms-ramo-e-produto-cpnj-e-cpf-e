CREATE PROCEDURE dbo.SEGS13358_SPI  
(    
@usuario    VARCHAR(20)    
)    
AS    
  
BEGIN  
BEGIN TRY    
SET NOCOUNT ON          
  
------------------------------------------------------------  
--**************SEGA9225_PROCESSAR_20************************  
------------------------------------------------------------       
  
/*  
-- TESTES --------------------------------------      
  declare  @layout_id      int    
          ,@usuario        varchar(20)    
  
  select @layout_id = 1824  
        ,@usuario = 'producao3'  
    
  begin tran  
    exec interface_dados_db.dbo.sega9225_processar_spi @layout_id, @usuario  
    exec seguros_db.dbo.segs9225_processar_10_spi @usuario  
    exec interface_dados_db.dbo.sega9225_processar_20_spi @usuario  
  
    select * from dbo.sega9225_processar_tb (nolock)  
    select * from dbo.sega9225_10_processar_tb (nolock)  
    select * from dbo.sega9225_20_processar_tb (nolock)  
  rollback  
------------------------------------------------      
*/      
      
-- DETALHE SEGURADO    
CREATE TABLE #DETALHE_20_SEGA9225 (  
    sequencial INT NOT NULL IDENTITY(1,1) PRIMARY KEY,  
    TipoRegistro VARCHAR(60) NULL,  
    NumeroPropostaAB numeric (9,0) NULL,  
    NomeSegurado VARCHAR(60) NULL,  
    EnderecoSegurado VARCHAR(90) NULL,  
    MunicipioSegurado VARCHAR(60) NULL,  
    UFSegurado CHAR(2) NULL,  
    CepSegurado VARCHAR(8) NULL,  
    NomeProduto VARCHAR(60) NULL,  
    MotivoRecusa VARCHAR(60) NULL,  
    NumeroPropostaBB NUMERIC(9,0) NULL,  
    CodigoAgencia NUMERIC(4,0) NULL,  
    DVCodAgencia CHAR(1) NULL,  
    NomeAgenciaCorretor VARCHAR(60) NULL,  
    num_proc_susep VARCHAR(25) NULL,  
    apolice_id NUMERIC(9,0) NULL,  
    cotacao_id INT NULL,  
    sega9225_processar_id INT NULL,  
    layout_id INT NULL,  
    cliente_ctc_atualizado char(1),
    ddd_celular			   VARCHAR(4),--migracao-documentacao-digital-2a-fase-cartas	  
	celular				   VARCHAR(9),--migracao-documentacao-digital-2a-fase-cartas
	email				   VARCHAR(60),--migracao-documentacao-digital-2a-fase-cartas  
	produto_id			   INT,
	ramo_id				   INT
)  
  
INSERT INTO #DETALHE_20_SEGA9225 (  
    TipoRegistro,  
    NumeroPropostaAB,  
    NomeSegurado,  
    EnderecoSegurado,  
    MunicipioSegurado,  
    UFSegurado,  
    CepSegurado,  
    NomeProduto,  
    MotivoRecusa,  
    NumeroPropostaBB,  
    CodigoAgencia,  
    DVCodAgencia,  
    NomeAgenciaCorretor,  
    num_proc_susep,  
    apolice_id,  
    cotacao_id,  
    sega9225_processar_id,  
    layout_id,  
    cliente_ctc_atualizado
 )  
SELECT '20' TipoRegistro,    
       SEGA9225_processar_tb.proposta_id NumeroPropostaAB,    
       SEGA9225_processar_tb.NomeSegurado NomeSegurado,    
       cliente_ctc.TX_LGR_CRSD + ' ' + cliente_ctc.TX_BAI_CRSD EnderecoSegurado,    
       cliente_ctc.TX_MUN_CRSD MunicipioSegurado,    
       cliente_ctc.SG_UF_CRSD UFSegurado,    
       RIGHT('00000000' + CONVERT(VARCHAR(8),cliente_ctc.NR_CEP_CRSD),8) AS CepSegurado,    
       SEGA9225_processar_tb.NomeProduto NomeProduto,    
       SEGA9225_processar_tb.MotivoRecusa,    
       SEGA9225_processar_tb.NumeroPropostaBB NumeroPropostaBB,    
       SEGA9225_processar_tb.CodigoAgencia CodigoAgencia,    
       SEGA9225_processar_tb.DVCodAgencia DVCodAgencia,    
       SEGA9225_processar_tb.NomeAgenciaCorretor NomeAgenciaCorretor,    
       SEGA9225_processar_tb.num_proc_susep,  
       apolice_tb.apolice_id,  
       avaliacao_cotacao_endosso_tb.num_cotacao_seguro as cotacao_id,    
       SEGA9225_processar_tb.sega9225_processar_id,    
       SEGA9225_processar_tb.layout_id,  
       cliente_ctc.atualizado    
 FROM interface_dados_db.dbo.SEGA9225_processar_tb SEGA9225_processar_tb (NOLOCK)    
INNER JOIN seguros_db.dbo.avaliacao_cotacao_endosso_tb avaliacao_cotacao_endosso_tb with(nolock)  
   ON SEGA9225_processar_tb.proposta_id = avaliacao_cotacao_endosso_tb.proposta_id   
INNER JOIN als_operacao_db.dbo.cliente_ctc cliente_ctc with(nolock)  
   ON avaliacao_cotacao_endosso_tb.num_cotacao_seguro = cliente_ctc.nr_ctc_sgro   
  AND avaliacao_cotacao_endosso_tb.num_versao_cotacao = cliente_ctc.nr_vrs_ctc   
  AND avaliacao_cotacao_endosso_tb.cod_produto = cliente_ctc.cd_prd   
  AND avaliacao_cotacao_endosso_tb.cod_modalidade = cliente_ctc.cd_mdld   
  AND avaliacao_cotacao_endosso_tb.cod_item_modalidade = cliente_ctc.cd_item_mdld     
INNER JOIN als_operacao_db.dbo.PRPN_CTC PRPN_CTC with(nolock)  
   ON cliente_ctc.cd_prd = prpn_ctc.cd_prd  
  AND cliente_ctc.cd_mdld = prpn_ctc.cd_mdld  
  AND cliente_ctc.cd_item_mdld = prpn_ctc.cd_item_mdld  
  AND cliente_ctc.nr_ctc_sgro = prpn_ctc.nr_ctc_sgro  
  AND cliente_ctc.nr_vrs_ctc = prpn_ctc.nr_vrs_ctc  
  AND cliente_ctc.cd_cli = prpn_ctc.cd_pss_ctc  
  AND prpn_ctc.cd_tip_prpn_ctc in (0,1) --titular  
INNER JOIN seguros_db.dbo.proposta_fechada_tb proposta_fechada_tb with(nolock)  
   ON proposta_fechada_tb.proposta_id = avaliacao_cotacao_endosso_tb.proposta_id   
INNER JOIN seguros_db.dbo.apolice_tb apolice_tb with(nolock)  
   ON apolice_tb.proposta_id = avaliacao_cotacao_endosso_tb.proposta_id     
GROUP BY   
      SEGA9225_processar_tb.proposta_id,   
      SEGA9225_processar_tb.produto_id,    
   SEGA9225_processar_tb.NomeSegurado,    
   cliente_ctc.TX_LGR_CRSD,  
   cliente_ctc.TX_BAI_CRSD,    
      cliente_ctc.TX_MUN_CRSD,    
   cliente_ctc.SG_UF_CRSD,    
   cliente_ctc.NR_CEP_CRSD,  
   SEGA9225_processar_tb.NomeProduto,    
   SEGA9225_processar_tb.MotivoRecusa,    
   SEGA9225_processar_tb.NumeroPropostaBB,    
   SEGA9225_processar_tb.CodigoAgencia,    
   SEGA9225_processar_tb.DVCodAgencia,    
   SEGA9225_processar_tb.NomeAgenciaCorretor,    
   SEGA9225_processar_tb.num_proc_susep,  
   apolice_tb.apolice_id,  
   avaliacao_cotacao_endosso_tb.num_cotacao_seguro ,    
   SEGA9225_processar_tb.sega9225_processar_id,    
   SEGA9225_processar_tb.layout_id,  
      cliente_ctc.atualizado            
ORDER BY cliente_ctc.NR_CEP_CRSD,    
         SEGA9225_processar_tb.produto_id,    
         SEGA9225_processar_tb.proposta_id    
           
           
 UPDATE temp  
 SET NomeSegurado = cliente_tb.nome,     
  EnderecoSegurado = endereco_corresp_tb.endereco,  
  MunicipioSegurado = endereco_corresp_tb.municipio,    
  CepSegurado = endereco_corresp_tb.cep,    
  UFSegurado = endereco_corresp_tb.estado  
   FROM #DETALHE_20_SEGA9225 temp WITH (NOLOCK)  
  inner join seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)       
     on temp.NumeroPropostaAB = proposta_tb.proposta_id   
  inner join seguros_db.dbo.cliente_tb cliente_tb WITH (NOLOCK)       
  on cliente_tb.cliente_id = proposta_tb.prop_cliente_id  
  inner join seguros_db.dbo.endereco_corresp_tb endereco_corresp_tb WITH (NOLOCK)       
  on endereco_corresp_tb.proposta_id = proposta_tb.proposta_id  
  WHERE temp.cliente_ctc_atualizado = 'N'  
  
  
   -- Atualizando o contatos do segurado --------------------------------------------  
  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
  UPDATE #DETALHE_20_SEGA9225
  SET ddd_celular = ISNULL(cliente_tb.ddd_1,''), celular = ISNULL(cliente_tb.telefone_1,''), email = ISNULL(cliente_tb.e_mail,''),
	   produto_id = proposta_tb.produto_id, ramo_id =  proposta_tb.ramo_id   
  FROM #DETALHE_20_SEGA9225 (NOLOCK)  
  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
	ON #DETALHE_20_SEGA9225.NumeroPropostaAB = proposta_tb.proposta_id  
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
    ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id 
    
             
    
/* DADOS DO SEGURADO */      
INSERT INTO interface_dados_db.dbo.SEGA9225_20_processar_tb(SEGA9225_processar_id,      
                                                         layout_id,      
                                                         NumeroPropostaAB,      
                                                         NomeSegurado,      
                                                         EnderecoSegurado,      
                                                         MunicipioSegurado,      
                                                         UFSegurado,      
                                                         CepSegurado,      
                                                         NomeProduto,      
                                                         Motivorecusa,      
                                                         NumeroPropostaBB,      
                                                         CodigoAgencia,      
                                                         DVCodAgencia,      
                                                         NomeAgenciaCorretor,      
                                                         num_proc_susep,  
                                                         apolice_id,  
                                                         cotacao_id,      
                                                         sequencial,      
                                                         tipo_registro,      
														 dt_inclusao,      
                                                         usuario,
                                                         ddd_celular,--migracao-documentacao-digital-2a-fase-cartas
                                                         celular,--migracao-documentacao-digital-2a-fase-cartas
                                                         email,--migracao-documentacao-digital-2a-fase-cartas      
														 produto_id,
														 ramo_id)
SELECT DISTINCT #DETALHE_20_SEGA9225.SEGA9225_processar_id,      
       #DETALHE_20_SEGA9225.layout_id AS layout_id,      
       #DETALHE_20_SEGA9225.NumeroPropostaAB AS NumeroPropostaAB,      
       #DETALHE_20_SEGA9225.NomeSegurado AS NomeSegurado,      
       #DETALHE_20_SEGA9225.EnderecoSegurado AS EnderecoSegurado,      
       #DETALHE_20_SEGA9225.MunicipioSegurado AS MunicipioSegurado,      
       #DETALHE_20_SEGA9225.UFSegurado AS UFSegurado,      
       LEFT(#DETALHE_20_SEGA9225.CepSegurado, 05) + '-' + RIGHT(#DETALHE_20_SEGA9225.CepSegurado, 03) AS CepSegurado,       
       #DETALHE_20_SEGA9225.NomeProduto AS NomeProduto,      
       #DETALHE_20_SEGA9225.Motivorecusa AS Motivorecusa,      
       #DETALHE_20_SEGA9225.NumeroPropostaBB AS NumeroPropostaBB,      
       #DETALHE_20_SEGA9225.CodigoAgencia AS CodigoAgencia1,      
       #DETALHE_20_SEGA9225.DVCodAgencia AS DVCodAgencia,      
       #DETALHE_20_SEGA9225.NomeAgenciaCorretor AS NomeAgenciaCorretor,      
       #DETALHE_20_SEGA9225.num_proc_susep AS num_proc_susep,   
       #DETALHE_20_SEGA9225.apolice_id AS apolice_id,   
       #DETALHE_20_SEGA9225.cotacao_id AS cotacao_id,      
       #DETALHE_20_SEGA9225.sequencial AS sequencial,      
       #DETALHE_20_SEGA9225.tiporegistro AS tipo_registro,      
       GETDATE() AS dt_inclusao,      
       @usuario AS usuario,
       #DETALHE_20_SEGA9225.ddd_celular, --migracao-documentacao-digital-2a-fase-cartas      
       #DETALHE_20_SEGA9225.celular, --migracao-documentacao-digital-2a-fase-cartas      
       #DETALHE_20_SEGA9225.email, --migracao-documentacao-digital-2a-fase-cartas      
       #DETALHE_20_SEGA9225.produto_id,
       #DETALHE_20_SEGA9225.ramo_id
FROM #DETALHE_20_SEGA9225 WITH (NOLOCK)    
ORDER BY sequencial    
    
         
      
SET NOCOUNT OFF          
    
RETURN  
END TRY  
  
BEGIN CATCH    
  DECLARE @ErrorMessage NVARCHAR(4000)  
   , @ErrorSeverity INT  
   , @ErrorState INT  
  
  SELECT @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15), ERROR_LINE()) + ' - ' + ERROR_MESSAGE()  
   , @ErrorSeverity = ERROR_SEVERITY()  
   , @ErrorState = ERROR_STATE()  
  
  RAISERROR (  
    @ErrorMessage  
    , @ErrorSeverity  
    , @ErrorState  
                 )  
END CATCH     
  END  

