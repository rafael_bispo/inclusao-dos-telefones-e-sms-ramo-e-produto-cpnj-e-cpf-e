CREATE PROCEDURE SEGS11926_SPI  
        
    @usuario UD_usuario                                   
        
AS                                    
-----------------------------------------------------------------------------------------------        
-- PARÂMETROS DE ENTRADA PARA TESTE        
/*         
EXEC SEGS10012_SPI 1636,1208,'PRODOCAO01'         
SET @p_usuario = 'PRODOCAO01'    
PROC DE INSER��O NA SEGA9177_20_PROCESSAR_TB       
*/         
-----------------------------------------------------------------------------------------------         
-----------------------------------------------------------                                    
-- Procedure Detalhe 20 - Segurado SEGA9177             --                                    
-----------------------------------------------------------                                    
        
BEGIN TRY    
    
  SET NOCOUNT ON                                    
            
  DECLARE @dt_sistema SMALLDATETIME          
  DECLARE @p_usuario UD_usuario    
        
  SELECT @dt_sistema = dt_operacional,    
      @p_usuario = @usuario    
    FROM seguros_db.dbo.parametro_geral_tb  WITH(NOLOCK)                
           
  SELECT DISTINCT SEGA9177.SEGA9177_processar_id,        
      SEGA9177.proposta_id,    
      SEGA9177.apolice_id apolice_id,        
      SEGA9177.proposta_bb,        
      c.nome nome_segurado,
      isnull(c.ddd_1,'') as ddd_celular,
      isnull(c.telefone_1,'') as celular,
      isnull(c.e_mail,'') AS email,        
      prod.nome nome_produto,        
      ISNULL(ce.certificado_id,0) AS certificado_id,    
      aca.num_cobranca,      
      aca.dt_agendamento as dt_vencimento,      
      aca.val_cobranca as val_premio,      
--      dt_vencimento_proxima_parcela = CASE ISDATE(aca_proxima.dt_agendamento) WHEN 1 THEN ISNULL(REPLACE(CONVERT(VARCHAR(10),aca_proxima.dt_agendamento,103),'/',''),'')    
--                         ELSE NULL    
--              END,    
      dt_vencimento_proxima_parcela = ISNULL(aca_proxima.dt_agendamento, '19000101'),  --Zoro.Gomes - Confitec - 06/08/2014 - DEMANDA: 17919477 - com esta linha elimina a repeticao de propostas - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma  
      dt_inclusao = GETDATE(),          
      usuario = @p_usuario,    
      dia_emissao = DAY(ISNULL(aci.dt_envio_aviso,@dt_sistema)),    
      mes_emissao = CASE MONTH(ISNULL(aci.dt_envio_aviso,@dt_sistema)) WHEN 01 THEN 'Janeiro'    
                    WHEN 02 THEN 'Fevereiro'    
                    WHEN 03 THEN 'Mar�o'    
                    WHEN 04 THEN 'Abril'    
                    WHEN 05 THEN 'Maio'    
                    WHEN 06 THEN 'Junho'    
                    WHEN 07 THEN 'Julho'    
                    WHEN 08 THEN 'Agosto'    
                    WHEN 09 THEN 'Setembro'     
                    WHEN 10 THEN 'Outubro'     
                    WHEN 11 THEN 'Novembro'     
                    WHEN 12 THEN 'Dezembro'                  
                    ELSE ''    
        END,    
      ano_emissao = RIGHT(YEAR(ISNULL(aci.dt_envio_aviso,@dt_sistema)),2),    
      ec.endereco as endereco_proponente, 
      CAST('' AS VARCHAR(14)) as cpf_cnpj   
    INTO #SEGA9177_20    
    FROM interface_dados_db.dbo.SEGA9177_processar_tb SEGA9177 WITH(NOLOCK)                                    
   INNER JOIN seguros_db.dbo.proposta_tb p WITH(NOLOCK)        
   ON p.proposta_id = SEGA9177.proposta_id           
   INNER JOIN seguros_db.dbo.produto_tb prod WITH(NOLOCK)        
   ON prod.produto_id = p.produto_id        
   LEFT JOIN seguros_db.dbo.aviso_cancelamento_inadimplencia_tb aci WITH(NOLOCK)    
   ON prod.produto_id = aci.produto_id    
     AND aci.dt_cancelamento IS NULL              
     and SEGA9177.PROPOSTA_ID = ACI.PROPOSTA_ID --Zoro.Gomes - Confitec - 06/08/2014 - DEMANDA: 17919477 - com esta linha elimina a repeticao de propostas - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma    
   INNER JOIN seguros_db.dbo.cliente_tb c WITH(NOLOCK)         
   ON c.cliente_id = p.prop_cliente_id    
   LEFT JOIN seguros_db.dbo.endereco_corresp_tb ec WITH(NOLOCK)    
   ON p.proposta_id = ec.proposta_id       
  LEFT JOIN seguros_db.dbo.certificado_tb ce    WITH (NOLOCK)        
   ON ce.proposta_id = p.proposta_id        
     AND ce.dt_inicio_vigencia < @dt_sistema          
  INNER JOIN seguros_db.dbo.agendamento_cobranca_atual_tb aca WITH (NOLOCK)      
     ON aca.proposta_id = p.proposta_id      
     AND aca.apolice_id = SEGA9177.apolice_id    
     AND aca.num_cobranca = SEGA9177.num_cobranca    
  LEFT JOIN seguros_db.dbo.agendamento_cobranca_atual_tb aca_proxima WITH (NOLOCK)    
     ON aca_proxima.proposta_id = aca.proposta_id    
     AND aca_proxima.apolice_id = aca.apolice_id     
--     AND aca_proxima.num_cobranca = aca.num_cobranca + 1     --Zoro.Gomes - Confitec - 06/08/2014 - Para pegar a data de vencimento correta - DEMANDA: 17919477 - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma    
     AND aca_proxima.num_cobranca = (SELECT TOP 1 num_cobranca   
                                       FROM seguros_db.dbo.agendamento_cobranca_atual_tb a WITH (NOLOCK)  
                                      WHERE a.proposta_id = p.proposta_id  
                                        AND a.apolice_id = aca.apolice_id  
                                        AND a.dt_agendamento >= @dt_sistema)  
                                     
  -- SBRJ0015154 - Altera��o do nome do Produto para exibi��o do Nome do Plano para o Produto 1237 - RMarins 11/11/2019  
  UPDATE #SEGA9177_20     
     SET nome_produto = plano_tb.nome        
    FROM #SEGA9177_20  
    JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)   
      ON #SEGA9177_20.proposta_id = proposta_tb.proposta_id  
    JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH (NOLOCK)   
      ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
    JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)                                    
      ON escolha_plano_tb.plano_id = plano_tb.plano_id                                                              
     AND escolha_plano_tb.produto_id = plano_tb.produto_id                                                              
     AND escolha_plano_tb.dt_inicio_vigencia = plano_tb.dt_inicio_vigencia   
   WHERE proposta_tb.produto_id = 1237   
     AND plano_tb.plano_id NOT IN (1,2,3)  
    AND (escolha_plano_tb.endosso_id IS NULL OR escolha_plano_tb.endosso_id = 0)    
    -- SBRJ0015154 - Fim altera��o - RMarins 11/11/2019     
    
    -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #SEGA9177_20
	set #SEGA9177_20.CPF_CNPJ = CASE 
									WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
									WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
									ELSE cliente_tb.cpf_cnpj
								END 
	from #SEGA9177_20 #SEGA9177_20
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
	on proposta_tb.proposta_id = #SEGA9177_20.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
	on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
	on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
	on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id  
	 
          
  INSERT INTO interface_dados_db..SEGA9177_20_processar_tb(    
     SEGA9177_processar_id,        
     proposta_id,        
     apolice_id,        
     proposta_bb,        
     nome_segurado,
     ddd_celular,
     celular,
     email,        
     nome_produto,        
     certificado_id,      
     num_cobranca,      
     dt_vencimento,      
     val_premio,      
     dt_vencimento_proxima_parcela,      
     dia_emissao,    
     mes_emissao,    
     ano_emissao,    
     endereco_proponente,    
     dt_inclusao,    
     usuario)              
    SELECT SEGA9177_processar_id,        
     proposta_id,        
     apolice_id,        
     proposta_bb,        
     nome_segurado,
     ddd_celular,
     celular,
     email,             
     nome_produto,    
     certificado_id = CASE certificado_id WHEN 0 THEN ''    
               ELSE 'Certificado n�:'+CAST(certificado_id AS VARCHAR(15)) -- rogerson.nazario 7/5/2014    
          END,    
     num_cobranca,      
     dt_vencimento,      
     val_premio,     
--     CASE ISDATE(dt_vencimento_proxima_parcela) WHEN 1 THEN CONVERT(VARCHAR(10),dt_vencimento_proxima_parcela ,103)    
--                     ELSE ''    
--     END as dt_vencimento_proxima_parcela,    
     dt_vencimento_proxima_parcela,   -- Zoro.Gomes - Confitec - 06/08/2014 - DEMANDA: 17919477 - Prestamista PJ - BB Seguro Cr�dito Protegido Para Empresa - Fora do Cronograma    
     dia_emissao,    
     mes_emissao,    
     ano_emissao,    
     ISNULL(endereco_proponente,''),       
     dt_inclusao,      
     usuario       
      FROM #SEGA9177_20    
                                      
  SET NOCOUNT OFF                                      
                   
  RETURN                                      
    
END TRY    
BEGIN CATCH    
 DECLARE @ErrorMessage NVARCHAR(4000)          
 DECLARE @ErrorSeverity INT          
 DECLARE @ErrorState INT        
  SELECT           
        @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),          
      @ErrorSeverity = ERROR_SEVERITY(),          
      @ErrorState = ERROR_STATE()          
    
  RAISERROR (@ErrorMessage,          
    @ErrorSeverity,          
    @ErrorState)    
END CATCH     
    
  
  
  
  
  

