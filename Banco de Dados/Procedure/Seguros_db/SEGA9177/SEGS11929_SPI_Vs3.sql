CREATE PROCEDURE SEGS11929_SPI  
  
    @usuario VARCHAR(20),      
    @num_versao INT      
      
AS      

BEGIN TRY
      
		SET NOCOUNT ON   

		DECLARE @p_usuario VARCHAR(20),
				@p_num_versao INT      

		SELECT @p_usuario = @usuario,
			   @p_num_versao = @num_versao      
		    
		  
		--Copiando dados para SEGA9177_processado_tb    
		INSERT INTO interface_dados_db.dbo.SEGA9177_processado_tb(
					SEGA9177_processar_id,  
					proposta_id,  
					apolice_id,  
					proposta_bb,  
					ramo_id,  
					produto_Id,  
					prop_cliente_id,  
					tp_documento_id,  
					tipo_documento,  
					num_solicitacao,          
					num_versao,  
					dt_inclusao,  
					usuario)      
			 SELECT SEGA9177_processar_id,  
					proposta_id,  
					apolice_id,  
					proposta_bb,  
					ramo_id,  
					produto_Id,  
					prop_cliente_id,  
					tp_documento_id,  
					tipo_documento,  
					num_solicitacao,          
					@p_num_versao as num_versao,  
					GETDATE() as dt_inclusao,  
					@p_usuario as usuario    
			   FROM interface_dados_db.dbo.SEGA9177_processar_tb WITH(NOLOCK)     
		  
		INSERT INTO interface_dados_db.dbo.SEGA9177_10_processado_tb( 
					SEGA9177_10_processar_id,   
					SEGA9177_processar_id,  
					proposta_id,  
					nome_destinatario,  
					endereco_destino,  
					bairro_destino,  
					municipio_destinatario,  
					UF_destinatario,  
					CEP_destinatario,
					tipo_documento,    
					gerencia_destino,  
					cod_retorno,    
					dt_inclusao,    
					usuario,
					num_cobranca)    
			 SELECT SEGA9177_10_processar_id,  
					SEGA9177_processar_id,  
					proposta_id,  
					nome_destinatario,  
					endereco_destino,  
					bairro_destino,  
					municipio_destinatario,  
					UF_destinatario,  
					CEP_destinatario,  
					tipo_documento,    
					gerencia_destino,  
					cod_retorno,  
					GETDATE() as dt_inclusao,      
					@p_usuario as usuario,
					num_cobranca    
			   FROM interface_dados_db.dbo.SEGA9177_10_processar_tb WITH(NOLOCK)     
		  
		INSERT INTO interface_dados_db.dbo.SEGA9177_20_processado_tb(
					SEGA9177_20_processar_id,  
					SEGA9177_processar_id,  
					proposta_id,  
					apolice_id,  
					proposta_bb,  
					nome_segurado,  
					ddd_celular,
					celular,
					email,
					cpf_cnpj,    
					nome_produto,  
					certificado_id,
					num_cobranca,
					dt_vencimento,
					val_premio,
					dt_vencimento_proxima_parcela,
					dia_emissao,
					mes_emissao,
					ano_emissao,
					endereco_proponente,
					dt_inclusao,  
					usuario)          
			 SELECT SEGA9177_20_processar_id,  
					SEGA9177_processar_id,  
					proposta_id,  
					apolice_id,  
					proposta_bb,  
					nome_segurado,  
				    ddd_celular,
				    celular,
				    email,
				    cpf_cnpj,    
					nome_produto, 
					certificado_id,
					num_cobranca,
					dt_vencimento,
					val_premio,
					dt_vencimento_proxima_parcela,
					dia_emissao,
					mes_emissao,
					ano_emissao,
					endereco_proponente,			
					GETDATE() as dt_inclusao,      
					@p_usuario as usuario     
			   FROM interface_dados_db.dbo.SEGA9177_20_processar_tb WITH(NOLOCK)     
		      
		-- Excluindo os registros das tabelas processar      
		DELETE FROM interface_dados_db.dbo.SEGA9177_10_processar_tb   
		DELETE FROM interface_dados_db.dbo.SEGA9177_20_processar_tb     
		DELETE FROM interface_dados_db.dbo.SEGA9177_processar_tb         
		      
		SET NOCOUNT OFF      
		      
		RETURN      
      
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage NVARCHAR(4000)      
	DECLARE @ErrorSeverity INT      
	DECLARE @ErrorState INT    
	 SELECT       
  		    @ErrorMessage = ERROR_PROCEDURE() + ' - Linha ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),      
		    @ErrorSeverity = ERROR_SEVERITY(),      
		    @ErrorState = ERROR_STATE()      

  RAISERROR (@ErrorMessage,      
			 @ErrorSeverity,      
			 @ErrorState)
END CATCH	

GO


