CREATE PROCEDURE SEGS10991_SPS  
AS            
         
BEGIN -- MARCIO NOGUEIRA         
            
BEGIN TRY          
        
 IF OBJECT_ID ('TEMPDB..##TEMP_AUX_PROCESSO_RETORNO_MQ_TB') IS NOT NULL        
 BEGIN        
  DROP TABLE ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB        
 END        
      
 SET NOCOUNT ON                     
           
 CREATE TABLE ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB          
      ( TPEMISSAO               CHAR(1)       NULL          
      , APOLICE_ID              NUMERIC(9)    NULL            
      , PROPOSTA_ID             NUMERIC(9)    NULL           
      , DT_INICIO_VIGENCIA      SMALLDATETIME NULL          
      , DT_FIM_VIGENCIA         SMALLDATETIME NULL          
      , ENDOSSO_ID              INT           NULL          
      , DT_PEDIDO_ENDOSSO       SMALLDATETIME NULL      
      , SEGURADORA_COD_SUSEP    NUMERIC(5)    NULL      
      , SUCURSAL_SEGURADORA_ID  NUMERIC(5)    NULL      
      , RAMO_ID                 INT           NULL      
      , APOLICE_ENVIA_CLIENTE   CHAR(1)       NULL      
      , APOLICE_NUM_VIAS        CHAR(1)       NULL      
      , APOLICE_ENVIA_CONGENERE CHAR(1)       NULL      
      , DT_EMISSAO              SMALLDATETIME NULL           
      , PRODUTO_ID              INT           NULL      
      , TP_ENDOSSO_ID           INT           NULL      
      , NUM_PROC_SUSEP          VARCHAR(25)   NULL         
      , IMPRESSAO_LIBERADA      CHAR(1)       NULL      
      , DESTINO                 CHAR(1)       NULL      
      , NUM_SOLICITACAO         INT           NULL      
      , DIRETORIA_ID            TINYINT       NULL      
      , CEP                     VARCHAR(8)    NULL      
      , VAL_LIMITE_MAX_GARANTIA NUMERIC(15,2) NULL --      RPORTO 15/01/2015        
      , CADEMP                  VARCHAR(10)   NULL      
      , VAL_FRANQUIA            VARCHAR(20)   NULL    
      , FL_VERBA_UNICA          VARCHAR(1)    NULL --   ARicardo 24/07/2017 - Verba �nica   
      , CPF_CNPJ				VARCHAR(14)	  NULL 
      )                
                
 INSERT INTO ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB          
      ( TPEMISSAO            
      , APOLICE_ID                                          
      , PROPOSTA_ID                                         
      , DT_INICIO_VIGENCIA                  
      , DT_FIM_VIGENCIA                     
      , ENDOSSO_ID              
      , DT_PEDIDO_ENDOSSO             
      , SEGURADORA_COD_SUSEP                                
      , SUCURSAL_SEGURADORA_ID                              
      , RAMO_ID                 
      , APOLICE_ENVIA_CLIENTE             
      , APOLICE_NUM_VIAS             
      , APOLICE_ENVIA_CONGENERE             
      , DT_EMISSAO                          
      , PRODUTO_ID              
      , TP_ENDOSSO_ID             
      , NUM_PROC_SUSEP                        
      , IMPRESSAO_LIBERADA             
      , DESTINO                 
      , NUM_SOLICITACAO             
      , DIRETORIA_ID             
      , CEP    
      , FL_VERBA_UNICA      --   ARicardo 24/07/2017 - Verba �nica
      )            
 SELECT DISTINCT TOP 1000       
       'A' AS TPEMISSAO          
      , A.APOLICE_ID          
      , A.PROPOSTA_ID          
      , A.DT_INICIO_VIGENCIA          
      , A.DT_FIM_VIGENCIA          
      , 0 ENDOSSO_ID          
      , '19000101' AS DT_PEDIDO_ENDOSSO          
      , A.SEGURADORA_COD_SUSEP          
      , A.SUCURSAL_SEGURADORA_ID          
      , A.RAMO_ID          
      , PD.APOLICE_ENVIA_CLIENTE          
      , PD.APOLICE_NUM_VIAS          
      , PD.APOLICE_ENVIA_CONGENERE          
      , A.DT_EMISSAO          
      , PP.PRODUTO_ID          
      , 0 AS TP_ENDOSSO_ID          
      , A.NUM_PROC_SUSEP          
      , PF.IMPRESSAO_LIBERADA          
      , NULL AS DESTINO      
      , NULL AS NUM_SOLICITACAO      
      , NULL AS DIRETORIA_ID       
      , EC.CEP    
      , PP.FL_VERBA_UNICA   --   ARicardo 24/07/2017 - Verba �nica    
   FROM SEGUROS_DB.DBO.APOLICE_TB                A WITH(NOLOCK)                       
   JOIN SEGUROS_DB.DBO.RAMO_TB                   R WITH(NOLOCK)             
     ON A.RAMO_ID      = R.RAMO_ID                       
   JOIN SEGUROS_DB.DBO.PROPOSTA_TB              PP WITH(NOLOCK)                ON A.PROPOSTA_ID  = PP.PROPOSTA_ID                      
   JOIN SEGUROS_DB.DBO.PRODUTO_TB               PD WITH(NOLOCK)             
     ON PP.PRODUTO_ID  = PD.PRODUTO_ID                      
   JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB      PF WITH(NOLOCK)             
     ON A.PROPOSTA_ID  = PF.PROPOSTA_ID                       
   JOIN SEGUROS_DB.DBO.ENDERECO_CORRESP_TB      EC WITH(NOLOCK)             
     ON EC.PROPOSTA_ID = PP.PROPOSTA_ID                     
   LEFT JOIN SEGUROS_DB.DBO.AVALIACAO_RETORNO_BB_TB RET WITH(NOLOCK)             
     ON PP.PROPOSTA_ID = RET.PROPOSTA_ID            
  WHERE A.DT_IMPRESSAO IS NULL          
    AND EXISTS(SELECT 1           
                 FROM SEGUROS_DB.DBO.SEGA9164_PRODUTO_RAMO_TB SEGA9164           
                WHERE SEGA9164.PRODUTO_ID = PP.PRODUTO_ID           
                  AND SEGA9164.RAMO_ID = R.RAMO_ID)          
        --18233457 - MIGRA��O DOS PRODUTOS DGGR SEGA9164 ELEMENTOS M�NIMOS - ZORO.GOMES - CONFITEC - 14/03/2014          
--      AND R.RAMO_ID = 51            
--      AND PP.PRODUTO_ID IN(100,400,800)            
          
    --AND ((PP.PRODUTO_ID = 100  AND R.RAMO_ID IN (51, 67))            
    -- OR (PP.PRODUTO_ID  = 113  AND R.RAMO_ID IN (18))            
    -- OR (PP.PRODUTO_ID  = 117  AND R.RAMO_ID IN (96))            
    -- OR (PP.PRODUTO_ID  = 118  AND R.RAMO_ID IN (96))            
    -- OR (PP.PRODUTO_ID  = 120  AND R.RAMO_ID IN (18))          
    -- OR (PP.PRODUTO_ID  = 400  AND R.RAMO_ID IN (18, 51, 67, 73, 96))           
    -- OR (PP.PRODUTO_ID  = 670  AND R.RAMO_ID IN (67)))            
           
    AND (PF.IMPRESSAO_LIBERADA = 'S'             
     OR (PF.IMPRESSAO_LIBERADA = 'N' AND PD.APOLICE_ENVIA_CLIENTE = 'S'))             
    AND R.TP_RAMO_ID= 2                   
    AND A.DT_EMISSAO >= '20000701'                      
    AND PP.SITUACAO = 'I'          -- RETIRAR            
    AND A.DT_INCLUSAO >= GETDATE() - 60   --ZORO.GOMES - CONFITEC - 02/04/2014          
    AND (PP.ORIGEM_PROPOSTA_ID = 1 OR PP.ORIGEM_PROPOSTA_ID IS NULL)            
    AND ((PP.PRODUTO_ID >= 1000)             
     OR (PP.PRODUTO_ID < 1000 AND (RET.ACEITE_BB = 'S' OR RET.ACEITE_BB IS NULL)))       
    AND ISNULL(RET.ENDOSSO_ID,0) = 0 --CORRIGINDO DUPLICIDADES DE PROPOSTAS - INC000005317992 - 26/01/2017 - MUNIZ      
          
          
             
 --'AP�LICE 2A VIA                      
            
UNION ALL      
         
 SELECT DISTINCT TOP 1000       
       'A' AS  TPEMISSAO          
      , A.APOLICE_ID          
      , A.PROPOSTA_ID          
      , A.DT_INICIO_VIGENCIA          
      , A.DT_FIM_VIGENCIA          
      , 0 ENDOSSO_ID          
      , '19000101' AS DT_PEDIDO_ENDOSSO          
      , A.SEGURADORA_COD_SUSEP          
      , A.SUCURSAL_SEGURADORA_ID          
      , A.RAMO_ID          
      , PD.APOLICE_ENVIA_CLIENTE          
      , PD.APOLICE_NUM_VIAS          
      , PD.APOLICE_ENVIA_CONGENERE          
      , A.DT_EMISSAO          
      , PP.PRODUTO_ID          
      , 0 TP_ENDOSSO_ID          
      , A.NUM_PROC_SUSEP          
      , '' AS IMPRESSAO_LIBERADA      
      , SSV.DESTINO          
      , SSV.NUM_SOLICITACAO          
      , SSV.DIRETORIA_ID          
      , EC.CEP    
      , PP.FL_VERBA_UNICA   --   ARicardo 24/07/2017 - Verba �nica    
   FROM EVENTO_SEGUROS_DB.DBO.EVENTO_IMPRESSAO_TB      SSV WITH(NOLOCK)                      
  INNER JOIN SEGUROS_DB.DBO.APOLICE_TB                   A WITH(NOLOCK)             
     ON SSV.PROPOSTA_ID =  A.PROPOSTA_ID                       
  INNER JOIN SEGUROS_DB.DBO.RAMO_TB                      R WITH(NOLOCK)             
     ON A.RAMO_ID       =  R.RAMO_ID                       
  INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB                 PP WITH(NOLOCK)             
     ON A.PROPOSTA_ID   = PP.PROPOSTA_ID              
  INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB                  PD WITH(NOLOCK)             
     ON PP.PRODUTO_ID   = PD.PRODUTO_ID                       
  INNER JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB         PF WITH(NOLOCK)             
     ON A.PROPOSTA_ID   = PF.PROPOSTA_ID                       
  INNER JOIN SEGUROS_DB.DBO.ENDERECO_CORRESP_TB         EC WITH(NOLOCK)             
     ON EC.PROPOSTA_ID  = PP.PROPOSTA_ID                       
   LEFT JOIN SEGUROS_DB.DBO.AVALIACAO_RETORNO_BB_TB    RET WITH(NOLOCK)             
     ON PP.PROPOSTA_ID  = RET.PROPOSTA_ID            
  WHERE SSV.STATUS = 'L'          
    AND EXISTS(SELECT 1           
                 FROM SEGUROS_DB.DBO.SEGA9164_PRODUTO_RAMO_TB SEGA9164           
                WHERE SEGA9164.PRODUTO_ID = PP.PRODUTO_ID           
                  AND SEGA9164.RAMO_ID = R.RAMO_ID)            
        --18233457 - MIGRA��O DOS PRODUTOS DGGR SEGA9164 ELEMENTOS M�NIMOS - ZORO.GOMES - CONFITEC - 14/03/2014          
--      AND R.RAMO_ID = 51            
--      AND PP.PRODUTO_ID IN(100,400,800)            
          
        --AND   ((PP.PRODUTO_ID = 100 AND R.RAMO_ID IN (51, 67))            
        --    OR (PP.PRODUTO_ID = 113 AND R.RAMO_ID IN (18))            
        --    OR (PP.PRODUTO_ID = 117 AND R.RAMO_ID IN (96))            
        --    OR (PP.PRODUTO_ID = 118 AND R.RAMO_ID IN (96))            
        --    OR (PP.PRODUTO_ID = 120 AND R.RAMO_ID IN (18))            
        --    OR (PP.PRODUTO_ID = 400 AND R.RAMO_ID IN (18, 51, 67, 73, 96))            
        --    OR (PP.PRODUTO_ID = 670 AND R.RAMO_ID IN (67)))            
           
          
        AND SSV.DT_GERACAO_ARQUIVO IS NULL             
        AND SSV.TP_DOCUMENTO_ID IN (4)            
        AND NOT (A.RAMO_ID = 22 AND PP.PRODUTO_ID <> 400)   --'MARTINES- O PROGRAMA N�O DEVE GERAR 2� VIA PARA TRANSPORTE A PEDIDO DO SENHOR MARCIO YOSHIMURA                      
        AND (PP.ORIGEM_PROPOSTA_ID = 1 OR PP.ORIGEM_PROPOSTA_ID IS NULL) -- RESTRINGE-SE A CAPTURAR APENAS AS PROPOSTAS QUE TEM ORIGEM NO SEGBR                      
        AND  PP.SITUACAO <> 'T'                
        AND A.DT_INCLUSAO >= GETDATE() - 60   --ZORO.GOMES - CONFITEC - 02/04/2014          
        AND ((PP.PRODUTO_ID >= 1000) OR (PP.PRODUTO_ID < 1000 AND (RET.ACEITE_BB = 'S' OR RET.ACEITE_BB IS NULL)))            
        AND ISNULL(RET.ENDOSSO_ID,0) = 0 --CORRIGINDO DUPLICIDADES DE PROPOSTAS - INC000005317992 - 26/01/2017 - MUNIZ      
        
            
--'ENDOSSO 1A VIA              
            
  INSERT INTO ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB          
       ( TPEMISSAO            
       , APOLICE_ID                                          
       , PROPOSTA_ID                                         
       , DT_INICIO_VIGENCIA                  
       , DT_FIM_VIGENCIA                     
       , ENDOSSO_ID              
       , DT_PEDIDO_ENDOSSO             
       , SEGURADORA_COD_SUSEP                                
       , SUCURSAL_SEGURADORA_ID                              
       , RAMO_ID                 
       , APOLICE_ENVIA_CLIENTE             
       , APOLICE_NUM_VIAS             
       , APOLICE_ENVIA_CONGENERE             
       , DT_EMISSAO                          
       , PRODUTO_ID              
       , TP_ENDOSSO_ID             
       , NUM_PROC_SUSEP                        
       , IMPRESSAO_LIBERADA             
       , DESTINO                 
       , NUM_SOLICITACAO             
       , DIRETORIA_ID             
       , CEP    
       , FL_VERBA_UNICA    --   ARicardo 24/07/2017 - Verba �nica    
       )            
  SELECT DISTINCT TOP 1000       
        'E' AS TPEMISSAO          
       , A.APOLICE_ID          
       , A.PROPOSTA_ID          
       , A.DT_INICIO_VIGENCIA          
       , A.DT_FIM_VIGENCIA          
       , E.ENDOSSO_ID          
       , E.DT_PEDIDO_ENDOSSO          
       , A.SEGURADORA_COD_SUSEP          
       , A.SUCURSAL_SEGURADORA_ID          
       , A.RAMO_ID          
       , PD.APOLICE_ENVIA_CLIENTE          
       , PD.APOLICE_NUM_VIAS          
       , PD.APOLICE_ENVIA_CONGENERE          
       , E.DT_EMISSAO          
       , PP.PRODUTO_ID          
       , E.TP_ENDOSSO_ID          
       , A.NUM_PROC_SUSEP          
       , ''   AS IMPRESSAO_LIBERADA      
       , NULL AS DESTINO      
       , NULL AS NUM_SOLICITACAO      
       , NULL AS DIRETORIA_ID          
       , EC.CEP    
       , PP.FL_VERBA_UNICA    --   ARicardo 24/07/2017 - Verba �nica    
    FROM SEGUROS_DB.DBO.APOLICE_TB                A WITH(NOLOCK)                       
   INNER JOIN SEGUROS_DB.DBO.ENDOSSO_TB                E WITH(NOLOCK)             
      ON (A.PROPOSTA_ID = E.PROPOSTA_ID)             
     AND E.TP_ENDOSSO_ID NOT IN (63,90,91,100,101)                       
   INNER JOIN SEGUROS_DB.DBO.RAMO_TB                   R WITH(NOLOCK)             
      ON A.RAMO_ID      = R.RAMO_ID                       
   INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB              PP WITH(NOLOCK)             
      ON E.PROPOSTA_ID  = PP.PROPOSTA_ID                      
   INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB               PD WITH(NOLOCK)             
      ON PP.PRODUTO_ID  = PD.PRODUTO_ID                      
   INNER JOIN SEGUROS_DB.DBO.ENDERECO_CORRESP_TB      EC WITH(NOLOCK)             
      ON EC.PROPOSTA_ID = PP.PROPOSTA_ID--01/12/2005 ANA PAULA - STEFANINI (ORDENA��O POR CEP)                      
    LEFT JOIN SEGUROS_DB.DBO.AVALIACAO_RETORNO_BB_TB RET WITH(NOLOCK)             
      ON PP.PROPOSTA_ID = RET.PROPOSTA_ID            
     AND ISNULL(E.ENDOSSO_ID,0) =  ISNULL(RET.ENDOSSO_ID,0) --  INC000004233563 - A PROC ESTAVA RETORNANDO DOIS REGISTROS CONTRATA��O E ENDOSSO O QUE CAUSAVA O ERRO.             
   WHERE E.DT_IMPRESSAO IS NULL           
     AND R.TP_RAMO_ID= 2            
     AND EXISTS(SELECT 1           
                  FROM SEGUROS_DB.DBO.SEGA9164_PRODUTO_RAMO_TB SEGA9164           
       WHERE SEGA9164.PRODUTO_ID = PP.PRODUTO_ID           
                   AND SEGA9164.RAMO_ID = R.RAMO_ID)          
        --18233457 - MIGRA��O DOS PRODUTOS DGGR SEGA9164 ELEMENTOS M�NIMOS - ZORO.GOMES - CONFITEC - 14/03/2014          
----      AND R.RAMO_ID = 51            
----      AND PP.PRODUTO_ID IN(100,400,800)            
--        AND   ((PP.PRODUTO_ID = 100 AND R.RAMO_ID IN (51, 67))            
--            OR (PP.PRODUTO_ID = 113 AND R.RAMO_ID IN (18))            
--            OR (PP.PRODUTO_ID = 117 AND R.RAMO_ID IN (96))            
--            OR (PP.PRODUTO_ID = 118 AND R.RAMO_ID IN (96))            
--            OR (PP.PRODUTO_ID = 120 AND R.RAMO_ID IN (18))            
--            OR (PP.PRODUTO_ID = 400 AND R.RAMO_ID IN (18, 51, 67, 73, 96))            
--            OR (PP.PRODUTO_ID = 670 AND R.RAMO_ID IN (67)))            
           
          
        AND E.DT_EMISSAO > '20000701'             
        AND PP.SITUACAO NOT IN('C', 'T')             
        AND E.DT_INCLUSAO >= GETDATE() - 60   --ZORO.GOMES - CONFITEC - 02/04/2014  - INC000005270486 - DEVE SER VERIFICANDO A DATA DE INCLUS�O DO ENDOSSO PARA A 1� E 2� VIA DO ENDOSSO.        
        AND (PP.ORIGEM_PROPOSTA_ID = 1 OR PP.ORIGEM_PROPOSTA_ID IS NULL) -- RESTRINGE-SE A CAPTURAR APENAS AS PROPOSTAS QUE TEM ORIGEM NO SEGBR                      
        AND ((PP.PRODUTO_ID >= 1000) OR (PP.PRODUTO_ID < 1000 AND (RET.ACEITE_BB = 'S' OR RET.ACEITE_BB IS NULL)))            
  AND NOT EXISTS (SELECT 1 FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB WHERE PP.PROPOSTA_ID = RET.PROPOSTA_ID)  --VERIFICA SE EXISTE PROPOSTA DE ADES�O. CASO VERDADEIRO FAZ A ADES�O PRIMEIRO E DEPOIS O ENDOSO. EVITA DUPLICIDADES DE PROPOSTAS - INC000005317992 - 26/01/2017 - MUNIZ      
--'ENDOSSO 2A VIA            
  UNION ALL           
        
  SELECT DISTINCT TOP 1000       
        'E' AS TPEMISSAO      
       , A.APOLICE_ID          
       , A.PROPOSTA_ID          
       , A.DT_INICIO_VIGENCIA          
       , A.DT_FIM_VIGENCIA          
       , ISNULL(E.ENDOSSO_ID,0) AS   ENDOSSO_ID      
       , ISNULL(E.DT_PEDIDO_ENDOSSO,'')    AS DT_PEDIDO_ENDOSSO      
       , A.SEGURADORA_COD_SUSEP          
       , A.SUCURSAL_SEGURADORA_ID          
       , A.RAMO_ID          
       , PD.APOLICE_ENVIA_CLIENTE          
       , PD.APOLICE_NUM_VIAS          
       , PD.APOLICE_ENVIA_CONGENERE          
       , ISNULL(E.DT_EMISSAO,'')    AS DT_EMISSAO      
       , PP.PRODUTO_ID          
       , ISNULL(E.TP_ENDOSSO_ID,0)    AS TP_ENDOSSO_ID      
       , ISNULL(A.NUM_PROC_SUSEP,0)    AS NUM_PROC_SUSEP      
       , '' AS IMPRESSAO_LIBERADA      
       , SSV.DESTINO          
       , SSV.NUM_SOLICITACAO          
       , SSV.DIRETORIA_ID          
  , EC.CEP    
       , PP.FL_VERBA_UNICA    --   ARicardo 24/07/2017 - Verba �nica    
    FROM EVENTO_SEGUROS_DB.DBO.EVENTO_IMPRESSAO_TB SSV      WITH(NOLOCK)                      
   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB                   A WITH(NOLOCK)             
      ON SSV.PROPOSTA_ID = A.PROPOSTA_ID                       
    LEFT JOIN SEGUROS_DB.DBO.ENDOSSO_TB                   E WITH(NOLOCK)             
      ON SSV.PROPOSTA_ID = E.PROPOSTA_ID             
     AND E.TP_ENDOSSO_ID NOT IN (63,90,91,100,101)             
     AND E.ENDOSSO_ID = SSV.ENDOSSO_ID                       
   INNER JOIN SEGUROS_DB.DBO.RAMO_TB                      R WITH(NOLOCK)             
      ON A.RAMO_ID = R.RAMO_ID                       
   INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB                 PP WITH(NOLOCK)             
      ON SSV.PROPOSTA_ID = PP.PROPOSTA_ID                       
   INNER JOIN SEGUROS_DB.DBO.PRODUTO_TB                  PD WITH(NOLOCK)             
      ON PP.PRODUTO_ID = PD.PRODUTO_ID                       
   INNER JOIN SEGUROS_DB.DBO.ENDERECO_CORRESP_TB         EC WITH(NOLOCK)             
      ON EC.PROPOSTA_ID = PP.PROPOSTA_ID           
    LEFT JOIN SEGUROS_DB.DBO.AVALIACAO_RETORNO_BB_TB    RET WITH(NOLOCK)             
      ON PP.PROPOSTA_ID = RET.PROPOSTA_ID            
   WHERE SSV.STATUS = 'L'            
     AND SSV.DT_GERACAO_ARQUIVO IS NULL                 
     AND EXISTS(SELECT 1           
                  FROM SEGUROS_DB.DBO.SEGA9164_PRODUTO_RAMO_TB SEGA9164           
                 WHERE SEGA9164.PRODUTO_ID = PP.PRODUTO_ID           
                   AND SEGA9164.RAMO_ID = R.RAMO_ID)          
        --18233457 - MIGRA��O DOS PRODUTOS DGGR SEGA9164 ELEMENTOS M�NIMOS - ZORO.GOMES - CONFITEC - 14/03/2014          
--      AND R.RAMO_ID = 51            
--      AND PP.PRODUTO_ID IN(100,400,800)            
        --AND   ((PP.PRODUTO_ID = 100 AND R.RAMO_ID IN (51, 67))            
        --    OR (PP.PRODUTO_ID = 113 AND R.RAMO_ID IN (18))            
        --    OR (PP.PRODUTO_ID = 117 AND R.RAMO_ID IN (96))            
        --    OR (PP.PRODUTO_ID = 118 AND R.RAMO_ID IN (96))            
        --    OR (PP.PRODUTO_ID = 120 AND R.RAMO_ID IN (18))            
        --    OR (PP.PRODUTO_ID = 400 AND R.RAMO_ID IN (18, 51, 67, 73, 96))            
        --    OR (PP.PRODUTO_ID = 670 AND R.RAMO_ID IN (67)))            
          
        AND SSV.TP_DOCUMENTO_ID IN (6)                      
        AND (PP.ORIGEM_PROPOSTA_ID = 1 OR PP.ORIGEM_PROPOSTA_ID IS NULL) -- RESTRINGE-SE A CAPTURAR APENAS AS PROPOSTAS QUE TEM ORIGEM NO SEGBR                      
        AND PP.SITUACAO <> 'T'                
        AND E.DT_INCLUSAO >= GETDATE() - 60   --ZORO.GOMES - CONFITEC - 02/04/2014  - INC000005270486 - DEVE SER VERIFICANDO A DATA DE INCLUS�O DO ENDOSSO PARA A 1� E 2� VIA DO ENDOSSO.        
        AND ((PP.PRODUTO_ID >= 1000) OR (PP.PRODUTO_ID < 1000 AND (RET.ACEITE_BB = 'S' OR RET.ACEITE_BB IS NULL)))            
       AND NOT EXISTS (SELECT 1 FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB WHERE PP.PROPOSTA_ID = RET.PROPOSTA_ID)  --VERIFICA SE EXISTE PROPOSTA DE ADES�O. CASO VERDADEIRO FAZ A ADES�O PRIMEIRO E DEPOIS O ENDOSO. EVITA DUPLICIDADES DE PROPOSTAS - INC000005317992 - 26/01/2017 - MUNIZ      
                  
                  
                  
-- CRISTOVAO.RODRIGUES 22/09/2016 - 19368999 - COBRAN�A REGISTRADA AB E ABS      
--SELECT TOP 20 * FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_9201_TB      
      
DELETE RETORNO_MQ_TB      
-- SELECT PROPOSTA_ADESAO_TB.FORMA_PGTO_ID, AGENDAMENTO_COBRANCA_TB.FL_BOLETO_REGISTRADO, *      
FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB RETORNO_MQ_TB WITH(NOLOCK)      
INNER JOIN SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_TB AGENDAMENTO_COBRANCA_TB WITH(NOLOCK)      
 ON  AGENDAMENTO_COBRANCA_TB.PROPOSTA_ID  = RETORNO_MQ_TB.PROPOSTA_ID      
 AND AGENDAMENTO_COBRANCA_TB.NUM_ENDOSSO  = ISNULL(RETORNO_MQ_TB.ENDOSSO_ID, 0)        
INNER JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB PROPOSTA_FECHADA_TB WITH(NOLOCK)      
 ON PROPOSTA_FECHADA_TB.PROPOSTA_ID = RETORNO_MQ_TB.PROPOSTA_ID      
WHERE 1 = 1      
 AND PROPOSTA_FECHADA_TB.FORMA_PGTO_ID = 3      
 AND ISNULL(AGENDAMENTO_COBRANCA_TB.FL_BOLETO_REGISTRADO,'N') <> 'S'        
-- CRISTOVAO.RODRIGUES 22/09/2016 - 19368999 - COBRAN�A REGISTRADA AB E ABS      
                  
                  
-- INICIO DEMANDA 12005250 - NILTON CONFITEC            
    EXECUTE SEGUROS_DB.DBO.SEGS10192_SPI            
-- FIM DEMANDA 12005250 - NILTON CONFITEC            
            
    --------------------RPORTO 15/01/2014 : 18443273 -532-CIRCULAR 491-ELEMENTOS M�NIMOS-IMPRESS�O----------------------------------------------------------         
            
    UPDATE TEMP        
       SET VAL_LIMITE_MAX_GARANTIA = PF.VAL_LIMITE_MAX_GARANTIA        
         , CADEMP                  = PF.CADEMP        
         , VAL_FRANQUIA            = PF.VAL_FRANQUIA        
      FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB TEMP        
      JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB PF WITH(NOLOCK)        
        ON PF.PROPOSTA_ID = TEMP.PROPOSTA_ID         
            
    ---------------------------------------------------------------------------------------     
    
    
    -- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	set ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id   
		
        
 IF OBJECT_ID ('TEMPDB..##SEGS9164') IS NOT NULL        
 BEGIN        
  DROP TABLE ##SEGS9164        
 END        
       
 -- MARCIO.NOGUEIRA            
CREATE TABLE ##SEGS9164          
      ( TPEMISSAO               CHAR(1)       NULL      
      , APOLICE_ID              NUMERIC(9)    NULL        
      , PROPOSTA_ID             NUMERIC(9)    NULL        
      , DT_INICIO_VIGENCIA      SMALLDATETIME NULL           
      , DT_FIM_VIGENCIA         SMALLDATETIME NULL           
      , ENDOSSO_ID              INT           NULL       
      , DT_PEDIDO_ENDOSSO       SMALLDATETIME NULL           
      , SEGURADORA_COD_SUSEP    NUMERIC(5)    NULL        
      , SUCURSAL_SEGURADORA_ID  NUMERIC(5)    NULL        
      , RAMO_ID                 INT           NULL      
      , APOLICE_ENVIA_CLIENTE   CHAR(1)       NULL      
      , APOLICE_NUM_VIAS        CHAR(1)       NULL      
      , APOLICE_ENVIA_CONGENERE CHAR(1)       NULL      
      , DT_EMISSAO              SMALLDATETIME NULL           
      , PRODUTO_ID              INT           NULL      
      , TP_ENDOSSO_ID           INT           NULL      
      , NUM_PROC_SUSEP          VARCHAR(25)   NULL         
      , IMPRESSAO_LIBERADA      CHAR(1)       NULL       
      , DESTINO                 CHAR(1)       NULL       
      , NUM_SOLICITACAO         INT           NULL       
      , DIRETORIA_ID            TINYINT       NULL      
      , CEP                     VARCHAR(8)    NULL      
      , FLAG_EMITE              CHAR(1)       NULL       
      , VAL_LIMITE_MAX_GARANTIA NUMERIC(15,2) NULL      
      , CADEMP                  VARCHAR(10)   NULL      
      , VAL_FRANQUIA            VARCHAR(20)   NULL    
      , FL_VERBA_UNICA          VARCHAR(1)    NULL   --   ARicardo 24/07/2017 - Verba �nica      
      , CPF_CNPJ				VARCHAR(14)   NULL
      )      
            
-- MARCIO.NOGUEIRA           
INSERT INTO ##SEGS9164( TPEMISSAO                     
       , APOLICE_ID                    
       , PROPOSTA_ID                   
       , DT_INICIO_VIGENCIA            
       , DT_FIM_VIGENCIA               
       , ENDOSSO_ID                    
       , DT_PEDIDO_ENDOSSO             
       , SEGURADORA_COD_SUSEP          
       , SUCURSAL_SEGURADORA_ID        
       , RAMO_ID                       
       , APOLICE_ENVIA_CLIENTE         
       , APOLICE_NUM_VIAS              
       , APOLICE_ENVIA_CONGENERE       
       , DT_EMISSAO                    
       , PRODUTO_ID                    
       , TP_ENDOSSO_ID                 
       , NUM_PROC_SUSEP                
       , IMPRESSAO_LIBERADA            
       , DESTINO                       
       , NUM_SOLICITACAO               
       , DIRETORIA_ID                  
       , CEP      
       , FLAG_EMITE                           
       , VAL_LIMITE_MAX_GARANTIA       
       , CADEMP                        
       , VAL_FRANQUIA    
       , FL_VERBA_UNICA   --   ARicardo 24/07/2017 - Verba �nica    
       , CPF_CNPJ )
      
SELECT TPEMISSAO          
     , APOLICE_ID          
     , PROPOSTA_ID                                         
     , DT_INICIO_VIGENCIA          
     , DT_FIM_VIGENCIA                     
     , ENDOSSO_ID         
     , DT_PEDIDO_ENDOSSO             
     , SEGURADORA_COD_SUSEP           
     , SUCURSAL_SEGURADORA_ID                              
     , RAMO_ID           
     , APOLICE_ENVIA_CLIENTE             
     , APOLICE_NUM_VIAS           
     , APOLICE_ENVIA_CONGENERE             
     , DT_EMISSAO           
     , PRODUTO_ID              
     , TP_ENDOSSO_ID           
     , NUM_PROC_SUSEP                        
     , IMPRESSAO_LIBERADA           
     , DESTINO           
     , NUM_SOLICITACAO           
     , DIRETORIA_ID             
     , CEP          
     , 'S' FLAG_EMITE        
     --RPORTO 15/01/215        
     , VAL_LIMITE_MAX_GARANTIA          
     , CADEMP         
     , VAL_FRANQUIA    
     , ISNULL(FL_VERBA_UNICA,'N') AS FL_VERBA_UNICA  --   ARicardo 24/07/2017 - Verba �nica  - MARCIO.NOGUEIRA - 08/05/2018 - VERBA �NICA - INCIDENTE IM00331930  
     , CPF_CNPJ
  --INTO ##SEGS9164   - MARCIO.NOGUEIRA            
  FROM ##TEMP_AUX_PROCESSO_RETORNO_MQ_TB             
WHERE PROPOSTA_ID NOT IN(021301147,022643501)    
            
 --DEMANDA: 18775962 - REMOVER OS ENDOSSOS DO RAMO 21 E 22 COM MOVIMENTA��O DE PR�MIO DO SEGA9164 - GILSON SILVA 01/09/2015        
 DELETE SEGS9164        
 FROM ##SEGS9164 SEGS9164        
 WHERE (RAMO_ID=21 AND ENDOSSO_ID<>0 AND PRODUTO_ID IN(6,7,100,105,400,800))       
    OR (RAMO_ID=22 AND ENDOSSO_ID<>0 AND PRODUTO_ID IN(4,5,400))       
    
DELETE SEGS9164    
 -- SELECT *    
 FROM ##SEGS9164 SEGS9164  --MUNIZ - INSERINDO TRATAMENTO PARA QUE PROPOSTAS CADASTRADAS COM DOLAR E QUE EST�O OCASIONADO NA TAREFA N�O ENTREM NO PROCESSO     
 JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB B WITH(NOLOCK)    
   ON SEGS9164.PROPOSTA_ID = B.PROPOSTA_ID    
  AND SEGS9164.PRODUTO_ID = 400    
  AND SEGS9164.RAMO_ID IN (18,33,35)    
  AND B.SEGURO_MOEDA_ID = 220     
            
 RETURN          
            
 END TRY           
          
BEGIN CATCH            
DECLARE @ERRORMESSAGE NVARCHAR(4000)                  
DECLARE @ERRORSEVERITY INT                  
DECLARE @ERRORSTATE INT                
            SELECT                   
                        @ERRORMESSAGE = ERROR_PROCEDURE() + ' - LINHA ' + CONVERT(VARCHAR(15),ERROR_LINE()) + ' - ' + ERROR_MESSAGE(),                  
              @ERRORSEVERITY = ERROR_SEVERITY(),                  
                        @ERRORSTATE = ERROR_STATE()                  
            
            RAISERROR (          
                        @ERRORMESSAGE,             
                        @ERRORSEVERITY,                  
                        @ERRORSTATE )            
END CATCH           
   SET NOCOUNT OFF       
 END --MARCIO NOGUEIRA    
    
    
    
  