CREATE PROCEDURE SEGS6511_SPI  
  
@usuario    VARCHAR(20)  
  
AS  
  
SET NOCOUNT ON         
    
-- TESTES --------------------------------------    
-- DECLARE @usuario        VARCHAR(20)    
-- SET @usuario = 'producao3'         
------------------------------------------------    
    
DECLARE @msg              VARCHAR(100)       
    
-- DETALHE SEGURADO    
SELECT '20' TipoRegistro,    
       IDENTITY(INT, 1, 1) sequencial,        
       A.proposta_id NumeroPropostaAB,        
       A.segurado NomeSegurado,        
       E.endereco + ' ' + E.bairro EnderecoSegurado,        
       E.municipio MunicipioSegurado,        
       E.estado UFSegurado,        
       A.CepDestino CepSegurado,  
       A.NomeProduto NomeProduto,  
       A.MotivoRecusa,  
       A.NumeroPropostaBB NumeroPropostaBB,  
       A.CodigoAgencia CodigoAgencia,  
       A.DVCodAgencia DVCodAgencia,  
       A.NomeAgenciaCorretor NomeAgenciaCorretor,  
       A.num_proc_susep,    
       A.SEGA8135_processar_id,    
       A.layout_id,
       CAST('' AS VARCHAR(4)) as ddd_celular, --migracao-documentacao-digital-2a-fase-cartas
       CAST('' AS VARCHAR(9)) as celular, --migracao-documentacao-digital-2a-fase-cartas
       CAST('' AS VARCHAR(60)) as email, --migracao-documentacao-digital-2a-fase-cartas    
       A.produto_id, --migracao-documentacao-digital-2a-fase-cartas    
       null as ramo_id, --migracao-documentacao-digital-2a-fase-cartas    
       CAST('' AS VARCHAR(14)) as cpf_cnpj
  INTO #DETALHE_20_SEGA8135    
  FROM interface_dados_db..SEGA8135_processar_tb A (NOLOCK)  
  JOIN endereco_corresp_tb E (NOLOCK)    
    ON E.proposta_id = A.proposta_id  
 ORDER BY A.CepDestino,    
          A.produto_id,    
          A.proposta_id  
          
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 26/03/2020 Inicio
update #DETALHE_20_SEGA8135
set #DETALHE_20_SEGA8135.NomeProduto = ISNULL(plano_tb.nome_fantasia,'')
from #DETALHE_20_SEGA8135 #DETALHE_20_SEGA8135 
inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
on proposta_tb.proposta_id = #DETALHE_20_SEGA8135.NumeroPropostaAB
inner join seguros_db.dbo.escolha_plano_tb escolha_plano_tb with(nolock)
on escolha_plano_tb.proposta_id = proposta_tb.proposta_id
and escolha_plano_tb.produto_id = proposta_tb.produto_id
inner join seguros_db.dbo.plano_tb plano_tb with(nolock)
on  plano_tb.produto_id = escolha_plano_tb.produto_id
and plano_tb.plano_id = escolha_plano_tb.plano_id
where proposta_tb.produto_id = 1243
-- Thiago SIlva - Confitec - Projeto Novo Prestamista - 26/03/2020 Fim


  -- Atualizando o contatos do segurado --------------------------------------------  
  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
  UPDATE #DETALHE_20_SEGA8135
  SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,'')
  , email = isnull(cliente_tb.e_mail,''), ramo_id = proposta_tb.ramo_id    
  FROM #DETALHE_20_SEGA8135 (NOLOCK)  
  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
	ON #DETALHE_20_SEGA8135.NumeroPropostaAB = proposta_tb.proposta_id  
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
	ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id  
	
	
	
	-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #DETALHE_20_SEGA8135
	set #DETALHE_20_SEGA8135.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #DETALHE_20_SEGA8135 #DETALHE_20_SEGA8135
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #DETALHE_20_SEGA8135.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id             
    
    
/* DADOS DO SEGURADO */    
INSERT INTO interface_dados_db..SEGA8135_20_processar_tb(SEGA8135_processar_id,    
                                                         layout_id,    
                                                         NumeroPropostaAB,    
                                                         NomeSegurado,    
                                                         EnderecoSegurado,    
                                                         MunicipioSegurado,    
                                                         UFSegurado,    
                                                         CepSegurado,    
                                                         NomeProduto,    
                                                         Motivorecusa,    
                                                         NumeroPropostaBB,    
                                                         CodigoAgencia,    
                                                         DVCodAgencia,    
                                                         NomeAgenciaCorretor,    
                                                         num_proc_susep,    
                                                         sequencial,    
                                                         tipo_registro,    
                                                         dt_inclusao,    
                                                         usuario,
                                                         ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
														 celular,  --migracao-documentacao-digital-2a-fase-cartas
														 email, --migracao-documentacao-digital-2a-fase-cartas       
														 produto_id, --migracao-documentacao-digital-2a-fase-cartas       
														 ramo_id,  --migracao-documentacao-digital-2a-fase-cartas       
														 cpf_cnpj )
SELECT #DETALHE_20_SEGA8135.SEGA8135_processar_id,    
       layout_id = #DETALHE_20_SEGA8135.layout_id,    
       NumeroPropostaAB = #DETALHE_20_SEGA8135.NumeroPropostaAB,    
       NomeSegurado = #DETALHE_20_SEGA8135.NomeSegurado ,    
       EnderecoSegurado = CAST(#DETALHE_20_SEGA8135.EnderecoSegurado AS VARCHAR(85)),    
       MunicipioSegurado = #DETALHE_20_SEGA8135.MunicipioSegurado,    
       UFSegurado = #DETALHE_20_SEGA8135.UFSegurado,    
       CepSegurado = LEFT(#DETALHE_20_SEGA8135.CepSegurado, 05) + '-' + RIGHT(#DETALHE_20_SEGA8135.CepSegurado, 03),     
       NomeProduto = #DETALHE_20_SEGA8135.NomeProduto,    
       Motivorecusa = #DETALHE_20_SEGA8135.Motivorecusa,    
       NumeroPropostaBB = #DETALHE_20_SEGA8135.NumeroPropostaBB,    
       CodigoAgencia1 = #DETALHE_20_SEGA8135.CodigoAgencia,    
       DVCodAgencia = #DETALHE_20_SEGA8135.DVCodAgencia,    
       NomeAgenciaCorretor = #DETALHE_20_SEGA8135.NomeAgenciaCorretor,    
       num_proc_susep = #DETALHE_20_SEGA8135.num_proc_susep,    
       sequencial = #DETALHE_20_SEGA8135.sequencial,    
       tipo_registro = #DETALHE_20_SEGA8135.tiporegistro,    
       dt_inclusao = GETDATE(),    
       usuario = @usuario,
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
	   celular,  --migracao-documentacao-digital-2a-fase-cartas
	   email,  --migracao-documentacao-digital-2a-fase-cartas 
	   produto_id, --migracao-documentacao-digital-2a-fase-cartas       
       ramo_id,   --migracao-documentacao-digital-2a-fase-cartas                   
       cpf_cnpj
FROM #DETALHE_20_SEGA8135 (NOLOCK)  
ORDER BY sequencial  
    
      
IF @@ERROR <> 0      
 BEGIN                  
    SELECT @msg = 'Erro na inclus�o em SEGA8135_20_processar_tb'      
     GOTO error                        
  END    
    
SET NOCOUNT OFF        
  
RETURN  
  
error:  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:40 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:40 - Fim --  
  
  
  
  
  
  
  
  

