CREATE PROCEDURE SEGS6516_SPI  
  
@usuario     VARCHAR(20),  
@num_remessa INT  
  
AS  
  
SET NOCOUNT ON  
  
DECLARE @msg  VARCHAR(100)  
      
INSERT INTO interface_dados_db..SEGA8135_20_processado_tb (sega8135_processar_id,  
                                                           tipo_registro,  
                                                           sequencial,  
                                                           NumeroPropostaAB,  
                                                           NomeSegurado,    
                                                           EnderecoSegurado,    
                                                           MunicipioSegurado,    
                                                           UFSegurado,    
                                                           CepSegurado,    
                                                           NomeProduto,    
                                                           Motivorecusa,    
                                                           NumeroPropostaBB,    
                                                           CodigoAgencia,    
                                                           DVCodAgencia,    
                                                           NomeAgenciaCorretor,    
                                                           num_proc_susep,    
                                                           layout_id,    
                                                           num_remessa,    
                                                           dt_inclusao,    
                                                           usuario,
                                                           ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
														   celular,  --migracao-documentacao-digital-2a-fase-cartas
														   email, --migracao-documentacao-digital-2a-fase-cartas
														   produto_id, --migracao-documentacao-digital-2a-fase-cartas
														   ramo_id,  --migracao-documentacao-digital-2a-fase-cartas      
														   cpf_cnpj )
SELECT sega8135_processar_id,    
       tipo_registro,    
       sequencial,    
       NumeroPropostaAB,    
       NomeSegurado,    
       EnderecoSegurado,    
       MunicipioSegurado,    
       UFSegurado,    
       CepSegurado,    
       NomeProduto,    
       Motivorecusa,    
       NumeroPropostaBB,    
       CodigoAgencia,    
       DVCodAgencia,    
       NomeAgenciaCorretor,    
       num_proc_susep,    
       layout_id,    
       @num_remessa,    
       GETDATE(),    
       @usuario,
       ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
	   celular,  --migracao-documentacao-digital-2a-fase-cartas
	   email, --migracao-documentacao-digital-2a-fase-cartas       
	   produto_id, --migracao-documentacao-digital-2a-fase-cartas       
	   ramo_id,  --migracao-documentacao-digital-2a-fase-cartas       
	   cpf_cnpj 
FROM interface_dados_db..sega8135_20_processar_tb    
      
IF @@ERROR <> 0      
 BEGIN                        
    SELECT @msg = 'Erro na inclus�o em SEGA8135_20_processado_tb'      
     GOTO error                        
  END      
      
SET NOCOUNT OFF      
  
RETURN  
  
error:  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:40 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec � sql2012 � 07/08/2015 16:11:40 - Fim --  
  
  
  
  
  

