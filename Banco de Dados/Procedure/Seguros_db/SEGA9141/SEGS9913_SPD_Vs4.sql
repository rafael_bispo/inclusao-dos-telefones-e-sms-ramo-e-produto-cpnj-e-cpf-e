CREATE PROCEDURE SEGS9913_SPD
        
    @usuario VARCHAR(20),        
    @num_versao INT        
        
AS  
      
        
SET NOCOUNT ON        
 /*
begin tran 
 exec SEGS9913_SPD 'teste', 1
rollback
*/         
DECLARE @msg VARCHAR(100)        
    
--Copiando dados para SEGA9141_processado_tb      
INSERT INTO interface_dados_db.dbo.SEGA9141_processado_tb    
      (SEGA9141_processar_id,    
       proposta_id,    
       proposta_bb,    
       apolice_id,    
       ramo_id,    
       produto_id,    
       seguradora_cod_susep,    
       sucursal_seguradora_id,    
       tp_documento_id,    
       tipo_documento,    
       endosso_id,    
       layout_id,    
       num_solicitacao,    
       dt_inclusao,    
       usuario,       
       num_versao)        
SELECT SEGA9141_processar_id,    
       proposta_id,    
       proposta_bb,    
       apolice_id,    
       ramo_id,    
       produto_id,    
       seguradora_cod_susep,    
       sucursal_seguradora_id,    
       tp_documento_id,    
       tipo_documento,    
       endosso_id,    
       layout_id,     
       num_solicitacao,    
       GETDATE() as dt_inclusao,    
       @usuario as usuario ,    
       @num_versao as num_versao    
  FROM interface_dados_db.dbo.SEGA9141_processar_tb         
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_processado_tb'        
    GOTO error        
END        
        
--Copiando dados para SEGA9141_10_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_10_processado_tb        
      (SEGA9141_10_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_destinatario,    
       endereco_destinatario,     
       municipio_destinatario,    
       UF_destinatario,    
       CEP_destinatario,    
       cod_retorno,    
       tipo_documento,      
       descricao_documento,    
       dt_inclusao,  
    --guilhermecruz - 18015684     
      gruporamo_ramoid,  
    --guilhermecruz - 18015684    
       usuario)        
SELECT SEGA9141_10_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_destinatario,    
       endereco_destinatario,     
       municipio_destinatario,    
       UF_destinatario,    
       CEP_destinatario,    
       cod_retorno,    
       tipo_documento,      
       descricao_documento,    
       GETDATE() dt_inclusao,  
    --guilhermecruz - 18015684     
      gruporamo_ramoid,  
    --guilhermecruz - 18015684     
       @usuario as usuario    
 FROM interface_dados_db.dbo.SEGA9141_10_processar_tb     
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_10_processado_tb'        
    GOTO error        
END        
        
--Copiando dados para SEGA9141_20_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_20_processado_tb        
      (SEGA9141_20_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       proposta_bb ,    
       apolice_id ,    
       endosso_id,          
       dt_inicio_vigencia,    
       dt_fim_vigencia,              
       cod_agencia,                  
       nome_agencia,                 
       nome_estipulante,             
       cnpj_estipulante,             
       nome_corretor,                
       cod_susep_corretor,
       dt_inclusao,  
       usuario)          
SELECT SEGA9141_20_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       proposta_bb ,    
       apolice_id ,    
       endosso_id,          
       dt_inicio_vigencia,    
       dt_fim_vigencia,              
       cod_agencia,                  
       nome_agencia,                 
       nome_estipulante,             
       cnpj_estipulante,             
       nome_corretor,                
       cod_susep_corretor,  
       GETDATE() as dt_inclusao,   
       @usuario as usuario       
  FROM interface_dados_db.dbo.SEGA9141_20_processar_tb   
  
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_20_processado_tb'        
    GOTO error        
END        
        
--Copiando dados para SEGA9141_30_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_30_processado_tb        
      (SEGA9141_30_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       qtd_vidas,    
       dt_inclusao,    
       usuario)   
SELECT SEGA9141_30_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       qtd_vidas,       
       GETDATE() as dt_inclusao,        
       @usuario as usuario      
  FROM interface_dados_db.dbo.SEGA9141_30_processar_tb      
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_30_processado_tb'        
    GOTO error        
END        
      
      
--Copiando dados para SEGA9141_31_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_31_processado_tb       
      (SEGA9141_31_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       qtd_vidas,    
       dt_inclusao,    
       usuario)        
SELECT SEGA9141_31_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       qtd_vidas,       
       GETDATE() as dt_inclusao,        
       @usuario as usuario      
  FROM interface_dados_db.dbo.SEGA9141_31_processar_tb          
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_31_processado_tb'        
    GOTO error        
END      
      
--Copiando dados para SEGA9141_40_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_40_processado_tb        
      (SEGA9141_40_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_cobertura,           
       val_capital_segurado,   
    --guilhermecruz - 18015684     
       val_premio,  
    --guilhermecruz - 18015684     
       dt_inclusao,      
       usuario)         
SELECT SEGA9141_40_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_cobertura,    
       val_capital_segurado,  
    --guilhermecruz - 18015684     
       val_premio,  
    --guilhermecruz - 18015684     
       GETDATE() as dt_inclusao,    
       @usuario as usuario       
  FROM interface_dados_db.dbo.SEGA9141_40_processar_tb    
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_40_processado_tb'        
    GOTO error        
END       
      
--Copiando dados para SEGA9141_41_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_41_processado_tb        
      (SEGA9141_41_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_cobertura,           
       val_capital_segurado,  
    --guilhermecruz - 18015684     
       val_premio,  
    --guilhermecruz - 18015684     
       dt_inclusao,      
       usuario)         
SELECT SEGA9141_41_processar_id,  
       SEGA9141_processar_id,    
       proposta_id,    
       endosso_id,    
       nome_cobertura,    
       val_capital_segurado,    
     --guilhermecruz - 18015684     
       val_premio,  
    --guilhermecruz - 18015684     
       GETDATE() as dt_inclusao,    
       @usuario as usuario       
  FROM interface_dados_db.dbo.SEGA9141_41_processar_tb      
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_41_processado_tb'        
    GOTO error        
END        
  
--Copiando dados para SEGA9141_50_processado_tb        
INSERT INTO interface_dados_db.dbo.SEGA9141_50_processado_tb        
      (  
       SEGA9141_processar_id,    
       proposta_id,    
    --guilhermecruz - 18015684     
       val_premio_tarifa,  
       val_iof,  
       val_premio_bruto,  
       forma_pgto_id,  
 --guilhermecruz - 18015684 
	   endosso_id ,--msalema - 23/07/2014 -18015684
       dt_inclusao,      
       usuario)         
SELECT   
       SEGA9141_processar_id,    
       proposta_id,  
    --guilhermecruz - 18015684     
       val_premio_tarifa,  
       val_iof,  
       val_premio_bruto,  
       forma_pgto_id,  
 --guilhermecruz - 18015684     
 	   endosso_id ,--msalema - 23/07/2014 -18015684
       GETDATE() as dt_inclusao,    
       @usuario as usuario       
  FROM interface_dados_db.dbo.SEGA9141_50_processar_tb    
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro na inclus�o em SEGA9141_50_processado_tb'        
    GOTO error        
END        

	/* MUDAN�A PARA INCLUIR ENDERE�O ESTIPULANTE
   Flow 18334512 - Circular 491 04/12/2014 Edu Bello
  
   DETALHE 80 */

SELECT @msg = 'Erro na inclus�o em SEGA9141_80_processado_tb'     

 INSERT INTO interface_dados_db.dbo.SEGA9141_80_processado_tb (
	    SEGA9141_processar_id
	  , SEGA9141_80_processar_id
	  , proposta_id
	  , endereco_estipulante 
	  , bairro_estipulante 
	  , municipio_estipulante
	  , uf_estipulante 
	  , cep_estipulante 
	  , ddd_estipulante 
	  , tel_estipulante 
	  , cpf_cnpj
	  , usuario 
	  , dt_inclusao 
	  , dt_alteracao)
 SELECT 
	    SEGA9141_processar_id 
	  , SEGA9141_80_processar_id
	  , proposta_id 
	  , endereco_estipulante 
	  , bairro_estipulante
	  , municipio_estipulante 
	  , uf_estipulante
	  , cep_estipulante
	  , ddd_estipulante
	  , tel_estipulante 
	  , cpf_cnpj
	  , @usuario 
	  , getdate() 
	  , dt_alteracao 
  FROM interface_dados_db.dbo.sega9141_80_processar_tb      
          
-- Excluindo os registros das tabelas processar        
DELETE FROM interface_dados_db.dbo.SEGA9141_10_processar_tb     
DELETE FROM interface_dados_db.dbo.SEGA9141_20_processar_tb       
DELETE FROM interface_dados_db.dbo.SEGA9141_30_processar_tb        
DELETE FROM interface_dados_db.dbo.SEGA9141_31_processar_tb        
DELETE FROM interface_dados_db.dbo.SEGA9141_40_processar_tb        
DELETE FROM interface_dados_db.dbo.SEGA9141_41_processar_tb  
DELETE FROM interface_dados_db.dbo.SEGA9141_50_processar_tb --guilhermecruz - 18015684
DELETE FROM interface_dados_db.dbo.SEGA9141_80_processar_tb -- Flow 18334512 - 04/12/2014 Edu Bello    
DELETE FROM interface_dados_db.dbo.SEGA9141_processar_tb        
        
IF @@ERROR <> 0        
BEGIN        
    SELECT @msg = 'Erro excluindo os registros das tabelas processar'        
    GOTO error        
END        
        
SET NOCOUNT OFF        
        
RETURN        
        
error:            

   -- Confitec � sql2012 � 07/08/2015 16:11:48 - Inicio --
    --raiserror 55555 @msg        
    Declare @msgRaiserror nvarchar(1000)
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg) 

    RAISERROR (@msgRaiserror, 16, 1)

   -- Confitec � sql2012 � 07/08/2015 16:11:48 - Fim --






