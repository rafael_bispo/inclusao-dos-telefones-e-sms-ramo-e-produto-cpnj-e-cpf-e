CREATE PROCEDURE segs9903_sps 
 @layout_id INT  
 ,@produto_id INT  
 ,@usuario VARCHAR(20)  
 ,@producao CHAR(1) = 'n'  
AS  
-- declare @layout_id int      
-- declare @produto_id int      
-- declare @usuario varchar(20)      
-- declare @producao char(1)      
-- select @layout_id = 1505      
-- select @usuario = 'producao3'      
-- select @producao = 'n'    
/*  
begin tran  
exec segs9903_sps 15015,null,'teste','n'  
rollback  
*/  
SET NOCOUNT ON  
SET ANSI_WARNINGS OFF  
  
DECLARE @dt_sistema SMALLDATETIME  
  
SELECT @dt_sistema = dt_operacional  
FROM parametro_geral_tb(NOLOCK)  
  
DECLARE @msg VARCHAR(1000)  
  
BEGIN TRY  
 -- inicio cria��o de tabela para valida��o de valores de cobertura flow 18334512 rogerio melo   
  
 --Altera��o para evitar que os valores das coberturas n�o sejam preenchidas - 26/01/2018 - Alexandre Debouch - Confitec  
 --Essas duas temporarias eram criadas na procedure SEGS9904_SPI e isso mata a instancia da procedure aqui. Assim elas ficam  
 --em branco  
   
 CREATE TABLE #max_nr_vrs_eds (  
  nr_vrs_eds INT  
  ,nosso_numero NUMERIC  
  ,proposta_id INT  
  ,nr_ctr_sgro INT  
  ,cd_tip_eds INT  
  ,cd_prd INT  
  ,cd_mdld INT  
  ,cd_item_mdld INT  
  ,endosso_id INT  
  ,endosso_id_proposta INT  
  )  
  
 CREATE TABLE #max_nr_vrs_eds2 (  
  nr_vrs_eds INT  
  ,nosso_numero NUMERIC  
  ,proposta_id INT  
  ,nr_ctr_sgro INT  
  ,cd_tip_eds INT  
  ,cd_prd INT  
  ,cd_mdld INT  
  ,cd_item_mdld INT  
  ,endosso_id INT  
  ,endosso_id_proposta INT  
  )  
  
 --Altera��o para evitar que os valores das coberturas n�o sejam preenchidas - 26/01/2018 - Alexandre Debouch - Confitec  
 --Essas duas temporarias eram criadas na procedure SEGS9904_SPI e isso mata a instancia da procedure aqui. Assim elas ficam  
 --em branco  
  
  
 --fim  
 -- processar  
 SELECT @msg = 'erro na carga de segs9904_spi'  
  
 EXEC segs9904_spi @layout_id  
  ,@produto_id  
  ,@usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9904_spi'      
 --    goto error      
 --end      
 -- detalhe 10   
 SELECT @msg = 'erro na carga de segs9905_spi'  
  
 EXEC segs9905_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9905_spi'      
 --    goto error      
 --end      
 -- detalhe 20   
 SELECT @msg = 'erro na carga de segs9906_spi'  
  
 EXEC segs9906_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9906_spi'      
 --    goto error      
 --end      
 -- detalhe 30   
 SELECT @msg = 'erro na carga de segs9907_spi'  
  
 EXEC segs9907_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9907_spi'      
 --    goto error      
 --end      
 -- detalhe 31     
 SELECT @msg = 'erro na carga de segs9908_spi'  
  
 EXEC segs9908_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9908_spi'      
 --    goto error      
 --end      
 -- detalhe 40   
 SELECT @msg = 'erro na carga de segs9909_spi'  
  
 EXEC segs9909_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9909_spi'      
 --    goto error      
 --end      
 -- detalhe 41   
 SELECT @msg = 'erro na carga de segs9910_spi'  
  
 EXEC segs9910_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs9910_spi'      
 --    goto error      
 --end     
 --detalhe 50   
 -- guilhermecruz  - confitec - flowbr18015684  
 SELECT @msg = 'erro na carga de segs11724_spi'  
  
 EXEC segs11724_spi @usuario  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga de segs11724_spi'      
 --    goto error      
 --end      
 -- guilhermecruz  - confitec - flowbr18015684    
 /* mudan�a para incluir endere�o estipulante  
   flow 18334512 - circular 491 20141204 confitec edu bello  
   detalhe 80 */  
 SELECT @msg = 'erro na carga na interface_dados_db.dbo.sega9141_80_processar_tb'  
  
 INSERT INTO interface_dados_db.dbo.sega9141_80_processar_tb (  
  sega9141_processar_id  
  ,proposta_id  
  ,endereco_estipulante  
  ,bairro_estipulante  
  ,municipio_estipulante  
  ,uf_estipulante  
  ,cep_estipulante  
  ,ddd_estipulante  
  ,tel_estipulante  
  ,dt_inclusao  
  ,usuario  
  )  
 SELECT g.sega9141_processar_id  
  ,a.proposta_id  
  ,e.endereco  
  ,e.bairro  
  ,e.municipio  
  ,e.estado  
  ,substring(e.cep, 1, 5) + '-' + substring(e.cep, 6, 3)  
  ,isnull(e.ddd, '')  
  ,isnull(e.telefone, '')  
  ,getdate()  
  ,@usuario  
 FROM seguros_db.dbo.proposta_fechada_tb a WITH (NOLOCK)  
 JOIN seguros_db.dbo.proposta_tb c WITH (NOLOCK) ON c.proposta_id = a.proposta_id  
 JOIN seguros_db.dbo.cliente_tb d WITH (NOLOCK) ON c.prop_cliente_id = d.cliente_id  
 JOIN seguros_db.dbo.endereco_corresp_tb e WITH (NOLOCK) ON e.proposta_id = c.proposta_id  
 JOIN interface_dados_db.dbo.sega9141_processar_tb g WITH (NOLOCK) ON a.proposta_id = g.proposta_id  
  
 --if @@error <> 0      
 --begin      
 --    select @msg = 'erro na carga na interface_dados_db.dbo.sega9141_80_processar_tb'      
 --    goto error      
 --end  
 
 
 
-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update sega9141_80_processar_tb
	set sega9141_80_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from interface_dados_db.dbo.sega9141_80_processar_tb sega9141_80_processar_tb with(nolock)
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = sega9141_80_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id
 
 
     
 IF @producao = 's'  
 BEGIN  
  -- atualiza apolice   
  SELECT @msg = 'erro na atualiza��o da ap�lice'  
  
  UPDATE a  
  SET a.dt_impressao = @dt_sistema  
   ,a.dt_alteracao = getdate()  
   ,a.usuario = @usuario  
  FROM apolice_tb a  
  JOIN interface_dados_db.dbo.sega9141_processar_tb sega9141 ON sega9141.sucursal_seguradora_id = a.sucursal_seguradora_id  
   AND sega9141.apolice_id = a.apolice_id  
   AND sega9141.seguradora_cod_susep = a.seguradora_cod_susep  
   AND sega9141.ramo_id = a.ramo_id  
   AND sega9141.endosso_id = 0  
  
  --if @@error <> 0      
  --begin      
  -- select @msg = 'erro na atualiza��o da ap�lice'      
  -- goto error      
  --end      
  -- atualiza endosso    
  SELECT @msg = 'erro na atualiza��o do endosso'  
  
  UPDATE e  
  SET e.dt_impressao = @dt_sistema  
   ,e.dt_alteracao = getdate()  
   ,e.usuario = @usuario  
  FROM endosso_tb e  
  JOIN interface_dados_db.dbo.sega9141_processar_tb sega9141 ON sega9141.proposta_id = e.proposta_id  
   AND sega9141.endosso_id = e.endosso_id  
   AND sega9141.endosso_id > 0  
   --if @@error <> 0      
   --begin      
   -- select @msg = 'erro na atualiza��o do endosso'      
   -- goto error      
   --end    
 END  
  
 --retorna o n�mero de registros gerados       
 SELECT count(1)  
 FROM interface_dados_db.dbo.sega9141_processar_tb(NOLOCK)  
  
 SET NOCOUNT OFF  
  --return      
  --error:      
  --    raiserror 55555 @msg      
END TRY  
  
BEGIN CATCH  
 SET @msg = isnull(error_procedure(), '') + ' - linha ' + convert(VARCHAR(15), isnull(error_line(), 0)) + ' - ' + isnull(error_message(), '') + ' - atividade: ' + isnull(@msg, '');  
  
 RAISERROR (  
   @msg  
   ,16  
   ,1  
   );  
END CATCH  
  