CREATE PROCEDURE SEGS6277_SPI  
  
@usuario  VARCHAR(20)  
  
AS  
  
SET NOCOUNT ON   
  
DECLARE @msg              VARCHAR(100)  
  
INSERT INTO interface_dados_db..SEGA8034_20_processado_tb (  
 SEGA8034_20_processar_id,  
 SEGA8034_processar_id,  
 proposta_id,  
 ramo_id,  
 apolice_id,  
 proposta_bb,  
 nome_segurado,  
 endereco_segurado,  
 municipio_segurado,  
 estado_segurado,  
 CEP_segurado,  
 codigo_agencia,  
 dv_agencia,  
 nome_agencia,  
 nome_produto,  
 dt_cancelamento,  
 dt_inclusao,  
 usuario,
 ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
 celular,  --migracao-documentacao-digital-2a-fase-cartas
 email, --migracao-documentacao-digital-2a-fase-cartas       
 produto_id, --migracao-documentacao-digital-2a-fase-cartas    
 cpf_cnpj   
)  
SELECT  
 SEGA8034_20_processar_id,  
 SEGA8034_processar_id,  
 proposta_id,  
 ramo_id,  
 apolice_id,  
 proposta_bb,  
 nome_segurado,  
 endereco_segurado,  
 municipio_segurado,  
 estado_segurado,  
 CEP_segurado,  
 codigo_agencia,  
 dv_agencia,  
 nome_agencia,  
 nome_produto,  
 dt_cancelamento,  
 dt_inclusao = getdate(),  
 @usuario,
 ddd_celular,  --migracao-documentacao-digital-2a-fase-cartas
 celular,  --migracao-documentacao-digital-2a-fase-cartas
 email,--migracao-documentacao-digital-2a-fase-cartas
 produto_id, --migracao-documentacao-digital-2a-fase-cartas  
 cpf_cnpj         
FROM interface_dados_db..SEGA8034_20_processar_tb  
  
  
  
IF @@ERROR <> 0  
 BEGIN                    
    SELECT @msg = 'Erro na inclusao do pós processamento do detalhe 20'  
     GOTO error                    
  END  
  
SET NOCOUNT OFF  
  
RETURN                    
                    
error:                    
  
   -- Confitec  sql2012  07/08/2015 16:11:38 - Inicio --  
    --raiserror 55555 @msg  
    Declare @msgRaiserror nvarchar(1000)  
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)   
  
    RAISERROR (@msgRaiserror, 16, 1)  
  
   -- Confitec  sql2012  07/08/2015 16:11:38 - Fim --  
  
  
  
  
  
  
  

