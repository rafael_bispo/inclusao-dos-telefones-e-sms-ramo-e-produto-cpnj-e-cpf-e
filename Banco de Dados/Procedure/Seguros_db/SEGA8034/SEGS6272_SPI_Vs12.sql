CREATE PROCEDURE SEGS6272_SPI  
          
   @Usuario          VARCHAR(20)          
          
AS          
          
          
SET NOCOUNT ON          
          
DECLARE @msg              VARCHAR(4000)          
          
          
CREATE TABLE #sega8034_processar_tb          
  (          
    sega8034_processar_id          INT ,           
    proposta_id                    NUMERIC(9,0),                
    produto_id                     INT,            
    ramo_id                        INT,          
    apolice_id                     INT,           
    proposta_bb                    INT,          
    segurado                       VARCHAR(255),          
    endereco                       VARCHAR(200), /* Jessica.Adao - Confitec Sistemas - 20181024 - Sprint 10: Aumento caracter endereco */                       
    bairro                         VARCHAR(30),            
    municipio                      VARCHAR(60),            
    cep                            VARCHAR(8),           
    cep_formatado                  VARCHAR(9),          
    estado                         VARCHAR(2),          
    cod_agencia                    INT,          
    dv_cod_agencia                 VARCHAR(1),          
    nome                           VARCHAR(60),          
    nome_agencia_corretor          VARCHAR(60),          
    dt_cancelamento             SMALLDATETIME,           
    tp_cancelamento                CHAR(1),          
    origem                         CHAR(2),
    ddd_celular					VARCHAR(4),  --migracao-documentacao-digital-2a-fase-cartas
    celular						VARCHAR(9),  --migracao-documentacao-digital-2a-fase-cartas
    email						VARCHAR(60),  --migracao-documentacao-digital-2a-fase-cartas  
    cpf_cnpj					VARCHAR(14)         
  )          
        
DECLARE @Data_sistema    SMALLDATETIME          
          
--Obtendo a data do sistema            
SET @Data_sistema = (SELECT dt_operacional          
                     FROM parametro_geral_tb)          
          
          
INSERT INTO #sega8034_processar_tb          
  (          
 sega8034_processar_id,                   
 proposta_id,          
 produto_id,          
 segurado,          
 endereco,          
 proposta_bb,          
 municipio,          
 cep_formatado,          
 estado,           
 dt_cancelamento,           
 tp_cancelamento,           
 apolice_id,          
 ramo_id,          
 origem          
  )          
          
SELECT sega8034_processar_tb.sega8034_processar_id,          
       cancelamento_proposta_tb.proposta_id,           
       sega8034_processar_tb.produto_id,          
       segurado = sega8034_10_processar_tb.nome_destinatario,          
       endereco = sega8034_10_processar_tb.endereco_destino,          
       sega8034_processar_tb.proposta_bb,          
       municipio = sega8034_10_processar_tb.municipio_destino,          
       cep_formatado = sega8034_10_processar_tb.cep_destino,           
       estado = sega8034_10_processar_tb.estado_destino,           
       cancelamento_proposta_tb.dt_inicio_cancelamento,           
       cancelamento_proposta_tb.tp_cancelamento,           
       sega8034_processar_tb.apolice_id,          
       sega8034_processar_tb.ramo_id,          
       sega8034_processar_tb.origem          
FROM interface_dados_db..sega8034_processar_tb sega8034_processar_tb (NOLOCK)          
INNER JOIN interface_dados_db..sega8034_10_processar_tb sega8034_10_processar_tb (NOLOCK)          
  ON sega8034_10_processar_tb.sega8034_processar_id = sega8034_processar_tb.sega8034_processar_id           
INNER JOIN cancelamento_proposta_tb (NOLOCK)           
  ON sega8034_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id          
 AND sega8034_processar_tb.endosso_id = cancelamento_proposta_tb.endosso_id           
WHERE sega8034_processar_tb.tipo_documento = 'ORIGINAL'     
      AND cancelamento_proposta_tb.dt_fim_cancelamento is null    
         
      
UNION           
--#VIDA - 2�VIA           
SELECT sega8034_processar_tb.sega8034_processar_id,          
       cancelamento_proposta_tb.proposta_id,           
       sega8034_processar_tb.produto_id,          
       segurado = sega8034_10_processar_tb.nome_destinatario,          
       endereco = sega8034_10_processar_tb.endereco_destino,          
   sega8034_processar_tb.proposta_bb,          
       municipio = sega8034_10_processar_tb.municipio_destino,          
       cep_formatado = sega8034_10_processar_tb.cep_destino,           
       estado = sega8034_10_processar_tb.estado_destino,           
       cancelamento_proposta_tb.dt_inicio_cancelamento,           
       cancelamento_proposta_tb.tp_cancelamento,            
       sega8034_processar_tb.apolice_id,          
       sega8034_processar_tb.ramo_id,          
       sega8034_processar_tb.origem          
 FROM interface_dados_db..sega8034_processar_tb sega8034_processar_tb(NOLOCK)        
INNER JOIN interface_dados_db..sega8034_10_processar_tb sega8034_10_processar_tb (NOLOCK)          
   ON sega8034_10_processar_tb.sega8034_processar_id = sega8034_processar_tb.sega8034_processar_id           
INNER JOIN cancelamento_proposta_tb (NOLOCK)          
   ON sega8034_processar_tb.proposta_id = cancelamento_proposta_tb.proposta_id           
  AND sega8034_processar_tb.endosso_id = cancelamento_proposta_tb.endosso_id           
WHERE sega8034_processar_tb.tipo_documento = 'SEGUNDA'    
      AND cancelamento_proposta_tb.dt_fim_cancelamento is null    
          
CREATE INDEX I001_sega8034_processar ON #sega8034_processar_tb (proposta_id)        
CREATE INDEX I002_sega8034_processar ON #sega8034_processar_tb (produto_id)      
        
UPDATE #sega8034_processar_tb          
   SET nome = produto_tb.nome      
  FROM #sega8034_processar_tb        
 INNER JOIN produto_tb (NOLOCK)           
    ON produto_tb.produto_id = #sega8034_processar_tb.produto_id       
   
-- SBRJ0015154 - Altera��o do nome do Produto para exibi��o do Nome do Plano para o Produto 1237 - RMarins 06/11/2019  
 UPDATE #sega8034_processar_tb     
    SET nome = plano_tb.nome        
   FROM #sega8034_processar_tb  
   JOIN seguros_db.dbo.proposta_tb proposta_tb WITH (NOLOCK)   
     ON #sega8034_processar_tb.proposta_id = proposta_tb.proposta_id  
   JOIN seguros_db.dbo.escolha_plano_tb escolha_plano_tb WITH (NOLOCK)   
     ON escolha_plano_tb.proposta_id = proposta_tb.proposta_id  
   JOIN seguros_db.dbo.plano_tb plano_tb WITH (NOLOCK)                                    
     ON escolha_plano_tb.plano_id = plano_tb.plano_id                                                              
    AND escolha_plano_tb.produto_id = plano_tb.produto_id                                                              
    AND escolha_plano_tb.dt_inicio_vigencia = plano_tb.dt_inicio_vigencia   
  WHERE proposta_tb.produto_id = 1237   
    AND plano_tb.plano_id NOT IN (1,2,3)  
 AND (escolha_plano_tb.endosso_id IS NULL OR escolha_plano_tb.endosso_id = 0)    
-- SBRJ0015154 - Fim altera��o - RMarins 06/11/2019        
          
UPDATE #sega8034_processar_tb          
SET cod_agencia = 0,          
    dv_cod_agencia = 0          
FROM #sega8034_processar_tb          
WHERE #sega8034_processar_tb.origem <> 'BB'          
          
--ObtemDadosCorretor           
SELECT proposta_tb.proposta_id,          
       corretor_id = ISNULL(corretor_tb.corretor_id,0),          
       nome_corretor = ISNULL(UPPER(corretor_tb.nome),'')          
INTO #dados_corretor          
FROM #sega8034_processar_tb          
INNER JOIN proposta_tb (NOLOCK)           
  ON #sega8034_processar_tb.proposta_id = proposta_tb.proposta_id          
INNER JOIN corretagem_pj_tb (NOLOCK)           
  ON proposta_tb.proposta_id = corretagem_pj_tb.proposta_id           
INNER JOIN corretor_tb (NOLOCK)           
  ON corretagem_pj_tb.corretor_id = corretor_tb.corretor_id           
INNER JOIN corretagem_tb (NOLOCK)           
  ON proposta_tb.proposta_id = corretagem_tb.proposta_id          
             
UNION           
          
SELECT proposta_tb.proposta_id,          
       corretor_id = ISNULL(corretor_tb.corretor_id,0),          
       nome_corretor = ISNULL(UPPER(corretor_tb.nome),'')          
FROM #sega8034_processar_tb          
INNER JOIN proposta_tb (NOLOCK)           
  ON #sega8034_processar_tb.proposta_id = proposta_tb.proposta_id          
INNER JOIN corretagem_pf_tb (NOLOCK)           
  ON proposta_tb.proposta_id = corretagem_pf_tb.proposta_id           
INNER JOIN corretor_tb (NOLOCK)           
  ON corretagem_pf_tb.corretor_id = corretor_tb.corretor_id           
INNER JOIN corretagem_tb (NOLOCK)           
   ON proposta_tb.proposta_id = corretagem_tb.proposta_id          
  AND corretagem_tb.dt_fim_corretagem IS NULL           
          
          
UPDATE #sega8034_processar_tb          
SET nome_agencia_corretor = ISNULL((SELECT DISTINCT nome_corretor           
                                    FROM #dados_corretor          
                                    WHERE #dados_corretor.proposta_id = #sega8034_processar_tb.proposta_id),'')          
FROM #sega8034_processar_tb          
WHERE #sega8034_processar_tb.origem <> 'BB'          
          
--ObtemDadosAgencia            
SELECT cont_agencia_id = proposta_adesao_tb.cont_agencia_id,          
       nome_agencia = agencia_tb.nome,          
       proposta_adesao_tb.proposta_id          
INTO #dados_agencia          
FROM #sega8034_processar_tb          
INNER JOIN proposta_adesao_tb (NOLOCK)          
  ON proposta_adesao_tb.proposta_id = #sega8034_processar_tb.proposta_id          
INNER JOIN agencia_tb (NOLOCK)          
  ON proposta_adesao_tb.cont_agencia_id = agencia_tb.agencia_id           
 AND agencia_tb.banco_id = proposta_adesao_tb.cont_banco_id           
          
UNION          
             
SELECT cont_agencia_id = proposta_fechada_tb.cont_agencia_id,          
       nome_agencia = agencia_tb.nome,          
       proposta_fechada_tb.proposta_id           
FROM #sega8034_processar_tb          
INNER JOIN proposta_fechada_tb (NOLOCK)          
   ON proposta_fechada_tb.proposta_id = #sega8034_processar_tb.proposta_id          
INNER JOIN agencia_tb (NOLOCK)          
   ON proposta_fechada_tb.cont_agencia_id = agencia_tb.agencia_id           
  AND agencia_tb.banco_id = proposta_fechada_tb.cont_banco_id           
          
           
UPDATE #sega8034_processar_tb          
SET nome_agencia_corretor = ISNULL(#dados_agencia.nome_agencia,''),          
    cod_agencia = ISNULL(#dados_agencia.cont_agencia_id,0),          
    dv_cod_agencia =  CONVERT(VARCHAR,dbo.calcula_dv_cc_ou_agencia(ISNULL(#dados_agencia.cont_agencia_id,0)))       
FROM #dados_agencia          
INNER JOIN #sega8034_processar_tb          
  ON #dados_agencia.proposta_id = #sega8034_processar_tb.proposta_id   
  
  
  -- Atualizando o contatos do segurado --------------------------------------------  
  --migracao-documentacao-digital-2a-fase-cartas - RAFAEL MARTINS
UPDATE #sega8034_processar_tb
   SET ddd_celular = isnull(cliente_tb.ddd_1,''), celular = isnull(cliente_tb.telefone_1,'')
     , email = isnull(cliente_tb.e_mail,'')
  FROM #sega8034_processar_tb (NOLOCK)  
  JOIN seguros_db..proposta_tb proposta_tb (NOLOCK)  
	ON #sega8034_processar_tb.proposta_id = proposta_tb.proposta_id  
  JOIN seguros_db..cliente_tb cliente_tb (NOLOCK)  
	ON proposta_tb.prop_cliente_id = cliente_tb.cliente_id    
	
	-- RAFAEL MARTINS - Inclus�o dos Telefones e SMS, Ramo e Produto, CPNJ e CPF, e Arquivo de Retorno
	update #sega8034_processar_tb
	set #sega8034_processar_tb.CPF_CNPJ = CASE 
												WHEN pessoa_fisica_tb.cpf <> '' THEN pessoa_fisica_tb.cpf
												WHEN pessoa_juridica_tb.cgc <> '' THEN pessoa_juridica_tb.cgc
												ELSE cliente_tb.cpf_cnpj
											END 
	from #sega8034_processar_tb #sega8034_processar_tb
	inner join seguros_db.dbo.proposta_tb proposta_tb with(nolock)
		on proposta_tb.proposta_id = #sega8034_processar_tb.proposta_id
	inner join seguros_db..cliente_tb cliente_tb with(nolock)
		on cliente_tb.cliente_id = proposta_tb.prop_cliente_Id
	left join seguros_db.dbo.pessoa_fisica_tb pessoa_fisica_tb with(nolock)
		on cliente_tb.cliente_id = pessoa_fisica_tb.pf_cliente_id
	left join seguros_db..pessoa_juridica_tb pessoa_juridica_tb (nolock)
		on cliente_tb.cliente_id = pessoa_juridica_tb.pj_cliente_id     
          
          
--Inserindo na tabela f�sica           
INSERT INTO interface_dados_db..sega8034_20_processar_tb          
  (          
    sega8034_processar_id,          
    ramo_id,          
    apolice_id,          
    proposta_id,          
    proposta_bb,          
    nome_segurado,          
    endereco_segurado,          
    municipio_segurado,          
    cep_segurado,          
    estado_segurado,          
    codigo_agencia,          
    dv_agencia,          
    nome_agencia,          
    nome_produto,          
    dt_cancelamento,          
    dt_inclusao,          
    usuario,
    ddd_celular, --migracao-documentacao-digital-2a-fase-cartas 
    celular, --migracao-documentacao-digital-2a-fase-cartas 
    email, --migracao-documentacao-digital-2a-fase-cartas 
    produto_id, --migracao-documentacao-digital-2a-fase-cartas 
    cpf_cnpj
  )          
          
SELECT sega8034_processar_id,          
       ramo_id,          
       apolice_id,          
       proposta_id,          
       proposta_bb,          
       LEFT(segurado,60),          
       LEFT(endereco,200), /* Jessica.Adao - Confitec Sistemas - 20181024 - Sprint 10: Aumento caracter endereco */                     
       LEFT(municipio,60),          
       LEFT(cep_formatado,9),          
       LEFT(estado,2),          
       ISNULL(cod_agencia,0),          
       LEFT(ISNULL(dv_cod_agencia,''),1),          
       LEFT(ISNULL(nome_agencia_corretor,''),60),          
       LEFT(ISNULL(nome,''),60),          
       dt_cancelamento,          
       GETDATE(),          
       @usuario,
       ddd_celular, --migracao-documentacao-digital-2a-fase-cartas 
	   celular, --migracao-documentacao-digital-2a-fase-cartas 
       email, --migracao-documentacao-digital-2a-fase-cartas 
       produto_id, --migracao-documentacao-digital-2a-fase-cartas 
       cpf_cnpj
FROM #sega8034_processar_tb          
          
          
          
IF @@ERROR <> 0          
 BEGIN                            
    SELECT @msg = 'Erro na inclus�o em SEGA8034_20_processar_tb'          
     GOTO error     
  END          
          
          
Drop table #sega8034_processar_tb          
          
SET NOCOUNT OFF          
          
RETURN                            
                            
error:                            
    
   -- Confitec � sql2012 � 07/08/2015 16:11:38 - Inicio --    
    --raiserror 55555 @msg          
    Declare @msgRaiserror nvarchar(1000)    
    Set @msgRaiserror =  convert(varchar(1000), '55555 ') + ' | ' + convert(varchar(1000), @msg)     
    
    RAISERROR (@msgRaiserror, 16, 1)    
    
   -- Confitec � sql2012 � 07/08/2015 16:11:38 - Fim --    
    
    
    
    
    
    
  
  
  

