DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 12
SET @total_registro_afetados = 0

declare @sega9133 int,  @sega9225 int,  @SEGA9094 int, @SEGA9156 int,  @sega9221 int,  @SEGA9220 int

select @sega9133 = layout_id from interface_db..layout_tb where nome = 'sega9133'
select @sega9225 = layout_id from interface_db..layout_tb where nome = 'sega9225'
select @SEGA9094 = layout_id from interface_db..layout_tb where nome = 'SEGA9094'
select @SEGA9156 = layout_id from interface_db..layout_tb where nome = 'SEGA9156'
select @sega9221 = layout_id from interface_db..layout_tb where nome = 'sega9221'
select @SEGA9220 = layout_id from interface_db..layout_tb where nome = 'SEGA9220'


INSERT INTO interface_db.dbo.layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT @SEGA9094,'2009-10-08 00:00:00',2,16,'Produto_id',16,'Produto_id',null,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','sega9094_10_processar_tb','produto_id','interface_dados_db','sega9094_10_processar_tb','produto_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9094,'2009-10-08 00:00:00',2,17,'Ramo_id',17,'Ramo_id',null,'NM','',9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','sega9094_10_processar_tb','ramo_id','interface_dados_db    ','sega9094_10_processar_tb','ramo_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9133,'2011-05-03 00:00:00',3,17,'Produto_id',14,'Produto_id',NULL,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9133_20_processar_tb','produto_id','interface_dados_db','SEGA9133_20_processar_tb','produto_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9133,'2011-05-03 00:00:00',3,18,'Ramo_id',15,'Ramo_id',NULL,'NM','',9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9133_20_processar_tb','ramo_id','interface_dados_db','SEGA9133_20_processar_tb','ramo_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9156,'2012-01-19 00:00:00',3,19,'Produto_id',19,'Produto_id',NULL,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','produto_id','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','produto_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9156,'2012-01-19 00:00:00',3,20,'Ramo_id',20,'Ramo_id',NULL,'NM','',9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','ramo_id','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','ramo_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9220,'2016-02-04 00:00:00',2,16,'Produto_id',16,'Produto_id',NULL,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','','','','','','',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9220,'2016-02-04 00:00:00',2,17,'Ramo_id',17,'Ramo_id',NULL,'NM','',9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','','','','','','',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9221,'2017-05-19 00:00:00',3,19,'Produto_id',14,'Produto_id',NULL,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9221_20_processar_tb','produto_id','interface_dados_db','SEGA9221_20_processar_tb','produto_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9221,'2017-05-19 00:00:00',3,20,'Ramo_id',15,'Ramo_id',NULL,'NM','',9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9221_20_processar_tb','ramo_id','interface_dados_db','SEGA9221_20_processar_tb','ramo_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9225,'2017-05-26 12:00:00',3,21,'Produto_id',21,'Produto_id',NULL,'NM','',4,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9225_20_PROCESSAR_TB','produto_id','interface_dados_db','SEGA9225_20_PROCESSAR_TB','produto_id',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9225,'2017-05-26 12:00:00',3,22,'Ramo_id',22,'Ramo_id',NULL,'NM','' ,9,'n','2020-08-28 09:45:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9225_20_PROCESSAR_TB','ramo_id','interface_dados_db','SEGA9225_20_PROCESSAR_TB','ramo_id',NULL,NULL,NULL,NULL,'P' 
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT 


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END
