DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 12
SET @total_registro_afetados = 0

declare @SEGA8034 INT,@SEGA8037 INT,@SEGA8040 INT,@SEGA8083 INT,@SEGA8084 INT,@SEGA8085 INT
,@SEGA8135 INT,@SEGA8177 INT,@SEGA8178 INT,@sega9094 INT,@SEGA9133 INT,@SEGA9134 INT,
@SEGA9141 INT,@SEGA9154 INT

select @SEGA8034 = layout_id from interface_db..layout_tb where nome = 'SEGA8034'
select @SEGA8037 = layout_id from interface_db..layout_tb where nome = 'SEGA8037'
select @SEGA8040 = layout_id from interface_db..layout_tb where nome = 'SEGA8040'
select @SEGA8083 = layout_id from interface_db..layout_tb where nome = 'SEGA8083'
select @SEGA8084 = layout_id from interface_db..layout_tb where nome = 'SEGA8084'
select @SEGA8085 = layout_id from interface_db..layout_tb where nome = 'SEGA8085'
select @SEGA8135 = layout_id from interface_db..layout_tb where nome = 'SEGA8135'
select @SEGA8177 = layout_id from interface_db..layout_tb where nome = 'SEGA8177'
select @SEGA8178 = layout_id from interface_db..layout_tb where nome = 'SEGA8178'
select @sega9094 = layout_id from interface_db..layout_tb where nome = 'sega9094'
select @SEGA9133 = layout_id from interface_db..layout_tb where nome = 'SEGA9133'
select @SEGA9134 = layout_id from interface_db..layout_tb where nome = 'SEGA9134'
select @SEGA9141 = layout_id from interface_db..layout_tb where nome = 'SEGA9141'
select @SEGA9154 = layout_id from interface_db..layout_tb where nome = 'SEGA9154'


INSERT INTO interface_db.dbo.layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT @SEGA8034,'2002-06-04 00:00:00',3,22,'CPF_CNPJ',22,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8034_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8034_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA8037,'2002-06-04 00:00:00',3,21,'CPF_CNPJ',20,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8037_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8037_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA8040,'2002-06-04 00:00:00',3,25,'CPF_CNPJ',21,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db ','SEGA8040_20_PROCESSAR_TB ','cpf_cnpj','interface_dados_db ','SEGA8040_20_PROCESSAR_TB ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA8083,'2002-06-04 00:00:00',3,22,'CPF_CNPJ',21,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8083_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8083_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P'  UNION ALL
SELECT @SEGA8084,'2002-06-04 00:00:00',3,20,'CPF_CNPJ',20,'CPF_CNPJ',NULL,'AN','' ,14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8084_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8084_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P'  UNION ALL
SELECT @SEGA8085,'2002-06-04 00:00:00',3,19,'CPF_CNPJ',19,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8085_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8085_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA8135,'2005-11-17 00:00:00',3,21,'CPF_CNPJ',21,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA8135_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA8135_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9094,'2009-10-08 00:00:00',2,18,'CPF_CNPJ',18,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db    ','sega9094_10_processar_tb   ','cpf_cnpj','interface_dados_db    ','sega9094_10_processar_tb   ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9133,'2011-05-03 00:00:00',3,19,'CPF_CNPJ',16,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db      ','SEGA9133_20_processar_tb    ','cpf_cnpj','interface_dados_db      ','SEGA9133_20_processar_tb    ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9134,'2011-05-03 00:00:00',3,20,'CPF_CNPJ',17,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db           ','SEGA9134_20_processar_tb      ','cpf_cnpj','interface_dados_db           ','SEGA9134_20_processar_tb      ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9141,'2011-12-13 00:00:00',9,8,'CPF_CNPJ',8,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9141_50_processar_tb','cpf_cnpj','interface_dados_db','SEGA9141_50_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9154,'2012-01-19 00:00:00',3,19,'CPF_CNPJ',14,'CPF_CNPJ',NULL,'AN', '',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db  ','SEGA9154_20_PROCESSAR_TB  ','cpf_cnpj','interface_dados_db  ','SEGA9154_20_PROCESSAR_TB  ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' 
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT 


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END
