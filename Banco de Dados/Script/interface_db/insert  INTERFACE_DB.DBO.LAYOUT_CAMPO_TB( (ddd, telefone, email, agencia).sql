DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 12
SET @total_registro_afetados = 0

declare @sega9134 int,  @sega9160 int,  @sega9231 int


select @sega9134 = layout_id from interface_db..layout_tb where nome = 'SEGA9134'
select @sega9160 = layout_id from interface_db..layout_tb where nome = 'SEGA9160'
select @sega9231 = layout_id from interface_db..layout_tb where nome = 'SEGA9231'


INSERT INTO INTERFACE_DB.DBO.LAYOUT_CAMPO_TB(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT @sega9134,'2011-05-03 00:00:00',3,16,'DDD Celular',13,'DDD Celular',NULL,'AN', ''     ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9134_20_processar_tb','ddd_celular','interface_dados_db','SEGA9134_20_processar_tb','ddd_celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9134,'2011-05-03 00:00:00',3,17,'Celular',14,'Celular',NULL,'AN', ''     ,9,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9134_20_processar_tb','celular','interface_dados_db','SEGA9134_20_processar_tb','celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9134,'2011-05-03 00:00:00',3,18,'Email',15,'Email',NULL,'AN',  ''    ,60,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9134_20_processar_tb','email','interface_dados_db','SEGA9134_20_processar_tb','email',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9134,'2011-05-03 00:00:00',3,19,'Ag�ncia',16,'Ag�ncia',NULL,'NM', ''     ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9134_20_processar_tb','agencia','interface_dados_db','SEGA9134_20_processar_tb','agencia',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9160,'2012-06-18 00:00:00',3,14,'DDD Celular',11,'DDD Celular',NULL,'AN',  ''   ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9160_20_processar_tb','ddd_celular','interface_dados_db','SEGA9160_20_processar_tb','ddd_celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9160,'2012-06-18 00:00:00',3,15,'Celular',12,'Celular',NULL,'AN',  ''    ,9,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9160_20_processar_tb','celular','interface_dados_db','SEGA9160_20_processar_tb','celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9160,'2012-06-18 00:00:00',3,16,'Email',13,'Email',NULL,'AN',  ''    ,60,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9160_20_processar_tb','email','interface_dados_db','SEGA9160_20_processar_tb','email',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9160,'2012-06-18 00:00:00',3,17,'Ag�ncia',14,'Ag�ncia',NULL,'NM',  ''    ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9160_20_processar_tb','agencia','interface_dados_db','SEGA9160_20_processar_tb','agencia',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9231,'2002-06-04 00:00:00',3,9,'DDD Celular',9,'DDD Celular',NULL,'AN',  ''    ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','sega9231_20_processar_tb','ddd_celular','interface_dados_db','sega9231_20_processar_tb','ddd_celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9231,'2002-06-04 00:00:00',3,10,'Celular',10,'Celular',NULL,'AN',  ''    ,9,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','sega9231_20_processar_tb','celular','interface_dados_db','sega9231_20_processar_tb','celular',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9231,'2002-06-04 00:00:00',3,11,'Email',11,'Email',NULL,'AN',  ''    ,60,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','sega9231_20_processar_tb','email','interface_dados_db','sega9231_20_processar_tb','email',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @sega9231,'2002-06-04 00:00:00',3,12,'Ag�ncia',12,'Ag�ncia',NULL,'NM',  ''    ,4,'n','2020-08-26 10:44:00','','MigracaoDocDigital','F','interface_dados_db','sega9231_20_processar_tb','agencia','interface_dados_db','sega9231_20_processar_tb','agencia',NULL,NULL,NULL,NULL,'P'
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT 


-- Verifica��o
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END



