DECLARE @total_registro INT
DECLARE @total_registro_afetados INT
DECLARE @mensagem VARCHAR(500)

SET @total_registro = 12
SET @total_registro_afetados = 0

declare @SEGA9155 INT,@SEGA9156 INT,@SEGA9177 INT,@SEGA9179 INT,
@SEGA9200 INT,@SEGA9206 INT,@SEGA9209 INT,@sega9210 INT,@sega9211 INT,@SEGA9212 INT,
@SEGA9222 INT,@SEGA9240 INT


select @SEGA9155 = layout_id from interface_db..layout_tb where nome = 'SEGA9155'
select @SEGA9156 = layout_id from interface_db..layout_tb where nome = 'SEGA9156'
select @SEGA9177 = layout_id from interface_db..layout_tb where nome = 'SEGA9177'
select @SEGA9179 = layout_id from interface_db..layout_tb where nome = 'SEGA9179'
select @SEGA9200 = layout_id from interface_db..layout_tb where nome = 'SEGA9200'
select @SEGA9206 = layout_id from interface_db..layout_tb where nome = 'SEGA9206'
select @SEGA9209 = layout_id from interface_db..layout_tb where nome = 'SEGA9209'
select @sega9210 = layout_id from interface_db..layout_tb where nome = 'sega9210'
select @sega9211 = layout_id from interface_db..layout_tb where nome = 'sega9211'
select @SEGA9212 = layout_id from interface_db..layout_tb where nome = 'SEGA9212'
select @SEGA9222 = layout_id from interface_db..layout_tb where nome = 'SEGA9222'
select @SEGA9240 = layout_id from interface_db..layout_tb where nome = 'SEGA9240'


INSERT INTO interface_db.dbo.layout_campo_tb(layout_id, dt_ini_vigencia, registro_id, campo_id, nome, seq_campo, descricao, constante, tp_dado, formato, tamanho, obrigatorio, dt_inclusao, dt_alteracao, usuario, tp_campo, banco_origem, tabela_origem, coluna_origem, banco, tabela, coluna, layout_campo_formato_id, preencher_com_zeros, qtde_casas_decimais, acrescentar_ao_contador, alinhamento)
SELECT @SEGA9155,'2012-01-19 00:00:00',3,21,'CPF_CNPJ',21,'CPF_CNPJ',NULL,'AN','' ,14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','INTERFACE_DADOS_DB','SEGA9155_20_processar_tb','cpf_cnpj','INTERFACE_DADOS_DB','SEGA9155_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9156,'2012-01-19 00:00:00',3,21,'CPF_CNPJ',21,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','cpf_cnpj','INTERFACE_DADOS_DB','SEGA9156_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9177,'2014-04-30 00:00:00',3,20,'CPF_CNPJ',20,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','Interface_dados_db','SEGA9177_20_processar_tb','cpf_cnpj','Interface_dados_db','SEGA9177_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9179,'2002-06-04 00:00:00',4,15,'CPF_CNPJ',14,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','Interface_dados_db','SEGA9179_30_processar_tb','cpf_cnpj','Interface_dados_db','SEGA9179_30_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9200,'2013-10-14 00:00:00',3,18,'CPF_CNPJ',18,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','INTERFACE_DADOS_DB','SEGA9200_20_PROCESSAR_TB','cpf_cnpj','INTERFACE_DADOS_DB','SEGA9200_20_PROCESSAR_TB','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9206,'2002-06-04 00:00:00',3,32,'CPF_CNPJ',31,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','Interface_dados_db','SEGA9206_20_processar_tb','cpf_cnpj','Interface_dados_db','SEGA9206_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9209,'2015-07-23 00:00:00',3,16,'CPF_CNPJ',16,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9209_20_processar_tb','cpf_cnpj','interface_dados_db','SEGA9209_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9210,'2002-06-04 00:00:00',3,20,'CPF_CNPJ',20,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','sega9210_20_processar_tb','cpf_cnpj','interface_dados_db','sega9210_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9211,'2002-06-04 00:00:00',3,18,'CPF_CNPJ',15,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db      ','sega9211_20_processar_tb    ','cpf_cnpj','interface_dados_db      ','sega9211_20_processar_tb    ','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9212,'2002-06-04 00:00:00',4,15,'CPF_CNPJ',14,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9212_30_processar_tb','cpf_cnpj','interface_dados_db','SEGA9212_30_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9222,'2016-11-21 21:32:00',4,12,'CPF_CNPJ',12,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','interface_dados_db','SEGA9222_30_processar_tb','cpf_cnpj','interface_dados_db','SEGA9222_30_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' UNION ALL
SELECT @SEGA9240,'2019-12-02 00:00:00',3,23,'CPF_CNPJ',23,'CPF_CNPJ',NULL,'AN','',14,'n','2020-08-28 10:48:00','','MigracaoDocDigital','F','Interface_dados_db','SEGA9240_20_processar_tb','cpf_cnpj','Interface_dados_db','SEGA9240_20_processar_tb','cpf_cnpj',NULL,NULL,NULL,NULL,'P' 
SET @total_registro_afetados = @total_registro_afetados + @@ROWCOUNT 


-- Verificação
SELECT @total_registro_afetados

SELECT @total_registro

IF ( (@@SERVERNAME = 'SISAB003' or @@SERVERNAME = 'SISAS003\ABS') AND @total_registro <> @total_registro_afetados )
BEGIN
SET @mensagem = 'qtd de registros que deveriam ser afetados: ' + CONVERT(VARCHAR(10), @total_registro) + 'qtd de registros afetados: ' + CONVERT(VARCHAR(10), @total_registro_afetados)

RAISERROR (@mensagem, 16, 1)
END
