VERSION 5.00
Begin VB.Form frmSEGA8151 
   Caption         =   "SEGA8151"
   ClientHeight    =   1650
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   2670
   LinkTopic       =   "Form1"
   ScaleHeight     =   1650
   ScaleWidth      =   2670
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   780
      TabIndex        =   0
      Top             =   360
      Width           =   1095
   End
End
Attribute VB_Name = "frmSEGA8151"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Const cteTpRegRenovGarantia = "07"
Const cteProdIdOurovidaGarantia = "11"

Dim arquivo_remessa                 As String

Dim proposta      As String
Dim PropostaBB    As String
Dim PropostaBB_ant As String
Dim DtAprovacao   As String
Dim Produto       As String
Dim NomeProduto   As String
Dim Endosso       As String
Dim nome          As String
Dim Endereco      As String
Dim Bairro        As String
Dim Municipio     As String
Dim Cep           As String
Dim UF            As String
Dim Endereco_ag   As String
Dim Municipio_ag  As String
Dim CEP_ag        As String
Dim UF_ag         As String
Dim CPF           As String
Dim agencia       As String
Dim Agencia_ant   As String
Dim DVAgencia     As String
Dim NomeAgencia   As String
Dim Nome_Conj     As String
Dim CPF_Conj      As String
Dim NomeCorretor  As String
Dim TelCorretor

Dim Seguradora    As String
Dim Seguradora_ant As String
Dim Sucursal      As String
Dim Sucursal_ant  As String
Dim ramo          As String
Dim Ramo_ant      As String
Dim Apolice       As String
Dim Apolice_ant   As String
Dim Certificado   As String
Dim DvCertificado As String
Dim Plano         As String
Dim TpPlano       As String
Dim TpPlano_ant   As String
Dim Banco         As String
Dim DtIniVig      As String
Dim Imp_Seg       As Double
Dim Val_Premio    As String
Dim Componente_Conjuge As String
Dim PercCobTit1   As Double
Dim PercCobTit2   As Double
Dim PercCobTit3   As Double
Dim PercCobTit4   As Double
Dim PercCobConj1  As Double
Dim PercCobConj2  As Double
Dim PercCobConj3  As Double
Dim PercCobConj4  As Double

Dim IS1_Tit       As String
Dim IS2_Tit       As String
Dim IS3_Tit       As String
Dim IS4_Tit       As String

Dim IS1_Conj      As String
Dim IS2_Conj      As String
Dim IS3_Conj      As String
Dim IS4_Conj      As String

Dim Observacao    As String
Dim Tp_Registro   As String

Dim Tp_Seguro     As String

Dim FlagPropBasica          As Boolean
Dim FlagExisteCertificado   As Boolean
Dim Flag_Tem_endosso        As Boolean

Dim Produto_Select          As String

Dim Nome_Rel_Certificado    As String
Dim NumRemessa              As String
Dim EnderecoIncompleto      As Boolean

Dim Certificado_path        As String
Dim logpath                 As String
Dim Conta_Linha_certificadoA As Long
Dim Conta_Linha_certificadoC As Long
Dim Conta_Linha_certificadoD As Long
Dim fAgencia                As Integer
Dim fCliente                As Integer
Dim fAlianca                As Integer
Dim Receptor()
Dim qtdRegA                 As Long
Dim qtdRegC                 As Long
Dim qtdRegD                 As Long
Dim Nome_ArqA               As String
Dim Nome_ArqC               As String
Dim Nome_ArqD               As String
Dim wNewA                   As Boolean
Dim wNewC                   As Boolean
Dim wNewD                   As Boolean

Dim ConfiguracaoBrasil      As Boolean

Const TpDocCertificado = "10"
Const cteArquivoPrintech = 3

Dim gTituloSegundaVia As String

    Dim Dt_Nascimento As String
    Dim Sexo          As String
    Dim Dt_Emissao          As String
    Dim Nome_Profissao      As String
    Dim DtInicioVigencia  As String
    Dim DtFimVigencia     As String
    Dim Resp1Tit As String
    Dim Resp2Tit As String
    Dim Resp3Tit As String
    Dim Resp4Tit As String
    Dim Resp5Tit As String
    Dim Descr1Tit As String
    Dim Descr2Tit As String
    Dim Descr3Tit As String
    Dim Descr4Tit As String
    Dim Descr5Tit As String
    Dim Resp1Compl As String
    Dim Resp2Compl As String
    Dim Resp3Compl As String
    Dim Resp4Compl As String
    Dim Resp5Compl As String
    Dim Descr1Compl As String
    Dim Descr2Compl As String
    Dim Descr3Compl As String
    Dim Descr4Compl As String
    Dim Descr5Compl As String

Private Sub cmdOk_Click()
    
    On Error GoTo Erro
    
    MousePointer = vbHourglass
       
    Seguradora_ant = ""
    Sucursal_ant = ""
    Ramo_ant = ""
    Apolice_ant = ""
    Tp_plano_ant = ""
    Agencia_ant = ""
    Conta_Linha_certificado = 0
    
    Nome_Rel_Certificado = "SEGA8151"
    Nome_Rel_Certificado = Left(Nome_Rel_Certificado + Space(25), 25)
      
    Produto_Select = "0012,0121"
    
    MousePointer = 11
    
    Certificado_path = LerArquivoIni("relatorios", "REMESSA_GERADO_PATH")
    
    logpath = LerArquivoIni("Producao", "LOG_PATH")
    
    sTpDocumentos = getArquivoDocumento("SEGA8151")
    
    Call Processa(sTpDocumentos)
    
    Call goProducao.Finaliza
    
    Exit Sub
    
Erro:
   TrataErroGeral "CmdOk_Click SEGA8151", Me.name
   TerminaSEGBR
    
End Sub

Private Sub cmdCanc_Click()
  Unload Me
End Sub

Private Sub Processa(ByVal sTpDocumentos As String)
   
   Dim valor_aux   As Currency
   Dim data_Aux    As Date
   Dim tam_reg     As Integer
   
   On Error GoTo Erro
   
    'Selecionando dados da proposta, nome e endereco para gera��o do cerficado''''''''''''''
    Conta_Linha_certificado = 0: conta_linha = 0: x = 0
    Sql = ""
    Sql = Sql & "EXEC SEGS4305_SPS " & sTpDocumentos
    Set rc = rdocn1.OpenResultset(Sql)
   
    rdocn.BeginTrans
   
    While Not rc.EOF
     
        DoEvents
        'Inicializando vari�veis que podem ou n�o ser preenchidas''''''''''''''''''''''''''
        
        TelCorretor = Space(15)
        
        'Obntendo dados do certificado'''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        proposta = Format(rc(0), "000000000")
        nome = Left(rc(1) + Space(50), 50)
        Endereco = Left(rc(2) + Space(50), 50)
        Bairro = rc(3)
        Municipio = Left(rc(4) + Space(45), 45)
        Cep = Format(rc(5), "00000000")
        Cep = Left(Cep, 5) & "-" & Right(Cep, 3)
        UF = rc(6)
        DtInicioVigencia = Format(rc(7), "dd/mm/yyyy")
        Produto = rc(8)
        CPF = rc(9)
        CPF = Left(CPF, 3) & "." & Mid(CPF, 4, 3) & "." & Mid(CPF, 7, 3) & "/" & Right(CPF, 2)
        num_solicitacao_reemissao = rc(10)
        Observacao = String(50, " ")
        Dt_Nascimento = Format(rc(11), "dd/mm/yyyy")
        Sexo = UCase(rc(12))
        Nome_Profissao = Left(UCase(rc(13)) & Space(60), 60)
        
        If CPF = "000.000.000/00" Then
           GoTo Continua
        End If
     
        'Obntendo dados adicionais para gera��o do certificado''''''''''''''''''''''''''''''

        Obtem_Dados_Produto
        
        Obtem_Dados_Proposta_Adesao
     
        If Not FlagPropBasica Then
        
            Obtem_Dados_Apolice
            Obtem_Dados_Agencia
            Obtem_Dados_Escolha_Plano
            Obtem_Dados_Coberturas
            Obtem_Dados_Conjuge
            
            '# identifica��o da exist�ncia de uma propostabb anterior e se o certificado
            '# se refere a uma migra��o ou renova��o, que � verificado pelo campo tp_registro
            '# obtido em proposta_adesao_tb.info_complementar
            
            If Trim(PropostaBB_ant) <> "" Then
                Select Case Tp_Registro
                    Case "11", "12"
                        Observacao = "Emiss�o de Certificado por Migra��o de Produto"
                        Observacao = Left(Observacao + Space(50), 50)
                    Case cteTpRegRenovGarantia
                        If Produto_Select = cteProdIdOurovidaGarantia Then
                            Observacao = "Renova��o da proposta N� " & PropostaBB_ant
                            Observacao = Left(Observacao + Space(50), 50)
                        End If
                End Select
            End If
            
            'Verificando se � uma re-emiss�o (def. campo observa��o)'''''''''''''''''''''
            
            Verifica_Certificado
            
            'Abrindo o arquivo de cerficados ''''''''''''''''''''''''''''''''''''''''''''

            If Conta_Linha_certificadoA = 0 And _
                Conta_Linha_certificadoC = 0 And _
                Conta_Linha_certificadoD = 0 Then
                gTituloSegundaVia = ""
                Abre_Arquivo
            End If
            
            'Obtendo o nr. do cerficado '''''''''''''''''''''''''''''''''''''''''''''''''

            Obtem_Numero_Certificado
        
            'Obtendo valores de IS para cada cobertura'''''''''''''''''''''''''''''''''''
            
            valor_aux = Imp_Seg * PercCobTit1   'Morte Natural
            IS1_Tit = Format(valor_aux, "###,###,##0.00")
            IS1_Tit = Right(String(14, " ") & IS1_Tit, 14)
            valor_aux = Imp_Seg * PercCobTit2   'Morte Acidental
            IS2_Tit = Format(valor_aux, "###,###,##0.00")
            IS2_Tit = Right(String(14, " ") & IS2_Tit, 14)
            valor_aux = Imp_Seg * PercCobTit3   'Invalidez Permanente
            IS3_Tit = Format(valor_aux, "###,###,##0.00")
            IS3_Tit = Right(String(14, " ") & IS3_Tit, 14)
            valor_aux = Imp_Seg * PercCobTit4    'Invalidez por Doen�a
            IS4_Tit = Format(valor_aux, "###,###,##0.00")
            IS4_Tit = Right(String(14, " ") & IS4_Tit, 14)
        
            If Componente_Conjuge <> "" Then
               valor_aux = Imp_Seg * PercCobConj1   'Morte Natural
               IS1_Conj = Format(valor_aux, "###,###,##0.00")
               IS1_Conj = Right(String(14, " ") & IS1_Conj, 14)
               valor_aux = Imp_Seg * PercCobConj2   'Morte Acidental
               IS2_Conj = Format(valor_aux, "###,###,##0.00")
               IS2_Conj = Right(String(14, " ") & IS2_Conj, 14)
               valor_aux = Imp_Seg * PercCobConj3  'Invalidez Permanente
               IS3_Conj = Format(valor_aux, "###,###,##0.00")
               IS3_Conj = Right(String(14, " ") & IS3_Conj, 14)
               valor_aux = Imp_Seg * PercCobConj4   'Invalidez por Doen�a
               IS4_Conj = Format(valor_aux, "###,###,##0.00")
               IS4_Conj = Right(String(14, " ") & IS4_Conj, 14)
            Else
               IS1_Conj = String(14, "*")
               IS2_Conj = String(14, "*")
               IS3_Conj = String(14, "*")
               IS4_Conj = String(14, "*")
            End If
        
            If Not ConfiguracaoBrasil Then
               IS1_Tit = TrocaValorAmePorBras(IS1_Tit)
               IS1_Tit = Right(String(14, " ") & IS1_Tit, 14)
               IS2_Tit = TrocaValorAmePorBras(IS2_Tit)
               IS2_Tit = Right(String(14, " ") & IS2_Tit, 14)
               IS3_Tit = TrocaValorAmePorBras(IS3_Tit)
               IS3_Tit = Right(String(14, " ") & IS3_Tit, 14)
               IS4_Tit = TrocaValorAmePorBras(IS4_Tit)
               IS4_Tit = Right(String(14, " ") & IS4_Tit, 14)
               
               IS1_Conj = TrocaValorAmePorBras(IS1_Conj)
               IS1_Conj = Right(String(14, " ") & IS1_Conj, 14)
               IS2_Conj = TrocaValorAmePorBras(IS2_Conj)
               IS2_Conj = Right(String(14, " ") & IS2_Conj, 14)
               IS3_Conj = TrocaValorAmePorBras(IS3_Conj)
               IS3_Conj = Right(String(14, " ") & IS3_Conj, 14)
               IS4_Conj = TrocaValorAmePorBras(IS4_Conj)
               IS4_Conj = Right(String(14, " ") & IS4_Conj, 14)
            End If

            Mascara_Valores_Zerados
        
            NrApoliceAux = Sucursal & ramo & Apolice
        
            cod_produto_barra = Format(Produto, "000")
        
            'Calculando data de fim de vigencia''''''''''''''''''''''''''''''''''''''''
            
            data_Aux = DtInicioVigencia
            DtFimVigencia = Format(DateAdd("yyyy", 1, data_Aux), "dd/mm/yyyy")
        
            'Gravando registro do certificado no arquivo txt''''''''''''''''''''''''''
            Conta_Linha_certificadoC = Conta_Linha_certificadoC + 1
            qtdRegC = qtdRegC + 1
            linha = "2" & "S" & Format(Conta_Linha_certificadoC, "00000")
            
            tam_reg = 334

            linha = linha & agencia & NomeAgencia & NrApoliceAux & Certificado & "-" & DvCertificado & PropostaBB
            linha = linha & CPF & nome & Endereco & Municipio & UF & Cep & CPF_Conj
            linha = linha & NomeProduto
            '** Alterado por Paulo Carvalho em 02/10/2002
            linha = linha & "05"
            linha = linha & proposta
            linha = linha & ConverteParaJulianDate(CDate(Data_Sistema))
            linha = linha & agencia
            '**
            linha = Left(linha + Space(tam_reg), tam_reg)
            
            Print #fCliente, linha
                        
            Conta_Linha_certificadoC = Conta_Linha_certificadoC + 1
            linha = "2" & "C" & Format(Conta_Linha_certificadoC, "00000")
                        
            linha = linha & Nome_Conj & IS1_Tit & IS2_Tit & IS3_Tit & IS4_Tit
            linha = linha & IS1_Conj & IS2_Conj & IS3_Conj & IS4_Conj & DtInicioVigencia & Observacao
            linha = Left(linha + Space(tam_reg), tam_reg)
            
            Print #fCliente, linha
                   
            conta_linha = conta_linha + 1
        
            arquivo_remessa = Trim(Nome_Rel_Certificado) & "." & Right(Nome_ArqC, 4)
        
            Call Atualiza_Evento_Impressao(rc!NumSolicitacao, arquivo_remessa, cUserName)
        
        End If
      
Continua:
      rc.MoveNext
   
   Wend
   
   rc.Close
   
   '#  grava trailler do arquivo_certificado
   
   If Conta_Linha_certificadoC > 0 Then
      linha = "9" & Format(Conta_Linha_certificadoC, "00000")
      linha = Left(linha + Space(tam_reg), tam_reg)
      Print #fCliente, linha
      Close #fCliente
   End If
    
    'Atualizando dados em arquivo_versao_gerado_tb
    If Conta_Linha_certificadoC > 0 And wNewC Then
       Call InserirArquivoVersaoGerado(Trim(Nome_Rel_Certificado), qtdRegC, Conta_Linha_certificadoC + 2, CInt(Right(Nome_ArqC, 4)))
    End If
    
    rdocn.CommitTrans
   
Exit Sub
   
Erro:
    TrataErroGeral "Processa SEGA8151", Me.name
    TerminaSEGBR

End Sub

Private Sub BuscaDtEmissao()
   
   On Error GoTo Trata_Erro
   
   Sql = "SELECT dt_avaliacao"
   Sql = Sql & " FROM"
   Sql = Sql & " avaliacao_proposta_tb "
   Sql = Sql & " WHERE"
   Sql = Sql & " proposta_id = " & proposta
   Sql = Sql & " and tp_avaliacao_id in (0,6,8)"
     
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      Dt_Emissao = Format(rs!dt_avaliacao, "dd/mm/yyyy")
   Else
      Dt_Emissao = Data_Sistema
   End If
   
   rs.Close
   
   Exit Sub
   
Trata_Erro:
    TrataErroGeral "BuscaDtEmissao SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub BuscaDadosDeclTitular()
   
   Resp1Tit = " "
   Resp2Tit = " "
   Resp3Tit = " "
   Resp4Tit = " "
   Resp5Tit = " "
   Descr1Tit = Space(60)
   Descr2Tit = Space(60)
   Descr3Tit = Space(60)
   Descr4Tit = Space(60)
   Descr5Tit = Space(60)
   
   On Error GoTo ErroBuscaDadosTitular
   
   Sql = "SELECT resp_titular, descricao, item"
   Sql = Sql & " FROM"
   Sql = Sql & " decl_titular_tb a "
   Sql = Sql & " WHERE"
   Sql = Sql & " a.proposta_id = " & proposta
     
   Set rs = rdocn.OpenResultset(Sql)
   
   While Not rs.EOF
        Select Case rs!Item
            Case 1
                  Resp1Tit = rs!resp_titular
                  Descr1Tit = Left(rs!Descricao & Space(60), 60)
            Case 2
                  Resp2Tit = rs!resp_titular
                  Descr2Tit = Left(rs!Descricao & Space(60), 60)
            Case 3
                  Resp3Tit = rs!resp_titular
                  Descr3Tit = Left(rs!Descricao & Space(60), 60)
            Case 4
                  Resp4Tit = rs!resp_titular
                  Descr4Tit = Left(rs!Descricao & Space(60), 60)
            Case 5
                  Resp5Tit = rs!resp_titular
                  Descr5Tit = Left(rs!Descricao & Space(60), 60)
        End Select
        
        rs.MoveNext
        
    Wend
                      
   rs.Close
         
   Exit Sub
   
ErroBuscaDadosTitular:
    TrataErroGeral "BuscaDadosDeclTitular SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub BuscaDadosDeclComplementar()
   
   On Error GoTo ErroBuscaDadosComplementar
   
   Resp1Compl = " "
   Resp2Compl = " "
   Resp3Compl = " "
   Resp4Compl = " "
   Resp5Compl = " "
   Descr1Compl = Space(60)
   Descr2Compl = Space(60)
   Descr3Compl = Space(60)
   Descr4Compl = Space(60)
   Descr5Compl = Space(60)

   Sql = "SELECT resp_complementar, descricao, item"
   Sql = Sql & " FROM"
   Sql = Sql & " decl_complementar_tb a "
   Sql = Sql & " WHERE"
   Sql = Sql & " a.proposta_id = " & proposta
  
   Set rs = rdocn.OpenResultset(Sql)
   
   While Not rs.EOF
   
        Select Case rs!Item
            Case 1
                  Resp1Compl = rs!resp_complementar
                  Descr1Compl = Left(rs!Descricao & Space(60), 60)
            Case 2
                  Resp2Compl = rs!resp_complementar
                  Descr2Compl = Left(rs!Descricao & Space(60), 60)
            Case 3
                  Resp3Compl = rs!resp_complementar
                  Descr3Compl = Left(rs!Descricao & Space(60), 60)
            Case 4
                  Resp4Compl = rs!resp_complementar
                  Descr4Compl = Left(rs!Descricao & Space(60), 60)
            Case 5
                  Resp5Compl = rs!resp_complementar
                  Descr5Compl = Left(rs!Descricao & Space(60), 60)
        End Select
        
        rs.MoveNext
                   
   Wend
      
   rs.Close
   
   Exit Sub
   
ErroBuscaDadosComplementar:
    TrataErroGeral "BuscaDadosDeclComplementar SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Sub AtualizaPedidoReemissao(ByVal pPropostaId, ByVal pNrPedido, ByVal pSituacao)

On Error GoTo Erro

    Sql = "exec reemissao_documento_spu "
    Sql = Sql & pPropostaId & ","
    Sql = Sql & pNrPedido & ",'"
    Sql = Sql & pSituacao & "',"
    Sql = Sql & TpDocCertificado & ",'"
    Sql = Sql & cUserName & "',"
    Sql = Sql & "Null"  'endosso_id n�o ser� atualizado
    rdocn.Execute (Sql)
    
    Exit Sub
   
Erro:
    TrataErroGeral "AtualizaPedidoReemissao SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Atualiza_Evento_Impressao(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal usuario As String)
                                      
Dim Sql         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    Sql = ""
    Sql = Sql & "exec evento_seguros_db..evento_impressao_geracao_spu "
    Sql = Sql & num_solicitacao & ", '" & arquivo_remessa & "', '" & usuario & "' "
    
    Set rsAtualizacao = rdocn.OpenResultset(Sql)
    rsAtualizacao.Close

    Exit Sub
    
Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.name
    TerminaSEGBR
End Sub
'
Sub AtualizaPedidoReemissao2Via(ByVal pPropostaId As Long, ByVal pNrPedido As Long, _
                                ByVal iVersao As Integer, ByVal iLayOut_id As Integer)

On Error GoTo Erro

    Sql = "exec Sol2viaDoctos_atual_Imp_spu "
    Sql = Sql & pPropostaId & ","
    Sql = Sql & pNrPedido & ",'"
    Sql = Sql & Format$(Data_Sistema, "yyyymmdd") & "', "
    Sql = Sql & iLayOut_id & ", "
    Sql = Sql & iVersao & ", '"
    Sql = Sql & cUserName & "' "
    rdocn.Execute (Sql)
    
Exit Sub
   
Erro:
    TrataErroGeral "AtualizaPedidoReemissao-Sol2ViaDoctos SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Produto()

On Error GoTo Erro
   
    Sql = "SELECT nome"
    Sql = Sql & " FROM produto_tb "
    Sql = Sql & " WHERE"
    Sql = Sql & " produto_id = " & Produto
    
    Set rs = rdocn.OpenResultset(Sql)
    
    If Not rs.EOF Then
        NomeProduto = Left(UCase(rs(0)) & Space(50), 50)
    End If
       
    rs.Close
    
Exit Sub

Erro:
   TrataErroGeral "Obtem_Dados_Produto SEGA8151", Me.name
   TerminaSEGBR

End Sub

Private Sub Obtem_Endosso()

On Error GoTo Erro
   
    Sql = "SELECT endosso_id"
    Sql = Sql & " FROM endosso_tb "
    Sql = Sql & " WHERE"
    Sql = Sql & " proposta_id = " & Val(proposta)
    
    Set rs = rdocn.OpenResultset(Sql)
    
    If Not rs.EOF Then
        Endosso = Format(rs(0), "000000000")
    Else
       Endosso = "000000000"
    End If
    
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Obtem_Endosso SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Private Sub Obtem_Dados_Proposta_Adesao()

On Error GoTo Erro
   
    Sql = "SELECT seguradora_cod_susep,"
    Sql = Sql & " sucursal_seguradora_id,"
    Sql = Sql & " ramo_id,"
    Sql = Sql & " apolice_id,"
    Sql = Sql & " cont_banco_id,"
    Sql = Sql & " cont_agencia_id,"
    Sql = Sql & " proposta_bb,"
    Sql = Sql & " proposta_bb_anterior,"
    Sql = Sql & " info_complementar"
    'Barney 09/07/2003 - Inclus�o do NOLOCK
    Sql = Sql & " FROM proposta_adesao_tb  WITH (NOLOCK)  "
    Sql = Sql & " WHERE"
    Sql = Sql & " proposta_id = " & proposta
    
    Set rs = rdocn.OpenResultset(Sql)
    
    If Not rs.EOF Then
       Seguradora = rs(0)
       Sucursal = Format(rs(1), "00")
       ramo = Format(rs(2), "00")
       Apolice = Format(rs(3), "00000000")
       Banco = Format(rs(4), "0000")
       agencia = Format(rs(5), "0000")
       PropostaBB = Format(rs(6), "000000000")
       PropostaBB_ant = Format(rs(7), "000000000")
       Tp_Registro = Format(Trim(rs(8)), "00")
       FlagPropBasica = False
    Else
       FlagPropBasica = True
    End If
       
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Obtem_Dados_Proposta_Adesao SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Private Sub Obtem_Dados_Apolice()

On Error GoTo Erro

    If Seguradora = Seguradora_ant And _
       Sucursal = Sucursal_ant And _
       ramo = Ramo_ant And _
       Apolice = Apolice_ant Then
       Exit Sub
    Else
       Seguradora_ant = Seguradora
       Sucursal_ant = Sucursal
       Ramo_ant = ramo
       Apolice_ant = Apolice
    End If
    
       
    Sql = "SELECT dt_inicio_vigencia"
    Sql = Sql & " FROM"
    Sql = Sql & " apolice_tb "
    Sql = Sql & " WHERE"
    Sql = Sql & " seguradora_cod_susep = " & Seguradora
    Sql = Sql & " and sucursal_seguradora_id = " & Sucursal
    Sql = Sql & " and ramo_id = " & ramo
    Sql = Sql & " and apolice_id = " & Apolice
    
    Set rs = rdocn.OpenResultset(Sql)
    
    If Not rs.EOF Then
       Dt_IniVig = Format(rs(0), "dd/mm/yyyy")
    End If
       
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Obtem_Dados_Apolice SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Function ObterNumRemessa(nome As String, ByRef NumRemessa As String) As Variant

Dim Sql As String
Dim rcNum As rdoResultset
Dim vObterNumRemessa() As Integer
ReDim vObterNumRemessa(0 To 1)
On Error GoTo Erro
    
    Sql = ""
    Sql = Sql & " SELECT"
    Sql = Sql & "     l.layout_id"
    Sql = Sql & " FROM"
    Sql = Sql & "     controle_proposta_db..layout_tb l"
    Sql = Sql & " WHERE"
    Sql = Sql & "     l.nome = '" & nome & "'"
    Set rcNum = rdocn1.OpenResultset(Sql)
    
        If rcNum.EOF Then
           Error 1000
        Else
        vObterNumRemessa(0) = rcNum(0)
        Sql = ""
        Sql = Sql & " SELECT"
        Sql = Sql & "     isnull(max(a.versao), 0)"
        Sql = Sql & " FROM"
        Sql = Sql & "     controle_proposta_db..arquivo_versao_gerado_tb a"
        Sql = Sql & " WHERE"
        Sql = Sql & "     a.layout_id = " & rcNum(0)
        
        rcNum.Close
        
        Set rcNum = rdocn1.OpenResultset(Sql)
        
            If Not rcNum.EOF Then
               NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "0000"), Format(rcNum(0) + 1, "0000"))
               ObterNumRemessa = NumRemessa
            Else
               ObterNumRemessa = Nothing
            End If
        End If
    
    rcNum.Close

Exit Function

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "ObterNumRemessa", nome
    TerminaSEGBR

End Function

Private Sub Obtem_Dados_Agencia()
   
On Error GoTo Erro
   
   If agencia = Agencia_ant Then
      Exit Sub
   Else
      Agencia_ant = agencia
   End If
   
    '  obtem dados da agencia
   Sql = "SELECT nome from agencia_tb  "
   Sql = Sql & " WHERE agencia_id  = " & agencia & " and "
   Sql = Sql & " banco_id = " & Banco
   
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      NomeAgencia = Left(UCase(rs(0)) + Space(25), 25)
   End If
   
   rs.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Agencia SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Corretor()
   
   On Error GoTo Erro
        
   '
   '  obtem dados da agencia
   '
   Sql = "SELECT b.nome, b.ddd, b.telefone"
   Sql = Sql & " FROM  corretagem_tb a, corretor_tb b"
   Sql = Sql & " WHERE proposta_id  = " & proposta
   Sql = Sql & " and a.corretor_id = b.corretor_id "
   
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      NomeCorretor = Left(UCase(rs(0)) + Space(40), 40)
      If Not IsNull(rs(1)) Then
         TelCorretor = "(" & rs(1) & ") " & rs(2)
         TelCorretor = Left(TelCorretor + Space(15), 15)
      Else
         TelCorretor = Left(rs(2) + Space(15), 15)
      End If
   Else
      NomeCorretor = Space(40)
      TelCorretor = Space(15)
   End If
   
   rs.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Corretor SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Escolha_Plano()

On Error GoTo Erro
   
    Sql = "SELECT a.imp_segurada,"
    Sql = Sql & " a.val_premio,"
    Sql = Sql & " a.plano_id,"
    Sql = Sql & " b.tp_plano_id"
    Sql = Sql & " FROM escolha_plano_tb a, plano_tb b "
    Sql = Sql & " WHERE a.proposta_id = " & proposta
    Sql = Sql & " and a.plano_id = b.plano_id"
    Sql = Sql & " and a.produto_id = b.produto_id"
    Sql = Sql & " and a.dt_fim_vigencia is null "
      
    Set rs = rdocn.OpenResultset(Sql)
   
    If Not rs.EOF Then
        
        'Obtendo importancia segurada e premio
        
        If ConfiguracaoBrasil Then
            Imp_Seg = Val(rs(0))
            Val_Premio = Val(rs(1))
        Else
            Imp_Seg = rs(0)
            Val_Premio = rs(1)
        End If
        
        'Formatando valor do premio para exibi��o
        
        Val_Premio = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val_Premio))
        Val_Premio = Left(Val_Premio & Space(14), 14)
        
        'Obtendo plano e tipo de plano
        
        Plano = Format(rs(2), "0000")
        TpPlano = rs(3)
    
    End If
    
    rs.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Escolha_Plano SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Function FormataValorParaPadraoBrasil(valor As String) As String
   
   On Error GoTo Erro
   
   ConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
   
   'Convertendo valor do d�bito para o padr�o brasileiro
   
   If ConfDecimal = "." Then
      valor = TrocaValorAmePorBras(Format(Val(valor), "###,###,##0.00"))
   Else
      valor = Format(Val(valor), "###,###,##0.00")
   End If

   FormataValorParaPadraoBrasil = valor
   
   Exit Function
Erro:
   TrataErroGeral "FormataValorParaPadraoBrasil SEGA8151", Me.name
   TerminaSEGBR
   
End Function

Private Sub Obtem_Dados_Coberturas()
   
On Error GoTo Erro
   
    If TpPlano = TpPlano_ant Then
       Exit Sub
    Else
       TpPlano_ant = TpPlano
    End If
    
    Sql = "SELECT distinct"
    Sql = Sql & " c.lim_max_cobertura,"
    Sql = Sql & " c.tp_componente_id,"
    Sql = Sql & " c.tp_cobertura_id "
    Sql = Sql & " FROM"
    Sql = Sql & " tp_plano_tp_comp_tb a,"
    Sql = Sql & " tp_cob_comp_plano_tb b,"
    Sql = Sql & " tp_cob_comp_tb c"
    Sql = Sql & " WHERE"
    Sql = Sql & " a.tp_plano_id = " & TpPlano
    Sql = Sql & " and a.tp_plano_id = b.tp_plano_id"
    Sql = Sql & " and b.tp_cob_comp_id = c.tp_cob_comp_id"
    Sql = Sql & " ORDER BY"
    Sql = Sql & " c.tp_componente_id, c.tp_cobertura_id"
    
    Set rs = rdocn.OpenResultset(Sql)
    
    While Not rs.EOF
    
       If ConfiguracaoBrasil Then
          valor = Val(rs(0))
       Else
          valor = rs(0)
       End If
       
       Select Case rs(2)
            Case 5
               ' 38 (desenvolvimento) morte natural
               ' 1 = titular   3 = conjuge (produ�ao)  13 (titular) e 14(conjuge) (desenvolvimento)
               ' 13 = titular  14 =conjuge (desenvolvimento)
               If rs(1) = 1 Then
                  PercCobTit1 = valor / 100
               Else
                  PercCobConj1 = valor / 100
               End If
            Case 2 '39(desenvolvimento) morte acidental
               If rs(1) = 1 Then
                  PercCobTit2 = valor / 100
               Else
                  PercCobConj2 = valor / 100
               End If
            Case 3 ' 46 (desenvolvimento) invalidez permanente total
               If rs(1) = 1 Then
                  PercCobTit3 = valor / 100
               Else
                  PercCobConj3 = valor / 100
               End If
            Case 4, 242 ' 47(desenvolvimento) invalidez permanente por doen�a
               If rs(1) = 1 Then
                  PercCobTit4 = valor / 100
               Else
                  PercCobConj4 = valor / 100
               End If
       End Select
    
    rs.MoveNext
    
    Wend
    
    rs.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Coberturas SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Conjuge()
   
On Error GoTo Erro
   
   Sql = "SELECT"
   Sql = Sql & " c.nome,"
   Sql = Sql & " b.cpf"
   Sql = Sql & " FROM"
   Sql = Sql & " proposta_complementar_tb a,"
   Sql = Sql & " pessoa_fisica_tb b,"
   Sql = Sql & " cliente_tb c"
   Sql = Sql & " WHERE"
   Sql = Sql & " a.proposta_id = " & proposta
   Sql = Sql & " and a.prop_cliente_id = b.pf_cliente_id"
   Sql = Sql & " and cliente_id = prop_cliente_id"
   Sql = Sql & " and a.situacao <> 'c'"
     
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      Nome_Conj = Left(rs(0) + Space(50), 50)
      CPF_Conj = rs(1)
      CPF_Conj = Left(CPF_Conj, 3) & "." & Mid(CPF_Conj, 4, 3) & "." & Mid(CPF_Conj, 7, 3) & "/" & Right(CPF_Conj, 2)
      Componente_Conjuge = "ok"
   Else
      Nome_Conj = String(50, "*")
      CPF_Conj = String(14, "*")
      Componente_Conjuge = ""
   End If
   
   rs.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Conjuge SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Verifica_Certificado()
   
On Error GoTo Erro
   
   Sql = "SELECT proposta_id "
   Sql = Sql & " FROM certificado_tb"
   Sql = Sql & " WHERE proposta_id = " & proposta & " and dt_fim_vigencia is null"
     
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      Observacao = "ESTE CERTIFICADO SUBSTITUI O ANTERIOR"
      Observacao = Left(Observacao + Space(50), 50)
   End If
   
   rs.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Verifica_Certificado SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Public Function InserirArquivoVersaoGerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    On Error GoTo Erro
            
    Sql = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    Sql = Sql & nome & "'," & NumRemessa & "," & qReg & ",'"
    Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    Sql = Sql & cUserName & "'"
    rdocn.Execute (Sql)
    
    Exit Function

Erro:
    TrataErroGeral "InserirArquivoVersaoGerado", nome
    TerminaSEGBR
    
End Function

Public Function ConverteParaJulianDate(ldate As Date) As String
   
   Dim lJulianDate As String * 5
   
   On Error GoTo Trata_Erro
 
   lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
   lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
   ConverteParaJulianDate = lJulianDate
 
   Exit Function
 
Trata_Erro:
   Call TrataErroGeral("ConverteParaJulianDate", "modSEGP0241")
 
End Function

Private Sub Obtem_Numero_Certificado()

On Error GoTo Erro
   
    Sql = ""
    Sql = Sql & " SELECT TOP 1 certificado_id"
    Sql = Sql & " FROM certificado_tb"
    Sql = Sql & " WHERE proposta_id = " & proposta
    Sql = Sql & " ORDER BY dt_inclusao desc "
    
    Set rs = rdocn.OpenResultset(Sql)
    
    If Not rs.EOF Then
       Certificado = Format(rs(0), "00000000")
       DvCertificado = Modulo11(Certificado)
    Else
       Certificado = "00000000"
       DvCertificado = "0"
    End If
       
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Obtem_Numero_Certificado SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Private Function Abre_Arquivo() As Variant
  
  On Error GoTo Erro
    Dim vAbre_arquivo As Variant
    Dim tam_reg       As Integer
    
    wFirst = True
    retensao = ""
    DataHora = Trim(Format(Data_Sistema, "dd/mm/yyyy hh:mm AMPM")) 'joconceicao 11/jul/01
    DataHora = DataHora & "  "
    Data = Format(Data_Sistema, "ddmmyyyy")
        
    tam_reg = 330
    
    DestinoA = Left("AGENC" & Space(20), 20)
    DestinoC = Left("CLIEN" & Space(20), 20)
    DestinoD = Left("ALIAN" & Space(20), 20)
     
    wNewC = True: wNewA = False: wNewD = False
     
    vAbre_arquivo = ObterNumRemessa(Trim(Nome_Rel_Certificado), NumRemessa)
    NumRemessa = Format(Val(NumRemessa), "000000")
    Nome_ArqC = Certificado_path & Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000") 'Data & ".txt"
    QualRemessa = Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000")
    fCliente = FreeFile
    Open Nome_ArqC For Output As fCliente
    
    'imprime o header
    linha = "1" & Nome_Rel_Certificado & DataHora & "A4   " & DestinoC & NumRemessa & gTituloSegundaVia
    linha = Left(linha + Space(tam_reg), tam_reg)
    Print #fCliente, linha
    
    txtArqCertificado = Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000") 'Data & ".txt"
    
    Abre_Arquivo = vAbre_arquivo
        
    Exit Function
    
Erro:
   TrataErroGeral "Abre_Arquivo SEGA8151", Me.name
   TerminaSEGBR
   
End Function

Private Sub Insere_Certificado()

On Error GoTo Erro
   
    Sql = "exec certificado_spi "
    Sql = Sql & Certificado & ", "
    Sql = Sql & Apolice & ", "
    Sql = Sql & Sucursal & ", "
    Sql = Sql & Seguradora & ", "
    Sql = Sql & ramo & ", "
    Sql = Sql & proposta & ", "
    Sql = Sql & "null" & ", "
    Sql = Sql & "null" & ", '"
    Sql = Sql & Format(DtInicioVigencia, "yyyymmdd") & "', "
    Sql = Sql & "null" & ", '"
    Sql = Sql & cUserName & "'"
    
    Set rs = rdocn.OpenResultset(Sql)
      
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Insere_Certificado SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Private Sub Mascara_Valores_Zerados()
  
  On Error GoTo Erro
  
  If Trim(IS1_Tit) = "0,00" Then
     IS1_Tit = String(14, "*")
  End If
  
  If Trim(IS2_Tit) = "0,00" Then
     IS2_Tit = String(14, "*")
  End If
  
  If Trim(IS3_Tit) = "0,00" Then
     IS3_Tit = String(14, "*")
  End If
  
  If Trim(IS4_Tit) = "0,00" Then
     IS4_Tit = String(14, "*")
  End If
  
  If Trim(IS1_Conj) = "0,00" Then
     IS1_Conj = String(14, "*")
  End If
  
  If Trim(IS2_Conj) = "0,00" Then
     IS2_Conj = String(14, "*")
  End If
  
  If Trim(IS3_Conj) = "0,00" Then
     IS1_Conj = String(14, "*")
  End If
  
  If Trim(IS4_Conj) = "0,00" Then
     IS1_Conj = String(14, "*")
  End If
  
  Exit Sub
Erro:
   TrataErroGeral "Mascara_Valores_Zerados SEGA8151", Me.name
   TerminaSEGBR
  
End Sub

Function Modulo11(ByVal numero As String) As String

    Dim Ponteiro_Numero As Integer
    Dim Multiplicador As Integer
    Dim Somatoria As Integer
    Dim resto As Integer

    On Error GoTo Erro
    
    numero = Right(String(50, "*") + Trim(numero), 50)
    
    Ponteiro_Numero = 50
    
    Multiplicador = 2
    
    Somatoria = 0
    
    While Ponteiro_Numero > 0 And Mid$(numero, Ponteiro_Numero, 1) <> "*"
        Somatoria = Somatoria + (Val(Mid$(numero, Ponteiro_Numero, 1)) * Multiplicador)
        
        Ponteiro_Numero = Ponteiro_Numero - 1
        
        Multiplicador = Multiplicador + 1
        If Multiplicador = 10 Then
            Multiplicador = 2
        End If
    Wend
    
    'Resto = Int(Somatoria / 11)
    'Resto = Somatoria - (Resto * 11)

    resto = Somatoria Mod 11
    
    If 11 - resto > 9 And resto <> 0 Then
        Digito_Verificador = "X"
    Else
        If resto = 0 Then
            Digito_Verificador = "0"
        Else
            Digito_Verificador = Trim(Str(11 - resto))
        End If
    End If

    Modulo11 = Digito_Verificador
    
    Exit Function
Erro:
   TrataErroGeral "Modulo11 SEGA8151", Me.name
   TerminaSEGBR
    
End Function

Sub Conexao_auxiliar()
   
On Error GoTo Erro

    With rdocn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 6000
        .EstablishConnection rdDriverNoPrompt
    End With
      
Exit Sub

Erro:
    TrataErroGeral "Conex�o com servidor indispon�vel SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Private Function getArquivoDocumento(arquivo_saida As String) As String
Dim Sql As String
Dim rc As rdoResultset
 
getArquivoDocumento = ""
 
Sql = " Select  tp_documento_id "
Sql = Sql & " from evento_seguros_db..documento_tb "
Sql = Sql & " where arquivo_saida = '" & Trim(arquivo_saida) & "'"
 
Set rc = rdocn.OpenResultset(Sql)
While Not rc.EOF
    getArquivoDocumento = IIf(Trim(getArquivoDocumento) = "", rc!tp_documento_id, getArquivoDocumento & "," & rc!tp_documento_id)
    rc.MoveNext
Wend
 
End Function

Sub Obtem_Endereco_Agencia()
   
   Dim mun_id As Integer
   
   On Error GoTo Erro
   '
   '  obtem endereco da agencia
   '
   Sql = "       SELECT"
   Sql = Sql & "   b.endereco,"
   Sql = Sql & "   b.cep,"
   Sql = Sql & "   b.estado,"
   Sql = Sql & "   b.municipio_id "
   Sql = Sql & "FROM"
   Sql = Sql & "   proposta_adesao_tb a,"
   Sql = Sql & "   agencia_tb b "
   Sql = Sql & "WHERE"
   Sql = Sql & " a.proposta_id = " & proposta
   Sql = Sql & " and a.cont_agencia_id = b.agencia_id "
   Sql = Sql & " and b.banco_id = a.cont_banco_id "
   Set rs = rdocn.OpenResultset(Sql)
   
   If Not rs.EOF Then
      
      If IsNull(rs(0)) Or Trim(rs(0)) = "" Then
         Call Grava_Log_Agencia(agencia, proposta)
         EnderecoIncompleto = True
         Exit Sub
      Else
         Endereco_ag = Left(UCase(rs(0)) + Space(50), 50)
      End If
      
     ' jorfilho/joconceicao - 12/jul/01 - trata cep para o rel017
      If IsNull(rs(1)) Or Len(Trim(rs(1))) <> 8 Or InStr(1, rs(1), "-") <> 0 Then
         Call Grava_Log_Agencia(agencia, proposta)
         EnderecoIncompleto = True
         Exit Sub
      Else
         CEP_ag = Format(rs(1), "00000000")
      End If

      UF_ag = rs(2)
      If rs(3) = "999" Then
         Call Grava_Log_Agencia(agencia, proposta)
         EnderecoIncompleto = True
         Exit Sub
      Else
         mun_id = rs(3)
      End If
   
   End If
   
   rs.Close
   
   'Obtem municipio da agencia
   Sql = "       SELECT"
   Sql = Sql & "   nome "
   Sql = Sql & "FROM"
   Sql = Sql & "   municipio_tb "
   Sql = Sql & "WHERE"
   Sql = Sql & "   municipio_id = " & mun_id
   Sql = Sql & "   and estado = '" & UF_ag & "'"
   Set rs = rdocn.OpenResultset(sql1)
   
   If Not rs.EOF Then
      Municipio_ag = Left(UCase(rs(0)) + Space(45), 45)
   End If
   
   rs.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Endereco_Agencia SEGA8151", Me.name
    TerminaSEGBR
    
End Sub

Sub Grava_Log_Agencia(agencia As String, proposta As String)

   Dim LogArquivo As Integer
   Dim nomearq As String
   
   On Error GoTo Erro
   
   If NumRemessa = "" Then
       Call ObterNumRemessa(Trim(Nome_Rel_Certificado), NumRemessa)
   End If
   LogArquivo = FreeFile
   nomearq = "Log" & Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000")
   Open logpath & nomearq For Append As LogArquivo
   linha = "Dados de Endere�o e/ou Cidade e/ou CEP da Agencia no." & agencia & ", incompleto(s) para a proposta " & proposta & "."
   Print #LogArquivo, linha
   Close #LogArquivo
   
   Exit Sub
   
Erro:
   TrataErroGeral "Grava_Log_Agencia SEGA8151", Me.name
   TerminaSEGBR
   
End Sub

Private Sub Form_Load()

 On Error GoTo Erro
  
    ConfiguracaoBrasil = True
    
    Me.Caption = "Gera Arquivo Emiss�o de Documentos" & " - " & Ambiente
    
    Call cmdOk_Click
    
    Unload Me
    Call TerminaSEGBR
    
    Exit Sub
    
Erro:
    TrataErroGeral "Form_Load SEGA8151", Me.name
    TerminaSEGBR
    
End Sub


