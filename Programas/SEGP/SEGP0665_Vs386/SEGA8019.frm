VERSION 5.00
Begin VB.Form frmSEGA8019 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Resumo da Emiss�o de Ap�lices/Endossos RE  "
   ClientHeight    =   5745
   ClientLeft      =   1200
   ClientTop       =   1575
   ClientWidth     =   7125
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5745
   ScaleWidth      =   7125
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   3255
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   6885
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   20
         Top             =   2520
         Width           =   855
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   1890
         Width           =   855
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   2520
         Width           =   5265
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   1890
         Width           =   5265
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   14
         Top             =   1230
         Width           =   5265
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1230
         Width           =   855
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   600
         Width           =   5265
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   3
         Left            =   5820
         TabIndex        =   24
         Top             =   2250
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   2
         Left            =   5820
         TabIndex        =   23
         Top             =   1620
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Endossos Alian�a:"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   22
         Top             =   2250
         Width           =   3525
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Ap�lice Alian�a:"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   21
         Top             =   990
         Width           =   3525
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Endosso Cliente:"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   16
         Top             =   1620
         Width           =   3525
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   1
         Left            =   5820
         TabIndex        =   15
         Top             =   960
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   0
         Left            =   5820
         TabIndex        =   11
         Top             =   330
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Ap�lice Cliente:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   3525
      End
   End
   Begin VB.CommandButton cmdCanc 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5760
      TabIndex        =   6
      Top             =   4905
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   4470
      TabIndex        =   5
      Top             =   4905
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6885
      Begin VB.TextBox txtfim 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox txtIni 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Fim........................."
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "In�cio....................."
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.PictureBox StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      ScaleHeight     =   195
      ScaleWidth      =   7065
      TabIndex        =   12
      Top             =   5490
      Width           =   7125
   End
End
Attribute VB_Name = "frmSEGA8019"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Conex�es auxiliares
'Dim rdocn1              As New rdoConnection          'comentado em 23/09/2003
'Dim rdocn2              As New rdoConnection
'Dim rdocn3              As New rdoConnection
Dim arquivo_remessa     As String

'Vari�veis
Const TpRamoRE = 2
Dim Carta_path          As String
' Dim Arquivo             As Integer
Dim arquivo1            As Integer
Dim arquivo2            As Integer
Dim arquivo3            As Integer
Dim arquivo4            As Integer
Dim Arq1                As Integer
Dim ContaLinhaAtual     As Long
Dim arq As Integer
Dim tam_reg             As Integer
Const Rel_Apolice = "APL201"
Const Rel_Endosso = "APL202"

Dim num_proposta        As String
Dim ContAgencia         As String
Dim num_solicitacao     As Integer
Dim num_apolice         As String
Dim processo_susep      As String
Dim Reg                 As String
Dim dtIniVigencia       As String
Dim ContaLinha1         As Long
Dim ContaLinha2         As Long
Dim ContaLinha3         As Long
Dim ContaLinha4         As Long
Dim ProdutoId           As Integer
Dim NomeProduto         As String
Dim num_endosso         As String
Dim Subramo             As String
Dim propostaAnterior    As Double
Dim NumRemessaApolice1  As String
Dim NumRemessaApolice2  As String
Dim NumRemessaEndosso1  As String
Dim NumRemessaEndosso2  As String
Dim giLayOut_id         As Integer
Dim QtdReg1             As Long
Dim QtdReg2             As Long
Dim QtdReg3             As Long
Dim QtdReg4             As Long
Dim Flag_2via           As Boolean

Dim SQL                 As String
Dim sql1                As String
Dim rc                  As rdoResultset
Dim Rc1                 As rdoResultset
Dim rc2                 As rdoResultset

Dim NumRegs             As Long
Dim rc_apl              As rdoResultset
Dim ValTotDesconto      As Double
Dim INI                 As String
Dim Fim                 As String
Dim mes                 As String
Dim IniVig              As String
Dim FimVig              As String
Dim ConfiguracaoBrasil  As Boolean
Dim produto_externo_id  As Integer
Dim LinhasCoberturas    As Long
Dim MoedaSeguro         As String
Dim MoedaPremio         As String
Dim NomeMoedaSeguro     As String
Dim NomeMoedaPremio     As String
Dim MoedaAtual          As String
Dim MoedaSeguroId       As String


'** Cobran�a
Dim Sacado_1            As String
Dim Sacado_2            As String
Dim Sacado_3            As String

Dim ArquivoCBR          As Integer
Dim Nosso_Numero        As String
Dim Nosso_numero_dv     As String
Dim Carteira            As String
Dim Val_Cobranca        As Double
Dim agencia             As String
Dim Codigo_Cedente      As String
Dim linha_digitavel     As String
Dim codigo_barras       As String
Dim Rel_cobranca        As String

Dim sDecimal            As String
Dim TpEmissao           As String
Dim DtInicioVigencia    As String
Dim DtEmissao           As Date
Dim TabEscolha          As String

Dim Seguradora          As String
Dim Sucursal            As String
Dim ramo_id             As String
Dim EnviaCliente        As Boolean
Dim EnviaCongenere      As Boolean
Dim QtdVias             As Byte
Dim CoberturasPrimPagina   As Boolean

Dim QtdCoberturas       As Long
Dim QtdLinhasCobertura  As Long
Dim Cobertura()         As String
Dim EnderecoRisco()     As String
Dim QtdObjetos          As Long
Dim Benef()             As String
Dim QtdBenefs           As Long
Dim Congenere()         As String
Dim QtdCongeneres       As Long
Dim CoberturaTransporte() As String

Dim Pagamentos          As New Collection
Dim CoberturasTransp    As New Collection
Dim Clausulas           As New Collection
Dim TranspInternacional As Boolean
Dim Verba               As New Collection

Dim atividadePrincipal                  As String
Dim PagoAto                             As Boolean
Dim ParcelaUnica                        As Boolean

Dim EndossoAnexo                        As Boolean
Dim EndossoDescricao                    As New Collection
Dim DescrEndossoBoleta(1)               As String
Dim tpEndossoId                         As Integer

Dim TextoCoberturasAnexo                As Boolean
Dim CoberturasProdutoAnexo              As Boolean
Dim ImprimeClienteBoleta                As Boolean

Dim Nome_Arq1 As String, Nome_Arq2      As String
Dim Nome_Arq3 As String, Nome_Arq4      As String
Dim conta_corrente_id                   As String
Dim agencia_id                          As String

Dim TotRegProcessados                   As Long
Dim dt_agendamento                      As String

Dim wNew              As Boolean
Dim Tinha             As Boolean
Dim wLinha            As String
Dim wFirst            As Boolean
Dim QualRemessa       As String
Dim aArquivo          As String
Dim Destino_id        As String
Dim Diretoria_id      As String
Dim nFile             As String
Dim flagEnderecoAgencia As Boolean

Dim colAtualiza As Collection

' ARQUIVO era vari�vel; devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o
' (Jo�o Mac-Cormick - 30/6/2000)
Private Function Arquivo() As Integer
  If TpEmissao = "A" Then
      Arquivo = Se(EnviaCliente, arquivo1, arquivo2)
  Else
      Arquivo = Se(EnviaCliente, arquivo3, arquivo4)
  End If
End Function

Sub Atualiza_pagamento(ByVal num_proposta, num_cobranca, num_via)

'** Cobran�a
    
Dim rc_Atualiza As rdoResultset
Dim SQL As String
   
On Error GoTo Erro
SQL = Ambiente & ".emissao_CBR_spu " _
      & num_proposta _
      & ", " & num_cobranca _
      & ", " & num_via _
      & ", '" & Format(Data_Sistema, "yyyymmdd") & "'" _
      & ", '" & cUserName & "'"
Set rc_Atualiza = rdocn.OpenResultset(SQL)
rc_Atualiza.Close
Exit Sub

Erro:
   TrataErroGeral "Atualiza_pagamento", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   TerminaSEGBR

End Sub


Private Function Buscar_cedente(ByVal banco_id, agencia_id, conta_corrente_id As String)
 
'** Cobran�a
 
    Dim rc As rdoResultset
    
    SQL = "SELECT b.nome " _
        & " FROM conta_convenio_seg_tb a (nolock), seguradora_tb b (nolock) " _
        & " WHERE a.agencia_id = " & agencia_id _
        & "   and a.banco_id = " & banco_id _
        & "   and a.conta_corrente_id = " & conta_corrente_id _
        & "   and b.seguradora_cod_susep = a.seguradora_cod_susep"
    Set rc = rdocn2.OpenResultset(SQL)
    
    If rc.EOF Then
       rc.Close
       SQL = "SELECT nome " _
           & " FROM conta_transitoria_corretora_tb a (nolock), corretor_tb b (nolock) " _
           & " WHERE a.agencia_id = " & agencia_id _
           & "   and a.banco_id = " & banco_id _
           & "   and a.conta_corrente_id = " & conta_corrente_id _
           & "   and b.corretor_id = a.corretor_id"
       Set rc = rdocn2.OpenResultset(SQL)
    End If
    
    Buscar_cedente = Left(rc(0) + Space(60), 60)
    rc.Close
 
End Function

Private Function calcula_dv_agencia_cc(ByVal Parte As String) As String
 
Dim i As Integer
'** Cobran�a
 
     Dim Peso As Integer
     Dim Soma As Integer
     Dim Parcela As Integer
     Dim dv As Integer
     Dim result As String
     
     Peso = 9
     Soma = 0
     For i = Len(Parte) To 1 Step -1
       Parcela = Peso * Val(Mid(Parte, i, 1))
       Soma = Soma + Parcela
       Peso = Peso - 1
       If Peso < 2 Then Peso = 9
     Next i
    
     dv = (Soma Mod 11)
     If dv = 10 Then
        result = "X"
     Else
        result = Format(dv, "0")
     End If
     calcula_dv_agencia_cc = result

End Function

Private Function calcula_mod10(ByVal Parte As String) As String
Dim i As Integer, dv As Long

'** Cobran�a
 
    Dim Peso As Integer
    Dim Soma As Integer
    Dim Parcela As Integer
    
    Peso = 2
    Soma = 0
    For i = Len(Parte) To 1 Step -1
      Parcela = Peso * Val(Mid(Parte, i, 1))
      If Parcela > 9 Then
         Parcela = Val(Mid(Format(Parcela, "00"), 1, 1)) + Val(Mid(Format(Parcela, "00"), 2, 1))
      End If
      Soma = Soma + Parcela
      If Peso = 2 Then Peso = 1 Else Peso = 2
    Next i
    
    dv = 10 - (Soma Mod 10)
    If dv > 9 Then dv = 0
    calcula_mod10 = Format(dv, "0")

End Function


Private Function calcula_mod11(ByVal Parte As String) As String
 
Dim i As Integer
'** Cobran�a
 
     Dim Peso As Integer
     Dim Soma As Integer
     Dim Parcela As Integer
     Dim dv As Integer
     
     Peso = 2
     Soma = 0
     For i = Len(Parte) To 1 Step -1
       If i <> 5 Then
          Parcela = Peso * Val(Mid(Parte, i, 1))
          Soma = Soma + Parcela
          Peso = Peso + 1
          If Peso > 9 Then Peso = 2
       End If
     Next i
    
     dv = 11 - (Soma Mod 11)
     If dv = 10 Or dv = 11 Then dv = 1
     calcula_mod11 = Format(dv, "0")

End Function

Sub Fecha_Arquivo()

On Error Resume Next
If arquivo1 <> 0 Then
   Kill Nome_Arq1 & arquivo1 & ".txt"
   Kill Nome_Arq1 & arquivo2 & ".txt"
   Kill Nome_Arq1 & arquivo3 & ".txt"
   Kill Nome_Arq1 & arquivo4 & ".txt"
End If

End Sub

Sub Ler_Congeneres()
Dim SQL As String, rs As rdoResultset, aux As String, i As Long
ReDim Congenere(2, 10)

On Error GoTo Erro
QtdCongeneres = 0
SQL = "SELECT a.perc_participacao, b.nome FROM co_seguro_repassado_tb a (nolock) "
SQL = SQL & "   INNER JOIN seguradora_tb b (nolock)"
SQL = SQL & "   ON a.rep_seguradora_cod_susep=b.seguradora_cod_susep "
SQL = SQL & "   WHERE a.apolice_id=" & num_apolice
SQL = SQL & "   AND a.seguradora_cod_susep=" & Seguradora
SQL = SQL & "   AND a.sucursal_seguradora_id=" & Sucursal
SQL = SQL & "   AND a.ramo_id=" & ramo_id
If TpEmissao = "A" Then
   SQL = SQL & " AND (endosso_id=0 or endosso_id is null) "
Else
   SQL = SQL & "   AND dt_inicio_participacao<='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
   SQL = SQL & "   AND (dt_fim_participacao>='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
   SQL = SQL & "   OR dt_fim_participacao is null )"
'   Sql = Sql & " AND endosso_id=" & num_endosso
End If
'Sql = Sql & "   AND dt_inicio_participacao<='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_participacao is null "
Set rs = rdocn.OpenResultset(SQL)
i = 0
If Not rs.EOF Then
   QtdLinhasCobertura = QtdLinhasCobertura + 1
   Do While Not rs.EOF
      If QtdCongeneres Mod 10 = 0 Then ReDim Preserve Congenere(2, QtdCongeneres + 10)
      Congenere(0, i) = rs!nome
      Congenere(1, i) = Format(Val(rs!perc_participacao), "##0.00")
      QtdLinhasCobertura = QtdLinhasCobertura + 1
      QtdCongeneres = QtdCongeneres + 1
      i = i + 1
      rs.MoveNext
   Loop
   rs.Close
   'para pular uma linha
   QtdLinhasCobertura = QtdLinhasCobertura + 1
End If
Exit Sub
Erro:
   TrataErroGeral "Ler_Congeneres", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   TerminaSEGBR
   
End Sub

Sub Ler_Beneficiarios()
Dim SQL As String, rs As rdoResultset, i As Long
ReDim Benef(2, 10)

On Error GoTo Erro
'Seleciona Benefici�rios
SQL = "SELECT cod_objeto_segurado, nome FROM seguro_item_benef_tb (nolock) WHERE proposta_id=" & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (endosso_id=0 OR endosso_id is null )"
Else
   SQL = SQL & " AND endosso_id=" & num_endosso
End If
Set rs = rdocn.OpenResultset(SQL)
i = 0: QtdBenefs = 0
Do While Not rs.EOF
   If QtdBenefs Mod 10 = 0 Then
      ReDim Preserve Benef(2, QtdBenefs + 10)
   End If
   Benef(0, i) = rs!cod_objeto_segurado
   Benef(1, i) = ("" & rs!nome)
   rs.MoveNext
   i = i + 1
   QtdBenefs = QtdBenefs + 1
   QtdLinhasCobertura = QtdLinhasCobertura + 1
Loop

Exit Sub
Erro:
   TrataErroGeral "Rotina: Ler_Beneficiarios", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   TerminaSEGBR
   
End Sub


Sub Ler_DescricaoEndosso()
Dim SQL As String, rs As rdoResultset, linha As Long, DescrEndosso As String
Dim ultQuebra As Long, Ultpos As Long, i As Long, aux As String
Dim RegClausula As Integer, nDescrEndosso As New DescrEndosso

On Error GoTo Erro
ImprimeClienteBoleta = False

DescrEndossoBoleta(0) = Space(60)
DescrEndossoBoleta(1) = Space(60)
SQL = "SELECT descricao_endosso FROM endosso_tb (nolock) "
SQL = SQL & "   WHERE proposta_id=" & num_proposta
SQL = SQL & "   AND   endosso_id=" & num_endosso
Set rs = rdocn.OpenResultset(SQL)
If Not rs.EOF Then
   linha = 1 'Linha em branco
   ReDim Endosso(17)
   DescrEndosso = Formata_Clausula("" & rs(0))
   ultQuebra = 1
   For i = 1 To Len(DescrEndosso)
      If Mid(DescrEndosso, i, 1) = Chr(13) Then
         linha = linha + 1
         If linha Mod 17 = 0 Then
            ReDim Preserve Endosso(UBound(Endosso) + 17)
         End If
         aux = Mid(DescrEndosso, ultQuebra, i - ultQuebra)
         If i < Len(DescrEndosso) Then
            If Mid(DescrEndosso, i + 1, 1) = Chr(10) Then
               i = i + 1
            End If
         End If
         With nDescrEndosso
            .DescrEndosso = aux
         End With
         EndossoDescricao.Add nDescrEndosso

         If tpEndossoId = 93 And linha < 4 Then
               If InStr(1, UCase(aux), "CLIENTE:") Then
                  DescrEndossoBoleta(linha - 2) = Left(aux & Space(60), 60)
                  ImprimeClienteBoleta = True
               End If
         End If

         Set nDescrEndosso = Nothing
         ultQuebra = i + 1
      End If
      Ultpos = Ultpos + 1
   Next
   If ultQuebra < i Then 'Ent�o ainda falta texto p/ ser impresso depois da �ltima quebra
      If UBound(Endosso) <= linha Then
         ReDim Preserve Endosso(UBound(Endosso) + 1)
      End If
      linha = linha + 1
      aux = Mid(DescrEndosso, ultQuebra, i - ultQuebra)
      If tpEndossoId = 93 And linha < 4 Then
            If InStr(1, UCase(aux), "CLIENTE:") Then
               DescrEndossoBoleta(linha - 1) = Left(aux & Space(60), 60)
               ImprimeClienteBoleta = True
            End If
      End If
     
      With nDescrEndosso
         .DescrEndosso = aux
      End With
      EndossoDescricao.Add nDescrEndosso
      Set nDescrEndosso = Nothing
   End If

End If
rs.Close
Exit Sub
Erro:
    TrataErroGeral "Ler_DescricaoEndosso", Me.Name
    On Error Resume Next
    Call Fecha_Arquivo
    TerminaSEGBR
    
End Sub
Sub Processa_DescricaoEndosso()

Dim vDescrEndosso As DescrEndosso, RegClausula As Byte
If EndossoDescricao.Count > 16 Then 'imprimir junto com as cl�usulas
   RegClausula = 22
Else
   'Se n� de linhas � menor que 17 e as coberturas n�o foram listadas na primeira p�gina
   If QtdLinhasCobertura > 16 Or (CoberturasProdutoAnexo) Then
      RegClausula = 21
   ElseIf ((QtdLinhasCobertura + EndossoDescricao.Count + 1) <= 16) And (Not CoberturasProdutoAnexo) Then 'se cabe a descri��o junto com as coberturas(pulando 1 linha)
      RegClausula = 21
   ElseIf QtdLinhasCobertura = 0 Then
      RegClausula = 21
   Else
      RegClausula = 22
   End If
End If
If RegClausula = 22 Then
   EndossoAnexo = True 'Imprimiu endosso no anexo
   If QtdLinhasCobertura = 0 Then
      'Se endosso n�o cabe na p�g. da frente e n�o tem coberturas ent�o ...
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(24) & String(16, "*")
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(25) & "CONFORME ANEXO"
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(24) & String(16, "*")
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
End If
If EndossoDescricao.Count > 0 Then
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   If RegClausula = 21 Then QtdLinhasCobertura = QtdLinhasCobertura + 1
   For Each vDescrEndosso In EndossoDescricao
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & vDescrEndosso.DescrEndosso
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      If RegClausula = 21 Then QtdLinhasCobertura = QtdLinhasCobertura + 1
   Next
'   For i = 1 To linha
'      If tpEndossoId = 93 And i < 3 Then
'         DescrEndossoBoleta(i - 1) = Left(Endosso(i) & Space(60), 60)
'      End If
'      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'      Reg = Reg & Endosso(i)
'      Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'      IncrementaLinha
'      If RegClausula = 21 Then QtdLinhasCobertura = QtdLinhasCobertura + 1
'   Next
End If

End Sub

Sub Ler_MoedaAtual()
Dim rs As rdoResultset

SQL = "SELECT VAL_PARAMETRO FROM PS_PARAMETRO_TB (nolock) WHERE PARAMETRO='MOEDA ATUAL'"
Set rs = rdocn.OpenResultset(SQL)
If Not rs.EOF Then
   MoedaAtual = rs(0)
End If
rs.Close
End Sub

' CONTA_LINHA era vari�vel; devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o chamada
' LINHAATUAL (Jo�o Mac-Cormick - 30/6/2000)
Private Function LinhaAtual() As String
  If TpEmissao = "A" Then
      LinhaAtual = Format(Se(EnviaCliente, ContaLinha1, ContaLinha2), "000000")
  Else
      LinhaAtual = Format(Se(EnviaCliente, ContaLinha3, ContaLinha4), "000000")
  End If
End Function

' Devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o
Private Function RegAtual() As Long
   If TpEmissao = "A" Then
      RegAtual = Se(EnviaCliente, QtdReg1, QtdReg2)
   Else
      RegAtual = Se(EnviaCliente, QtdReg3, QtdReg4)
   End If
End Function

Sub Lista_Beneficiarios(ByVal RegClausula As Integer, Optional ByVal ObjSegurado As Long)
Dim PriVez As Boolean, j As Integer, ObjSeguradoAnterior As Integer

'Lista Benefici�rios do �ltimo obj. segurado
PriVez = True: ObjSeguradoAnterior = -1
If QtdBenefs > 0 Then
   For j = 0 To QtdBenefs - 1
      'No caso de endosso de benefici�rio (somente) listar tb o t�tulo do item
      If ObjSegurado = 0 Then
         If Val(Benef(0, j)) <> ObjSeguradoAnterior Then
            'T�tulo
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "ITEM " & Format(Benef(0, j), "00") & ": "
            Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "BENEFICI�RIO(S) : "
            ObjSeguradoAnterior = Val(Benef(0, j))
         Else
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(18)
         End If
         Reg = Reg & Trim(Benef(1, j))
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      Else
         If Val(Benef(0, j)) = ObjSegurado Then
            If PriVez Then
               'Titulo
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "BENEFICI�RIO(S) : "
               PriVez = False
            Else
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(18)
            End If
            Reg = Reg & Trim(Benef(1, j))
            Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
         End If
      End If
   Next
End If
End Sub

Sub Lista_CoberturasTransp(ByVal RegClausula As Integer)
Dim vCobTransp As CoberturaTransp, vVerbaTransp As VerbaTransp, sMercadoria As String, TamStr As Integer
Dim TextoMercadoria As String, primLinha As Boolean, Inicio As Integer, i As Integer, TextoAux As String
Dim vMercadoria As String, pos As Integer, vLinhaMercadoria As Mercadoria

For Each vCobTransp In CoberturasTransp
   With vCobTransp
      'T�tulo do item
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "ITEM " & Format$(.ObjSegurado, "00") & ":" & Space(8)
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      For Each vVerbaTransp In .Verbas
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & Left(vVerbaTransp.Descr & Space(19), 19) & ": "
         Reg = Reg & Right(Space(16) & vVerbaTransp.ValIs, 16)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      Next
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Meio de Transporte : " & .MeioTransporte
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Pa�s de Proced�ncia: " & .PaisProcedencia
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Destino Final      : " & .DestinoFinal
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      'mercadoria - pode ter mais de uma linha
      Reg = Reg & "Mercadoria         : "
      TextoAux = ""
      For Each vLinhaMercadoria In .Mercadorias
         TextoMercadoria = TextoAux & vLinhaMercadoria.Descricao
         Reg = Reg & Trim(TextoMercadoria)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         TextoAux = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(21)
         Reg = ""
         ContaLinhaAtual = ContaLinhaAtual + 1
      Next
'      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'      Reg = Reg & "Embalagem          : " & .Embalagem
'      Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'      IncrementaLinha
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Cobertura(s)       : " & .Cobertura
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Franquia           : " & .Franquia
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "N�mero do documento: " & .NumDoc
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "Taxa especial      : " & .Taxa
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      'linha em branco
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End With
Next
Set CoberturasTransp = Nothing

End Sub

Sub Monta_ColecaoPagamentos(ByVal proposta As String, ByVal NumCobranca As String, ByVal NumVia As String)
Dim novoPagamento As New Pagamento

With novoPagamento
   .NumCobranca = NumCobranca
   .NumVia = NumVia
   .proposta = proposta
End With
Pagamentos.Add novoPagamento

End Sub

Function Monta_SqlCoberturas() As String

SQL = "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_aceito_tb e (nolock), tp_cobertura_tb c (nolock), tp_cob_item_prod_tb t (nolock) "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_avulso_tb e (nolock), tp_cobertura_tb c (nolock), tp_cob_item_prod_tb t (nolock) "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_cond_tb e (nolock) , tp_cobertura_tb c (nolock) , tp_cob_item_prod_tb t (nolock)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_emp_tb e (nolock) , tp_cobertura_tb c (nolock) , tp_cob_item_prod_tb t (nolock)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_maq_tb e (nolock) , tp_cobertura_tb c (nolock) , tp_cob_item_prod_tb t (nolock)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, c.descricao, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia, e.texto_franquia, e.fat_franquia, t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_res_tb e (nolock) , tp_cobertura_tb c (nolock) , tp_cob_item_prod_tb t (nolock)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If
SQL = SQL & " ORDER BY e.val_is desc "
Monta_SqlCoberturas = SQL

End Function

Private Sub Montar_linha_digitavel()
 
'** Cobran�a
 
    Dim Parte1                                     As String
    Dim Parte2                                     As String
    Dim Parte3                                     As String
    Dim Dv1                                        As String
    Dim Dv2                                        As String
    Dim Dv3                                        As String
    Dim Dv_geral                                   As String
    Dim Codigo_barras_1                            As String
    Dim Codigo_barras_2                            As String
    Dim Codigo_barras_3                            As String
       
    Parte1 = "0019" 'c�digo banco + dv
    Parte1 = Parte1 & Left(Nosso_Numero, 1) _
           & Mid(Nosso_Numero, 2, 4)
    Dv1 = calcula_mod10(Parte1)
    Parte1 = Mid(Parte1, 1, 5) & "." & Mid(Parte1, 6, 4) & Dv1
    
    Parte2 = Mid(Nosso_Numero, 6, 5) _
           & Mid(Nosso_Numero, 11, 1) _
           & Format(agencia_id, "0000") '& "0452"   'Ag�ncia cedente
    Dv2 = calcula_mod10(Parte2)
    Parte2 = Mid(Parte2, 1, 5) & "." & Mid(Parte2, 6, 5) & Dv2
    
    'Parte3 = "00405200"  'conta cedente
    Parte3 = Format(conta_corrente_id, "00000000")
    Parte3 = Parte3 & Left(Carteira, 2)
    Dv3 = calcula_mod10(Parte3)
    Parte3 = Mid(Parte3, 1, 5) & "." & Mid(Parte3, 6, 5) & Dv3
    
    Codigo_barras_1 = "0019" 'c�digo banco + dv
    ' acrescentado a diferen�a de dias entre a data de vencimento e (7/10/97)
    ' para a forma��o da linha digit�vel -- Jo�o Mac-Cormick em 19/3/2001
    Dim Fator
    Fator = Format(DateDiff("d", "07/10/1997", dt_agendamento), "0000")
    Codigo_barras_2 = Fator & Format(Val_Cobranca * 100, "0000000000")
    
    Codigo_barras_3 = Format(Nosso_Numero, "00000000000") & Left(agencia, 4) & Left(Codigo_Cedente, 8) _
                    & Left(Carteira, 2)
    codigo_barras = Codigo_barras_1 & " " & Codigo_barras_2 & Codigo_barras_3
    Dv_geral = calcula_mod11(codigo_barras)
    
    codigo_barras = Left(Codigo_barras_1 & Dv_geral & Codigo_barras_2 & Codigo_barras_3 & Space(44), 44)
    
    ' acerto da linha digit�vel -- Jo�o Mac-Cormick em 16/5/2001
    linha_digitavel = Right(Space(54) & Parte1 & " " & Parte2 & " " & Parte3 & " " & Dv_geral & " " _
                      & Fator & Format(Val_Cobranca * 100, "0000000000"), 54)

End Sub


Function Obtem_Dados_Cliente(VProposta_Id As Long) As Boolean
Dim nome As String, Endereco As String, Bairro As String, Municipio As String
Dim Cep As String, UF As String
'** Cobran�a
    
    On Error GoTo Erro
        
    Obtem_Dados_Cliente = False
           
    Dim SQL As String
    Dim rc_Dados_Cliente As rdoResultset
           
    SQL = "SELECT b.nome, c.endereco, c.bairro, c.municipio, c.cep, c.estado " _
        & " FROM proposta_tb a (nolock) , cliente_tb b (nolock) , endereco_corresp_tb c (nolock)  " _
        & " WHERE a.proposta_id = " & VProposta_Id _
        & "   and a.prop_cliente_id = b.cliente_id " _
        & "   and a.proposta_id = c.proposta_id "
    
    Set rc_Dados_Cliente = rdocn2.OpenResultset(SQL)
      
    Sacado_1 = ""
    Sacado_2 = ""
    Sacado_3 = ""
    
    If Not rc_Dados_Cliente.EOF Then
       nome = UCase(Left(rc_Dados_Cliente(0) & Space(50), 50))
       Endereco = UCase(Left(rc_Dados_Cliente(1) & Space(50), 50))
       Bairro = UCase(Left(rc_Dados_Cliente(2) & Space(30), 30))
       Municipio = UCase(Left(rc_Dados_Cliente(3) & Space(45), 45))
       Cep = Format(rc_Dados_Cliente(4), "00000000")
       If IsNull(rc_Dados_Cliente(5)) Then
         UF = "  "
       Else
         UF = rc_Dados_Cliente(5)
       End If
    Else
       Exit Function
    End If
           
    rc_Dados_Cliente.Close
    Set rc_Dados_Cliente = Nothing
    
    Sacado_1 = Left(nome & Space(60), 60)
    Sacado_2 = Left(Endereco & Space(60), 60)
    Sacado_3 = Left(Cep & " " & Trim(Bairro) & " " & Trim(Municipio) & " " & UF & Space(60), 60)
    
    Obtem_Dados_Cliente = True
    Exit Function
      
Erro:
    TrataErroGeral "Obtem_Dados_Cliente", Me.Name
    Resume Next:
    Call Fecha_Arquivo
    TerminaSEGBR
    
End Function


Private Sub Processa_Cobranca()

'** Cobran�a
Dim rc_pagamentos                     As rdoResultset
Dim SQL                               As String
Dim Reg                               As String
Dim TraillerArq                       As String
Dim proposta                          As Long
Dim Produto                           As String
Dim num_via                           As String
Dim rc                                As rdoResultset
Dim banco_id                          As String
Dim Conta_cobrancas                   As Long
Dim num_cobranca                      As Long
Dim num_parcela_endosso               As Long
'Dim Val_cobranca                      As Double
Dim val_iof                           As Double
Dim Dt_inclusao                       As String
Dim ramo_id                           As String
Dim Apolice_id                        As String
Dim Local_pagto                       As String
Dim Cedente                           As String
Dim Especie_doc                       As String
Dim Aceite                            As String
Dim dt_processamento                  As String
Dim nosso_numero_2                    As String
Dim Num_Conta                         As String
Dim Especie                           As String
Dim Valor_unitario                    As String
Dim Valor_documento                   As String
Dim Quantidade                        As String
Dim linha_1                           As String
Dim linha_2                           As String
Dim linha_3                           As String
Dim linha_4                           As String
Dim linha_5                           As String
Dim vPagamento                       As New Pagamento
Dim Convenio As String
On Error GoTo Erro
     
Conta_cobrancas = 0
   
'Obtem os pagamentos ainda n�o emitidos: registros de emissao_CBR_tb com dt_emissao = Null
'Referentes a Ap�lices emitidas

SQL = "SELECT a.num_cobranca, b.val_cobranca, b.dt_agendamento, " _
    & "       a.proposta_id, b.val_iof, c.dt_proposta, " _
    & "       b.apolice_id, b.ramo_id, a.num_via, " _
    & "       nosso_numero=isNull(b.nosso_numero, 0), " _
    & "       nosso_numero_dv=isNull(b.nosso_numero_dv, ' '),  " _
    & "       b.dt_agendamento, b.num_parcela_endosso " _
    & " FROM emissao_CBR_tb a (nolock) , " _
    & "      agendamento_cobranca_tb b (nolock) , " _
    & "      proposta_tb c (nolock)  " _
    & " WHERE  " _
    & "    b.proposta_id = a.proposta_id " _
    & "   and b.num_cobranca = a.num_cobranca " _
    & "   and c.proposta_id = a.proposta_id " _
    & "   and a.proposta_id = " & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (b.num_endosso=0 or b.num_endosso is null) "
Else
   SQL = SQL & " AND b.num_endosso=" & num_endosso
End If
'    & " WHERE dt_emissao is Null and " _

'Set rc_pagamentos = rdocn2.OpenResultset(Sql, rdOpenStatic)
Set rc_pagamentos = rdocn.OpenResultset(SQL)
conta_corrente_id = "": agencia_id = ""
While Not rc_pagamentos.EOF
   DoEvents
   num_parcela_endosso = Val(0 & rc_pagamentos("num_parcela_endosso"))
   num_cobranca = Format(rc_pagamentos("num_cobranca"), "0000")
   Val_Cobranca = Val(rc_pagamentos("val_cobranca"))
   dt_agendamento = Format(rc_pagamentos("dt_agendamento"), "dd-mm-yyyy")
   proposta = rc_pagamentos("proposta_id")
   val_iof = Val(rc_pagamentos("val_iof"))
   Nosso_Numero = rc_pagamentos("nosso_numero")
   Nosso_numero_dv = rc_pagamentos("nosso_numero_dv")
   num_via = rc_pagamentos("num_via")
   'Dt_inclusao = rc_pagamentos("dt_proposta")
   Dt_inclusao = DtEmissao
   ramo_id = rc_pagamentos("ramo_id")
   Apolice_id = rc_pagamentos("apolice_id")
         
   If Obtem_Dados_Cliente(proposta) = False Then
      Exit Sub
   End If
   
   Convenio = Mid(Nosso_Numero, 1, 6)
   
   '---------------------------------------------------------------------------
   ' joconceicao - 31/05/2001 sol. by M Esteves
   ' a partir de 30 abril 2001o convenio do outro maquinas mudou e
   ' continua o mesmo produto
   If Not (Produto = "8" And (Convenio = "5437" Or Convenio = "5575")) Then
   
      
        SQL = "SELECT num_convenio FROM tp_movimentacao_financ_tb (nolock)  "
        SQL = SQL & "   WHERE produto_id           = " & ProdutoId
       'SQL = SQL & "   AND tp_operacao_financ_id = " & num_cobranca
        SQL = SQL & "   AND ramo_id                = " & ramo_id
        SQL = SQL & "   AND num_convenio in ('" & Convenio & "', '" & Mid(Convenio, 1, 4) & "')"
        
        'joconceicao - 2002 aug 09 - busca convenio por moeda
        SQL = SQL & "   AND moeda_id               = " & MoedaSeguroId
        '
        Set rc = rdocn2.OpenResultset(SQL)
        If Not rc.EOF Then
             Convenio = rc(0)
             '
             rc.Close
         Else
             rdocn.RollbackTrans
             MensagemBatch "Conv�nio n�o cadastrado para o Ramo " & Format(ramo_id, "00") & " / Produto " & ProdutoId & ". O Programa ser� cancelado.", vbCritical
             Unload Me
             Call TerminaSEGBR
             End
         End If
   End If
   '--------------------------------------------------------
   Local_pagto = Left("QUALQUER AG�NCIA" + Space(60), 60)
   
   Produto = Format(ProdutoId, "000")
   
   SQL = "SELECT banco_id, agencia_id, conta_corrente_id " _
       & " FROM convenio_tb (nolock)  " _
       & " WHERE num_convenio = '" & Convenio & "'" 'Mid(Nosso_numero, 1, 4)
   Set rc = rdocn2.OpenResultset(SQL)
   If Not rc.EOF Then
      banco_id = rc("banco_id")
      agencia_id = rc("agencia_id")
      conta_corrente_id = rc("conta_corrente_id")
      agencia = Format(agencia_id, "0000") & "-" & calcula_dv_agencia_cc(Format(agencia_id, "0000"))
      Codigo_Cedente = Format(conta_corrente_id, "00000000") & "-" & calcula_dv_agencia_cc(Format(conta_corrente_id, "00000000"))
   Else
      banco_id = 0
      agencia = "      "
      Codigo_Cedente = "          "
   End If
   rc.Close
       
   If banco_id > 0 Then
      Cedente = Buscar_cedente(banco_id, agencia_id, conta_corrente_id)
   Else
      Cedente = Left("BB CORRETORA SEGS ADM BENS S/A" + Space(60), 60)
   End If
   
   Especie_doc = "NS"
   Aceite = "N"
   dt_processamento = Format(Now, "dd-mm-yyyy")
   nosso_numero_2 = Format(Nosso_Numero, "00\.000\.000\.000\-") & Nosso_numero_dv
   Num_Conta = Space(10)
    '' Mudar a carteira de acordo com o conv�nio
    If Convenio = "110054" Then
        Carteira = "15-019"
    Else
        Carteira = "16-019"
    End If
   Especie = Left(MoedaSeguro & Space(4), 4)
   'Especie = "R$"
   Quantidade = Space(10)
   Valor_unitario = Space(10)
   
   'Se estiver em moeda estrangeira e val iof for diferente de 0
   If Val(MoedaSeguroId) <> Val(MoedaAtual) And val_iof <> 0 Then
      Val_Cobranca = Val_Cobranca - val_iof
   End If
   
   Valor_documento = Right(Space(16) + Format(Val_Cobranca, "#,###,###,##0.00"), 16)
   
   linha_1 = Left("***  VALORES EM " & NomeMoedaSeguro & " ***" + Space(60), 60)
   If Val(MoedaSeguroId) = Val(MoedaAtual) And val_iof <> 0 Then
      linha_2 = Left("I.O.F.: " & MoedaSeguro & " " & Format(val_iof, "##,##0.00") + Space(60), 60)
   Else
      linha_2 = Space(60)
   End If
      
   linha_digitavel = Space(54)
   codigo_barras = Space(44)
   
   If num_cobranca = 1 Then
      'Se parcela j� foi quitada, n�o montar linha digit�vel
      If TpEmissao = "E" And tpEndossoId = 93 And ImprimeClienteBoleta Then
         linha_3 = Left("N�O RECEBER AP�S O VENCIMENTO" + Space(60), 60)
         linha_4 = DescrEndossoBoleta(0)
         linha_5 = DescrEndossoBoleta(1)
         Montar_linha_digitavel
      ElseIf TpEmissao = "A" And PagoAto Then
         linha_3 = Space(60)
         linha_4 = Left("ATEN��O: " + Space(60), 60)
         linha_5 = Left("ESTA PARCELA J� FOI QUITADA" + Space(60), 60)
      ElseIf TpEmissao = "E" And (CDate(DtInicioVigencia) = CDate(rc_pagamentos!dt_agendamento)) Then
         linha_3 = Space(60)
         linha_4 = Left("ATEN��O: " + Space(60), 60)
         linha_5 = Left("ESTA PARCELA J� FOI QUITADA" + Space(60), 60)
      Else
         linha_3 = Left("N�O RECEBER AP�S O VENCIMENTO" + Space(60), 60)
         linha_4 = Space(60)
         linha_5 = Space(60)
         Montar_linha_digitavel
      End If
   Else
      If TpEmissao = "E" And tpEndossoId = 93 And ImprimeClienteBoleta Then
         linha_3 = Left("N�O RECEBER AP�S O VENCIMENTO" + Space(60), 60)
         linha_4 = DescrEndossoBoleta(0)
         linha_5 = DescrEndossoBoleta(1)
         Montar_linha_digitavel
      ElseIf TpEmissao = "E" And (CDate(DtInicioVigencia) = CDate(rc_pagamentos!dt_agendamento)) Then
         linha_3 = Space(60)
         linha_4 = Left("ATEN��O: " + Space(60), 60)
         linha_5 = Left("ESTA PARCELA J� FOI QUITADA" + Space(60), 60)
      Else
         linha_3 = Left("N�O RECEBER AP�S O VENCIMENTO" + Space(60), 60)
         linha_4 = Space(60)
         linha_5 = Space(60)
         Montar_linha_digitavel
      End If
   End If
   
'   Linha_digitavel = Space(54)
'   Codigo_barras = Space(44)
'   If num_cobranca <> 1 Then
'      Montar_linha_digitavel
'   End If
   '
   ' Gera registro detalhe para pagamento
   '
   Reg = "60" _
       & Format(ContaLinhaAtual, "000000") _
       & num_proposta _
       & linha_digitavel _
       & Local_pagto _
       & dt_agendamento _
       & Cedente _
       & Left(agencia & " / " & Codigo_Cedente & Space(20), 20) _
       & Format(Dt_inclusao, "dd-mm-yyyy") _
       & Format(ramo_id, "00") & Format(Apolice_id, "0000000") & Format(num_endosso, "00000") & Format(num_parcela_endosso, "000") _
       & Especie_doc _
       & Aceite _
       & dt_processamento _
       & nosso_numero_2 _
       & Num_Conta _
       & Carteira _
       & Especie _
       & Quantidade _
       & Valor_unitario _
       & Valor_documento _
       & linha_1 & linha_2 & linha_3 & linha_4 & linha_5 _
       & Sacado_1 & Sacado_2 & Sacado_3 _
       & codigo_barras _
       & Produto

'       & Format(ramo_id, "00") & Format(Apolice_id, "0000000") & Format(num_cobranca, "00000000")
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   
   ContaLinhaAtual = ContaLinhaAtual + 1
   
   'monta cole��o de pagamentos p/ serem atualizados no final do loop
   Monta_ColecaoPagamentos proposta, num_cobranca, num_via
'   Atualiza_pagamento Proposta, num_cobranca, Num_via
   rc_pagamentos.MoveNext
Wend
rc_pagamentos.Close

For Each vPagamento In Pagamentos
   Atualiza_pagamento vPagamento.proposta, vPagamento.NumCobranca, vPagamento.NumVia
Next
Set Pagamentos = Nothing

Exit Sub
   
Erro:
   TrataErroGeral "Processa_Cobranca", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Function RetornaQtdCongeneres(ByVal Apolice As Long) As Long
  Dim OSQL As String
  Dim rs As rdoResultset
  OSQL = "   SELECT COUNT(*) AS Total "
  Inc OSQL, "FROM co_seguro_repassado_tb (nolock)  "
  Inc OSQL, "WHERE apolice_id = " & Apolice
  Inc OSQL, "  AND dt_fim_participacao IS NULL"
  
  Set rs = rdocn.OpenResultset(OSQL)
  
  RetornaQtdCongeneres = rs!Total
End Function

Sub Ler_TransporteInternacional()
ReDim CoberturaTransporte(2, 10)
Dim nCobTransp As New CoberturaTransp, rs As rdoResultset, Rs1 As rdoResultset, PriVez As Boolean
Dim vMercadoria As String, pos As Integer, Inicio As Integer, i As Integer, posvazio As Long
Dim NovoInicio As Integer

SQL = "   SELECT via, porto, pais_origem, municipio_dest, estado_dest, "
SQL = SQL & "       desc_mercadoria, '  ' embalagem, tp_cob_basica, "
SQL = SQL & "       franquia, guia, taxa_especial, "
SQL = SQL & "       franquia, guia, taxa_especial, "
SQL = SQL & "       cod_objeto_segurado, dt_inicio_vigencia_seg, seq_canc_endosso_seg "
SQL = SQL & "FROM proposta_transporte_tb p (nolock)  "
SQL = SQL & "LEFT OUTER JOIN seguro_transporte_tb s "
SQL = SQL & "  ON p.proposta_id = s.proposta_id "
SQL = SQL & "WHERE p.proposta_id = " & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (s.endosso_id=0 or s.endosso_id is null) "
Else
   SQL = SQL & " AND s.endosso_id=" & num_endosso
End If
'SQL = SQL & " AND endosso_id IS NULL "
Set rs = rdocn.OpenResultset(SQL)
PriVez = True: QtdObjetos = 0
While Not rs.EOF
   If PriVez Then
      PriVez = False
   Else
      QtdLinhasCobertura = QtdLinhasCobertura + 1 'pular uma linha
      'QtdLinhasCobertura = 16 'Se tem 2 objetos, n�o vai conseguir imprimir na 1� p�gina
   End If
   QtdObjetos = QtdObjetos + 1
   QtdCoberturas = QtdCoberturas + 1
   QtdLinhasCobertura = QtdLinhasCobertura + 1 'item ...
   With nCobTransp
      SQL = "   SELECT desc_verba, val_is_verba "
      SQL = SQL & "FROM escolha_verba_transporte_tb e (nolock)  "
      SQL = SQL & "INNER JOIN verba_transporte_tb v (nolock)  "
      SQL = SQL & "   ON e.verba_transporte_id = v.verba_transporte_id "
      SQL = SQL & "WHERE e.proposta_id = " & num_proposta
      SQL = SQL & "  AND cod_objeto_segurado = " & rs("cod_objeto_segurado")
      SQL = SQL & "  AND seq_canc_endosso_seg = " & rs("seq_canc_endosso_seg")
      SQL = SQL & "  AND dt_inicio_vigencia_seg = "
      SQL = SQL & "'" & Format(rs("dt_inicio_vigencia_seg"), "yyyymmdd") & "' "
      SQL = SQL & "ORDER BY e.verba_transporte_id"
      Set Rs1 = rdocn2.OpenResultset(SQL)
      While Not Rs1.EOF
         .Verbas.Add Rs1("desc_verba"), Format(Val(Rs1("val_is_verba")), "###,###,##0.00")
         Rs1.MoveNext
         QtdLinhasCobertura = QtdLinhasCobertura + 1
      Wend
      Rs1.Close
      .ObjSegurado = QtdObjetos
      .MeioTransporte = rs("via") '& " - " & rs("porto")
      .PaisProcedencia = rs("pais_origem")
      .DestinoFinal = rs("municipio_dest") & " - " & rs("estado_dest")
      vMercadoria = Trim("" & rs("desc_mercadoria"))
      pos = InStr(Chr(13) & Chr(10), vMercadoria)
      Do While pos <> 0
         vMercadoria = Mid(vMercadoria, 1, pos - 1) & Mid(vMercadoria, 1, pos + 2)
         pos = InStr(Chr(13) & Chr(10), vMercadoria)
      Loop
      Inicio = 1
      If Len(vMercadoria) <= 79 Then
         .Mercadorias.Add (vMercadoria)
         QtdLinhasCobertura = QtdLinhasCobertura + 1
      Else
         For i = 1 To Len(vMercadoria)
            If Len(vMercadoria) - Inicio >= 79 Then
               .Mercadorias.Add (Mid(vMercadoria, Inicio, 79))
               QtdLinhasCobertura = QtdLinhasCobertura + 1
               Inicio = Inicio + 79
            Else
               If Inicio < Len(vMercadoria) Then
                  .Mercadorias.Add (Mid(vMercadoria, Inicio, Len(vMercadoria) - Inicio))
                  QtdLinhasCobertura = QtdLinhasCobertura + 1
               End If
               Exit For
            End If
         Next
      End If
      .Embalagem = rs("embalagem")
      .Cobertura = "B�sica - " & IIf(LCase(rs("tp_cob_basica")) = "a", "Ampla", "Restrita")
      .Franquia = rs("franquia") & "%"
      .NumDoc = rs("guia")
      .Taxa = IIf(Logico(rs("taxa_especial")), "sim", "n�o")
      QtdLinhasCobertura = QtdLinhasCobertura + 10
   End With
   CoberturasTransp.Add nCobTransp
   Set nCobTransp = Nothing
   rs.MoveNext
Wend
rs.Close

End Sub

Private Sub Form_Activate()

'    On Error GoTo Erro
'
'    Call cmdOK_Click
'
'    Unload Me
'
'Exit Sub
'
'Erro:
'    TrataErroGeral "Form_Activate APL201", Me.name
'    TerminaSEGBR

End Sub

Private Sub Form_Load()

    On Error GoTo Erro
    
    Me.Caption = "Resumo da Emiss�o de Ap�lices e Endossos RE - " & Ambiente
      
    'Call Conexao_auxiliar           'comentado em 23/09/2003
    
    sDecimal = LeArquivoIni2("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
       ConfiguracaoBrasil = False
    Else
       ConfiguracaoBrasil = True
    End If
    cmdCanc.Caption = "&Sair"
    cmdCanc.Refresh
    
    Call cmdOk_Click
    
    Call cmdCanc_Click
    
Exit Sub
    
Erro:
    TrataErroGeral "Form_Load SEGA8055", Me.Name
    TerminaSEGBR

End Sub

Function LeArquivoIni2(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String
Dim RetornoDefault As String, nc As String
     Dim Retorno As String * 100
     
     RetornoDefault = "*"
     nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
     LeArquivoIni2 = Left$(Retorno, nc)

End Function

Private Sub cmdOk_Click()

    Dim sTpDocumentos As String

    'Tratamento para o Scheduler
    'Call InicializaParametrosExecucaoBatch(Me)

    txtIni = Now
    MousePointer = 11
    cmdOk.Enabled = False
      
    Carta_path = LerArquivoIni("relatorios", "remessa_gerado_path")
    'Carta_path = "c:\jvieira\"
    
    cmdCanc.Caption = "&Cancelar"
    cmdCanc.Refresh
    
    'Call Libera_Impressao         'Incluido dentro da fun��o Processa() em 15092003
    
    'Processa Ap�lices

    NumRegs = 0

    Call ProcessarSega8055

    txtfim = Now
    MousePointer = 0
    
    cmdCanc.Caption = "&Sair"
    cmdCanc.Refresh
    
    Call goProducao.Finaliza

End Sub

Private Sub cmdCanc_Click()

If UCase(cmdCanc.Caption) = "&CANCELAR" Then
   If Not goProducao.Automatico Then
      If MsgBox("Deseja realmente cancelar o gera��o do arquivo ?", vbYesNo + vbQuestion) = vbYes Then
         rdocn.RollbackTrans
         'If rdocn1.StillConnecting = True Then rdocn1.Close
         MsgBox "Programa Cancelado", vbInformation
         Unload Me
         Call TerminaSEGBR
         End
      End If
   End If
Else
   If rdocn1.StillConnecting = True Then rdocn1.Close
   Unload Me
   Call TerminaSEGBR
   End
End If

End Sub

Private Sub ProcessarSega8055()

Dim TraillerArq As String, vPagamento As Pagamento, rs As rdoResultset, TpEmissaoAnt As String
Dim rc_diretoria As rdoResultset
Dim rc_corretor As rdoResultset
Dim primeiro_loop As Boolean

'Adenilson (11/06/2003) - V�ri�vel para armazenamento tempor�rio de atualiza��es
Dim colAtualiza As Collection
Dim nCont As Long
Dim NumRegs As Long

On Error GoTo Erro
     
ContaLinhaAtual = 1
NumRegs = 0

tam_reg = 2010 'Alterado por Alexandre Ricardo em 01/07/2003 para atender novo layout de arquivos - Corretor Susep
primeiro_loop = True

TabEscolha = "": TpEmissaoAnt = ""

'Ap�lice 1a VIA
    SQL = "SELECT  'A' tpemissao, apolice_id, a.proposta_id, dt_inicio_vigencia, dt_fim_vigencia, 0 endosso_id, '19000101' dt_pedido_endosso,  "
    SQL = SQL & " a.seguradora_cod_susep, sucursal_seguradora_id, a.ramo_id, "
    SQL = SQL & " apolice_envia_cliente, apolice_num_vias, apolice_envia_congenere, a.dt_emissao, pp.produto_id, 0 tp_endosso_id , a.num_proc_susep, pf.impressao_liberada, "
    SQL = SQL & "  NULL 'destino', "
    SQL = SQL & "  NULL 'num_solicitacao', "
    SQL = SQL & "  NULL 'diretoria_id' "
    SQL = SQL & "  FROM apolice_tb a (nolock) "
    SQL = SQL & "       inner join ramo_tb r (nolock) ON a.ramo_id=r.ramo_id "
    SQL = SQL & "       INNER JOIN proposta_tb pp (nolock) ON a.proposta_id=pp.proposta_id "  ' jo�o
    SQL = SQL & "       INNER JOIN produto_tb pd (nolock) ON pp.produto_id=pd.produto_id "    ' jo�o
    SQL = SQL & "       INNER JOIN proposta_fechada_tb pf (nolock) ON a.propostA_id = pf.proposta_id "
    SQL = SQL & "  WHERE a.dt_impressao is null        AND "
    SQL = SQL & "       (pf.impressao_liberada = 's' or (pf.impressao_liberada = 'n' and pd.Apolice_envia_cliente = 'S')) AND "
    SQL = SQL & "        r.tp_ramo_id=" & TpRamoRE & "       AND "
    SQL = SQL & "        a.dt_emissao >= '20000701'          and "
    SQL = SQL & "        pp.situacao = 'i'                   and "       'alterado em 28/04/2004
    SQL = SQL & "        pp.produto_id not in (1, 2, 3, 4, 5, 6, 7, 103, 109, 114, 116, 119, 423, 424, 425, 717, 718, 719) " 'jorfilho - 16/08/2001 - Novo Ouro Residencial
'   SQL = SQL & "  AND (pp.produto_id <> 8 OR (pp.produto_id=8 and isnull(pp.proposta_id_anterior,0) = 0))"
'   Sql = Sql & "  AND pp.produto_id not in (8,710, 777) "     'remover esta linha depois do teste
    'Madorno - 15/02/2005
    SQL = SQL & " AND a.dt_emissao >= '20050210' "

    SQL = SQL & " UNION "
'Ap�lice 2a VIA
    SQL = SQL & " SELECT  "
    SQL = SQL & "   'A' tpemissao, "
    SQL = SQL & "   apolice_id, "
    SQL = SQL & "   a.proposta_id, "
    SQL = SQL & "   dt_inicio_vigencia, "
    SQL = SQL & "   dt_fim_vigencia, "
    SQL = SQL & "   0 endosso_id, "
    SQL = SQL & "   '19000101' dt_pedido_endosso, "
    SQL = SQL & "   a.seguradora_cod_susep, "
    SQL = SQL & "   sucursal_seguradora_id, "
    SQL = SQL & "   a.ramo_id, "
    SQL = SQL & "   apolice_envia_cliente, "
    SQL = SQL & "   apolice_num_vias, "
    SQL = SQL & "   apolice_envia_congenere, "
    SQL = SQL & "   a.dt_emissao, "
    SQL = SQL & "   pp.produto_id, "
    SQL = SQL & "   0 tp_endosso_id, "
    SQL = SQL & "   a.num_proc_susep, "
    SQL = SQL & "   'impressao_liberada' = '', "
    SQL = SQL & "   ssv.destino, "
    SQL = SQL & "   ssv.num_solicitacao, "
    SQL = SQL & "   ssv.diretoria_id "
    SQL = SQL & " FROM "
    SQL = SQL & "   evento_seguros_db..evento_impressao_tb ssv (nolock)"
    SQL = SQL & "   inner join apolice_tb a (nolock)"
    SQL = SQL & "       on ssv.proposta_id = a.proposta_id "
    SQL = SQL & "        inner join ramo_tb r (nolock)"
    SQL = SQL & "       on a.ramo_id=r.ramo_id "
    SQL = SQL & "        inner join proposta_tb pp (nolock)"
    SQL = SQL & "       on a.proposta_id=pp.proposta_id "
    SQL = SQL & "        inner join produto_tb pd (nolock)"
    SQL = SQL & "       on pp.produto_id=pd.produto_id "
    SQL = SQL & "        inner join proposta_fechada_tb pf (nolock)"
    SQL = SQL & "       on a.propostA_id = pf.proposta_id "
    SQL = SQL & " WHERE "
    SQL = SQL & "   ssv.status = 'l' and "
    SQL = SQL & "   ssv.dt_geracao_arquivo is NULL and "
    SQL = SQL & "   ssv.tp_documento_id in (4) "
    SQL = SQL & "   and pp.produto_id not in (1, 2, 3, 4, 5, 6, 7, 103, 109, 114, 116, 119, 423, 424, 425, 717, 718, 719) "  '24/09/2003
    SQL = SQL & "   and NOT (a.ramo_id = 22 and pp.produto_id <> 400) "  'Martines- O programa n�o deve gerar 2� Via para transporte a pedido do Senhor Marcio Yoshimura

   SQL = SQL & " UNION "
'Endosso 1a VIA
   SQL = SQL & "SELECT  'E' tpemissao, a.apolice_id, a.proposta_id, a.dt_inicio_vigencia, a.dt_fim_vigencia, e.endosso_id, dt_pedido_endosso,  "
   SQL = SQL & " a.seguradora_cod_susep, a.sucursal_seguradora_id, a.ramo_id, "
   SQL = SQL & " apolice_envia_cliente, apolice_num_vias, apolice_envia_congenere, e.dt_emissao, pp.produto_id, e.tp_endosso_id , a.num_proc_susep, 'impressao_liberada' = '', " ' jo�o
   SQL = SQL & "  NULL 'destino', "
   SQL = SQL & "  NULL 'num_solicitacao', "
   SQL = SQL & "  NULL 'diretoria_id' "
   SQL = SQL & "  FROM apolice_tb a (nolock) "
   SQL = SQL & "  INNER JOIN endosso_tb e (nolock) ON (a.proposta_id=e.proposta_id) and tp_endosso_id not in (63,90,91,100,101) "
   'Sql = Sql & "         AND e.endosso_id = ssv.endosso_id "
   SQL = SQL & "  INNER JOIN ramo_tb r (nolock) ON a.ramo_id=r.ramo_id "
   SQL = SQL & "  INNER JOIN proposta_tb pp (nolock) ON e.proposta_id=pp.proposta_id "  ' jo�o
   SQL = SQL & "  INNER JOIN produto_tb pd (nolock) ON pp.produto_id=pd.produto_id "    ' jo�o
   SQL = SQL & "  WHERE  e.dt_impressao is null              AND "
   SQL = SQL & "         r.tp_ramo_id=" & TpRamoRE & "       AND "
   SQL = SQL & "         e.dt_emissao > '20000701'           and "
   SQL = SQL & "         pp.situacao <> 'c'                  and "
   SQL = SQL & "         pp.produto_id not in (1, 2, 3, 4, 5, 6, 7, 103, 109, 114, 116, 119, 423, 424, 425, 717, 718, 719) " 'jorfilho - 16/08/2001 - Novo Ouro Residencial
    'Madorno - 15/02/2005
    SQL = SQL & " AND a.dt_emissao >= '20050210' "

    SQL = SQL & " UNION "
'Endosso 2a VIA
    SQL = SQL & "SELECT  'tpemissao' = 'E', "
    SQL = SQL & "a.apolice_id, "
    SQL = SQL & "a.proposta_id, "
    SQL = SQL & "a.dt_inicio_vigencia, "
    SQL = SQL & "a.dt_fim_vigencia, "
    SQL = SQL & "'endosso_id' = isnull(e.endosso_id,0), "
    SQL = SQL & "'dt_pedido_endosso' = isnull(dt_pedido_endosso,''), "
    SQL = SQL & "a.seguradora_cod_susep, "
    SQL = SQL & "a.sucursal_seguradora_id, "
    SQL = SQL & "a.ramo_id, "
    SQL = SQL & "apolice_envia_cliente, "
    SQL = SQL & "apolice_num_vias, "
    SQL = SQL & "apolice_envia_congenere, "
    SQL = SQL & "'dt_emissao' = isnull(e.dt_emissao,''), "
    SQL = SQL & "pp.produto_id, "
    SQL = SQL & "'tp_endosso_id' = isnull(e.tp_endosso_id,0), "
    SQL = SQL & "'num_proc_susep' = isnull(a.num_proc_susep,0), "
    SQL = SQL & "'impressao_liberada' = '', "
    SQL = SQL & "ssv.Destino, "
    SQL = SQL & "ssv.num_solicitacao, "
    SQL = SQL & "ssv.diretoria_id "
    SQL = SQL & "FROM    evento_seguros_db..evento_impressao_tb ssv (nolock)"
    SQL = SQL & "INNER   JOIN apolice_tb a (nolock) ON ssv.proposta_id = a.proposta_id "
    SQL = SQL & "LEFT    JOIN endosso_tb e (nolock) ON ssv.proposta_id = e.proposta_id "
    SQL = SQL & "AND     e.tp_endosso_id not in (63,90,91,100,101) "
    SQL = SQL & "AND     e.endosso_id = ssv.endosso_id "
    SQL = SQL & "INNER   JOIN ramo_tb r (nolock) ON a.ramo_id = r.ramo_id "
    SQL = SQL & "INNER   JOIN proposta_tb pp (nolock) ON ssv.proposta_id = pp.proposta_id "
    SQL = SQL & "INNER   JOIN produto_tb pd (nolock) ON pp.produto_id = pd.produto_id "
    SQL = SQL & "WHERE   ssv.status = 'l' "
    SQL = SQL & "AND     ssv.dt_geracao_arquivo is NULL "
    SQL = SQL & "AND     pp.produto_id not in (114, 116, 119, 717, 718, 719) "          '24/10/2003
    SQL = SQL & "AND     ssv.tp_documento_id in (6) "
    
   SQL = SQL & "  ORDER BY tpEmissao "
'End If

Set rc_apl = rdocn1.OpenResultset(SQL)

arquivo1 = 0

'Adenilson (11/06/2003) - Inst�ncia tempor�ria da cole��o
Set colAtualiza = New Collection

Do While Not rc_apl.EOF
      
   If UCase(rc_apl!impressao_liberada) = "N" Then
     If Not Atualiza_Impressao_Liberada(rc_apl!proposta_id) Then
        GoTo Continua
     End If
   End If
   
   TpEmissao = rc_apl("tpemissao")
   tpEndossoId = 0
   ProdutoId = rc_apl!produto_id
   If ProdutoId < 8 Or ProdutoId > 10 Then
      'Para produtos <> 8, 9 ou 10 imprime coberturas no anexo
      CoberturasProdutoAnexo = True
   Else
      CoberturasProdutoAnexo = False
   End If

   DoEvents

   If TpEmissao = "A" Then
      DtInicioVigencia = Format$(rc_apl!dt_inicio_vigencia, "dd/mm/yyyy")
      DtEmissao = rc_apl!Dt_Emissao
   Else
      DtInicioVigencia = Format$(rc_apl!dt_pedido_endosso, "dd/mm/yyyy")
      DtEmissao = rc_apl!Dt_Emissao
   End If
   
   Destino_id = IIf(IsNull(rc_apl!Destino), "", rc_apl!Destino)
   If UCase(Destino_id) = "D" Then
        SQL = ""
        SQL = SQL & " SELECT nome_centro_custo "
        SQL = SQL & "   FROM web_intranet_db..ips_lotacao_tb (nolock)  "
        SQL = SQL & "  WHERE lotacao_id = " & rc_apl!Diretoria_id
   
        Set rc_diretoria = rdocn.OpenResultset(SQL)
        
        If Not rc_diretoria.EOF Then
            Diretoria_id = rc_diretoria(0)
        Else
            Diretoria_id = ""
        End If
        rc_diretoria.Close
   End If
        
   If primeiro_loop Then
      Abre_Arquivo
      primeiro_loop = False
   End If
   
   num_apolice = Format$(rc_apl!Apolice_id, "000000000")
   If TpEmissao = "E" Then
      num_endosso = Format$(rc_apl!endosso_id, "000000000")
      tpEndossoId = Val(0 & rc_apl!tp_endosso_id)
   Else
      num_endosso = "000000000"
   End If
   num_proposta = Format$(rc_apl!proposta_id, "000000000")
   Seguradora = rc_apl!seguradora_cod_susep
   Sucursal = rc_apl!sucursal_seguradora_id
   IniVig = Format$(rc_apl!dt_inicio_vigencia, "dd/mm/yyyy")
   FimVig = Format$(rc_apl!dt_fim_vigencia, "dd/mm/yyyy")
   ramo_id = Val(0 & rc_apl!ramo_id)
   processo_susep = IIf(IsNull(rc_apl!num_proc_susep), "", Trim(rc_apl!num_proc_susep))
   
    '************** Verifica��o se existe corretor susep
   SQL = ""
   SQL = SQL & " SELECT d.corretor_susep FROM corretagem_tb a (nolock)  "
   SQL = SQL & " inner join  corretor_tb d (nolock)  on a.corretor_id = d.corretor_id"
   SQL = SQL & " Where a.proposta_id = " & num_proposta & " And (endosso_id = 0 Or endosso_id Is Null) And D.Corretor_Susep Is Not Null"
   SQL = SQL & " Union SELECT d.corretor_susep FROM corretagem_pj_endosso_fin_tb a (nolock) "
   SQL = SQL & " inner join  corretor_tb d (nolock)  on a.corretor_id = d.corretor_id"
   SQL = SQL & " Where a.proposta_id = " & num_proposta & " And a.endosso_id = " & num_endosso & " And D.Corretor_Susep Is Not Null"
   SQL = SQL & " Union SELECT d.corretor_susep FROM corretagem_tb a (nolock) "
   SQL = SQL & " inner join  corretor_tb d  (nolock) on a.corretor_id = d.corretor_id"
   SQL = SQL & " Where a.proposta_id = " & num_proposta & " And a.dt_fim_corretagem Is Null And D.Corretor_Susep Is Not Null"
   Set rc_corretor = rdocn.OpenResultset(SQL)
   'Se hover valor ent�o monta o registro
   If rc_corretor.EOF Then
        rc_corretor.Close
        GoTo Continua
   End If
   
   'Barney - 07/06/2004
   rc_corretor.Close
   
   EnviaCliente = Logico(rc_apl!apolice_envia_cliente)
   If EnviaCliente Then
     QtdVias = 1
   Else
     QtdVias = Se(IsNull(rc_apl!apolice_num_vias), 1, rc_apl!apolice_num_vias)
     EnviaCongenere = Logico(rc_apl!apolice_envia_congenere)
     If EnviaCongenere Then Inc QtdVias, RetornaQtdCongeneres(CLng(num_apolice))
   End If
   
   QtdLinhasCobertura = 0
   
   DoEvents

   Processa_Controle_Documento       'Tipo 10
   
   If Not flagEnderecoAgencia Then
        GoTo Continua
   End If
   
   Processa_Dados_Gerais   'Tipo 20
   Ler_Congeneres
   EndossoAnexo = False: TextoCoberturasAnexo = False
   Ler_DescricaoEndosso
   
   'Se descri��o do endosso cabe na primeira p�gina e coberturas n�o, processar endosso primeiro,
   'com tipo de registro=3 e coberturas com tipo de registro=4
   If (EndossoDescricao.Count < 16 And EndossoDescricao.Count > 0) And (QtdLinhasCobertura > 16 Or CoberturasProdutoAnexo) Then
      Processa_DescricaoEndosso
      Processa_Coberturas     'Tipo 21 ou 22
   Else
      Processa_Coberturas     'Tipo 21 ou 22
      Processa_DescricaoEndosso
   End If

   Ler_Clausulas           'Tipo 22
   Processa_Cobranca       'Tipo 60
   
   NumRegs = NumRegs + 1
  
   'limpa cole��o de descri��es de endosso
   Set EndossoDescricao = Nothing
   
   If Not IsNull(rc_apl!num_solicitacao) Then
   
        'Call Atualiza_Evento_Impressao2(rc_apl!num_solicitacao, arquivo_remessa, cUserName)
        
        SQL = ""
        SQL = SQL & "exec evento_seguros_db..evento_impressao_geracao_spu "
        SQL = SQL & rc_apl!num_solicitacao & ", '" & arquivo_remessa & "', '" & cUserName & "' "
    
        colAtualiza.Add SQL          'Armazena a procedure de atualiza��o na cole��o
        
   Else
        If TpEmissao = "A" Then
           
           'Atualizar dt_emissao das apl emitidas
           SQL = ""
           SQL = SQL & "exec atualiza_emissao_apolice_spu 'a"
           SQL = SQL & "','" & Format$(Data_Sistema, "yyyymmdd") & "','" & cUserName & "'"
           SQL = SQL & " , " & num_proposta & ", " & num_apolice
           
           colAtualiza.Add SQL
           
           'Insere registro em evento_impressao_tb
           SQL = ""
           SQL = SQL & "exec evento_seguros_db..evento_impressao_spi " & _
                        num_proposta & _
                        ", " & num_endosso & _
                        ", null " & _
                        ", null " & _
                        ", 'C'" & _
                        ", 04" & _
                        ",'i'" & _
                        ", null" & _
                        ", 0" & _
                        ", 'C'" & _
                        ", '' " & _
                        ", '' " & _
                        ", '" & cUserName & "'" & _
                        ", '" & Format(Date, "yyyymmdd") & "'" & _
                        ", '" & arquivo_remessa & "'"
                        
           colAtualiza.Add SQL
                        
        Else
        
           'Atualizar dt_emissao das apl emitidas
           SQL = ""
           SQL = SQL & "exec atualiza_emissao_apolice_spu 'e"
           SQL = SQL & "','" & Format$(Data_Sistema, "yyyymmdd") & "','" & cUserName & "'"
           SQL = SQL & " , " & num_proposta & ", " & num_apolice
           
           colAtualiza.Add SQL
          
           'Insere registro em evento_impressao_tb
           SQL = ""
           SQL = SQL & "exec evento_seguros_db..evento_impressao_spi " & _
                        num_proposta & _
                        ", " & num_endosso & _
                        ", null " & _
                        ", null " & _
                        ", 'C'" & _
                        ", 06" & _
                        ",'i'" & _
                        ", null" & _
                        ", 0" & _
                        ", 'E'" & _
                        ", '' " & _
                        ", '' " & _
                        ", '" & cUserName & "'" & _
                        ", '" & Format(Date, "yyyymmdd") & "'" & _
                        ", '" & arquivo_remessa & "'"
                        
            colAtualiza.Add SQL
                        
        End If
   End If
   primeiro_loop = False
   
Continua:
   rc_apl.MoveNext
   DoEvents
   
Loop
rc_apl.Close

'Adenilson (11/06/2003) - In�cio da Transa��o
rdocn.BeginTrans
    
If Not IsMissing(colAtualiza) Then
    'Adenilson (11/06/2003) - Atualizar todas as atualiza��es armazenadas na cole��o no banco de dados
    For nCont = 1 To colAtualiza.Count
        rdocn.Execute colAtualiza.Item(nCont)
    Next
End If
    
If NumRegs > 0 Then
        
    'Gravando Trailler          Tipo 99
    TraillerArq = "99" & Format(Val(ContaLinhaAtual) - 1, "000000")
    TraillerArq = Left(TraillerArq + Space(tam_reg), tam_reg)
    Print #Arq1, TraillerArq
    Close #Arq1
    
    Call Insere_Arquivo_Versao_Gerado("SEGA8055", NumRegs, CLng(ContaLinhaAtual) + 1, CInt(NumRemessaApolice1))
        
Else
    
    If Trim(Nome_Arq1) <> "" Then
        Close #Arq1
        Kill Nome_Arq1
    End If
   
    Call MensagemBatch("Nenhuma Ap�lice/Endosso foi Processada.", , , False)
   
End If

rdocn.CommitTrans

    'Loga o n�mero de registros processados - Scheduler
    Call goProducao.AdicionaLog(1, IIf(NumRegs > 0, arquivo_remessa, "Arquivo n�o gerado"), 1)
    Call goProducao.AdicionaLog(2, IIf(NumRegs > 0, ContaLinhaAtual, "0"), 1)
    Call goProducao.AdicionaLog(3, IIf(NumRegs > 0, NumRegs, "0"), 1)
    
Exit Sub
   
Erro:
   rdocn.RollbackTrans
   TrataErroGeral "ProcessarSega8055", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
   
End Sub

Private Function getArquivoDocumento(arquivo_saida As String) As String
Dim SQL As String
Dim rc As rdoResultset
 
getArquivoDocumento = ""
 
SQL = " Select  tp_documento_id "
SQL = SQL & " from evento_seguros_db..documento_tb (nolock)  "
SQL = SQL & " where arquivo_saida = '" & Trim(arquivo_saida) & "'"
 
Set rc = rdocn.OpenResultset(SQL)
While Not rc.EOF
    getArquivoDocumento = IIf(Trim(getArquivoDocumento) = "", rc!tp_documento_id, getArquivoDocumento & "," & rc!tp_documento_id)
    rc.MoveNext
Wend
 
End Function

' a vari�vel CONTA_LINHA era incrementada como x = x + 1; devido a solicita��o de separar
' a emiss�o de ap�lice em dois arquivos distintos, optou-se por transformar esta opera��o
' em uma rotina (Jo�o Mac-Cormick - 30/6/2000)
Private Sub IncrementaLinha()
  If TpEmissao = "A" Then
      If EnviaCliente Then
        Inc ContaLinha1
      Else
        Inc ContaLinha2
      End If
   Else
      If EnviaCliente Then
        Inc ContaLinha3
      Else
        Inc ContaLinha4
      End If
   End If
End Sub
Private Sub IncrementaRegistro()

' Devido a solicita��o de separar a emiss�o de ap�lice em dois arquivos
' distintos, optou-se por transformar esta opera��o em uma rotina
   If TpEmissao = "A" Then
      If EnviaCliente Then
         Inc QtdReg1
      Else
         Inc QtdReg2
      End If
   Else
      If EnviaCliente Then
         Inc QtdReg3
      Else
         Inc QtdReg4
      End If
   End If

End Sub

Sub Abre_Arquivo()
'Abre o arquivo
On Error GoTo Erro
   
Dim HeaderAlianca As String, HeaderCliente As String, DataHora As String
   
DataHora = Format(Data_Sistema, "dd/mm/yyyy hh:mm AMPM") ' joconceicao 7-jun-01
    
Call Obtem_Num_Remessa("SEGA8055", NumRemessaApolice1)
   
Nome_Arq1 = Carta_path & "SEGA8055" & "." & Format(NumRemessaApolice1, "0000")

If Trim(Nome_Arq1) <> "" Then
    If Dir(Nome_Arq1) <> "" Then
       If Not goProducao.Automatico Then
          If MsgBox("J� existe um arquivo de Ap�lice gerado com a vers�o " & Format(NumRemessaApolice1, "0000") & ". Continuar ?", vbQuestion + vbYesNo) = vbNo Then
             MsgBox "Processo cancelado.", vbCritical
             rdocn.RollbackTrans
             Unload Me: Call TerminaSEGBR
             End
          End If
       Else
          MensagemBatch "J� existe um arquivo de Ap�lice gerado com a vers�o " & Format(NumRemessaApolice1, "0000")
          rdocn.RollbackTrans
          Unload Me: Call TerminaSEGBR
          End
       End If
    End If
End If

HeaderCliente = "01" & Left("SEGA8055" & Space(8), 8) & NumRemessaApolice1 & _
                 Left(DataHora & Space(10), 10)
HeaderCliente = Left(HeaderCliente + Space(tam_reg), tam_reg)

If Trim(Nome_Arq1) <> "" Then
    Arq1 = FreeFile
    Open Nome_Arq1 For Output As Arq1
    Print #Arq1, HeaderCliente   'Grava o header do arquivo
End If

If Trim(Nome_Arq1) <> "" Then
    txtArq(0).Text = "SEGA8055" & "." & Format(NumRemessaApolice1, "0000")
    txtArq(0).Refresh
    arquivo_remessa = "SEGA8055" & "." & Format(NumRemessaApolice1, "0000")
End If

Exit Sub
    
Erro:
   If Arq1 = 0 Then
      MensagemBatch "Erro na Rotina : Abre Arquivo. Programa ser� Cancelado", vbCritical
   Else
      MensagemBatch "Erro na cria��o do arquivo: " & Chr(13) & UCase(Nome_Arq1 & arquivo1 & ".txt") & Chr(13) & "Programa ser� Cancelado", vbCritical
   End If
   Unload Me: Call TerminaSEGBR
   End
End Sub

Private Sub Ler_Clausulas()
Dim Clausula   As String, QTD As Long, ContClausula As Integer, ClausulaAux As String
Dim linha As Integer, ultQuebra As Long, Ultpos As Long, i As Long, QuebraPagina As String
Dim aux As String, tamFinal As Long, nLinhaClausula As New Clausula, LinhasClausula As Long
Dim vLinhaClausula As Clausula, RegClausula As String, idAnt As Integer, Mudouclausula As Boolean

On Error GoTo Erro
'separa o texto da cl�usula em linhas para ser gravado no arquivo
SQL = "SELECT cast(texto_clausula as varchar(8000)) as 'texto_clausula' FROM clausula_personalizada_TB (nolock)  "
SQL = SQL & " WHERE proposta_id = " & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (endosso_id=0 OR endosso_id is null)"
Else
   SQL = SQL & " AND endosso_id=" & num_endosso
End If
Set rc = rdocn.OpenResultset(SQL)

ContClausula = 0
Do While Not rc.EOF
   ContClausula = ContClausula + 1
   
   ClausulaAux = CorrigeTextoClausula(IIf(IsNull(rc!Texto_Clausula), "", rc!Texto_Clausula))
   Clausula = Formata_Clausula(ClausulaAux)
   tamFinal = 0
   ultQuebra = 1 ': Ultpos = 0 'Tamanho da string (+- 90 caracteres)
   For i = 1 To Len(Clausula)
      'Procura mudan�a de linha no texto
      If Mid(Clausula, i, 1) = Chr(13) Then
 '        If ultQuebra > 0 And Ultpos - 2 > 0 Then
            aux = Mid(Clausula, ultQuebra, i - ultQuebra)
'         Else
'            aux = ""
'         End If
         With nLinhaClausula
            .Id = ContClausula
            .LinhaClausula = Space(5) & aux
         End With
         Clausulas.Add nLinhaClausula
         If i < Len(Clausula) Then
            If Mid(Clausula, i + 1, 1) = Chr(10) Then
               i = i + 1
            End If
         End If
         LinhasClausula = LinhasClausula + 1
         Set nLinhaClausula = Nothing
         'tamFinal = Ultpos
         'Ultpos = 0 'Tamanho da string da �ltima quebra  at� a pr�xima
         ultQuebra = i + 1 '  2 'Qdo acha a quebra de linha, guarda a posi��o para ser o in�cio da pr�xima linha
       End If
       'Ultpos = Ultpos + 1
   Next
   rc.MoveNext
'   If Ultpos > 0 Then 'Ent�o ainda falta texto p/ ser impresso depois da �ltima quebra
   If ultQuebra < i Then 'Ent�o ainda falta texto p/ ser impresso depois da �ltima quebra
      aux = Mid(Clausula, ultQuebra, Ultpos)
      With nLinhaClausula
         .Id = ContClausula
         .LinhaClausula = Space(5) & aux
      End With
      Clausulas.Add nLinhaClausula
      Set nLinhaClausula = Nothing
      LinhasClausula = LinhasClausula + 1
   End If
Loop
rc.Close
     
If EndossoAnexo Then   'Se imprimiu descr. do endosso no anexo, imprimir cl�usula tb no anexo
   RegClausula = 22
Else
   If QtdLinhasCobertura = 0 And EndossoDescricao.Count = 0 Then 'Se n�o t�m coberturas nem descri��o de endosso, pode imprimir cl�usula na p�g da frente, se couber
      If (LinhasClausula + 1) <= 16 Then  'Verificar se cabe cl�usula
         RegClausula = 21
      Else
         RegClausula = 22
      End If
   Else
      If Not CoberturasPrimPagina Then 'Se tem cobertura e as mesmas n�o foram listadas na 1� p�g
         RegClausula = 22               'listar cl�usuas tb nos anexos
      Else
         If (QtdLinhasCobertura + LinhasClausula + 1) <= 16 Then  'Verificar se cabe coberturas + cl�usula + t�tulo cl�usula
            RegClausula = 21
         Else
            RegClausula = 22
         End If
      End If
   End If
End If
     
If Clausulas.Count > 0 Then
   'Pular uma linha
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "CL�USULA(S) "
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
End If
i = 0: Mudouclausula = False
For Each vLinhaClausula In Clausulas
   If vLinhaClausula.Id <> idAnt Then
      i = i + 1
      Mudouclausula = True
      idAnt = vLinhaClausula.Id
   Else
      Mudouclausula = False
   End If
   If i <> 1 Then 'Se n�o for a primeira cl�usula
      If Mudouclausula Then
         'Quebra de p�gina a cada mudan�a de cl�usula
         QuebraPagina = "S"
      Else
         QuebraPagina = Space(1)
      End If
   Else
      QuebraPagina = Space(1)
   End If
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Left(vLinhaClausula.LinhaClausula & Space(100), 100)
   Reg = Reg & QuebraPagina
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
Next
Set Clausulas = Nothing
     
'If Not rc.EOF Then
'   'T�tulo
'   Reg = "4" & Format(ContaLinhaAtual, "000000") & num_proposta
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
'   Reg = "4" & Format(ContaLinhaAtual, "000000") & num_proposta & "CL�USULAS "
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
'
'   Do While Not rc.EOF
'      tamFinal = 0
'      Reg = "4" & Format(ContaLinhaAtual, "000000") & num_proposta
'      Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'      IncrementaLinha
'
'      TextoClausula.Text = rc!texto_clausula
'      Clausula = Formata_Clausula(TextoClausula)
'      'Clausula = TextoClausula.Text
'      ultQuebra = 1: Ultpos = 0 'Tamanho da string (+- 90 caracteres)
'      For i = 1 To Len(Clausula)
'         If Mid(Clausula, i, 2) = Chr(13) & Chr(10) Then
'            If ultQuebra > 0 And Ultpos - 2 > 0 Then
'               aux = Mid(Clausula, ultQuebra, Ultpos - 2)
'            Else
'               aux = ""
'            End If
'            Reg = "4" & Format(ContaLinhaAtual, "000000") & num_proposta
'            Reg = Reg & Space(5) & aux
'            Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'            IncrementaLinha
'            tamFinal = Ultpos
'            Ultpos = 0
'            ultQuebra = i + 2
'          End If
'          Ultpos = Ultpos + 1
'      Next
'      rc.MoveNext
'      If Ultpos > 0 Then 'Ent�o ainda falta texto p/ ser impresso depois da �ltima quebra
'         aux = Mid(Clausula, ultQuebra, Ultpos)
'         Reg = "4" & Format(ContaLinhaAtual, "000000") & num_proposta
'         Reg = Reg & Space(5) & aux
'         Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'         IncrementaLinha
'      End If
'   Loop
'End If
'
'rc.Close

Set Clausulas = Nothing
Exit Sub

Erro:
   TrataErroGeral "Ler_Clausulas", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Sub Processa_Coberturas()
Dim RegClausula As Integer, i As Long, j As Long, linhaFranquia As String, ObjAnteriorBenef As Long
Dim ObjAnterior As Long, Endereco As String, linhasCobertura As Long, PriVez As String, k As Long, aux As String
Dim rs As rdoResultset

On Error GoTo Erro
If QtdLinhasCobertura > 16 Then
   If EndossoDescricao.Count > 16 Or EndossoDescricao.Count = 0 Then
      'Tr�s linhas em branco
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(20) & String(26, "*")
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(20) & "CONTRATA��O CONFORME ANEXO"
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Space(20) & String(26, "*")
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
   CoberturasPrimPagina = False
   RegClausula = 22
   linhasCobertura = 3
ElseIf QtdLinhasCobertura = 0 Then
   CoberturasPrimPagina = False
   RegClausula = 21
   linhasCobertura = 0
ElseIf QtdLinhasCobertura <= 16 Then
   If Not CoberturasProdutoAnexo Then
      CoberturasPrimPagina = True
      RegClausula = 21
      linhasCobertura = QtdCoberturas
   Else
      CoberturasPrimPagina = False
      If EndossoDescricao.Count > 16 Or EndossoDescricao.Count = 0 Then
         TextoCoberturasAnexo = True
         
         'Tr�s linhas em branco
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & Space(20) & String(26, "*")
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & Space(20) & "CONTRATA��O CONFORME ANEXO"
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         
         Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & Space(20) & String(26, "*")
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      End If
      RegClausula = 22
      linhasCobertura = QtdCoberturas
   End If
End If
ObjAnterior = 0
'Para cada objeto segurado, listar coberturas e benefici�rios
If QtdObjetos > 0 Then
   If TranspInternacional Then
      Lista_CoberturasTransp RegClausula
   Else
      For k = 1 To QtdObjetos
         'Atualiza obj Anterior
         ObjAnterior = EnderecoRisco(0, k)
         'T�tulo do item
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & "ITEM " & Format$(EnderecoRisco(0, k), "00") & ":" & Space(8)
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
         
         If ramo_id <> "22" Then 'Para transporte internacional, n�o listar endere�o de risco
            'Local do Risco
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
            Reg = Reg & "LOCAL DO RISCO: " & EnderecoRisco(1, k)
            Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            linhasCobertura = linhasCobertura + 1
            If produto_externo_id = 1111 Then
               'Listar RUBRICA e LOC
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
               Reg = Reg & "RUBRICA       : " & Format(EnderecoRisco(2, k), "@@@@\.@@")
               Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
               ContaLinhaAtual = ContaLinhaAtual + 1
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
               Reg = Reg & "L.O.C.        : " & EnderecoRisco(3, k)
               'Classe Ocupac�o
               SQL = "Select classe_Ocupacao_id "
               SQL = SQL & "From rubrica_tb (nolock)  Where "
               SQL = SQL & "tp_rubrica = 2 And "
               SQL = SQL & "cod_rubrica ='" & EnderecoRisco(2, k) & "'"
               Set rs = rdocn.OpenResultset(SQL)
               If Not rs.EOF Then
                  Reg = Reg & "." & Val(0 & rs(0))
               Else
                  Reg = Reg & "." & " "
               End If
               rs.Close
               'constru��o
               Reg = Reg & "." & EnderecoRisco(4, k)
               Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
               ContaLinhaAtual = ContaLinhaAtual + 1
            End If
         End If
         If QtdCoberturas <> 0 Then
            PriVez = True
            For i = 0 To QtdCoberturas - 1
               If Cobertura(0, i) = EnderecoRisco(0, k) Then
               
                  If ramo_id <> "22" Then
                     If PriVez Then
                         'T�tulo Coberturas
                        Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
                        'Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA" & Space(178) & "I.S.(" & MoedaSeguro & ")"       '200 caracteres para a descri��o 27/10/2003
                        Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA" & Space(50) & "I.S.(" & MoedaSeguro & ")"       '72 caracteres para a descri��o 19/10/2004
                        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                        ContaLinhaAtual = ContaLinhaAtual + 1
                        linhasCobertura = linhasCobertura + 1
                        PriVez = False
                     End If
                     'C�d e descri��o
                     Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
                     Reg = Reg & Format$(Cobertura(1, i), "000") & Space(8)
                     'Reg = Reg & Left(Cobertura(2, i) & Space(200), 200)     '200 caracteres para a descri��o sem UCase 27/10/2003
                     Reg = Reg & Left(Cobertura(2, i) & Space(72), 72)     '72 caracteres para a descri��o sem UCase 19/10/2004
                     
                     Reg = Reg & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
                     Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                     ContaLinhaAtual = ContaLinhaAtual + 1
                  Else
                     If CDbl(Cobertura(3, i)) <> 0 Then
                        Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(11) & "IMPORT�NCIA SEGURADA :  (" & MoedaSeguro & ")"
                        Reg = Reg & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
                        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                        ContaLinhaAtual = ContaLinhaAtual + 1
                     End If
                  End If
'                  'Imp Segurada
'                  If ConfiguracaoBrasil Then
'                      Reg = Reg & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
'                  Else
'                      Reg = Reg & Right(Space(16) & TrocaValorAmePorBras(Format(Cobertura(3, i), "#,###,###,##0.00")), 16)
'                  End If
'                  Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'                  IncrementaLinha
                  
                  'Franquia
                  linhaFranquia = ""
                  If Cobertura(4, i) <> 0 Or Cobertura(5, i) <> "" Or Cobertura(6, i) <> 0 Then
                     Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(11) & "FRANQUIA : "
                     If Cobertura(4, i) <> 0 Then
                        linhaFranquia = linhaFranquia & Right(Space(8) & Format(Cobertura(4, i), "##0.00") & " %", 8)
                     End If
                     If Cobertura(5, i) <> "" Then
                        'Colocar separador caso o anterior estiver preenchido
                        If linhaFranquia <> "" Then
                           linhaFranquia = linhaFranquia & " - "
                        End If
                        linhaFranquia = linhaFranquia & Trim(Cobertura(5, i))
                     End If
                     If Cobertura(6, i) <> 0 Then
                        'Colocar separador caso o anterior estiver preenchido
                        If linhaFranquia <> "" Then
                           linhaFranquia = linhaFranquia & " - "
                        End If
                        linhaFranquia = linhaFranquia & "M�nimo de: " & MoedaSeguro & " " & Format(Cobertura(6, i), "#,###,###,##0.00")
                     End If
                     Reg = Reg & linhaFranquia
                     Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                     ContaLinhaAtual = ContaLinhaAtual + 1
                  End If
               End If
            Next
         End If
         'Monta linhas de Benefici�rios do obj. segurado
         Lista_Beneficiarios RegClausula, EnderecoRisco(0, k)
         
         'Pula uma linha para cada novo obj. segurado
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
      Next
   End If
Else
   'No caso de ENDOSSO, os objetos podem n�o ter sido alterados
   If QtdCoberturas > 0 Then
      Lista_Coberturas (RegClausula)
   ElseIf QtdBenefs > 0 Then
      Lista_Beneficiarios RegClausula
   End If
End If

'Total Descontos
'If ValTotDesconto <> 0 Then
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Reg = Reg & "Total de descontos : " & Left(MoedaPremio + Format(ValTotDesconto, "#,###,###,##0.00") & Space(16), 16)
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
'   'pular linha
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
'End If
        
If QtdCongeneres > 0 Then
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & String(100, " ")
'   Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'   IncrementaLinha
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & "CONG�NERE(S)" & Space(69) & "PERC. PARTICIPA��O"
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   For i = 0 To (QtdCongeneres - 1)
      aux = Left(Congenere(0, i) & Space(59), 59) & Right(Space(40) & Congenere(1, i), 40)
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & aux
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   Next
End If
        
        
Exit Sub

Erro:
   TrataErroGeral "Processa_Coberturas", Me.Name
   On Error Resume Next
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Sub Lista_Coberturas(ByVal RegClausula As Integer)
Dim ObjAnterior As Integer, i As Long, j As Long, Endereco As String, linhaFranquia As String

ObjAnterior = 0
For i = 0 To QtdCoberturas - 1
   If Cobertura(0, i) <> ObjAnterior Then
      If i > 0 Then
         'Monta linhas de Benefici�rios do obj. segurado anterior
         Lista_Beneficiarios RegClausula, ObjAnterior
         
         'Pula uma linha para cada novo obj. segurado
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      End If
      
      'Atualiza obj Anterior
      ObjAnterior = Cobertura(0, i)

      'T�tulo do item
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "ITEM " & Format$(Cobertura(0, i), "00") & ":" & Space(8)
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      'Local do Risco
'      For j = 1 To QtdObjetos
'         If EnderecoRisco(0, j) = Cobertura(0, i) Then
'            Endereco = EnderecoRisco(1, j)
'         End If
'      Next
'      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'      Reg = Reg & "LOCAL DO RISCO: " & Endereco
'      Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
'      IncrementaLinha
       'T�tulo Coberturas
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      'Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA" & Space(178) & "I.S(" & MoedaSeguro & ")"      '200 caracteres para a descri��o 27/10/2003
      Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA" & Space(50) & "I.S(" & MoedaSeguro & ")"      '72 caracteres para a descri��o 19/10/2004
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & Format$(Cobertura(1, i), "000") & Space(8)
   'Reg = Reg & Left(Cobertura(2, i) & Space(200), 200)             '200 caracteres para a descri��o sem UCase 27/10/2003
   Reg = Reg & Left(Cobertura(2, i) & Space(72), 72)             '72 caracteres para a descri��o sem UCase 19/10/2004
               
   'Imp Segurada
   If ConfiguracaoBrasil Then
       Reg = Reg & MoedaSeguro & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
   Else
       Reg = Reg & MoedaSeguro & Right(Space(16) & TrocaValorAmePorBras(Format(Cobertura(3, i), "#,###,###,##0.00")), 16)
   End If
   Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   
   'Franquia
   linhaFranquia = ""
   If Cobertura(4, i) <> 0 Or Cobertura(5, i) <> "" Or Cobertura(6, i) <> 0 Then
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "FRANQUIA : "
      If Cobertura(4, i) <> 0 Then
         If ConfiguracaoBrasil Then
             linhaFranquia = linhaFranquia & Right(Space(8) & Format(Cobertura(4, i), "##0.00") & " %", 8)
         Else
             linhaFranquia = linhaFranquia & Right(Space(8) & TrocaValorAmePorBras(Format(Cobertura(4, i), "##0.00")) & " %", 8)
         End If
      End If
      If Cobertura(5, i) <> "" Then
         'Colocar separador caso o anterior estiver preenchido
         If linhaFranquia <> "" Then
            linhaFranquia = linhaFranquia & " - "
         End If
         linhaFranquia = linhaFranquia & Trim(Cobertura(5, i))
      End If
      If Cobertura(6, i) <> 0 Then
         'Colocar separador caso o anterior estiver preenchido
         If linhaFranquia <> "" Then
            linhaFranquia = linhaFranquia & " - M�nimo de: " & MoedaSeguro & " "
         End If
         If ConfiguracaoBrasil Then
            linhaFranquia = linhaFranquia & Format(Cobertura(6, i), "#,###,###,##0.00")
         Else
            linhaFranquia = linhaFranquia & TrocaValorAmePorBras(Format(Cobertura(6, i), "#,###,###,##0.00"))
         End If
     End If
      Reg = Reg & linhaFranquia
      Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
Next

'Monta linhas de Benefici�rios do �ltimo obj. segurado
Lista_Beneficiarios RegClausula, ObjAnterior

'Pulando uma linha...
Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & String(16, " ")
Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
ContaLinhaAtual = ContaLinhaAtual + 1

End Sub

Private Sub Ler_Cliente()
Dim PK As String, TABLE As String, CAMPOS As String
   
On Error GoTo Erro
propostaAnterior = 0
SQL = "SELECT c.nome, isnull(pf.cpf,'') as CPF, pj.pj_cliente_id, "
'SQL = SQL & " convert(char(12), isnull(pf.dt_nascimento, ''), 103), "
SQL = SQL & " isnull(pj.cgc,'') as CGC, e.endereco, e.bairro, e.municipio, "
SQL = SQL & " e.estado, e.cep, c.ddd_1 , c.telefone_1, p.produto_id,"
SQL = SQL & " pd.Nome as Nom_Prod, p.proposta_id_anterior "
If processo_susep = "" Then
    SQL = SQL & ",pd.num_proc_susep "
End If
SQL = SQL & " FROM proposta_tb p INNER JOIN cliente_tb c (nolock)  "
SQL = SQL & " ON (p.prop_cliente_id = c.cliente_id)"
SQL = SQL & " INNER JOIN produto_tb pd (nolock) "
SQL = SQL & " ON (p.produto_id=pd.produto_id)"
SQL = SQL & " INNER JOIN endereco_corresp_tb e (nolock) "
SQL = SQL & " ON (p.proposta_id=e.proposta_id)"
SQL = SQL & " LEFT JOIN pessoa_fisica_tb pf (nolock) "
SQL = SQL & " ON (pf_cliente_id = p.prop_cliente_id)"
SQL = SQL & " LEFT JOIN pessoa_juridica_tb pj (nolock) "
SQL = SQL & " ON (pj_cliente_id = p.prop_cliente_id)"
SQL = SQL & " WHERE  p.proposta_id = " & num_proposta

Set rc = rdocn.OpenResultset(SQL)
If Not rc.EOF Then
   
   If processo_susep = "" Then
      processo_susep = Trim("" & rc!num_proc_susep)
   End If
   
   If TpEmissao = "A" Then
      Reg = "20" & Format(ContaLinhaAtual, "000000") & num_proposta & num_apolice & "APOLICE"
   Else
      Reg = "20" & Format(ContaLinhaAtual, "000000") & num_proposta & num_apolice & "ENDOSSO"
   End If
   'Nome
   If Not IsNull(rc!nome) Then
       Reg = Reg & "SEGURADO  : " & UCase(Left(rc!nome & Space(60), 60))
   Else
       Reg = Reg & "SEGURADO  : " & Space(60)
   End If
   'Cgc/Cpf
   If IsNull(rc!pj_cliente_id) Then
       Reg = Reg & "CPF       : " & Left(Format(rc!CPF, "&&&.&&&.&&&-&&") & "    " & Space(18), 18)
   Else
       Reg = Reg & "CNPJ      : " & Left(Format(rc!CGC, "&&.&&&.&&&/&&&&-&&") & Space(18), 18)
   End If
   'Endere�o
   If Not IsNull(rc!Endereco) Then
       Reg = Reg & "ENDERECO  : " & UCase(Left(("" & rc!Endereco) & Space(60), 60))
   Else
       Reg = Reg & "ENDERECO  : " & Space(60)
   End If
   'Bairro
   If Trim("" & rc!Bairro) <> "" Then
       Reg = Reg & UCase(Left(rc!Bairro & Space(32), 32))
   Else
       Reg = Reg & Space(32)
   End If
   'Cidade
   If Not IsNull(rc!Municipio) Then
       Reg = Reg & "MUNICIPIO : " & UCase(Left(rc!Municipio & Space(35), 35))
   Else
       Reg = Reg & "MUNICIPIO : " & Space(35)
   End If
   'UF
   If Not IsNull(rc!Estado) Then
       Reg = Reg & UCase(Left(rc!Estado & Space(2), 2))
   Else
       Reg = Reg & "  "
   End If
   'CEP
   If Not IsNull(rc!Cep) And Trim(rc!Cep) <> "" Then
       Reg = Reg & "C.E.P.    : " & Left(Format$(rc!Cep, "&&&&&-&&&") & Space(9), 9)
   Else
       Reg = Reg & "C.E.P.    : " & Space(9)
   End If
   'Produto
   ProdutoId = rc!produto_id
   NomeProduto = rc!nom_prod
   propostaAnterior = Val(0 & rc!proposta_id_anterior)
    
   Reg = Reg & Format$(ProdutoId, "000")
   Reg = Reg & UCase(Left(NomeProduto & Space(33), 33))
   
   'Busca Produto Externo
   SQL = "SELECT produto_externo_id "
   SQL = SQL & "FROM arquivo_produto_tb (nolock)  "
   SQL = SQL & "WHERE produto_id=" & rc!produto_id
   Set Rc1 = rdocn2.OpenResultset(SQL)
   If Not Rc1.EOF Then
      produto_externo_id = Rc1!produto_externo_id
   Else
      produto_externo_id = 0
   End If
   Rc1.Close
End If
    
rc.Close
Exit Sub

Erro:
   TrataErroGeral "Ler_Cliente", Me.Name
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Sub Processa_Controle_Documento()
Dim PK As String, TABLE As String, CAMPOS As String
Dim rc As rdoResultset
   
On Error GoTo Erro
SQL = "SELECT c.nome, pf.cont_agencia_id, age.nome 'nome_agen', age.endereco 'endereco_agen', age.bairro 'bairro_agen', age.estado 'estado_agen', age.cep 'cep_agen', "
SQL = SQL & " e.endereco, e.bairro, e.municipio, "
SQL = SQL & " e.estado, e.cep "
SQL = SQL & " FROM proposta_tb p (nolock)  INNER JOIN cliente_tb c (nolock)  "
SQL = SQL & " ON (p.prop_cliente_id = c.cliente_id)"
SQL = SQL & " INNER JOIN endereco_corresp_tb e (nolock) "
SQL = SQL & " ON (p.proposta_id=e.proposta_id)"
SQL = SQL & " LEFT JOIN proposta_fechada_tb pf (nolock) "
SQL = SQL & " ON (pf.proposta_id=p.proposta_id)"
SQL = SQL & " LEFT JOIN agencia_tb age (nolock) "
SQL = SQL & " ON  (pf.cont_agencia_id = age.agencia_id)"
SQL = SQL & " AND (pf.cont_banco_id = age.banco_id)"
SQL = SQL & " WHERE  p.proposta_id = " & num_proposta

Set rc = rdocn2.OpenResultset(SQL)

If Not rc.EOF Then
   
   If (Destino_id <> "A") Or (Not IsNull(rc!cont_agencia_id)) Then
   
        Reg = "10" & Format(ContaLinhaAtual, "000000") & num_proposta
        
        'Nome do destinat�rio
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!nome) Then
                 Reg = Reg & UCase(Left(rc!nome & Space(60), 60))
             Else
                 Reg = Reg & Space(60)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left(Diretoria_id & Space(60), 60))
        Else
             Reg = Reg & UCase(Left("AG�NCIA: " & rc!nome_agen & Space(60), 60))
        End If
        'Endere�o
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Endereco) Then
                 Reg = Reg & UCase(Left(("" & rc!Endereco) & Space(60), 60))
             Else
                 Reg = Reg & Space(60)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("RUA MANOEL DA NOBREGA, 1280 - 8�/9� ANDAR" & Space(60), 60))
        Else
             Reg = Reg & UCase(Left(("" & rc!endereco_agen) & Space(60), 60))
        End If
        'Bairro
        If Destino_id = "" Or Destino_id = "C" Then
             If Trim("" & rc!Bairro) <> "" Then
                 Reg = Reg & UCase(Left(rc!Bairro & Space(30), 30))
             Else
                 Reg = Reg & Space(30)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("PARA�SO" & Space(30), 30))
        Else
             Reg = Reg & UCase(Left(rc!bairro_agen & Space(30), 30))
        End If
        'Municipio
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Municipio) Then
                 Reg = Reg & UCase(Left(rc!Municipio & Space(30), 30))
             Else
                 Reg = Reg & Space(30)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("S�O PAULO" & Space(30), 30))
        Else
             Reg = Reg & Space(30)
        End If
        'UF
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Estado) Then
                 Reg = Reg & UCase(Left(rc!Estado & Space(2), 2))
             Else
                 Reg = Reg & "  "
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & "SP"
        Else
             Reg = Reg & UCase(Left(rc!estado_agen & Space(2), 2))
        End If
        'CEP
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Cep) And Trim(rc!Cep) <> "" Then
                 Reg = Reg & Left(Format$(rc!Cep, "&&&&&-&&&") & Space(9), 9)
             Else
                 Reg = Reg & Space(9)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & "04001-004"
        Else
             Reg = Reg & Left(Format$(rc!cep_agen, "&&&&&-&&&") & Space(9), 9)
        End If
        'Aos Cuidados de
        If Destino_id = "" Or Destino_id = "C" Then
             Reg = Reg & "A/C" & Space(60)
        ElseIf Destino_id = "D" Then
             Reg = Reg & "A/C" & Space(60)
        Else
             Reg = Reg & "A/C" & Space(60)
        End If
        'Referencia
        Reg = Reg & "Ref."
        'Nome do segurado
        If Not IsNull(rc!nome) Then
            Reg = Reg & UCase(Left(rc!nome & Space(60), 60))
        Else
            Reg = Reg & Space(60)
        End If
        'C�digo de barras ddpppppppppjjjjjaaaa
        If TpEmissao = "A" Then
            Reg = Reg & "04" 'Tipo_documento ap�lice
        ElseIf TpEmissao = "E" Then
            Reg = Reg & "06" 'Tipo_documento endosso
        End If
        Reg = Reg & num_proposta         'Proposta_id
        Reg = Reg & ConverteParaJulianDate(CDate(Data_Sistema))       'data
        'C�digo da ag�ncia
        If Not IsNull(rc!cont_agencia_id) And Trim(rc!cont_agencia_id) <> "" Then
           Reg = Reg & Format(rc!cont_agencia_id, "0000")         'C�digo da ag�ncia
        Else
           Reg = Reg & "0000"
        End If
        'N�mero da AR
        Reg = Reg & Space(15)
        
        flagEnderecoAgencia = True
   Else
        flagEnderecoAgencia = False
        rc.Close
        Exit Sub
   End If
End If
rc.Close

Reg = Left(Reg & Space(tam_reg), tam_reg)
Print #Arq1, Reg
ContaLinhaAtual = ContaLinhaAtual + 1

Exit Sub

Erro:
   TrataErroGeral "Processa_Controle_Documento", Me.Name
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub
Private Sub Ler_Endereco_Risco()
Dim Endereco As String, InicioVigencia As String, FimVigencia As String
Dim EndRisco As String, PriVez As Boolean
On Error GoTo Erro

SQL = "SELECT 1, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id "
SQL = SQL & ", b.cod_rubrica rubrica, b.classe_local_id Local, b.tp_construcao_id construcao, null atividade " 'c.nome atividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_empresarial_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
'Sql = Sql & "   LEFT JOIN tp_atividade_tb c (nolock)  ON b.tp_atividade_id=c.tp_atividade_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 2, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao, null atividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_residencial_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id  "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 3, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao, c.nome tividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_condominio_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   LEFT JOIN tp_condominio_tb c (nolock)  ON b.tp_condominio_id=c.tp_condominio_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 4, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao , null atividade "
SQL = SQL & "   FROM endereco_risco_tb a (nolock)  INNER JOIN seguro_maquinas_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 5, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao,  null atividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_aceito_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 6, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao,  null atividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_avulso_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 7, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao, null atividade "
SQL = SQL & "   FROM  endereco_risco_tb a (nolock)  INNER JOIN seguro_generico_tb b (nolock)  ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
SQL = SQL & " UNION "
SQL = SQL & "SELECT 8, 0, '' endereco, '' bairro, '' municipio, '  ' estado, '' cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & ", ' ' rubrica, 0 Local, 0 construcao,  null atividade "
SQL = SQL & "   FROM  seguro_transporte_tb b (nolock)  "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (b.endosso_id=0 or b.endosso_id is null) "
Else
   SQL = SQL & " b.endosso_id=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
Set rc = rdocn.OpenResultset(SQL)
If Not rc.EOF Then
   Subramo = rc!SUBRAMO_ID
Else
   Subramo = 0
End If
QtdObjetos = 0
ReDim EnderecoRisco(5, 5)
TabEscolha = ""
If Not rc.EOF Then
   If (Not IsNull(rc!atividade)) And (produto_externo_id <> 1111) Then
      atividadePrincipal = Trim("" & rc!atividade)
   Else
      atividadePrincipal = ""
   End If
End If

PriVez = True

Do While Not rc.EOF
   Select Case rc(0)
   Case 1
      TabEscolha = "escolha_tp_cob_emp_tb"
   Case 2
      TabEscolha = "escolha_tp_cob_res_tb"
   Case 3
      TabEscolha = "escolha_tp_cob_cond_tb"
   Case 4
      TabEscolha = "escolha_tp_cob_maq_tb"
   Case 5
      TabEscolha = "escolha_tp_cob_aceito_tb"
   Case 6
      TabEscolha = "escolha_tp_cob_avulso_tb"
   Case 7
      TabEscolha = "escolha_tp_cob_generico_tb"
   Case 8
      TabEscolha = "escolha_verba_transporte_tb"
   End Select
   Endereco = ""
   'Endere�o Risco
   If Trim(rc!Endereco) <> "" Then
       Endereco = UCase(Trim(rc!Endereco))
   End If
   'Bairro Risco
   If Trim(rc!Bairro) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Bairro))
   End If
   'Cidade Risco
   If Trim("" & rc!Municipio) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Municipio))
   End If
   'UF Risco
   If Trim("" & rc!Estado) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Estado))
   End If
   'Atribui o 1� endere�o de risco
   If PriVez Then EndRisco = Endereco: PriVez = False
   
   QtdObjetos = QtdObjetos + 1
   If QtdObjetos > 5 Then
      ReDim Preserve EnderecoRisco(5, QtdObjetos + 5)
   End If
   EnderecoRisco(0, QtdObjetos) = Val(0 & rc!cod_objeto_segurado)
   EnderecoRisco(1, QtdObjetos) = Endereco
   If produto_externo_id = 1111 Then
      QtdLinhasCobertura = QtdLinhasCobertura + 2
      EnderecoRisco(2, QtdObjetos) = Trim("" & rc!rubrica)  'rubrica
      EnderecoRisco(3, QtdObjetos) = Val(0 & rc!Local)      'local
      EnderecoRisco(4, QtdObjetos) = Val(0 & rc!construcao) 'construcao
   End If
   rc.MoveNext
   If Not rc.EOF Then
      'Se tem mais de um endere�o de risco e � o produto AVULSO
      If produto_externo_id = 999 Then
         EndRisco = String(11, " ") & "( DIVERSOS )"
      End If
   End If
Loop
If (ramo_id = "22" Or ramo_id = "44") Or Trim(EndRisco) = "" Then
   EndRisco = String(75, "*")
End If
Reg = Reg & Left(EndRisco & Space(112), 112)
rc.Close

If TpEmissao = "E" Then 'Verifica se � endosso de fatura, para buscar in�cio e fim do endosso
   SQL = "SELECT dt_inicio_vigencia, dt_fim_vigencia FROM fatura_tb (nolock)  "
   SQL = SQL & "   WHERE   apolice_id=" & num_apolice
   SQL = SQL & "   AND     sucursal_seguradora_id=" & Sucursal
   SQL = SQL & "   AND     seguradora_cod_susep=" & Seguradora
   SQL = SQL & "   AND     ramo_id=" & ramo_id
   SQL = SQL & "   AND     endosso_id=" & num_endosso
   Set rc = rdocn.OpenResultset(SQL)
   If Not rc.EOF Then
      InicioVigencia = Format$(rc(0), "dd/mm/yyyy")
      FimVigencia = Format$(rc(1), "dd/mm/yyyy")
   Else
      InicioVigencia = DtInicioVigencia
      FimVigencia = FimVig
   End If
   rc.Close
Else
   InicioVigencia = DtInicioVigencia
   FimVigencia = FimVig
End If

'In�cio Vig�ncia
INI = Mid(InicioVigencia, 1, 2) & " de "
Select Case Mid(InicioVigencia, 4, 2)
   Case "01": mes = "Janeiro"
   Case "02": mes = "Fevereiro"
   Case "03": mes = "Mar�o"
   Case "04": mes = "Abril"
   Case "05": mes = "Maio"
   Case "06": mes = "Junho"
   Case "07": mes = "Julho"
   Case "08": mes = "Agosto"
   Case "09": mes = "Setembro"
   Case "10": mes = "Outubro"
   Case "11": mes = "Novembro"
   Case "12": mes = "Dezembro"
End Select
INI = INI & mes & " de " & Mid(InicioVigencia, 7, 4)
Reg = Reg & Left(INI & Space(23), 23)
    
''Data Contratacao
'dtIniVigencia = DtInicioVigencia
'Fim Vig�ncia
If FimVigencia <> "" Then
   Fim = Mid(FimVigencia, 1, 2) & " de "
   Select Case Mid(FimVigencia, 4, 2)
      Case "01": mes = "Janeiro"
      Case "02": mes = "Fevereiro"
      Case "03": mes = "Mar�o"
      Case "04": mes = "Abril"
      Case "05": mes = "Maio"
      Case "06": mes = "Junho"
      Case "07": mes = "Julho"
      Case "08": mes = "Agosto"
      Case "09": mes = "Setembro"
      Case "10": mes = "Outubro"
      Case "11": mes = "Novembro"
      Case "12": mes = "Dezembro"
   End Select
   Fim = Fim & mes & " de " & Mid(FimVigencia, 7, 4)
Else
   Fim = ""
End If
Reg = Reg & Left(Fim & Space(23), 23)
      
Exit Sub

Erro:
   TrataErroGeral "Ler_Endereco_Risco", Me.Name
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub
Private Sub Ler_Proposta_Fechada()

Dim QtdParcelas As Long, rs As rdoResultset, i As Long, Valcobranca As Currency
Dim ValUltParcela As Currency, ValParcela1 As Currency, ValParcelaDemais As Currency
Dim ValIof As Currency, ValJuros As Currency, CustoApolice As Currency
Dim PremioTarifa As Currency, PremioBruto As Currency, QtdDatas As Integer
Dim DataAgendamento() As String, PremioLiquido As Currency, ValPagoAto As Currency
Dim Nome_agencia As String, agencia As String
On Error GoTo Erro
                
PagoAto = False
ParcelaUnica = False

SQL = "SELECT p.proposta_bb, p.proposta_id, p.cont_agencia_id, p.agencia_id, " & _
      " p.val_iof, p.custo_apolice, p.val_premio_tarifa, " & _
      " p.val_premio_bruto, p.num_parcelas, p.val_juros, " & _
      " p.val_pgto_ato, p.val_parcela, p.forma_pgto_id,  " & _
      " p.val_tot_desconto_comercial, m.sigla, m.nome, p.seguro_moeda_id, p.premio_moeda_id " & _
      " FROM proposta_fechada_tb p (nolock)  " & _
      " inner join moeda_tb as m (nolock)  " & _
      " on p.seguro_moeda_id = m.moeda_id " & _
      " WHERE proposta_id = " & num_proposta
Set rc = rdocn.OpenResultset(SQL)
        
If Not rc.EOF Then
   ValPagoAto = Val(0 & rc!val_pgto_ato)
   MoedaSeguroId = Val(0 & rc!seguro_moeda_id)
   MoedaSeguro = Trim("" & rc!sigla)
   NomeMoedaSeguro = Trim("" & rc!nome)
   If Val(0 & rc!premio_moeda_id) <> Val(0 & rc!seguro_moeda_id) Then
      SQL = "SELECT sigla, nome  FROM moeda_tb (nolock)  WHERE moeda_id=" & Val(0 & rc!premio_moeda_id)
      Set rs = rdocn.OpenResultset(SQL)
      MoedaPremio = Trim("" & rs!sigla)
      NomeMoedaPremio = Trim("" & rc!nome)
      rs.Close
   Else
      MoedaPremio = Trim("" & rc!sigla)
      NomeMoedaPremio = Trim("" & rc!nome)
   End If
   'Numero Endosso
   Reg = Reg & num_endosso
   'Renova Proposta
   If propostaAnterior = 0 Then
      If Val(0 & rc!proposta_bb) <> 0 Then
         SQL = "select a.apolice_id  from proposta_fechada_tb p (nolock)  INNER JOIN apolice_tb a (nolock)  "
         SQL = SQL & "  ON     a.proposta_id=p.proposta_id "
         SQL = SQL & "  WHERE  p.proposta_bb_anterior = " & rc!proposta_bb
'         Set Rc1 = rdocn.OpenResultset(SQL)
'         If Not Rc1.EOF Then
'            Reg = Reg & Format(Val(0 & Rc1(0)), "000000000")
'         Else
            Reg = Reg & "000000000"
'         End If
'         Rc1.Close
      Else
         Reg = Reg & "000000000"
      End If
   Else
      SQL = "SELECT apolice_id  FROM apolice_tb (nolock)  "
      SQL = SQL & "  WHERE  proposta_id = " & propostaAnterior
      Set Rc1 = rdocn.OpenResultset(SQL)
      If Not Rc1.EOF Then
         Reg = Reg & Format(Rc1(0), "000000000")
      Else
         Reg = Reg & "000000000"
      End If
   End If
   'Proposta_bb
   Reg = Reg & IIf(IsNull(rc!proposta_bb), "000000000", Format(rc!proposta_bb, "000000000"))
   'C�digo da ag�ncia
   ContAgencia = IIf(IsNull(rc!cont_agencia_id), "0000", Format(rc!cont_agencia_id, "0000"))
   'Ag�ncia Cliente
   Nome_agencia = ""
   If Not IsNull(rc!cont_agencia_id) Then
      Reg = Reg & Format(rc!cont_agencia_id, "0000")
      Nome_agencia = Format(rc!cont_agencia_id, "0000")
      'Nome Ag�ncia
      SQL = " SELECT nome FROM agencia_tb (nolock)  " & _
                " WHERE (agencia_id = " & rc!cont_agencia_id & ") AND (" & _
                " banco_id = 1)"
      Set Rc1 = rdocn.OpenResultset(SQL)
      If Not Rc1.EOF Then
         If Not IsNull(Rc1(0)) Then
            agencia = Trim(Rc1(0))
            If UCase(agencia) = "NAO IDENTIFICADA" Or UCase(agencia) = "N�O IDENTIFICADA" Then
               agencia = ""
            Else
               agencia = " - " & agencia
            End If
            Reg = Reg & UCase(Left(agencia & Space(51), 51)) '" - " & UCase(Left(Trim(Rc1(0)) & Space(36), 36))
            Nome_agencia = Nome_agencia + agencia
         Else
            Reg = Mid(Reg & Space(51), 1, 51)
         End If
      Else
          Reg = Mid(Reg & Space(51), 1, 51) ' Reg & Space(39)  '" - " & "NAO IDENTIFICADA" & Space(20)
      End If
      Rc1.Close
      Set Rc1 = Nothing
   Else
       Reg = Reg & Space(55)
   End If
   'Agencia Cobran�a
   'Preenchendo com ag�ncia contratante ao inv�s da ag�ncia do cliente... 23/07/2000
   'If Not IsNull(rc!agencia_id) Then
   '    Reg = Reg & Format(rc!agencia_id, "0000") & " - "
   '    'Nome Ag�ncia
   '    SQL = " SELECT nome FROM agencia_tb (nolock)  " & _
   '              " WHERE (agencia_id = " & rc!agencia_id & ") AND (" & _
   '              " banco_id = 1)"
   '    Set Rc1 = rdocn.OpenResultset(SQL)
   '    If Not Rc1.EOF Then
   '        If Not IsNull(Rc1(0)) Then
   '            Reg = Reg & UCase(Left(Trim(Rc1(0)) & Space(16), 16))
   '        Else
   '            Reg = Reg & Space(16)
   '        End If
   '    Else
   '        Reg = Reg & "NAO IDENTIFICADA"
   '    End If
   '    Rc1.Close
   '    Set Rc1 = Nothing
   'Else
   '    Reg = Reg & "9999 - NAO IDENTIFICADA"
   'End If
    
   Reg = Reg & Mid(Nome_agencia & Space(23), 1, 23)
    
   If TpEmissao = "E" Then 'Procurar valores em endosso_financeiro_tb
      SQL = "SELECT isnull(e.val_iof,0) val_iof, isnull(custo_apolice,0) custo_apolice , "
      SQL = SQL & " isnull(val_adic_fracionamento, 0) val_adic_fracionamento, "
      SQL = SQL & " isnull(val_premio_tarifa,0) val_premio_tarifa, "
      SQL = SQL & " isnull(val_desconto_comercial,0) val_desconto_comercial, "
      SQL = SQL & " isnull(num_parcelas,0) num_parcelas, seguro_moeda_id, premio_moeda_id,  "
      SQL = SQL & "   m.sigla, m.nome "
      SQL = SQL & "  FROM endosso_financeiro_tb e (nolock)  JOIN moeda_tb as m (nolock)  "
      SQL = SQL & " on e.seguro_moeda_id = m.moeda_id   Where "
      SQL = SQL & " e.proposta_id=" & num_proposta
      SQL = SQL & " AND e.endosso_id = " & num_endosso
      Set Rc1 = rdocn.OpenResultset(SQL)
      If Not Rc1.EOF Then
         ValIof = Val(Rc1!val_iof)
         CustoApolice = Val(Rc1!Custo_Apolice)
         ValJuros = Val(Rc1!val_adic_fracionamento)
         PremioTarifa = Val(Rc1!val_premio_tarifa)
         ValTotDesconto = Val(Rc1!val_desconto_comercial)
         QtdParcelas = Val(Rc1!num_parcelas)
         PremioBruto = PremioTarifa - ValTotDesconto + CustoApolice + ValIof + ValJuros
         
         MoedaSeguro = Rc1!sigla
         NomeMoedaSeguro = ("" & Rc1!nome)
         MoedaSeguroId = Rc1!seguro_moeda_id
         If Val(0 & Rc1!premio_moeda_id) <> Val(0 & Rc1!seguro_moeda_id) Then
            SQL = "SELECT moeda_id, sigla, nome  FROM moeda_tb (nolock)  WHERE moeda_id=" & Val(0 & Rc1!premio_moeda_id)
            Set rs = rdocn.OpenResultset(SQL)
            If Not rs.EOF Then
               MoedaPremio = Trim("" & rs!sigla)
               NomeMoedaPremio = Trim("" & rs!nome)
            Else
               MoedaPremio = MoedaSeguro
               NomeMoedaPremio = NomeMoedaSeguro
            End If
            rs.Close
         Else
            MoedaPremio = Trim("" & Rc1!sigla)
            NomeMoedaPremio = Trim("" & Rc1!nome)
         End If
      End If
      Rc1.Close
   Else
      ValIof = Val(0 & rc!val_iof)
      CustoApolice = Val(0 & rc!Custo_Apolice)
      ValJuros = Val(0 & rc!val_juros)
      PremioTarifa = Val(0 & rc!val_premio_tarifa)
      PremioBruto = Val(0 & rc!val_premio_bruto)
      ValTotDesconto = Val(0 & rc!val_tot_desconto_comercial)
      QtdParcelas = Val(0 & rc!num_parcelas)
      PremioBruto = PremioTarifa - ValTotDesconto + CustoApolice + ValIof + ValJuros
   End If
   'Valor IOF
   If ValIof <> 0 Then
      If ConfiguracaoBrasil Then
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + Format(ValIof, "#,###,###,##0.00") & Space(16), 16)
      Else
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + TrocaValorAmePorBras(Format(ValIof, "#,###,###,##0.00")) & Space(16), 16)
      End If
   Else
       Reg = Reg & Format(MoedaSeguro, "@@@") & "0,00" & String(9, " ")
   End If
   'Valor Custo Ap�lice
   If CustoApolice <> 0 Then
      If ConfiguracaoBrasil Then
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + Format(CustoApolice, "#,###,###,##0.00") & Space(16), 16)
      Else
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + TrocaValorAmePorBras(Format(CustoApolice, "#,###,###,##0.00")) & Space(16), 16)
      End If
   Else
      Reg = Reg & Format(MoedaSeguro, "@@@") & "0,00" & String(9, " ")
   End If
   'Valor Juros
   If ValJuros > 0 Then
      If ConfiguracaoBrasil Then
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + Format(ValJuros, "#,###,###,##0.00") & Space(16), 16)
      Else
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + TrocaValorAmePorBras(Format(ValJuros, "#,###,###,##0.00")) & Space(16), 16)
      End If
   Else
      Reg = Reg & Format(MoedaSeguro, "@@@") & "0,00" & String(9, " ")
   End If
   'Valor Pr�mio Liquido
   If PremioTarifa = 0 Then
      Reg = Reg & Format(MoedaSeguro, "@@@") & "0,00" & String(9, " ")
   Else
      If ConfiguracaoBrasil Then
         PremioLiquido = CCur(PremioTarifa) - CCur(ValTotDesconto)
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + Format(PremioLiquido, "#,###,###,##0.00") & Space(16), 16)
      Else
         PremioLiquido = CCur(TrocaValorAmePorBras(PremioTarifa)) - CCur(TrocaValorAmePorBras(ValTotDesconto))
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + TrocaValorAmePorBras(Format(PremioLiquido, "#,###,###,##0.00")) & Space(16), 16)
      End If
   End If
   'Valor Pr�mio Bruto
   If PremioBruto <> 0 Then
      If ConfiguracaoBrasil Then
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + Format(PremioBruto, "#,###,###,##0.00") & Space(16), 16)
      Else
         Reg = Reg & Left(Format(MoedaSeguro, "@@@") + TrocaValorAmePorBras(Format(PremioBruto, "#,###,###,##0.00")) & Space(16), 16)
      End If
   Else
      Reg = Reg & Format(MoedaSeguro, "@@@") & "0,00" & String(9, " ")
   End If
   'Valor Total Descontos ********* n�o ser� mais impresso
'   If ValTotDesconto <> 0 Then
'      QtdLinhasCobertura = QtdLinhasCobertura + 2 '1 linha p/ o desconto e uma linha em branco (p/ n�o ficar colado no limite de responsabilidade)
'      If ConfiguracaoBrasil Then
'         ValTotDesconto = Format(ValTotDesconto, "#,###,###,##0.00")
'      Else
'         ValTotDesconto = TrocaValorAmePorBras(Format(ValTotDesconto, "#,###,###,##0.00"))
'      End If
'   End If
'   'Qtd Parcelas
'   If QtdParcelas <> 0 Then
'      Reg = Reg & "Qt. Parcelas    : " & Format$(rc!num_parcelas, "00")
'   Else
'      Reg = Reg & "00"
'   End If
   SQL = "SELECT  val_cobranca, num_cobranca, dt_agendamento FROM agendamento_cobranca_tb (nolock)  "
   SQL = SQL & "  WHERE proposta_id=" & num_proposta
   SQL = SQL & "  AND seguradora_cod_susep=" & Seguradora
   SQL = SQL & "  AND sucursal_seguradora_id=" & Sucursal
   SQL = SQL & "  AND ramo_id=" & ramo_id
   SQL = SQL & "  AND situacao in ('a','e','i','r','p') "
   If TpEmissao = "A" Then
      SQL = SQL & "  AND (num_endosso=0 or num_endosso is null) "
   Else
      SQL = SQL & "  AND num_endosso=" & num_endosso
   End If
   SQL = SQL & "  ORDER BY num_cobranca"
   Set Rc1 = rdocn.OpenResultset(SQL)
   QtdDatas = 0
   If Not Rc1.EOF Then
      ReDim DataAgendamento(10)
      ValParcela1 = Val(Rc1(0))
      QtdDatas = QtdDatas + 1
      DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
      Rc1.MoveNext
      If Rc1.EOF Then    's� tem uma parcela
         ValParcelaDemais = 0
         ValUltParcela = 0
      Else 'tem mais de uma parcela
         ValParcelaDemais = Val(Rc1(0))
         ValUltParcela = 0
         QtdDatas = QtdDatas + 1
         DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
         Rc1.MoveNext
         Do While Not Rc1.EOF
            QtdDatas = QtdDatas + 1
            If QtdDatas Mod 10 = 0 Then
               ReDim Preserve DataAgendamento(QtdDatas + 10)
            End If
            DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
            ValUltParcela = Val(0 & Rc1(0))
            Rc1.MoveNext
         Loop
         If ValParcelaDemais = ValUltParcela Then
            ValUltParcela = 0
         End If
      End If
   Else
      ValParcela1 = 0
      ValParcelaDemais = 0
      ValUltParcela = 0
   End If
   Rc1.Close
   'Qtd Parcelas (Qtd. de datas em agendamento cobran�a)
   Reg = Reg & "Qt.Parcelas " & Format$(QtdDatas, "00")
   'Valor 1� Parcela
   If ValParcela1 <> 0 Then
      If ConfiguracaoBrasil Then
          Reg = Reg & "Parcela 1 : " & Format(MoedaSeguro, "@@@") & Right(Space(13) & Format(ValParcela1, "##,###,##0.00"), 13)
      Else
          Reg = Reg & "Parcela 1 : " & Format(MoedaSeguro, "@@@") & Right(Space(13) & TrocaValorAmePorBras(Format(ValParcela1, "##,###,##0.00")), 13)
      End If
   Else
      Reg = Reg & Space(28)
   End If
   'Valor Demais Parcelas
   If ValParcelaDemais <> 0 Then
       If ConfiguracaoBrasil Then
           Reg = Reg & "Demais Parcelas : " & Format(MoedaSeguro, "@@@") + Right(Space(13) + Format(ValParcelaDemais, "##,###,##0.00"), 13)
       Else
           Reg = Reg & "Demais Parcelas : " & Format(MoedaSeguro, "@@@") + Right(Space(13) + TrocaValorAmePorBras(Format(ValParcelaDemais, "##,###,##0.00")), 13)
       End If
   Else
       Reg = Reg & String(34, " ")
   End If
   'Valor �ltima Parcela
   If ValUltParcela <> 0 Then
       If ConfiguracaoBrasil Then
           Reg = Reg & "�ltima Parcela  : " & Format(MoedaSeguro, "@@@") + Right(Space(13) + Format(ValUltParcela, "##,###,##0.00"), 13)
       Else
           Reg = Reg & "�ltima Parcela  : " & Format(MoedaSeguro, "@@@") + Right(Space(13) + TrocaValorAmePorBras(Format(ValUltParcela, "##,###,##0.00")), 13)
       End If
   Else
       Reg = Reg & String(34, " ")
   End If
   'Tratar dt vencimento
   If QtdDatas <> 0 Then
      'se valor pago no ato est� preenchido, parcela j� foi paga
      If ValPagoAto <> 0 Then
          Reg = Reg & "01-� VISTA   "
          PagoAto = True
      Else
          Reg = Reg & "01-" & DataAgendamento(1)
      End If
      'Preenche Dts Vencimento
      For i = 2 To QtdDatas
          Reg = Reg & Format$(i, "00") & "-" & DataAgendamento(i)
      Next
   End If
   'Preenche vazios
   For i = QtdDatas + 1 To 12
       Reg = Reg & Space(13)
   Next
   If QtdDatas = 1 Then ParcelaUnica = True
   'Forma Cobran�a
   If Not IsNull(rc!forma_pgto_id) Then
       SQL = "SELECT nome FROM forma_pgto_tb (nolock)  " & _
             "WHERE forma_pgto_id = " & rc!forma_pgto_id
       Set Rc1 = rdocn.OpenResultset(SQL)
       If Not Rc1.EOF Then
           If Not IsNull(Rc1(0)) Then
               Reg = Reg & UCase(Left(Rc1(0) & Space(30), 30))
           End If
       Else
           Reg = Reg & String(30, "*")
       End If
       Rc1.Close
   Else
       Reg = Reg & String(30, "*")
   End If
End If
rc.Close
Exit Sub

Erro:
   TrataErroGeral "Ler_Proposta_Fechada", Me.Name
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Function Formata_Clausula(TextoClausula As String) As String

    Dim texto As String, ULTIMA_QUEBRA As Long
    Dim encontrou As Boolean, FRASE As String
    Dim CONT_CLAUSULA As Long, CONT_FRASE As Integer, TamanhoClausula As Long
    Dim ACHA_ESPACO As Long
    
    ULTIMA_QUEBRA = 1
    encontrou = False
    texto = ""
    CONT_FRASE = 0
    TamanhoClausula = Len(TextoClausula)
    For CONT_CLAUSULA = 1 To Len(TextoClausula)
        CONT_FRASE = CONT_FRASE + 1
        If Mid(TextoClausula, CONT_CLAUSULA, 1) = Chr(9) Then
          TextoClausula = Mid(TextoClausula, 1, CONT_CLAUSULA - 1) & Space(1) & Mid(TextoClausula, CONT_CLAUSULA + 1, Len(TextoClausula) - CONT_CLAUSULA)
          TamanhoClausula = Len(TextoClausula)
        End If
        If Mid(TextoClausula, CONT_CLAUSULA, 1) = Chr(13) Then
            FRASE = Mid(TextoClausula, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA) & " " & vbNewLine
            ULTIMA_QUEBRA = CONT_CLAUSULA + 2
            CONT_CLAUSULA = CONT_CLAUSULA + 1
            CONT_FRASE = 0
        ElseIf CONT_FRASE = 90 Then
            encontrou = False
            If Mid(TextoClausula, CONT_CLAUSULA + 1, 1) <> " " And Mid(TextoClausula, CONT_CLAUSULA + 1, 1) <> Chr(13) Then
                For ACHA_ESPACO = CONT_CLAUSULA To ULTIMA_QUEBRA Step -1
                    If Mid(TextoClausula, ACHA_ESPACO, 1) = " " Then
                        FRASE = Mid(TextoClausula, ULTIMA_QUEBRA, ACHA_ESPACO - ULTIMA_QUEBRA) & vbNewLine
                        CONT_FRASE = CONT_CLAUSULA - ACHA_ESPACO
                        ULTIMA_QUEBRA = ACHA_ESPACO + 1
                        encontrou = True
                        Exit For
                    End If
                Next ACHA_ESPACO
            End If
            If Not encontrou Then
                FRASE = RTrim(Mid(TextoClausula, ULTIMA_QUEBRA, 90)) & vbNewLine
                CONT_FRASE = 0
                ULTIMA_QUEBRA = ULTIMA_QUEBRA + 90
                If Mid(TextoClausula, ULTIMA_QUEBRA, 1) = Chr(13) Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                    CONT_CLAUSULA = CONT_CLAUSULA + 2
                ElseIf Mid(TextoClausula, ULTIMA_QUEBRA, 1) = " " Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 1
                    CONT_CLAUSULA = CONT_CLAUSULA + 1
                    If Mid(TextoClausula, ULTIMA_QUEBRA, 1) = Chr(13) Then
                        ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                        CONT_CLAUSULA = CONT_CLAUSULA + 2
                    End If
                End If
            End If
        End If
        If FRASE <> "" Then
            texto = texto & FRASE
            FRASE = ""
        End If
    Next CONT_CLAUSULA
    If ULTIMA_QUEBRA < Len(TextoClausula) Then
        texto = texto & Mid(TextoClausula, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA + 1)
    End If
    
    Formata_Clausula = texto

End Function

'agin - 20/12/2004
Public Sub LerGrupoRamo()

Dim rc As rdoResultset
Dim sSQL As String
    
On Error GoTo TratarErro
        
    sSQL = ""
    sSQL = sSQL & "SELECT ISNULL(ramo_tb.grupo_ramo, '00') AS grupo_ramo " & vbNewLine
    sSQL = sSQL & "  FROM ramo_tb (nolock)  " & vbNewLine
    sSQL = sSQL & " WHERE ramo_tb.ramo_id = " & ramo_id
    
    Set rc = rdocn.OpenResultset(sSQL)
    
    If Not rc.EOF Then
        Reg = Reg & Left(Format(rc!grupo_ramo, "00") & Space(2), 2)
    Else
        Call MensagemBatch("Grupo Ramo n�o encontrado!!!")
        Call TerminaSEGBR
    End If
    
    rc.Close
    Set rc = Nothing
    
    Exit Sub

TratarErro:
    Call TrataErroGeral("LerGrupoRamo", Me.Name)
    Call TerminaSEGBR

End Sub

Sub Ler_Dados_Corretor()
Dim processoSusep As String, DtFim As String, Obs As String, Corretor As String
Corretor = Space(0)
On Error GoTo Erro
'Alterado por Alexandre Ricardo em 01/07/2003 - Buscar Corretor_Susep e Nome do Corretor
sql1 = "SELECT d.corretor_susep, d.nome  FROM corretagem_tb a (nolock)  "
sql1 = sql1 & " inner join  corretor_tb d (nolock)  "
sql1 = sql1 & " on a.corretor_id = d.corretor_id "
sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
If TpEmissao = "A" Then
   sql1 = sql1 & " AND (endosso_id=0 or endosso_id is null) "
Else
   sql1 = sql1 & " AND endosso_id = " & num_endosso
End If
'sql1 = sql1 & "  AND dt_fim_corretagem IS NULL "

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    If IsNull(rc!Corretor_Susep) Then
       Corretor = "   .  .  . .      . "
    Else
       'Alterado por Alexandre Ricardo em 01/07/2003 - Formata��o do C�digo Susep do Corretor
       If rc!Corretor_Susep <> "100067199" Then           'incluido em 16/07/2003
           Corretor = Space(15 - Len(rc!Corretor_Susep)) & rc!Corretor_Susep
       Else
           Corretor = " 02890710067199"
       End If
       Corretor = Mid(Corretor, 1, 3) & "." & _
                Mid(Corretor, 4, 2) & "." & _
                Mid(Corretor, 6, 2) & "." & _
                Mid(Corretor, 8, 1) & "." & _
                Mid(Corretor, 9, 6) & "." & _
                Mid(Corretor, 15, 1)
   End If
   ''Corretor = Left(Format$(rc(0), "000000-000") & Space(10), 10)
   Corretor = Corretor & UCase(Left(("" & rc!nome) & Space(50), 50))
Else
   If Val(num_endosso) <> 0 Then
      sql1 = "SELECT d.corretor_susep, d.nome  FROM corretagem_pj_endosso_fin_tb a (nolock)  "
      sql1 = sql1 & " inner join  corretor_tb d (nolock)  "
      sql1 = sql1 & " on a.corretor_id = d.corretor_id "
      sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
      sql1 = sql1 & " AND   a.endosso_id=" & num_endosso
      Set rc = Nothing
      Set rc = rdocn.OpenResultset(sql1)
      If Not rc.EOF Then
        'Alterado por Alexandre Ricardo em 01/07/2003 - Formata��o do C�digo Susep do Corretor
        'Corretor = Left(Format$(rc(0), "000000-000") & Space(10), 10)
        'Corretor = Space(14 - Len(rc(0))) & rc(0)
        If rc(0) <> "100067199" Then           'incluido em 16/07/2003
           Corretor = Space(15 - Len(rc(0))) & rc(0)
        Else
           Corretor = " 02890710067199"
        End If
        Corretor = Mid(Corretor, 1, 3) & "." & _
                    Mid(Corretor, 4, 2) & "." & _
                    Mid(Corretor, 6, 2) & "." & _
                    Mid(Corretor, 8, 1) & "." & _
                    Mid(Corretor, 9, 6) & "." & _
                    Mid(Corretor, 15, 1) & Space(1)
         'Nome
         Corretor = Corretor & UCase(Left(("" & rc!nome) & Space(50), 50))
      Else
         SQL = "SELECT a.corretor_id, d.nome  FROM corretagem_tb a (nolock)  "
         SQL = SQL & " inner join  corretor_tb d (nolock)  "
         SQL = SQL & " on a.corretor_id = d.corretor_id "
         SQL = SQL & " WHERE a.proposta_id = " & num_proposta
         SQL = SQL & " AND a.dt_fim_corretagem is null "
         Set rc = Nothing
         Set rc = rdocn.OpenResultset(SQL)
         If Not rc.EOF Then
            'Alterado por Alexandre Ricardo em 01/07/2003 - Formata��o do C�digo Susep do Corretor
            'Corretor = Left(Format$(rc(0), "000000-000") & Space(10), 10)
            'Corretor = Space(14 - Len(rc(0))) & rc(0)
            If rc(0) <> "100067199" Then           'incluido em 16/07/2003
                Corretor = Space(15 - Len(rc(0))) & rc(0)
            Else
                Corretor = " 02890710067199"
            End If
            Corretor = Mid(Corretor, 1, 3) & "." & _
                        Mid(Corretor, 4, 2) & "." & _
                        Mid(Corretor, 6, 2) & "." & _
                        Mid(Corretor, 8, 1) & "." & _
                        Mid(Corretor, 9, 6) & "." & _
                        Mid(Corretor, 15, 1) & Space(1)
            'Nome
            Corretor = Corretor & UCase(Left(("" & rc!nome) & Space(50), 50))
         End If
     End If
   End If
End If
Set rc = Nothing
If FimVig = "" Then
   DtFim = "01/01/1001"
Else
   DtFim = FimVig
End If
If TpEmissao = "A" And DateDiff("d", CDate(IniVig), CDate(DtFim)) > 31 And Not (PagoAto And ParcelaUnica) Then
    'Observa��o 6 Linhas
    'Obs = Space(20) & "ANEXOS : MANUAL DO SEGURADO E CL�USULAS ESPEC�FICAS" & String(53, " ")
    Obs = Obs & "Em cumprimento a Circular SUSEP nro. 97 em 09 de julho de 1999, informamos que:" & String(45, " ")
    Obs = Obs & "* O n�o pagamento da primeira parcela implicar� no cancelamento da ap�lice" & String(50, " ")
    Obs = Obs & "* O n�o pagamento das demais parcelas implicar� no cancelamento da ap�lice, observado o artigo primeiro " & Space(20)
    Obs = Obs & "  da referida circular, o qual faculta ao segurado o direito de restabelecer a cobertura da ap�lice." & String(24, " ")
    Obs = Obs & String(124, " ")
 Else
    Obs = String(124, " ")
    Obs = Obs & String(124, " ")
    Obs = Obs & String(124, " ")
    Obs = Obs & String(124, " ")
    Obs = Obs & String(124, " ")
 End If
Reg = Reg & Left(Corretor & Space(70), 70) & String(9, "*") & Obs
Reg = Reg & Left("S�o Paulo, " & DataExtenso(DtEmissao) & Space(40), 40) & Space(20)
If Trim(processo_susep) <> "" Then
   Reg = Reg & "Processo SUSEP : " & Left(processo_susep & Space(47), 47)
Else
   Reg = Reg & Space(64)
End If
If Trim(atividadePrincipal) <> "" Then
   Reg = Reg & Left(atividadePrincipal & Space(20), 20)
Else
   Reg = Reg & String(20, "*")
End If
Exit Sub

Erro:
    TrataErroGeral "Ler_Dados_Corretor", Me.Name
    Call Fecha_Arquivo
    Unload Me
    Call TerminaSEGBR
    End
End Sub


Sub Ler_Dados_Corretor2()
Dim processoSusep As String, DtFim As String, Obs As String
    
On Error GoTo Erro
'Buscar nome e Cod_susep
sql1 = "SELECT a.corretor_id, d.nome  FROM corretagem_tb a (nolock)  "
sql1 = sql1 & " inner join  corretor_tb d (nolock)  "
sql1 = sql1 & " on a.corretor_id = d.corretor_id "
sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
If TpEmissao = "A" Then
   sql1 = sql1 & " AND (endosso_id=0 or endosso_id is null) "
Else
   sql1 = sql1 & " AND endosso_id = " & num_endosso
End If
'sql1 = sql1 & "  AND dt_fim_corretagem IS NULL "
Set rc = rdocn.OpenResultset(sql1)
If Not rc.EOF Then
   'Cod Susep
   Reg = Reg & Left(Format$(rc(0), "000000-000") & Space(10), 10)
   'Nome
   If Not IsNull(rc!nome) Then
      Reg = Reg & UCase(Left(rc!nome & Space(50), 50))
   Else
      Reg = Reg & Space(50)
   End If
   'Inspetoria
   Reg = Reg & String(9, "*")
   If FimVig = "" Then
      DtFim = "01/01/1001"
   Else
      DtFim = FimVig
   End If
   If TpEmissao = "A" And DateDiff("d", CDate(IniVig), CDate(DtFim)) > 31 Then
      'Observa��o 6 Linhas
      'Obs = Space(20) & "ANEXOS : MANUAL DO SEGURADO E CL�USULAS ESPEC�FICAS" & String(53, " ")
      Obs = Obs & "Em cumprimento a Circular SUSEP nro. 97 em 09 de julho de 1999, informamos que:" & String(45, " ")
      Obs = Obs & "* O n�o pagamento da primeira parcela implicar� no cancelamento da ap�lice" & String(50, " ")
      Obs = Obs & "* O n�o pagamento das demais parcelas implicar� no cancelamento da ap�lice, observado o artigo primeiro " & Space(20)
      Obs = Obs & "  da referida circular, o qual faculta ao segurado o direito de restabelecer a cobertura da ap�lice." & String(24, " ")
      Obs = Obs & String(124, " ")
   Else
      Obs = String(124, " ")
      Obs = Obs & String(124, " ")
      Obs = Obs & String(124, " ")
      Obs = Obs & String(124, " ")
      Obs = Obs & String(124, " ")
   End If
Else
   If Val(num_endosso) = 0 Then
      Reg = Reg & Space(10)
      Reg = Reg & Space(50)
      Reg = Reg & String(9, "*")
      If FimVig = "" Then
         DtFim = "01/01/1001"
      Else
         DtFim = FimVig
      End If
   Else
      sql1 = "SELECT a.corretor_id, d.nome  FROM corretagem_pj_endosso_fin_tb a (nolock)  "
      sql1 = sql1 & " inner join  corretor_tb d (nolock)  "
      sql1 = sql1 & " on a.corretor_id = d.corretor_id "
      sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
      sql1 = sql1 & " AND   a.endosso_id=" & num_endosso
      Set rc = Nothing
      Set rc = rdocn.OpenResultset(sql1)
      If Not rc.EOF Then
         'Cod Susep
         Reg = Reg & Left(Format$(rc(0), "000000-000") & Space(10), 10)
         'Nome
         Reg = Reg & UCase(Left(("" & rc!nome) & Space(50), 50))
         'Inspetoria
         Reg = Reg & String(9, "*")
         If FimVig = "" Then
            DtFim = "01/01/1001"
         Else
            DtFim = FimVig
         End If
      Else
         Reg = Reg & Space(10)
         Reg = Reg & Space(50)
         Reg = Reg & String(9, "*")
         If FimVig = "" Then
            DtFim = "01/01/1001"
         Else
            DtFim = FimVig
         End If
      End If
   End If
   If TpEmissao = "A" And DateDiff("d", CDate(IniVig), CDate(DtFim)) > 31 Then
       'Observa��o 6 Linhas
       'Obs = Space(20) & "ANEXOS : MANUAL DO SEGURADO E CL�USULAS ESPEC�FICAS" & String(53, " ")
       Obs = Obs & "Em cumprimento a Circular SUSEP nro. 97 em 09 de julho de 1999, informamos que:" & String(45, " ")
       Obs = Obs & "* O n�o pagamento da primeira parcela implicar� no cancelamento da ap�lice" & String(50, " ")
       Obs = Obs & "* O n�o pagamento das demais parcelas implicar� no cancelamento da ap�lice, observado o artigo primeiro " & Space(20)
       Obs = Obs & "  da referida circular, o qual faculta ao segurado o direito de restabelecer a cobertura da ap�lice." & String(24, " ")
       Obs = Obs & String(124, " ")
    Else
       Obs = String(124, " ")
       Obs = Obs & String(124, " ")
       Obs = Obs & String(124, " ")
       Obs = Obs & String(124, " ")
       Obs = Obs & String(124, " ")
    End If
End If
rc.Close
Reg = Reg & Obs
Reg = Reg & Left("S�o Paulo, " & DataExtenso(DtEmissao) & Space(40), 40) & Space(20)
If Trim(processo_susep) <> "" Then
   Reg = Reg & "Processo SUSEP : " & Left(processo_susep & Space(47), 47)
Else
   Reg = Reg & Space(64)
End If
If Trim(atividadePrincipal) <> "" Then
   Reg = Reg & Left(atividadePrincipal & Space(20), 20)
Else
   Reg = Reg & String(20, "*")
End If
Exit Sub

Erro:
    TrataErroGeral "Ler_Dados_Corretor2", Me.Name
    Call Fecha_Arquivo
    Unload Me
    Call TerminaSEGBR
    End
    
End Sub

Sub Conexao_auxiliar()
   
 On Error GoTo Erro
    
 With rdocn1
     .Connect = rdocn.Connect
     '.CursorDriver = rdUseServer
     .CursorDriver = rdUseNone
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 
 With rdocn2
     .Connect = rdocn.Connect
     '.CursorDriver = rdUseServer
     .CursorDriver = rdUseNone
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 
 With rdocn3
     .Connect = rdocn.Connect
     .CursorDriver = rdUseNone
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 Exit Sub

Erro:
    MensagemBatch "Conex�o com BRCAPDB indispon�vel.", vbCritical
    Unload Me
    Call TerminaSEGBR
    End
End Sub

Public Sub Processa_Dados_Gerais()

Ler_Cliente
Ler_Endereco_Risco
'Obt�m coberturas e limite de responsabilidade
Ler_CoberturasTotIS
Ler_Beneficiarios
Ler_MoedaAtual
Ler_Proposta_Fechada
'Obt�m descri��o do ramo e da modalidade
Ler_RamoModalidade
Ler_Dados_Corretor

'agin - 20/12/2004
LerGrupoRamo

'Completa com espa�os
Reg = Left(Reg & Space(tam_reg), tam_reg)
Print #Arq1, Reg

ContaLinhaAtual = ContaLinhaAtual + 1
    
End Sub

Public Sub Ler_CodBarras_Retorno()

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Jorfilho 15/08/2002 - Novo c�digo de barras com 20 posi��es

If TpEmissao = "A" Then
   Reg = Reg & "04" 'Tipo_documento
ElseIf TpEmissao = "E" Then
   Reg = Reg & "06" 'Tipo_documento
End If

Reg = Reg & num_proposta 'Proposta_id

Reg = Reg & ConverteParaJulianDate(CDate(Data_Sistema))

Reg = Reg & ContAgencia

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

If Not Flag_2via Then 'jorfilho - 07/08/2001 - Atributo: mensagem
    Reg = Reg & Space(10)
Else
    Reg = Reg & "2� via    "
End If

End Sub

Public Sub Ler_RamoModalidade()
Dim RamoModalidade As String

On Error GoTo Erro

SQL = "SELECT Nome "
SQL = SQL & "FROM ramo_tb (nolock)  "
SQL = SQL & "WHERE ramo_id= " & ramo_id
Set rc = rdocn.OpenResultset(SQL)
If Not rc.EOF Then
    'madorno - 11/01/2005
    'RamoModalidade = Format(ramo_id, "000") & Trim("" & rc!nome)
   RamoModalidade = Left(Format(ramo_id, "00") & Space(3), 3)
   RamoModalidade = RamoModalidade & " " & Trim(rc!nome)
   'Reg = Reg & Ramo
Else
   RamoModalidade = ""
   'Reg = Reg & Ramo
End If
rc.Close

SQL = "SELECT distinct m.modalidade_seguro_id, m.nome FROM "
SQL = SQL & "modalidade_seguro_tb m (nolock)  INNER JOIN  subramo_tb s (nolock)  "
SQL = SQL & "ON (m.modalidade_seguro_id=s.modalidade_seguro_id "
SQL = SQL & "AND m.ramo_irb_id=s.ramo_irb_id) "
SQL = SQL & "WHERE s.dt_fim_vigencia_sbr Is Null "
SQL = SQL & "AND s.ramo_id=" & ramo_id
SQL = SQL & "AND s.subramo_id= " & Subramo
' Modifica��o 30/05
SQL = SQL & " AND m.dt_inicio_vigencia_mod <= '" & Format(DtInicioVigencia, "yyyymmdd") & "'"
SQL = SQL & " AND (m.dt_fim_vigencia_mod is null"
SQL = SQL & " OR m.dt_fim_vigencia_mod >= '" & Format(DtInicioVigencia, "yyyymmdd") & "')"
Set rc = rdocn.OpenResultset(SQL)

If Not rc.EOF Then
   If RamoModalidade <> "" And Trim("" & rc!nome) <> "" Then
      RamoModalidade = RamoModalidade & " - "
   End If
   RamoModalidade = RamoModalidade & Trim("" & rc!nome)
   'Reg = Reg & Ramo
End If
rc.Close
Reg = Reg & UCase(Left(Trim(RamoModalidade) & Space(63), 63))

Exit Sub

Erro:
   TrataErroGeral "Ler_RamoModalidade", Me.Name
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Public Sub Ler_CoberturasTotIS()
Dim CodObjAnterior As Long, i As Long, PercFranquia As Double, TotIS As Double, vStrTotIs As String
ReDim Cobertura(7, 17)

On Error GoTo Erro
QtdCoberturas = 0
TotIS = 0
If ramo_id = "22" And ProdutoId <> 400 Then
   TranspInternacional = True
   Ler_TransporteInternacional
Else
   TranspInternacional = False
   If TabEscolha = "" Then
      SQL = Monta_SqlCoberturas
   Else
      SQL = "SELECT e.tp_cobertura_id, c.nome, c.descricao, e.val_is, "       'descri��o da cobertura 27/10/2003
      SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
      SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia, e.acumula_is,  "
      SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
      SQL = SQL & "FROM " & TabEscolha & " e (nolock) , tp_cobertura_tb c (nolock)  "
      SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
      SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
      SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
      If TpEmissao = "A" Then
         SQL = SQL & " (e.num_endosso=0 OR e.num_endosso is null ) "
      Else
         SQL = SQL & " e.num_endosso=" & num_endosso
         SQL = SQL & " AND dt_fim_vigencia_esc is null "
      End If
      SQL = SQL & " ORDER BY e.val_is desc "
   End If
   CodObjAnterior = 0
   Set rc = rdocn.OpenResultset(SQL)
   Do While Not rc.EOF
      'Obtendo Limite de Responsabilidade
      If UCase("" & rc!acumula_is) = "S" Then
         TotIS = TotIS + Val(0 & rc!val_is)
      End If
      If Val(0 & rc!cod_objeto_segurado) <> CodObjAnterior Then
         'Contando com t�tulo, local do risco, t�tulo coberturas e espa�o itens
         QtdLinhasCobertura = QtdLinhasCobertura + 4
         CodObjAnterior = Val(0 & rc!cod_objeto_segurado)
      End If
      Cobertura(0, QtdCoberturas) = Val(0 & rc!cod_objeto_segurado)
      Cobertura(1, QtdCoberturas) = Val(0 & rc!Tp_Cobertura_Id)
      If IsNull(rc!nome) Or (rc!nome = "") Then           'descri��o da cobertura 27/10/2003
        Cobertura(2, QtdCoberturas) = "" & rc!Descricao
      Else
        Cobertura(2, QtdCoberturas) = "" & rc!nome
      End If
      Cobertura(3, QtdCoberturas) = Val(0 & rc!val_is)
      If Val(0 & rc!fat_franquia) <> 0 Then
         'PercFranquia = (1 - Val(0 & rc!fat_franquia)) * 100
         PercFranquia = Val(0 & rc!fat_franquia) * 100
      Else
         PercFranquia = 0
      End If
      Cobertura(4, QtdCoberturas) = PercFranquia                  'Perc franquia
      Cobertura(5, QtdCoberturas) = Trim("" & rc!texto_franquia)  'Texto franquia
      Cobertura(6, QtdCoberturas) = Val(0 & rc!val_min_franquia)  'M�n. franquia
      If PercFranquia <> 0 Or Cobertura(5, QtdCoberturas) <> "" Or Cobertura(6, QtdCoberturas) <> "0" Then
         QtdLinhasCobertura = QtdLinhasCobertura + 1
      End If
      
      If QtdCoberturas Mod 17 = 0 Then
         ReDim Preserve Cobertura(7, QtdCoberturas + 17)
      End If
      QtdCoberturas = QtdCoberturas + 1
      QtdLinhasCobertura = QtdLinhasCobertura + 1
      i = i + 1
      rc.MoveNext
   Loop
   rc.Close
End If

' Limite de responsabilidade
'If Not ConfiguracaoBrasil Then
'   vStrTotIs = TrocaValorAmePorBras(Format(TotIS, "###,###,##0.00"))
'Else
'   vStrTotIs = Format(TotIS, "###,###,##0.00")
'End If
'If TpEmissao = "A" And Not TranspInternacional Then
'   Reg = Reg & Left("LIMITE MAXIMO DE REPONSABILIDADE DA APOLICE :  R$ " & vStrTotIs & Space(100), 100)
'Else
   Reg = Reg & Space(100)
'End If

Exit Sub

Erro:
   'TrataErroGeral "Erro na estrutura dos dados para proposta " & num_proposta
   TrataErroGeral "Ler_CoberturasTotIS", Me.Name
   MensagemBatch "Erro ao tentar ler coberturas da proposta " & num_proposta & ". Programa ser� cancelado", vbCritical
   Call Fecha_Arquivo
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Sub Libera_Impressao()

DoEvents

SQL = "SELECT "
SQL = SQL & "     a.proposta_id, "
SQL = SQL & "     a.dt_inclusao "
SQL = SQL & " FROM "
SQL = SQL & "     apolice_tb a (nolock)  "
SQL = SQL & "     INNER join proposta_tb p (nolock)  "
SQL = SQL & "           on p.proposta_id = a.proposta_id "
SQL = SQL & "     INNER join proposta_fechada_tb pf (nolock)  "
SQL = SQL & "           on pf.proposta_id = p.proposta_id "
SQL = SQL & "     INNER join produto_tb pd (nolock)  "
SQL = SQL & "           on pd.produto_id = p.produto_id "

SQL = SQL & " WHERE "
SQL = SQL & "     a.dt_impressao is null             "
SQL = SQL & "     and pf.impressao_liberada    = 'N' "
           ' joconceicao nov 2001, 30 - para atual somente os
           ' produtos que tenha s no atributo apolice_envia_cliente
SQL = SQL & "     and pd.Apolice_envia_cliente = 'S' "

'SQL = SQL & "     and (pd.emissao_on_line = 's' or exists"
'SQL = SQL & "                                     (select * from avaliacao_retorno_bb_tb av (nolock) "
'SQL = SQL & "                                      where av.proposta_id = p.proposta_id "
'SQL = SQL & "                                      and av.endosso_id is null "
'SQL = SQL & "                                      and av.aceite_bb = 'S'))"

Set rc = rdocn1.OpenResultset(SQL)

If Not rc.EOF Then
    rdocn.BeginTrans
    
    While Not rc.EOF
       
       DoEvents
       
       If DateDiff("d", DateValue(rc(1)), DateValue(Now)) > 7 Then
          
          num_proposta = rc(0)
          
          If Not Atualiza_ImpressaoApolice("S") Then
             MensagemBatch "Erro na Altera��o da Ap�lice"
             GoTo Erro
          End If
          
          TotRegProcessados = TotRegProcessados + 1
          If TotRegProcessados Mod 10 = 0 Then
             'Efetuando atualiza��es e iniciando nova transa��o
             rdocn.CommitTrans
             rdocn.BeginTrans
          End If
       End If
       
       rc.MoveNext
    
    Wend

    rdocn.CommitTrans
    '
    rc.Close
End If

Exit Sub

Erro:
   TrataErroGeral "Libera_Impressao", Me.Name
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Private Sub Atualiza_Evento_Impressao(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal Usuario As String)
                                      
Dim SQL         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    SQL = ""
    SQL = SQL & "exec evento_seguros_db..evento_impressao_geracao_spu "
    SQL = SQL & num_solicitacao & ", '" & arquivo_remessa & "', '" & Usuario & "' "
    
    Set rsAtualizacao = rdocn.OpenResultset(SQL)
    rsAtualizacao.Close
    
    Exit Sub

Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.Name
    TerminaSEGBR
End Sub

Private Sub Atualiza_Evento_Impressao2(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal Usuario As String)
                                      
Dim SQL         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    SQL = ""
    SQL = SQL & "exec evento_seguros_db..evento_impressao_geracao_spu "
    SQL = SQL & num_solicitacao & ", '" & arquivo_remessa & "', '" & Usuario & "' "
    
    colAtualiza.Add SQL          'Armazena a procedure de atualiza��o na cole��o
    
    Exit Sub

Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.Name
    TerminaSEGBR
End Sub

Function Atualiza_ImpressaoApolice(fFlag As String) As Boolean
Dim SQL As String

Atualiza_ImpressaoApolice = True
On Error GoTo Erro

SQL = "EXEC atualiza_impressao_apolice_spu "
SQL = SQL & num_proposta
SQL = SQL & ", '" & cUserName & "'"
SQL = SQL & ", '" & fFlag & "'"

rdocn.Execute (SQL)
Exit Function

Erro:
   TrataErroGeral "Atualiza_ImpressaoApolice", Me.Name
   Atualiza_ImpressaoApolice = False

End Function

Private Function Atualiza_Impressao_Liberada(ByVal proposta_id) As Boolean

DoEvents

SQL = ""
SQL = SQL & "SELECT "
SQL = SQL & "     a.proposta_id, "
SQL = SQL & "     a.dt_inclusao "
SQL = SQL & " FROM "
SQL = SQL & "     apolice_tb a (nolock)  "
SQL = SQL & "     INNER join proposta_tb p  (nolock) "
SQL = SQL & "           on p.proposta_id = a.proposta_id "
SQL = SQL & "     INNER join proposta_fechada_tb pf (nolock)  "
SQL = SQL & "           on pf.proposta_id = p.proposta_id "
SQL = SQL & "     INNER join produto_tb pd (nolock)  "
SQL = SQL & "           on pd.produto_id = p.produto_id "

SQL = SQL & " WHERE "
SQL = SQL & "     a.dt_impressao is null             "
SQL = SQL & "     and pf.impressao_liberada    = 'N' "
           ' joconceicao nov 2001, 30 - para atual somente os
           ' produtos que tenha s no atributo apolice_envia_cliente
SQL = SQL & "     and pd.Apolice_envia_cliente = 'S' "
SQL = SQL & "     and p.proposta_id = " & proposta_id
'SQL = SQL & "     and (pd.emissao_on_line = 's' or exists"
'SQL = SQL & "                                     (select * from avaliacao_retorno_bb_tb av (nolock) "
'SQL = SQL & "                                      where av.proposta_id = p.proposta_id "
'SQL = SQL & "                                      and av.endosso_id is null "
'SQL = SQL & "                                      and av.aceite_bb = 'S'))"

Set rc = rdocn2.OpenResultset(SQL)

Atualiza_Impressao_Liberada = False

If Not rc.EOF Then
       
    DoEvents
    
    If DateDiff("d", DateValue(rc(1)), DateValue(Now)) > 7 Then
       
       num_proposta = rc(0)
       
       If Not Atualiza_ImpressaoApolice("S") Then
          MensagemBatch "Erro na Altera��o da Ap�lice"
          GoTo Erro
       Else
         Atualiza_Impressao_Liberada = True
       End If
       
    End If

    rc.Close
    
End If

Exit Function

Erro:
   TrataErroGeral "Libera_Impressao", Me.Name
   Unload Me
   Call TerminaSEGBR
   End
End Function

Function ObterNumRemessa(nome As String, ByRef NumRemessa As String) As Variant

Dim SQL As String
Dim rcNum As rdoResultset
Dim vObterNumRemessa() As Integer
ReDim vObterNumRemessa(0 To 1)
On Error GoTo Erro
    
    SQL = " SELECT"
    SQL = SQL & "     l.layout_id"
    SQL = SQL & " FROM"
    SQL = SQL & "     controle_proposta_db..layout_tb l (nolock) "
    SQL = SQL & " WHERE"
    SQL = SQL & "     l.nome = '" & nome & "'"
    Set rcNum = rdocn2.OpenResultset(SQL)
    
        If rcNum.EOF Then
           Error 1000
        Else
        vObterNumRemessa(0) = rcNum(0)
        SQL = "       SELECT"
        SQL = SQL & "     isnull(max(a.versao), 0)"
        SQL = SQL & " FROM"
        SQL = SQL & "     controle_proposta_db..arquivo_versao_gerado_tb a (nolock) "
        SQL = SQL & " WHERE"
        SQL = SQL & "     a.layout_id = " & rcNum(0)
        
        rcNum.Close
        
        Set rcNum = rdocn2.OpenResultset(SQL)
        
            If Not rcNum.EOF Then
               NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "0000"), Format(rcNum(0) + 1, "0000"))
               ObterNumRemessa = NumRemessa
            Else
               ObterNumRemessa = Nothing
            End If
        End If
    
    rcNum.Close

Exit Function

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "ObterNumRemessa", nome
    TerminaSEGBR

End Function

Function Obtem_Num_Remessa(nome As String, ByRef NumRemessa As String) As Variant
'joconceicao - 11jul01
'incluida a variant tNum_Remessa que deve retornar o numero da versao e lay out do arquivo
'
Dim SQL As String
Dim rcNum As rdoResultset
Dim tNum_Remessa() As Integer
ReDim tNum_Remessa(0 To 1)
On Error GoTo Erro

    SQL = SQL & " SELECT"
    SQL = SQL & "     l.layout_id"
    SQL = SQL & " FROM"
    SQL = SQL & "     controle_proposta_db..layout_tb l (nolock) "
    SQL = SQL & " WHERE"
    SQL = SQL & "     l.nome = '" & nome & "'"
    Set rcNum = rdocn.OpenResultset(SQL)
    
    If rcNum.EOF Then
           Error 1000
    Else
        tNum_Remessa(0) = rcNum!Layout_id
        SQL = "       SELECT"
        SQL = SQL & "     max(a.versao)"
        SQL = SQL & " FROM"
        SQL = SQL & "     controle_proposta_db..arquivo_versao_gerado_tb a (nolock) "
        SQL = SQL & " WHERE"
        SQL = SQL & "     a.layout_id = " & rcNum(0)
        
        rcNum.Close
        
        Set rcNum = rdocn.OpenResultset(SQL)
        
            If Not rcNum.EOF Then
              If Not IsNull(rcNum!column1) Then
                tNum_Remessa(1) = rcNum!column1 + 1
                NumRemessa = Format(rcNum(0) + 1, "000000")
              Else
                tNum_Remessa(1) = 1
                NumRemessa = Format(1, "000000")
              End If
              Obtem_Num_Remessa = tNum_Remessa
            Else
               Set Obtem_Num_Remessa = Nothing
            End If
    
    End If
    
    rcNum.Close

Exit Function

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "Obtem_Num_Remessa", Me.Name
    Unload Me
    Call TerminaSEGBR
    End
End Function


Public Function InserirArquivoVersaoGerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    On Error GoTo Erro
            
    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    SQL = SQL & nome & "'," & NumRemessa & "," & qReg & ",'"
    SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    SQL = SQL & cUserName & "'"
    rdocn.Execute (SQL)
    
    Exit Function

Erro:
    TrataErroGeral "InserirArquivoVersaoGerado", nome
    TerminaSEGBR
    
End Function

Sub Insere_Arquivo_Versao_Gerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    Dim rcGer As rdoResultset
    Dim SQL As String
    
    On Error GoTo Erro
            
    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    SQL = SQL & nome & "'," & NumRemessa & "," & qReg & ",'"
    SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    SQL = SQL & cUserName & "'"
    Set rcGer = rdocn.OpenResultset(SQL)
        
    rcGer.Close
        
    Exit Sub

Erro:
    TrataErroGeral "Insere_Arquivo_Versao_Gerado", Me.Name
    Unload Me
    Call TerminaSEGBR
    End
End Sub

Function CorrigeTextoClausula(car As String) As String
   
   Dim pos As Long
  
   For pos = 1 To Len(car)
      If Asc(Mid(car, pos, 1)) = 10 Then
         If pos < Len(car) Then
            If Asc(Mid(car, pos + 1, 1)) = 10 Then
               car = Mid(car, 1, pos - 1) & Chr(13) & Mid(car, pos + 1)
            End If
         End If
      End If
   Next
   
   CorrigeTextoClausula = car
   
End Function

Public Function ConverteParaJulianDate(ldate As Date) As String
Dim lJulianDate  As String * 5
   On Error GoTo Erro

lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
ConverteParaJulianDate = lJulianDate

  Exit Function

Erro:
   TrataErroGeral "ConverteParaJulianDate", Me.Name
   TerminaSEGBR
End Function
