VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSEGA9201Vida 
   Caption         =   "Resumo da Emiss�o de Ap�lices/Endossos VIDA"
   ClientHeight    =   5760
   ClientLeft      =   345
   ClientTop       =   870
   ClientWidth     =   7125
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   5760
   ScaleWidth      =   7125
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame Frame2 
      Enabled         =   0   'False
      Height          =   3255
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   6885
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   3
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   19
         Top             =   2520
         Width           =   855
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   2
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   18
         Top             =   1890
         Width           =   855
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   17
         Top             =   2520
         Width           =   5265
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   1890
         Width           =   5265
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   13
         Top             =   1230
         Width           =   5265
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   1
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   1230
         Width           =   855
      End
      Begin VB.TextBox txtNrReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Index           =   0
         Left            =   5820
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox txtArq 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   0
         Left            =   240
         Locked          =   -1  'True
         TabIndex        =   3
         Top             =   600
         Width           =   5265
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   3
         Left            =   5820
         TabIndex        =   23
         Top             =   2250
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   2
         Left            =   5820
         TabIndex        =   22
         Top             =   1620
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Endosso Alian�a:"
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   21
         Top             =   2250
         Width           =   3525
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Ap�lice Alian�a:"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   20
         Top             =   990
         Width           =   3525
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Endosso Cliente:"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   15
         Top             =   1620
         Width           =   3525
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   1
         Left            =   5820
         TabIndex        =   14
         Top             =   960
         Width           =   795
      End
      Begin VB.Label Label4 
         Caption         =   "Registros :"
         Height          =   255
         Index           =   0
         Left            =   5820
         TabIndex        =   11
         Top             =   330
         Width           =   795
      End
      Begin VB.Label Label3 
         Caption         =   "Arquivo Ap�lice Cliente:"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   3525
      End
   End
   Begin VB.CommandButton cmdCanc 
      Caption         =   "&Cancelar"
      Height          =   375
      Left            =   5760
      TabIndex        =   6
      Top             =   4905
      Width           =   1095
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   4470
      TabIndex        =   5
      Top             =   4905
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Enabled         =   0   'False
      Height          =   1215
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   6885
      Begin VB.TextBox txtfim 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   2
         Top             =   720
         Width           =   2175
      End
      Begin VB.TextBox txtIni 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         Locked          =   -1  'True
         TabIndex        =   1
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label2 
         Caption         =   "Fim........................."
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "In�cio....................."
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   1575
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   24
      Top             =   5505
      Width           =   7125
      _ExtentX        =   12568
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Object.Width           =   15240
            MinWidth        =   15240
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmSEGA9201Vida"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const TP_RAMO_VIDA = 1
Const TP_RAMO_RE = 2

'Conex�es auxiliares
'Dim rdocn1              As New rdoConnection          'comentado em 23/09/2003
'Dim rdocn2              As New rdoConnection
Dim arquivo_remessa     As String

'Vari�veis
Dim wFirst              As Boolean
Dim wNew                As Boolean
Dim Tinha               As Boolean
Dim Carta_path          As String
' Dim Arquivo             As Integer
Dim arquivo1            As Integer
Dim arquivo2            As Integer
Dim arq                 As Integer
Dim tam_reg             As Integer
Dim Rel_Apolice         As String
Dim Rel_Header          As String
Dim qtd_corretores      As Integer
Dim corretor_id()       As String
Dim nome_corretor()     As String
Dim Varios_corretores   As Boolean
Dim num_proposta        As String
Dim num_apolice         As String
Dim ContAgencia         As String
Dim processo_susep      As String
Dim Reg                 As String
Dim dtIniVigencia       As String
Dim ContaLinha1         As Long
Dim ContaLinha2         As Long
Dim ProdutoId           As Integer
Dim NomeProduto         As String
Dim num_endosso         As String
Dim Subramo             As String
Dim PropAnt             As Long
Dim Texto_Clausula      As String
'Dados Cliente
Dim nome                As String
Dim Endereco            As String
Dim Bairro              As String
Dim Municipio           As String
Dim Cep                 As String
Dim UF                  As String

'cristovao.rodrigues 28/10/2016 - 19368999/19370584 - cobran�a registrada AB e ABS
Dim Cpf_Cnpj_Sacado     As String
Dim Cpf_Cnpj_Cedente    As String
Dim Endereco_Cedente    As String
Dim Bairro_Cedente      As String
Dim Municipio_Cedente   As String
Dim UF_Cedente          As String
Dim CEP_Cedente         As String

''
Dim SQL                 As String
Dim sql1                As String
Dim rc                  As rdoResultset
Dim Rc1                 As rdoResultset
Dim rc2                 As rdoResultset

Dim NumRegs             As Long
Dim NumRemessaApolice1  As String
Dim NumRemessaApolice2  As String
Dim QtdReg1             As Long
Dim QtdReg2             As Long
Dim rc_apl              As rdoResultset
Dim ValTotDesconto      As Double
Dim INI                 As String
Dim Fim                 As String
Dim mes                 As String
Dim IniVig              As String
Dim FimVig              As String
Dim ConfiguracaoBrasil  As Boolean
Dim produto_externo_id  As Integer
Dim LinhasCoberturas    As Long
Dim MoedaSeguro         As String
Dim MoedaPremio         As String

'** Cobran�a
Dim Sacado_1            As String
Dim Sacado_2            As String
Dim Sacado_3            As String

Dim ArquivoCBR          As Integer
Dim Nosso_Numero        As String
Dim Nosso_numero_dv     As String
Dim Carteira            As String
Dim Val_Cobranca        As Double
Dim agencia             As String
Dim Codigo_Cedente      As String
Dim linha_digitavel     As String
Dim codigo_barras       As String
Dim Rel_cobranca        As String

Dim sDecimal            As String
Dim TpEmissao           As String
Dim DtInicioVigencia    As String
Dim DtEmissao           As Date
Dim TabEscolha          As String

Dim Seguradora          As String
Dim Sucursal            As String
Dim ramo_id             As String
Dim EnviaCliente        As Boolean
Dim EnviaCongenere      As Boolean
Dim QtdVias             As Byte
Dim CoberturasPrimPagina   As Boolean

Dim QtdCoberturas       As Long
Dim QtdLinhasCobertura  As Long
Dim Cobertura()         As String
Dim EnderecoRisco()     As String
Dim QtdObjetos          As Long
Dim Benef()             As String
Dim QtdBenefs           As Long
Dim Congenere()         As String
Dim QtdCongeneres       As Long
Dim CoberturaTransporte() As String

Dim Pagamentos As New Collection
Dim CoberturasTransp As New Collection
Dim TranspInternacional As Boolean
Dim Verba As New Collection

Dim agencia_id                  As String
Dim conta_corrente_id           As String

Dim dt_agendamento                    As String
Dim FLAG_EXISTE_APOLINDOSSO           As Boolean
Dim FLAG_EXISTE_APOLINDOSSO_CLIENTE   As Boolean

Dim wLinha            As String
Dim QualRemessa       As String
Dim aArquivo          As String
Dim Destino_id        As String
Dim Diretoria_id      As String
Dim nFile             As String
Dim flagEnderecoAgencia   As Boolean
Dim ContaLinhaAtual       As Long
Dim Arq1                  As String
Dim Nome_Arq1             As String
Dim convenio_global As String


Function Le_Parametro_Geral(param As String) As String
Dim SQL As String
Dim rc As rdoResultset

On Error GoTo Erro_Le_Param

SQL = "SELECT val_parametro FROM ps_parametro_tb  WITH (NOLOCK)   "
SQL = SQL & "WHERE parametro = '" & param & "'"
'
Set rc = rdocn2.OpenResultset(SQL)
'
If Not rc.EOF Then
    If IsNull(rc!val_parametro) Then
        MensagemBatch "C�digo do par�metro " & param & " est� nulo na tabela de par�metros." _
        & Chr(13) & Chr(10) & "O Programa ser� cancelado.", vbCritical
        TerminaSEGBR
    Else
        Le_Parametro_Geral = Trim(rc!val_parametro)
    End If
Else
    MensagemBatch "C�digo do par�metro " & param & " n�o est� definido na tabela de par�metros." _
    & Chr(13) & Chr(10) & "O Programa ser� cancelado.", vbCritical
    TerminaSEGBR
End If
'
Exit Function

Erro_Le_Param:
    'TrataErroGeral "Erro obtendo par�metro " & param & " . Programa ser� cancelado"
    TrataErroGeral "Le_Parametro_Geral", Me.name
    TerminaSEGBR
    
End Function

' ARQUIVO era vari�vel; devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o
' (Jo�o Mac-Cormick - 30/6/2000)
Private Function Arquivo() As Integer
  Arquivo = Se(EnviaCliente, arquivo1, arquivo2)
End Function

' Devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o
Private Function RegAtual() As Integer
  RegAtual = Se(EnviaCliente, QtdReg1, QtdReg2)
End Function

Private Sub Atualiza_Evento_Impressao(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal Usuario As String)
                                      
Dim SQL         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    SQL = ""
    SQL = SQL & "exec evento_seguros_db..evento_impressao_geracao_spu "
    SQL = SQL & num_solicitacao & ", '" & arquivo_remessa & "', '" & Usuario & "' "
    
    Set rsAtualizacao = rdocn.OpenResultset(SQL)
    rsAtualizacao.Close
    
    Exit Sub

Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.name
    TerminaSEGBR
End Sub

Private Sub Atualiza_Evento_Impressao_Temp(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal Usuario As String)
                                      
Dim SQL         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    SQL = ""
    SQL = SQL & "exec seguros_temp_db..evento_impressao_geracao_temp_spu "
    SQL = SQL & num_solicitacao & ", '" & arquivo_remessa & "', '" & Usuario & "' "
    
    Set rsAtualizacao = rdocn.OpenResultset(SQL)
    rsAtualizacao.Close
    
    Exit Sub

Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.name
    TerminaSEGBR
End Sub


Sub Atualiza_pagamento(ByVal num_proposta, num_cobranca, num_via)
'** Cobran�a
    
Dim rc_Atualiza As rdoResultset
Dim SQL As String
   
On Error GoTo Erro
SQL = Ambiente & ".emissao_CBR_spu " _
      & num_proposta _
      & ", " & num_cobranca _
      & ", " & num_via _
      & ", '" & Format(Data_Sistema, "yyyymmdd") & "'" _
      & ", '" & cUserName & "'"
      
      Set rc_Atualiza = rdocn.OpenResultset(SQL)
      rc_Atualiza.Close
Exit Sub

Erro:
    TrataErroGeral "Atualiza_pagamento", Me.name
    TerminaSEGBR

End Sub


Private Function Buscar_cedente(ByVal banco_id, agencia_id, conta_corrente_id As String)
 
'** Cobran�a
 
    Dim rc As rdoResultset
    
    On Error GoTo Erro
    
    SQL = "SELECT b.nome " _
        & " FROM conta_convenio_seg_tb a  WITH (NOLOCK)  , seguradora_tb b  WITH (NOLOCK)   " _
        & " WHERE a.agencia_id = " & agencia_id _
        & "   and a.banco_id = " & banco_id _
        & "   and a.conta_corrente_id = " & conta_corrente_id _
        & "   and b.seguradora_cod_susep = a.seguradora_cod_susep"
    Set rc = rdocn2.OpenResultset(SQL)
    
    If rc.EOF Then
       rc.Close
       
           '       Sql = "SELECT nome " _
'           & " FROM conta_transitoria_corretora_tb a  WITH (NOLOCK) , corretor_tb b  WITH (NOLOCK)  " _
'           & " WHERE a.agencia_id = " & agencia_id _
'           & "   and a.banco_id = " & banco_id _
'           & "   and a.conta_corrente_id = " & conta_corrente_id _
'           & "   and b.corretor_id = a.corretor_id"
          
'cristovao.rodrigues 28/10/2016 - 19368999/19370584 - cobran�a registrada AB e ABS
'select alterado
        
        SQL = "SELECT distinct   " & vbNewLine
        SQL = SQL & " b.nome, isnull(municipio_tb.nome, '') municipio, b.endereco, b.bairro, b.estado, b.cep, isnull(sucursal_corretor_tb.cgc, corretor_pf_tb.cpf) cpf_cnpj " & vbNewLine
        SQL = SQL & " FROM seguros_db..conta_transitoria_corretora_tb a  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & " inner join seguros_db..corretor_tb b  WITH (NOLOCK) " & vbNewLine
        SQL = SQL & "   on b.corretor_id = a.corretor_id " & vbNewLine
        SQL = SQL & " left join seguros_db..sucursal_corretor_tb with(nolock) " & vbNewLine
        SQL = SQL & "   on sucursal_corretor_tb.pj_corretor_id = b.corretor_id " & vbNewLine
        SQL = SQL & " left join seguros_db..corretor_pf_tb with(nolock) " & vbNewLine
        SQL = SQL & "   on corretor_pf_tb.pf_corretor_id = b.corretor_id " & vbNewLine
        SQL = SQL & " left join seguros_db..municipio_tb with(nolock) " & vbNewLine
        SQL = SQL & "   on municipio_tb.municipio_id = b.municipio_id " & vbNewLine
        SQL = SQL & "   and municipio_tb.estado = b.estado " & vbNewLine
        SQL = SQL & " WHERE a.agencia_id = " & agencia_id & vbNewLine
        SQL = SQL & "  and a.banco_id = " & banco_id & vbNewLine
        SQL = SQL & " and a.conta_corrente_id = " & conta_corrente_id & vbNewLine
       
       Set rc = rdocn2.OpenResultset(SQL)
    End If
    
    Buscar_cedente = Left(rc(0) + Space(60), 60)
    
    'cristovao.rodrigues 28/10/2016 - 19368999/19370584 - cobran�a registrada AB e ABS
    Cpf_Cnpj_Cedente = rc!Cpf_Cnpj
    Endereco_Cedente = rc!Endereco
    Bairro_Cedente = rc!Bairro
    Municipio_Cedente = rc!Municipio
    UF_Cedente = rc!Estado
    CEP_Cedente = rc!Cep
    
    rc.Close
 
Exit Function

Erro:
    TrataErroGeral "Buscar_cedente", Me.name
    TerminaSEGBR
    
End Function

Private Function calcula_dv_agencia_cc(ByVal Parte As String) As String
 
Dim i As Integer
'** Cobran�a
 
     Dim Peso As Integer
     Dim Soma As Integer
     Dim Parcela As Integer
     Dim dv As Integer
     Dim result As String
     
     Peso = 9
     Soma = 0
     For i = Len(Parte) To 1 Step -1
       Parcela = Peso * Val(Mid(Parte, i, 1))
       Soma = Soma + Parcela
       Peso = Peso - 1
       If Peso < 2 Then Peso = 9
     Next i
    
     dv = (Soma Mod 11)
     If dv = 10 Then
        result = "X"
     Else
        result = Format(dv, "0")
     End If
     calcula_dv_agencia_cc = result

End Function

Private Function calcula_mod10(ByVal Parte As String) As String
Dim i As Integer, dv As Long

'** Cobran�a
 
    Dim Peso As Integer
    Dim Soma As Integer
    Dim Parcela As Integer
    
    Peso = 2
    Soma = 0
    For i = Len(Parte) To 1 Step -1
      Parcela = Peso * Val(Mid(Parte, i, 1))
      If Parcela > 9 Then
         Parcela = Val(Mid(Format(Parcela, "00"), 1, 1)) + Val(Mid(Format(Parcela, "00"), 2, 1))
      End If
      Soma = Soma + Parcela
      If Peso = 2 Then Peso = 1 Else Peso = 2
    Next i
    
    dv = 10 - (Soma Mod 10)
    If dv > 9 Then dv = 0
    calcula_mod10 = Format(dv, "0")

End Function


Private Function calcula_mod11(ByVal Parte As String) As String
 
Dim i As Integer
'** Cobran�a
     Dim Peso As Integer
     Dim Soma As Integer
     Dim Parcela As Integer
     Dim dv As Integer
     
     Peso = 2
     Soma = 0
     For i = Len(Parte) To 1 Step -1
       If i <> 5 Then
          Parcela = Peso * Val(Mid(Parte, i, 1))
          Soma = Soma + Parcela
          Peso = Peso + 1
          If Peso > 9 Then Peso = 2
       End If
     Next i
    
     dv = 11 - (Soma Mod 11)
     If dv = 10 Or dv = 11 Then dv = 1
     calcula_mod11 = Format(dv, "0")

End Function

Sub Ler_Congeneres()
Dim SQL As String, rs As rdoResultset, aux As String, i As Long
ReDim Congenere(2, 10)

On Error GoTo Erro

    QtdCongeneres = 0
    '
    SQL = "SELECT a.perc_participacao, b.nome "
    SQL = SQL & " FROM co_seguro_repassado_tb a  WITH (NOLOCK)   "
    SQL = SQL & "   INNER JOIN seguradora_tb b  WITH (NOLOCK)  "
    SQL = SQL & "   ON a.rep_seguradora_cod_susep = b.seguradora_cod_susep "
    SQL = SQL & "   WHERE a.apolice_id = " & num_apolice
    SQL = SQL & "   AND a.seguradora_cod_susep = " & Seguradora
    SQL = SQL & "   AND a.sucursal_seguradora_id = " & Sucursal
    SQL = SQL & "   AND a.ramo_id = " & ramo_id
    If TpEmissao = "A" Then
       SQL = SQL & " AND (endosso_id = 0 or endosso_id is null) "
    Else
       SQL = SQL & " AND endosso_id = " & num_endosso
    End If
    '' Estas 2 linhas estavam comentadas, n�o sei pq. - 03/08/2000
    SQL = SQL & "   AND dt_inicio_participacao <= '" & Format(DtInicioVigencia, "yyyymmdd") & "'"
    SQL = SQL & "   AND (dt_fim_participacao is null "
    '' Adicionei mais esta condi��o
    SQL = SQL & "   OR dt_fim_participacao >= '" & Format(DtInicioVigencia, "yyyymmdd") & "')"
    '
    Set rs = rdocn.OpenResultset(SQL)
    ''
    i = 0
    If Not rs.EOF Then
        QtdLinhasCobertura = QtdLinhasCobertura + 1
        Do While Not rs.EOF
            If QtdCongeneres Mod 10 = 0 Then ReDim Preserve Congenere(2, QtdCongeneres + 10)
            Congenere(0, i) = rs!nome
            Congenere(1, i) = Format(Val(rs!perc_participacao), "##0.00")
            QtdLinhasCobertura = QtdLinhasCobertura + 1
            QtdCongeneres = QtdCongeneres + 1
            rs.MoveNext
        Loop
        rs.Close
        'para pular uma linha
        QtdLinhasCobertura = QtdLinhasCobertura + 1
    End If
    
Exit Sub

Erro:
    TrataErroGeral "Ler_Congeneres", Me.name
    TerminaSEGBR
    
End Sub

Sub Ler_Beneficiarios()
Dim SQL As String, rs As rdoResultset, i As Long
ReDim Benef(2, 10)

On Error GoTo Erro
'Seleciona Benefici�rios
SQL = "SELECT cod_objeto_segurado, nome FROM seguro_item_benef_tb  WITH (NOLOCK)   WHERE proposta_id=" & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (endosso_id=0 OR endosso_id is null )"
Else
   SQL = SQL & " AND endosso_id=" & num_endosso
End If
Set rs = rdocn.OpenResultset(SQL)
i = 0: QtdBenefs = 0
Do While Not rs.EOF
   If QtdBenefs Mod 10 = 0 Then
      ReDim Preserve Benef(2, QtdBenefs + 10)
   End If
   Benef(0, i) = rs!cod_objeto_segurado
   Benef(1, i) = ("" & rs!nome)
   rs.MoveNext
   i = i + 1
   QtdBenefs = QtdBenefs + 1
   QtdLinhasCobertura = QtdLinhasCobertura + 1
Loop

Exit Sub
Erro:
    TrataErroGeral "Rotina: Ler_Beneficiarios", Me.name
    TerminaSEGBR
    
End Sub

Sub Ler_DescricaoEndosso()
Dim SQL As String, rs As rdoResultset, linha As Long, DescrEndosso As String
Dim ultQuebra As Long, Ultpos As Long, i As Long, aux As String
Dim Endosso() As String, RegClausula As Integer

On Error GoTo Erro

linha = 0
'
'Demanda 4532649 - Jos� Edson
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    linha = linha + 1
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(45) & "A N E X O S"
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    linha = linha + 1
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    linha = linha + 1
'--------------------


SQL = "SELECT descricao_endosso FROM endosso_tb  WITH (NOLOCK)   "
SQL = SQL & "   WHERE proposta_id = " & num_proposta
SQL = SQL & "   AND   endosso_id = " & num_endosso
'
Set rs = rdocn.OpenResultset(SQL)
If Not rs.EOF Then
    ReDim Endosso(17)
    ''
    DescrEndosso = Formata_Clausula("" & rs(0))
    'Demanda 4532649 - Jos� Edson
    If Len(LTrim(DescrEndosso)) = 0 Then
          Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & "N�o h� texto cadastrado para esse endosso"
          Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
          linha = linha + 1
          Exit Sub
    End If
    '-----------------------------------------
    
    ultQuebra = 1: Ultpos = 0
    For i = 1 To Len(DescrEndosso)
        If Mid(DescrEndosso, i, 2) = Chr(13) & Chr(10) Then
            linha = linha + 1
            If linha Mod 17 = 0 Then
                ReDim Preserve Endosso(UBound(Endosso) + 17)
            End If
            'Demanda 4532649 - Jos� Edson
            'aux = Mid(DescrEndosso, ultQuebra, Ultpos - 2)
            aux = Mid(DescrEndosso, ultQuebra, Ultpos - IIf(Ultpos < 2, Ultpos, 2))
           
            Endosso(linha) = aux
            Ultpos = 0
            ultQuebra = i + 2
        End If
        Ultpos = Ultpos + 1
    Next
End If
rs.Close

If linha > 17 Then
    RegClausula = 22
Else
   'Se n� de linhas � menor que 17 e as coberturas n�o foram listadas na primeira p�gina
    If Not CoberturasPrimPagina Then
        RegClausula = 21
    Else
        RegClausula = 22
    End If
End If
'
If linha > 0 Then
    Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
    
    'Demanda 4532649 - Jos� Edson
    'Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
     Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)

    
    ContaLinhaAtual = ContaLinhaAtual + 1
    '
    For i = 1 To linha
        Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
        Reg = Reg & Endosso(i)
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
    Next
    
'Demanda 4532649 -  Jos� Edson
Else
      Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & "N�o h� texto cadastrado para esse endosso"
      Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
      linha = linha + 1
'--------------------------------------------------------

End If
Exit Sub

Erro:
    TrataErroGeral "Rotina: Ler_DescricaoEndosso", Me.name
    TerminaSEGBR
    
End Sub

Sub Ler_Produto()

Dim SQL As String
Dim rc As rdoResultset

On Error GoTo Erro
    
    Reg = Reg & Format$(ProdutoId, "000") & " - "
    Reg = Reg & UCase(Left(NomeProduto & Space(27), 27))
    
    'Busca Produto Externo
    
    SQL = "SELECT produto_externo_id "
    SQL = SQL & "FROM arquivo_produto_tb  WITH (NOLOCK)   "
    SQL = SQL & "WHERE produto_id = " & ProdutoId
    '
    Set rc = rdocn1.OpenResultset(SQL)
    If Not rc.EOF Then
        produto_externo_id = rc!produto_externo_id
    Else
        produto_externo_id = 0
    End If
    rc.Close

    Exit Sub
    
Erro:
    'TrataErroGeral "Erro obtendo produto externo para o produto " & ProdutoId & " . Programa ser� cancelado"
    TrataErroGeral "Ler_Produto", Me.name
    TerminaSEGBR
    
End Sub

Sub Ler_Proposta_Adesao_OuroVidaEmp()

'Declara��es''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Dim QtdParcelas As Long, rs As rdoResultset, i As Long, Valcobranca As Double
Dim ValUltParcela As Currency, ValParcela1 As Currency, ValParcelaDemais As Currency
Dim ValIof As Currency, ValJuros As Currency, CustoApolice As Currency
Dim PremioTarifa As Currency, PremioBruto As Currency, QtdDatas As Integer
Dim DataAgendamento() As String, PremioLiquido As Currency
Dim Nome_agencia As String

On Error GoTo Erro
                               
QtdParcelas = 12

'Selecionando dados da proposta de adesao'''''''''''''''''''''''''''''''''''''''''''''''''''
SQL = "SELECT "
SQL = SQL & " p.apolice_id, "
SQL = SQL & " p.proposta_bb, "
SQL = SQL & " p.proposta_id, "
SQL = SQL & " p.cont_agencia_id, "
SQL = SQL & " p.cont_banco_id, "
SQL = SQL & " p.deb_agencia_id, "
SQL = SQL & " p.deb_banco_id, "
SQL = SQL & " isnull(p.forma_pgto_id, 99) forma_pgto_id, "
SQL = SQL & " m.sigla sigla_premio, "
SQL = SQL & " p.premio_moeda_id, "
SQL = SQL & " m1.sigla sigla_seg, "
SQL = SQL & " p.seguro_moeda_id "
SQL = SQL & " FROM "
SQL = SQL & "       proposta_adesao_tb p  WITH (NOLOCK)   "
SQL = SQL & "       inner join moeda_tb as m  WITH (NOLOCK)   on p.premio_moeda_id = m.moeda_id "
SQL = SQL & "       inner join moeda_tb as m1   WITH (NOLOCK)  on p.seguro_moeda_id = m1.moeda_id "
SQL = SQL & " WHERE "
SQL = SQL & "       proposta_id = " & num_proposta

Set rc = rdocn.OpenResultset(SQL)
        
If Not rc.EOF Then
    'Gravando Apolice anterior ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If PropAnt = 0 Then
        Reg = Reg & "000000000"
    Else
        'Apolice Renovada (No layout esta errado, est� com proposta renova��o)''''''''''
        'Caso haja proposta anterior e esta estiver em proposta fechada, ent�o existe uma
        'Ap�lice individual para esta proposta, pois as propostas do Ouro Vida Empresa
        'migradas do Sise possuem possuem cada uma a sua apolice respectiva.
        SQL = "Select * from proposta_fechada_tb  WITH (NOLOCK)   where proposta_id = " & PropAnt
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            SQL = "SELECT apolice_id  FROM apolice_tb  WITH (NOLOCK)   "
            SQL = SQL & "  WHERE  proposta_id = " & PropAnt
            Set Rc1 = rdocn.OpenResultset(SQL)
            If Not Rc1.EOF Then
               Reg = Reg & Format(Rc1(0), "000000000")
            Else
               MensagemBatch "Ap�lice anterior da proposta " & PropAnt & " n�o foi encontrada."
            End If
        Else
            'Caso seja uma proposta de adesao a ap�lice � a mesma da proposta renovada.
            Reg = Reg & Format(rc!Apolice_id, "000000000")
        End If
    End If
    
    'Gravando Proposta_bb
    Reg = Reg & IIf(IsNull(rc!proposta_bb), "000000000", Format(rc!proposta_bb, "000000000"))

    'C�digo da ag�ncia
    ContAgencia = IIf(IsNull(rc!cont_agencia_id), "0000", Format(rc!cont_agencia_id, "0000"))

    'Gravando Dados da Ag�ncia do Cliente''''''''''''''''''''''''''''''''''''''''''''''''''
    'Obs.: S� apresentar a linha se a ag�ncia existir
    Nome_agencia = ""
    
    
    '''Leandro A. Souza - Stefanini IT - flow  197392
    If Not IsNull(rc!deb_agencia_id) And Not IsNull(rc!deb_banco_id) Then
        SQL = " SELECT nome FROM agencia_tb  WITH (NOLOCK)   "
        SQL = SQL & " WHERE agencia_id = " & rc!deb_agencia_id
        SQL = SQL & " AND banco_id = " & rc!deb_banco_id
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            If Not IsNull(Rc1(0)) Then
                Reg = Reg & Format(rc!deb_agencia_id, "0000")
                Reg = Reg & " - " & UCase(Left(Trim(Rc1(0)) & Space(48), 48))
                'Nome_agencia = Nome_agencia + " - " + Trim(Rc1(0)) ????
            Else
                Reg = Reg & Space(55)
            End If
        Else
            Reg = Reg & Space(55)
        End If
        
        Rc1.Close
        Set Rc1 = Nothing
    Else
        Reg = Reg & Space(55)
    End If
        
    'Gravando Dados da Ag�ncia de Cobran�a'''''''''''''''''''''''''''''''''''''''''''''''''
    'Obs.: S� apresentar a linha se a ag�ncia existir
    If Not IsNull(rc!cont_agencia_id) Then
        Reg = Reg & Format(rc!cont_agencia_id, "0000") & " - "
        SQL = " SELECT nome FROM agencia_tb  WITH (NOLOCK)   "
        SQL = SQL & " WHERE agencia_id = " & rc!cont_agencia_id
        SQL = SQL & " AND banco_id = " & rc!cont_banco_id
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            If Not IsNull(Rc1(0)) Then
                Reg = Reg & UCase(Left(Trim(Rc1(0)) & Space(16), 16))
            Else
                Reg = Reg & Space(16)
            End If
        Else
            Reg = Reg & "NAO IDENTIFICADA"
        End If
        Rc1.Close
        Set Rc1 = Nothing
    Else
        Reg = Reg & "9999 - NAO IDENTIFICADA"
    End If
    
    'Gravando valores ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    MoedaPremio = Trim(rc!sigla_premio)
    MoedaSeguro = Trim(rc!sigla_seg)
    
    'Valor IOF
    Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    
    'Valor Custo Ap�lice
    Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    
    'Valor Juros
    Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    
    'Valor Pr�mio Liquido
    Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    
    'Valor Pr�mio Bruto
    Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
   
    'Gravando dados de parcelamento''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    'Quantidade de Parcelas
    Reg = Reg & "Qt.Parcelas " & Format$(QtdParcelas, "00")
    
    'N�o preencheremos para o Ouro Vida Empres aos campos referentes ao valor da 1�,
    'demais e �ltima parcela''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' Altera��o: O campo Valor Demais Parcelas passar� a ter, como conte�do, a constante "MENSAL". (Marisa)
    Reg = Reg & String(28, " ")
    Reg = Reg & "MENSAL" & String(28, " ")
    Reg = Reg & String(34, " ")

    'Gravando Datas de Vencimento''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    SQL = "SELECT num_cobranca, dt_agendamento "
    SQL = SQL & "FROM agendamento_cobranca_tb  WITH (NOLOCK)   "
    SQL = SQL & " WHERE proposta_id = " & num_proposta
    ''Sql = Sql & " AND situacao in ('a','e','i','r','p') "           '??
    SQL = SQL & "  ORDER BY num_cobranca"
    Set Rc1 = rdocn.OpenResultset(SQL)
    If Not Rc1.EOF Then
        Do While Not Rc1.EOF
            Reg = Reg & Format$(Rc1!num_cobranca, "00") & "-" & Format(Rc1!dt_agendamento, "dd/mm/yyyy")
            Rc1.MoveNext
        Loop
    End If
    Rc1.Close
   
    'Gravando Forma de Cobran�a'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    If Not IsNull(rc!forma_pgto_id) Then
        SQL = "SELECT nome FROM forma_pgto_tb  WITH (NOLOCK)   "
        SQL = SQL & "WHERE forma_pgto_id = " & rc!forma_pgto_id
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            If Not IsNull(Rc1(0)) Then
                Reg = Reg & UCase(Left(Rc1(0) & Space(30), 30))
            End If
        Else
            Reg = Reg & Space(30)
        End If
        Rc1.Close
    Else
        Reg = Reg & Space(30)
    End If
Else
    Call Ler_Proposta_Fechada
End If
    
rc.Close
Set rc = Nothing

Exit Sub

Erro:
   TrataErroGeral "Ler_Proposta_Adesao_OuroVidaEmp", Me.name
   TerminaSEGBR

End Sub
Sub Ler_Vigencia()
Dim SQL As String
Dim rc As rdoResultset
Dim InicioVigencia As String, FimVigencia As String

On Error GoTo Erro

    '' Em Vida, endossos de cobran�a s�o faturas,
    '' que s�o impressas em outro layout
    ''If TpEmissao = "E" Then 'Verifica se � endosso de fatura, para buscar in�cio e fim do endosso
    
    ''    Sql = "SELECT dt_inicio_vigencia, dt_fim_vigencia "
    ''    Sql = Sql & "FROM fatura_tb  WITH (NOLOCK)   "
    ''    Sql = Sql & "   WHERE   apolice_id = " & num_apolice
    ''    Sql = Sql & "   AND     sucursal_seguradora_id = " & Sucursal
    ''    Sql = Sql & "   AND     seguradora_cod_susep = " & Seguradora
    ''    Sql = Sql & "   AND     ramo_id = " & ramo_id
    ''    Sql = Sql & "   AND     endosso_id = " & num_endosso
        '
    ''    Set rc = rdocn.OpenResultset(Sql)
    ''    If Not rc.EOF Then
    ''        InicioVigencia = Format$(rc(0), "dd/mm/yyyy")
    ''        FimVigencia = Format$(rc(1), "dd/mm/yyyy")
    ''    Else
    ''        InicioVigencia = DtInicioVigencia
    ''        FimVigencia = FimVig
    ''    End If
    ''    rc.Close
    ''Else
        InicioVigencia = DtInicioVigencia
        FimVigencia = FimVig
    ''End If
    
    'In�cio Vig�ncia
    INI = Mid(InicioVigencia, 1, 2) & " de "
    Select Case Mid(InicioVigencia, 4, 2)
        Case "01": mes = "Janeiro"
        Case "02": mes = "Fevereiro"
        Case "03": mes = "Mar�o"
        Case "04": mes = "Abril"
        Case "05": mes = "Maio"
        Case "06": mes = "Junho"
        Case "07": mes = "Julho"
        Case "08": mes = "Agosto"
        Case "09": mes = "Setembro"
        Case "10": mes = "Outubro"
        Case "11": mes = "Novembro"
        Case "12": mes = "Dezembro"
    End Select
    INI = INI & mes & " de " & Mid(InicioVigencia, 7, 4)
    Reg = Reg & Left(INI & Space(23), 23)
        
    'Fim Vig�ncia
    If FimVigencia <> "" Then
        Fim = Mid(FimVigencia, 1, 2) & " de "
        Select Case Mid(FimVigencia, 4, 2)
            Case "01": mes = "Janeiro"
            Case "02": mes = "Fevereiro"
            Case "03": mes = "Mar�o"
            Case "04": mes = "Abril"
            Case "05": mes = "Maio"
            Case "06": mes = "Junho"
            Case "07": mes = "Julho"
            Case "08": mes = "Agosto"
            Case "09": mes = "Setembro"
            Case "10": mes = "Outubro"
            Case "11": mes = "Novembro"
            Case "12": mes = "Dezembro"
        End Select
        Fim = Fim & mes & " de " & Mid(FimVigencia, 7, 4)
    Else
        Fim = ""
    End If
    Reg = Reg & Left(Fim & Space(23), 23)
    '
    Exit Sub
    
Erro:
    'TrataErroGeral "Erro obtendo vigencia da Ap�lice " & num_apolice & " Ramo " & ramo_id & " . Programa ser� cancelado"
    TrataErroGeral "Ler_Vigencia", Me.name
    TerminaSEGBR
    
End Sub

' CONTA_LINHA era vari�vel; devido a solicita��o de separar a emiss�o de ap�lice em dois
' arquivos distintos, optou-se por transformar a vari�vel em uma fun��o chamada
' LINHAATUAL (Jo�o Mac-Cormick - 30/6/2000)
Private Function LinhaAtual() As String
  
    LinhaAtual = Format(Se(EnviaCliente, ContaLinha1, ContaLinha2), "000000")
    
End Function
Sub Lista_Beneficiarios(ByVal RegClausula As Integer, Optional ByVal ObjSegurado As Long)
Dim PriVez As Boolean, j As Integer, ObjSeguradoAnterior As Integer

'Lista Benefici�rios do �ltimo obj. segurado

On Error GoTo Erro

PriVez = True: ObjSeguradoAnterior = -1
If QtdBenefs > 0 Then
   For j = 0 To QtdBenefs - 1
      'No caso de endosso de benefici�rio (somente) listar tb o t�tulo do item
      If ObjSegurado = 0 Then
         If Val(Benef(0, j)) <> ObjSeguradoAnterior Then
            'T�tulo
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "ITEM " & Format(Benef(0, j), "00") & ": "
            Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "BENEFICI�RIO(S) : "
            ObjSeguradoAnterior = Val(Benef(0, j))
         Else
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(18)
         End If
         Reg = Reg & Trim(Benef(1, j))
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      Else
         If Val(Benef(0, j)) = ObjSegurado Then
            If PriVez Then
               'Titulo
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "BENEFICI�RIO(S) : "
               PriVez = False
            Else
               Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(18)
            End If
            Reg = Reg & Trim(Benef(1, j))
            Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
         End If
      End If
   Next
End If

Exit Sub

Erro:
    TrataErroGeral "Lista Beneficiarios", Me.name
    TerminaSEGBR

End Sub

Sub Lista_Coberturas(ByVal RegClausula As Integer)
Dim ObjAnterior As Integer, i As Long, j As Long, Endereco As String, linhaFranquia As String

On Error GoTo Erro

ObjAnterior = 0
For i = 0 To QtdCoberturas - 1
   If Cobertura(0, i) <> ObjAnterior Then
      If i > 0 Then
         'Monta linhas de Benefici�rios do obj. segurado anterior
         Lista_Beneficiarios RegClausula, ObjAnterior
         
         'Pula uma linha para cada novo obj. segurado
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      End If
      
      'Atualiza obj Anterior
      ObjAnterior = Cobertura(0, i)

      'T�tulo do item
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "ITEM " & Format$(Cobertura(0, i), "00") & ":" & Space(8)
      Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      'Local do Risco
'      For j = 1 To QtdObjetos
'         If EnderecoRisco(0, j) = Cobertura(0, i) Then
'            Endereco = EnderecoRisco(1, j)
'         End If
'      Next
'      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'      Reg = Reg & "LOCAL DO RISCO: " & Endereco
'      Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'      ContaLinhaAtual = ContaLinhaAtual + 1
       'T�tulo Coberturas
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA                                                I.S(" & MoedaSeguro & ")"
      Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & Format$(Cobertura(1, i), "000") & Space(8)
   'Reg = Reg & Left(Cobertura(2, i) & Space(200), 200)     '200 caracteres para a descri��o sem UCase 27/10/2003
   Reg = Reg & Left(Cobertura(2, i) & Space(72), 72)     '72 caracteres para a descri��o sem UCase 19/10/2004
               
   'Imp Segurada
   If ConfiguracaoBrasil Then
       Reg = Reg & MoedaSeguro & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
   Else
       Reg = Reg & MoedaSeguro & Right(Space(16) & TrocaValorAmePorBras(Format(Cobertura(3, i), "#,###,###,##0.00")), 16)
   End If
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   
   'Franquia
   linhaFranquia = ""
   If Cobertura(4, i) <> 0 Or Cobertura(5, i) <> "" Or Cobertura(6, i) <> 0 Then
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "FRANQUIA : "
      If Cobertura(4, i) <> 0 Then
         If ConfiguracaoBrasil Then
             linhaFranquia = linhaFranquia & Right(Space(8) & Format(Cobertura(4, i), "##0.00") & " %", 8)
         Else
             linhaFranquia = linhaFranquia & Right(Space(8) & TrocaValorAmePorBras(Format(Cobertura(4, i), "##0.00")) & " %", 8)
         End If
      End If
      If Cobertura(5, i) <> "" Then
         'Colocar separador caso o anterior estiver preenchido
         If linhaFranquia <> "" Then
            linhaFranquia = linhaFranquia & " - "
         End If
         linhaFranquia = linhaFranquia & Trim(Cobertura(5, i))
      End If
      If Cobertura(6, i) <> 0 Then
         'Colocar separador caso o anterior estiver preenchido
         If linhaFranquia <> "" Then
            linhaFranquia = linhaFranquia & " - M�nimo de: R$ "
         End If
         If ConfiguracaoBrasil Then
            linhaFranquia = linhaFranquia & Format(Cobertura(6, i), "#,###,###,##0.00")
         Else
            linhaFranquia = linhaFranquia & TrocaValorAmePorBras(Format(Cobertura(6, i), "#,###,###,##0.00"))
         End If
     End If
      Reg = Reg & linhaFranquia
      Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   End If
Next

'Monta linhas de Benefici�rios do �ltimo obj. segurado
Lista_Beneficiarios RegClausula, ObjAnterior

'Pulando uma linha...
Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & String(16, " ")
Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
ContaLinhaAtual = ContaLinhaAtual + 1

Exit Sub

Erro:
    TrataErroGeral "Lista Coberturas", Me.name
    TerminaSEGBR

End Sub
Sub lixo()
'SQL = "SELECT count(*)  From escolha_tp_cob_aceito_tb  a  WITH (NOLOCK)  , proposta_tb b  WITH (NOLOCK)  ," & _
'      " proposta_basica_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb d  WITH (NOLOCK)   " & _
'      " WHERE b.proposta_id = " & num_proposta & " AND " & _
'      "      c.proposta_id = b.proposta_id AND " & _
'      "      a.proposta_id = b.proposta_id AND " & _
'      "      c.ramo_id = a.ramo_id AND " & _
'      "      a.dt_fim_vigencia_esc is null AND " & _
'      "      b.produto_id = d.produto_id AND " & _
'      "      d.tp_cobertura_id = a.tp_cobertura_id AND " & _
'      "      c.ramo_id = d.ramo_id "
'If produto_externo_id <> 999 Then
'      SQL = SQL & "      d.acumula_is = 's' "
'End If

'If Trim(DtInicioVigencia) <> "" Then
'    SQL = SQL & _
'          " AND (e.dt_inicio_vigencia_esc <= '" & Format(dtIniVigencia, "yyyymmdd") & "') AND " & _
'          "     (e.dt_fim_vigencia_esc >= '" & Format(dtIniVigencia, "yyyymmdd") & "' OR " & _
'          "      e.dt_fim_vigencia_esc IS NULL ) "


'   'Valor 1� Parcela
'   If Not IsNull(rc!val_pgto_ato) Then
'       If ConfiguracaoBrasil Then
'           Reg = Reg & "Parcela 1       : " & MoedaPremio & Right(Space(16) & Format(rc!val_pgto_ato, "#,###,###,##0.00"), 16)
'       Else
'           Reg = Reg & "Parcela 1       : " & MoedaPremio & Right(Space(16) & TrocaValorAmePorBras(Format(Val(rc!val_pgto_ato), "#,###,###,##0.00")), 16)
'       End If
'   Else
'       Reg = Reg & "Parcela 1       : " & MoedaPremio & "         0,00"
'   End If
'
'   'Valor Demais Parcelas
'   If Not IsNull(rc!val_parcela) Then
'       If ConfiguracaoBrasil Then
'           Reg = Reg & "Demais Parcelas : " & MoedaPremio + Right(Space(16) + Format(rc!val_parcela, "#,###,###,##0.00"), 16)
'       Else
'           Reg = Reg & "Demais Parcelas : " & MoedaPremio + Right(Space(16) + TrocaValorAmePorBras(Format(Val(rc!val_parcela), "#,###,###,##0.00")), 16)
'       End If
'   Else
'       Reg = Reg & "Demais Parcelas : " & MoedaPremio & "         0,00"
'   End If

End Sub

Sub Monta_ColecaoPagamentos(ByVal proposta As String, ByVal NumCobranca As String, ByVal NumVia As String)
Dim novoPagamento As New Pagamento

With novoPagamento
   .NumCobranca = NumCobranca
   .NumVia = NumVia
   .proposta = proposta
End With

Pagamentos.Add novoPagamento

End Sub

Function Monta_SqlCoberturas() As String

On Error GoTo Erro

SQL = "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_aceito_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t  WITH (NOLOCK)   "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_avulso_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t  WITH (NOLOCK)   "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_cond_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t   WITH (NOLOCK)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_emp_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t   WITH (NOLOCK)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia,  t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_maq_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t   WITH (NOLOCK)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

SQL = SQL & " UNION "

SQL = SQL & "SELECT t.tp_cobertura_id, c.nome, e.val_is, "
SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
SQL = SQL & "e.ramo_id,  e.val_min_franquia, e.texto_franquia, e.fat_franquia, t.acumula_is , "
SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
SQL = SQL & "FROM escolha_tp_cob_res_tb e  WITH (NOLOCK)  , tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_item_prod_tb t   WITH (NOLOCK)  "
SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.produto_id = " & ProdutoId & ")      AND "
SQL = SQL & "       (t.tp_cobertura_id = e.tp_cobertura_id) AND "
SQL = SQL & "       (e.ramo_id = t.ramo_id)                 AND "
If TpEmissao = "A" Then
   SQL = SQL & " (num_endosso=0 or num_endosso is null) "
Else
   SQL = SQL & " num_endosso=" & num_endosso
   SQL = SQL & " AND e.dt_fim_vigencia_esc is null "
End If

Monta_SqlCoberturas = SQL

Exit Function

Erro:
    TrataErroGeral "Monta SQL Coberturas", Me.name
    TerminaSEGBR

End Function

Private Sub Montar_linha_digitavel()
 
'** Cobran�a
 
    Dim Parte1                                     As String
    Dim Parte2                                     As String
    Dim Parte3                                     As String
    Dim Dv1                                        As String
    Dim Dv2                                        As String
    Dim Dv3                                        As String
    Dim Dv_geral                                   As String
    Dim Codigo_barras_1                            As String
    Dim Codigo_barras_2                            As String
    Dim Codigo_barras_3                            As String
       
    Parte1 = "0019" 'c�digo banco + dv
    Parte1 = Parte1 & Left(Nosso_Numero, 1) _
           & Mid(Nosso_Numero, 2, 4)
    Dv1 = calcula_mod10(Parte1)
    Parte1 = Mid(Parte1, 1, 5) & "." & Mid(Parte1, 6, 4) & Dv1
    
    Parte2 = Mid(Nosso_Numero, 6, 5) _
           & Mid(Nosso_Numero, 11, 1) _
           & Format(agencia_id, "0000") '"0452"   'Ag�ncia cedente
    Dv2 = calcula_mod10(Parte2)
    Parte2 = Mid(Parte2, 1, 5) & "." & Mid(Parte2, 6, 5) & Dv2
    
    Parte3 = Format(conta_corrente_id, "00000000") '"00405200"  'conta cedente
    Parte3 = Parte3 & Left(Carteira, 2)
    Dv3 = calcula_mod10(Parte3)
    Parte3 = Mid(Parte3, 1, 5) & "." & Mid(Parte3, 6, 5) & Dv3
    
    Codigo_barras_1 = "0019" 'c�digo banco + dv
    
    ' acrescentado a diferen�a de dias entre a data de vencimento e (7/10/97)
    ' para a forma��o da linha digit�vel -- Jo�o Mac-Cormick em 19/3/2001
    Dim Fator
    Fator = Format(DateDiff("d", "07/10/1997", dt_agendamento), "0000")
    Codigo_barras_2 = Fator & Format(Val_Cobranca * 100, "0000000000")
    
    Codigo_barras_3 = Format(Nosso_Numero, "00000000000") & Left(agencia, 4) & Left(Codigo_Cedente, 8) _
                    & Left(Carteira, 2)
    codigo_barras = Codigo_barras_1 & " " & Codigo_barras_2 & Codigo_barras_3
    Dv_geral = calcula_mod11(codigo_barras)
    
    codigo_barras = Left(Codigo_barras_1 & Dv_geral & Codigo_barras_2 & Codigo_barras_3 & Space(44), 44)
    
    ' acerto da linha digit�vel -- Jo�o Mac-Cormick em 16/5/2001
    linha_digitavel = Right(Space(54) & Parte1 & " " & Parte2 & " " & Parte3 & " " & Dv_geral & " " _
                      & Fator & Format(Val_Cobranca * 100, "0000000000"), 54)

End Sub

Function Obtem_Dados_Cliente(lnPropostaId As Long) As Boolean
Dim rc_Dados_Cliente As rdoResultset
Dim SQL As String
Dim nome As String, Endereco As String
Dim Bairro As String, Municipio As String
Dim Cep As String, UF As String
    
On Error GoTo Erro
'** Cobran�a
    Obtem_Dados_Cliente = False
    '
    SQL = "SELECT b.nome, c.endereco, c.bairro, c.municipio, c.cep, c.estado "
    SQL = SQL & " FROM proposta_tb a  WITH (NOLOCK)  , cliente_tb b  WITH (NOLOCK)  , endereco_corresp_tb c   WITH (NOLOCK)  "
    SQL = SQL & " WHERE a.proposta_id = " & lnPropostaId
    SQL = SQL & " AND a.prop_cliente_id = b.cliente_id "
    SQL = SQL & " AND a.proposta_id = c.proposta_id "
    
    Set rc_Dados_Cliente = rdocn2.OpenResultset(SQL)
      
    Sacado_1 = ""
    Sacado_2 = ""
    Sacado_3 = ""
    
    If Not rc_Dados_Cliente.EOF Then
        nome = UCase(Left(rc_Dados_Cliente(0) & Space(50), 50))
        Endereco = UCase(Left(rc_Dados_Cliente(1) & Space(50), 50))
        Bairro = UCase(Left(rc_Dados_Cliente(2) & Space(30), 30))
        Municipio = UCase(Left(rc_Dados_Cliente(3) & Space(45), 45))
        Cep = Format(rc_Dados_Cliente(4), "00000000")
        UF = rc_Dados_Cliente(5)
    Else
        Exit Function
    End If
           
    rc_Dados_Cliente.Close
    
    Sacado_1 = Left(nome & Space(60), 60)
    Sacado_2 = Left(Endereco & Space(60), 60)
    Sacado_3 = Left(Cep & " " & Trim(Bairro) & " " & Trim(Municipio) & " " & UF & Space(60), 60)
    '
    Obtem_Dados_Cliente = True
    
    Exit Function
      
Erro:
    TrataErroGeral "Obtem_Dados_Cliente", Me.name
    TerminaSEGBR
    
End Function


Private Sub Processa_Cobranca()

'** Cobran�a
Dim rc_pagamentos                     As rdoResultset
Dim rc                                As rdoResultset
Dim vPagamento                        As New Pagamento
Dim SQL                               As String
Dim Reg                               As String
Dim TraillerArq                       As String
Dim proposta                          As Long
Dim Produto                           As String
Dim num_via                           As String
Dim num_convenio                      As String
Dim banco_id                          As String
'Dim agencia_id                        As String
'Dim conta_corrente_id                 As String
Dim Conta_cobrancas                   As Long
Dim num_cobranca                      As Long
Dim Val_Cobranca                      As Double
Dim val_iof                           As Double
Dim Dt_inclusao                       As String
Dim ramo_id                           As String
Dim Apolice_id                        As String
Dim Local_pagto                       As String
Dim Cedente                           As String
Dim Especie_doc                       As String
Dim Aceite                            As String
Dim dt_processamento                  As String
Dim nosso_numero_2                    As String
Dim Num_Conta                         As String
Dim Especie                           As String
Dim Valor_unitario                    As String
Dim Valor_documento                   As String
Dim Quantidade                        As String
Dim linha_1                           As String
Dim linha_2                           As String
Dim linha_3                           As String
Dim linha_4                           As String
Dim linha_5                           As String
Dim Convenio                          As String
Dim Mensagem_Quitada                  As Boolean
Dim MoedaNacional, moeda_id, EmissaoApolice
Dim moeda_atual                       As Integer

On Error GoTo Erro
     
Conta_cobrancas = 0
'' Obtem o codigo da moeda padr�o.
moeda_atual = Val(Le_Parametro_Geral("MOEDA ATUAL"))

'Obtem os pagamentos ainda n�o emitidos: registros de emissao_CBR_tb com dt_emissao = Null
'Referentes a Ap�lices emitidas

SQL = "SELECT a.num_cobranca, val_cobranca, dt_agendamento, "
SQL = SQL & "       c.produto_id, a.proposta_id, b.val_iof, c.dt_proposta, "
SQL = SQL & "       b.apolice_id, b.ramo_id, a.num_via, "
SQL = SQL & "       nosso_numero = isNull(nosso_numero, 0), "
SQL = SQL & "       nosso_numero_dv = isNull(nosso_numero_dv, ' '), "
SQL = SQL & "       pf.premio_moeda_id moeda_ap, ef.premio_moeda_id moeda_endo, "
SQL = SQL & "       pa.premio_moeda_id moeda_ad "
SQL = SQL & " FROM emissao_CBR_tb a  WITH (NOLOCK)  "
SQL = SQL & " JOIN agendamento_cobranca_tb b  WITH (NOLOCK)   "
SQL = SQL & "   ON b.proposta_id = a.proposta_id "
SQL = SQL & "  and b.num_cobranca = a.num_cobranca "
SQL = SQL & " JOIN proposta_tb c  WITH (NOLOCK)  "
SQL = SQL & "   ON c.proposta_id = a.proposta_id "
SQL = SQL & " LEFT JOIN proposta_fechada_tb pf  WITH (NOLOCK)  "
SQL = SQL & "   ON a.proposta_id = pf.proposta_id "
SQL = SQL & " LEFT JOIN proposta_adesao_tb pa  WITH (NOLOCK)  "
SQL = SQL & "   ON a.proposta_id = pa.proposta_id "
SQL = SQL & " LEFT JOIN endosso_financeiro_tb ef  WITH (NOLOCK)   "
SQL = SQL & "   ON a.proposta_id = ef.proposta_id "
SQL = SQL & " WHERE dt_emissao is Null "
SQL = SQL & "   and a.proposta_id = " & num_proposta
'
If TpEmissao = "A" Then
    SQL = SQL & " AND (b.num_endosso = 0 or b.num_endosso is null) "
Else
    SQL = SQL & " AND b.num_endosso = " & num_endosso
End If
 
Set rc_pagamentos = rdocn.OpenResultset(SQL)
   
While Not rc_pagamentos.EOF
    DoEvents
    ''
    num_cobranca = Format(rc_pagamentos("num_cobranca"), "0000")
    Val_Cobranca = Val(rc_pagamentos("val_cobranca"))
    dt_agendamento = Format(rc_pagamentos("dt_agendamento"), "dd-mm-yyyy")
    proposta = rc_pagamentos("proposta_id")
    val_iof = Val(rc_pagamentos("val_iof"))
    Nosso_Numero = rc_pagamentos("nosso_numero")
    Nosso_numero_dv = rc_pagamentos("nosso_numero_dv")
    num_via = rc_pagamentos("num_via")
    Dt_inclusao = rc_pagamentos("dt_proposta")
    ramo_id = rc_pagamentos("ramo_id")
    Apolice_id = rc_pagamentos("apolice_id")
    Produto = ProdutoId
             
    EmissaoApolice = (TpEmissao = "A")
    If Produto = 15 Then
        moeda_id = IIf(IsNull(rc_pagamentos("moeda_ad")), moeda_atual, rc_pagamentos("moeda_ad"))
    Else
        If EmissaoApolice Then
            moeda_id = IIf(IsNull(rc_pagamentos("moeda_ap")), moeda_atual, rc_pagamentos("moeda_ap"))
        Else
            moeda_id = IIf(IsNull(rc_pagamentos("moeda_endo")), rc_pagamentos("moeda_ap"), rc_pagamentos("moeda_endo"))
        End If
    End If
    '
    MoedaNacional = (moeda_id = moeda_atual)
    '
    Sacado_1 = ""
    Sacado_2 = ""
    Sacado_3 = ""
    '' Ler o cliente diretamente na leitura da ap�lice
    ''
    'If Obtem_Dados_Cliente(Proposta) = False Then
    '    Exit Sub
    'End If
    Sacado_1 = Left(nome & Space(60), 60)
    Sacado_2 = Left(Endereco & Space(60), 60)
    Sacado_3 = Left(Cep & " " & Trim(Bairro) & " " & Trim(Municipio) & " " & UF & Space(60), 60)

    Local_pagto = Left("QUALQUER AG�NCIA" + Space(60), 60)
   
    'Produto = Format(ProdutoId, "000")
    Produto = Format(ProdutoId, "0000")
    '
    'Convenio = Mid(Nosso_numero, 1, 6)
    'Sql = "SELECT num_convenio FROM tp_movimentacao_financ_tb  WITH (NOLOCK)   "
    'Sql = Sql & "   WHERE produto_id = " & produtoid
    ''SQL = SQL & "   AND tp_operacao_financ_id=" & num_cobranca
    'Sql = Sql & "   AND ramo_id = " & ramo_id
    'Sql = Sql & "   AND num_convenio in ('" & Convenio & "', '" & Mid(Convenio, 1, 4) & "')"
    ''
    'Set rc = rdocn2.OpenResultset(Sql)
    'Convenio = rc(0)
   
    '
    'Sql = "SELECT banco_id, agencia_id, conta_corrente_id " _
    '    & " FROM convenio_tb  WITH (NOLOCK)   " _
    '   & " WHERE num_convenio = '" & Convenio & "'" 'Mid(Nosso_numero, 1, 4)
   
    'Set rc = rdocn2.OpenResultset(Sql)
    'If Not rc.EOF Then
    '   banco_id = rc("banco_id")
    '   agencia_id = rc("agencia_id")
    '   conta_corrente_id = rc("conta_corrente_id")
    '   Agencia = Format(agencia_id, "0000") & "-" & calcula_dv_agencia_cc(Format(agencia_id, "0000"))
    '   Codigo_cedente = Format(conta_corrente_id, "00000000") & "-" & calcula_dv_agencia_cc(Format(conta_corrente_id, "00000000"))
    'Else
    '   banco_id = 0
    '   Agencia = "      "
    '   Codigo_cedente = "          "
    'End If
    'rc.Close
    '
    'If banco_id > 0 Then
    '   Cedente = Buscar_cedente(banco_id, agencia_id, conta_corrente_id)
    'Else
    '   Cedente = Left("BB CORRETORA SEGS ADM BENS S/A" + Space(60), 60)
    'End If
   
    '
    SQL = "SELECT mf.num_convenio, c.banco_id, c.agencia_id, c.conta_corrente_id "
    SQL = SQL & "FROM convenio_tb c  WITH (NOLOCK)  , tp_movimentacao_financ_tb mf   WITH (NOLOCK)  "
    SQL = SQL & " WHERE mf.ramo_id             =  " & ramo_id
    SQL = SQL & " AND mf.produto_id            =  " & ProdutoId
    SQL = SQL & " AND mf.tp_operacao_financ_id =  " & IIf(Val(num_cobranca) = 1, 1, 2)
    SQL = SQL & " AND c.num_convenio           =      mf.num_convenio "
'--------------------------------------------------------------------------
'  JoConceicao 08/08 SAS02-0027 - Convenio com Moeda
'
    SQL = SQL & " AND mf.moeda_id              =  " & moeda_id
'--------------------------------------------------------------------------
    Set rc = rdocn2.OpenResultset(SQL)

    If Not rc.EOF Then
        banco_id = rc("banco_id")
        agencia_id = rc("agencia_id")
        conta_corrente_id = rc("conta_corrente_id")
        agencia = Format(agencia_id, "0000") & "-" & calcula_dv_agencia_cc(Format(agencia_id, "0000"))
        Codigo_Cedente = Format(conta_corrente_id, "00000000") & "-" & calcula_dv_agencia_cc(Format(conta_corrente_id, "00000000"))
        num_convenio = Trim(rc!num_convenio)
    Else
        banco_id = 0
        agencia = Space(6)
        Codigo_Cedente = Space(10)
        '
        MensagemBatch "Banco n�o registrado para o conv�nio do produto " & Produto & ". Programa ser� cancelado.", vbCritical
        Exit Sub
    End If
    '
    rc.Close
    '
        
    'Inicio -  Marcio.Nogueira - 19368999 - Registro online convenios BB - MU-2017-027161 - 10/11/2017
    
    Local_pagto = Left("QUALQUER AG�NCIA" + Space(90), 90)

    If Produto = 123 Or Produto = 400 Then
    
        Select Case glAmbiente_id
            
            Case 2 'Produ��o AB
             
             If num_convenio = "2921623" Or num_convenio = "2904182" Then
                Local_pagto = Left("AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO.AP�S, ATUALIZE O BOLETO EM BB.COM.BR" + Space(90), 90)
             End If
            
            Case 3 'Qualidade AB
             
             If num_convenio = "2921623" Or num_convenio = "2904182" Then
                Local_pagto = Left("AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO.AP�S, ATUALIZE O BOLETO EM BB.COM.BR" + Space(90), 90)
             End If
             
            Case 6 'Produ��o ABS
            
            If num_convenio = "2921632" Or num_convenio = "2904325" Then
                Local_pagto = Left("AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO.AP�S, ATUALIZE O BOLETO EM BB.COM.BR" + Space(90), 90)
             End If
            
            Case 7 'Qualidade ABS
            
            If num_convenio = "2921632" Or num_convenio = "2904325" Then
                Local_pagto = Left("AT� O VENCIMENTO PAG�VEL EM QUALQUER BANCO.AP�S, ATUALIZE O BOLETO EM BB.COM.BR" + Space(90), 90)
             End If
        
        End Select
   
   End If
   
   'Fim -  Marcio.Nogueira - 19368999 - Registro online convenios BB - MU-2017-027161 - 10/11/2017
   
    If banco_id > 0 Then
        Cedente = Buscar_cedente(banco_id, agencia_id, conta_corrente_id)
    Else
        MensagemBatch "Cedente n�o encontrado para a proposta " & proposta & ". Programa ser� cancelado.", vbCritical
        Exit Sub
    End If
    '
    Especie_doc = "NS"
    Aceite = "N"
    dt_processamento = Format(Now, "dd-mm-yyyy")
    'comentar se mandado
    'nosso_numero_2 = Format(Nosso_Numero, "00\.000\.000\.000\-") & Nosso_numero_dv
    'flavio.abreu - Tratamento de Nosso N�mero - 2011-12-02
    If Len(Nosso_Numero) = 17 Then
       nosso_numero_2 = Format(Nosso_Numero, "00000000\.000\.000\.000")
    Else
       nosso_numero_2 = Format(Nosso_Numero, "00\.000\.000\.000\-") & Nosso_numero_dv
    End If
    Num_Conta = Space(10)
'   Carteira = "16-019"
'   Mudar a carteira de acordo com o conv�nio
    
    'comentar se mandado
    'If num_convenio = "110054" Then
    '    Carteira = "15-019"
    'Else
    '    Carteira = "16-019"
    'End If
    'flavio.abreu - Tratamento de Conv�nios - 2011-12-13/2011-12-14
     'convenio_global = Convenio ' GuilhermeCruz -- CONFITEC SISTEMAS -- 22/03/2017 -- Valida��o da Carteira ' MARCIO.NOGUEIRA - 29/11/2017
     
     convenio_global = num_convenio ' MARCIO.NOGUEIRA - 29/11/2017
       Select Case num_convenio
       Case "110054"
            Carteira = "15-019"
       Case "2253566", "2253574", "2253577"
            Carteira = "18-019"
       Case Else
            'Carteira = "16-019"
            Obtem_Carteira ' GuilhermeCruz -- CONFITEC SISTEMAS -- 22/03/2017 -- Valida��o da Carteira
       End Select

'
'    Especie = "R$  "
'   Seleciona o simbolo da moeda
    SQL = "SELECT isnull(sigla,'') sigla "
    SQL = SQL & " FROM moeda_tb  WITH (NOLOCK)   "
    SQL = SQL & " WHERE moeda_id = " & moeda_id
    '
    Set rc2 = rdocn2.OpenResultset(SQL)
    If Not rc2.EOF Then
        Especie = Left(Trim(rc2!sigla) & Space(4), 4)
        '
        rc2.Close
    Else
        rdocn.RollbackTrans
        ''
        MensagemBatch "Sigla da moeda " & moeda_id & " n�o encontrada. O programa ser� abortado.", vbCritical
        TerminaSEGBR
    End If
    
'
    Quantidade = Space(10)
    Valor_unitario = Space(10)
    '
    If Not MoedaNacional Then
        Val_Cobranca = Val_Cobranca - val_iof
    End If
    '
    If Val_Cobranca <> 0 Then
        Valor_documento = Right(Space(16) + Format(Val_Cobranca, "#,###,###,##0.00"), 16)
    Else
        Valor_documento = Space(16)
    End If
    ''Valor_documento = Right(Space(16) + Format(Val_cobranca, "#,###,###,##0.00"), 16)
'    Linha_1 = Left("***  VALORES EM REAIS ***" + Space(60), 60) ---> modificado ! Marisa.
    linha_1 = Left("***  VALORES EM " & IIf(MoedaNacional, "REAIS ", Left(Especie & Space(6), 6)) & "***" + Space(60), 60)

    '
    If val_iof <> 0 And MoedaNacional Then
        linha_2 = Left("I.O.F.: R$ " & Format(val_iof, "##,##0.00") + Space(60), 60)
    Else
        linha_2 = "I.O.F.:" + Space(53)
    End If
    '
    ''Linha_2 = Left("I.O.F.: R$ " & Format(val_iof, "##,##0.00") + Space(60), 60)
    '
    'If num_cobranca = 1 Then
    '    Linha_3 = Space(60)
    '    Linha_4 = Left("ATEN��O: " + Space(60), 60)
    '    Linha_5 = Left("ESTA PARCELA J� FOI QUITADA" + Space(60), 60)
    ''
    '
    '' Inicializa linha digitavel e cod. barras com branco
    linha_digitavel = Space(54)
    codigo_barras = Space(44)
    '
    ''Verifica se envia mensagem "Parcela J� Quitada" para a 1a. parcela
    Mensagem_Quitada = False
    '
    If num_cobranca = 1 Then
        '' Separar OuroVidaEmpresa dos Outros
        If Produto = 15 Then
            '' Essa cr�tica s� � v�lida para o produto 15 - 18/07/2000
            If Val_Cobranca <> 0 Then
                Mensagem_Quitada = True
            End If
        Else 'Demais Produtos
            SQL = "SELECT val_pgto_ato = isnull(val_pgto_ato,0) "
            SQL = SQL & " FROM proposta_fechada_tb  WITH (NOLOCK)   "
            SQL = SQL & " WHERE proposta_id = " & proposta
            '
            Set rc = rdocn2.OpenResultset(SQL)
            
            If Val(rc("val_pgto_ato")) <> 0 Then
                Mensagem_Quitada = True
            End If
        End If
    End If
    '
    If Mensagem_Quitada Then
        linha_3 = Space(60)
        linha_4 = Left("ATEN��O: " + Space(60), 60)
        linha_5 = Left("ESTA PARCELA J� FOI QUITADA" + Space(60), 60)
    Else
        linha_3 = Left("N�O RECEBER AP�S O VENCIMENTO" + Space(60), 60)
        linha_4 = Space(60)
        linha_5 = Space(60)
        '
        Montar_linha_digitavel
    End If
    'If num_cobranca <> 1 Then
    '    Montar_linha_digitavel
    'End If
    '
    ' Gera registro detalhe para pagamento
    '
    Reg = "60" & Format(ContaLinhaAtual, "000000")
    Reg = Reg & num_proposta
    Reg = Reg & linha_digitavel
    Reg = Reg & Local_pagto
    Reg = Reg & dt_agendamento
    Reg = Reg & Cedente
    Reg = Reg & Left(agencia & " / " & Codigo_Cedente & Space(20), 20)
    Reg = Reg & Format(Dt_inclusao, "dd-mm-yyyy")
    If ProdutoId = 15 Then
        'Altera��o em 10/11/2004 por M�rcio Adorno-> o campo "n�mero do documento" (60.10) estava errado.
        '   Colocava-se o "num_cobranca" no lugar no "num_endosso" e
        '   "proposta" no lugar de "Apolice_id"
        'Reg = Reg & Format(ramo_id, "00") & Format(proposta, "0000000") & Format(num_cobranca, "00000000")
        Reg = Reg & Format(ramo_id, "00") & Format(Apolice_id, "0000000") & Format(num_endosso, "00000000")
    Else
        Reg = Reg & Format(ramo_id, "00") & Format(Apolice_id, "0000000") & Format(num_cobranca, "00000000")
    End If
    Reg = Reg & Especie_doc
    Reg = Reg & Aceite
    Reg = Reg & dt_processamento
    'Ricardo Toledo : Confitec : 19/08/2010
    'Formata��o do nosso_numero_2 (para utilizar 24 posi��es)
    Reg = Reg & Left(nosso_numero_2 & Space(24), 24)
    Reg = Reg & Num_Conta
    Reg = Reg & Carteira
    Reg = Reg & Especie
    Reg = Reg & Quantidade
    Reg = Reg & Valor_unitario
    Reg = Reg & Valor_documento
    Reg = Reg & linha_1 & linha_2 & linha_3 & linha_4 & linha_5
    Reg = Reg & Sacado_1 & Sacado_2 & Sacado_3
    Reg = Reg & codigo_barras
    Reg = Reg & Produto
    
    'cristovao.rodrigues 28/10/2016 - 19368999/19370584 - cobran�a registrada AB e ABS
    Reg = Reg & Left(Cpf_Cnpj_Cedente & Space(18), 18)
    Reg = Reg & Left(Endereco_Cedente & Space(60), 60)
    Reg = Reg & Left(Bairro_Cedente & Space(30), 30)
    Reg = Reg & Left(Municipio_Cedente & Space(30), 30)
    Reg = Reg & Left(UF_Cedente & Space(2), 2)
    Reg = Reg & Left(CEP_Cedente & Space(9), 9)
    Reg = Reg & Left(Cpf_Cnpj_Sacado & Space(18), 18)

    Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
   
    ContaLinhaAtual = ContaLinhaAtual + 1
   
    ''Monta cole��o de pagamentos p/ serem atualizados no final do loop
    Monta_ColecaoPagamentos proposta, num_cobranca, num_via
    '   Atualiza_pagamento Proposta, num_cobranca, Num_via
    rc_pagamentos.MoveNext
Wend
rc_pagamentos.Close

For Each vPagamento In Pagamentos
    Atualiza_pagamento vPagamento.proposta, vPagamento.NumCobranca, vPagamento.NumVia
Next
Set Pagamentos = Nothing

Exit Sub
   
Erro:
    TrataErroGeral "Processa_Cobranca", Me.name
    TerminaSEGBR

End Sub
Private Function Obtem_Carteira()
  
Dim SQL As String
 'Dim rs As rdoResultset 'Marcio.Nogueira - 10/08/2017 - SD00115860
 Dim rsCarteira As rdoResultset 'Marcio.Nogueira - 10/08/2017 - SD00115860

  SQL = ""
  SQL = " select isnull(cod_variacao_carteira,16-019)as cod_variacao_carteira"
  SQL = SQL & " from seguros_db..convenio_tb with(nolock)   "
  SQL = SQL & " where num_convenio = " & convenio_global
  'Inc OSQL, "  AND dt_fim_participacao IS NULL"
  
  'Set rs = rdocn.OpenResultset(SQL) 'Marcio.Nogueira - 10/08/2017 - SD00115860
   Set rsCarteira = rdocn4.OpenResultset(SQL) 'Marcio.Nogueira - 10/08/2017 - SD00115860
  'Carteira = rs!cod_variacao_carteira 'Marcio.Nogueira - 10/08/2017 - SD00115860
    
    If Not rsCarteira.EOF Then
       Carteira = rsCarteira!cod_variacao_carteira 'Marcio.Nogueira - 10/08/2017 - SD00115860
    Else
       Carteira = "16-019"
    End If
  
End Function
Sub Processa_Detalhe_VidaEmpresa()
Dim i As Integer
Dim SQL As String
Dim rc As rdoResultset
Dim rc2 As rdoResultset

On Error GoTo Erro

'' Seleciona os dados fixos do plano
SQL = "SELECT tp.nome nome_plano "
SQL = SQL & ", isnull(pe.qtd_salarios, 0) qtd_salarios "
SQL = SQL & ", isnull(lim_max_sm, 0) lim_max_sm "
SQL = SQL & ", isnull (coef_custo, 0) coef_custo "
SQL = SQL & " FROM escolha_plano_tb ep  WITH (NOLOCK)  , plano_tb p  WITH (NOLOCK)   "
SQL = SQL & ", tp_plano_tb tp  WITH (NOLOCK)  , plano_empresa_tb pe  WITH (NOLOCK)   "
SQL = SQL & " WHERE ep.proposta_id = " & num_proposta
SQL = SQL & " AND ep.produto_id = " & ProdutoId
SQL = SQL & " AND ep.dt_fim_vigencia is null "
SQL = SQL & " AND p.plano_id = ep.plano_id "
SQL = SQL & " AND p.dt_inicio_vigencia = ep.dt_inicio_vigencia "
SQL = SQL & " AND p.produto_id = ep.produto_id "
SQL = SQL & " AND tp.tp_plano_id = p.tp_plano_id "
SQL = SQL & " AND pe.plano_id = p.plano_id "
SQL = SQL & " AND pe.dt_inicio_vigencia = p.dt_inicio_vigencia "
SQL = SQL & " AND pe.produto_id = p.produto_id "
'
Set rc = rdocn.OpenResultset(SQL)
'
If Not rc.EOF Then
    '' Linha em Branco - 1
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha Dupla - 2
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Msg. Anexos - 3
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(45) & "P L A N O"
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha Dupla - 4
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha em Branco - 5
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Tipo de Plano
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(5) & "TIPO DE PLANO         : " & UCase(Trim("" & rc!nome_plano))
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Seleciona as coberturas do plano
    SQL = "SELECT  distinct c.nome "
    SQL = SQL & " FROM tp_cobertura_tb c  WITH (NOLOCK)  , tp_cob_comp_tb cc  WITH (NOLOCK)  , tp_cob_comp_plano_tb ccp   WITH (NOLOCK)  "
    SQL = SQL & ", tp_cob_comp_item_tb cci  WITH (NOLOCK)  , escolha_plano_tb ep  WITH (NOLOCK)  , plano_tb p  WITH (NOLOCK)  , tp_plano_tb tp   WITH (NOLOCK)  "
    SQL = SQL & " WHERE ep.proposta_id = " & num_proposta
    SQL = SQL & " AND ep.produto_id = " & ProdutoId
    SQL = SQL & " AND ep.dt_fim_vigencia is null "
    SQL = SQL & " AND p.plano_id = ep.plano_id "
    SQL = SQL & " AND p.dt_inicio_vigencia = ep.dt_inicio_vigencia "
    SQL = SQL & " AND p.produto_id = ep.produto_id "
    SQL = SQL & " AND tp.tp_plano_id = p.tp_plano_id "
    SQL = SQL & " AND ccp.tp_plano_id = tp.tp_plano_id "
    SQL = SQL & " AND cc.tp_cob_comp_id = ccp.tp_cob_comp_id "
    SQL = SQL & " AND c.tp_cobertura_id = cc.tp_cobertura_id "
    SQL = SQL & " AND cci.tp_cob_comp_id = cc.tp_cob_comp_id "
    SQL = SQL & " AND cci.produto_id = " & ProdutoId
    SQL = SQL & " AND cci.ramo_id = " & ramo_id
    '
    Set rc2 = rdocn.OpenResultset(SQL)
     
    If Not rc2.EOF Then
        '' Posso imprimir 6 linhas no detalhe
        i = 0
        Do While Not rc2.EOF And i < 7
            ''
            If i = 0 Then
                Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
                Reg = Reg & Space(5) & "COBERTURAS PLANO      : "
                Reg = Reg & UCase(Trim(rc2!nome))
            Else
                Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
                Reg = Reg & Space(29) & UCase(Trim(rc2!nome))
            End If
            '
            Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            '
            i = i + 1
            rc2.MoveNext
        Loop
    Else
        Exit Sub
    End If
    '' Qtde. Sal�rios
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(5) & "QTDE. SAL�RIOS        : " & rc!qtd_salarios
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Limite M�x. em Sal Min.
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(5) & "LIM. M�X. EM SAL. MIN.: " & rc!lim_max_sm
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Coef. Custo
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(5) & "COEFICIENTE DE CUSTO  : " & Format(Val(rc!coef_custo), "#0.00000")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '
    CoberturasPrimPagina = True
Else
    CoberturasPrimPagina = False
    Exit Sub
End If
'
Exit Sub

Erro:
   TrataErroGeral "Processa_Detalhe_VidaEmpresa", Me.name
   TerminaSEGBR

End Sub

Sub Processa_Titulo_Clausulas()
Dim i As Integer
Dim SQL As String
Dim rc As rdoResultset

On Error GoTo Erro

SQL = "SELECT c.descr_clausula "
SQL = SQL & "FROM clausula_personalizada_tb cp  WITH (NOLOCK)  , clausula_tb c  WITH (NOLOCK)   "
SQL = SQL & "WHERE cp.proposta_id = " & num_proposta
If TpEmissao = "A" Then
    SQL = SQL & " AND (cp.endosso_id = 0 OR cp.endosso_id is null)"
Else
    SQL = SQL & " AND cp.endosso_id = " & num_endosso
End If
SQL = SQL & " AND c.cod_clausula = cp.cod_clausula_original"
'Sql = Sql & " AND c.dt_fim_vigencia is null"   'Inserida em 20082003

'lrocha - 27/12/2005
SQL = SQL & " AND c.dt_inicio_vigencia <= '" & Format(DtInicioVigencia, "yyyymmdd") & "' "
SQL = SQL & " AND (c.dt_fim_vigencia IS NULL OR c.dt_fim_vigencia >= '" & Format(DtInicioVigencia, "yyyymmdd") & "')  "

'SQL = SQL & " AND c.dt_inicio_vigencia = cp.dt_inicio_vigencia_cl_original"       'Removido em 20082003
'
Set rc = rdocn.OpenResultset(SQL)
     
If Not rc.EOF Then
    '' Linha em Branco - 1
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha Dupla - 2
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Msg. Anexos - 3
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(45) & "A N E X O S"
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha Dupla - 4
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Linha em Branco - 5
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '' Posso imprimir mais 11 linhas no detalhe
    i = 0
    Do While Not rc.EOF And i < 11
        Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta & Left(Space(15) & "- " & Trim(rc!descr_clausula) & Space(100), 100)
        Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        '
        i = i + 1
        rc.MoveNext
    Loop
    ''
    CoberturasPrimPagina = True
Else
    'For i = 1 To 17
    '    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    '    Print #Arq1, Left(Reg + Space(Tam_reg), Tam_reg)
    '    ContaLinhaAtual = ContaLinhaAtual + 1
    'Next
    CoberturasPrimPagina = False
End If
'
Exit Sub

Erro:
   TrataErroGeral "Processa_Titulo_Clausulas", Me.name
   TerminaSEGBR

End Sub

Private Function RetornaQtdCongeneres(ByVal Apolice As Long, ByVal ramo As Integer) As Long
  Dim OSQL As String
  Dim rs As rdoResultset
  
  On Error GoTo Erro
  
  OSQL = "   SELECT COUNT(*) AS Total "
  Inc OSQL, "FROM co_seguro_repassado_tb   WITH (NOLOCK)  "
  Inc OSQL, "WHERE apolice_id = " & Apolice
  Inc OSQL, " AND ramo_id = " & ramo
  Inc OSQL, " AND dt_fim_participacao IS NULL"
  
  Set rs = rdocn.OpenResultset(OSQL)
  
  RetornaQtdCongeneres = rs!Total

Exit Function

Erro:
    TrataErroGeral "RetornaQtdCongeneres", Me.name
    TerminaSEGBR

End Function

Private Sub Form_Activate()

'    On Error GoTo Erro
'
'    Call cmdOK_Click
'
'    Unload Me
'
'Exit Sub
'
'Erro:
'    TrataErroGeral "Form_Activate APV203", Me.name
'    TerminaSEGBR

End Sub

Private Sub Form_Load()

On Error GoTo Erro

Me.Caption = "Emiss�o de Ap�lices e Endossos Vida - " & Ambiente

'Call Conexao_auxiliar          'comentado em 23/09/2003

sDecimal = LeArquivoIni2("WIN.INI", "intl", "sDecimal")
If sDecimal = "." Then
   ConfiguracaoBrasil = False
Else
   ConfiguracaoBrasil = True
End If

cmdCanc.Caption = "&Sair"
cmdCanc.Refresh

Call cmdOk_Click

Call cmdCanc_Click

Exit Sub
   
Erro:
    TrataErroGeral "Form_Load APV203", Me.name
    TerminaSEGBR
   
End Sub

Function LeArquivoIni2(ByVal FileName As String, ByVal SectionName As String, ByVal Item As String) As String
Dim RetornoDefault As String, nc As String
     Dim Retorno As String * 100
     
     RetornoDefault = "*"
     nc = GetPrivateProfileString(SectionName, Item, RetornoDefault, Retorno, Len(Retorno), FileName)
     LeArquivoIni2 = Left$(Retorno, nc)

End Function

Private Sub cmdOk_Click()
    
    Dim sTpDocumentos As String

    'Tratamento para o Scheduler
    'Call InicializaParametrosExecucaoBatch(Me)
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    txtIni = Now
    MousePointer = vbHourglass
    cmdOk.Enabled = False
      
    'Obtendo caminho destino dos arquivos a serem gerados '''''''''''''''''''''''''''''''
    
    Carta_path = LerArquivoIni("relatorios", "remessa_gerado_path")
    'Carta_path = "c:\jvieira\"
    
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Texto_Clausula = ""
    cmdCanc.Caption = "&Cancelar"
    cmdCanc.Refresh
    
    'Processando Ap�lices''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    TpEmissao = "A"
    NumRegs = 0
    
    sTpDocumentos = getArquivoDocumento("APV203")
    
    'Call ProcessarSega8055Vida  -- RCA 03/06/2015
    Call ProcessarSega9201Vida
    
    'Atualizando interface'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    txtfim = Now
    MousePointer = vbDefault
      
    cmdCanc.Caption = "&Sair"
    cmdCanc.Refresh

    Call goProducao.Finaliza
   
End Sub

Private Sub cmdCanc_Click()

If UCase(cmdCanc.Caption) = "&CANCELAR" Then
   If Not goProducao.Automatico Then
      If MsgBox("Deseja realmente cancelar a gera��o do arquivo ?", vbYesNo + vbQuestion) = vbYes Then
         rdocn.RollbackTrans
         'If rdocn1.StillConnecting = True Then rdocn1.Close
         MsgBox "Programa Cancelado", vbInformation
         Call TerminaSEGBR
      End If
   End If
Else
   If rdocn1.StillConnecting = True Then rdocn1.Close
   Call TerminaSEGBR
End If

End Sub

Private Function getArquivoDocumento(arquivo_saida As String) As String
Dim SQL As String
Dim rc As rdoResultset
 
getArquivoDocumento = ""
 
SQL = " Select  tp_documento_id "
SQL = SQL & " from evento_seguros_db..documento_tb   WITH (NOLOCK)  "
SQL = SQL & " where arquivo_saida = '" & Trim(arquivo_saida) & "'"
 
Set rc = rdocn.OpenResultset(SQL)
While Not rc.EOF
   getArquivoDocumento = IIf(Trim(getArquivoDocumento) = "", rc!tp_documento_id, getArquivoDocumento & "," & rc!tp_documento_id)
   rc.MoveNext
Wend
 
End Function

Private Function Pagamento_Adimplente(iProposta_id As Long) As Boolean
'Esta fun��o verifica se o pagamento � no ato. Caso seja ela verifica tamb�m se j� foi baixado para
'   fazer o envio do Kit


Dim rc_Auxiliar As rdoResultset
Dim sSQL As String
Dim bPgtoAto As Boolean
Dim bPesquisaFechada As Boolean 'Identifica se deve pesquisa na tabela de proposta_fechada
On Error GoTo Erro

    bPesquisaFechada = False
    
    sSQL = ""
    sSQL = "select PgtoAto = case when val_pgto_ato <> 0 then 1 "
    sSQL = sSQL & "  else 0 "
    sSQL = sSQL & " End "
    sSQL = sSQL & "From proposta_adesao_tb  WITH (NOLOCK)  "
    sSQL = sSQL & "where proposta_id = " & CStr(iProposta_id)
    
    '------------------------------------------------------------------------
    ' Alterado por Stefanini em 23/09/2005 - Francisco (RNC)
    '------------------------------------------------------------------------
    '--Set rc_Auxiliar = rdocn1.OpenResultset(sSQL)
    Set rc_Auxiliar = rdocn3.OpenResultset(sSQL)
    
    If rc_Auxiliar.EOF Then
        rc_Auxiliar.Close
        bPesquisaFechada = True
    End If
    
    If bPesquisaFechada Then
        sSQL = ""
        sSQL = sSQL & "select PgtoAto = case when val_pgto_ato <> 0 then 1 "
        sSQL = sSQL & " else 0 "
        sSQL = sSQL & "End "
        sSQL = sSQL & "From proposta_fechada_tb  WITH (NOLOCK)  "
        sSQL = sSQL & "where proposta_id = " & CStr(iProposta_id)
        
        '------------------------------------------------------------------------
        ' Alterado por Stefanini em 23/09/2005 - Francisco (RNC)
        '------------------------------------------------------------------------
        '--Set rc_Auxiliar = rdocn1.OpenResultset(sSQL)
        Set rc_Auxiliar = rdocn3.OpenResultset(sSQL)
        
        If rc_Auxiliar.EOF Then
            rc_Auxiliar.Close
            Pagamento_Adimplente = True
            Exit Function
        End If
    End If
    
    If rc_Auxiliar!PgtoAto = 0 Then 'Caso n�o seja pagamento no Ato siginifica que o boleto precisa ir para o segurado
        Pagamento_Adimplente = True
    ElseIf rc_Auxiliar!PgtoAto = 1 Then 'Caso o pagamento for no ato dever� verificar a adimpl�ncia
        rc_Auxiliar.Close
        
        sSQL = ""
        sSQL = sSQL & "select case dt_baixa when null then 0 else 1 end Pago "
        sSQL = sSQL & "From agendamento_cobranca_tb  WITH (NOLOCK)  "
        sSQL = sSQL & "where proposta_id = " & CStr(iProposta_id)
        sSQL = sSQL & "      and num_cobranca = 1 "
        
        '------------------------------------------------------------------------
        '  Alterado por Stefanini em 23/09/2005 - Francisco (RNC)
        '------------------------------------------------------------------------
        '--Set rc_Auxiliar = rdocn1.OpenResultset(sSQL)
        Set rc_Auxiliar = rdocn3.OpenResultset(sSQL)
        
        'Se foi processada a Baixa o cliente receber� o Kit
        'Demanda 15740031 - Edilson Silva - 27/09/2012
        If Valida_produtoRE(ProdutoId) = True Then
            If rc_Auxiliar!Pago = 0 Then
                Pagamento_Adimplente = False
            ElseIf rc_Auxiliar!Pago = 1 Then
                Pagamento_Adimplente = True
            End If
        Else
            Pagamento_Adimplente = True
        End If
    Else
        Pagamento_Adimplente = True
    End If
    
    rc_Auxiliar.Close
    Exit Function

Erro:
    TrataErroGeral "Pagamento_Adimplente - proposta n�:" & Trim(CStr(iProposta_id)), Me.name
    TerminaSEGBR
End Function

Private Sub ProcessarSega9201Vida()

Dim TraillerArq As String
Dim vPagamento As Pagamento
Dim rs As rdoResultset
Dim rc_diretoria As rdoResultset
Dim PropostaBB As String
Dim situacao As String
Dim primeiro_loop As Boolean

Dim iContador_log As Integer
iContador_log = 1

' Demanda 4532649 - Jos� Edson - 17/02/2011 - Corre��o de erro
'' Demanda 4532649  - Jos� Edson
'Dim Pulaitem As Boolean
'------------------------------------------


On Error GoTo Erro
     
     
     
' Demanda 4532649 - Jos� Edson - 17/02/2011 - Corre��o de erro
'Sql = "segs4026_sps"
SQL = "EXEC seguros_db.dbo.segs4026_sps"  'cristovao.rodrigues 28/10/2016

rdocn1.Execute SQL

' Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
'Sql = "SELECT * FROM ##SEGS4026 WHERE PROPOSTA_ID IN (SELECT TOP 100 PROPOSTA_ID FROM ##SEGS4026 WHERE FLAG_EMITE = 'S') ORDER BY cep, produto_id "
SQL = "SELECT top 100 * FROM ##SEGS4026 WHERE FLAG_EMITE = 's' ORDER BY cep, produto_id "


Set rc_apl = rdocn1.OpenResultset(SQL)

'alterado por Leandro A. Souza - flow 189277 - 30/11/2006
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
gerar_novo_arquivo:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
     
     
'Inicializando vari�veis'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

ContaLinhaAtual = 1
tam_reg = 2000 'Alterado em 24/04/2001 para atender novo layout de arquivos
TabEscolha = ""

'Selecionando dados '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
primeiro_loop = True

MousePointer = vbHourglass

MousePointer = vbDefault

'Iniciando Transa��o''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

rdocn.BeginTrans

If Not rc_apl.EOF Then
    
    Arq1 = 0
    
    Do While Not rc_apl.EOF
        DoEvents
        
    ' Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
    '' Demanda 4532649 -------------------------------------------------------------------------------
    '        Pulaitem = False
    '        num_proposta = Format$(rc_apl!proposta_id, "000000000")
    '        num_endosso = Format$(rc_apl!endosso_id, "000000000")
    '        TpEmissao = rc_apl!tp_emissao
    '        Sql = ""
    '        Sql = "SELECT seq_clausula, cod_clausula_original "
    '        Sql = Sql & "FROM clausula_personalizada_tb  WITH (NOLOCK)    "
    '        Sql = Sql & "WHERE proposta_id = " & num_proposta
    '        If TpEmissao = "A" Then
    '           Sql = Sql & " AND (endosso_id = 0 OR endosso_id is null)"
    '        Else
    '           Sql = Sql & " AND endosso_id = " & num_endosso
    '        End If
    '        '
    '        Set rc2 = rdocn2.OpenResultset(Sql)
    '        If rc2.EOF Then
    '            'MsgBox "A proposta n� " & Val(num_proposta) & IIf(Val(num_endosso) <> 0, ", endosso n� " & Val(num_endosso), "") & " n�o possui clausulas cadastradas. Esse item n�o ser� incluido no arquivo de remessa.", vbExclamation
    '            rc_apl.MoveNext
    '            rc2.Close
    '            Pulaitem = True
    '        End If
    '
    ' '------------------------------------------------------------------------------------------------
    '
   '' Demanda 4532649 -------------------------------------------------------------------------------
    ' If Not Pulaitem Then
   ''-------------------------------------------------------------------------------------------------
        
        
        num_proposta = Format$(rc_apl!proposta_id, "000000000")
        
   ' Demanda 4532649 -------------------------------------------------------------------------------
        TpEmissao = rc_apl!tp_emissao
   ' -----------------------------------------------------------------------------------------------
   
        
        If frmSEGA9201RE.Proposta_inibida(rc_apl!proposta_id) Then  ' Verifica se a proposta deve ser inibida por ser cliente Private (Daniel Landwehrkamp)
            GoTo Continua
        End If
        
        If Not Pagamento_Adimplente(rc_apl!proposta_id) Then
            GoTo Continua
        End If
        
        'Luciana - 04/07/2003 - Verifica se h� corretor_susep - Se n�o houver, vai para o pr�ximo registro
        SQL = ""
'        Sql = Sql & " SELECT d.corretor_susep FROM corretagem_tb a  WITH (NOLOCK)   "
'        Sql = Sql & " inner join  corretor_tb d on a.corretor_id = d.corretor_id"
'        Sql = Sql & " Where a.proposta_id = " & rc_apl("proposta_id") & " And (endosso_id = 0 Or endosso_id Is Null) And D.Corretor_Susep Is Not Null"
'        Sql = Sql & " Union SELECT d.corretor_susep FROM corretagem_pj_endosso_fin_tb a  WITH (NOLOCK)  "
'        Sql = Sql & " inner join  corretor_tb d on a.corretor_id = d.corretor_id"
'        Sql = Sql & " Where a.proposta_id = " & rc_apl("proposta_id") & " And D.Corretor_Susep Is Not Null"
'        Sql = Sql & " Union SELECT d.corretor_susep FROM corretagem_tb a  WITH (NOLOCK)  "
'        Sql = Sql & " inner join  corretor_tb d on a.corretor_id = d.corretor_id"
'        Sql = Sql & " Where a.proposta_id = " & rc_apl("proposta_id") & " And a.dt_fim_corretagem Is Null And D.Corretor_Susep Is Not Null"
        'madorno - 24/11/2004 - Altera��o na query para acr�scimo do conceito de sub-grupos na ap�lice quando pesquisa o corretor
        SQL = SQL & " SELECT c.corretor_susep"
        SQL = SQL & " FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK) "
        SQL = SQL & " INNER JOIN corretor_tb c  WITH (NOLOCK)  ON  c.corretor_id = cs.corretor_id"
        SQL = SQL & " INNER JOIN apolice_tb apol  WITH (NOLOCK)  ON cs.apolice_id = apol.apolice_id"
        SQL = SQL & " WHERE apol.proposta_id = " & rc_apl("proposta_id")
        SQL = SQL & " And c.Corretor_Susep Is Not Null"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT c.corretor_susep"
        SQL = SQL & " FROM corretagem_sub_grupo_tb cs  WITH (NOLOCK) "
        SQL = SQL & " INNER JOIN corretor_tb c  WITH (NOLOCK)  ON c.corretor_id = cs.corretor_id"
        SQL = SQL & " INNER JOIN apolice_tb apol  WITH (NOLOCK)  ON cs.apolice_id = apol.apolice_id"
        SQL = SQL & " WHERE apol.proposta_id = " & rc_apl("proposta_id")
        SQL = SQL & " And c.Corretor_Susep Is Not Null"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT d.corretor_susep"
        SQL = SQL & " FROM corretagem_tb a  WITH (NOLOCK)  "
        SQL = SQL & " inner join  corretor_tb d  WITH (NOLOCK)   on a.corretor_id = d.corretor_id"
        SQL = SQL & " Where a.proposta_id = " & rc_apl("proposta_id")
        SQL = SQL & " And (endosso_id = 0 Or endosso_id Is Null)"
        SQL = SQL & " And D.Corretor_Susep Is Not Null"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT d.corretor_susep"
        SQL = SQL & " FROM corretagem_pj_endosso_fin_tb a  WITH (NOLOCK)  "
        SQL = SQL & " inner join  corretor_tb d  WITH (NOLOCK)   on a.corretor_id = d.corretor_id"
        SQL = SQL & " Where a.proposta_id = " & rc_apl("proposta_id")
        SQL = SQL & " And D.Corretor_Susep Is Not Null"
        SQL = SQL & " Union"
        SQL = SQL & " SELECT d.corretor_susep"
        SQL = SQL & " FROM corretagem_tb a  WITH (NOLOCK)  "
        SQL = SQL & " inner join  corretor_tb d  WITH (NOLOCK)   on a.corretor_id = d.corretor_id"
        SQL = SQL & " Where a.proposta_id = " & rc_apl("proposta_id")
        SQL = SQL & " And a.dt_fim_corretagem Is Null"
        SQL = SQL & " And D.Corretor_Susep Is Not Null"
        
        Set rc = rdocn.OpenResultset(SQL)
        
        If rc.EOF Or IsNull(rc("corretor_susep")) Then
            rc.Close
            GoTo Continua
        End If
        rc.Close 'RRAMOS - 2007/08/03 - Flow 288140 - Esta linha foi inclu�da.
            
        'Colhendo data de inicio de vigencia e data de emiss�o''''''''''''''''''''''''''''''''''
        If TpEmissao = "A" Then
           DtInicioVigencia = Format$(rc_apl!dt_inicio_vigencia, "dd/mm/yyyy")
           DtEmissao = rc_apl!Dt_Emissao
        Else
           DtInicioVigencia = Format$(rc_apl!dt_pedido_endosso, "dd/mm/yyyy")
           DtEmissao = rc_apl!dt_pedido_endosso
        End If
        
        Destino_id = IIf(IsNull(rc_apl!Destino), "", rc_apl!Destino)
        If UCase(Destino_id) = "D" Then
             SQL = ""
             SQL = SQL & " SELECT nome_centro_custo "
             SQL = SQL & "   FROM web_intranet_db..ips_lotacao_tb  WITH (NOLOCK)   "
             SQL = SQL & "  WHERE lotacao_id = " & rc_apl!Diretoria_id
        
             Set rc_diretoria = rdocn.OpenResultset(SQL)
             
             If Not rc_diretoria.EOF Then
                 Diretoria_id = rc_diretoria(0)
             Else
                 Diretoria_id = ""
             End If
             rc_diretoria.Close
        End If
        'Abrindo arquivos ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
        If primeiro_loop Then
            Call Abre_Arquivo
            primeiro_loop = False
        End If
        
        'Colhendo demais dados necess�rios para a gera��o dos arquivos''''''''''''''''''''''''''
        
        num_apolice = Format$(rc_apl!Apolice_id, "000000000")
        If TpEmissao = "E" Then
           num_endosso = Format$(rc_apl!endosso_id, "000000000")
        Else
           num_endosso = "000000000"
        End If
        
        ProdutoId = rc_apl!produto_id
        NomeProduto = Trim(rc_apl!nom_prod)
        processo_susep = Trim(rc_apl!num_proc_susep)
    
        num_proposta = Format$(rc_apl!proposta_id, "000000000")
        'Demanda 4532649 - Jos� Edson
        'PropostaBB = rc_apl!proposta_bb
        PropostaBB = IIf(IsNull(rc_apl!proposta_bb), 0, rc_apl!proposta_bb)
        
        PropAnt = IIf(IsNull(rc_apl!proposta_id_anterior), 0, rc_apl!proposta_id_anterior)
    
        Seguradora = rc_apl!seguradora_cod_susep
        Sucursal = rc_apl!sucursal_seguradora_id
        IniVig = Format$(rc_apl!dt_inicio_vigencia, "dd/mm/yyyy")
        FimVig = IIf(IsNull(rc_apl!dt_fim_vigencia), "", Format$(rc_apl!dt_fim_vigencia, "dd/mm/yyyy"))
        ramo_id = Val(0 & rc_apl!ramo_id)
        situacao = UCase(Trim(rc_apl!situacao))
        '' Le os dados do cliente
        nome = UCase(Left(rc_apl!nome_cli & Space(50), 50))
        
        Cpf_Cnpj_Sacado = rc_apl!Cpf_Cnpj 'cristovao.rodrigues 28/10/2016 - 19368999/19370584 - cobran�a registrada AB e ABS
        
        Endereco = UCase(Left(rc_apl!Endereco & Space(50), 50))
        Bairro = UCase(Left(rc_apl!Bairro & Space(30), 30))
        Municipio = UCase(Left(rc_apl!Municipio & Space(45), 45))
        Cep = Format(rc_apl!Cep, "00000-000")
        UF = UCase(rc_apl!Estado)
        '
        EnviaCliente = Logico(rc_apl!apolice_envia_cliente)
        If EnviaCliente Then
             QtdVias = 1
        Else
             QtdVias = Se(IsNull(rc_apl!apolice_num_vias), 1, rc_apl!apolice_num_vias)
             EnviaCongenere = Logico(rc_apl!apolice_envia_congenere)
             If EnviaCongenere Then Inc QtdVias, RetornaQtdCongeneres(CLng(num_apolice), ramo_id)
        End If
        
        'Gravando Detalhes''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
       
        '' Dados da capa da ap�lice, menos mensagem dos anexos
        
        Processa_Controle_Documento    'Tipo 10
        
        If Not flagEnderecoAgencia Then
            GoTo Continua
        End If
   
        Processa_Dados_Gerais    'Tipo 20
        
        'Processa_Coberturas     'Tipo 21 ou 22
        '' Para Vida , procurar  os t�tulos das cl�usulas
        '' constantes na ap�lice, para imprimir mensagem dos anexos
        '' Para Vida Empresa buscar as informa��es do plano
        If ProdutoId = 15 Then
            Processa_Detalhe_VidaEmpresa
            
        'Demanda 4532649 - Jos� Edson -------------
        'Else
        '    Processa_Titulo_Clausulas
        'End If
        Else
            If TpEmissao <> "E" Then
                Processa_Titulo_Clausulas
            Else
                Ler_DescricaoEndosso
            End If
        End If
        '------------------------------------------
        
       
        'Demanda 4532649 - Jos� Edson ---------------------------
        'Ler_Clausulas           'Tipo 22
        If TpEmissao <> "E" Then
            Ler_Clausulas           'Tipo 22
        End If
        '--------------------------------------------------------
        
        'MATHAYDE - 28/08/2009
        'DEMANDA: 920234 - Emitindo SEGA com m�ltiplos corretores
        Ler_Dados_Complementares 'Tipo 23 - Corretores
        
        
        Processa_Cobranca       'Tipo 60
        
        'GENJUNIOR - FLOW 18672750 - CIRCULAR 491 10/02/2016
        Processa_Dados_Subgrupo
        
        Processa_grupo_seguravel
        
        Processa_beneficiarios
        
        Processa_garantia_is_serv
        
        Processa_garantias_9201
        
        Processa_assistencias
        
        Processa_Dados_Cobranca_Subgrupo
        
        Processa_administrador_sub_grupo
        
        Processa_distribuicao_cosseguro
        
        Processa_Dados_Estipulante
        
        Processa_Valores_Coberturas
        
        'FIM GENJUNIOR
        
        
        NumRegs = NumRegs + 1
       
        '  Atualizar dt_emissao das apl emitidas'''''''''''''''''''''''''''''''''''''''''''''''''
        '  Se o produto for OuroVida Empresa, a atualizacao recebera outro tratamento.
        
        If Not IsNull(rc_apl!num_solicitacao) Then
   
            'Call Atualiza_Evento_Impressao_Temp(rc_apl!num_solicitacao, arquivo_remessa, cUserName)
            Call Atualiza_Evento_Impressao(rc_apl!num_solicitacao, arquivo_remessa, cUserName)
        
        Else
            If TpEmissao = "A" Then 'Ap�lices
                If ProdutoId = 15 Then  ' OuroVida Empresa
                    If situacao = "A" Then
                        SQL = "exec situacao_proposta_spu "
                        SQL = SQL & num_proposta
                        SQL = SQL & ", 'i'"
                        SQL = SQL & ", '" & cUserName & "'"
                        '
                        Set rs = rdocn.OpenResultset(SQL)
                        Set rs = Nothing
                        '' Atualiza tamb�m a situa��o na EMI
                        SQL = "exec situacao_propostaBB_spu "
                        SQL = SQL & PropostaBB
                        SQL = SQL & ", 'i'"
                        SQL = SQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
                        SQL = SQL & ", '" & cUserName & "'"
                        SQL = SQL & ", '01'"
                        SQL = SQL & ", 'seg409a%'"
                        '
                        Set rs = rdocn.OpenResultset(SQL)
                        Set rs = Nothing
                    End If
                Else
                    SQL = "exec atualiza_emissao_apolice_spu 'a'"
                    SQL = SQL & ", '" & Format$(Data_Sistema, "yyyymmdd") & "'"
                    SQL = SQL & ", '" & cUserName & "'"
                    SQL = SQL & ", " & num_proposta
                    SQL = SQL & ", " & num_apolice
                    '
                    Set rs = rdocn.OpenResultset(SQL)
                    Set rs = Nothing
                End If
                
                
                
                rdocn.Execute ("exec evento_seguros_db..evento_impressao_spi " & _
                       num_proposta & _
                       ", " & num_endosso & _
                       ", null " & _
                       ", null " & _
                       ", 'C'" & _
                       ", 04" & _
                       ",'i'" & _
                       ", null" & _
                       ", 0" & _
                       ", 'C'" & _
                       ", '' " & _
                       ", '' " & _
                       ", '" & cUserName & "'" & _
                       ", '" & Format(Date, "yyyymmdd") & "'" & _
                       ", '" & arquivo_remessa & "'")
            Else
                SQL = "exec atualiza_emissao_apolice_spu 'e'"
                SQL = SQL & ", '" & Format$(Data_Sistema, "yyyymmdd") & "'"
                SQL = SQL & ", '" & cUserName & "'"
                SQL = SQL & ", " & num_proposta
                SQL = SQL & ", " & num_apolice
                ' Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
                SQL = SQL & ", " & num_endosso
                
                '
                Set rs = rdocn.OpenResultset(SQL)
                Set rs = Nothing
                
                'rdocn.Execute ("exec seguros_temp_db..evento_impressao_temp_spi "
                rdocn.Execute ("exec evento_seguros_db..evento_impressao_spi " & _
                       num_proposta & _
                       ", " & num_endosso & _
                       ", null " & _
                       ", null " & _
                       ", 'C'" & _
                       ", 06" & _
                       ",'i'" & _
                       ", null" & _
                       ", 0" & _
                       ", 'E'" & _
                       ", '' " & _
                       ", '' " & _
                       ", '" & cUserName & "'" & _
                       ", '" & Format(Date, "yyyymmdd") & "'" & _
                       ", '" & arquivo_remessa & "'")
                   
                       
            End If
        End If
        primeiro_loop = False
    
    'Luciana - 04/07/2003
Continua:


        'Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
        'Sql = "UPDATE ##SEGS4026 SET FLAG_EMITE = 'N' WHERE PROPOSTA_ID = " & num_proposta & " "
        If Not IsNull(rc_apl!endosso_id) Then
            num_endosso = rc_apl!endosso_id
        End If
        SQL = "UPDATE ##SEGS4026 SET FLAG_EMITE = 'N' WHERE PROPOSTA_ID = " & Val(num_proposta) & IIf(TpEmissao = "E", " AND ENDOSSO_ID = " & Val(num_endosso), "") & " "
        
        rdocn.Execute SQL
    
        rc_apl.MoveNext
        DoEvents
   
   ' Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
   '' Demanda 4532649 -------------------------------------------------------------------------------
   ' Else
   '         If Not rc_apl.EOF Then
   '             rc_apl.MoveNext
   '         End If
   ' End If
   '' -----------------------------------------------------------------------------------------------
   
    Loop
End If

rc_apl.Close

'FLOW 714369 - JFILHO - CONFITEC - 02/02/2009 - Rotina para comitar transa��o independentemente do n�mero de ap�lices processadas.
rdocn.CommitTrans

If NumRegs > 0 Then
    'Atualizando dados em arquivo_versao_gerado_tb
        
    'FLOW 714369 - JFILHO - CONFITEC - 02/02/2009 - Movendo Commit: Rotina para comitar transa��o independentemente do n�mero de ap�lices processadas.
    'rdocn.CommitTrans
    Call Insere_Arquivo_Versao_Gerado("SEGA9201", NumRegs, ContaLinhaAtual + 1, CInt(NumRemessaApolice1))

    'Gravando Trailler  Tipo 99''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    TraillerArq = "99" & Format(Val(ContaLinhaAtual) - 1, "000000")
    TraillerArq = Left(TraillerArq + Space(tam_reg), tam_reg)
    Print #Arq1, TraillerArq
    Close #Arq1
    
Else
    If Trim(Nome_Arq1) <> "" Then
        Close #Arq1
        Kill Nome_Arq1
    End If
    
    'rdocn.RollbackTrans
    MensagemBatch "Nenhuma Ap�lice foi Processada."
End If




'Loga o n�mero de registros processados - Scheduler
    Call goProducao.AdicionaLog(1, IIf(NumRegs > 0, arquivo_remessa, "Arquivo n�o gerado"), iContador_log)
    Call goProducao.AdicionaLog(2, IIf(NumRegs > 0, ContaLinhaAtual, "0"), iContador_log)
    Call goProducao.AdicionaLog(3, IIf(NumRegs > 0, NumRegs, "0"), iContador_log)



'alterado por Leandro A. Souza - flow 189277 - 30/11/2006
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Demanda 4532649 - Jos� Edson - 17/02/2012 - Corre��o de erro
'Sql = "SELECT * FROM ##SEGS4026 WHERE PROPOSTA_ID IN (SELECT TOP 100 PROPOSTA_ID FROM ##SEGS4026 WHERE FLAG_EMITE = 'S') ORDER BY cep, produto_id "
SQL = "SELECT TOP 100 * FROM ##SEGS4026 WHERE FLAG_EMITE = 'S' ORDER BY cep, produto_id "

Set rc_apl = rdocn1.OpenResultset(SQL)
If Not rc_apl.EOF Then
    primeiro_loop = True
    TpEmissao = "A"
    NumRegs = 0
    Nome_Arq1 = ""
    iContador_log = iContador_log + 1
    GoTo gerar_novo_arquivo
End If
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


rc_apl.Close
Set rc_apl = Nothing





Exit Sub
   
Erro:
    TrataErroGeral "Processa - " & Str(Err.Number) & " - " & Err.Description & "  ", Me.name
    MousePointer = vbDefault
    TerminaSEGBR
    
End Sub
'GENJUNIOR - FLOW 18672750
Private Sub Processa_Dados_Estipulante()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " SELECT '80' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) " & vbNewLine
sql1 = sql1 & "       + RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9) " & vbNewLine
sql1 = sql1 & "       + LEFT(D.NOME + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(D.CPF_CNPJ, '') + SPACE(14), 14) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(I.NOME, '') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(E.ENDERECO + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(E.BAIRRO + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(E.MUNICIPIO + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(E.ESTADO + SPACE(2), 2) " & vbNewLine
sql1 = sql1 & "       + LEFT(SUBSTRING(E.CEP, 1, 5) + '-' + SUBSTRING(E.CEP, 6, 3) + SPACE(9), 9) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(D.DDD_1, '') + SPACE(4), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(D.TELEFONE_1, '') + SPACE(9), 9) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(D.E_MAIL, '') + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(G.NOME, '') + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(G.CPF_CNPJ, '') + SPACE(14), 14) " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB D WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON A.PROP_CLIENTE_ID = D.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.ENDERECO_CORRESP_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.PROPOSTA_ID = A.PROPOSTA_ID" & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_dB.DBO.ADMINISTRACAO_APOLICE_TB F WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = F.PROPOSTA_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.CLIENTE_TB G WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "     ON F.ADM_CLIENTE_ID = G.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.PESSOA_JURIDICA_TB H WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.CLIENTE_ID = H.PJ_CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.ATIVIDADE_TB I WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON H.ATIVIDADE_ID = I.ATIVIDADE_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & " " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_Dados_Estipulante", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_Dados_Subgrupo()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_41_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_41_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & "  SELECT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(D.NOME + SPACE(80), 80) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(G.CGC , ISNULL(F.CPF, '')) + SPACE(14), 14) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(H.NOME, ISNULL(I.NOME, '')) + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(J.ENDERECO, '') + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(J.BAIRRO, '') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(K.NOME, '') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(SUBSTRING(J.CEP, 1, 5), '') + '-' + ISNULL(SUBSTRING(J.CEP, 6, 3), '') + SPACE(9), 9) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(J.ESTADO, '') + SPACE(2), 2) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(L.E_MAIL, '') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(L.DDD_1, '') + SPACE(4), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(L.TELEFONE_1, '') + SPACE(9), 9) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(N.NOME, '') + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(N.CPF_CNPJ, '') + SPACE(14), 14) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_41_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.REPRESENTACAO_SUB_GRUPO_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.PESSOA_FISICA_TB F WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.EST_CLIENTE_ID = F.PF_CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.PESSOA_JURIDICA_TB G WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.EST_CLIENTE_ID = G.PJ_CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.PROFISSAO_TB H WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON F.PROFISSAO_CBO = H.PROFISSAO_CBO " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.ATIVIDADE_TB I WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON G.ATIVIDADE_ID = I.ATIVIDADE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.ENDERECO_CLIENTE_TB J WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.EST_CLIENTE_ID = J.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.MUNICIPIO_TB K WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON J.MUNICIPIO_ID = K.MUNICIPIO_ID " & vbNewLine
sql1 = sql1 & "     AND J.ESTADO = K.ESTADO " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.CLIENTE_TB L WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.EST_CLIENTE_ID = L.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.ADMINISTRACAO_APOLICE_TB M WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = M.PROPOSTA_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.CLIENTE_TB N WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON M.ADM_CLIENTE_ID = N.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "   " & vbNewLine

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = "   SELECT DISTINCT '41' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_41_9201 " & vbNewLine

'Dim rcAux As rdoResultset
Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_Dados_Subgrupo", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_grupo_seguravel()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_42_9201') IS NOT NULL  " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_42_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & " SELECT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(F.NOME, 'TITULAR') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(CASE WHEN D.TP_IS = 'C' THEN 'COMPULS�RIA' WHEN D.TP_IS IS NULL THEN 'FACULTATIVA' ELSE 'FACULTATIVA' END + SPACE(15), 15) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(G.LIM_MIN_INCLUSAO AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(G.LIM_MAX_INCLUSAO AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(G.LIM_MIN_MOVIMENTACAO AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(G.LIM_MAX_MOVIMENTACAO AS VARCHAR(4)), 4) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_42_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.SEGURO_VIDA_SUB_GRUPO_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID " & vbNewLine
sql1 = sql1 & "    LEFT JOIN SEGUROS_DB.DBO.TP_COMPONENTE_TB F WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON E.TP_COMPONENTE_ID = F.TP_COMPONENTE_ID " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.LIMITE_IDADE_SUB_GRUPO_APOLICE_TB G WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = G.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = G.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = G.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = G.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = G.SUB_GRUPO_ID " & vbNewLine
sql1 = sql1 & "     AND G.ENDOSSO_ID = (SELECT MAX(ENDOSSO_ID) FROM SEGUROS_DB.DBO.LIMITE_IDADE_SUB_GRUPO_APOLICE_TB WHERE APOLICE_ID = G.APOLICE_ID) " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "    AND (E.DT_FIM_VIGENCIA_SBG IS NULL OR E.DT_FIM_VIGENCIA_SBG >= GETDATE()) " & vbNewLine
sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '42' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_42_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_grupo_seguravel", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_garantia_is_serv()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_44_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_44_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & "  SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(CASE WHEN D.TP_IS = 'C' THEN 'CAPITAL INFORMADO' " & vbNewLine
sql1 = sql1 & "                   WHEN D.TP_IS = 'F' THEN 'CAPITAL FIXO' " & vbNewLine
sql1 = sql1 & "                   WHEN D.TP_IS = 'S' THEN 'M�LTIPLO SALARIAL' " & vbNewLine
sql1 = sql1 & "                   WHEN D.TP_IS = 'G' THEN 'CAPITAL GLOBAL' " & vbNewLine
sql1 = sql1 & "                   WHEN D.TP_IS = 'P' THEN 'CAPITAL POR PLANOS' " & vbNewLine
sql1 = sql1 & "                   WHEN D.TP_IS = 'E' THEN 'CAPITAL POR FAIXA ET�RIA' " & vbNewLine
sql1 = sql1 & "                   ELSE 'N�O INFORMADO' END + SPACE(30), 30)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('000000' + CAST(ISNULL(D.QTD_SALARIOS, 0) AS VARCHAR(6)), 6)  " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(E.VAL_LIM_MIN_IS AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(E.VAL_LIM_MAX_IS AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_44_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "   JOIN SEGUROS_DB.DBO.escolha_sub_grp_tp_cob_comp_tb E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '44' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_44_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_garantia_is_serv", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_garantias_9201()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro
''18672750- mcamaral modificado a ordena��o para sair igual  o segp0223.
'sql1 = ""
'sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_45_9201') IS NOT NULL " & vbNewLine
'sql1 = sql1 & "     BEGIN " & vbNewLine
'sql1 = sql1 & "         DROP TABLE ##REG_45_9201 " & vbNewLine
'sql1 = sql1 & "     END " & vbNewLine
'sql1 = sql1 & "  " & vbNewLine
''sql1 = sql1 & "  SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine - corre��o da ordena��o da cobertura conforme segp0223 20170301 -18672750 mcamaral-confitec-SP
'sql1 = sql1 & "  SELECT ROW_NUMBER() OVER (ORDER BY t.tp_componente_id ,D.SUB_GRUPO_ID asc,  isnull(e.class_tp_cobertura, 'a') desc) AS ID  " & vbNewLine
'sql1 = sql1 & "       , RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9) " & vbNewLine
'sql1 = sql1 & "       + RIGHT('00000000' + CAST(ISNULL(D.SUB_GRUPO_ID, 0) AS VARCHAR(5)), 8) " & vbNewLine
'sql1 = sql1 & "       + RIGHT('0000' + CAST(t.tp_componente_id AS VARCHAR(4)), 4) " & vbNewLine
'sql1 = sql1 & "       + RIGHT('0000' + CAST(C.tp_COBERTURA_id AS VARCHAR(4)), 4) " & vbNewLine
'sql1 = sql1 & "       + LEFT(C.NOME + SPACE(60), 60) " & vbNewLine
'sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.VAL_IS, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
'sql1 = sql1 & "       + RIGHT('0000' + CAST(ISNULL(E.CARENCIA_DIAS, 0) AS VARCHAR(4)), 4) " & vbNewLine
'sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.FAT_FRANQUIA, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
'sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.VAL_TAXA, 0) AS VARCHAR(15)), 7, 1), '*****') + SPACE(15), 15) AS REGISTRO " & vbNewLine
'sql1 = sql1 & "    INTO ##REG_45_9201 " & vbNewLine
'sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
'sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
'sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
'sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
'sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
'sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
'sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
'sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
'sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.ESCOLHA_SUB_GRP_TP_COB_COMP_TB E WITH (NOLOCK) " & vbNewLine
'sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
'sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
'sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
'sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
'sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID  " & vbNewLine
'sql1 = sql1 & "   INNER JOIN TP_COB_COMP_TB T WITH (NOLOCK)   " & vbNewLine
'sql1 = sql1 & "      ON T.TP_COB_COMP_ID = E.TP_COB_COMP_ID  " & vbNewLine
'sql1 = sql1 & "   INNER JOIN TP_COB_COMP_ITEM_TB I  WITH (NOLOCK)   " & vbNewLine
'sql1 = sql1 & "      ON I.TP_COB_COMP_ID = E.TP_COB_COMP_ID  " & vbNewLine
'sql1 = sql1 & "     AND I.DT_INICIO_VIGENCIA_COMP = E.DT_INICIO_VIGENCIA_COMP  " & vbNewLine
'sql1 = sql1 & "   INNER JOIN TP_COBERTURA_TB C WITH (NOLOCK)  " & vbNewLine
'sql1 = sql1 & "      ON C.TP_COBERTURA_ID = T.TP_COBERTURA_ID " & vbNewLine
'sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
'sql1 = sql1 & "   ORDER BY t.tp_componente_id ,D.SUB_GRUPO_ID asc,  isnull(e.class_tp_cobertura, 'a') desc" & vbNewLine
'

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_45_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_45_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine


sql1 = sql1 & " CREATE TABLE ##REG_45_9201 (  " & vbNewLine
sql1 = sql1 & " ID_NUM INT IDENTITY(1, 1) " & vbNewLine
sql1 = sql1 & " ,SUB_GRUPO_ID INT " & vbNewLine
sql1 = sql1 & " ,tp_componente_id TINYINT " & vbNewLine
sql1 = sql1 & " ,tp_cobertura_id INT  " & vbNewLine
sql1 = sql1 & " ,tp_cob_comp_id smallint " & vbNewLine
sql1 = sql1 & " ,class_tp_cobertura CHAR(1) " & vbNewLine
sql1 = sql1 & " ,REGISTRO VARCHAR(200))" & vbNewLine
sql1 = sql1 & " " & vbNewLine
sql1 = sql1 & " " & vbNewLine
sql1 = sql1 & " INSERT INTO ##REG_45_9201 (" & vbNewLine
sql1 = sql1 & " SUB_GRUPO_ID" & vbNewLine
sql1 = sql1 & " ,TP_COMPONENTE_ID" & vbNewLine
sql1 = sql1 & " ,CLASS_TP_COBERTURA" & vbNewLine
sql1 = sql1 & " ,tp_cobertura_id  " & vbNewLine
sql1 = sql1 & " ,tp_cob_comp_id " & vbNewLine
sql1 = sql1 & " ,REGISTRO)" & vbNewLine


sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & "  SELECT DISTINCT E.SUB_GRUPO_ID , t.tp_componente_id,   isnull(e.class_tp_cobertura, 'a')class_tp_cobertura, c.tp_cobertura_id, t.tp_cob_comp_id" & vbNewLine
sql1 = sql1 & "       , RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9) " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(ISNULL(E.SUB_GRUPO_ID, 0) AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(t.tp_componente_id AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(C.tp_COBERTURA_id AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(C.NOME + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.VAL_IS, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(ISNULL(E.CARENCIA_DIAS, 0) AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.FAT_FRANQUIA, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.VAL_TAXA, 0) AS VARCHAR(15)), 7, 1), '*****') + SPACE(15), 15) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    FROM ESCOLHA_SUB_GRP_TP_COB_COMP_TB E WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN TP_COB_COMP_TB T WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON T.TP_COB_COMP_ID = E.TP_COB_COMP_ID " & vbNewLine
sql1 = sql1 & "   INNER JOIN TP_COB_COMP_ITEM_TB I WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON I.TP_COB_COMP_ID = E.TP_COB_COMP_ID " & vbNewLine
sql1 = sql1 & "    AND I.DT_INICIO_VIGENCIA_COMP = E.DT_INICIO_VIGENCIA_COMP " & vbNewLine
sql1 = sql1 & "     INNER JOIN TP_COBERTURA_TB C WITH (NOLOCK)" & vbNewLine
sql1 = sql1 & "     ON C.TP_COBERTURA_ID = T.TP_COBERTURA_ID  " & vbNewLine
sql1 = sql1 & "    INNER JOIN APOLICE_TB AP WITH (NOLOCK)" & vbNewLine
sql1 = sql1 & "      ON E.APOLICE_ID = AP.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND E.RAMO_ID = AP.RAMO_ID " & vbNewLine
sql1 = sql1 & "    INNER JOIN SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "     ON A.PROPOSTA_ID = AP.PROPOSTA_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "   ORDER BY E.SUB_GRUPO_ID ,T.TP_COMPONENTE_ID,ISNULL(E.CLASS_TP_COBERTURA, 'a') DESC" & vbNewLine


Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

'sql1 = " SELECT DISTINCT '45' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
'sql1 = sql1 & "   FROM ##REG_45_9201 " & vbNewLine - corre��o da ordena��o da cobertura conforme segp0223 20170301 -18672750 mcamaral-confitec-SP

sql1 = " SELECT '45' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_45_9201 " & vbNewLine
sql1 = sql1 & " ORDER BY ID_NUM " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_garantias_9201", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_assistencias()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_46_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_46_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & " SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(G.TXT_TIPO_ASSISTENCIA + SPACE(60), 60) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_46_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP   " & vbNewLine
sql1 = sql1 & "    JOIN ASSISTENCIA_DB..SUB_GRUPO_ASSISTENCIA_ATUAL_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID  " & vbNewLine
sql1 = sql1 & "    JOIN ASSISTENCIA_DB..PLANO_ASSISTENCIA_TB F WITH (NOLOCK)   " & vbNewLine
sql1 = sql1 & "      ON F.PLANO_ASSISTENCIA_ID = E.PLANO_ASSISTENCIA_ID   " & vbNewLine
sql1 = sql1 & "     AND F.DT_INICIO_VIGENCIA   = E.DT_INICIO_VIGENCIA   " & vbNewLine
sql1 = sql1 & "    JOIN ASSISTENCIA_DB..TIPO_ASSISTENCIA_TB G WITH (NOLOCK)   " & vbNewLine
sql1 = sql1 & "      ON G.TIPO_ASSISTENCIA_ID = F.TIPO_ASSISTENCIA_ID  " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '46' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_46_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_assistencias", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR


'GENJUNIOR - FLOW 18672750
Private Sub Processa_Dados_Cobranca_Subgrupo()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_47_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_47_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & " SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(C.NOME + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(D.DIA_FATURAMENTO AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(D.DIA_COBRANCA AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(E.NOME, '') + SPACE(30), 30) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(G.VAL_COBRANCA, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(G.VAL_IOF, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST((ISNULL(G.VAL_COBRANCA, 0) + ISNULL(G.VAL_IOF, 0)) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15)  " & vbNewLine
sql1 = sql1 & "       + LEFT(CASE WHEN D.TP_CUSTEIO = 'C' THEN 'CONTRIBUT�RIO' ELSE 'N�O CONTRIBUT�RIO' END + SPACE(25), 25)  " & vbNewLine
'segp9201-18672750 inclus�o dados bancarios confitec-SP mcamaral
sql1 = sql1 & "       + RIGHT('00000' + CAST(isnull(d.banco_id,0) AS VARCHAR(5)), 5) --Codigo Banco " & vbNewLine
sql1 = sql1 & "       + LEFT(isnull(ag.nome,' ') + SPACE(60), 60)" & vbNewLine
sql1 = sql1 & "       + RIGHT('00000' + CAST(isnull(isnull(d.agencia_id,g.deb_agencia_id),0) AS VARCHAR(5)), 5) --Codigo Agencia " & vbNewLine
sql1 = sql1 & "       + RIGHT('000000000' + CAST(isnull(isnull(d.conta_corrente_id,g.deb_conta_corrente_id),0) AS VARCHAR(9)), 9) AS REGISTRO --Codigo Conta Corrente" & vbNewLine
'segp9201-18672750 inclus�o dados bancarios confitec-SP mcamaral
sql1 = sql1 & "    INTO ##REG_47_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.PERIODO_PGTO_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.PERIODO_PGTO_ID = E.PERIODO_PGTO_ID " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.FORMA_PGTO_TB C WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.FORMA_PGTO_ID = C.FORMA_PGTO_ID " & vbNewLine
'segp9201-18672750 inclus�o dados bancarios confitec-SP mcamaral
sql1 = sql1 & "    LEFT JOIN AGENCIA_TB ag WITH (NOLOCK)" & vbNewLine
sql1 = sql1 & "      ON AG.BANCO_ID = D.BANCO_ID " & vbNewLine
sql1 = sql1 & "     AND AG.AGENCIA_ID = D.AGENCIA_ID " & vbNewLine
'segp9201-18672750 inclus�o dados bancarios confitec-SP mcamaral

If ProdutoId = 150 Then

    sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB G WITH (NOLOCK) " & vbNewLine
    sql1 = sql1 & "      ON D.APOLICE_ID = G.APOLICE_ID " & vbNewLine
    sql1 = sql1 & "     AND D.RAMO_ID = G.RAMO_ID " & vbNewLine
    sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = G.SUCURSAL_SEGURADORA_ID " & vbNewLine
    sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = G.SEGURADORA_COD_SUSEP " & vbNewLine


Else
    sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.FATURA_TB F WITH (NOLOCK) " & vbNewLine
    sql1 = sql1 & "      ON D.APOLICE_ID = F.APOLICE_ID " & vbNewLine
    sql1 = sql1 & "     AND D.RAMO_ID = F.RAMO_ID " & vbNewLine
    sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = F.SUCURSAL_SEGURADORA_ID " & vbNewLine
    sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = F.SEGURADORA_COD_SUSEP " & vbNewLine
    sql1 = sql1 & "     AND D.SUB_GRUPO_ID = F.SUB_GRUPO_ID " & vbNewLine
    sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_ATUAL_TB G WITH (NOLOCK) " & vbNewLine
    sql1 = sql1 & "      ON F.APOLICE_ID = G.APOLICE_ID " & vbNewLine
    sql1 = sql1 & "     AND F.RAMO_ID = G.RAMO_ID " & vbNewLine
    sql1 = sql1 & "     AND F.SUCURSAL_SEGURADORA_ID = G.SUCURSAL_SEGURADORA_ID " & vbNewLine
    sql1 = sql1 & "     AND F.SEGURADORA_COD_SUSEP = G.SEGURADORA_COD_SUSEP " & vbNewLine
    sql1 = sql1 & "     AND F.FATURA_ID = G.FATURA_ID " & vbNewLine
End If

sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "     AND G.num_cobranca = 1 " & vbNewLine


Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '47' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_47_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_Dados_Cobranca_Subgrupo", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR


'GENJUNIOR - FLOW 18672750
Private Sub Processa_administrador_sub_grupo()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_48_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_48_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & " SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + LEFT(F.NOME + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(F.CPF_CNPJ,'') + SPACE(14), 14) " & vbNewLine
sql1 = sql1 & "       + LEFT(ISNULL(DBO.FORMATA_NUMERO_FN(CAST(ISNULL(E.PERC_PRO_LABORE, 0) AS VARCHAR(15)), 2, 1), '*****') + SPACE(15), 15) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_48_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP   " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.ADM_SUB_GRUPO_APOLICE_TB E WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = E.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = E.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = E.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = E.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = E.SUB_GRUPO_ID  " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.CLIENTE_TB F WITH (NOLOCK)   " & vbNewLine
sql1 = sql1 & "      ON E.ADM_CLIENTE_ID = F.CLIENTE_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '48' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_48_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_administrador_sub_grupo", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR

'GENJUNIOR - FLOW 18672750
Private Sub Processa_beneficiarios()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_43_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "     BEGIN " & vbNewLine
sql1 = sql1 & "         DROP TABLE ##REG_43_9201 " & vbNewLine
sql1 = sql1 & "     END " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & "  SELECT DISTINCT RIGHT('000000000' + CAST(A.PROPOSTA_ID AS VARCHAR(20)), 9)  " & vbNewLine
sql1 = sql1 & "       + RIGHT('00000000' + CAST(D.SUB_GRUPO_ID AS VARCHAR(5)), 8) " & vbNewLine
sql1 = sql1 & "       + RIGHT('0000' + CAST(SV.TP_COMPONENTE_ID AS VARCHAR(4)), 4) " & vbNewLine
sql1 = sql1 & "       + LEFT(CASE WHEN ISNULL(TEMP.TEXTO_BENEFICIARIO,'') = '' THEN REPLACE(ISNULL(SV.TEXTO_BENEFICIARIO, ''), CHAR(13), ' ') " & vbNewLine
sql1 = sql1 & "             ELSE REPLACE(ISNULL(TEMP.TEXTO_BENEFICIARIO, ''), CHAR(13), ' ')  " & vbNewLine
sql1 = sql1 & "        END + SPACE(80), 80) AS REGISTRO " & vbNewLine
sql1 = sql1 & "    INTO ##REG_43_9201 " & vbNewLine
sql1 = sql1 & "    FROM SEGUROS_DB.DBO.PROPOSTA_TB A WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.APOLICE_TB B WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON A.PROPOSTA_ID = B.PROPOSTA_ID  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB D WITH (NOLOCK)  " & vbNewLine
sql1 = sql1 & "      ON B.APOLICE_ID = D.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND B.RAMO_ID = D.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND B.SUCURSAL_SEGURADORA_ID = D.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND B.SEGURADORA_COD_SUSEP = D.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "   INNER JOIN SEGUROS_DB.DBO.ESCOLHA_SUB_GRP_TP_COB_COMP_TB ES WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "      ON D.APOLICE_ID = ES.APOLICE_ID " & vbNewLine
sql1 = sql1 & "     AND D.RAMO_ID = ES.RAMO_ID " & vbNewLine
sql1 = sql1 & "     AND D.SUCURSAL_SEGURADORA_ID = ES.SUCURSAL_SEGURADORA_ID " & vbNewLine
sql1 = sql1 & "     AND D.SEGURADORA_COD_SUSEP = ES.SEGURADORA_COD_SUSEP  " & vbNewLine
sql1 = sql1 & "     AND D.SUB_GRUPO_ID = ES.SUB_GRUPO_ID " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.TP_COB_COMP_TB CC  (NOLOCK)        " & vbNewLine
sql1 = sql1 & "      ON ES.TP_COB_COMP_ID = CC.TP_COB_COMP_ID        " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB..TP_COBERTURA_TB CB  (NOLOCK)        " & vbNewLine
sql1 = sql1 & "      ON CC.TP_COBERTURA_ID = CB.TP_COBERTURA_ID         " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.SEGURO_VIDA_SUB_GRUPO_TB SV (NOLOCK)    " & vbNewLine
sql1 = sql1 & "      ON ES.APOLICE_ID = SV.APOLICE_ID       " & vbNewLine
sql1 = sql1 & "     AND ES.RAMO_ID = SV.RAMO_ID       " & vbNewLine
sql1 = sql1 & "     AND SV.SUB_GRUPO_ID = ES.SUB_GRUPO_ID       " & vbNewLine
sql1 = sql1 & "     AND SV.SUCURSAL_SEGURADORA_ID = ES.SUCURSAL_SEGURADORA_ID       " & vbNewLine
sql1 = sql1 & "     AND SV.SEGURADORA_COD_SUSEP = ES.SEGURADORA_COD_SUSEP       " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB.DBO.SUB_GRUPO_APOLICE_TB SG (NOLOCK)        " & vbNewLine
sql1 = sql1 & "      ON SG.APOLICE_ID = SV.APOLICE_ID       " & vbNewLine
sql1 = sql1 & "     AND SG.RAMO_ID = SV.RAMO_ID       " & vbNewLine
sql1 = sql1 & "     AND SG.SUB_GRUPO_ID = SV.SUB_GRUPO_ID       " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB..PESSOA_FISICA_TB CL       " & vbNewLine
sql1 = sql1 & "      ON SV.PROP_CLIENTE_ID = CL.PF_CLIENTE_ID       " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB..CLIENTE_TB CT       " & vbNewLine
sql1 = sql1 & "      ON CL.PF_CLIENTE_ID = CT.CLIENTE_ID       " & vbNewLine
sql1 = sql1 & "    JOIN SEGUROS_DB..SEGURADO_TEMPORARIO_TB TEMP " & vbNewLine
sql1 = sql1 & "      ON TEMP.APOLICE_ID = ES.APOLICE_ID       " & vbNewLine
sql1 = sql1 & "     AND TEMP.RAMO_ID = ES.RAMO_ID       " & vbNewLine
sql1 = sql1 & "     AND TEMP.SUB_GRUPO_ID = ES.SUB_GRUPO_ID       " & vbNewLine
sql1 = sql1 & "     AND TEMP.SUCURSAL_SEGURADORA_ID = ES.SUCURSAL_SEGURADORA_ID       " & vbNewLine
sql1 = sql1 & "     AND TEMP.PROP_CLIENTE_ID =  SV.PROP_CLIENTE_ID " & vbNewLine
sql1 = sql1 & "   WHERE A.PROPOSTA_ID = " & num_proposta & vbNewLine
sql1 = sql1 & "    " & vbNewLine
'sql1 = sql1 & "  ORDER BY A.PROPOSTA_ID " & vbNewLine
'sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing


sql1 = "  SELECT DISTINCT '43' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_43_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_beneficiarios", Me.name
    TerminaSEGBR
    
End Sub

'GENJUNIOR - FLOW 18672750
Private Sub Processa_distribuicao_cosseguro()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " IF OBJECT_ID('TEMPDB..##REG_49_9201') IS NOT NULL " & vbNewLine
sql1 = sql1 & "      BEGIN  " & vbNewLine
sql1 = sql1 & "          DROP TABLE ##REG_49_9201  " & vbNewLine
sql1 = sql1 & "      END  " & vbNewLine
sql1 = sql1 & "  " & vbNewLine
sql1 = sql1 & " SELECT DISTINCT RIGHT('000000000' + CAST(a.proposta_id AS VARCHAR(20)), 9)   " & vbNewLine
sql1 = sql1 & "      + LEFT(ISNULL(d.nome, '*****') + SPACE(60), 60)  " & vbNewLine
sql1 = sql1 & "      + LEFT(ISNULL(d.cgc, '*****') + SPACE(14), 14) " & vbNewLine
sql1 = sql1 & "      + RIGHT('00000' + CAST(d.seguradora_cod_susep AS VARCHAR(10)), 5) " & vbNewLine
sql1 = sql1 & "      + RIGHT('000000000' + dbo.formata_numero_fn(c.perc_participacao, 2, 1), 9) AS registro " & vbNewLine
sql1 = sql1 & "   INTO ##reg_49_9201  " & vbNewLine
sql1 = sql1 & "   FROM seguros_db.dbo.proposta_tb a WITH (NOLOCK)    " & vbNewLine
sql1 = sql1 & "  INNER JOIN seguros_db.dbo.apolice_tb b WITH (NOLOCK)    " & vbNewLine
sql1 = sql1 & "     ON a.proposta_id = b.proposta_id    " & vbNewLine
sql1 = sql1 & "   LEFT JOIN seguros_db.dbo.co_seguro_repassado_tb c WITH (NOLOCK)    " & vbNewLine
sql1 = sql1 & "     ON b.apolice_id = c.apolice_id   " & vbNewLine
sql1 = sql1 & "    AND b.ramo_id = c.ramo_id   " & vbNewLine
sql1 = sql1 & "    AND b.sucursal_seguradora_id = c.sucursal_seguradora_id   " & vbNewLine
sql1 = sql1 & "    AND b.seguradora_cod_susep = c.seguradora_cod_susep     " & vbNewLine
sql1 = sql1 & "   LEFT JOIN seguros_db.dbo.seguradora_tb d WITH (NOLOCK)   " & vbNewLine
sql1 = sql1 & "     ON c.seguradora_cod_susep = d.seguradora_cod_susep    " & vbNewLine
sql1 = sql1 & "  WHERE a.proposta_id =  " & num_proposta & vbNewLine
sql1 = sql1 & "    AND c.dt_inicio_participacao <= '" & Format(Data_Sistema, "yyyymmdd") & "'" & vbNewLine
sql1 = sql1 & "    AND c.dt_fim_participacao IS NULL " & vbNewLine
sql1 = sql1 & "  " & vbNewLine

Set rc = Nothing

rdocn.Execute (sql1)

Set rc = Nothing

sql1 = " SELECT DISTINCT '49' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) + REGISTRO " & vbNewLine
sql1 = sql1 & "   FROM ##REG_49_9201 " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Processa_distribuicao_cosseguro", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR
'GENJUNIOR - FLOW 18672750
Private Sub Processa_Valores_Coberturas()

Dim DtFim As String, Obs As String
Dim var_corretor_susep As String
    
On Error GoTo Erro

sql1 = ""
sql1 = sql1 & " SELECT '81' + RIGHT('000000' + '" & CStr(ContaLinhaAtual) & "', 6) " & vbNewLine
sql1 = sql1 & "      + RIGHT('000000000' + CAST(B.PROPOSTA_ID AS VARCHAR(20)), 9) " & vbNewLine
sql1 = sql1 & "      + LEFT(CAST(B.tp_cobertura_id AS VARCHAR(10)) + SPACE(4), 4) " & vbNewLine
sql1 = sql1 & "      + LEFT(c.nome + SPACE(60), 60) " & vbNewLine
sql1 = sql1 & "      + LEFT(CAST(B.val_is AS VARCHAR(20)) + SPACE(15), 15) " & vbNewLine
sql1 = sql1 & "      + LEFT(CAST(B.val_premio_tarifa_sem_custo_assist_facul AS VARCHAR(20)) + SPACE(15), 15) " & vbNewLine
sql1 = sql1 & "   FROM seguros_db.dbo.cobert_obj_risco_tb b WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "  INNER JOIN seguros_db.dbo.tp_cobertura_tb c WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "     ON b.tp_cobertura_id = c.tp_cobertura_id " & vbNewLine
sql1 = sql1 & "  WHERE b.tp_componente_id = 1 " & vbNewLine
sql1 = sql1 & "    AND b.proposta_id = " & num_proposta & vbNewLine
sql1 = sql1 & "    AND ISNULL(b.endosso_id, 0) = (SELECT ISNULL(MAX(endosso_id ), 0) " & vbNewLine
sql1 = sql1 & "                                     FROM seguros_db.dbo.cobert_obj_risco_tb cobert WITH (NOLOCK) " & vbNewLine
sql1 = sql1 & "                                    WHERE cobert.proposta_id = b.proposta_id) " & vbNewLine
sql1 = sql1 & "    AND b.val_premio_tarifa_sem_custo_assist_facul IS NOT NULL " & vbNewLine

Set rc = rdocn.OpenResultset(sql1)

If Not rc.EOF Then
    Do While Not rc.EOF
    
        Reg = rc(0)
    
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
        
        rc.MoveNext
    Loop
End If

rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Ler_Dados_Complementares", Me.name
    TerminaSEGBR
    
End Sub
'FIM GENJUNIOR
Private Sub IncrementaLinha()

' a vari�vel CONTA_LINHA era incrementada como x = x + 1; devido a solicita��o de separar
' a emiss�o de ap�lice em dois arquivos distintos, optou-se por transformar esta opera��o
' em uma rotina (Jo�o Mac-Cormick - 30/6/2000)

  If EnviaCliente Then
    Inc ContaLinha1
  Else
    Inc ContaLinha2
  End If
  
End Sub

Private Sub IncrementaRegistro()

' Devido a solicita��o de separar a emiss�o de ap�lice em dois arquivos
' distintos, optou-se por transformar esta opera��o em uma rotina

  If EnviaCliente Then
    Inc QtdReg1
  Else
    Inc QtdReg2
  End If
  
End Sub

Sub Abre_Arquivo()

'Abrindo o arquivos''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

On Error GoTo Erro
   
Dim HeaderCliente As String, DataHora As String

DataHora = Trim(Format(CDate(Data_Sistema), "dd/mm/yyyy hh:mm AMPM"))
    
Call Obtem_Num_Remessa("SEGA9201", NumRemessaApolice1)
'Carta_path = App.PATH & "\"
'Carta_path = "C:\Camila\"

Nome_Arq1 = Carta_path & "SEGA9201" & "." & Format(NumRemessaApolice1, "0000")


If Trim(Nome_Arq1) <> "" Then
    If Dir(Nome_Arq1) <> "" Then
       If Not goProducao.Automatico Then
          If MsgBox("J� existe um arquivo de " & IIf(TpEmissao = "A", "Ap�lice", "Endosso") & " gerado com a vers�o " & Format(NumRemessaApolice1, "0000") & ". Continuar ?", vbQuestion + vbYesNo) = vbNo Then
             MsgBox "Processo cancelado.", vbCritical
             rdocn.RollbackTrans
             TerminaSEGBR
          End If
       Else
          MensagemBatch "J� existe um arquivo de " & IIf(TpEmissao = "A", "Ap�lice", "Endosso") & " gerado com a vers�o " & Format(NumRemessaApolice1, "0000")
          rdocn.RollbackTrans
          TerminaSEGBR
       End If
    End If
End If

HeaderCliente = "01" & Left("SEGA9201" & Space(8), 8) & NumRemessaApolice1 & _
                Left(DataHora & Space(10), 10)
HeaderCliente = Left(HeaderCliente + Space(tam_reg), tam_reg)

Arq1 = FreeFile
Open Nome_Arq1 For Output As Arq1
Print #Arq1, HeaderCliente   'Grava o header do arquivo do cliente.

'jessica.adao - Confitec Sistemas - 21/11/2012 - INC000003815607 / INC000003808494 : Sem gera��o no SEGBR (Gravar remessa, independente do TpEmissao)
'If TpEmissao = "A" Then
If Trim(Nome_Arq1) <> "" Then
    txtArq(0).Text = "SEGA9201" & "." & Format(NumRemessaApolice1, "0000")
    txtArq(0).Refresh
    arquivo_remessa = "SEGA9201" & "." & Format(NumRemessaApolice1, "0000")
End If

Exit Sub
    
Erro:
    TrataErroGeral "Abre_Arquivo", Me.name
    TerminaSEGBR

End Sub

'
'
Private Sub Processa_Coberturas1()
Dim RegClausula As Integer, i As Long, j As Long, linhaFranquia As String, ObjAnteriorBenef As Long
Dim ObjAnterior As Long, Endereco As String, linhasCobertura As Long, PriVez As String

On Error GoTo Erro
If QtdLinhasCobertura > 14 Then
   Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & Space(15) & String(38, "*")
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   
   Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & Space(15) & "COBERTURAS CONTRATADAS CONFORME ANEXO"
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   
   Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & Space(15) & String(38, "*")
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   CoberturasPrimPagina = False
   RegClausula = 22
   linhasCobertura = 3
ElseIf QtdLinhasCobertura = 0 Then
   CoberturasPrimPagina = False
   RegClausula = 21
   linhasCobertura = 0
ElseIf QtdLinhasCobertura <= 14 Then
   CoberturasPrimPagina = True
   RegClausula = 21
   linhasCobertura = QtdCoberturas
End If

If QtdCoberturas <> 0 Then
   ObjAnterior = 0: ObjAnteriorBenef = 0
   For i = 0 To QtdCoberturas - 1
      If Cobertura(0, i) <> ObjAnterior Then
         If i > 0 Then
            'Monta linhas de Benefici�rios do obj. segurado anterior
            Lista_Beneficiarios RegClausula, ObjAnterior
            
            'Pula uma linha para cada novo obj. segurado
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
            Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            linhasCobertura = linhasCobertura + 1
         End If
         
         'Atualiza obj Anterior
         ObjAnterior = Cobertura(0, i)

         'T�tulo do item
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & "ITEM " & Format$(Cobertura(0, i), "00") & ":" & Space(8)
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
         'Local do Risco
         For j = 0 To QtdObjetos - 1
            If EnderecoRisco(0, j) = Cobertura(0, i) Then
               Endereco = EnderecoRisco(1, j)
            End If
         Next
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & "LOCAL DO RISCO: " & Endereco
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
          'T�tulo Coberturas
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA                                                I.S(" & MoedaSeguro & ")"
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
      End If
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & Format$(Cobertura(1, i), "000") & Space(8)
      'Reg = Reg & Left(Cobertura(2, i) & Space(200), 200)       '200 caracteres para a descri��o sem UCase 27/10/2003
      Reg = Reg & Left(Cobertura(2, i) & Space(72), 72)       '72 caracteres para a descri��o sem UCase 19/10/2004
                  
      'Imp Segurada
      If ConfiguracaoBrasil Then
          Reg = Reg & MoedaSeguro & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
      Else
          Reg = Reg & MoedaSeguro & Right(Space(16) & TrocaValorAmePorBras(Format(Cobertura(3, i), "#,###,###,##0.00")), 16)
      End If
      Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
      
      'Franquia
      linhaFranquia = ""
      If Cobertura(4, i) <> 0 Or Cobertura(5, i) <> "" Or Cobertura(6, i) <> 0 Then
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & "FRANQUIA : "
         If Cobertura(4, i) <> 0 Then
            If ConfiguracaoBrasil Then
                linhaFranquia = linhaFranquia & Right(Space(8) & Format(Cobertura(4, i), "##0.00") & " %", 8)
            Else
                linhaFranquia = linhaFranquia & Right(Space(8) & TrocaValorAmePorBras(Format(Cobertura(4, i), "##0.00")) & " %", 8)
            End If
         End If
         If Cobertura(5, i) <> "" Then
            'Colocar separador caso o anterior estiver preenchido
            If linhaFranquia <> "" Then
               linhaFranquia = linhaFranquia & " - "
            End If
            linhaFranquia = linhaFranquia & Trim(Cobertura(5, i))
         End If
         If Cobertura(6, i) <> 0 Then
            'Colocar separador caso o anterior estiver preenchido
            If linhaFranquia <> "" Then
               linhaFranquia = linhaFranquia & " - M�nimo de: R$ "
            End If
            If ConfiguracaoBrasil Then
               linhaFranquia = linhaFranquia & Format(Cobertura(6, i), "#,###,###,##0.00")
            Else
               linhaFranquia = linhaFranquia & TrocaValorAmePorBras(Format(Cobertura(6, i), "#,###,###,##0.00"))
            End If
        End If
         Reg = Reg & linhaFranquia
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
      End If
   Next
   
   'Monta linhas de Benefici�rios do �ltimo obj. segurado
   Lista_Beneficiarios RegClausula, ObjAnterior
   
   'Pulando uma linha...
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & String(16, " ")
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
Else
   'Listar objetos segurados e seus benefici�rios (caso existam)

End If

'Total Descontos
'If ValTotDesconto <> 0 Then
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Reg = Reg & "Total de descontos : " & Left(MoedaPremio + Format(ValTotDesconto, "#,###,###,##0.00") & Space(16), 16)
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   QtdLinhasCobertura = QtdLinhasCobertura + 1
'   ContaLinhaAtual = ContaLinhaAtual + 1
'   'pular linha
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   QtdLinhasCobertura = QtdLinhasCobertura + 1
'   ContaLinhaAtual = ContaLinhaAtual + 1
'End If
        
'If TpEmissao = "A" Then 'Somente se ap�lice
'   'Limite Max Responsabilidade da Ap�lice
'   If linhasCobertura < 14 Then
'      For i = 1 To (14 - linhasCobertura)
'         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'         Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'         ContaLinhaAtual = ContaLinhaAtual + 1
'         linhasCobertura = 14
'      Next
'   End If
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Reg = Reg & "LIMITE MAXIMO DE REPONSABILIDADE DA APOLICE : " & Left(MoedaPremio + Format(TotIS, "#,###,###,##0.00") & Space(16), 16)
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   ContaLinhaAtual = ContaLinhaAtual + 1
'End If
    
Exit Sub

Erro:
   TrataErroGeral "Processa_Coberturas1", Me.name
   TerminaSEGBR

End Sub


Private Sub Ler_Clausulas()
Dim Clausula        As String, QTD As Long
Dim linha As Integer, ultQuebra As Long, Ultpos As Long, i As Long
Dim aux As String, tamFinal As Long
Dim cont_clau       As Integer
Dim rc2             As rdoResultset
Dim ind_corretor    As Integer

'Lucaina - 04/07/2003
Dim var_corretor_susep As String

On Error GoTo Erro

'  Se houver mais de um corretor para uma mesma proposta, os codigos e
'  nomes serao impressos aqui, como anexos. (Marisa).

If Varios_corretores Then
'
'  Imprime linhas de cabecalho dos corretores.
'  Linha em branco - 1
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    
    ContaLinhaAtual = ContaLinhaAtual + 1
'' Linha Dupla - 2
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
'' Msg. Corretores - 3
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(40) & "C O R R E T O R E S"
    
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
'' Linha Dupla - 4
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(15) & String(70, "=")
    
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
'' Linha em Branco - 5
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Print #Arq1, Left(Reg & Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
'
'  Descarrega dados de corretores.
'
    For ind_corretor = 1 To qtd_corretores
        Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(10)
        
        'Luciana - 04/07/2003 - Altera��o no layout do corretor_id
        'Reg = Reg & Left(Format$(corretor_id(ind_corretor), "000000-000") & Space(10), 10)
        var_corretor_susep = Space(15 - Len(corretor_id(ind_corretor))) & corretor_id(ind_corretor)
        Reg = Reg & Mid(var_corretor_susep, 1, 3) & "." & _
                    Mid(var_corretor_susep, 4, 2) & "." & _
                    Mid(var_corretor_susep, 6, 2) & "." & _
                    Mid(var_corretor_susep, 8, 1) & "." & _
                    Mid(var_corretor_susep, 9, 6) & "." & _
                    Mid(var_corretor_susep, 15, 1)
                
        Reg = Reg & Space(2)
        Reg = Reg & Left(nome_corretor(ind_corretor) & Space(60), 60)
        
        'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Reg = Replace(Reg, vbCr, vbNullString)
        Reg = Replace(Reg, vbLf, vbNullString)
        Reg = Replace(Reg, vbCrLf, vbNullString)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
    Next ind_corretor
'
'  For�a salto de pagina ao final da impressao dos corretores.
'
    Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(100) & "S"
    
    'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
'
End If
'

'' Seleciona primeiro as clausulas, para depois selecionar os textos um a um
SQL = "SELECT seq_clausula, cod_clausula_original "
SQL = SQL & "FROM clausula_personalizada_tb  WITH (NOLOCK)    "
SQL = SQL & "WHERE proposta_id = " & num_proposta
If TpEmissao = "A" Then
   SQL = SQL & " AND (endosso_id = 0 OR endosso_id is null)"
Else
   SQL = SQL & " AND endosso_id = " & num_endosso
End If
'
Set rc2 = rdocn2.OpenResultset(SQL)
'
cont_clau = 0
'
Do While Not rc2.EOF

    'altera��o de Leandro A. Souza - Stefanini IT - 28/08/2006
    'OBS: foi feito um tratamento para que o campo "texto_clausula" da query abaixo possa ser tratado corretamente quando o mesmo possuir mais de 8000 caracteres.
    'Por�m, estabeleci um limite de 80000 caracteres.
    SQL = ""
    SQL = SQL & " SELECT  "
    SQL = SQL & " substring(texto_clausula,00001,8000) 'texto_clausula_01' , "
    SQL = SQL & " substring(texto_clausula,08001,8000) 'texto_clausula_02' , "
    SQL = SQL & " substring(texto_clausula,16001,8000) 'texto_clausula_03' , "
    SQL = SQL & " substring(texto_clausula,24001,8000) 'texto_clausula_04' , "
    SQL = SQL & " substring(texto_clausula,32001,8000) 'texto_clausula_05' , "
    SQL = SQL & " substring(texto_clausula,40001,8000) 'texto_clausula_06' , "
    SQL = SQL & " substring(texto_clausula,48001,8000) 'texto_clausula_07' , "
    SQL = SQL & " substring(texto_clausula,56001,8000) 'texto_clausula_08' , "
    SQL = SQL & " substring(texto_clausula,64001,8000) 'texto_clausula_09' , "
    SQL = SQL & " substring(texto_clausula,72001,8000) 'texto_clausula_10'   "
    SQL = SQL & " FROM clausula_personalizada_tb  WITH (NOLOCK)   "
    SQL = SQL & " WHERE proposta_id = " & num_proposta
    If TpEmissao = "A" Then
       SQL = SQL & " AND (endosso_id = 0 OR endosso_id is null)"
    Else
       SQL = SQL & " AND endosso_id = " & num_endosso
    End If
    '' Adicionar seq e c�d.
    SQL = SQL & " AND seq_clausula = " & rc2!seq_clausula
    SQL = SQL & " AND cod_clausula_original = " & rc2!cod_clausula_original
 
    Set rc = rdocn.OpenResultset(SQL)

        '
    Do While Not rc.EOF
        cont_clau = cont_clau + 1
        '' For�ar quebra de p�gina a partir da 2a. cl�usula
        If cont_clau > 1 Then
            Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta & Space(100) & "S"
            
            'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Reg = Replace(Reg, vbCr, vbNullString)
            Reg = Replace(Reg, vbLf, vbNullString)
            Reg = Replace(Reg, vbCrLf, vbNullString)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
        End If
        '' Linha em branco
        Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
        
        'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Reg = Replace(Reg, vbCr, vbNullString)
        Reg = Replace(Reg, vbLf, vbNullString)
        Reg = Replace(Reg, vbCrLf, vbNullString)
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
        ContaLinhaAtual = ContaLinhaAtual + 1
            
        tamFinal = 0
        '
                
        'altera��o de Leandro A. Souza - Stefanini IT - 28/08/2006
        'OBS: foi feito um tratamento para que o campo "texto_clausula" da query abaixo possa ser tratado corretamente quando o mesmo possuir mais de 8000 caracteres.
        'Por�m, estabeleci um limite de 80000 caracteres.
        Texto_Clausula = Trim(rc!Texto_Clausula_01) & Trim(rc!Texto_Clausula_02) & _
                         Trim(rc!Texto_Clausula_03) & Trim(rc!Texto_Clausula_04) & _
                         Trim(rc!Texto_Clausula_05) & Trim(rc!Texto_Clausula_06) & _
                         Trim(rc!Texto_Clausula_07) & Trim(rc!Texto_Clausula_08) & _
                         Trim(rc!Texto_Clausula_09) & Trim(rc!Texto_Clausula_10)


        'Clausula = Formata_Clausula(TextoClausula)
        Clausula = Formata_Clausula(TrocaTabPorEspaco(Texto_Clausula, 1))
        
        
        'altera��o de Leandro A. Souza - Stefanini IT - 28/08/2006
        'OBS: substitui��o de caracteres '�' existentes no texto por " ", pois estes caracteres especiais causam falhas na gera��o das ap�lices.
        Clausula = Replace(Clausula, "�", " ")
                
        'Clausula = TextoClausula.Text
        ultQuebra = 1: Ultpos = 0 'Tamanho da string (+- 90 caracteres)
        For i = 1 To Len(Clausula)
            '' Se � final de linha
            If Mid(Clausula, i, 2) = Chr(13) & Chr(10) Then
                If ultQuebra > 0 And Ultpos - 2 > 0 Then
                    aux = Mid(Clausula, ultQuebra, Ultpos - 2)
                    
                    'corre��o dos flows 152061, 152062, 152083
                    'Leandro A. Souza - Stefanini
                    '04/08/2006
                    'para inserir corretamente linha em branco no arquivo,
                    'quando o 1� caracter da vari�vel aux for igual a <enter>
                    If Mid(aux, 1, 1) = vbLf Or Mid(aux, 1, 1) = vbCr Or Mid(aux, 1, 1) = vbCrLf Then
                        aux = Replace(aux, vbLf, vbNullString, 1, 1)
                        aux = Replace(aux, vbCrLf, vbNullString, 1, 1)
                        aux = Replace(aux, vbCr, vbNullString, 1, 1)
                        Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
                        
                        
                        'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Reg = Replace(Reg, vbCr, vbNullString)
                        Reg = Replace(Reg, vbLf, vbNullString)
                        Reg = Replace(Reg, vbCrLf, vbNullString)
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                        
                        ContaLinhaAtual = ContaLinhaAtual + 1
                    End If
                    ''''''''''''' fim da corre��o ''''''''''''''''''''''''''''''''''
                    
                Else
                    aux = ""
                End If
                Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
                Reg = Reg & Space(10) & aux
                              
                              
                'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Reg = Replace(Reg, vbCr, vbNullString)
                Reg = Replace(Reg, vbLf, vbNullString)
                Reg = Replace(Reg, vbCrLf, vbNullString)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                ContaLinhaAtual = ContaLinhaAtual + 1
                '
                tamFinal = Ultpos
                Ultpos = 0
                ultQuebra = i + 2
             End If
             '
             Ultpos = Ultpos + 1
        Next
        '
        If Ultpos > 0 Then 'Ent�o ainda falta texto p/ ser impresso depois da �ltima quebra
            aux = Mid(Clausula, ultQuebra, Ultpos)
            
            'corre��o dos flows 152061, 152062, 152083
            'Leandro A. Souza - Stefanini
            '04/08/2006
            'para inserir corretamente linha em branco no arquivo,
            'quando o 1� caracter da vari�vel aux for igual a <enter>
            If Mid(aux, 1, 1) = vbLf Or Mid(aux, 1, 1) = vbCr Or Mid(aux, 1, 1) = vbCrLf Then
                aux = Replace(aux, vbLf, vbNullString, 1, 1)
                aux = Replace(aux, vbCrLf, vbNullString, 1, 1)
                aux = Replace(aux, vbCr, vbNullString, 1, 1)
                Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
                
                
                'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Reg = Replace(Reg, vbCr, vbNullString)
                Reg = Replace(Reg, vbLf, vbNullString)
                Reg = Replace(Reg, vbCrLf, vbNullString)
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
                ContaLinhaAtual = ContaLinhaAtual + 1
            End If
            ''''''''''''' fim da corre��o ''''''''''''''''''''''''''''''''''
                    
                    
            Reg = "22" & Format(ContaLinhaAtual, "000000") & num_proposta
            Reg = Reg & Space(10) & aux
          
            'altera��o de Leandro A. Souza - Stefanini IT - 21/09/2006 - flow 162845
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Reg = Replace(Reg, vbCr, vbNullString)
            Reg = Replace(Reg, vbLf, vbNullString)
            Reg = Replace(Reg, vbCrLf, vbNullString)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
            Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
        End If
        '
        rc.MoveNext
    Loop
    'End If
    rc.Close
    rc2.MoveNext
Loop 'rc2
rc2.Close

Exit Sub

Erro:
   TrataErroGeral "Ler_Clausulas", Me.name
   TerminaSEGBR

End Sub



Private Sub Processa_Coberturas()
Dim RegClausula As Integer, i As Long, j As Long, linhaFranquia As String, ObjAnteriorBenef As Long
Dim ObjAnterior As Long, Endereco As String, linhasCobertura As Long, PriVez As String, k As Long, aux As String

On Error GoTo Erro
If QtdLinhasCobertura > 16 Then
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(15) & String(38, "*")
    Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(15) & "COBERTURAS CONTRATADAS CONFORME ANEXO"
    Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '
    Reg = "21" & Format(ContaLinhaAtual, "000000") & num_proposta
    Reg = Reg & Space(15) & String(38, "*")
    Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    '
    CoberturasPrimPagina = False
    RegClausula = 22
    linhasCobertura = 3
ElseIf QtdLinhasCobertura = 0 Then
    CoberturasPrimPagina = False
    RegClausula = 21
    linhasCobertura = 0
ElseIf QtdLinhasCobertura <= 16 Then
    CoberturasPrimPagina = True
    RegClausula = 21
    linhasCobertura = QtdCoberturas
End If
ObjAnterior = 0
'Para cada objeto segurado, listar coberturas e benefici�rios
If QtdObjetos > 0 Then
   If TranspInternacional Then
      ''Lista_CoberturasTransp RegClausula
   Else
      For k = 1 To QtdObjetos
         'Atualiza obj Anterior
         ObjAnterior = EnderecoRisco(0, k)
         'T�tulo do item
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Reg = Reg & "ITEM " & Format$(EnderecoRisco(0, k), "00") & ":" & Space(8)
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
         
         If ramo_id <> "22" Then 'Para transporte internacional, n�o listar endere�o de risco
            'Local do Risco
            Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
            Reg = Reg & "LOCAL DO RISCO: " & EnderecoRisco(1, k)
            Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
            ContaLinhaAtual = ContaLinhaAtual + 1
            linhasCobertura = linhasCobertura + 1
         End If
         If QtdCoberturas <> 0 Then
            PriVez = True
            For i = 0 To QtdCoberturas - 1
               If Cobertura(0, i) = EnderecoRisco(0, k) Then
                  If PriVez Then
                      'T�tulo Coberturas
                     Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
                     Reg = Reg & "CODIGO     DESCRICAO DA COBERTURA                                                     I.S.(" & MoedaSeguro & ")"
                     Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
                     ContaLinhaAtual = ContaLinhaAtual + 1
                     linhasCobertura = linhasCobertura + 1
                     PriVez = False
                  End If
                  'C�d e descri��o
                  Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
                  Reg = Reg & Format$(Cobertura(1, i), "000") & Space(8)
                  'Reg = Reg & Left(Cobertura(2, i) & Space(200), 200)           '200 caracteres para a descri��o sem UCase 27/10/2003
                  Reg = Reg & Left(Cobertura(2, i) & Space(72), 72)           '72 caracteres para a descri��o sem UCase 19/10/2004
                  'Imp Segurada
                  If ConfiguracaoBrasil Then
                      Reg = Reg & Right(Space(16) & Format(Cobertura(3, i), "#,###,###,##0.00"), 16)
                  Else
                      Reg = Reg & Right(Space(16) & TrocaValorAmePorBras(Format(Cobertura(3, i), "#,###,###,##0.00")), 16)
                  End If
                  Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
                  ContaLinhaAtual = ContaLinhaAtual + 1
                  
                  'Franquia
                  linhaFranquia = ""
                  If Cobertura(4, i) <> 0 Or Cobertura(5, i) <> "" Or Cobertura(6, i) <> 0 Then
                     Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & Space(11) & "FRANQUIA : "
                     If Cobertura(4, i) <> 0 Then
                        If ConfiguracaoBrasil Then
                            linhaFranquia = linhaFranquia & Right(Space(8) & Format(Cobertura(4, i), "##0.00") & " %", 8)
                        Else
                            linhaFranquia = linhaFranquia & Right(Space(8) & TrocaValorAmePorBras(Format(Cobertura(4, i), "##0.00")) & " %", 8)
                        End If
                     End If
                     If Cobertura(5, i) <> "" Then
                        'Colocar separador caso o anterior estiver preenchido
                        If linhaFranquia <> "" Then
                           linhaFranquia = linhaFranquia & " - "
                        End If
                        linhaFranquia = linhaFranquia & Trim(Cobertura(5, i))
                     End If
                     If Cobertura(6, i) <> 0 Then
                        'Colocar separador caso o anterior estiver preenchido
                        If linhaFranquia <> "" Then
                           linhaFranquia = linhaFranquia & " - M�nimo de: R$ "
                        End If
                        If ConfiguracaoBrasil Then
                           linhaFranquia = linhaFranquia & Format(Cobertura(6, i), "#,###,###,##0.00")
                        Else
                           linhaFranquia = linhaFranquia & TrocaValorAmePorBras(Format(Cobertura(6, i), "#,###,###,##0.00"))
                        End If
                     End If
                     Reg = Reg & linhaFranquia
                     Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
                     ContaLinhaAtual = ContaLinhaAtual + 1
                  End If
               End If
            Next
         End If
         'Monta linhas de Benefici�rios do obj. segurado
         Lista_Beneficiarios RegClausula, EnderecoRisco(0, k)
         
         'Pula uma linha para cada novo obj. segurado
         Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
         Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
         ContaLinhaAtual = ContaLinhaAtual + 1
         linhasCobertura = linhasCobertura + 1
      Next
   End If
Else
   'No caso de ENDOSSO, os objetos podem n�o ter sido alterados
   If QtdCoberturas > 0 Then
      Lista_Coberturas (RegClausula)
   ElseIf QtdBenefs > 0 Then
      Lista_Beneficiarios RegClausula
   End If
End If

'Total Descontos
'If ValTotDesconto <> 0 Then
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Reg = Reg & "Total de descontos : " & Left(MoedaPremio + Format(ValTotDesconto, "#,###,###,##0.00") & Space(16), 16)
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   ContaLinhaAtual = ContaLinhaAtual + 1
'   'pular linha
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   ContaLinhaAtual = ContaLinhaAtual + 1
'End If
        
If QtdCongeneres > 0 Then
'   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta & String(100, " ")
'   Print #Arquivo, Left(Reg + Space(Tam_reg), Tam_reg)
'   ContaLinhaAtual = ContaLinhaAtual + 1
   Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
   Reg = Reg & "CONG�NERE(S)" & Space(69) & "PERC. PARTICIPA��O"
   Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
   ContaLinhaAtual = ContaLinhaAtual + 1
   For i = 0 To QtdCongeneres
      aux = Left(Congenere(0, i) & Space(59), 59) & Right(Space(40) & Congenere(1, i), 40)
      Reg = RegClausula & Format(ContaLinhaAtual, "000000") & num_proposta
      Reg = Reg & aux
      Print #Arquivo, Left(Reg + Space(tam_reg), tam_reg)
      ContaLinhaAtual = ContaLinhaAtual + 1
   Next
End If
        
Exit Sub

Erro:
   TrataErroGeral "Processa_Coberturas", Me.name
   TerminaSEGBR

End Sub

Private Sub Ler_Cliente()
   
On Error GoTo Erro

SQL = "SELECT c.nome, isnull(pf.cpf,'') as CPF, pj.pj_cliente_id, "
SQL = SQL & " isnull(pj.cgc,'') as CGC, e.endereco, e.bairro, e.municipio, "
SQL = SQL & " e.estado, e.cep, c.ddd_1 , c.telefone_1"
SQL = SQL & " FROM proposta_tb p INNER JOIN cliente_tb c  WITH (NOLOCK)   "
SQL = SQL & " ON (p.prop_cliente_id = c.cliente_id)"
SQL = SQL & " INNER JOIN endereco_corresp_tb e  WITH (NOLOCK)  "
SQL = SQL & " ON (p.proposta_id=e.proposta_id)"
SQL = SQL & " LEFT JOIN pessoa_fisica_tb pf  WITH (NOLOCK)  "
SQL = SQL & " ON (pf_cliente_id = p.prop_cliente_id)"
SQL = SQL & " LEFT JOIN pessoa_juridica_tb pj  WITH (NOLOCK)  "
SQL = SQL & " ON (pj_cliente_id = p.prop_cliente_id)"
SQL = SQL & " WHERE  p.proposta_id = " & num_proposta
''
'----------------------------------------------------------------------
' RNC - 09/09/2005 - Chamado 91.060
' Alterado por Francisco Teixeira - Stefanini
'----------------------------------------------------------------------
Set rc = rdocn3.OpenResultset(SQL)
'
If Not rc.EOF Then
    If TpEmissao = "A" Then
        If ProdutoId = 15 Then
            Reg = "20" & Format(ContaLinhaAtual, "000000") & num_proposta & num_apolice & "CERTIF "
        Else
            Reg = "20" & Format(ContaLinhaAtual, "000000") & num_proposta & num_apolice & "APOLICE"
        End If
    Else
        Reg = "20" & Format(ContaLinhaAtual, "000000") & num_proposta & num_apolice & "ENDOSSO"
    End If
    ''Nome
    If Not IsNull(rc!nome) Then
        Reg = Reg & "SEGURADO  : " & UCase(Left(rc!nome & Space(60), 60))
    Else
        Reg = Reg & "SEGURADO  : " & Space(60)
    End If
    ''Cgc/Cpf
    If IsNull(rc!pj_cliente_id) Then
        Reg = Reg & "CPF       : " & Left(Format(rc!CPF, "&&&.&&&.&&&-&&") & "    " & Space(18), 18)
    Else
        Reg = Reg & "CNPJ      : " & Left(Format(rc!CGC, "&&.&&&.&&&/&&&&-&&") & Space(18), 18)
    End If
    ''Endere�o
    If Not IsNull(rc!Endereco) Then
        'Reg = Reg & "ENDERECO  : " & UCase(Left(("" & rc!Endereco) & Space(60), 60))
        Reg = Reg & "ENDERECO  : " & UCase(Left(("" & rc!Endereco) & Space(200), 200)) 'Sala �gil Sprint 17 - rfarzat 14/02/2019
    Else
        'Reg = Reg & "ENDERECO  : " & Space(60)
        Reg = Reg & "ENDERECO  : " & Space(200) 'Sala �gil Sprint 17 - rfarzat 14/02/2019
    End If
    'Bairro
    If Not IsNull(rc!Bairro) Then
        Reg = Reg & UCase(Left(rc!Bairro & Space(32), 32))
    Else
        Reg = Reg & Space(32)
    End If
    'Cidade
    If Not IsNull(rc!Municipio) Then
        Reg = Reg & "MUNICIPIO : " & UCase(Left(rc!Municipio & Space(35), 35))
    Else
        Reg = Reg & "MUNICIPIO : " & Space(35)
    End If
    'UF
    If Not IsNull(rc!Estado) Then
        Reg = Reg & UCase(Left(rc!Estado & Space(2), 2))
    Else
        Reg = Reg & "  "
    End If
    'CEP
    If Not IsNull(rc!Cep) And Trim(rc!Cep) <> "" Then
        'Ricardo Toledo : Confitec : 09/09/2010
        'N�o est� formatando corretamente quando o CEP tem menos de 08 posi��es. O correto � inserir zeros � frente
        'Reg = Reg & "C.E.P.    : " & Left(Format$(rc!Cep, "&&&&&-&&&") & Space(9), 9)
        Reg = Reg & "C.E.P.    : " & Left(Format$(rc!Cep, "00000-000") & Space(9), 9)
    Else
        Reg = Reg & "C.E.P.    : " & Space(9)
    End If
    
End If
    
rc.Close
Set rc = Nothing
''
Exit Sub

Erro:
    TrataErroGeral "Ler_Cliente", Me.name
    TerminaSEGBR

End Sub


Private Sub Ler_Endereco_Risco()
Dim Endereco As String
Dim EndRisco As String
On Error GoTo Erro

SQL = "SELECT 1, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id "
SQL = SQL & "   FROM  endereco_risco_tb a   WITH (NOLOCK)  INNER JOIN seguro_empresarial_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 2, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_residencial_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id  "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 3, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_condominio_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 4, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_maquinas_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 5, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_aceito_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 6, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_avulso_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
'Sql = Sql & "   dt_inicio_vigencia_seg <='" & Format(DtInicioVigencia, "yyyymmdd") & "'"
'Sql = Sql & "   AND dt_fim_vigencia_seg IS NULL"
SQL = SQL & " UNION "
SQL = SQL & "SELECT 7, a.end_risco_id, endereco, bairro, municipio, estado, cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  endereco_risco_tb a  WITH (NOLOCK)   INNER JOIN seguro_generico_tb b  WITH (NOLOCK)   ON "
SQL = SQL & "   a.end_risco_id=b.end_risco_id AND "
SQL = SQL & "   a.proposta_id=b.proposta_id "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (a.num_endosso=0 or a.num_endosso is null) "
Else
   SQL = SQL & " a.num_endosso=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If
SQL = SQL & " UNION "
SQL = SQL & "SELECT 8, 0, '' endereco, '' bairro, '' municipio, '  ' estado, '' cep, b.cod_objeto_segurado, b.subramo_id  "
SQL = SQL & "   FROM  seguro_transporte_tb b  WITH (NOLOCK)   "
SQL = SQL & "   WHERE b.proposta_id  = " & num_proposta & " AND "
If TpEmissao = "A" Then
   SQL = SQL & " (b.endosso_id=0 or b.endosso_id is null) "
Else
   SQL = SQL & " b.endosso_id=" & num_endosso
   SQL = SQL & " AND b.dt_fim_vigencia_seg is null "
End If

Set rc = rdocn.OpenResultset(SQL)
If Not rc.EOF Then
   Subramo = rc!SUBRAMO_ID
Else
   Subramo = 0
End If
QtdObjetos = 0
ReDim EnderecoRisco(2, 5)
TabEscolha = ""
Do While Not rc.EOF
   Select Case rc(0)
   Case 1
      TabEscolha = "escolha_tp_cob_emp_tb"
   Case 2
      TabEscolha = "escolha_tp_cob_res_tb"
   Case 3
      TabEscolha = "escolha_tp_cob_cond_tb"
   Case 4
      TabEscolha = "escolha_tp_cob_maq_tb"
   Case 5
      TabEscolha = "escolha_tp_cob_aceito_tb"
   Case 6
      TabEscolha = "escolha_tp_cob_avulso_tb"
   Case 7
      TabEscolha = "escolha_tp_cob_generico_tb"
   Case 8
      TabEscolha = "escolha_verba_transporte_tb"
   End Select
   Endereco = ""
   'Endere�o Risco
   If Trim(rc!Endereco) <> "" Then
       Endereco = UCase(Trim(rc!Endereco))
   End If
   'Bairro Risco
   If Trim(rc!Bairro) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Bairro))
   End If
   'Cidade Risco
   If Trim("" & rc!Municipio) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Municipio))
   End If
   'UF Risco
   If Trim("" & rc!Estado) <> "" Then
      If Endereco <> "" Then Endereco = Endereco & " - "
      Endereco = Endereco & UCase(Trim(rc!Estado))
   End If
   EndRisco = Endereco
   QtdObjetos = QtdObjetos + 1
   If QtdObjetos > 5 Then
      ReDim Preserve EnderecoRisco(2, QtdObjetos + 5)
   End If
   EnderecoRisco(0, QtdObjetos) = Val(0 & rc!cod_objeto_segurado)
   EnderecoRisco(1, QtdObjetos) = Endereco
   rc.MoveNext
   If Not rc.EOF Then
      'Se tem mais de um endere�o de risco e � o produto AVULSO
      If produto_externo_id = 999 Then
         EndRisco = String(11, " ") & "( DIVERSOS )"
      End If
   End If
Loop
'** pcarvalho - 27/12/2002 Novos Ramos
If ramo_id = "22" Or ramo_id = "44" Then
   EndRisco = String(75, "*")
End If
Reg = Reg & Left(EndRisco & Space(112), 112)
rc.Close
      
Exit Sub

Erro:
   TrataErroGeral "Ler_Endereco_Risco", Me.name
   TerminaSEGBR

End Sub
Private Sub Ler_Proposta_Fechada()

Dim QtdParcelas As Long, rs As rdoResultset, rc As rdoResultset, i As Long, Valcobranca As Double
Dim ValUltParcela As Currency, ValParcela1 As Currency, ValParcelaDemais As Currency
Dim ValIof As Currency, ValJuros As Currency, CustoApolice As Currency
Dim PremioTarifa As Currency, PremioBruto As Currency, QtdDatas As Integer
Dim DataAgendamento() As String, PremioLiquido As Currency
Dim Nome_agencia As String
Dim PAR As Boolean
Dim Periodo_pgto_id As Integer

On Error GoTo Erro
                
SQL = "SELECT p.proposta_bb, p.proposta_id, p.cont_agencia_id, p.periodo_pgto_id "
SQL = SQL & ", p.cont_banco_id, p.agencia_id, p.banco_id "
SQL = SQL & ", p.val_iof, p.custo_apolice, p.val_premio_tarifa "
SQL = SQL & ", p.val_premio_bruto, p.num_parcelas, p.val_juros "
SQL = SQL & ", p.val_pgto_ato, p.val_parcela, isnull(p.forma_pgto_id, 99) forma_pgto_id "
SQL = SQL & ", p.val_tot_desconto_comercial"
SQL = SQL & ", m.sigla sigla_premio, p.premio_moeda_id "
SQL = SQL & ", m1.sigla sigla_seg, p.seguro_moeda_id "
SQL = SQL & " FROM proposta_fechada_tb p  WITH (NOLOCK)   "
SQL = SQL & " inner join moeda_tb as m on p.premio_moeda_id = m.moeda_id "
SQL = SQL & " inner join moeda_tb as m1 on p.seguro_moeda_id = m1.moeda_id "
SQL = SQL & " WHERE proposta_id = " & num_proposta
Set rc = rdocn.OpenResultset(SQL)
        
If Not rc.EOF Then
    If PropAnt = 0 Then
        Reg = Reg & "000000000"
    Else
        SQL = "SELECT apolice_id  FROM apolice_tb  WITH (NOLOCK)   "
        SQL = SQL & "  WHERE  proposta_id = " & PropAnt
        '
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            Reg = Reg & Format(Rc1(0), "000000000")
        Else
            Reg = Reg & "000000000"
        End If
    End If
    
    'Proposta_bb
    Reg = Reg & IIf(IsNull(rc!proposta_bb), "000000000", Format(rc!proposta_bb, "000000000"))
    
    Reg = Reg & Space(55)    'N�o imprime mais o nome da ag�ncia do cliente
    
    'C�digo da ag�ncia
    ContAgencia = IIf(IsNull(rc!cont_agencia_id), "0000", Format(rc!cont_agencia_id, "0000"))

    'Agencia Cobran�a
    If Not IsNull(rc!cont_agencia_id) Then
        If rc!cont_agencia_id <> 9999 Then
           'Nome Ag�ncia
            SQL = " SELECT nome FROM agencia_tb  WITH (NOLOCK)   "
            SQL = SQL & " WHERE agencia_id = " & rc!cont_agencia_id
            SQL = SQL & " AND banco_id = " & IIf(IsNull(rc!cont_banco_id), 1, rc!cont_banco_id)
            '
            Set Rc1 = rdocn.OpenResultset(SQL)
            If Not Rc1.EOF Then   ' Achou o nome.
                If Not IsNull(Rc1(0)) Then  '  Nome n�o nulo.
                    Reg = Reg & Format(rc!cont_agencia_id, "0000") & " - "
                    Reg = Reg & UCase(Left(Trim(Rc1(0)) & Space(16), 16))
                Else  '  Nome nulo.
                    Reg = Reg & Space(6) & String(11, "*") & Space(6)
                End If
            Else   ' n�o achou o nome da agencia.
    '            Reg = Reg & "NAO IDENTIFICADA"
                Reg = Reg & Space(6) & String(11, "*") & Space(6)
            End If
            Rc1.Close
            Set Rc1 = Nothing
        Else    '' C�d. Agencia = 9999
            Reg = Reg & Space(6) & String(11, "*") & Space(6)
        End If
    Else  ' sem codigo de agencia.
'        Reg = Reg & "9999 - NAO IDENTIFICADA"
        Reg = Reg & Space(6) & String(11, "*") & Space(6)
    End If
    
    ''Reg = Reg & Left$(Nome_agencia & Space(23), 23)

    ''
    'Procurar valores em endosso_financeiro_tb no caso de endosso
    If TpEmissao = "E" Then
        SQL = "SELECT * FROM endosso_financeiro_tb  WITH (NOLOCK)   WHERE "
        SQL = SQL & " proposta_id = " & num_proposta
        SQL = SQL & " AND endosso_id = " & num_endosso
        '
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            ValIof = Val(0 & Rc1!val_iof)
            CustoApolice = Val(0 & Rc1!Custo_Apolice)
            ValJuros = Val(0 & Rc1!val_adic_fracionamento)
            PremioTarifa = Val(0 & Rc1!val_premio_tarifa)
            ValTotDesconto = Val(0 & Rc1!val_desconto_comercial)
            QtdParcelas = Val(0 & Rc1!num_parcelas)
            PremioBruto = PremioTarifa - ValTotDesconto + CustoApolice + ValIof + ValJuros
        End If
        Rc1.Close
    Else 'Se for ap�lice, pega de proposta fechada
        ValIof = Val(0 & rc!val_iof)
        CustoApolice = Val(0 & rc!Custo_Apolice)
        ValJuros = Val(0 & rc!val_juros)
        PremioTarifa = Val(0 & rc!val_premio_tarifa)
        PremioBruto = Val(0 & rc!val_premio_bruto)
        ValTotDesconto = Val(0 & rc!val_tot_desconto_comercial)
        QtdParcelas = Val(0 & rc!num_parcelas)
        PremioBruto = PremioTarifa - ValTotDesconto + CustoApolice + ValIof + ValJuros
    End If
    ''
    MoedaPremio = Trim(rc!sigla_premio)
    MoedaSeguro = Trim(rc!sigla_seg)
    '
    'Valor IOF
    If ValIof <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + Format(ValIof, "#,###,###,##0.00") & Space(16), 16)
        Else
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + TrocaValorAmePorBras(Format(ValIof, "#,###,###,##0.00")) & Space(16), 16)
        End If
    Else
        Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    End If
    'Valor Custo Ap�lice
    If CustoApolice <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + Format(CustoApolice, "#,###,###,##0.00") & Space(16), 16)
        Else
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + TrocaValorAmePorBras(Format(CustoApolice, "#,###,###,##0.00")) & Space(16), 16)
        End If
    Else
        Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    End If
    'Valor Juros
    If ValJuros > 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + Format(ValJuros, "#,###,###,##0.00") & Space(16), 16)
        Else
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + TrocaValorAmePorBras(Format(ValJuros, "#,###,###,##0.00")) & Space(16), 16)
        End If
    Else
        Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    End If
    'Valor Pr�mio Liquido
    If PremioTarifa = 0 Then
        Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    Else
        If ConfiguracaoBrasil Then
            PremioLiquido = CCur(PremioTarifa) - CCur(ValTotDesconto)
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + Format(PremioLiquido, "#,###,###,##0.00") & Space(16), 16)
        Else
            PremioLiquido = CCur(TrocaValorAmePorBras(PremioTarifa)) - CCur(TrocaValorAmePorBras(ValTotDesconto))
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + TrocaValorAmePorBras(Format(PremioLiquido, "#,###,###,##0.00")) & Space(16), 16)
        End If
    End If
    'Valor Pr�mio Bruto
    If PremioBruto <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + Format(PremioBruto, "#,###,###,##0.00") & Space(16), 16)
        Else
            Reg = Reg & Left(Format(MoedaPremio, "@@@") + TrocaValorAmePorBras(Format(PremioBruto, "#,###,###,##0.00")) & Space(16), 16)
        End If
    Else
        Reg = Reg & Format(MoedaPremio, "@@@") & "0,00" & String(9, " ")
    End If
    ''
    'Valor Total Descontos ********* n�o ser� mais impresso
    'If ValTotDesconto <> 0 Then
    '      QtdLinhasCobertura = QtdLinhasCobertura + 2 '1 linha p/ o desconto e uma linha em branco (p/ n�o ficar colado no limite de responsabilidade)
    '      If ConfiguracaoBrasil Then
    '         ValTotDesconto = Format(ValTotDesconto, "#,###,###,##0.00")
    '      Else
    '         ValTotDesconto = TrocaValorAmePorBras(Format(ValTotDesconto, "#,###,###,##0.00"))
    '      End If
    '   End If
    '   'Qtd Parcelas
    '   If QtdParcelas <> 0 Then
    '      Reg = Reg & "Qt. Parcelas    : " & Format$(rc!num_parcelas, "00")
    '   Else
    '      Reg = Reg & "00"
    '   End If
    ''
    '' Agendamento n�o tem �ndice por ap�lice
    '' Al�m do mais, para Vida Empresa tem que buscar por proposta
    SQL = "SELECT  val_cobranca, num_cobranca, dt_agendamento "
    SQL = SQL & "FROM agendamento_cobranca_tb  WITH (NOLOCK)   "
    'sql = sql & " WHERE apolice_id = " & num_apolice
    'sql = sql & " AND seguradora_cod_susep = " & Seguradora
    'sql = sql & " AND sucursal_seguradora_id = " & Sucursal
    'sql = sql & " AND ramo_id = " & ramo_id
    SQL = SQL & " WHERE proposta_id = " & num_proposta
    SQL = SQL & " AND situacao in ('a','e','i','r','p') "
    If TpEmissao = "A" Then
        SQL = SQL & "  AND (num_endosso = 0 OR num_endosso is null) "
    Else
        SQL = SQL & "  AND num_endosso = " & num_endosso
    End If
    SQL = SQL & "  ORDER BY num_cobranca"
    '
    Set Rc1 = rdocn.OpenResultset(SQL)
    QtdDatas = 0
    If Not Rc1.EOF Then
        '' Permitir at� 12 agendamentos
        'ReDim DataAgendamento(10)
        ReDim DataAgendamento(12)
        ''
        ValParcela1 = Val(Rc1(0))
        QtdDatas = QtdDatas + 1
        DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
        '
        Rc1.MoveNext
        If Rc1.EOF Then    's� tem uma parcela
            ValParcelaDemais = 0
            ValUltParcela = 0
        Else 'tem mais de uma parcela
            ValParcelaDemais = Val(Rc1(0))
            ValUltParcela = 0
            QtdDatas = QtdDatas + 1
            DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
            '
            Rc1.MoveNext
            Do While Not Rc1.EOF
                QtdDatas = QtdDatas + 1
                '' Estava somando mais dez posi��es ao vetor
                '' quando chegava a 10 parcelas
                ''If QtdDatas Mod 10 = 0 Then
                ''   ReDim Preserve DataAgendamento(QtdDatas + 10)
                ''End If
                DataAgendamento(QtdDatas) = Format$(Rc1!dt_agendamento, "dd/mm/yyyy")
                ValUltParcela = Val(0 & Rc1(0))
                Rc1.MoveNext
            Loop
            If ValParcelaDemais = ValUltParcela Then
                ValUltParcela = 0
            End If
        End If
    Else
        ValParcela1 = 0
        ValParcelaDemais = 0
        ValUltParcela = 0
    End If
    Rc1.Close
    'Qtd Parcelas (Qtd. de datas em agendamento cobran�a)
    If QtdDatas <> 0 Then
        Reg = Reg & "Qt.Parcelas " & Format$(QtdDatas, "00")
    Else
'        Reg = Reg & String(20, " ")
        If IsNull(rc!Periodo_pgto_id) Then
            Reg = Reg & "FATURA" & Space(8)
        ElseIf rc!Periodo_pgto_id = 99 Then
            Reg = Reg & "FATURA" & Space(8)
        Else
            SQL = "select nome from periodo_pgto_tb  WITH (NOLOCK)   "
            SQL = SQL & " where periodo_pgto_id = " & rc!Periodo_pgto_id
            Set Rc1 = rdocn.OpenResultset(SQL)
            If Not Rc1.EOF Then
                Reg = Reg & Left(Rc1!nome & Space(14), 14)
            Else
                Reg = Reg & "FATURA" & Space(8)
            End If
        End If
    End If
      'Valor 1� Parcela
    If ValParcela1 <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & "Parcela 1 : " & Format(MoedaPremio, "@@@") & Right(Space(13) & Format(ValParcela1, "##,###,##0.00"), 13)
        Else
            Reg = Reg & "Parcela 1 : " & Format(MoedaPremio, "@@@") & Right(Space(13) & TrocaValorAmePorBras(Format(ValParcela1, "##,###,##0.00")), 13)
        End If
    Else
        Reg = Reg & String(28, " ")
    End If
    'Valor Demais Parcelas
    If ValParcelaDemais <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & "Demais Parcelas : " & Format(MoedaPremio, "@@@") + Right(Space(13) + Format(ValParcelaDemais, "##,###,##0.00"), 13)
        Else
            Reg = Reg & "Demais Parcelas : " & Format(MoedaPremio, "@@@") + Right(Space(13) + TrocaValorAmePorBras(Format(ValParcelaDemais, "##,###,##0.00")), 13)
        End If
    Else
        Reg = Reg & String(34, " ")
    End If
    'Valor �ltima Parcela
    If ValUltParcela <> 0 Then
        If ConfiguracaoBrasil Then
            Reg = Reg & "�ltima Parcela  : " & Format(MoedaPremio, "@@@") + Right(Space(13) + Format(ValUltParcela, "##,###,##0.00"), 13)
        Else
            Reg = Reg & "�ltima Parcela  : " & Format(MoedaPremio, "@@@") + Right(Space(13) + TrocaValorAmePorBras(Format(ValUltParcela, "##,###,##0.00")), 13)
        End If
    Else
        Reg = Reg & String(34, " ")
    End If
    'Tratar dt vencimento
    If QtdDatas <> 0 Then
       If Val(0 & rc!val_pgto_ato) = 0 Then
           Reg = Reg & "01-� VISTA   "
       Else
           Reg = Reg & "01-" & DataAgendamento(1)
       End If
       'Preenche Dts Vencimento
       For i = 2 To QtdDatas
           Reg = Reg & Format$(i, "00") & "-" & DataAgendamento(i)
       Next
    End If
    'Preenche vazios
    'For i = QtdDatas + 1 To 10
    '' Agora s�o at� 12 parcelas
    For i = QtdDatas + 1 To 12
        Reg = Reg & Space(13)
    Next
    'Forma Cobran�a
    If Not IsNull(rc!forma_pgto_id) Then
        SQL = "SELECT nome FROM forma_pgto_tb  WITH (NOLOCK)   "
        SQL = SQL & "WHERE forma_pgto_id = " & rc!forma_pgto_id
        '
        Set Rc1 = rdocn.OpenResultset(SQL)
        If Not Rc1.EOF Then
            If Not IsNull(Rc1(0)) Then
                Reg = Reg & UCase(Left(Rc1(0) & Space(30), 30))
            End If
        Else
            Reg = Reg & Space(30)
        End If
        Rc1.Close
    Else
        Reg = Reg & Space(30)
    End If
Else
    Ler_Proposta_Adesao_OuroVidaEmp '* jmendes - 30/04/2002
End If
rc.Close
Set rc = Nothing
Exit Sub

Erro:
   TrataErroGeral "Ler_Proposta_Fechada", Me.name
   TerminaSEGBR

End Sub

Public Function BuscaParametro(ByVal pParametro As String) As String

Dim rs As rdoResultset

On Error GoTo ErroBuscaParametro

SQL = "Select val_parametro"
SQL = SQL & " From ps_parametro_tb  WITH (NOLOCK)  "
SQL = SQL & " Where parametro = '" & pParametro & "'"

Set rs = rdocn1.OpenResultset(SQL)

If Not rs.EOF Then
    BuscaParametro = rs(0)
Else
    BuscaParametro = ""
End If
rs.Close

Exit Function

ErroBuscaParametro:
    TrataErroGeral "BuscaParametro", Me.name
    TerminaSEGBR

End Function

Private Function Formata_Clausula(TextoClausula As String) As String

    Dim texto As String, ULTIMA_QUEBRA As Long
    Dim encontrou As Boolean, FRASE As String
    Dim CONT_CLAUSULA As Long, CONT_FRASE As Integer
    Dim ACHA_ESPACO As Long
    
    ULTIMA_QUEBRA = 1
    encontrou = False
    texto = ""
    CONT_FRASE = 0
    For CONT_CLAUSULA = 1 To Len(TextoClausula)
        CONT_FRASE = CONT_FRASE + 1
        If Mid(TextoClausula, CONT_CLAUSULA, 1) = Chr(13) Then
            FRASE = Mid(TextoClausula, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA) & " " & vbNewLine
            ULTIMA_QUEBRA = CONT_CLAUSULA + 2
            CONT_CLAUSULA = CONT_CLAUSULA + 1
            CONT_FRASE = 0
        ElseIf CONT_FRASE = 90 Then
            encontrou = False
            If Mid(TextoClausula, CONT_CLAUSULA + 1, 1) <> " " And Mid(TextoClausula, CONT_CLAUSULA + 1, 1) <> Chr(13) Then
                For ACHA_ESPACO = CONT_CLAUSULA To ULTIMA_QUEBRA Step -1
                    If Mid(TextoClausula, ACHA_ESPACO, 1) = " " Then
                        FRASE = Mid(TextoClausula, ULTIMA_QUEBRA, ACHA_ESPACO - ULTIMA_QUEBRA) & vbNewLine
                        CONT_FRASE = CONT_CLAUSULA - ACHA_ESPACO
                        ULTIMA_QUEBRA = ACHA_ESPACO + 1
                        encontrou = True
                        Exit For
                    End If
                Next ACHA_ESPACO
            End If
            If Not encontrou Then
                FRASE = RTrim(Mid(TextoClausula, ULTIMA_QUEBRA, 90)) & vbNewLine
                CONT_FRASE = 0
                ULTIMA_QUEBRA = ULTIMA_QUEBRA + 90
                If Mid(TextoClausula, ULTIMA_QUEBRA, 1) = Chr(13) Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                    CONT_CLAUSULA = CONT_CLAUSULA + 2
                ElseIf Mid(TextoClausula, ULTIMA_QUEBRA, 1) = " " Then
                    ULTIMA_QUEBRA = ULTIMA_QUEBRA + 1
                    CONT_CLAUSULA = CONT_CLAUSULA + 1
                    If Mid(TextoClausula, ULTIMA_QUEBRA, 1) = Chr(13) Then
                        ULTIMA_QUEBRA = ULTIMA_QUEBRA + 2
                        CONT_CLAUSULA = CONT_CLAUSULA + 2
                    End If
                End If
            End If
        End If
        If FRASE <> "" Then
            texto = texto & FRASE
            FRASE = ""
        End If
    Next CONT_CLAUSULA
    If ULTIMA_QUEBRA < Len(TextoClausula) Then
        texto = texto & Mid(TextoClausula, ULTIMA_QUEBRA, CONT_CLAUSULA - ULTIMA_QUEBRA + 1)
    End If
    
    Formata_Clausula = texto

End Function

Sub Ler_Dados_Complementares()

Dim DtFim As String, Obs As String
'Luciana - 04/07/2003
Dim var_corretor_susep As String
    
On Error GoTo Erro

'Gravando dados do Corretor Alterado ou Incluido no Endosso''''''''''''''''''''''''''''''''''''''''


'Luciana - 04/07/2003 - Alterado de corretor_id para corretor_susep

'Demanda 4532649 - Jos� Edson
'sql1 = "SELECT d.corretor_susep, d.nome  "
sql1 = "SELECT Distinct d.corretor_susep, d.nome  "
'------------------------------


sql1 = sql1 & "FROM corretagem_tb a  WITH (NOLOCK)   "
sql1 = sql1 & " inner join  corretor_tb d "
sql1 = sql1 & " on a.corretor_id = d.corretor_id "
sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
If TpEmissao = "A" Then
    sql1 = sql1 & " AND (endosso_id = 0 or endosso_id is null) "
Else
    sql1 = sql1 & " AND endosso_id = " & num_endosso
End If
sql1 = sql1 & "  AND dt_fim_corretagem is null "

Set rc = rdocn.OpenResultset(sql1)

If rc.EOF Or IsNull(rc!Corretor_Susep) Then            'incluido em 17/07/2003

    sql1 = ""
    'Demanda 4532649 - Jos� Edson
    'sql1 = sql1 & " SELECT d.corretor_susep, d.nome "
    sql1 = sql1 & " SELECT Distinct d.corretor_susep, d.nome "
    
    sql1 = sql1 & " FROM corretagem_pj_endosso_fin_tb a  WITH (NOLOCK)  "
    sql1 = sql1 & " inner join  corretor_tb d "
    sql1 = sql1 & " on a.corretor_id = d.corretor_id"
    sql1 = sql1 & " WHERE a.proposta_id = " & num_proposta
    sql1 = sql1 & " AND D.Corretor_Susep Is Not Null"
    
    Set rc = rdocn.OpenResultset(sql1)
    
End If

' Se houver mais de um corretor para uma mesma proposta,
' seus codigos e nomes serao armazenados em area temporaria
' para que sejam descarregados posteriormente, na area
' de ANEXOS. (Marisa).


''-------------------------------
'MATHAYDE - 28/08/2009
'DEMANDA: 920234 - comentado para aceitar mais de um corretor
'qtd_corretores = 0
'Varios_corretores = False
'
'Do While Not rc.EOF
'    qtd_corretores = qtd_corretores + 1
'    ReDim Preserve corretor_id(qtd_corretores) As String
'    ReDim Preserve nome_corretor(qtd_corretores) As String
'    'Luciana - 04/07/2003 - Alterado de corretor_id para corretor_susep
'    corretor_id(qtd_corretores) = rc!Corretor_Susep
'    nome_corretor(qtd_corretores) = rc!nome
'
'    rc.MoveNext
'Loop
'
'rc.Close
'
'If qtd_corretores > 1 Then
'    Varios_corretores = True
'End If
'
''
'' So serao impressos os dados do corretor aqui se ele for o unico
'' para a proposta corrente. (Marisa).
''
''If Not rc.EOF Then
'
'If Not Varios_corretores And qtd_corretores > 0 Then
''--------------------------------

    'Cod Susep
    
    'Luciana - 04/07/2003
    'Reg = Reg & Left(Format$(corretor_id(1), "000000-000") & Space(10), 10)
    
'MATHAYDE - 28/08/2009
'DEMANDA: 920234 - Emitindo SEGA com m�ltiplos corretores
Do While Not rc.EOF
    
    'montando detalhe 23 - CORRETORES
    Reg = "23" & Format(ContaLinhaAtual, "000000") & Format(num_proposta, "000000000")
    Reg = Replace(Reg, vbCr, vbNullString)
    Reg = Replace(Reg, vbLf, vbNullString)
    Reg = Replace(Reg, vbCrLf, vbNullString)
    
    'c�digo susep
    var_corretor_susep = Space(15 - Len(rc!Corretor_Susep)) & rc!Corretor_Susep
    Reg = Reg & Mid(var_corretor_susep, 1, 3) & "." & _
                Mid(var_corretor_susep, 4, 2) & "." & _
                Mid(var_corretor_susep, 6, 2) & "." & _
                Mid(var_corretor_susep, 8, 1) & "." & _
                Mid(var_corretor_susep, 9, 6) & "." & _
                Mid(var_corretor_susep, 15, 1)
    
    'Nome do corretor
    If Not IsNull(rc!nome) Then
       Reg = Reg & UCase(Left(rc!nome & Space(50), 50))
    Else
       Reg = Reg & Space(50)
    End If
    
''-------------------------------
'MATHAYDE - 28/08/2009
'DEMANDA: 920234 - comentado para aceitar mais de um corretor
'ElseIf qtd_corretores = 0 Then
'       Reg = Reg & Space(70)
'
'Else
'
''    Reg = Reg & Space(10)
''    Reg = Reg & Space(50)
'
'    Reg = Reg & "********************"
'    Reg = Reg & UCase(Left("(CONFORME ANEXO)" & Space(50), 50))
'
'End If
''-------------------------------

    'Preenchendo o arquivos
    Print #Arq1, Left(Reg + Space(tam_reg), tam_reg)
    ContaLinhaAtual = ContaLinhaAtual + 1
    
    rc.MoveNext
Loop


'MATHAYDE - 28/08/2009
rc.Close
Set rc = Nothing

Exit Sub

Erro:
    TrataErroGeral "Ler_Dados_Complementares", Me.name
    TerminaSEGBR
    
End Sub

Function TrocaTabPorEspaco(texto As String, no_espacos As Integer) As String
Dim cont_char As Variant
    
    For cont_char = 1 To Len(texto)
        If Mid(texto, cont_char, 1) = vbTab Then
            texto = Mid(texto, 1, cont_char - 1) & Space(no_espacos) & Mid(texto, cont_char + 1)
            cont_char = cont_char + no_espacos - 1
        End If
    Next
    
    TrocaTabPorEspaco = texto

End Function

Sub Conexao_auxiliar()
   
 On Error GoTo Erro
    
 With rdocn1
     .Connect = rdocn.Connect
     .CursorDriver = rdUseServer
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 With rdocn2
     .Connect = rdocn.Connect
     .CursorDriver = rdUseServer
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 
 With rdocn4 'Marcio.Nogueira - 10/08/2017 - SD00115860
     .Connect = rdocn.Connect
     .CursorDriver = rdUseNone
     .QueryTimeout = 3600
     .EstablishConnection rdDriverNoPrompt
 End With
 
 Exit Sub

Erro:
    MensagemBatch "Conex�o com BRCAPDB indispon�vel.", vbCritical
    TerminaSEGBR
    
End Sub

Private Sub Processa_Controle_Documento()
Dim PK As String, TABLE As String, CAMPOS As String
Dim rc As rdoResultset
   
On Error GoTo Erro
'Rafael Oshiro 09/08/2005 - Inlcuido LEFT JOIN com proposta_adesao_tb
'Sql = "SELECT c.nome, pf.cont_agencia_id, age.nome 'nome_agen', age.endereco 'endereco_agen', age.bairro 'bairro_agen', age.estado 'estado_agen', age.cep 'cep_agen', "
'Sql = Sql & " e.endereco, e.bairro, e.municipio, "
'Sql = Sql & " e.estado, e.cep "
'Sql = Sql & " FROM proposta_tb p  WITH (NOLOCK)   INNER JOIN cliente_tb c  WITH (NOLOCK)   "
'Sql = Sql & " ON (p.prop_cliente_id = c.cliente_id)"
'Sql = Sql & " INNER JOIN endereco_corresp_tb e  WITH (NOLOCK)  "
'Sql = Sql & " ON (p.proposta_id=e.proposta_id)"
'Sql = Sql & " LEFT JOIN proposta_fechada_tb pf  WITH (NOLOCK)  "
'Sql = Sql & " ON (pf.proposta_id=p.proposta_id)"
'Sql = Sql & " LEFT JOIN agencia_tb age  WITH (NOLOCK)  "
'Sql = Sql & " ON  (pf.cont_agencia_id = age.agencia_id)"
'Sql = Sql & " AND (pf.cont_banco_id = age.banco_id)"
'Sql = Sql & " WHERE  p.proposta_id = " & num_proposta

SQL = " SELECT  c.nome, " & vbNewLine
SQL = SQL & "   ISNULL(pf.cont_agencia_id, pa.cont_agencia_id) cont_agencia_id, " & vbNewLine
SQL = SQL & "   ISNULL(age.nome, age2.nome) 'nome_agen', " & vbNewLine
SQL = SQL & "   ISNULL(age.endereco, age2.endereco) 'endereco_agen', " & vbNewLine
SQL = SQL & "   ISNULL(age.bairro, age2.endereco) 'bairro_agen', " & vbNewLine
SQL = SQL & "   ISNULL(age.estado, age2.estado) 'estado_agen', " & vbNewLine
SQL = SQL & "   ISNULL(age.cep, age2.cep) 'cep_agen', " & vbNewLine
SQL = SQL & "   'municipio_agen' = mun.nome, " & vbNewLine
SQL = SQL & "   e.endereco, " & vbNewLine
SQL = SQL & "   e.bairro, " & vbNewLine
SQL = SQL & "   e.municipio, " & vbNewLine
SQL = SQL & "   e.estado, " & vbNewLine
SQL = SQL & "   e.cep " & vbNewLine
SQL = SQL & " FROM proposta_tb p  WITH (NOLOCK)   INNER JOIN cliente_tb c  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON (p.prop_cliente_id = c.cliente_id) " & vbNewLine
SQL = SQL & " INNER JOIN endereco_corresp_tb e  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON (p.proposta_id=e.proposta_id) " & vbNewLine
SQL = SQL & " LEFT JOIN proposta_fechada_tb pf  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON (pf.proposta_id=p.proposta_id) " & vbNewLine
SQL = SQL & " LEFT JOIN proposta_adesao_tb pa  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON (pa.proposta_id=p.proposta_id) " & vbNewLine
SQL = SQL & " LEFT JOIN agencia_tb age  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON  (pf.cont_agencia_id = age.agencia_id) " & vbNewLine
SQL = SQL & "   AND (pf.cont_banco_id = age.banco_id) " & vbNewLine
SQL = SQL & " LEFT JOIN agencia_tb age2  WITH (NOLOCK)  " & vbNewLine
SQL = SQL & "   ON  (pa.cont_agencia_id = age2.agencia_id) " & vbNewLine
SQL = SQL & "   AND (pa.cont_banco_id = age2.banco_id) " & vbNewLine
SQL = SQL & " LEFT JOIN municipio_tb mun  WITH (NOLOCK) "
SQL = SQL & "   ON mun.municipio_id = isnull(age.municipio_id, age2.municipio_id) "
SQL = SQL & " WHERE  p.proposta_id = " & num_proposta

Set rc = rdocn2.OpenResultset(SQL)

If Not rc.EOF Then
   
   If (Destino_id <> "A") Or (Not IsNull(rc!cont_agencia_id)) Then
   
        Reg = "10" & Format(ContaLinhaAtual, "000000") & num_proposta
        
        'Nome do destinat�rio
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!nome) Then
                 Reg = Reg & UCase(Left(rc!nome & Space(60), 60))
             Else
                 Reg = Reg & Space(60)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left(Diretoria_id & Space(60), 60))
        Else
             Reg = Reg & UCase(Left("AG�NCIA: " & rc!nome_agen & Space(60), 60))
        End If
        'Endere�o
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Endereco) Then
                 'Reg = Reg & UCase(Left(("" & rc!Endereco) & Space(60), 60))
                 Reg = Reg & UCase(Left(("" & rc!Endereco) & Space(200), 200)) 'Sala �gil Sprint 17 - rfarzat 14/02/2019
             Else
                 'Reg = Reg & Space(60)
                 Reg = Reg & Space(200) 'Sala �gil Sprint 17 - rfarzat 14/02/2019
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("RUA MANOEL DA NOBREGA, 1280 - 8�/9� ANDAR" & Space(60), 60))
        Else
             Reg = Reg & UCase(Left(("" & rc!endereco_agen) & Space(60), 60))
        End If
        'Bairro
        If Destino_id = "" Or Destino_id = "C" Then
             If Trim("" & rc!Bairro) <> "" Then
                 Reg = Reg & UCase(Left(rc!Bairro & Space(30), 30))
             Else
                 Reg = Reg & Space(30)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("PARA�SO" & Space(30), 30))
        Else
             Reg = Reg & UCase(Left(rc!bairro_agen & Space(30), 30))
        End If
        'Municipio
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Municipio) Then
                 Reg = Reg & UCase(Left(rc!Municipio & Space(30), 30))
             Else
                 Reg = Reg & Space(30)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & UCase(Left("S�O PAULO" & Space(30), 30))
        Else
             Reg = Reg & UCase(Left(rc!municipio_agen & Space(30), 30))
        End If
        'UF
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Estado) Then
                 Reg = Reg & UCase(Left(rc!Estado & Space(2), 2))
             Else
                 Reg = Reg & "  "
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & "SP"
        Else
             Reg = Reg & UCase(Left(rc!estado_agen & Space(2), 2))
        End If
        'CEP
        If Destino_id = "" Or Destino_id = "C" Then
             If Not IsNull(rc!Cep) And Trim(rc!Cep) <> "" Then
                'Ricardo Toledo : Confitec : 09/09/2010
                'N�o est� formatando corretamente quando o CEP tem menos de 08 posi��es. O correto � inserir zeros � frente
                 'Reg = Reg & Left(Format$(rc!Cep, "&&&&&-&&&") & Space(9), 9)
                 Reg = Reg & Left(Format$(rc!Cep, "00000-000") & Space(9), 9)
             Else
                 Reg = Reg & Space(9)
             End If
        ElseIf Destino_id = "D" Then
             Reg = Reg & "04001-004"
        Else
             'Ricardo Toledo : Confitec : 09/09/2010
             'N�o est� formatando corretamente quando o CEP tem menos de 08 posi��es. O correto � inserir zeros � frente
             'Reg = Reg & Left(Format$(rc!cep_agen, "&&&&&-&&&") & Space(9), 9)
             Reg = Reg & Left(Format$(rc!cep_agen, "00000-000") & Space(9), 9)
        End If
        'Aos Cuidados de
        If Destino_id = "" Or Destino_id = "C" Then
             Reg = Reg & "A/C" & Space(60)
        ElseIf Destino_id = "D" Then
             Reg = Reg & "A/C" & Space(60)
        Else
             Reg = Reg & "A/C" & Space(60)
        End If
        'Referencia
        Reg = Reg & "Ref."
        'Nome do segurado
        If Not IsNull(rc!nome) Then
            Reg = Reg & UCase(Left(rc!nome & Space(60), 60))
        Else
            Reg = Reg & Space(60)
        End If
        'C�digo de barras ddpppppppppjjjjjaaaa
        If TpEmissao = "A" Then
            Reg = Reg & "04" 'Tipo_documento ap�lice
        ElseIf TpEmissao = "E" Then
            Reg = Reg & "06" 'Tipo_documento endosso
        End If
        Reg = Reg & num_proposta         'Proposta_id
        Reg = Reg & ConverteParaJulianDate(CDate(Data_Sistema))       'data
        'C�digo da ag�ncia
        If Not IsNull(rc!cont_agencia_id) And Trim(rc!cont_agencia_id) <> "" Then
           Reg = Reg & Format(rc!cont_agencia_id, "0000")         'C�digo da ag�ncia
        Else
           Reg = Reg & "0000"
        End If
        'N�mero da AR
        Reg = Reg & Space(15)
        
        flagEnderecoAgencia = True
   Else
        flagEnderecoAgencia = False
        rc.Close
        Exit Sub
   End If
End If
rc.Close

Reg = Left(Reg & Space(tam_reg), tam_reg)
Print #Arq1, Reg
ContaLinhaAtual = ContaLinhaAtual + 1
'Leandro A. Souza - Stefanini IT - flow 139830 -> p/ evitar deslocamento no arquivo
If ContaLinhaAtual > 999999 Then ContaLinhaAtual = 1


Exit Sub

Erro:
   TrataErroGeral "Processa_Controle_Documento", Me.name
   Unload Me
   Call TerminaSEGBR
   End
End Sub

Public Sub Processa_Dados_Gerais()

On Error GoTo Erro

'Gravando inicio do detalhe e dados do cliente'''''''''''''''''''''''''''''''''''''''''''
DoEvents
Ler_Cliente

'Gravando dados do produto ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'Reg = Reg & Format$(ProdutoId, "000")
Reg = Reg & Format$(ProdutoId, "0000")
Reg = Reg & UCase(Left(NomeProduto & Space(33), 33))

'Preenchendo com espa�os o endere�o de risco (Vida n�o tem endere�o de risco)

Reg = Reg & Space(112)

'Gravando o extenso das datas de vigencia'''''''''''''''''''''''''''''''''''''''''''

Ler_Vigencia
DoEvents
'Preenchendo com espa�os o Limite de Responsabilidade (Para vida esta mensagem n�o �
'impressa)

Reg = Reg & Space(100)

'Gravando Nr. do endosso'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Reg = Reg & num_endosso

'Gravando dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

If ProdutoId <> 15 Then
   Ler_Proposta_Fechada
Else
   Ler_Proposta_Adesao_OuroVidaEmp
End If
DoEvents


'Vida n�o tem modalidade!!
'Obt�m descri��o do ramo e da modalidade'''''''''''''''''''''''''''''''''''''''''''''

Ler_RamoModalidade

'Gravando dados Corretor

'MATHAYDE - 28/08/2009
'DEMANDA: 920234 - Comentado para aceitar m�ltiplos resseguradores
'Ler_Dados_Complementares ''Acertar esta rotina

DoEvents
'Inspetoria

Reg = Reg & String(9, "*")

'Obs.: Para Vida, n�o colocar observa��es de 1 a 5, apenas a 6� ser� impressa

Reg = Reg & String(124, " ")    'Obs. 1
Reg = Reg & String(124, " ")    'Obs. 2
Reg = Reg & String(124, " ")    'Obs. 3
Reg = Reg & String(124, " ")    'Obs. 4
Reg = Reg & String(124, " ")    'Obs. 5

'Obs.: 6

Reg = Reg & Left("S�o Paulo, " & DataExtenso(DtEmissao) & Space(40), 40) & Space(20)
If Trim(processo_susep) <> "" Then
    Reg = Reg & "Processo SUSEP : " & Left(processo_susep & Space(47), 47)
Else
    Reg = Reg & Space(64)
End If

'Atividade

Reg = Reg & String(20, "*")

'agin - 20/12/2004
LerGrupoRamo

'Completa com espa�os
Reg = Left(Reg & Space(tam_reg), tam_reg)

'Imprimindo registro no arquivo''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Print #Arq1, Reg

ContaLinhaAtual = ContaLinhaAtual + 1
DoEvents

 Exit Sub

Erro:
    MensagemBatch "Processa_dados_gerais", vbCritical
    TerminaSEGBR

End Sub

Public Sub Ler_CodBarras_Retorno()

   On Error GoTo Erro

   'Jorfilho 15/08/2002 - Novo c�digo de barras com 20 posi��es
   If TpEmissao = "A" Then
      Reg = Reg & "04" 'Tipo_documento
      Reg = Reg & num_proposta 'Proposta_id
      Reg = Reg & ConverteParaJulianDate(CDate(Data_Sistema))
      Reg = Reg & ContAgencia
      Reg = Reg & Space(10) 'jorfilho - 07/08/2001 - Atributo: mensagem
   End If

   Exit Sub

Erro:
   MensagemBatch "Ler_CodBarras_Retorno", vbCritical
   TerminaSEGBR

End Sub

Public Sub Ler_RamoModalidade()

Dim RamoModalidade As String

On Error GoTo Erro

SQL = "SELECT Nome "
SQL = SQL & "FROM ramo_tb  WITH (NOLOCK)   "
SQL = SQL & "WHERE ramo_id = " & ramo_id
'
Set rc = rdocn.OpenResultset(SQL)
If Not rc.EOF Then
    'madorno - 11/01/2005
    'RamoModalidade = Format(ramo_id, "000") & " " & Trim("" & rc!nome)
   RamoModalidade = Left(Format(ramo_id, "00") & Space(3), 3)
   RamoModalidade = RamoModalidade & " " & Trim("" & rc!nome)
Else
   RamoModalidade = ""
End If
rc.Close

Reg = Reg & UCase(Left(Trim(RamoModalidade) & Space(63), 63))

''Sql = "SELECT distinct m.modalidade_seguro_id, m.nome FROM "
''Sql = Sql & "modalidade_seguro_tb m  WITH (NOLOCK)   INNER JOIN  subramo_tb s  WITH (NOLOCK)   "
''Sql = Sql & "ON (m.modalidade_seguro_id = s.modalidade_seguro_id "
''Sql = Sql & "AND m.ramo_id = s.ramo_id) "
''Sql = Sql & "WHERE s.dt_fim_vigencia_sbr is null "
''Sql = Sql & " AND s.ramo_id = " & ramo_id
''Sql = Sql & " AND s.subramo_id = " & Subramo
'
''Set rc = rdocn.OpenResultset(Sql)
''If Not rc.EOF Then
''    If RamoModalidade <> "" And Trim("" & rc!Nome) <> "" Then
''        RamoModalidade = RamoModalidade & " - "
''    End If
''    RamoModalidade = RamoModalidade & Trim("" & rc!Nome)
''    'Reg = Reg & Ramo
''End If
''rc.Close
''Reg = Reg & UCase(Left(Trim(RamoModalidade) & Space(70), 70))

Exit Sub

Erro:
   TrataErroGeral "Ler_RamoModalidade", Me.name
   TerminaSEGBR
    
End Sub

Public Sub Ler_CoberturasTotIS()
Dim CodObjAnterior As Long, i As Long, PercFranquia As Double, TotIS As Double, vStrTotIs As String
ReDim Cobertura(7, 17)

On Error GoTo Erro
QtdCoberturas = 0: QtdLinhasCobertura = 0
TotIS = 0
If (ramo_id = "22" Or ramo_id = "44") And ProdutoId <> 400 Then
   ''TranspInternacional = True
   ''Ler_TransporteInternacional
Else
   TranspInternacional = False
   If TabEscolha = "" Then
      SQL = Monta_SqlCoberturas
   Else
      SQL = "SELECT e.tp_cobertura_id, c.nome, e.val_is, "
      SQL = SQL & "e.fat_taxa, e.fat_desconto_tecnico, e.val_premio, "
      SQL = SQL & "e.ramo_id,  e.val_min_franquia,  e.texto_franquia, e.fat_franquia, e.acumula_is,  "
      SQL = SQL & "e.dt_inicio_vigencia_esc, e.dt_fim_vigencia_esc, cod_objeto_segurado "
      SQL = SQL & "FROM " & TabEscolha & " e  WITH (NOLOCK)  , tp_cobertura_tb c   WITH (NOLOCK)  "
      SQL = SQL & "WHERE  (c.tp_cobertura_id = e.tp_cobertura_id) AND "
      SQL = SQL & "       (e.proposta_id = " & num_proposta & ")  AND "
      SQL = SQL & "       (e.produto_id = " & ProdutoId & ")      AND "
      If TpEmissao = "A" Then
         SQL = SQL & " (e.num_endosso=0 OR e.num_endosso is null ) "
      Else
         SQL = SQL & " e.num_endosso=" & num_endosso
         SQL = SQL & " AND dt_fim_vigencia_esc is null "
      End If
   End If
   CodObjAnterior = 0
   Set rc = rdocn.OpenResultset(SQL)
   Do While Not rc.EOF
      'Obtendo Limite de Responsabilidade
      If UCase("" & rc!acumula_is) = "S" Then
         TotIS = TotIS + Val(0 & rc!val_is)
      End If
      If Val(0 & rc!cod_objeto_segurado) <> CodObjAnterior Then
         'Contando com t�tulo, local do risco, t�tulo coberturas e espa�o itens
         QtdLinhasCobertura = QtdLinhasCobertura + 4
         CodObjAnterior = Val(0 & rc!cod_objeto_segurado)
      End If
      Cobertura(0, QtdCoberturas) = Val(0 & rc!cod_objeto_segurado)
      Cobertura(1, QtdCoberturas) = Val(0 & rc!Tp_Cobertura_Id)
      Cobertura(2, QtdCoberturas) = "" & rc!nome
      Cobertura(3, QtdCoberturas) = Val(0 & rc!val_is)
      If Val(0 & rc!fat_franquia) <> 0 Then
         'PercFranquia = (1 - Val(0 & rc!fat_franquia)) * 100
         PercFranquia = Val(0 & rc!fat_franquia) * 100
      Else
         PercFranquia = 0
      End If
      Cobertura(4, QtdCoberturas) = PercFranquia                  'Perc franquia
      Cobertura(5, QtdCoberturas) = Trim("" & rc!texto_franquia)  'Texto franquia
      Cobertura(6, QtdCoberturas) = Val(0 & rc!val_min_franquia)  'M�n. franquia
      If PercFranquia <> 0 Or Cobertura(5, QtdCoberturas) <> "" Or Cobertura(6, QtdCoberturas) <> "0" Then
         QtdLinhasCobertura = QtdLinhasCobertura + 1
      End If
      
      If QtdCoberturas Mod 17 = 0 Then
         ReDim Preserve Cobertura(7, QtdCoberturas + 17)
      End If
      QtdCoberturas = QtdCoberturas + 1
      QtdLinhasCobertura = QtdLinhasCobertura + 1
      i = i + 1
      rc.MoveNext
   Loop
   rc.Close
End If

' Limite de responsabilidade
If Not ConfiguracaoBrasil Then
   vStrTotIs = TrocaValorAmePorBras(Format(TotIS, "###,###,##0.00"))
Else
   vStrTotIs = Format(TotIS, "###,###,##0.00")
End If
If TpEmissao = "A" And Not TranspInternacional Then
   Reg = Reg & Left("LIMITE MAXIMO DE REPONSABILIDADE DA APOLICE :  R$ " & vStrTotIs & Space(100), 100)
Else
   Reg = Reg & Space(100)
End If

Exit Sub

Erro:
   'TrataErroGeral "Erro na estrutura dos dados para proposta " & num_proposta
   TrataErroGeral "Ler_CoberturasTotIS", Me.name
   TerminaSEGBR
   
End Sub

Function ObterNumRemessa(nome As String, ByRef NumRemessa As String) As Variant

Dim SQL As String
Dim rcNum As rdoResultset
Dim vObterNumRemessa() As Integer
ReDim vObterNumRemessa(0 To 1)
On Error GoTo Erro
    
    SQL = " SELECT"
    SQL = SQL & "     l.layout_id"
    SQL = SQL & " FROM"
    SQL = SQL & "     controle_proposta_db..layout_tb l  WITH (NOLOCK)  "
    SQL = SQL & " WHERE"
    SQL = SQL & "     l.nome = '" & nome & "'"
    Set rcNum = rdocn2.OpenResultset(SQL)
    
        If rcNum.EOF Then
           Error 1000
        Else
        vObterNumRemessa(0) = rcNum(0)
        
        'carolina.marinho 05/09/2010 - Tratamento ABS
        SQL = ""
        SQL = SQL & "SELECT max(versao)" & vbNewLine
        SQL = SQL & "  FROM (" & vbNewLine
        SQL = SQL & "        SELECT versao = isnull(max(a.versao), 0)" & vbNewLine
        SQL = SQL & "          FROM controle_proposta_db..arquivo_versao_gerado_tb a" & vbNewLine
        SQL = SQL & "         WHERE a.layout_id = " & rcNum(0) & vbNewLine
        SQL = SQL & "         UNION " & vbNewLine
        SQL = SQL & "        SELECT versao = isnull(max(b.versao), 0)" & vbNewLine
        SQL = SQL & "          FROM abss.controle_proposta_db.dbo.arquivo_versao_gerado_tb b" & vbNewLine
        SQL = SQL & "         WHERE b.layout_id = " & rcNum(0) & vbNewLine
        SQL = SQL & "       ) AS T" & vbNewLine
'        Sql = "       SELECT"
'        Sql = Sql & "     isnull(max(a.versao), 0)"
'        Sql = Sql & " FROM"
'        Sql = Sql & "     controle_proposta_db..arquivo_versao_gerado_tb a  WITH (NOLOCK)  "
'        Sql = Sql & " WHERE"
'        Sql = Sql & "     a.layout_id = " & rcNum(0)
'
        rcNum.Close
        
        Set rcNum = rdocn2.OpenResultset(SQL)
        
            If Not rcNum.EOF Then
               NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "0000"), Format(rcNum(0) + 1, "0000"))
               ObterNumRemessa = NumRemessa
            Else
               ObterNumRemessa = Nothing
            End If
        End If
    
    rcNum.Close

Exit Function

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "ObterNumRemessa", nome
    TerminaSEGBR

End Function

Sub Obtem_Num_Remessa(nome As String, ByRef NumRemessa As String)

Dim SQL As String
Dim rcNum As rdoResultset

On Error GoTo Erro

SQL = SQL & " SELECT"
SQL = SQL & "     l.layout_id"
SQL = SQL & " FROM"
SQL = SQL & "     controle_proposta_db..layout_tb l  WITH (NOLOCK)  "
SQL = SQL & " WHERE"
SQL = SQL & "     l.nome = '" & nome & "'"
Set rcNum = rdocn.OpenResultset(SQL)

If rcNum.EOF Then
   Error 1000
Else

'carolina.marinho 05/09/2010 - Tratamento ABS
SQL = ""
SQL = SQL & "SELECT max(versao)" & vbNewLine
SQL = SQL & "  FROM (" & vbNewLine
SQL = SQL & "        SELECT versao = isnull(max(a.versao), 0)" & vbNewLine
SQL = SQL & "          FROM controle_proposta_db..arquivo_versao_gerado_tb a" & vbNewLine
SQL = SQL & "         WHERE a.layout_id = " & rcNum(0) & vbNewLine
SQL = SQL & "         UNION " & vbNewLine
SQL = SQL & "        SELECT versao = isnull(max(b.versao), 0)" & vbNewLine
SQL = SQL & "          FROM abss.controle_proposta_db.dbo.arquivo_versao_gerado_tb b" & vbNewLine
SQL = SQL & "         WHERE b.layout_id = " & rcNum(0) & vbNewLine
SQL = SQL & "       ) AS T" & vbNewLine

'Sql = "       SELECT"
'Sql = Sql & "     max(a.versao)"
'Sql = Sql & " FROM"
'Sql = Sql & "     controle_proposta_db..arquivo_versao_gerado_tb a  WITH (NOLOCK)  "
'Sql = Sql & " WHERE"
'Sql = Sql & "     a.layout_id = " & rcNum(0)

rcNum.Close

Set rcNum = rdocn.OpenResultset(SQL)

If Not rcNum.EOF Then
   NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "000000"), Format(rcNum(0) + 1, "000000"))
End If

End If

rcNum.Close

Exit Sub

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "Obtem_Num_Remessa", Me.name
    TerminaSEGBR

End Sub

Public Function InserirArquivoVersaoGerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    On Error GoTo Erro
            
    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    SQL = SQL & nome & "'," & NumRemessa & "," & qReg & ",'"
    SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    SQL = SQL & cUserName & "'"
    rdocn.Execute (SQL)
    
    Exit Function

Erro:
    TrataErroGeral "InserirArquivoVersaoGerado", nome
    TerminaSEGBR
    
End Function

Sub Insere_Arquivo_Versao_Gerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    Dim rcGer As rdoResultset
    Dim SQL As String
    
    On Error GoTo Erro
            
    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    SQL = SQL & nome & "'," & NumRemessa & "," & qReg & ",'"
    SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    SQL = SQL & cUserName & "'"
    Set rcGer = rdocn.OpenResultset(SQL)
        
    rcGer.Close
        
    Exit Sub

Erro:
    TrataErroGeral "Insere_Arquivo_Versao_Gerado", Me.name
    TerminaSEGBR
    
End Sub

Public Function ConverteParaJulianDate(ldate As Date) As String
Dim lJulianDate  As String * 5
   On Error GoTo Erro

lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
ConverteParaJulianDate = lJulianDate

  Exit Function

Erro:
   TrataErroGeral "ConverteParaJulianDate", Me.name
   TerminaSEGBR
End Function

'agin - 20/12/2004
Public Sub LerGrupoRamo()

Dim rc As rdoResultset
Dim sSQL As String
    
On Error GoTo TratarErro
        
    sSQL = ""
    sSQL = sSQL & "SELECT ISNULL(ramo_tb.grupo_ramo, '00') AS grupo_ramo " & vbNewLine
    sSQL = sSQL & "  FROM ramo_tb   WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & " WHERE ramo_tb.ramo_id = " & ramo_id
    
    Set rc = rdocn.OpenResultset(sSQL)
    
    If Not rc.EOF Then
        Reg = Reg & Left(Format(rc!grupo_ramo, "00") & Space(2), 2)
    Else
        Call MensagemBatch("Grupo Ramo n�o encontrado!!!")
        Call TerminaSEGBR
    End If
    
    rc.Close
    Set rc = Nothing
    
    Exit Sub

TratarErro:
    Call TrataErroGeral("LerGrupoRamo", Me.name)
    Call TerminaSEGBR

End Sub



Private Function Valida_produtoRE(vprod As Integer) As Boolean
'Demanda 15740031 - Edilson Silva - 27/09/2012
Valida_produtoRE = False
    If vprod <> 8 And vprod <> 9 And vprod <> 10 And vprod <> 100 And vprod <> 104 _
            And vprod <> 105 And vprod <> 106 And vprod <> 107 And vprod <> 108 And vprod <> 109 _
            And vprod <> 111 And vprod <> 112 And vprod <> 113 And vprod <> 114 And vprod <> 116 _
            And vprod <> 117 And vprod <> 118 And vprod <> 119 And vprod <> 120 And vprod <> 220 _
            And vprod <> 400 And vprod <> 670 And vprod <> 709 And vprod <> 710 And vprod <> 711 _
            And vprod <> 719 And vprod <> 777 And vprod <> 800 And vprod <> 809 And vprod <> 810 _
            And vprod <> 811 And vprod <> 900 And vprod <> 1002 And vprod <> 1016 And vprod <> 1017 _
            And vprod <> 1021 And vprod <> 1038 And vprod <> 1123 And vprod <> 1125 And vprod <> 1141 _
            And vprod <> 1146 And vprod <> 1147 And vprod <> 1148 And vprod <> 1149 And vprod <> 1150 _
            And vprod <> 1151 And vprod <> 1162 And vprod <> 1167 And vprod <> 1176 And vprod <> 1178 _
            And vprod <> 1184 And vprod <> 1185 And vprod <> 1188 And vprod <> 1195 Then
        Valida_produtoRE = True
    End If
End Function
