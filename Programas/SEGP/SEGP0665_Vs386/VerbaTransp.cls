VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "VerbaTransp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Private mvarDescr As String 'local copy
Private mvarValIs As String 'local copy
Public Property Let ValIs(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ValIs = 5
    mvarValIs = vData
End Property


Public Property Get ValIs() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ValIs
    ValIs = mvarValIs
End Property



Public Property Let Descr(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Descr = 5
    mvarDescr = vData
End Property


Public Property Get Descr() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Descr
    Descr = mvarDescr
End Property



