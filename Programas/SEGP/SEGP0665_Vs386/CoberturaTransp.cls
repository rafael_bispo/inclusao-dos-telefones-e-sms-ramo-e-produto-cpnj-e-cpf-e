VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CoberturaTransp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"Verba"
Attribute VB_Ext_KEY = "Member1" ,"Verba"
'local variable(s) to hold property value(s)
Private mvarMeioTransporte As String 'local copy
Private mvarPaisProcedencia As String 'local copy
Private mvarDestinoFinal As String 'local copy
Private mvarMercadoria As String 'local copy
Private mvarEmbalagem As String 'local copy
Private mvarCobertura As String 'local copy
Private mvarFranquia As String 'local copy
Private mvarNumDoc As String 'local copy
Private mvarTaxa As String 'local copy
Private mvarObjSegurado As String 'local copy
Private mvarVerba As Verba
Private mvarMercadorias As Mercadorias



Public Property Get Mercadorias() As Mercadorias
    If mvarMercadorias Is Nothing Then
        Set mvarMercadorias = New Mercadorias
    End If


    Set Mercadorias = mvarMercadorias
End Property


Public Property Set Mercadorias(vData As Mercadorias)
    Set mvarMercadorias = vData
End Property



Public Property Get Verbas() As Verba
    If mvarVerba Is Nothing Then
        Set mvarVerba = New Verba
    End If


    Set Verbas = mvarVerba
End Property


Public Property Set Verbas(vData As Verba)
    Set mvarVerba = vData
End Property
Private Sub Class_Terminate()
  Set mvarMercaldorias = Nothing
  Set mvarMercadorias = Nothing
    Set mvarVerba = Nothing
End Sub









Public Property Let ObjSegurado(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ObjSegurado = 5
    mvarObjSegurado = vData
End Property


Public Property Get ObjSegurado() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ObjSegurado
    ObjSegurado = mvarObjSegurado
End Property



Public Property Let Taxa(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Taxa = 5
    mvarTaxa = vData
End Property


Public Property Get Taxa() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Taxa
    Taxa = mvarTaxa
End Property



Public Property Let NumDoc(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumDoc = 5
    mvarNumDoc = vData
End Property


Public Property Get NumDoc() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumDoc
    NumDoc = mvarNumDoc
End Property



Public Property Let Franquia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Franquia = 5
    mvarFranquia = vData
End Property


Public Property Get Franquia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Franquia
    Franquia = mvarFranquia
End Property



Public Property Let Cobertura(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Cobertura = 5
    mvarCobertura = vData
End Property


Public Property Get Cobertura() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cobertura
    Cobertura = mvarCobertura
End Property



Public Property Let Embalagem(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Embalagem = 5
    mvarEmbalagem = vData
End Property


Public Property Get Embalagem() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Embalagem
    Embalagem = mvarEmbalagem
End Property



Public Property Let Mercadoria1(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Mercadoria = 5
    mvarMercadoria = vData
End Property


Public Property Get Mercadoria1() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Mercadoria
    Mercadoria1 = mvarMercadoria
End Property



Public Property Let DestinoFinal(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DestinoFinal = 5
    mvarDestinoFinal = vData
End Property


Public Property Get DestinoFinal() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DestinoFinal
    DestinoFinal = mvarDestinoFinal
End Property



Public Property Let PaisProcedencia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PaisProcedencia = 5
    mvarPaisProcedencia = vData
End Property


Public Property Get PaisProcedencia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PaisProcedencia
    PaisProcedencia = mvarPaisProcedencia
End Property



Public Property Let MeioTransporte(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.MeioTransporte = 5
    mvarMeioTransporte = vData
End Property


Public Property Get MeioTransporte() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MeioTransporte
    MeioTransporte = mvarMeioTransporte
End Property








Public Property Let DescVerba(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.FOB = 5
    mvarFOB = vData
End Property


Public Property Get DescVerba() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.FOB
    DescVerba = mvarFOB
End Property



