VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Pagamento"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarProposta As String 'local copy
Private mvarNumCobranca As String 'local copy
Private mvarNumVia As String 'local copy
Public Property Let NumVia(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumVia = 5
    mvarNumVia = vData
End Property


Public Property Get NumVia() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumVia
    NumVia = mvarNumVia
End Property



Public Property Let NumCobranca(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NumCobranca = 5
    mvarNumCobranca = vData
End Property


Public Property Get NumCobranca() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NumCobranca
    NumCobranca = mvarNumCobranca
End Property



Public Property Let Proposta(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Proposta = 5
    mvarProposta = vData
End Property


Public Property Get Proposta() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Proposta
    Proposta = mvarProposta
End Property



