Attribute VB_Name = "Conexao_auxiliar"
'# declara��o de conex�o auxiliar
Public ConnRdoAux                      As New rdoConnection
Public rdocn1                          As New rdoConnection
Public rdocn2                          As New rdoConnection
Public rdocn3                          As New rdoConnection
Public rdocn4                          As New rdoConnection



Sub ConexaoAuxiliar()

On Error GoTo Erro

    With ConnRdoAux
      .Connect = rdocn.Connect
      .CursorDriver = rdUseServer
      .QueryTimeout = 60000
      .EstablishConnection rdDriverNoPrompt
    End With

    With rdocn1
      .Connect = rdocn.Connect
      .CursorDriver = rdUseServer
      .QueryTimeout = 60000
      .EstablishConnection rdDriverNoPrompt
    End With

    With rdocn2
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 60000
        .EstablishConnection rdDriverNoPrompt
    End With
    
    With rdocn3
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 60000
        .EstablishConnection rdDriverNoPrompt
    End With
    
    With rdocn4
        .Connect = rdocn.Connect
        .CursorDriver = rdUseServer
        .QueryTimeout = 60000
        .EstablishConnection rdDriverNoPrompt
    End With
    
   Exit Sub

Erro:
    MensagemBatch "Conex�o indispon�vel.", vbCritical
    TerminaSEGBR

End Sub
