VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Begin VB.Form frmGeraArqCertWeb 
   Caption         =   "SEGA8039"
   ClientHeight    =   3825
   ClientLeft      =   2025
   ClientTop       =   2295
   ClientWidth     =   8235
   LinkTopic       =   "Form1"
   ScaleHeight     =   3825
   ScaleWidth      =   8235
   Begin VB.Frame frame1 
      Caption         =   "Dados de Gera��o"
      Height          =   2415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8175
      Begin VB.Label label4 
         AutoSize        =   -1  'True
         Caption         =   "Caminho:"
         Height          =   195
         Left            =   240
         TabIndex        =   8
         Top             =   1845
         Width           =   660
      End
      Begin VB.Label lblStatus 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   1110
         TabIndex        =   7
         Top             =   1395
         Width           =   75
      End
      Begin VB.Label lblVersao 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   1110
         TabIndex        =   6
         Top             =   930
         Width           =   75
      End
      Begin VB.Label lblArquivo 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   1110
         TabIndex        =   5
         Top             =   480
         Width           =   75
      End
      Begin VB.Label label3 
         AutoSize        =   -1  'True
         Caption         =   "Status:"
         Height          =   195
         Left            =   270
         TabIndex        =   4
         Top             =   1395
         Width           =   495
      End
      Begin VB.Label label2 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o:"
         Height          =   195
         Left            =   270
         TabIndex        =   3
         Top             =   930
         Width           =   540
      End
      Begin VB.Label label1 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo:"
         Height          =   195
         Left            =   270
         TabIndex        =   2
         Top             =   480
         Width           =   585
      End
      Begin VB.Label lblCaminho 
         AutoSize        =   -1  'True
         Height          =   195
         Left            =   1110
         TabIndex        =   1
         Top             =   1845
         Width           =   75
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   300
      Left            =   0
      TabIndex        =   9
      Top             =   3525
      Width           =   8235
      _ExtentX        =   14526
      _ExtentY        =   529
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.Toolbar TlbSair 
      Height          =   2430
      Left            =   6480
      TabIndex        =   10
      Top             =   2640
      Width           =   1545
      _ExtentX        =   2725
      _ExtentY        =   4286
      ButtonWidth     =   1270
      ButtonHeight    =   1429
      Style           =   1
      ImageList       =   "ImlLista"
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   3
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "��O&K��"
            Key             =   "BtnOK"
            ImageIndex      =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Caption         =   "��Sai&r��"
            Key             =   "BtnSair"
            ImageIndex      =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   225
      Left            =   720
      TabIndex        =   11
      Top             =   2760
      Visible         =   0   'False
      Width           =   5415
      _ExtentX        =   9551
      _ExtentY        =   397
      _Version        =   393216
      Appearance      =   1
      Min             =   1e-4
      Scrolling       =   1
   End
   Begin MSComctlLib.ImageList ImlLista 
      Left            =   0
      Top             =   2640
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   12
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":0000
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":005E
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":00BC
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":011A
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":0178
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":01D6
            Key             =   ""
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":0234
            Key             =   ""
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":0292
            Key             =   ""
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":02F0
            Key             =   ""
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":034E
            Key             =   ""
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":03AC
            Key             =   ""
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGeraArqCertWeb.frx":040A
            Key             =   ""
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmGeraArqCertWeb"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const ARQ_CERTIF = "SEGA8039"

Private Sub MontaTrailler(ByVal PArquivo As Integer, ByVal pLinhas As Long)
On Error GoTo Erro_Trailler

Dim sTrailler As String * 8

    sTrailler = Space(8)
    Mid(sTrailler, 1, 2) = "99"
    Mid(sTrailler, 3, 6) = Format(pLinhas, "000000")

    Print #PArquivo, sTrailler
    
    Exit Sub
    
Erro_Trailler:
    TrataErroGeral "Monta Trailler", Me.name
    TerminaSEGBR
    
End Sub

Private Function calcula_mod11(ByVal Parte As String) As String
 
Dim i As Integer
'** Cobran�a
 
     Dim Peso As Integer
     Dim Soma As Integer
     Dim Parcela As Integer
     Dim dv As Integer
     
     Peso = 2
     Soma = 0
     For i = Len(Parte) To 1 Step -1
       If i <> 5 Then
          Parcela = Peso * Val(Mid(Parte, i, 1))
          Soma = Soma + Parcela
          Peso = Peso + 1
          If Peso > 9 Then Peso = 2
       End If
     Next i
    
     dv = 11 - (Soma Mod 11)
     If dv = 10 Or dv = 11 Then dv = 1
     calcula_mod11 = Format(dv, "0")

End Function

Private Function calcula_mod10(ByVal Parte As String) As String
Dim i As Integer, dv As Long

'** Cobran�a
 
    Dim Peso As Integer
    Dim Soma As Integer
    Dim Parcela As Integer
    
    Peso = 2
    Soma = 0
    For i = Len(Parte) To 1 Step -1
      Parcela = Peso * Val(Mid(Parte, i, 1))
      If Parcela > 9 Then
         Parcela = Val(Mid(Format(Parcela, "00"), 1, 1)) + Val(Mid(Format(Parcela, "00"), 2, 1))
      End If
      Soma = Soma + Parcela
      If Peso = 2 Then Peso = 1 Else Peso = 2
    Next i
    
    dv = 10 - (Soma Mod 10)
    If dv > 9 Then dv = 0
    calcula_mod10 = Format(dv, "0")

End Function

Private Sub Montar_linha_digitavel(ByVal Nosso_Numero As String, ByVal agencia As String, ByVal Codigo_Cedente As Variant, ByVal Carteira As String, ByVal dt_agendamento As Variant, ByVal Val_Cobranca As Double, ByRef codigo_barras As String, ByRef linha_digitavel As String)
 
Dim Parte1                                     As String
Dim Parte2                                     As String
Dim Parte3                                     As String
Dim Dv1                                        As String
Dim Dv2                                        As String
Dim Dv3                                        As String
Dim Dv_geral                                   As String
Dim Codigo_barras_1                            As String
Dim Codigo_barras_2                            As String
Dim Codigo_barras_3                            As String
       
    Parte1 = "0019" 'c�digo banco + dv
    Parte1 = Parte1 & Left(Nosso_Numero, 1) _
           & Mid(Nosso_Numero, 2, 4)
    Dv1 = calcula_mod10(Parte1)
    '' Deixar a linha sem formata��o para envio � gr�fica
    'Parte1 = Mid(Parte1, 1, 5) & "." & Mid(Parte1, 6, 4) & Dv1
    Parte1 = Parte1 & Dv1
    
    Parte2 = Mid(Nosso_Numero, 6, 5) _
           & Mid(Nosso_Numero, 11, 1) _
           & Left(agencia, 4)
    Dv2 = calcula_mod10(Parte2)
    '' Deixar a linha sem formata��o para envio � gr�fica
    'Parte2 = Mid(Parte2, 1, 5) & "." & Mid(Parte2, 6, 5) & Dv2
    Parte2 = Parte2 & Dv2
    
    'Parte3 = "00405200"  'conta cedente
    Parte3 = Left(Codigo_Cedente, 8)
    Parte3 = Parte3 & Left(Carteira, 2)
    Dv3 = calcula_mod10(Parte3)
    '' Deixar a linha sem formata��o para envio � gr�fica
    'Parte3 = Mid(Parte3, 1, 5) & "." & Mid(Parte3, 6, 5) & Dv3
    Parte3 = Parte3 & Dv3
    
    Codigo_barras_1 = "0019" 'c�digo banco + dv
    ' acrescentado a diferen�a de dias entre a data de vencimento e (7/10/97)
    ' para a forma��o da linha digit�vel -- Jo�o Mac-Cormick em 19/3/2001
    Dim Fator
    Fator = Format(DateDiff("d", "07/10/1997", dt_agendamento), "0000")
    Codigo_barras_2 = Fator & Format(Val_Cobranca * 100, "0000000000")
    
    Codigo_barras_3 = Format(Left(Nosso_Numero, 11), "00000000000") & Left(agencia, 4) & Left(Codigo_Cedente, 8) _
                    & Left(Carteira, 2)
    codigo_barras = Codigo_barras_1 & " " & Codigo_barras_2 & Codigo_barras_3
    Dv_geral = calcula_mod11(codigo_barras)
    
    codigo_barras = Left(Codigo_barras_1 & Dv_geral & Codigo_barras_2 & Codigo_barras_3 & Space(44), 44)
    
    ' acerto da linha digit�vel -- Jo�o Mac-Cormick em 16/5/2001
    '' Deixar a linha sem formata��o para envio � gr�fica
    'linha_digitavel = Right(Space(54) & Parte1 & " " & Parte2 & " " & Parte3 & " " & Dv_geral & " " _
                      & Fator & Format(Val_Cobranca * 100, "0000000000"), 54)
    linha_digitavel = Right(Space(47) & Parte1 & Parte2 & Parte3 & Dv_geral _
                      & Fator & Format(Val_Cobranca * 100, "0000000000"), 47)

End Sub

Private Sub GerarArquivo()

On Error GoTo Trata_Erro

Dim arqoutput       As Integer
Dim sSQL            As String
Dim lRegistros      As Long
Dim lLinhas         As Long
'Dim rsDestinatario  As ADODB.Recordset
'Dim rsSegurado      As ADODB.Recordset
'Dim rsBoleto        As ADODB.Recordset
Dim rsDestinatario  As rdoResultset
Dim rsSegurado      As rdoResultset
Dim rsBoleto        As rdoResultset
Dim sDestinatario   As String * 227
Dim sDocumento      As String * 1103
Dim sBoleto         As String * 501
Dim iRamo           As Integer
Dim lApolice        As Long
Dim lSub            As Integer
Dim sAgencia        As String
Dim sCedente        As String
Dim sCodBarras      As String
Dim sLinhaDigitavel As String
Dim iIdade          As Integer
Dim dlTaxa          As Double
Dim dlPremioTarifa  As Double
Dim cPremioMensal   As Currency

    'InicializaParametrosExecucaoBatch (Me)
    '
    'Dados de gera��o do arquivo
    lblVersao.Caption = CStr(ObterNumeroRemessa(ARQ_CERTIF))
    lblArquivo.Caption = ARQ_CERTIF & "." & Format(lblVersao.Caption, "0000")
    lblStatus.Caption = "Em processamento"
    lblCaminho.Caption = LerArquivoIni("Relatorios", "REMESSA_GERADO_PATH")
       
    lRegistros = 0
    lLinhas = 0
       
' Passa a abrir o arquivo depois do select - 10/10/2003
'    'Abrindo o arquivo
'    arqoutput = FreeFile
'    Open lblCaminho.Caption & lblArquivo.Caption For Output As arqoutput
'
'    'Header
'    Call MontaHeader(arqoutput)

    '----------------------------------------------------------
    'selecionando dados do sub estipulante /destinatario
    '----------------------------------------------------------

    sSQL = ""
    sSQL = sSQL & " SELECT " & vbNewLine
    sSQL = sSQL & "     sa.seguradora_cod_susep, sa.sucursal_seguradora_id, sa.ramo_id, sa.apolice_id, sa.sub_grupo_id, sa.tp_faturamento " & vbCrLf
    sSQL = sSQL & "     , isnull(sa.val_custo_cobranca, 0) custo_boleto,sa.texto_certificado, isnull(sga.val_assistencia, 0) custo_uss " & vbNewLine
    sSQL = sSQL & "     , isnull(pj.cgc, pf.cpf) cpf_cnpj_cli , cl.nome nome_cli, en.endereco, en.bairro, en.cep, " & vbNewLine
    sSQL = sSQL & "     isnull(en.municipio_sise, mun.nome) municipio_sub , en.estado " & vbNewLine
    sSQL = sSQL & "     , isnull(pj2.cgc, pf2.cpf) cpf_cnpj_sub , cl2.nome nome_sub " & vbNewLine
    sSQL = sSQL & "     , p.dt_proposta, p.proposta_id, isnull(pf3.cpf, pj3.cgc) cpf_cnpj_seg , cl3.nome nome_seg, pf3.dt_nascimento " & vbCrLf
    sSQL = sSQL & "     , ec.endereco endereco_seg, ec.bairro bairro_seg, ec.cep cep_seg " & vbNewLine
    'Abosco @ 2004-abr-14 - renovacao seguro vida alianca III
    'sSQL = sSQL & "     , ec.municipio municipio_seg, ec.estado estado_seg, pa.val_is, isnull(pa.val_parcela, 0) val_premio, isnull(pa.val_iof, 0) val_iof " & vbNewLine
    sSQL = sSQL & "     , ec.municipio municipio_seg, ec.estado estado_seg, pva.val_imp_segurada val_is, isnull(pva.val_premio_total, 0) val_premio, " & vbNewLine
    sSQL = sSQL & "     isnull(pa.val_iof, 0) val_iof " & vbNewLine
    sSQL = sSQL & "     , pa.proposta_bb, pva.proposta_pi, pva.proposta_pi_dv, ce.certificado_id, isnull(sv.texto_beneficiario, '') texto_beneficiario " & vbNewLine
    sSQL = sSQL & "     , pa.dt_inicio_vigencia, sv.dt_inicio_vigencia_sbg " & vbNewLine
    sSQL = sSQL & "     , pva.nome_beneficiario_cb, pva.endereco_beneficiario_cb, pva.bairro_beneficiario_cb " & vbNewLine
    sSQL = sSQL & "     , pva.municipio_beneficiario_cb, pva.uf_beneficiario_cb, pva.cep_beneficiario_cb " & vbNewLine
    sSQL = sSQL & " FROM " & vbNewLine
    sSQL = sSQL & "     sub_grupo_apolice_tb sa  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & " LEFT JOIN sub_grupo_assistencia_tb sga  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  sga.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND sga.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND sga.ramo_id = sa.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND sga.apolice_id = sa.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND sga.sub_grupo_id = sa.sub_grupo_id " & vbNewLine
    sSQL = sSQL & "     AND sga.dt_inicio_vigencia_sbg = sa.dt_inicio_vigencia_sbg " & vbNewLine
    sSQL = sSQL & "     AND sga.dt_fim_assist_sbg is null " & vbNewLine
    sSQL = sSQL & " JOIN apolice_tb ap  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  ap.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND ap.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND ap.ramo_id = sa.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND ap.apolice_id = sa.apolice_id " & vbNewLine
    sSQL = sSQL & " JOIN proposta_tb pr  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  pr.proposta_id = ap.proposta_id " & vbNewLine
    sSQL = sSQL & " JOIN cliente_tb cl  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON cl.cliente_id = pr.prop_cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_fisica_tb pf  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pf.pf_cliente_id = cl.cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_juridica_tb pj  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pj.pj_cliente_id = cl.cliente_id " & vbNewLine
    sSQL = sSQL & " JOIN representacao_sub_grupo_tb re  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  re.apolice_id = sa.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND re.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND re.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND re.ramo_id = sa.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND re.sub_grupo_id = sa.sub_grupo_id " & vbNewLine
    sSQL = sSQL & "     AND re.dt_inicio_vigencia_sbg = sa.dt_inicio_vigencia_sbg " & vbNewLine
    sSQL = sSQL & " JOIN cliente_tb cl2  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON cl2.cliente_id = re.est_cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_fisica_tb pf2  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pf2.pf_cliente_id = cl2.cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_juridica_tb pj2  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pj2.pj_cliente_id = cl2.cliente_id " & vbNewLine
    sSQL = sSQL & " JOIN endereco_cliente_tb en  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  en.cliente_id = re.est_cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN municipio_tb mun " & vbNewLine
    sSQL = sSQL & "     ON  mun.municipio_id = en.municipio_id " & vbNewLine
    sSQL = sSQL & "     AND mun.estado = en.estado " & vbNewLine
    sSQL = sSQL & " JOIN seguro_vida_sub_grupo_tb sv  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  sv.apolice_id = sa.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND sv.sucursal_seguradora_id = sa.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND sv.seguradora_cod_susep = sa.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND sv.ramo_id = sa.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND sv.sub_grupo_id = sa.sub_grupo_id " & vbNewLine
    sSQL = sSQL & "     AND sv.dt_inicio_vigencia_apol_sbg = sa.dt_inicio_vigencia_sbg " & vbNewLine
    sSQL = sSQL & " JOIN certificado_tb ce  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  ce.seguradora_cod_susep = sv.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND ce.sucursal_seguradora_id = sv.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND ce.ramo_id = sv.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND ce.apolice_id = sv.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND ce.proposta_id = sv.proposta_adesao_id " & vbNewLine
    sSQL = sSQL & " JOIN proposta_adesao_tb pa  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  pa.seguradora_cod_susep = sv.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND pa.sucursal_seguradora_id = sv.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND pa.ramo_id = sv.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND pa.apolice_id = sv.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND pa.proposta_id = sv.proposta_adesao_id " & vbNewLine
    sSQL = sSQL & " JOIN web_seguros_db..proposta_vida_alianca_tb pva  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  pva.seguradora_cod_susep = sv.seguradora_cod_susep " & vbNewLine
    sSQL = sSQL & "     AND pva.sucursal_seguradora_id = sv.sucursal_seguradora_id " & vbNewLine
    sSQL = sSQL & "     AND pva.ramo_id = sv.ramo_id " & vbNewLine
    sSQL = sSQL & "     AND pva.apolice_id = sv.apolice_id " & vbNewLine
    sSQL = sSQL & "     AND pva.proposta_id = sv.proposta_adesao_id " & vbNewLine
    sSQL = sSQL & " JOIN proposta_tb p  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  p.proposta_id = pa.proposta_id " & vbNewLine
    sSQL = sSQL & " JOIN cliente_tb cl3  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  cl3.cliente_id = p.prop_cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_fisica_tb pf3  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pf3.pf_cliente_id = cl3.cliente_id " & vbNewLine
    sSQL = sSQL & " LEFT JOIN pessoa_juridica_tb pj3  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON pj3.pj_cliente_id = cl3.cliente_id " & vbNewLine
    sSQL = sSQL & " JOIN endereco_corresp_tb ec  WITH (NOLOCK)  " & vbNewLine
    sSQL = sSQL & "     ON  ec.proposta_id = p.proposta_id " & vbNewLine
    sSQL = sSQL & " WHERE " & vbNewLine
    'Ezequiel - 20040402 - Inclus�o de condi��o de dt_fim_vigencia_sbg nula para
    '                      selecionar as renova��es do produto 714
    sSQL = sSQL & "      sv.dt_fim_vigencia_sbg IS NULL" & vbNewLine
    sSQL = sSQL & "     AND sa.dt_fim_vigencia_sbg IS NULL" & vbNewLine
    sSQL = sSQL & "     AND (sa.tp_faturamento = 'i' OR sa.tp_faturamento = 'f') " & vbNewLine
    sSQL = sSQL & "     AND sa.tp_vida = 'a' " & vbNewLine
    sSQL = sSQL & "     AND ce.dt_emissao is null " & vbNewLine
    sSQL = sSQL & "     AND p.situacao = 'i' " & vbNewLine
    sSQL = sSQL & "     AND re.dt_fim_representacao is null " & vbNewLine
    sSQL = sSQL & " ORDER BY ce.proposta_id " & vbNewLine

    Set rsDestinatario = rdocn1.OpenResultset(sSQL, rdOpenStatic)

    If Not rsDestinatario.EOF Then

        'Abrindo o arquivo                '10/10/2003
        arqoutput = FreeFile
        Open lblCaminho.Caption & lblArquivo.Caption For Output As arqoutput
          
        'Header                           '10/10/2003
        Call MontaHeader(arqoutput)
    
        lRegistros = 0
        lLinhas = 0
        
        'Inicializando progress bar
'        ProgressBar1.Min = 0
'        ProgressBar1.Max = rsDestinatario.RecordCount
'        ProgressBar1.Visible = True
        ''
        rdocn.BeginTrans
        
        While Not rsDestinatario.EOF
  
            DoEvents
            lLinhas = lLinhas + 1
            ''Imprime registro 10 para destinat�rio
            
            'Endere�o Destinat�rio
            Mid(sDestinatario, 1, 227) = Space(227)
            Mid(sDestinatario, 1, 2) = "10"
            Mid(sDestinatario, 3, 6) = Format(lLinhas, "000000")
            Mid(sDestinatario, 9, 9) = Format(rsDestinatario!proposta_id, "000000000")
            '' Altera��o Solicitada por Fabiana em 19/03/2003
            '' Utilizar o endere�o de correspond�ncia da proposta do segurado para o destinat�rio
    '        Mid(sDestinatario, 18, 60) = rsDestinatario("nome_sub")
    '        Mid(sDestinatario, 78, 60) = rsDestinatario("endereco")
    '        Mid(sDestinatario, 138, 30) = rsDestinatario("bairro")
    '        Mid(sDestinatario, 168, 30) = rsDestinatario("municipio_sub")
    '        Mid(sDestinatario, 198, 2) = rsDestinatario("estado")
    '        Mid(sDestinatario, 200, 8) = rsDestinatario("cep")
            Mid(sDestinatario, 18, 60) = rsDestinatario("nome_seg")
            Mid(sDestinatario, 78, 60) = rsDestinatario("endereco_seg")
            Mid(sDestinatario, 138, 30) = rsDestinatario("bairro_seg")
            Mid(sDestinatario, 168, 30) = rsDestinatario("municipio_seg")
            Mid(sDestinatario, 198, 2) = rsDestinatario("estado_seg")
            Mid(sDestinatario, 200, 8) = rsDestinatario("cep_seg")
    
            Mid(sDestinatario, 208, 20) = "05" & Format(rsDestinatario!proposta_id, "000000000") & Format(ConverteParaJulianDate(CDate(Data_Sistema)), "00000") & "0000" 'N�o existe ag�ncia para ades�o WEB!!!!
            '
            Print #arqoutput, sDestinatario
            
            'Dados do documento - Registro 20
            lLinhas = lLinhas + 1
            Mid(sDocumento, 1, 613) = Space(613)
            Mid(sDocumento, 1, 2) = "20"
            Mid(sDocumento, 3, 6) = Format(lLinhas, "000000")
            Mid(sDocumento, 9, 9) = Format(rsDestinatario("proposta_id"), "000000000")
            Mid(sDocumento, 18, 60) = rsDestinatario("nome_cli")
            Mid(sDocumento, 78, 18) = IIf(Len(rsDestinatario("cpf_cnpj_cli")) = 11, Format(rsDestinatario("cpf_cnpj_cli"), "000\.000\.000\-00"), Format(rsDestinatario("cpf_cnpj_cli"), "00\.000\.000\/0000\-00"))
            Mid(sDocumento, 96, 60) = rsDestinatario("nome_seg")
            Mid(sDocumento, 156, 14) = IIf(Len(rsDestinatario("cpf_cnpj_seg")) = 11, Format(rsDestinatario("cpf_cnpj_seg"), "000\.000\.000\-00"), Format(rsDestinatario("cpf_cnpj_seg"), "00\.000\.000\/0000\-00"))
            Mid(sDocumento, 170, 60) = rsDestinatario("nome_sub")
            Mid(sDocumento, 230, 18) = IIf(Len(rsDestinatario("cpf_cnpj_sub")) = 11, Format(rsDestinatario("cpf_cnpj_sub"), "000\.000\.000\-00"), Format(rsDestinatario("cpf_cnpj_sub"), "00\.000\.000\/0000\-00"))
            Mid(sDocumento, 248, 9) = rsDestinatario("certificado_id")
            'Mid(sDocumento, 257, 9) = Format(rsDestinatario("proposta_bb"), "000000000")
            If IsNull(rsDestinatario!proposta_pi) Then
                Mid(sDocumento, 257, 9) = Format(rsDestinatario("proposta_id"), "000000000")
            Else
                'Solicitado por Fabiana Yamamoto em 17/03/2003 - 10h
                'Mid(sDocumento, 257, 9) = Format(rsDestinatario("proposta_pi"), "0000000") & "-" & rsDestinatario!proposta_pi_dv
                'Solicitado por Fabiana Yamamoto em 17/03/2003 - 14h
                Mid(sDocumento, 257, 9) = Format(rsDestinatario("proposta_pi"), "000000000")
            End If
            Mid(sDocumento, 266, 300) = Left(Trim(rsDestinatario("texto_beneficiario")) & Space(300), 300)
            Mid(sDocumento, 566, 10) = Format(rsDestinatario("dt_inicio_vigencia"), "dd/mm/yyyy")
            
            ''Calcula a idade do segurado para obter a taxa e calcular o pr�mio mensal
            sSQL = "EXEC calcula_idade_sps '" & Format(rsDestinatario!dt_proposta, "yyyymmdd") & "'," & _
                                  "'" & Format(rsDestinatario!Dt_Nascimento, "yyyymmdd") & "',null"

            Set rsSegurado = rdocn2.OpenResultset(sSQL)
            
            iIdade = rsSegurado(0)
            
            rsSegurado.Close

            sSQL = ""
            sSQL = sSQL & "SELECT DISTINCT (taxa / 100) taxa" & vbNewLine
            sSQL = sSQL & "FROM sub_grupo_plano_tb pl" & vbNewLine
            sSQL = sSQL & "INNER JOIN sub_grupo_plano_faixa_tb f" & vbNewLine
            sSQL = sSQL & "  ON f.seguradora_cod_susep = pl.seguradora_cod_susep" & vbNewLine
            sSQL = sSQL & "  AND f.sucursal_seguradora_id = pl.sucursal_seguradora_id" & vbNewLine
            sSQL = sSQL & "  AND f.ramo_id = pl.ramo_id" & vbNewLine
            sSQL = sSQL & "  AND f.apolice_id = pl.apolice_id" & vbNewLine
            sSQL = sSQL & "  AND f.sub_grupo_id = pl.sub_grupo_id" & vbNewLine
            sSQL = sSQL & "  AND f.dt_inicio_vigencia_sbg = pl.dt_inicio_vigencia_sbg" & vbNewLine
            sSQL = sSQL & "  AND f.tp_plano_id = pl.tp_plano_id" & vbNewLine
            sSQL = sSQL & "  AND f.dt_inicio_vig_plano = pl.dt_inicio_vig_plano" & vbNewLine
            sSQL = sSQL & "WHERE pl.seguradora_cod_susep = " & rsDestinatario!seguradora_cod_susep & vbNewLine
            sSQL = sSQL & "AND pl.sucursal_seguradora_id = " & rsDestinatario!sucursal_seguradora_id & vbNewLine
            sSQL = sSQL & "AND pl.ramo_id = " & rsDestinatario!ramo_id & vbNewLine
            sSQL = sSQL & "AND pl.apolice_id = " & rsDestinatario!Apolice_id & vbNewLine
            sSQL = sSQL & "AND pl.sub_grupo_id = " & rsDestinatario!sub_grupo_id & vbNewLine
            sSQL = sSQL & "AND pl.dt_inicio_vig_plano <= '" & Format(rsDestinatario!dt_inicio_vigencia, "yyyymmdd") & "'" & vbNewLine
            sSQL = sSQL & "AND (pl.dt_fim_vig_plano > '" & Format(rsDestinatario!dt_inicio_vigencia, "yyyymmdd") & "' OR pl.dt_fim_vig_plano is null )" & vbNewLine
            'Abosco @ 2004-abr-14 - renovacao seguro vida alianca III
            'sSQL = sSQL & "AND f.val_is = " & rsDestinatario!val_is & vbNewLine
            sSQL = sSQL & "AND f.idade_min <= " & iIdade & vbNewLine
            sSQL = sSQL & "AND f.idade_max >= " & iIdade & vbNewLine

            Set rsSegurado = rdocn2.OpenResultset(sSQL)

            If rsSegurado.EOF Then
                dlTaxa = 0
            Else
                dlTaxa = Val(rsSegurado("taxa"))        '14/10/2003
            End If

            rsSegurado.Close
            
            'Abosco @ 2004-abr-14 - renovacao seguro vida alianca III
            dlPremioTarifa = TruncaDecimal((Val(rsDestinatario!val_is) * dlTaxa) + Val(rsDestinatario!custo_boleto) + Val(rsDestinatario!custo_uss))
            cPremioMensal = TruncaDecimal(dlPremioTarifa * 1.07)   'IOF
            
           
            Mid(sDocumento, 576, 14) = Right(Space(14) & Format(Val(rsDestinatario("val_is")), "###,###,##0.00"), 14)          '14/010/2003
            Mid(sDocumento, 590, 14) = Right(Space(14) & Format(Val(cPremioMensal), "###,###,##0.00"), 14)
            Mid(sDocumento, 604, 10) = Format(Data_Sistema, "dd/mm/yyyy")
            '' Inclu�do em 04/04/2003
            '' Solicitado por Paulo Marques
            If Not IsNull(rsDestinatario("nome_beneficiario_cb")) Then
                Mid(sDocumento, 614, 60) = Left(Trim(rsDestinatario("nome_beneficiario_cb")) & Space(60), 60)
                Mid(sDocumento, 674, 60) = Left(Trim(rsDestinatario("endereco_beneficiario_cb")) & Space(60), 60)
                Mid(sDocumento, 734, 30) = Left(Trim(rsDestinatario("bairro_beneficiario_cb")) & Space(30), 30)
                Mid(sDocumento, 764, 30) = Left(Trim(rsDestinatario("municipio_beneficiario_cb")) & Space(30), 30)
                Mid(sDocumento, 794, 2) = Left(Trim(rsDestinatario("uf_beneficiario_cb")) & Space(2), 2)
                Mid(sDocumento, 796, 8) = Left(Trim(rsDestinatario("cep_beneficiario_cb")) & Space(8), 8)
            Else
                Mid(sDocumento, 614, 60) = Space(60)
                Mid(sDocumento, 674, 60) = Space(60)
                Mid(sDocumento, 734, 30) = Space(30)
                Mid(sDocumento, 764, 30) = Space(30)
                Mid(sDocumento, 794, 2) = Space(2)
                Mid(sDocumento, 796, 8) = Space(8)
            End If
            
            'Texto vari�vel do Certificado por subgrupo
            Mid(sDocumento, 804, 300) = Left(Trim(rsDestinatario("texto_certificado")) & Space(300), 300)
            
            '
            Print #arqoutput, sDocumento
            
            'Dados do Boleto (Cobran�a)
            If UCase(rsDestinatario("tp_faturamento")) = "I" Then
          
                'Selecionando dados da cobran�a
                sSQL = ""
                sSQL = sSQL & " SELECT " & vbNewLine
                sSQL = sSQL & "     ag.proposta_id , seg.nome cedente, ag.dt_agendamento, " & vbNewLine
                sSQL = sSQL & "     cta.agencia_id , cta.conta_corrente_id, "
                sSQL = sSQL & "     cast(nosso_numero as char(11)) + cast(nosso_numero_dv as char(1)) AS NossoNum, " & vbNewLine
                sSQL = sSQL & "     ag.val_cobranca , ag.num_cobranca, ag.val_iof, pra.apolice_id, pra.ramo_id " & vbNewLine
                sSQL = sSQL & " , '18-019' carteira "
                sSQL = sSQL & " INTO ##TEMP_SEGA8039 " 'MARCIO.NOGUEIRA  - 19368999 - cobran�a registrada AB e ABS
                sSQL = sSQL & " FROM " & vbNewLine
                sSQL = sSQL & "     agendamento_cobranca_tb ag  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & " JOIN emissao_cbr_tb em  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON  ag.proposta_id = em.proposta_id " & vbNewLine
                sSQL = sSQL & "     AND ag.num_cobranca = em.num_cobranca " & vbNewLine
                sSQL = sSQL & " JOIN proposta_tb prp  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON prp.proposta_id = ag.proposta_id " & vbNewLine
                sSQL = sSQL & " JOIN proposta_adesao_tb pra  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON pra.proposta_id = prp.proposta_id " & vbNewLine
                sSQL = sSQL & " JOIN  tp_movimentacao_financ_tb mf  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON  mf.ramo_id = pra.ramo_id" & vbNewLine
                sSQL = sSQL & "     AND mf.produto_id =  prp.produto_id " & vbNewLine
                sSQL = sSQL & "     AND mf.tp_operacao_financ_id = 2 " & vbNewLine
                sSQL = sSQL & "     AND mf.moeda_id = 790 "
                sSQL = sSQL & " JOIN  convenio_tb c  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON  c.num_convenio = mf.num_convenio " & vbNewLine
                sSQL = sSQL & " LEFT JOIN agencia_tb cob  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON  c.banco_id = cob.banco_id " & vbNewLine
                sSQL = sSQL & "     and c.agencia_id = cob.agencia_id " & vbNewLine
                sSQL = sSQL & " LEFT JOIN conta_convenio_seg_tb cta  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON  c.banco_id = cta.banco_id " & vbNewLine
                sSQL = sSQL & "     AND c.agencia_id = cta.agencia_id " & vbNewLine
                sSQL = sSQL & "     AND c.conta_corrente_id = cta.conta_corrente_id " & vbNewLine
                sSQL = sSQL & " LEFT JOIN seguradora_tb seg  WITH (NOLOCK)  " & vbNewLine
                sSQL = sSQL & "     ON seg.seguradora_cod_susep = cta.seguradora_cod_susep " & vbNewLine
                sSQL = sSQL & " WHERE " & vbNewLine
                sSQL = sSQL & "         ag.proposta_id = " & rsDestinatario("proposta_id") & vbNewLine
                sSQL = sSQL & "     AND ag.num_cobranca > 1 " & vbNewLine
                
                'dt_agendamento '''''''''''''''''''''''
                sSQL = sSQL & "     AND ag.dt_agendamento >= '20040401' " & vbNewLine
                
                'situacao do agendamento ''''''''''''''
                sSQL = sSQL & "     AND ag.situacao = 'P' " & vbNewLine
                
                sSQL = sSQL & " ORDER BY ag.proposta_id, ag.num_cobranca " & vbNewLine
              
                'INICIO - MARCIO.NOGUEIRA - 19368999 - cobran�a registrada AB e ABS
                sSQL = sSQL & " DELETE ##TEMP_SEGA8039" & vbNewLine
                sSQL = sSQL & " FROM ##TEMP_SEGA8039" & vbNewLine
                sSQL = sSQL & " JOIN SEGUROS_DB.DBO.PROPOSTA_ADESAO_TB WITH(NOLOCK)" & vbNewLine
                sSQL = sSQL & " ON ##TEMP_SEGA8039.PROPOSTA_ID = PROPOSTA_ADESAO_TB.PROPOSTA_ID" & vbNewLine
                sSQL = sSQL & " JOIN SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_TB WITH(NOLOCK)" & vbNewLine
                sSQL = sSQL & "  ON AGENDAMENTO_COBRANCA_TB.PROPOSTA_ID = ##TEMP_SEGA8039.PROPOSTA_ID" & vbNewLine
                sSQL = sSQL & " AND AGENDAMENTO_COBRANCA_TB.num_cobranca  = ##TEMP_SEGA8039.num_cobranca" & vbNewLine
                sSQL = sSQL & " WHERE PROPOSTA_ADESAO_TB.FORMA_PGTO_ID = 3" & vbNewLine
                sSQL = sSQL & "  AND AGENDAMENTO_COBRANCA_TB.fl_boleto_registrado = 'N'" & vbNewLine
                
                sSQL = sSQL & " DELETE ##TEMP_SEGA8039" & vbNewLine
                sSQL = sSQL & " FROM ##TEMP_SEGA8039" & vbNewLine
                sSQL = sSQL & " JOIN SEGUROS_DB.DBO.PROPOSTA_FECHADA_TB WITH(NOLOCK)" & vbNewLine
                sSQL = sSQL & "    ON ##TEMP_SEGA8039.PROPOSTA_ID = PROPOSTA_FECHADA_TB.PROPOSTA_ID" & vbNewLine
                sSQL = sSQL & " JOIN SEGUROS_DB.DBO.AGENDAMENTO_COBRANCA_TB WITH(NOLOCK)" & vbNewLine
                sSQL = sSQL & "  ON AGENDAMENTO_COBRANCA_TB.PROPOSTA_ID = ##TEMP_SEGA8039.PROPOSTA_ID" & vbNewLine
                sSQL = sSQL & " AND AGENDAMENTO_COBRANCA_TB.num_cobranca  = ##TEMP_SEGA8039.num_cobranca" & vbNewLine
                sSQL = sSQL & " WHERE PROPOSTA_FECHADA_TB.FORMA_PGTO_ID = 3" & vbNewLine
                sSQL = sSQL & "  AND AGENDAMENTO_COBRANCA_TB.fl_boleto_registrado = 'N'" & vbNewLine
                
                rdocn2.Execute (sSQL)
                sSQL = ""
                 sSQL = "SELECT * FROM ##TEMP_SEGA8039"
                Set rsBoleto = rdocn2.OpenResultset(sSQL, rdOpenStatic)
                 sSQL = "DROP TABLE ##TEMP_SEGA8039"
                rdocn.Execute (sSQL)
                'FIM - MARCIO.NOGUEIRA - 19368999 - cobran�a registrada AB e ABS
              
                While Not rsBoleto.EOF
        
                    lLinhas = lLinhas + 1
                    '
                    sBoleto = ""
                    Mid(sBoleto, 1, 498) = Space(498)
                    Mid(sBoleto, 1, 2) = "30"
                    Mid(sBoleto, 3, 6) = Format(lLinhas, "000000")
                    Mid(sBoleto, 9, 9) = Format(rsDestinatario("proposta_id"), "000000000")
                    Mid(sBoleto, 18, 60) = Left(Trim(rsBoleto("cedente")) & Space(60), 60)
                    Mid(sBoleto, 78, 10) = Format(Data_Sistema, "dd/mm/yyyy")
                    Mid(sBoleto, 88, 50) = "QUALQUER AG�NCIA"
                    Mid(sBoleto, 138, 10) = Format(rsBoleto("dt_agendamento"), "dd/mm/yyyy")
                    Mid(sBoleto, 148, 15) = rsBoleto("agencia_id") & "-" & calcula_dv_cc_ou_agencia(rsBoleto("agencia_id")) & _
                     rsBoleto("conta_corrente_id") & "-" & calcula_dv_cc_ou_agencia(rsBoleto("conta_corrente_id"))
        
                    Mid(sBoleto, 163, 10) = Format(Data_Sistema, "dd/mm/yyyy")
                    Mid(sBoleto, 173, 17) = Format(rsBoleto("ramo_id"), "00") & Format(rsBoleto("apolice_id"), "0000000") & Format(rsBoleto("num_cobranca"), "00000000")
                    Mid(sBoleto, 190, 2) = "NS"
                    Mid(sBoleto, 192, 1) = "N"
                    Mid(sBoleto, 193, 10) = Format(Data_Sistema, "dd/mm/yyyy")
                    Mid(sBoleto, 203, 16) = Format(rsBoleto("NossoNum"), "00\.000\.000\.000\-0")
                    Mid(sBoleto, 225, 6) = rsBoleto("carteira")
                    Mid(sBoleto, 231, 4) = "R$"
                    Mid(sBoleto, 245, 10) = Right(Space(10) & Format(Val(rsBoleto("val_cobranca")), "0.00"), 10)       '14/10/2003
                    Mid(sBoleto, 255, 10) = Right(Space(10) & Format(Val(rsBoleto("val_cobranca")), "0.00"), 10)       '14/10/2003
                    Mid(sBoleto, 265, 60) = Left(Trim(rsDestinatario("nome_seg")) & Space(60), 60)
                    Mid(sBoleto, 325, 60) = Left(Trim(rsDestinatario("endereco_seg")) & Space(60), 60)
                    Mid(sBoleto, 385, 60) = Left(Trim(rsDestinatario("municipio_seg")) & Space(60), 60)
                    Mid(sBoleto, 445, 2) = rsDestinatario("estado_seg")
                    Mid(sBoleto, 447, 8) = rsDestinatario("cep_seg")
                    sAgencia = Format(rsBoleto!agencia_id, "0000") & "-" & calcula_dv_cc_ou_agencia(Format(rsBoleto!agencia_id, "0000"))
                    sCedente = Format(rsBoleto!conta_corrente_id, "00000000") & "-" & calcula_dv_cc_ou_agencia(Format(rsBoleto!conta_corrente_id, "00000000"))
    
                    Call Montar_linha_digitavel(rsBoleto!nossonum, sAgencia, sCedente, rsBoleto!Carteira, rsBoleto!dt_agendamento, Val(rsBoleto!Val_Cobranca), sCodBarras, sLinhaDigitavel)          '14/10/2003
                    'Mid(sBoleto, 455, 44) = sCodBarras
                    '' Rafael - 25/03/2003 - Solicitado por Paulo Marcos
                    '' Enviar a linha digit�vel e n�o o c�digo de barras
                    Mid(sBoleto, 455, 47) = sLinhaDigitavel
                    
                    'Inclu�do IF para n�o gerar as parcelas com vencimento em 25/04/2003
                    'in�cio
                    If Format(rsBoleto("dt_agendamento"), "dd/mm/yyyy") <> "25/04/2003" Then
                    
                        Print #arqoutput, sBoleto
                                
                        Call ObservacaoBoleto(lLinhas, rsDestinatario("proposta_id"), arqoutput)
                        Call InstrucaoBoleto(lLinhas, rsDestinatario("proposta_id"), Val(rsBoleto("val_iof")), arqoutput)        '14/10/2003
            
                        '' Atualiza a data de emissao do boleto
                        sSQL = ""
                        sSQL = "exec emissao_cbr_spu " & rsDestinatario("proposta_id")
                        sSQL = sSQL & ", " & rsBoleto("num_cobranca")
                        sSQL = sSQL & ", 1 "
                        sSQL = sSQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
                        sSQL = sSQL & ", '" & Trim(cUserName) & "'"
                        rdocn.Execute (sSQL)
                    End If
                    'fim
                    
                    rsBoleto.MoveNext
                Wend
                rsBoleto.Close
                Set rsBoleto = Nothing
            End If
      
            '' Atualiza a data de emissao do certificado
            sSQL = ""
            sSQL = "exec certificado_emissao_spu " & rsDestinatario!seguradora_cod_susep
            sSQL = sSQL & ", " & rsDestinatario!sucursal_seguradora_id
            sSQL = sSQL & ", " & rsDestinatario!ramo_id
            sSQL = sSQL & ", " & rsDestinatario!Apolice_id
            sSQL = sSQL & ", " & rsDestinatario("proposta_id")
            sSQL = sSQL & ", " & rsDestinatario!certificado_id
            sSQL = sSQL & ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
            sSQL = sSQL & ", '" & Trim(cUserName) & "'"
            '
            rdocn.Execute (sSQL)
            
            sSQL = ""          '14/10/2003
            sSQL = sSQL & "exec evento_seguros_db..evento_impressao_spi " & _
                   rsDestinatario("proposta_id") & _
                   ", 0" & _
                   ", null " & _
                   ", null " & _
                   ", 'C'" & _
                   ", 05" & _
                   ",'i'" & _
                   ", null" & _
                   ", " & rsDestinatario("sub_grupo_id") & _
                   ", 'C'" & _
                   ", '' " & _
                   ", '' " & _
                   ", '" & cUserName & "'" & _
                   ", '" & Format(Date, "yyyymmdd") & "'" & _
                   ", '" & lblArquivo.Caption & "'"
                   
            rdocn.Execute (sSQL)
            
            'Contador de registros do recordset
            lRegistros = lRegistros + 1
            
            rsDestinatario.MoveNext
            
            'ProgressBar1.Value = lRegistros
        Wend
  
        rsDestinatario.Close
        Set rsDestinatario = Nothing
      
        'Trailler
        Call MontaTrailler(arqoutput, lLinhas)
        
        'Fechando o arquivo
        Close #arqoutput
        
        'Atualizando dados de vers�o de arquivo
        Call InserirArquivoVersao(ARQ_CERTIF, lRegistros, CInt(lblVersao.Caption))
    
        rdocn.CommitTrans
    
        MensagemBatch "Arquivo gerado com sucesso."
    End If
    
    'Atualizando interface
    lblStatus.Caption = "Concluido"
    'ProgressBar1.Visible = False
    TlbSair.Buttons("BtnOK").Enabled = False
    
    Call goProducao.AdicionaLog(1, IIf(lLinhas > 0, lblArquivo.Caption, "Arquivo n�o gerado"))         '17/10/2003
    Call goProducao.AdicionaLog(2, IIf(lLinhas > 0, lLinhas, "0"))
    Call goProducao.AdicionaLog(3, IIf(lLinhas > 0, lRegistros, "0"))
    Call goProducao.Finaliza
    
    Exit Sub

Trata_Erro:
   Call TrataErroGeral("GerarArquivo", Me.name)
   Call TerminaSEGBR

End Sub

Private Sub InserirArquivoVersao(ByVal sNome As String, ByVal iRegistros As Long, _
                                 ByVal iRemessa As Integer)
Dim sSQL As String

   On Error GoTo Erro

    adSQL sSQL, "EXEC controle_proposta_db..arquivo_versao_gerado_spi "
    adSQL sSQL, "'" & sNome & "'"
    adSQL sSQL, ",  " & iRemessa
    adSQL sSQL, ",  " & iRegistros
    adSQL sSQL, ", '" & Format(Data_Sistema, "yyyymmdd") & "'"
    adSQL sSQL, ",  " & iRegistros
    adSQL sSQL, ", '" & cUserName & "'"

    Call rdocn.Execute(sSQL)

   Exit Sub

Erro:
   Call TrataErroGeral("InserirArquivoVersao", Me.name)
   Call TerminaSEGBR

End Sub

Sub InstrucaoBoleto(ByRef Sequencial As Long, ByVal proposta As Long, ByVal ValorIof As Currency, ByVal Arquivo As Integer)

On Error GoTo Erro

Dim LinhaInstrucao As String * 317
Dim linha_1 As String
Dim linha_2 As String
Dim linha_3 As String
Dim linha_4 As String
Dim linha_5 As String

On Error GoTo Erro

Sequencial = Sequencial + 1

linha_1 = "***  VALORES EM REAIS ***"

ValorIof = Format(ValorIof, "##,##0.00")

If ValorIof <> 0 Then
  linha_2 = "I.O.F.: R$ " & ValorIof
Else
  linha_2 = "I.O.F.:"
End If

linha_3 = "RECEBER AT� O 5� DIA AP�S O VENCIMENTO" '"N�O RECEBER AP�S O VENCIMENTO"
linha_4 = "APENAS NAS AG�NCIAS DO BANCO DO BRASIL"
linha_5 = "AP�S O VENCIMENTO COBRAR MULTA DE R$ 0,08 AO DIA"

   
Mid(LinhaInstrucao, 1, 317) = Space(317)
Mid(LinhaInstrucao, 1, 2) = "32"
Mid(LinhaInstrucao, 3, 6) = Format(Sequencial, "000000")
Mid(LinhaInstrucao, 9, 9) = Format(proposta, "000000000")
Mid(LinhaInstrucao, 18, 60) = linha_1
Mid(LinhaInstrucao, 78, 60) = linha_2
Mid(LinhaInstrucao, 138, 60) = linha_3
Mid(LinhaInstrucao, 198, 60) = linha_4
Mid(LinhaInstrucao, 258, 60) = linha_5

Print #Arquivo, LinhaInstrucao
   
Exit Sub

Erro:
   Call TrataErroGeral("InstrucaoBoleto", Me.name)
   Call TerminaSEGBR
   
End Sub

Private Sub MontaHeader(Arquivo)

On Error GoTo Erro

Dim sHeader As String * 22

  'Montando o Header
  Mid(sHeader, 1, 22) = Space(22)
  Mid(sHeader, 1, 2) = "01"
  Mid(sHeader, 3, 8) = ARQ_CERTIF
  Mid(sHeader, 11, 4) = Format(lblVersao.Caption, "0000")
  Mid(sHeader, 15, 8) = Format(Data_Sistema, "ddmmyyyy")
  
  Print #Arquivo, sHeader
  
Exit Sub

Erro:
   Call TrataErroGeral("MontaHeader", Me.name)
   Call TerminaSEGBR
  
End Sub

Sub ObservacaoBoleto(ByRef Sequencial As Long, ByVal proposta As Long, ByVal Arquivo As Integer)

Dim sObservacao As String * 317

On Error GoTo Erro

Sequencial = Sequencial + 1
'
Mid(sObservacao, 1, 317) = Space(317)
Mid(sObservacao, 1, 2) = "31"
Mid(sObservacao, 3, 6) = Format(Sequencial, "000000")
Mid(sObservacao, 9, 9) = Format(proposta, "000000000")
Mid(sObservacao, 18, 60) = "Informa��es Adicionais"
Mid(sObservacao, 78, 60) = "Em cumprimento � circular SUSEP nro 97 de 99, informamos que:"
Mid(sObservacao, 138, 60) = "*N�o pagamento da 1� parcela implicar� no canc. da ap�lice."
Mid(sObservacao, 198, 60) = "*O n�o pagamento das demais implicar� no canc. da ap�lice,"
Mid(sObservacao, 258, 60) = "o qual faculta o segurado reestabelecer a cobertura da mesma."
Print #Arquivo, sObservacao

Exit Sub

Erro:
   Call TrataErroGeral("ObservacaoBoleto", Me.name)
   Call TerminaSEGBR

End Sub

Private Sub Form_Load()

   'Call Conexao

   'Call Seguranca("SEGP0686", "Gera��o de Arquivo Certificados - Seguro Vida Alian�a")

   Call CentraFrm(Me)

   Me.Caption = "SEGP0686 - Gera��o de Arquivo Certificados - Seguro Vida Alian�a - " & Ambiente

   StatusBar1.SimpleText = "Clique em OK para gerar o arquivo."

'   'Iniciando o processo automaticamente caso seja uma chamada do Scheduler
'   If Obtem_agenda_diaria_id(Command) > 0 Then
      Me.Show
      Call TlbSair_ButtonClick(TlbSair.Buttons(1))
      Call TerminaSEGBR
'   End If

End Sub

Private Sub Processar()

   On Error GoTo Erro

   'Tratamento para o Scheduler
   'Call InicializaParametrosExecucaoBatch(Me)
   
   'Desabilitando os bot�es
   'TlbSair.Buttons(1).Enabled = False
   'TlbSair.Buttons(3).Enabled = False
   
   MousePointer = vbHourglass

   Call GerarArquivo

   MousePointer = vbDefault

   If Trim(lblArquivo.Caption) = "" Then
      'Informa��es para o Autoprod
      Call goProducao.AdicionaLog(1, "Arquivo n�o gerado")
      Call goProducao.AdicionaLog(2, "0")
   Else
      Call MensagemBatch("Fim de Processamento", vbInformation, , False)
      lblStatus.Caption = "Conclu�do"
      'StatusBar1.SimpleText = "Arquivo gerado com sucesso."
   End If

   Call goProducao.Finaliza

   Exit Sub

Erro:
   Call TrataErroGeral("Processar", Me.name)
   Call TerminaSEGBR

End Sub

Private Function ObterNumeroRemessa(ByVal sNome As String) As Integer

Dim sSQL As String, rsRemessa As rdoResultset
Dim iLayout As Integer

On Error GoTo Erro

sSQL = ""
adSQL sSQL, "SELECT"
adSQL sSQL, "  layout_id"
adSQL sSQL, "FROM"
adSQL sSQL, "  controle_proposta_db..layout_tb"
adSQL sSQL, "WHERE"
adSQL sSQL, "  nome = '" & sNome & "'"

Set rsRemessa = rdocn2.OpenResultset(sSQL, rdOpenStatic)

If rsRemessa.EOF Then
  Call MensagemBatch("O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical)
  Call TerminaSEGBR
Else
  iLayout = rsRemessa("layout_id")
End If

rsRemessa.Close
   
sSQL = ""
adSQL sSQL, "SELECT"
adSQL sSQL, "  ISNULL(MAX(versao), 0) + 1 AS versao"
adSQL sSQL, "FROM"
adSQL sSQL, "  controle_proposta_db..arquivo_versao_gerado_tb"
adSQL sSQL, "WHERE"
adSQL sSQL, "  layout_id = " & iLayout
  
Set rsRemessa = rdocn2.OpenResultset(sSQL, rdOpenStatic)

If Not rsRemessa.EOF Then
  ObterNumeroRemessa = rsRemessa("versao")
End If

rsRemessa.Close
Set rsRemessa = Nothing

Exit Function

Erro:
   Call TrataErroGeral("ObterNumeroRemessa", Me.name)
   Call TerminaSEGBR

End Function

Public Function ConverteParaJulianDate(ldate As Date) As String
Dim lJulianDate  As String * 5

  On Error GoTo Erro

  lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
  lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
  ConverteParaJulianDate = lJulianDate

  Exit Function

Erro:
  Call TrataErroGeral("ConverteParaJulianDate", Me.name)
  Call TerminaSEGBR

End Function

Private Sub TlbSair_ButtonClick(ByVal Button As MSComctlLib.Button)

   On Error GoTo Erro

   Select Case Button.Key

      Case "BtnOK"
         Call GerarArquivo

      Case "BtnSair"
         Call TerminaSEGBR
         
   End Select

   Exit Sub

Erro:
   Call TrataErroGeral("TlbSair_ButtonClick", Me.name)
   Call TerminaSEGBR
End Sub
