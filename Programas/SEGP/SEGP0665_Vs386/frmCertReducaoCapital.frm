VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmCertReducaoCapital 
   Caption         =   "SEGA8052"
   ClientHeight    =   5295
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7650
   LinkTopic       =   "Form1"
   ScaleHeight     =   5295
   ScaleWidth      =   7650
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.StatusBar stbPrincipal 
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   4920
      Width           =   7365
      _ExtentX        =   12991
      _ExtentY        =   450
      Style           =   1
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdSair 
      Caption         =   "Sai&r"
      Height          =   495
      Left            =   5760
      TabIndex        =   12
      Top             =   4200
      Width           =   1575
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&Aplicar"
      Enabled         =   0   'False
      Height          =   495
      Left            =   3960
      TabIndex        =   11
      Top             =   4200
      Width           =   1575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Frame1"
      Height          =   2775
      Left            =   120
      TabIndex        =   2
      Top             =   1200
      Width           =   7335
      Begin VB.TextBox txtRegProc 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   17
         ToolTipText     =   "Total de registros processados"
         Top             =   2280
         Width           =   975
      End
      Begin VB.TextBox txtTotReg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   15
         Top             =   1800
         Width           =   975
      End
      Begin VB.TextBox txtFim 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         Locked          =   -1  'True
         TabIndex        =   10
         ToolTipText     =   "Data final de processamento"
         Top             =   1200
         Width           =   1935
      End
      Begin VB.TextBox txtInicio 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2640
         Locked          =   -1  'True
         TabIndex        =   8
         ToolTipText     =   "Data de in�cio de processamento"
         Top             =   1200
         Width           =   1935
      End
      Begin VB.TextBox txtVersao 
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   6
         ToolTipText     =   "Vers�o do arquivo"
         Top             =   1200
         Width           =   615
      End
      Begin VB.TextBox txtArquivo 
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         TabIndex        =   4
         ToolTipText     =   "Nome do arquivo"
         Top             =   480
         Width           =   6015
      End
      Begin VB.Label Label6 
         AutoSize        =   -1  'True
         Caption         =   "Total de registros processados:"
         Height          =   195
         Left            =   240
         TabIndex        =   16
         Top             =   2280
         Width           =   2205
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "Total de registros gerados:"
         Height          =   195
         Left            =   240
         TabIndex        =   14
         Top             =   1800
         Width           =   1875
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Fim:"
         Height          =   195
         Left            =   4680
         TabIndex        =   9
         Top             =   1200
         Width           =   285
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "In�cio:"
         Height          =   195
         Left            =   2040
         TabIndex        =   7
         Top             =   1200
         Width           =   450
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Vers�o:"
         Height          =   195
         Left            =   240
         TabIndex        =   5
         Top             =   1200
         Width           =   540
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "Arquivo:"
         Height          =   195
         Left            =   240
         TabIndex        =   3
         Top             =   480
         Width           =   585
      End
   End
   Begin VB.Frame frmProduto 
      Caption         =   "Produto"
      Height          =   975
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7335
      Begin VB.ListBox lstProduto 
         Enabled         =   0   'False
         Height          =   255
         Left            =   5040
         TabIndex        =   18
         Top             =   360
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.ComboBox cmbProduto 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Tag             =   "1"
         ToolTipText     =   "Produtos"
         Top             =   360
         Width           =   6735
      End
   End
End
Attribute VB_Name = "frmCertReducaoCapital"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim Teste                   As String
Dim Seguradora_ant          As String
Dim Sucursal_ant            As String
Dim ramo                    As String
Dim Ramo_ant                As String
Dim Apolice                 As String
Dim TpPlano_ant             As String
Dim Agencia_ant             As String
Dim Conta_Linha_certificado As Long
Dim Nome_Rel_Certificado    As String
Dim Produto_Select          As String
Dim qtdReg                  As Long
Dim Produto                 As String
Dim Proposta_Basica         As String
Dim Estipulante             As String
Dim Val_Premio              As String
Dim Val_is_novo             As String
Dim Seguradora              As String
Dim Sucursal                As String
Dim Banco                   As String
Dim agencia                 As String
Dim PropostaBB              As String
Dim PropostaBB_ant          As String
Dim Tp_Registro             As String
Dim proposta                As String
Dim nome                    As String
Dim Endereco                As String
Dim Endereco_CI             As String
Dim Bairro                  As String
Dim Bairro_CI               As String
Dim Municipio               As String
Dim Municipio_CI            As String
Dim Cep                     As String
Dim Cep_CI                  As String
Dim Flag_Corretor           As String
Dim UF                      As String
Dim UF_CI                   As String
Dim DtInicioVigencia        As String
Dim CPF                     As String
Dim Dt_Nascimento           As String
Dim EndossoId               As String
Dim OrigemEndosso           As String
Dim TpDocumento             As String
Dim Dt_Emissao              As String
Public Valor_SOS            As String
Dim Plano                   As String
Dim NomePlano               As String
Dim TpPlano                 As String
Dim TipoPlano               As String
Dim ConfiguracaoBrasil      As Boolean
Dim PercCobTit1             As Double
Dim PercCobTit2             As Double
Dim PercCobTit3             As Double
Dim PercCobTit4             As Double
Dim PercCobConj1            As Double
Dim PercCobConj2            As Double
Dim PercCobConj3            As Double
Dim PercCobConj4            As Double
Dim Nome_Conj               As String
Dim CPF_Conj                As String
Dim Componente_Conjuge      As String
Dim NumRemessa              As String
Dim Tp_Seguro               As String
Dim Cod_Retorno             As String
Dim IS1_Tit                 As String
Dim IS1_Conj                As String
Dim IS2_Conj                As String
Dim IS3_Conj                As String
Dim IS4_Conj                As String
Dim Certificado             As String
Dim Layout                  As String
Dim tam_reg                 As Integer
Dim Certificado_path        As String
Dim logpath                 As String


Dim NomeProduto            As String
Dim NomeRamo               As String



Private Sub cmbProduto_Click()
    
    lstProduto.ListIndex = cmbProduto.ListIndex
    'S� habilita se escolher algum produto
    cmdOk.Enabled = True
    
    'Limpa os campos ap�s selecionar um produto
    txtArquivo.Text = ""
    txtInicio.Text = ""
    txtfim.Text = ""
    txtVersao.Text = ""
    txtTotReg.Text = ""
    
End Sub

Private Sub cmdOk_Click()
    
    On Erro GoTo TrataErro
    
     
    'Tratamento para o Scheduler
    'InicializaParametrosExecucaoBatch Me
    
    stbPrincipal.SimpleText = "Selecionando Registros."
    Me.MousePointer = vbHourglass
    Me.Refresh
    
    'Limpa campos
    Seguradora_ant = ""
    Sucursal_ant = ""
    Ramo_ant = ""
    Apolice_ant = ""
    Tp_plano_ant = ""
    Agencia_ant = ""
    txtTotReg.Text = 0
    txtRegProc.Text = 0
    Conta_Linha_certificado = 0
                
    MousePointer = 11
       
    txtInicio.Text = Now
    txtInicio.Refresh
    cmdOk.Enabled = False
    
    Certificado_path = LerArquivoIni("relatorios", "remessa_a_verificar_path")
    'Certificado_path = "D:\MYOSHIMURA\PROGRAMAS SEGBR\SEGP0665 altera��o"
    
    logpath = LerArquivoIni("Producao", "LOG_PATH")
    logpath = "D:\MYOSHIMURA\PROGRAMAS SEGBR\SEGP0665 altera��o"
    
''    Certificado_path = "D:\rogsantos\Meus Documentos\Manuten��o\SEGP0724"
''    LogPath = "D:\rogsantos\Meus Documentos\SEGP0724"

    Nome_Rel_Certificado = "SEGA8052" + Space(25)
    'Produto_Select = lstProduto.Text
        
    Call Processa_SEGA

    cmdOk.Enabled = True
    txtfim.Text = Now
    MousePointer = 0
    MensagemBatch "Fim do Processamento", vbInformation, , False

    'Loga o n�mero de registros processados - Scheduler
    Call goProducao.AdicionaLog(1, IIf(Len(Trim(txtArquivo)) = 0, "Arquivo n�o gerado", txtArquivo))
    Call goProducao.AdicionaLog(2, IIf(Len(Trim(txtTotReg)) = 0, "0", txtTotReg))
    Call goProducao.AdicionaLog(3, IIf(Len(Trim(txtRegProc)) = 0, "0", txtRegProc))
    Call goProducao.Finaliza
    
    Me.MousePointer = vbDefault

Exit Sub

TrataErro:
   
   TrataErroGeral "cmdOK_Click", Me.name
   TerminaSEGBR

End Sub

Private Sub cmdSair_Click()
    Unload Me
End Sub

Private Sub Form_Load()

    Dim Sql As String
    
    On Error GoTo TrataErro
    
    'Conexao
    
    'Call Seguranca("SEGP0724", "Gera��o de Certificado OVGE")
    
    sDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
    If sDecimal = "." Then
       ConfiguracaoBrasil = False
    Else
       ConfiguracaoBrasil = True
    End If
   
'Combo removida - 17/10/2003
'    'preenche combo 'Produto'
'    Sql = "Select nome, produto_id " & vbNewLine
'    Sql = Sql & "From produto_tb " & vbNewLine
'    Sql = Sql & "Where ativo = 's' " & vbNewLine
'    Sql = Sql & "And produto_id in (12, 121, 135, 136, 716)"
'    Sql = Sql & "Order By nome " & vbNewLine
'    Call CarregaCombo(cmbProduto, Sql, lstProduto)
    
    Call CentraFrm(Me)
    
    'stbPrincipal.SimpleText = "Selecione o Produto."
    
    'Iniciando o processo automaticamente caso seja uma chamada do Scheduler
    'If Obtem_agenda_diaria_id(Command) > 0 Then
       Me.Show
       Call cmdOk_Click
       TerminaSEGBR
    'End If

    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Erro: " & Err.Number & " - " & Err.Description
    End If
   
End Sub

Public Sub CarregaCombo(CMB As ComboBox, Sql As String, LST As ListBox)
    
    Dim rsCombo As rdoResultset
    
    On Erro GoTo TrataErro
    
    Set rsCombo = rdocn.OpenResultset(Sql)
    
    'limpa o combo
    CMB.Clear
    LST.Clear
    
    'preenche o combo
    While Not rsCombo.EOF
        CMB.AddItem rsCombo(0)
        LST.AddItem rsCombo(1)
        rsCombo.MoveNext
    Wend
    
    'fecha o recordest
    rsCombo.Close
    
TrataErro:
    If Err.Number <> 0 Then
        MsgBox "Erro: " & Err.Number & " - " & Err.Description
    End If
    Set rsCombo = Nothing
End Sub

Private Sub Processa_SEGA()
     
    Dim rcSeleciona As rdoResultset
    Dim Sql As String
    
    On Error GoTo TrataErro
    
    'Selecionando dados da proposta, nome e endereco para gera��o do cerficado'''''''''''
    Sql = "exec SEGS5641_SPS"

    rdocn1.Execute (Sql)

    Sql = ""
    Sql = Sql & " SELECT * FROM ##SEGA8052_tb "
    
    Set rcSeleciona = rdocn1.OpenResultset(Sql)

    If Not rcSeleciona.EOF Then
        Conta_Linha_certificado = 0
        conta_linha = 0
        qtdReg = 0
        ProdutoAnt = 0
    Else
        txtfim.Text = Now
        txtfim.Refresh
        MensagemBatch "Nenhuma proposta foi selecionada", vbExclamation
        rcSeleciona.Close
        stbPrincipal.SimpleText = "Selecione o Produto."
        Exit Sub
    End If
    
'17/10/2003 - removido deste ponto
'    Produto = rcSeleciona(11)
'    Ler_Proposta_Basica
'    Obtem_Dados_Estipulante
    
    rdocn.BeginTrans
    
    While Not rcSeleciona.EOF
         
        DoEvents
        
        Produto = rcSeleciona(11)
        If ProdutoAnt <> Produto Then        '17/10/2003
            Ler_Proposta_Basica
            Obtem_Dados_Estipulante
            ProdutoAnt = Produto
        End If
        
        'Obntendo dados do certificado'''''''''''''''''''''''''''''''''''''''''''''''''''''
         
        Val_Premio = rcSeleciona!Val_Premio & ""
        Val_is_novo = rcSeleciona(35)
        Seguradora = rcSeleciona(15)
        Sucursal = Format(rcSeleciona(16), "00")
        ramo = Format(rcSeleciona(17), "00")
        Apolice = Format(rcSeleciona(18), "00000000")
        Banco = Format(rcSeleciona(19), "0000")
        agencia = Format(rcSeleciona(20), "0000")
        PropostaBB = Format(rcSeleciona(21), "000000000")
        PropostaBB_ant = Format(rcSeleciona(22), "000000000")
        Tp_Registro = Format(Trim(rcSeleciona(23)), "00")

        proposta = Format(rcSeleciona(0), "000000000")
        nome = rcSeleciona(1)
        Endereco = rcSeleciona(2)
        Endereco_CI = rcSeleciona(28)
        Bairro = rcSeleciona(3)
        Bairro_CI = rcSeleciona(29)
        Municipio = rcSeleciona(4)
        Municipio_CI = rcSeleciona(34)
        Cep = Format(rcSeleciona(5), "00000000")
        Cep = Left(Cep, 5) & "-" & Right(Cep, 3)
        Cep_CI = Format(rcSeleciona(31), "00000000")
        Cep_CI = Left(Cep_CI, 5) & "-" & Right(Cep_CI, 3)
        UF = rcSeleciona(6) 'UF de acordo com sele��o
        UF_CI = rcSeleciona(32)
        Flag_Corretor = rcSeleciona(27)
        DtInicioVigencia = Format(rcSeleciona(7), "dd/mm/yyyy")
        CPF = rcSeleciona(8)
        CPF = Left(CPF, 3) & "." & Mid(CPF, 4, 3) & "." & Mid(CPF, 7, 3) & "-" & Right(CPF, 2)
        num_solicitacao_reemissao = rcSeleciona(9)
        Dt_Nascimento = Format(rcSeleciona(10), "dd/mm/yyyy")
        EndossoId = Format(rcSeleciona!endosso_id, "000000000")
        OrigemEndosso = rcSeleciona(33) & ""
        TpDocumento = rcSeleciona!via
        Dt_Emissao = Format(rcSeleciona!dt_avaliacao, "dd/mm/yyyy")
        
        NomeRamo = (Left(rcSeleciona!nome_ramo & Space(30), 30))
        NomeProduto = (Left(rcSeleciona!nome_produto & Space(40), 40))
        
        Obtem_Nome_Plano
        Obtem_Dados_Coberturas
        Obtem_Dados_Conjuge

        If Conta_Linha_certificado = 0 Then
            Abre_Arquivo
        End If
        
        Certificado = Format(rcSeleciona(26), "0000000000")
        
        If Not ConfiguracaoBrasil Then
            IS1_Tit = TrocaValorAmePorBras(IS1_Tit)
            IS1_Tit = Left("R$" & IS1_Tit & String(15, " "), 15)
            IS2_Tit = TrocaValorAmePorBras(IS2_Tit)
            IS2_Tit = Left("R$" & IS2_Tit & String(15, " "), 15)
            IS3_Tit = TrocaValorAmePorBras(IS3_Tit)
            IS3_Tit = Left("R$" & IS3_Tit & String(15, " "), 15)
            IS4_Tit = TrocaValorAmePorBras(IS4_Tit)
            IS4_Tit = Left("R$" & IS4_Tit & String(15, " "), 15)

            IS1_Conj = TrocaValorAmePorBras(IS1_Conj)
            IS1_Conj = Left("R$" & IS1_Conj & String(15, " "), 15)
            IS2_Conj = TrocaValorAmePorBras(IS2_Conj)
            IS2_Conj = Left("R$" & IS2_Conj & String(15, " "), 15)
            IS3_Conj = TrocaValorAmePorBras(IS3_Conj)
            IS3_Conj = Left("R$" & IS3_Conj & String(15, " "), 15)
            IS4_Conj = TrocaValorAmePorBras(IS4_Conj)
            IS4_Conj = Left("R$" & IS4_Conj & String(15, " "), 15)
        End If

        Mascara_Valores_Zerados_Sigla_Real
                 
        Processa_SEGA_8052
         
        txtTotReg = Conta_Linha_certificado
        txtTotReg.Refresh
                   
        conta_linha = conta_linha + 1
        txtRegProc = conta_linha
        txtRegProc.Refresh
        
        stbPrincipal.SimpleText = "Gerando Registros."
                 
        Call AtualizaCertificadoImpressoGE(proposta)
        
        'Gravando dados em evento_seguros_db..evento_tb
        Call GravaEventoSeguros
        
        sSQL = ""          '17/10/2003
        sSQL = sSQL & "exec evento_seguros_db..evento_impressao_spi " & _
               proposta & _
               ", " & EndossoId & _
               ", null " & _
               ", null " & _
               ", 'C'" & _
               ", 05" & _
               ",'i'" & _
               ", null" & _
               ", 0" & _
               ", 'E'" & _
               ", '' " & _
               ", '' " & _
               ", '" & cUserName & "'" & _
               ", '" & Format(Date, "yyyymmdd") & "'" & _
               ", '" & Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000") & "'"
                   
        rdocn.Execute (sSQL)
        
        rcSeleciona.MoveNext

    Wend
    
    rcSeleciona.Close
    
    Sql = ""
    Sql = Sql & " DROP TABLE ##SEGA8052_tb"
    
    rdocn1.Execute (Sql)
    
    '#  grava trailler do arquivo_certificado
    If Conta_Linha_certificado > 0 Then
        linha = "99" & Format(Conta_Linha_certificado, "000000")
        linha = Left(linha + Space(tam_reg), tam_reg)
        Print #1, linha
        Close #1
    End If
   
    If Conta_Linha_certificado > 0 Then
        'Atualizando dados em arquivo_versao_gerado_tb
        Call Insere_Arquivo_Versao_Gerado(Trim(Nome_Rel_Certificado), qtdReg, Conta_Linha_certificado + 2, CInt(NumRemessa))
    End If
   
    rdocn.CommitTrans
       
    stbPrincipal.SimpleText = "Selecione o Produto."
    cmdOk.Enabled = False
    Exit Sub
    
TrataErro:
  
    TrataErroGeral "Processa_Sega", Me.name
    TerminaSEGBR
Resume
End Sub

Private Sub Ler_Proposta_Basica()
    
    Dim rcSeleciona As rdoResultset
       
    On Error GoTo MSGError
    
    Sql = "SELECT  a.proposta_id, a.ramo_id,  a.seguradora_cod_susep, " & vbNewLine
    Sql = Sql & "  a.sucursal_seguradora_id                           " & vbNewLine
    Sql = Sql & " FROM    proposta_basica_vida_tb c,                  " & vbNewLine
    Sql = Sql & "         proposta_tb b,                              " & vbNewLine
    Sql = Sql & "         proposta_basica_tb a,                       " & vbNewLine
    Sql = Sql & "         apolice_tb d                                " & vbNewLine     '17/10/2003
    Sql = Sql & " WHERE   a.proposta_id = b.proposta_id and           " & vbNewLine
    Sql = Sql & "         a.proposta_id = c.proposta_id and           " & vbNewLine
    Sql = Sql & "         a.proposta_id = d.proposta_id and           " & vbNewLine
    Sql = Sql & "         d.dt_fim_vigencia is null     and           " & vbNewLine
    Sql = Sql & "         b.produto_id = " & Produto & vbNewLine

    Set rcSeleciona = rdocn.OpenResultset(Sql)
    
    If Not rcSeleciona.EOF Then
        Proposta_Basica = rcSeleciona(0)
    Else
        linha = "Proposta B�sica n�o encontrada para o produto " & Produto & Chr(13) & Chr(10)
        MensagemBatch linha
    End If
    
    rcSeleciona.Close
    
    Exit Sub

MSGError:
    If Err.Number <> 0 Then
        MsgBox "Erro: " & Err.Number & " - " & Err.Description
    End If

End Sub

Private Sub Obtem_Dados_Estipulante()

    Dim rcSeleciona As rdoResultset
    
    On Error GoTo MSGError

    Sql = "SELECT a.nome                                  " & vbNewLine
    Sql = Sql & " FROM cliente_tb a                       " & vbNewLine
    Sql = Sql & " INNER JOIN administracao_apolice_tb b   " & vbNewLine
    Sql = Sql & "      ON a.cliente_id = b.est_cliente_id " & vbNewLine
    Sql = Sql & " WHERE"
    Sql = Sql & "      b.proposta_id = " & Proposta_Basica & vbNewLine

    Set rcSeleciona = rdocn.OpenResultset(Sql)
    
    If Not rcSeleciona.EOF Then
        Estipulante = Left(UCase(rcSeleciona(0)) & Space(60), 60)
    Else
        Estipulante = Space(60)
    End If
       
    rcSeleciona.Close
    
    Exit Sub

MSGError:
    If Err.Number <> 0 Then
        MsgBox "Erro: " & Err.Number & " - " & Err.Description
    End If

End Sub

Private Sub Obtem_Nome_Plano()

    Dim rcSeleciona As rdoResultset
          
    On Error GoTo Erro
    
    Sql = Sql & "select a.plano_id, b.tp_plano_id, c.nome, c.tp_plano_id" & vbNewLine
    Sql = Sql & " from escolha_plano_tb a, plano_tb b, tp_plano_tb c" & vbNewLine
    Sql = Sql & " WHERE a.proposta_id = " & proposta & vbNewLine
    Sql = Sql & " and a.plano_id = b.plano_id" & vbNewLine
    Sql = Sql & " and a.dt_fim_vigencia is null" & vbNewLine
    Sql = Sql & " and b.produto_id = 136" & vbNewLine
    Sql = Sql & " and c.tp_plano_id = b.tp_plano_id"
    
    Set rcSeleciona = rdocn.OpenResultset(Sql)
   
    If Not rcSeleciona.EOF Then
        NomePlano = rcSeleciona!nome
        Plano = Format(rcSeleciona!plano_id, "0000")
        TpPlano = rcSeleciona(1)
        TipoPlano = rcSeleciona(3)
        Set rcSeleciona = Nothing
    Else
        Call MensagemBatch("Dados da escolha plano referentes a proposta" & proposta & "n�o encontrados.")
        Call TerminaSEGBR
    End If
    
    Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Escolha_Plano", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Coberturas()
   
    Dim rcSeleciona  As rdoResultset

    On Error GoTo Erro
    
    Sql = "SELECT distinct" & vbNewLine
    Sql = Sql & " c.lim_max_cobertura," & vbNewLine
    Sql = Sql & " c.tp_componente_id," & vbNewLine
    Sql = Sql & " c.tp_cobertura_id " & vbNewLine
    Sql = Sql & " FROM" & vbNewLine
    Sql = Sql & " tp_plano_tp_comp_tb a," & vbNewLine
    Sql = Sql & " tp_cob_comp_plano_tb b," & vbNewLine
    Sql = Sql & " tp_cob_comp_tb c" & vbNewLine
    Sql = Sql & " WHERE" & vbNewLine
    Sql = Sql & " a.tp_plano_id = " & TpPlano & vbNewLine
    Sql = Sql & " and a.tp_plano_id = b.tp_plano_id" & vbNewLine
    Sql = Sql & " and b.tp_cob_comp_id = c.tp_cob_comp_id" & vbNewLine
    Sql = Sql & " ORDER BY" & vbNewLine
    Sql = Sql & " c.tp_componente_id, c.tp_cobertura_id" & vbNewLine
    
    Set rcSeleciona = rdocn.OpenResultset(Sql)
    
    While Not rcSeleciona.EOF
        
        If ConfiguracaoBrasil Then
            valor = Val(rcSeleciona(0))
        Else
            valor = rcSeleciona(0)
        End If
        
        Select Case rcSeleciona(2)
            Case 5
                ' 38 (desenvolvimento) morte natural
                ' 1 = titular   3 = conjuge (produ�ao)  13 (titular) e 14(conjuge) (desenvolvimento)
                ' 13 = titular  14 =conjuge (desenvolvimento)
                If rcSeleciona(1) = 1 Then
                    PercCobTit1 = valor / 100
                Else
                    PercCobConj1 = valor / 100
                End If
            Case 2 '39 (desenvolvimento) morte acidental (IEA)
                If rcSeleciona(1) = 1 Then
                    PercCobTit2 = valor / 100
                Else
                    PercCobConj2 = valor / 100
                End If
            Case 3 ' 46 (desenvolvimento) invalidez permanente total (IPA)
                If rcSeleciona(1) = 1 Then
                    PercCobTit3 = valor / 100
                Else
                    PercCobConj3 = valor / 100
                End If
            Case 4, 242 ' 47 (desenvolvimento) invalidez permanente por doen�a (IPD)(DT)
                If rcSeleciona(1) = 1 Then
                    PercCobTit4 = valor / 100
                Else
                    PercCobConj4 = valor / 100
                End If
        End Select
        
        rcSeleciona.MoveNext
    
    Wend
    
    rcSeleciona.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Coberturas", Me.name
    TerminaSEGBR
    
End Sub

Private Sub Obtem_Dados_Conjuge()
   
    Dim rcSeleciona As rdoResultset

    On Error GoTo Erro
   
    Sql = "SELECT" & vbNewLine
    Sql = Sql & " c.nome," & vbNewLine
    Sql = Sql & " b.cpf" & vbNewLine
    Sql = Sql & " FROM" & vbNewLine
    Sql = Sql & " proposta_complementar_tb a," & vbNewLine
    Sql = Sql & " pessoa_fisica_tb b," & vbNewLine
    Sql = Sql & " cliente_tb c" & vbNewLine
    Sql = Sql & " WHERE" & vbNewLine
    Sql = Sql & " a.proposta_id = " & proposta & vbNewLine
    Sql = Sql & " and a.prop_cliente_id = b.pf_cliente_id" & vbNewLine
    Sql = Sql & " and cliente_id = prop_cliente_id" & vbNewLine
    Sql = Sql & " and a.situacao <> 'c'" & vbNewLine
      
    Set rcSeleciona = rdocn.OpenResultset(Sql)
    
    If Not rcSeleciona.EOF Then
    
        Nome_Conj = Left(rcSeleciona(0) + Space(60), 60)
        CPF_Conj = rcSeleciona(1)
        CPF_Conj = Left(CPF_Conj, 3) & "." & Mid(CPF_Conj, 4, 3) & "." & Mid(CPF_Conj, 7, 3) & "-" & Right(CPF_Conj, 2)
        Componente_Conjuge = "ok"
        NomePlano = "Titular & C�njuge"
          
    Else
     
        Nome_Conj = Left(String(40, "*") & Space(60), 60)
        CPF_Conj = String(14, "*")
        Componente_Conjuge = ""
        NomePlano = "Exclusivo para Titular"
             
    End If
       
    rcSeleciona.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "Obtem_Dados_Conjuge", Me.name
    TerminaSEGBR
    
End Sub

Private Function Abre_Arquivo() As Variant
  
    Dim vAbre_arquivo As Variant
    Dim tam_reg       As Integer
    
    On Error GoTo Erro
    
    retensao = ""
    DataHora = Trim(Format(Data_Sistema, "dd/mm/yyyy hh:mm AMPM")) 'joconceicao 11/jul/01
    DataHora = DataHora & "  "
    Data = Format(Data_Sistema, "ddmmyyyy")
    tam_reg = 440
    
    vAbre_arquivo = Obtem_Num_Remessa(Trim(Nome_Rel_Certificado), NumRemessa)
    
    nome_arq = Certificado_path & Trim(Nome_Rel_Certificado) & "." & Format(NumRemessa, "0000") 'Data & ".txt"
    Open nome_arq For Output As 1
         
    linha = "01" & Trim(Nome_Rel_Certificado) & Format(NumRemessa, "00000") & Data
    linha = Left(linha + Space(tam_reg), tam_reg)
    Print #1, linha
    txtArquivo = Trim(Nome_Rel_Certificado)
    txtVersao = Format(NumRemessa, "0000")
    Abre_Arquivo = vAbre_arquivo
        
    Exit Function
    
Erro:
    TrataErroGeral "Abre_Arquivo", Me.name
    TerminaSEGBR
   
End Function

Function Obtem_Num_Remessa(nome As String, ByRef NumRemessa As String) As Variant

    Dim Sql As String
    Dim rcNum As rdoResultset
    Dim lObtem_Num_Remessa() As Integer
    ReDim lObtem_Num_Remessa(0 To 1)
    On Error GoTo Erro
    
    Sql = ""
    Sql = Sql & "SELECT" & vbNewLine
    Sql = Sql & "     l.layout_id" & vbNewLine
    Sql = Sql & " FROM" & vbNewLine
    Sql = Sql & "     controle_proposta_db..layout_tb l" & vbNewLine
    Sql = Sql & " WHERE" & vbNewLine
    Sql = Sql & "     l.nome = '" & nome & "'" & vbNewLine
    
    Set rcNum = rdocn.OpenResultset(Sql)
    
    If rcNum.EOF Then
        Error 1000
    Else
        Layout = rcNum(0)
        lObtem_Num_Remessa(0) = rcNum(0)
        Sql = ""
        Sql = Sql & "SELECT" & vbNewLine
        Sql = Sql & "     max(a.versao)" & vbNewLine
        Sql = Sql & " FROM" & vbNewLine
        Sql = Sql & "     controle_proposta_db..arquivo_versao_gerado_tb a" & vbNewLine
        Sql = Sql & " WHERE" & vbNewLine
        Sql = Sql & "     a.layout_id = " & lObtem_Num_Remessa(0) & vbNewLine
        
        rcNum.Close
        
        Set rcNum = rdocn.OpenResultset(Sql)
        
        If Not rcNum.EOF Then
            lObtem_Num_Remessa(1) = IIf(IsNull(rcNum(0)), 0, rcNum(0)) + 1
            NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "000000"), Format(rcNum(0) + 1, "000000"))
            Obtem_Num_Remessa = lObtem_Num_Remessa
            GoTo Fim
        End If
        
    End If
    Set Obtem_Num_Remessa = Nothing
Fim:
    rcNum.Close
    Set rcNum = Nothing

    Exit Function

Erro:
    If Err.Number = 1000 Then
       MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "Obtem_Num_Remessa", Me.name
    TerminaSEGBR

End Function

Private Sub Processa_SEGA_8052()

    On Error GoTo Erro

    'Monta o certificado do OuroVida Grupo Especial ''''''''''''''

    qtdReg = qtdReg + 1
    Conta_Linha_certificado = Conta_Linha_certificado + 1
    
    tam_reg = 485
    
    Tp_Seguro = NomePlano
    
    'Header
    Cod_Retorno = "05" & Format(proposta, "000000000") & ConverteParaJulianDate(CDate(Data_Sistema)) & Format(agencia, "0000")
    
    'Controle do documento
    linha = "10" & Format(Conta_Linha_certificado, "000000")  'tipo de registro e n� seq registro
    linha = linha & PropostaBB                                'proposta bb
    linha = linha & Left(Produto + Space(3), 3)               'produto_id
    linha = linha & Left(nome + Space(60), 60)                'nome do destinat�rio
    If Flag_Corretor = "S" Or Flag_Corretor = "s" Then
        linha = linha & Left(Endereco + Space(60), 60)            'endere�o do destino
        linha = linha & Left(Bairro + Space(30), 30)              'bairro do destino
        linha = linha & Left(Municipio + Space(30), 30)           'munic�pio de destino
        linha = linha & UF                                        'UF do destino
        linha = linha & Cep                                       'CEP do destino
    Else
        linha = linha & Left(Endereco_CI + Space(60), 60)         'endere�o do corretor independente
        linha = linha & Left(Bairro_CI + Space(30), 30)           'bairro do corretor independente
        linha = linha & Left(Municipio_CI + Space(30), 30)        'munic�pio de corretor independente
        linha = linha & UF_CI                                     'UF do corretor independente
        linha = linha & Cep_CI                                    'CEP do corretor independente
    End If
    linha = linha & Cod_Retorno                               'C�digo de barras
    linha = linha & Space(10)                                 'Ger�ncia solicitante
    linha = linha & Left(TpDocumento & Space(20), 20)         'Tipo do documento
    linha = linha & EndossoId                                 'N�mero do endosso
    If Trim(OrigemEndosso) = "" Then
        linha = linha & "AGENCIA"
    Else
        linha = linha & "CENTRAL"
    End If
    
    linha = Left(linha + Space(tam_reg), tam_reg)
    Print #1, linha

    'Segurado
    Conta_Linha_certificado = Conta_Linha_certificado + 1
      
    linha = "20" & Format(Conta_Linha_certificado, "000000")          'tipo de registro e n� seq registro
    linha = linha & PropostaBB                                        'proposta bb
    linha = linha & Format(Apolice, "000000000")                      'n� da ap�lice
    linha = linha & Format(Certificado, "0000000000")                 'n� do certificado
    linha = linha & Left(Estipulante + Space(60), 60)                 'Estipulante
    linha = linha & Left(CPF + Space(14), 14)                         'CPF do segurado
    linha = linha & Left(nome + Space(50), 50)                        'nome do segurado
    linha = linha & Dt_Nascimento                                     'data do nascimento
    linha = linha & Left(Endereco + Space(60), 60)                    'endere�o do segurado
    linha = linha & Left(Municipio & Space(45), 45)                   'cidade do segurado
    linha = linha & UF                                                'UF do segurado
    linha = linha & Cep                                               'CEP do segurado
    linha = linha & Dt_Emissao                                        'data de In�cio de vig�ncia
    linha = linha & Left(Tp_Seguro & Space(30), 30)                   'tipo do seguro
    linha = linha & Left("R$" & Format(Val(Val_Premio), "#,##0.00") & Space(15), 15)          'pr�mio mensal
    IS1_Tit = Format(Val(Val_is_novo), "#,##0.00")
    IS1_Tit = Left("R$" & IS1_Tit & String(15, " "), 15)
    linha = linha & IS1_Tit                                           'garantia b�sica segurado
    linha = linha & IS1_Tit                                           'valor IEA segurado
    linha = linha & IS1_Tit                                           'valor IPA segurado
    linha = linha & IS1_Tit                                           'valor DT segurado
    linha = linha & Data_Sistema                                      'data de emiss�o
    linha = linha & Format(TipoPlano, "0000")
    linha = linha & NomeRamo
    linha = linha & NomeProduto
    
    linha = Left(linha + Space(tam_reg), tam_reg)
    Print #1, linha
          
    'C�njuge
    
    Conta_Linha_certificado = Conta_Linha_certificado + 1
    
    linha = "21" & Format(Conta_Linha_certificado, "000000")              'tipo de registro e n� seq registro
    linha = linha & PropostaBB                                            'n� da proposta BB
    
    Nome_Conj = Left(Nome_Conj & Space(60), 60)
    linha = linha & IIf(Trim(Nome_Conj) = "", String(60, "*"), Nome_Conj)   'nome do c�njugue
        
    CPF_Conj = Left(CPF_Conj & Space(14), 14)
    
    Select Case CPF_Conj
        Case "000.000.000-00"
            CPF_Conj = Space(14)
        Case ""
            CPF_Conj = String(14, "*")
    End Select
    linha = linha & CPF_Conj     'CPF do c�njugue
  
    If Trim(Componente_Conjuge) <> "" Then
        
        IS1_Conj = Format(Val(Val_is_novo) / 2, "###,###,##0.00")
        IS1_Conj = Left("R$" & IS1_Conj & String(15, " "), 15)
 
        IS2_Conj = Format(Val(Val_is_novo) / 2, "###,###,##0.00")
        IS2_Conj = Left("R$" & IS2_Conj & String(15, " "), 15)
 
        IS3_Conj = Format(Val(Val_is_novo) / 2, "###,###,##0.00")
        IS3_Conj = Left("R$" & IS3_Conj & String(15, " "), 15)
 
        IS4_Conj = Format(Val(Val_is_novo) / 2, "###,###,##0.00")
        IS4_Conj = Left("R$" & IS4_Conj & String(15, " "), 15)
    Else
        IS1_Conj = String(15, "*")
        IS2_Conj = String(15, "*")
        IS3_Conj = String(15, "*")
        IS4_Conj = String(15, "*")
    End If
      
    linha = linha & IS1_Conj                                              'Valor da garantia b�sica do c�njugue
    linha = linha & IS2_Conj                                              'Valor IEA do c�njugue
    linha = linha & IS3_Conj                                              'Valor IPA do c�njugue
    linha = linha & IS4_Conj                                              'Valor DT do c�njugue
    
    
    linha = Left(linha + Space(tam_reg), tam_reg)
    
    Print #1, linha
            
    Exit Sub
   
Erro:
    TrataErroGeral "Processa_SEGA_8052", Me.name
    TerminaSEGBR

Resume
End Sub

Private Sub Mascara_Valores_Zerados_Sigla_Real()
  
    If Trim(IS1_Tit) = "R$0,00" Then
        IS1_Tit = String(15, "*")
    End If
  
    If Trim(IS2_Tit) = "R$0,00" Then
        IS2_Tit = String(15, "*")
    End If
  
    If Trim(IS3_Tit) = "R$0,00" Then
        IS3_Tit = String(15, "*")
    End If
  
    If Trim(IS4_Tit) = "R$0,00" Then
        IS4_Tit = String(15, "*")
    End If
  
    If Trim(IS1_Conj) = "R$0,00" Then
        IS1_Conj = String(15, "*")
    End If
  
    If Trim(IS2_Conj) = "R$0,00" Then
        IS2_Conj = String(15, "*")
    End If
  
    If Trim(IS3_Conj) = "R$0,00" Then
        IS1_Conj = String(15, "*")
    End If
  
    If Trim(IS4_Conj) = "R$0,00" Then
        IS1_Conj = String(15, "*")
    End If

End Sub

Sub AtualizaCertificadoImpressoGE(ByVal pPropostaId)

    Dim Sql                    As String
    Dim rsAtualizaCertificado  As rdoResultset

    On Error GoTo Erro
    
    'gravar na tabela endosso_tb, campo dt_impressao
    Sql = "exec endosso_certificado_ourovida_spu " & vbNewLine
    Sql = Sql & proposta & "," & vbNewLine
    Sql = Sql & EndossoId & vbNewLine
    
    Set rsAtualizaCertificado = rdocn.OpenResultset(Sql)
    rsAtualizaCertificado.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "AtualizaCertificadoImpressoGE", Me.name
    TerminaSEGBR
    
End Sub

Sub Insere_Arquivo_Versao_Gerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    Dim rcGer As rdoResultset
    Dim Sql As String
    
    On Error GoTo Erro
            
    Sql = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    Sql = Sql & nome & "'," & NumRemessa & "," & qReg & ",'"
    Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    Sql = Sql & cUserName & "'"
    Set rcGer = rdocn.OpenResultset(Sql)
        
    rcGer.Close
        
    Exit Sub

Erro:
    TrataErroGeral "Insere_Arquivo_Versao_Gerado", Me.name
    TerminaSEGBR
    
End Sub

Public Function ConverteParaJulianDate(ldate As Date) As String

    Dim lJulianDate  As String * 5
    On Error GoTo Erro

    lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
    lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
    ConverteParaJulianDate = lJulianDate

    Exit Function

Erro:
    TrataErroGeral "ConverteParaJulianDate", Me.name
    TerminaSEGBR

End Function

Private Sub GravaEventoSeguros()

    Dim Sql As String
    
    On Error GoTo Erro
    
    Sql = "Exec evento_seguros_db..evento_spi " & vbNewLine
    Sql = Sql & Val(proposta) & ", " & vbNewLine
    Sql = Sql & Val(PropostaBB) & ", " & vbNewLine
    Sql = Sql & "NULL, " & vbNewLine
    Sql = Sql & Produto & ", " & vbNewLine
    Sql = Sql & NumRemessa & ", " & vbNewLine
    Sql = Sql & Layout & ", " & vbNewLine
    Sql = Sql & "'" & Format(Conta_Linha_certificado, "000000") & "', " & vbNewLine
    Sql = Sql & "'SEGA8052', " & vbNewLine
    Sql = Sql & "'" & NumRemessa & "', " & vbNewLine
    Sql = Sql & "NULL, " & vbNewLine
    Sql = Sql & "NULL, " & vbNewLine
    Sql = Sql & "NULL, " & vbNewLine
    Sql = Sql & "'" & SIS_usuario & "', " & vbNewLine
    Sql = Sql & "'" & Format(Date, "yyyymmdd") & "'" & vbNewLine
    
    rdocn.Execute (Sql)

Erro:
    If Err.Number <> 0 Then
        MsgBox "Erro: " & Err.Number & " - " & Err.Description
    End If

End Sub
