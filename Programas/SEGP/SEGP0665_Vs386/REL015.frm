VERSION 5.00
Begin VB.Form REL015 
   Caption         =   "REL015"
   ClientHeight    =   1650
   ClientLeft      =   150
   ClientTop       =   1860
   ClientWidth     =   2670
   LinkTopic       =   "Form1"
   ScaleHeight     =   1650
   ScaleWidth      =   2670
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   1080
      TabIndex        =   0
      Top             =   600
      Width           =   1095
   End
End
Attribute VB_Name = "REL015"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'# Programa para Gerar Arquivo da Primeira Via de Documentos
'# (Certificado + Cart�o 24h ...)
'# Layout Unificado para todos os produtos (Ourovida Garantia gera arquivo anterior - REL010)

'# declara��o de conex�o auxiliar
Public aNroRegArquivo  As Long
Public cDestino As String
'Dim ConnAdoAux  As New adodb.Connection
'Dim rdocn1  As New rdoConnection           'comentado em 23/09/2003
'Dim rdocn2  As New rdoConnection

'# declara��o de constantes
Const cteProdutoIdGarantia = "11"
Const cteProdutoIdOuroVidaAntigo = "12"
Const cteProdutoIdOurovida = "121"
Const cteProdutoIdBrasilVida = "122"
Const cteProdutoIdOuroVidaProdutorRural = "14"
Const cteProdutoIdOuroVida2000 = "135"
Const cteProdutoIdOuroAgricola = "145"
Const cteProdutoIdPenhorRural = "155"
Const cteTamanhoRegistroUnificado = 400
Const cteTamanhoRegistroGarantia = 263
Const cteTpRegistroRenovacao = "07"
Const cteTipoDocumento = "05"           'fixo no layout anterior

'# globais
Dim arquivo_remessa                 As String
Dim wNew                            As Boolean
Dim gPropostaId                     As Variant
Dim gPropostaIdAnt                  As Variant
Dim gSeguradora                     As String
Dim gSeguradoraAnt                  As String
Dim gSucursal                       As String
Dim gSucursalAnt                    As String
Dim gRamo                           As String
Dim gRamoAnt                        As String
Dim gApolice                        As String
Dim gApoliceAnt                     As String
Dim gPropostaBBAnterior             As String
Dim gBanco                          As String
Dim gAgenciaAnt                     As String
Dim gTpRegistro                     As String
Dim gListaProdutoIdSel              As String   'produto id ou lista selecionado
Dim gOrigemProdutoIdSel             As String   'origem do produto selecionado
Dim gArquivoProdutoIdSel            As String   'arquivo produto recebido para o produto selecionado
Dim gDestinoProdutoIdSel            As String   'destino para o produto selecionado
Dim gNomeArquivoDocProdutoIdSel     As String   'nome do arquivo a ser gerado para o produto selecionado
Dim gConfiguracaoBrasil             As Boolean
Dim gImportanciaSegurada            As Double
Dim gTpPlano                        As String
Dim gTpPlanoAnt                     As String
Dim gPercCobMorteNaturalTit         As Double
Dim gPercCobIEATit                  As Double
Dim gPercCobIPATit                  As Double
Dim gPercCobIPDTit                  As Double
Dim gPercCobMorteNaturalConj        As Double
Dim gPercCobIEAConj                 As Double
Dim gPercCobIPAConj                 As Double
Dim gPercCobIPDConj                 As Double
Dim gComponenteConjuge              As String
Dim gNomeProduto                    As String
Dim gDtEmissao                      As String
Dim gVetProdutos()                              'armazena os dados referentes aos produtos
Dim NumRemessa                      As String

'# Campos do arquivo globais
Dim gcpoNumeroCertificado           As String * 8
Dim gcpoNumeroCertificadoAnt        As String * 8
Dim gcpoDVCertificado               As String * 1
Dim gcpoCodAgencia                  As String * 4
Dim gcpoDVCodAgencia                As String * 1
Dim gcpoNumeroProposta              As String * 9
Dim gcpoNomeAgenciaCorretor         As String * 40
Dim gcpoMunicipioAgencia            As String * 60
Dim gcpoUFAgencia                   As String * 2
Dim gcpoCEPAgencia                  As String * 8
Dim gcpoEnderecoAgencia             As String * 85
Dim gcpoTelCorretor                 As String * 15
Dim gcpoNomeConjuge                 As String * 50
Dim gcpoCPFConjuge                  As String * 14
Dim gcpoPlanoId                     As String * 4
Dim gcpoTipoSeguro                  As String * 1
Dim gcpoValorPremio                 As String * 14 'atencao atencao so 14 bytes aqui!!!!!!
Dim gcpoResp1Tit                    As String * 1
Dim gcpoDescr1Tit                   As String * 60
Dim gcpoResp2Tit                    As String * 1
Dim gcpoDescr2Tit                   As String * 60
Dim gcpoResp3Tit                    As String * 1
Dim gcpoDescr3Tit                   As String * 60
Dim gcpoResp4Tit                    As String * 1
Dim gcpoDescr4Tit                   As String * 60
Dim gcpoResp5Tit                    As String * 1
Dim gcpoDescr5Tit                   As String * 60
Dim gcpoResp1Compl                  As String * 1
Dim gcpoDescr1Compl                 As String * 60
Dim gcpoResp2Compl                  As String * 1
Dim gcpoDescr2Compl                 As String * 60
Dim gcpoResp3Compl                  As String * 1
Dim gcpoDescr3Compl                 As String * 60
Dim gcpoResp4Compl                  As String * 1
Dim gcpoDescr4Compl                 As String * 60
Dim gcpoResp5Compl                  As String * 1
Dim gcpoDescr5Compl                 As String * 60


'# flags
Dim FlagPropBasica                  As Boolean
Dim FlagExisteCertificado           As Boolean
Dim Flag_Tem_endosso                As Boolean


'Dados do Ouro Agr�cola ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    
Dim SistemaCultivoId                As String
Dim DescSistemaCultivo              As String
Dim TpCulturaId                     As String
Dim DescTpCultura                   As String
Dim SistemaProducao                 As String
Dim AreaSegHectare                  As String
Dim NomePropriedade                 As String
Dim ProducaoEsperada                As String
Dim IndLavouraPlant                 As String
Dim NumInspPrevia                   As String
Dim DtInspPrevia                    As String
Dim NumContrato                     As String
Dim DtInicioFinanc                  As String
Dim DtFimFinanc                     As String
Dim DtIniVigSeguro                  As String
Dim DtFimVigSeguro                  As String

Dim EnderecoRisco                   As String
Dim MunicipioRisco                  As String
Dim UFRisco                         As String
Dim CepRisco                        As String
Dim MunicipioIBGE                   As String
Dim Imp_Seg_Hectare_Arq             As String
Dim Imp_Seg_Arq                     As String
Dim Val_Premio_Arq As String

Dim cpoISMorteNaturalTit            As String * 14
Dim cpoISIEATit                     As String * 14
Dim cpoISIPATit                     As String * 14
Dim cpoISIPDTit                     As String * 14
Dim cpoISMorteNaturalConj           As String * 14
Dim cpoISIEAConj                    As String * 14
Dim cpoISIPAConj                    As String * 14
Dim cpoISIPDConj                    As String * 14

Dim aValorIS             As Currency
Dim PropostaBasica       As String
Dim cpoProduto           As String * 3
Dim NomeEstipulante      As String


'Dados do Penhor RuraL ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Dim CodigoReferencia            As String
Dim NumeroContrato              As String
Dim PrefixoContrato             As String
Dim AnoContrato                 As String
Dim ValorSegurado               As String
Dim ValorPremio                 As String
Dim TaxaRisco                   As String
Dim DataEmissao                 As String
Dim DescBemSegurado             As String
Dim cpoDDD                      As String
Dim cpoTelefone                 As String
Dim CodObjetoSegurado           As String
Dim CodObjetoSegurado_Anterior  As String
Dim ValorSeguradoTotal          As Currency
Dim ValorPremioTotal            As Currency
Dim InicioPenhorRural           As Boolean
Dim num_rsa                     As String
Dim emi_re_proposta_id          As Long
Dim logpath                     As String
Dim Proposta_id_LogPnhRural     As String

Sub ObtemNumeroCertificadoReservado()

    On Error GoTo Erro
    
    Dim Rst As rdoResultset
    
    Sql = "Select certificado_id From certificado_tb "
    Sql = Sql & " Where proposta_id = " & gPropostaId
    
    Set Rst = rdocn1.OpenResultset(Sql)
    
    If Not Rst.EOF Then
        gcpoNumeroCertificado = Format(Rst(0), "00000000")
        gcpoDVCertificado = Modulo11(gcpoNumeroCertificado)
    Else
        TrataErroGeral "O n�mero do certificado da proposta REL015", Me.name
        TerminaSEGBR
    End If
    
    Exit Sub
Erro:
    TrataErroGeral "ObtemNumeroCertificadoReservado REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub Form_Activate()

On Error GoTo Erro

    Call cmdOk_Click
    
    Unload Me

Exit Sub
    
Erro:
    TrataErroGeral "Form_Activate REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub Form_Load()

    On Error GoTo Erro
  
    'gConfiguracaoBrasil = BuscaConfigDecimal
    gConfiguracaoBrasil = True
    
    logpath = LerArquivoIni("Producao", "LOG_PATH")
    
    'ConexaoAuxiliar          'comentado em 23/09/2003
    
    Me.Caption = "Gera Arquivo Emiss�o de Documentos" & " - " & Ambiente
    
    Exit Sub
    
Erro:
    TrataErroGeral "Form_Load REL015", Me.name
    TerminaSEGBR
   
End Sub

Sub ConexaoAuxiliar()
   
On Error GoTo Erro

    With rdocn1
        .Connect = rdocn.Connect
        .CursorDriver = rdUseNone
        .QueryTimeout = 60000
        .EstablishConnection rdDriverNoPrompt
    End With
    
    With rdocn2
        .Connect = rdocn.Connect
        .CursorDriver = rdUseNone
        .QueryTimeout = 60000
        .EstablishConnection rdDriverNoPrompt
    End With
          
    Exit Sub

Erro:
    TrataErroGeral "Conex�o indispon�vel REL015", Me.name
    TerminaSEGBR
End Sub

Public Function ConverteParaJulianDate(ldate As Date) As String
   
   Dim lJulianDate As String * 5
   
   On Error GoTo Trata_Erro
 
   lJulianDate = DateDiff("d", CDate("01/01/" & Year(ldate)), ldate) + 1
   lJulianDate = Format(ldate, "yy") & Format(Trim(lJulianDate), "000")
   ConverteParaJulianDate = lJulianDate
 
   Exit Function
 
Trata_Erro:
   Call TrataErroGeral("ConverteParaJulianDate", "modSEGP0241")
 
End Function

Function Modulo11(ByVal numero As String) As String

    Dim Ponteiro_Numero As Integer
    Dim Multiplicador As Integer
    Dim Somatoria As Integer
    Dim resto As Integer
    Dim Digito_Verificador

    numero = Right(String(50, "*") + Trim(numero), 50)
    
    Ponteiro_Numero = 50
    
    Multiplicador = 2
    
    Somatoria = 0
    
    While Ponteiro_Numero > 0 And Mid$(numero, Ponteiro_Numero, 1) <> "*"
        Somatoria = Somatoria + (Val(Mid$(numero, Ponteiro_Numero, 1)) * Multiplicador)
        
        Ponteiro_Numero = Ponteiro_Numero - 1
        
        Multiplicador = Multiplicador + 1
        If Multiplicador = 10 Then
            Multiplicador = 2
        End If
    Wend
    
    resto = Somatoria Mod 11
    
    If 11 - resto > 9 And resto <> 0 Then
        Digito_Verificador = "X"
    Else
        If resto = 0 Then
            Digito_Verificador = "0"
        Else
            Digito_Verificador = Trim(Str(11 - resto))
        End If
    End If

    Modulo11 = Digito_Verificador
    
End Function

Private Sub MontaListaProduto()

    On Error GoTo Erro
    
    Const TpRamoVida = 1
    Const cteQtdInfProduto = 6
'# informa��es referentes ao produto :
'# ProdutoId + ProdutoAnteriorId + Origem + NomeArquivo + Destino (header do arquivo) + Nome Arquivo Documentos (arquivo a ser gerado)
    
'# Quando o processamento for v�lido para outros produtos, os nomes dos arquivos a serem gerados
'# devem ser definidos na query abaixo

    Dim rsProd      As rdoResultset
    Dim C           As Integer
    Dim i           As Integer
    
    cmbProduto.Clear
    
    Sql = "SELECT Distinct a.nome, a.produto_id, "
    Sql = Sql & " IsNull(a.produto_anterior_id,0),"
    Sql = Sql & " b.origem,"
    Sql = Sql & " b.arquivo,"
    Sql = Sql & " destino = case a.produto_id when "
    Sql = Sql & cteProdutoIdBrasilVida & " then 'CORRE' when "
    Sql = Sql & cteProdutoIdPenhorRural & " then 'AGENC' else 'CLIEN' end,"
    Sql = Sql & " arquivo_documentos = case a.produto_id when "
    Sql = Sql & cteProdutoIdGarantia & " then 'REL010' when "
    Sql = Sql & cteProdutoIdOuroAgricola & " then 'REL011' when "
    Sql = Sql & cteProdutoIdOuroVidaProdutorRural & " then 'REL012' when "
    Sql = Sql & cteProdutoIdOurovida & " then 'REL015' when "
    Sql = Sql & cteProdutoIdOuroVida2000 & " then 'REL018' when "
    Sql = Sql & cteProdutoIdPenhorRural & " then 'REL108' when "
    Sql = Sql & cteProdutoIdBrasilVida & " then 'REL016' else '' end"
    Sql = Sql & " FROM produto_tb a, arquivo_produto_tb b"
    Sql = Sql & " WHERE a.produto_id = b.produto_id"
    Sql = Sql & " and ativo = 's'"
    Sql = Sql & " and a.produto_id in"
    Sql = Sql & " (Select produto_id"
    Sql = Sql & " FROM item_produto_tb i, ramo_tb r, tp_ramo_tb t"
    Sql = Sql & " WHERE (i.ramo_id = r.ramo_id"
    Sql = Sql & " and r.tp_ramo_id = t.tp_ramo_id"
    Sql = Sql & " and i.dt_fim_vigencia is NULL"
    Sql = Sql & " and t.tp_ramo_id = " & TpRamoVida & ") or a.produto_id = 145 or a.produto_id = 155 ) "
    Sql = Sql & " ORDER BY a.nome "
    
    Set rsProd = rdocn.OpenResultset(Sql)
    
    '#inicializa contador de produtos selecionados
    C = 0
    
    While Not rsProd.EOF
        
        '# adiciona o produto na combo box
        cmbProduto.AddItem UCase(rsProd(0))                         'nome do produto
        cmbProduto.ItemData(cmbProduto.NewIndex) = CLng(rsProd(1))  'produto_id
        
        '# grava as informa��es adicionais do produto num vetor
        C = C + 1
        ReDim Preserve gVetProdutos(cteQtdInfProduto, C)
        For i = 1 To cteQtdInfProduto
            gVetProdutos(i, C) = rsProd(i)
        Next
        
        rsProd.MoveNext
    Wend
    
    rsProd.Close
    
    Exit Sub
Erro:
    TrataErroGeral "MontaListaProduto REL015", Me.name
    TerminaSEGBR

End Sub

Private Function getArquivoDocumento(arquivo_saida As String) As String
Dim Sql As String
Dim rc As rdoResultset
 
getArquivoDocumento = ""
 
Sql = " Select  tp_documento_id "
Sql = Sql & " from evento_seguros_db..documento_tb "
Sql = Sql & " where arquivo_saida = '" & Trim(arquivo_saida) & "'"
 
Set rc = rdocn.OpenResultset(Sql)
While Not rc.EOF
    getArquivoDocumento = IIf(Trim(getArquivoDocumento) = "", rc!tp_documento_id, getArquivoDocumento & "," & rc!tp_documento_id)
    rc.MoveNext
Wend
 
End Function


Private Sub cmdOk_Click()

    Dim sTpDocumentos As String

    On Error GoTo Erro
    
    '# Processamento realizado para Ourovida Garantia, Ourovida e Brasil Vida
    InicializaValores
       
    MousePointer = 11
        
    '# verifica se o produto selecionado possui um arquivo a ser gerado
    VerificaProdutoSelecionado
    
    sTpDocumentos = getArquivoDocumento("REL015")
    
    Call Processa(sTpDocumentos)
    
    Exit Sub
    
Erro:
    TrataErroGeral "cmdOk_Click REL015", Me.name
    TerminaSEGBR
    
End Sub

Sub VerificaProdutoSelecionado()

    On Error GoTo Erro
    
    tmpprodutoanterior = "12"
    
    gListaProdutoIdSel = "121,12"
    
    gOrigemProdutoIdSel = "BB"
    gArquivoProdutoIdSel = "BBC492%"
    gNomeArquivoDocProdutoIdSel = "REL015"
    
    Exit Sub
    
Erro:
    TrataErroGeral "VerificaSelecionado REL015", Me.name
    TerminaSEGBR

End Sub

Sub InicializaValores()
    
'# vari�veis globais

    On Error GoTo Erro
    
    gSeguradoraAnt = ""
    gSucursalAnt = ""
    gRamoAnt = ""
    gApoliceAnt = ""
    gAgenciaAnt = ""
    gBanco = ""
    gTpPlanoAnt = ""
    
    '# campos que s�o inicializados aqui por sempre serem atualizados
    '# durante o processamento de um registro do arquivo
    gcpoNumeroCertificado = ""   'rotina ObtemNumeroCertificado
    gcpoDVCertificado = ""       'idem
    gcpoCodAgencia = ""          'rotina BuscaDadosPropostaAdesao
    gcpoNumeroProposta = ""      'idem
    gcpoNomeAgenciaCorretor = "" 'rotina BuscaDadosAgencia ou BuscaDadosCorretor

    Exit Sub
    
Erro:
    TrataErroGeral "InicializaValores REL015", Me.name
    TerminaSEGBR
End Sub

Public Function InserirArquivoVersaoGerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)

    On Error GoTo Erro
            
    Sql = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
    Sql = Sql & nome & "'," & NumRemessa & "," & qReg & ",'"
    Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
    Sql = Sql & cUserName & "'"
    rdocn1.Execute (Sql)
    
    Exit Function

Erro:
    TrataErroGeral "InserirArquivoVersaoGerado", nome
    TerminaSEGBR
    
End Function

Private Sub Processa(ByVal sTpDocumentos As String)
   
'# Vari�veis Ref. Campos do Arquivo '''''''''''''''''''''''''''''''''''''''''''''''''''''

Dim cpoNumeroApolice                As String * 12
Dim cpoNomeSegurado                 As String * 50
Dim cpoDtNascimento                 As String * 10
Dim cpoSexo                         As String * 1
Dim cpoCPF                          As String * 14
Dim cpoEndereco                     As String * 50
Dim cpoCidade                       As String * 45
Dim cpoUF                           As String * 2
Dim cpoCEP                          As String * 9
Dim cpoProfissao                    As String * 60

Dim cpoDtInicioVigencia             As String * 10
Dim cpoDtFimVigencia                As String * 10
Dim cpoObservacao                   As String * 50
Dim cpoTipoDocumento                As String * 2

'# Vari�veis Ref. consultas''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Dim Found As Boolean

'# Vari�veis Auxiliares''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Dim aReg            As String                               'gera��o de registro
Dim aContaPropostas 'contador de propostas processadas
Dim cpoCPFAux       'aux�lio na defini��o do acampo CPF
Dim cpoCEPAux       'aux�lio na defini��o do campo CEP
Dim RegUnificado    As String * cteTamanhoRegistroUnificado 'registro formatado do layout unificado
Dim RegGarantia     As String * cteTamanhoRegistroGarantia  'registro formatado do layout certificado ourovida garantia

On Error GoTo ErroProcessa
   
    '# Consulta de informa��es da proposta a serem utilizados na gera��o do arquivo''''''
    Found = False
    Sql = ""
    Sql = Sql & "SELECT  a.proposta_id, "
    Sql = Sql & "b.nome, "
    Sql = Sql & "c.endereco, "
    Sql = Sql & "c.bairro, "
    Sql = Sql & "c.municipio, "
    Sql = Sql & "c.cep, "
    Sql = Sql & "c.estado, "
    Sql = Sql & "a.dt_contratacao, "
    Sql = Sql & "a.produto_id, "
    Sql = Sql & "d.cpf, "
    Sql = Sql & "d.dt_nascimento, "
    Sql = Sql & "'sexo' = isnull(d.sexo,'N'), "
    Sql = Sql & "'nome' = isnull(e.nome,' '), "
    Sql = Sql & "f.cgc, "
    Sql = Sql & "c.ddd, "
    Sql = Sql & "c.telefone, "
    Sql = Sql & "ssv.num_solicitacao, "
    Sql = Sql & "'DestinoParaEnvio'     = CASE "
    Sql = Sql & "WHEN ssv.destino = 'A' then 'AGENC' "
    Sql = Sql & "WHEN ssv.destino = 'D' then 'SEGUR' "
    Sql = Sql & "WHEN ssv.destino = 'C' then 'CLIEN' "
    Sql = Sql & "END, "
    Sql = Sql & "'DepartamentoDestinos' = Isnull(ips.descr_lotacao,' '), "
    Sql = Sql & "ssv.Destino "
    Sql = Sql & "FROM    evento_seguros_db..evento_impressao_tb ssv "
    Sql = Sql & "INNER   JOIN proposta_tb a ON ssv.proposta_id = a.proposta_id "
    Sql = Sql & "INNER   JOIN cliente_tb b ON a.prop_cliente_id = b.cliente_id "
    Sql = Sql & "LEFT    JOIN endereco_corresp_tb c ON a.proposta_id = c.proposta_id "
    Sql = Sql & "AND     (c.emite_documento is null or c.emite_documento = 'a') "
    Sql = Sql & "LEFT    JOIN pessoa_fisica_tb d ON a.prop_cliente_id = d.pf_cliente_id "
    Sql = Sql & "LEFT    JOIN pessoa_juridica_tb f ON a.prop_cliente_id = f.pj_cliente_id "
    Sql = Sql & "LEFT    JOIN profissao_tb e ON d.profissao_cbo = e.profissao_cbo "
    Sql = Sql & "LEFT    JOIN web_intranet_db..ips_lotacao_tb ips on ssv.diretoria_id = ips.lotacao_id "
    Sql = Sql & "WHERE   ssv.status = 'l' "
    Sql = Sql & "AND     ssv.dt_geracao_arquivo is NULL "
    Sql = Sql & "AND     ssv.tp_documento_id in (" & sTpDocumentos & ") "
    Sql = Sql & "AND     a.produto_id = 121 "
    Sql = Sql & "ORDER   BY a.proposta_id "
    
    Set rs = rdocn2.OpenResultset(Sql)
    
    aNroRegArquivo = 0
    
    aContaPropostas = 0
    
    InicioPenhorRural = True
        
    While Not rs.EOF
    
        rdocn.BeginTrans

        gPropostaId = rs(0)
        
        '# **** CONSULTA EVENTUAL EM CERTIFICADO_TB ****
        
        'SQL = "Select count(*) From certificado_tb"
        'SQL = SQL & " Where proposta_id = " & gPropostaId
        'Set rsAux = rdocn.execute(SQL)
        
        'If rsAux(0) = 0 Then
            
            '# N�o encontrou certificado para a proposta
            '# Assim, efetua o processamento para a primeira via
        
            '# Inicializa campos que podem n�o ser preenchidos durante a gera��o de
            '# um registro''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            
            gcpoTelCorretor = ""
            gcpoNomeConjuge = ""
            gcpoCPFConjuge = ""
            gDtEmissao = ""
            cpoObservacao = ""
            
            '# Armazena campos do registro atual da consulta''''''''''''''''''''''''''''''''
            
            cpoNomeSegurado = rs(1)
            cpoEndereco = rs(2) & " " & rs(3)       'endere�o + bairro
            cpoCidade = "" & rs(4)                    'munic�pio
            cpoCEPAux = Format(rs(5), "00000000")
            cpoCEP = Left(cpoCEPAux, 5) & "-" & Right(cpoCEPAux, 3)
            cpoUF = "" & rs(6)
            cpoDtInicioVigencia = Format(rs(7), "dd/mm/YYYY")
            cpoProduto = Format(rs(8), "000")
            cpoCPFAux = Format(rs(9), "00000000000")
            cpoCPF = Left(cpoCPFAux, 3) & "." & Mid(cpoCPFAux, 4, 3) & "." & Mid(cpoCPFAux, 7, 3) & "/" & Right(cpoCPFAux, 2)
            cpoDtNascimento = Format(rs(10), "dd/mm/yyyy")
            cpoSexo = rs(11)
            cpoProfissao = rs(12)
            
            
            If IsNull(rs!CGC) = False Then
               cpoCGC = IIf(IsNull(rs!CGC) = False, rs!CGC, "")
               cpoCGC = Left(cpoCGC, 2) & "." & Mid(cpoCGC, 3, 3) & "." & Mid(cpoCGC, 6, 3) & "/" & Mid(cpoCGC, 9, 4) & "-" & Right(cpoCGC, 2)
            Else
               cpoCGC = ""
            End If
           
            cpoDDD = Left(rs!ddd & Space(6), 6)
            cpoTelefone = Left(rs!telefone & Space(8), 8)
            
            '# impede a emiss�o do kit para cpf zerado'''''''''''''''''''''''''''''''''''''
                
           If (cpoCPF = "000.000.000/00" Or cpoCPF = "") And (cpoCGC = "00.000.000/0000-00" Or cpoCGC = "") And cpoProduto <> 155 Then
              GoTo Continua
           End If
        
           '# Obtendo dados adicionais necess�rios para gera��o do arquivo de Kit''''''''''
           
           Obtem_Dados_Produto
           
           BuscaDadosPropostaAdesao        'ok
        
           If Not FlagPropBasica Then      'False = n�o � proposta b�sica porque foi encontrada na ades�o
           
               If gOrigemProdutoIdSel = "CI" Then
                   BuscaDadosCorretor       'ok
               Else
                   BuscaDadosAgencia        'ok
               End If
               
               If Val(cpoProduto) = 155 Then
                   If Trim(gcpoEnderecoAgencia) = "" Or (gcpoCEPAgencia = "00000000" Or gcpoCEPAgencia = "99999999") Then
                       If Proposta_id_LogPnhRural <> gPropostaId Then
                           gravaLogAgenciaPenhorRural
                       End If
                       GoTo Continua
                   End If
               End If
               
            Select Case Val(cpoProduto)
                Case Is = cteProdutoIdOuroAgricola
                    Obtem_Proposta_Basica
                    Obtem_Dados_Ouro_Agricola
                    Obtem_Dados_Estipulante
                Case Is = cteProdutoIdPenhorRural
                    Obtem_Dados_Endereco_Risco
                Case Else
                    BuscaDadosEscolhaPlano          'ok
                    BuscaDadosCoberturas            'ok
                    BuscaDadosConjuge               'ok
                    Formata_Coberturas
                    If Val(cpoProduto) = cteProdutoIdOuroVida2000 Then
                        BuscaDtEmissao               'ok
                        BuscaDadosDeclTitular        'ok
                        BuscaDadosDeclComplementar
                    End If
            End Select
                
            '# identifica��o da exist�ncia de uma propostabb anterior e se o certificado
            '# se refere a uma migra��o ou renova��o, que � verificado pelo campo tp_registro
            '# obtido em proposta_adesao_tb.info_complementar
                
            If Trim(gPropostaBBAnterior) <> "" Then
                Select Case gTpRegistro
                    Case "11", "12"
                        cpoObservacao = "Emiss�o de Certificado por Migra��o de Produto"
                        Case cteTpRegistroRenovacao
                            cpoObservacao = "Renova��o da proposta N� " & gPropostaBBAnterior
                    End Select
                End If
                
                '# Cria��o do arquivo de emiss�o do KIT'''''''''''''''''''''''''''''''''''''
                
                If aNroRegArquivo = 0 Then
                   If Val(cpoProduto) <> cteProdutoIdOuroVidaProdutorRural Then
                      AbreArquivo
                      'Jorfilho 04-ago-2003: Nome do arquivo a ser logado
                      frmGeraArqEmissaoDocumentos.txtArqCertificado.Text = gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
                      arquivo_remessa = gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
                   End If
                End If
            
                'Comentado para impress�o dos certificados j� existentes na base''''''''''
                If Val(cpoProduto) <> 155 Then
                    ObtemNumeroCertificado
                End If
                
                '# Acerto para Valores de IS zerados''''''''''''''''''''''''''''''''''''''''
                
                GoSub MascaraValoresZerados
            
                '# Formata��o do campo N�mero da Ap�lice''''''''''''''''''''''''''''''''''''
                
                cpoNumeroApolice = gSucursal & gRamo & gApolice

                aContaPropostas = aContaPropostas + 1

                Select Case Val(cpoProduto)
                
                Case cteProdutoIdGarantia
                    
                    '# Fim de vig�ncia considerado default como 1 ano a partir do in�cio de vig�ncia
                    '# Define-se o fim de vig�ncia somente para o Ouro Vida Garantia
                    '# OBS : Futuramente esta informa��o dever� vir da seguro_tb, que
                    '# ser� comum a todos os produtos
                    '#
                    cpoDtFimVigencia = Format(DateAdd("yyyy", 1, cpoDtInicioVigencia), "dd/mm/yyyy")
            
                    '# Produto Ourovida Garantia gerar� o KIT no layout antigo (REL010)
                    '# por n�o ter cart�o 24 horas
                    
                    aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                    
                    aReg = "2" & "S" & Format(aNroRegArquivo, "00000")
                    aReg = aReg & gcpoCodAgencia
                    aReg = aReg & Left(gcpoNomeAgenciaCorretor, 25)
                    aReg = aReg & cpoNumeroApolice
                    aReg = aReg & gcpoNumeroCertificado & "-" & gcpoDVCertificado
                    aReg = aReg & gcpoNumeroProposta
                    aReg = aReg & cpoCPF
                    aReg = aReg & cpoNomeSegurado
                    aReg = aReg & cpoEndereco
                    aReg = aReg & cpoCidade
                    aReg = aReg & cpoUF
                    aReg = aReg & cpoCEP
                    
                    RegGarantia = aReg
                    
                    Print #1, RegGarantia
                    
                    aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 2
                    
                    aReg = "2" & "C" & Format(aNroRegArquivo, "00000")
                    aReg = aReg & cpoISMorteNaturalTit
                    aReg = aReg & cpoISIEATit
                    aReg = aReg & cpoDtInicioVigencia
                    aReg = aReg & cpoDtFimVigencia
                    aReg = aReg & cpoObservacao
                    aReg = aReg & cteTipoDocumento
                    aReg = aReg & cpoProduto
                    
                    RegGarantia = aReg
                    
                    Print #1, RegGarantia
                    
                Case cteProdutoIdOuroAgricola
                
                    aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                    Conta_Linha_certificado = Conta_Linha_certificado + 1
                    
                    linha = "2"
                    linha = linha & Format(Conta_Linha_certificado, "00000")
                    linha = linha & gcpoNumeroCertificado ''& "-" & gcpoDVCertificado
                    linha = linha & gApolice
                    linha = linha & gRamo
                    linha = linha & cpoNomeSegurado
                    
                    If Trim(cpoCGC) = "" Then
                       linha = linha & Space(4) & cpoCPF
                    Else
                       linha = linha & cpoCGC
                    End If
                    
                    linha = linha & cpoEndereco
                    linha = linha & cpoCidade
                    linha = linha & cpoUF
                    linha = linha & cpoCEP
                    linha = linha & cpoDDD
                    linha = linha & cpoTelefone
                    linha = linha & NomeEstipulante
                    linha = linha & Left(gcpoNomeAgenciaCorretor, 25)
                    linha = linha & gcpoCodAgencia
                    linha = linha & gcpoDVCodAgencia
                    linha = linha & NomePropriedade
                    linha = linha & EnderecoRisco
                    linha = linha & MunicipioIBGE
                    linha = linha & MunicipioRisco
                    
                    linha = linha & NumContrato
                    linha = linha & DtFimFinanc
                    
                    linha = linha & IndLavouraPlant
                    linha = linha & DescSistemaCultivo
                    linha = linha & NumInspPrevia
                    linha = linha & DtInspPrevia
                    linha = linha & DescTpCultura
                    linha = linha & TpCulturaId
                    linha = linha & ProducaoEsperada
                    linha = linha & DtIniVigSeguro
                    linha = linha & DtFimVigSeguro
                    linha = linha & AreaSegHectare
                    linha = linha & Imp_Seg_Arq
                    linha = linha & Imp_Seg_Hectare_Arq
                    linha = linha & Val_Premio_Arq
                    linha = linha & Left(gcpoEnderecoAgencia & Space(85), 85)
                    linha = linha & Left(gcpoMunicipioAgencia & Space(60), 60)
                    linha = linha & Left(gcpoUFAgencia & Space(2), 2)
                    linha = linha & Left(gcpoCEPAgencia & Space(8), 8)
                    linha = linha & Space(48)
                    
                    Print #1, linha
                
                Case cteProdutoIdPenhorRural
                   
                    'Obtem_Dados_Penhor_Rural
                    
                    Sql = "       SELECT"
                    Sql = Sql & "       a.tp_referencia_id, "
                    Sql = Sql & "       a.num_contrato, "
                    Sql = Sql & "       a.contrato_dv, "
                    Sql = Sql & "       a.prefixo_contrato, "
                    Sql = Sql & "       a.ano_contrato, "
                    Sql = Sql & "       d.val_is, "
                    Sql = Sql & "       d.val_premio, "
                    Sql = Sql & "       a.taxa_referencia, "
                    Sql = Sql & "       a.dt_inicio_vigencia_seg, "
                    Sql = Sql & "       c.nome, "
                    Sql = Sql & "       a.cod_objeto_segurado, "
                    Sql = Sql & "       a.num_rsa, "
                    Sql = Sql & "       d.emi_re_proposta_id "
                    Sql = Sql & "   From "
                    Sql = Sql & "       seguro_penhor_rural_tb a, "
                    Sql = Sql & "       proposta_adesao_tb b, "
                    Sql = Sql & "       tp_referencia_tb c, "
                    Sql = Sql & "       emi_re_penhor_rural_tb d, "
                    Sql = Sql & "       emi_re_proposta_tb e "
                    Sql = Sql & "   Where "
                    Sql = Sql & "       a.tp_referencia_id = c.tp_referencia_id  and"
                    Sql = Sql & "       a.tp_referencia_id = d.tp_referencia_id  and"
                    Sql = Sql & "       a.cod_objeto_segurado = d.cod_objeto_segurado and"
                    Sql = Sql & "       a.num_contrato = d.num_contrato and"
                    Sql = Sql & "       a.prefixo_contrato = d.prefixo_contrato and"
                    Sql = Sql & "       a.contrato_dv = d.contrato_dv and"
'                    SQL = SQL & "       a.dt_contrato_seg = d.dt_contrato_seg and"
                    Sql = Sql & "       a.num_rsa = d.num_rsa  and"
                    Sql = Sql & "       d.emi_re_proposta_id = e.emi_re_proposta_id and"
                    Sql = Sql & "       e.dt_inicio_vigencia = a.dt_inicio_vigencia_seg  and"
                    Sql = Sql & "       a.num_ordem = d.num_ordem and"
                    Sql = Sql & "       a.proposta_id = b.proposta_id and"
                    Sql = Sql & "       b.cont_agencia_id = convert(numeric(4), e.agencia_contratante) and"
                    Sql = Sql & "       a.proposta_id = " & gPropostaId
                    
                    Set Rst = rdocn.OpenResultset(Sql)
                    
                    While Not Rst.EOF
                    
                        CodigoReferencia = Format(Rst!tp_referencia_id, "000")
                        NumeroContrato = Format(Rst!num_contrato, "000000000")
                        PrefixoContrato = Format(Rst!prefixo_contrato, "00")
                        AnoContrato = Rst!ano_contrato
                        If AnoContrato >= 80 And AnoContrato <= 99 Then
                            AnoContrato = "19" & AnoContrato
                        ElseIf AnoContrato >= 20 And AnoContrato < 30 Then
                            AnoContrato = "200" & Right(AnoContrato, 1)
                        Else
                            AnoContrato = "00" & AnoContrato
                        End If
                        ValorSegurado = Format(Val(Rst!val_is), "#,###,##0.00")
                        ValorPremio = Format(Val(Rst!Val_Premio), "#,###,##0.00")
                        TaxaRisco = Format(Val(Rst!taxa_referencia), "#######0.00000")
                        DataEmissao = Rst!dt_inicio_vigencia_seg
                        DescBemSegurado = Left(Rst!nome & Space(60), 60)
                        CodObjetoSegurado = Rst!cod_objeto_segurado
                        num_rsa = Format(Rst!num_rsa, "000")
                        emi_re_proposta_id = Rst!emi_re_proposta_id
                        
                        If gPropostaId <> gPropostaIdAnt Then
                        
                            If InicioPenhorRural = False Then
                        
                                GoSub Detalhe3_Penhor_Rural
                            
                            End If
                            
                            aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                            Conta_Linha_certificado = Conta_Linha_certificado + 1
                                           
                            ObtemNumeroCertificadoRsa

                            ValorPremioTotal = 0
                            gcpoNumeroCertificadoAnt = gcpoNumeroCertificado
                            gRamoAnt = gRamo
                            gApoliceAnt = gApolice
                            
                            'Detalhe 1 - Mutu�rio
                            linha = "2"                                                     'tipo registro
                            linha = linha & "S"                                             'Flag continua��o
                            linha = linha & Format(Conta_Linha_certificado, "00000")        'N�mero Sequencial
                            linha = linha & gcpoNumeroCertificado                           'N�mero Certificado
                            linha = linha & gApolice                                        'N�mero Ap�lice
                            linha = linha & gRamo                                           'Ramo
                            linha = linha & cpoNomeSegurado                                 'Nome do Segurado
                            linha = linha & Left(gcpoNomeAgenciaCorretor & Space(25), 25)   'Nome da Ag�ncia
                            linha = linha & gcpoCodAgencia                                  'C�digo da Ag�ncia
                            linha = linha & gcpoDVCodAgencia                                'D�gito verificador da Ag�ncia
                            linha = linha & gcpoMunicipioAgencia                            'Munic�pio da Ag�ncia
                            linha = linha & gcpoUFAgencia                                   'UF da Ag�ncia
                            linha = linha & gcpoCEPAgencia                                  'CEP da Ag�ncia
                            linha = linha & NumeroContrato                                  'N�mero do Contrato Financeiro
                            linha = linha & PrefixoContrato                                 'Prefixo do Contrato
                            linha = linha & AnoContrato                           'Ano do Contrato
                            linha = linha & DtIniVigSeguro                                  'Data INICIO DE VIGENCIA
                            linha = linha & DtFimVigSeguro                                  'Data FIM DE VIGENCIA
                            linha = linha & Left(gcpoEnderecoAgencia & Space(85), 85)       'Endere�o da Ag�ncia Contratante
                            linha = linha & num_rsa                                         'N�mero do RSA
                            linha = linha & Space(52)                                       'Filler
                        
                            Print #1, linha
                        
                            InicioPenhorRural = False
                            
                        End If
                        
                        If gPropostaId <> gPropostaIdAnt Or _
                           CodObjetoSegurado <> CodObjetoSeguradoAnt Then
                        
                            aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                            Conta_Linha_certificado = Conta_Linha_certificado + 1
                            
                            'Detalhe 2 - Objetos
                            linha = "3"                                                     'tipo registro
                            linha = linha & "S"                                             'Flag continua��o
                            linha = linha & Format(Conta_Linha_certificado, "00000")        'N�mero Sequencial
                            linha = linha & gcpoNumeroCertificado                           'N�mero Certificado
                            linha = linha & gApolice                                        'N�mero Ap�lice
                            linha = linha & gRamo                                           'Ramo
                            linha = linha & Space(14 - Len(ValorSegurado)) & ValorSegurado  'Importancia Segurada
                            linha = linha & Space(14 - Len(ValorPremio)) & ValorPremio      'Valor Pr�mio
                            linha = linha & CodigoReferencia                                'C�d. Refer�ncia do Bem Segurado
                            linha = linha & DescBemSegurado                                 'Descri��o do Bem Segurado
                            linha = linha & Space(234)                                      'Filler
                            
                            ValorPremioTotal = ValorPremioTotal + ValorPremio
    
                            Print #1, linha
                            
                            gPropostaIdAnt = gPropostaId
                            CodObjetoSeguradoAnt = CodObjetoSegurado
                        End If
                        Rst.MoveNext
                    Wend
                    Rst.Close
                Case Else
                
                    If Val(cpoProduto) <> cteProdutoIdOuroVidaProdutorRural Then
                    
                        '# para outros produtos (no caso o Ouro Vida e Brasil Vida) n�o h�
                        '# data de fim de vig�ncia, conforme o processamento anterior do
                        '# programa Gera Arquivos Certificado
                        cpoDtFimVigencia = ""
                        
                        '# Para os outros produtos a emiss�o do KIT ser� feita
                        '# pelo layout unificado
                                        
                        If Trim(gcpoNomeConjuge) = "" Or Mid(gcpoNomeConjuge, 1, 5) = "*****" Then
                            gTipoSeguro = "2"
                        Else
                            gTipoSeguro = "1"
                        End If
                                       
                        aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                                         
                        aReg = "2" & "S" & Format(aNroRegArquivo, "00000")
                        aReg = aReg & gcpoCodAgencia
                        aReg = aReg & gcpoNomeAgenciaCorretor
                        aReg = aReg & gcpoTelCorretor
                        aReg = aReg & cpoNumeroApolice
                        aReg = aReg & gcpoNumeroCertificado & "-" & gcpoDVCertificado
                        aReg = aReg & gcpoNumeroProposta
                        aReg = aReg & cpoNomeSegurado
                        aReg = aReg & cpoDtNascimento
                        aReg = aReg & cpoSexo
                        aReg = aReg & cpoCPF
                        aReg = aReg & cpoEndereco
                        aReg = aReg & cpoCidade
                        aReg = aReg & cpoUF
                        aReg = aReg & cpoCEP
                        
                        If Val(cpoProduto) = cteProdutoIdOuroVida2000 Then
                           aReg = aReg & gcpoPlanoId
                           aReg = aReg & gTipoSeguro
                           aReg = aReg & gcpoValorPremio
                           aReg = aReg & gDtEmissao
                        End If
                        
                        aReg = aReg & gNomeProduto
                        '** Alterado por Paulo Carvalho - 02/10/2002
                        aReg = aReg & "22"
                        aReg = aReg & Format(gPropostaId, "000000000")
                        aReg = aReg & ConverteParaJulianDate(CDate(Data_Sistema))
                        aReg = aReg & gcpoCodAgencia
                        '**
                        aReg = Left(aReg & Space(cteTamanhoRegistroUnificado), cteTamanhoRegistroUnificado)
                        RegUnificado = aReg
                                            
                        Print #1, RegUnificado
                                            
                        aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 2
                        
                        aReg = "2" & "C" & Format(aNroRegArquivo, "00000")
                        aReg = aReg & cpoProfissao
                        aReg = aReg & gcpoNomeConjuge
                        aReg = aReg & gcpoCPFConjuge
                        aReg = aReg & cpoISMorteNaturalTit
                        aReg = aReg & cpoISIEATit
                        aReg = aReg & cpoISIPATit
                        aReg = aReg & cpoISIPDTit
                        aReg = aReg & cpoISMorteNaturalConj
                        aReg = aReg & cpoISIEAConj
                        aReg = aReg & cpoISIPAConj
                        aReg = aReg & cpoISIPDConj
                        aReg = aReg & cpoDtInicioVigencia
                        aReg = aReg & cpoDtFimVigencia
                        aReg = aReg & cpoObservacao
                        'aReg = aReg & cteTipoDocumento
                        'aReg = aReg & cpoProduto
                        aReg = Left(aReg & Space(cteTamanhoRegistroUnificado), cteTamanhoRegistroUnificado)
                        RegUnificado = aReg
                        
                        Print #1, RegUnificado
                        
                        If Val(cpoProduto) = cteProdutoIdOuroVida2000 Then
                        
                            aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                                         
                            aReg = "3" & "C" & Format(aNroRegArquivo, "00000")
                            aReg = aReg & gcpoResp1Tit & gcpoDescr1Tit
                            aReg = aReg & gcpoResp2Tit & gcpoDescr2Tit
                            aReg = aReg & gcpoResp3Tit & gcpoDescr3Tit
                            aReg = aReg & gcpoResp4Tit & gcpoDescr4Tit
                            aReg = aReg & gcpoResp5Tit & gcpoDescr5Tit
                            aReg = Left(aReg & Space(cteTamanhoRegistroUnificado), cteTamanhoRegistroUnificado)
                            RegUnificado = aReg
                                            
                            Print #1, RegUnificado
                            
                            aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
                                       
                            aReg = "4" & "C" & Format(aNroRegArquivo, "00000")
                            aReg = aReg & gcpoResp1Compl & gcpoDescr1Compl
                            aReg = aReg & gcpoResp2Compl & gcpoDescr2Compl
                            aReg = aReg & gcpoResp3Compl & gcpoDescr3Compl
                            aReg = aReg & gcpoResp4Compl & gcpoDescr4Compl
                            aReg = aReg & gcpoResp5Compl & gcpoDescr5Compl
                            aReg = Left(aReg & Space(cteTamanhoRegistroUnificado), cteTamanhoRegistroUnificado)
                            RegUnificado = aReg
                            
                            Print #1, RegUnificado
                                            
                        End If
                        
                        Call Atualiza_Evento_Impressao(rs!num_solicitacao, arquivo_remessa, cUserName)
                        
'                        SQL = ""
'                        SQL = SQL & " Exec atualiza_impressao_solicsegvia_spu "
'                        SQL = SQL & RS("Proposta_id") & ", '"
'                        SQL = SQL & cUserName & "', "
'                        SQL = SQL & "27, '"
'                        SQL = SQL & RS("destino") & "'"
'                        rdocn.Execute (SQL)

                    End If
                
                End Select

                'Jorfilho 04-ago-2003: Total de registros e propostas a serem logados
                frmGeraArqEmissaoDocumentos.lblRegCertificado = aNroRegArquivo
                frmGeraArqEmissaoDocumentos.lblPropostas = aContaPropostas

                'Comentado para impress�o dos certificados j� existentes na base''''''''''
                                    
            End If  'se a proposta selecionada foi encontrada na proposta ades�o, ou seja, n�o � proposta b�sica

Continua:
        rs.MoveNext

        Wend
       
        rs.Close
    
        '# Trailler
        If aNroRegArquivo > 0 Then
            If Val(cpoProduto) <> cteProdutoIdOuroVidaProdutorRural Then
            
                 If Val(cpoProduto) = cteProdutoIdPenhorRural Then
                     GoSub Detalhe3_Penhor_Rural
                     aReg = "9" & Format(aNroRegArquivo, "00000") & Space(306)
                 Else
                     aReg = "9" & Format(aNroRegArquivo, "00000")
                     If Val(cpoProduto) = cteProdutoIdOuroAgricola Then
                        aReg = aReg & Space(889)
                     ElseIf Val(cpoProduto) = cteProdutoIdGarantia Then
                        aReg = aReg & Space(324)
                     Else
                        aReg = aReg & Space(394)
                     End If
                  End If
                 
                 Print #1, aReg
                
                 Close #1
           
            End If
        
        End If
        
        If aNroRegArquivo > 0 And wNew Then
           Call InserirArquivoVersaoGerado("REL015", aNroRegArquivo, aNroRegArquivo + 2, CInt(NumRemessa))
        End If
    
    rdocn.CommitTrans
    
    Exit Sub

MascaraValoresZerados:

    If Trim(cpoISMorteNaturalTit) = "0,00" Then
       cpoISMorteNaturalTit = String(14, "*")
    End If
    
    If Trim(cpoISIEATit) = "0,00" Then
       cpoISIEATit = String(14, "*")
    End If
    
    If Trim(cpoISIPATit) = "0,00" Then
       cpoISIPATit = String(14, "*")
    End If
    
    If Trim(cpoISIPDTit) = "0,00" Then
       cpoISIPDTit = String(14, "*")
    End If
    
    If Trim(cpoISMorteNaturalConj) = "0,00" Then
       cpoISMorteNaturalConj = String(14, "*")
    End If
    
    If Trim(cpoISIEAConj) = "0,00" Then
       cpoISIEAConj = String(14, "*")
    End If
    
    If Trim(cpoISIPAConj) = "0,00" Then
       cpoISMorteNaturalConj = String(14, "*")
    End If
    
    If Trim(cpoISIPDConj) = "0,00" Then
       cpoISMorteNaturalConj = String(14, "*")
    End If

    Return
    
Detalhe3_Penhor_Rural:
    aNroRegArquivo = aNroRegArquivo + 1 'incrementa contador com o registro detalhe 1
    Conta_Linha_certificado = Conta_Linha_certificado + 1
    'Detalhe 3 - Total
    linha = "4"                                                                 'tipo registro
    linha = linha & "N"                                                         'Flag continua��o
    linha = linha & Format(Conta_Linha_certificado, "00000")                    'N�mero Sequencial
    linha = linha & gcpoNumeroCertificadoAnt                                    'N�mero Certificado
    linha = linha & gApoliceAnt                                                 'N�mero Ap�lice
    linha = linha & gRamoAnt                                                    'Ramo
    linha = linha & Space(14 - Len(Format(ValorPremioTotal, "#,###,##0.00"))) & Format(ValorPremioTotal, "#,###,##0.00")    'Valor Pr�mio Total
    linha = linha & "TOTAL"                                                  'Filler
    linha = linha & Space(306)                                                  'Filler

    Print #1, linha
    
    Return
    
ErroProcessa:
    rdocn.RollbackTrans
    TrataErroGeral "Processa REL015", Me.name
    TerminaSEGBR

End Sub

Public Sub gravaLogAgenciaPenhorRural()

    On Error GoTo Erro

   filenum = FreeFile
   'Rotina para buscar nome do arquivo de log atual
   nomearq = "LogCertPnhRural" & Format$(Date, "mmyyyy") & ".txt"
   Open logpath & nomearq For Append As filenum

   linha = "Dados de endere�o e/ou CEP da Agencia, incompleto(s) para a proposta " & gPropostaId & "."
   
   Print #filenum, linha
   Close #filenum
   
   Proposta_id_LogPnhRural = gPropostaId
   
   Exit Sub
   
Erro:
    TrataErroGeral "gravaLogAgenciaPenhorRural REL015", Me.name
    TerminaSEGBR

End Sub

Private Sub BuscaDadosPropostaAdesao()

On Error GoTo ErroBuscaDadosPropostaAdesao
    Dim Rst As rdoResultset
   
    Sql = "SELECT"
    Sql = Sql & " seguradora_cod_susep,"
    Sql = Sql & " sucursal_seguradora_id,"
    Sql = Sql & " ramo_id,"
    Sql = Sql & " apolice_id,"
    Sql = Sql & " cont_banco_id,"
    Sql = Sql & " cont_agencia_id, "
    Sql = Sql & " proposta_bb,"
    Sql = Sql & " proposta_bb_anterior,"
    Sql = Sql & " info_complementar,"
    Sql = Sql & " dt_inicio_vigencia, "
    Sql = Sql & " dt_fim_vigencia "
    Sql = Sql & " FROM"
    Sql = Sql & " proposta_adesao_tb"
    Sql = Sql & " WHERE"
    Sql = Sql & " proposta_id = " & gPropostaId
    
    Set Rst = rdocn1.OpenResultset(Sql)

    If Not Rst.EOF Then
       
       gSeguradora = Rst(0)
       gSucursal = Format(Rst(1), "00")
       gRamo = Format(Rst(2), "00")
       gApolice = Format(Rst(3), "00000000")
       gBanco = Format(Rst(4), "0000")
       gPropostaBBAnterior = Format(Rst(7), "000000000") 'migra��o/renova��o
       gTpRegistro = Format(Trim(Rst(8)), "00")          'info_complementar (migra��o/renova��o)
       
       gcpoCodAgencia = Format(Rst(5), "0000")           'c�digo ag�ncia
       gcpoNumeroProposta = Format(Rst(6), "000000000")  'proposta_bb
       DtIniVigSeguro = IIf(IsNull(Rst!dt_inicio_vigencia) = False, Format(Rst!dt_inicio_vigencia, "dd/mm/yyyy"), "")
       DtFimVigSeguro = IIf(IsNull(Rst!dt_fim_vigencia) = False, Format(Rst!dt_fim_vigencia, "dd/mm/yyyy"), "")
       FlagPropBasica = False
    
    Else
       
       FlagPropBasica = True
    
    End If
   
    Rst.Close
    
    Exit Sub

ErroBuscaDadosPropostaAdesao:
   TrataErroGeral "Busca Dados Proposta Adesao REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub BuscaDadosAgencia()

    Dim Rst As rdoResultset
   
    On Error GoTo ErroBuscaDadosAgencia
   
    If gcpoCodAgencia = gAgenciaAnt Then
       Exit Sub
    Else
       gAgenciaAnt = gcpoCodAgencia
    End If
   
    Sql = "SELECT a.nome, b.nome, a.estado, a.cep, a.endereco "
    Sql = Sql & " FROM "
    Sql = Sql & "   agencia_tb a, municipio_tb b"
    Sql = Sql & " WHERE "
    Sql = Sql & "   a.agencia_id  = " & gcpoCodAgencia & " and "
    Sql = Sql & "   a.banco_id = " & gBanco & " and "
    Sql = Sql & "   a.municipio_id = b.municipio_id and "
    Sql = Sql & "   a.estado = b.estado "
    
    Set Rst = rdocn1.OpenResultset(Sql)
   
    If Not Rst.EOF Then
        gcpoNomeAgenciaCorretor = UCase(Rst(0))
        gcpoMunicipioAgencia = UCase(Rst(1))
        gcpoUFAgencia = UCase(Rst(2))
        gcpoCEPAgencia = Format(Rst(3), "00000000")
        If Trim(Rst(4)) <> "" Then
            gcpoEnderecoAgencia = UCase(Trim(Rst(4)))
        Else
            gcpoEnderecoAgencia = ""
        End If
    Else
        gcpoNomeAgenciaCorretor = ""
        gcpoMunicipioAgencia = ""
        gcpoUFAgencia = ""
        gcpoCEPAgencia = ""
        gcpoEnderecoAgencia = ""
    End If
    
    Rst.Close
    
    gcpoDVCodAgencia = calcula_dv_cc_ou_agencia(agencia)
    
Exit Sub
   
ErroBuscaDadosAgencia:
    TrataErroGeral "Busca Dados Ag�ncia REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub BuscaDadosCorretor()
   
   On Error GoTo ErroBuscaDadosCorretor
        
   Sql = "SELECT"
   Sql = Sql & " b.nome, IsNull(b.ddd,'0'), IsNull(b.telefone,'0')"
   Sql = Sql & " FROM"
   Sql = Sql & " corretagem_tb a, corretor_tb b"
   Sql = Sql & " WHERE"
   Sql = Sql & " a.corretor_id = b.corretor_id"
   Sql = Sql & " and a.proposta_id  = " & gPropostaId
   
   Set rs = rdocn1.OpenResultset(Sql)
   
   If Not rs.EOF Then
      gcpoNomeAgenciaCorretor = UCase(rs(0))
      gcpoTelCorretor = "(" & Format(rs(1), "@@@") & ")" & Format(rs(2), "@@@@@@@@") 'o telefone apesar de num�rico est� definido como string no layout
   Else
      gcpoNomeAgenciaCorretor = ""
      gcpoTelCorretor = ""
   End If
   
   rs.Close
   
   Exit Sub
   
ErroBuscaDadosCorretor:
    TrataErroGeral "Busca Dados Corretor REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub BuscaDadosEscolhaPlano()
    
On Error GoTo ErroBuscaDadosEscolhaPlano
    
    Dim Rst As rdoResultset
    
    Sql = "SELECT"
    Sql = Sql & " isnull(a.imp_segurada,0) imp_segurada, "
    Sql = Sql & " b.tp_plano_id, "
    Sql = Sql & " isnull(a.val_premio,0) val_premio, "
    Sql = Sql & " a.plano_id"
    Sql = Sql & " FROM"
    Sql = Sql & " escolha_plano_tb a, plano_tb b"
    Sql = Sql & " WHERE"
    Sql = Sql & " a.proposta_id = " & gPropostaId
    Sql = Sql & " and a.plano_id = b.plano_id"
    Sql = Sql & " and a.produto_id = b.produto_id"
    Sql = Sql & " and a.dt_fim_vigencia is null"
    
    Set Rst = rdocn1.OpenResultset(Sql)
    
    If Not Rst.EOF Then
       
       
       gImportanciaSegurada = Val(Rst!imp_segurada)
       gcpoValorPremio = Val(Rst!Val_Premio)
              
       gTpPlano = Rst!tp_plano_id
       gcpoPlanoId = Format(Rst!plano_id, "0000")
       gcpoValorPremio = Format(gcpoValorPremio, "###,###,##0.00")
       gcpoValorPremio = Right(String(14, " ") & gcpoValorPremio, 14)
       
    End If
    
    Rst.Close
    
Exit Sub
   
ErroBuscaDadosEscolhaPlano:
    TrataErroGeral "Dados Escolha Plano REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub BuscaDadosCoberturas()
   
    On Error GoTo ErroBuscaDadosCoberturas
    
    Dim lValLimMaxCob
    Dim Rst As rdoResultset
    
    If gTpPlano = gTpPlanoAnt Then
       Exit Sub
    Else
       gTpPlanoAnt = gTpPlano
    End If
   
    Sql = "SELECT distinct"
    Sql = Sql & " c.lim_max_cobertura, c.tp_componente_id, c.tp_cobertura_id"
    Sql = Sql & " FROM"
    Sql = Sql & " tp_plano_tp_comp_tb a,"
    Sql = Sql & " tp_cob_comp_plano_tb b,"
    Sql = Sql & " tp_cob_comp_tb c"
    Sql = Sql & " WHERE"
    Sql = Sql & " a.tp_plano_id = " & gTpPlano
    Sql = Sql & " and a.tp_plano_id = b.tp_plano_id"
    Sql = Sql & " and b.tp_cob_comp_id = c.tp_cob_comp_id"
    Sql = Sql & " ORDER BY c.tp_componente_id, c.tp_cobertura_id"
   
    Set Rst = rdocn1.OpenResultset(Sql)
    
    While Not Rst.EOF
       
        If gConfiguracaoBrasil Then
           lValLimMaxCob = Val(Rst(0))
        Else
           lValLimMaxCob = Rst(0)
        End If
       
        Select Case Rst(2)    'tp_cobertura_id
        
            Case 5  '48 em desenvolvimento  morte
                If Rst(1) = 1 Then
                   gPercCobMorteNaturalTit = lValLimMaxCob / 100
                Else
                   gPercCobMorteNaturalConj = lValLimMaxCob / 100
                End If
            
            Case 2 '39(desenvolvimento) IEA - morte acidental
                If Rst(1) = 1 Then
                   gPercCobIEATit = lValLimMaxCob / 100
                Else
                   gPercCobIEAConj = lValLimMaxCob / 100
                End If
            
            Case 3 ' 46 (desenvolvimento) IPA - invalidez permanente total
                If Rst(1) = 1 Then
                   gPercCobIPATit = lValLimMaxCob / 100
                Else
                   gPercCobIPAConj = lValLimMaxCob / 100
                End If
            
            Case 4, 242 ' 47(desenvolvimento) IPD invalidez permanente por doen�a
                If Rst(1) = 1 Then
                   gPercCobIPDTit = lValLimMaxCob / 100
                Else
                   gPercCobIPDConj = lValLimMaxCob / 100
                End If
        End Select
    
        Rst.MoveNext
    
    Wend
    
    Rst.Close
    
    Exit Sub
   
ErroBuscaDadosCoberturas:
    TrataErroGeral "Busca Dados Coberturas REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub BuscaDadosConjuge()
   
    On Error GoTo ErroBuscaDadosConjuge
    
    Dim gcpoCPFConjugeAux As String
    Dim Rst As rdoResultset
   
    Sql = "SELECT c.nome, b.cpf"
    Sql = Sql & " FROM"
    Sql = Sql & " proposta_complementar_tb a, pessoa_fisica_tb b, cliente_tb c"
    Sql = Sql & " WHERE"
    Sql = Sql & " a.proposta_id = " & gPropostaId
    Sql = Sql & " and a.prop_cliente_id = b.pf_cliente_id"
    Sql = Sql & " and cliente_id = prop_cliente_id"
    Sql = Sql & " and a.situacao <> 'c'"
      
    Set Rst = rdocn1.OpenResultset(Sql)
    
    If Not Rst.EOF Then
       gcpoNomeConjuge = UCase(Rst(0))
       gcpoCPFConjugeAux = Rst(1)
       gcpoCPFConjuge = Left(gcpoCPFConjugeAux, 3) & "." & Mid(gcpoCPFConjugeAux, 4, 3) & "." & Mid(gcpoCPFConjugeAux, 7, 3) & "/" & Right(gcpoCPFConjugeAux, 2)
       gComponenteConjuge = "ok"
    Else
       gcpoNomeConjuge = String(50, "*")
       gcpoCPFConjuge = String(14, "*")
       gComponenteConjuge = ""
    End If
    
    Rst.Close
    
Exit Sub
   
ErroBuscaDadosConjuge:
    TrataErroGeral "Dados Conjuge REL015", Me.name
    TerminaSEGBR
End Sub
Private Sub BuscaDadosDeclTitular()
   
    On Error GoTo ErroBuscaDadosTitular
    
    Dim Rst As rdoResultset
   
    gcpoResp1Tit = " "
    gcpoResp2Tit = " "
    gcpoResp3Tit = " "
    gcpoResp4Tit = " "
    gcpoResp5Tit = " "
    gcpoDescr1Tit = Space(60)
    gcpoDescr2Tit = Space(60)
    gcpoDescr3Tit = Space(60)
    gcpoDescr4Tit = Space(60)
    gcpoDescr5Tit = Space(60)
   
    Sql = "SELECT resp_titular, descricao, item"
    Sql = Sql & " FROM"
    Sql = Sql & " decl_titular_tb a "
    Sql = Sql & " WHERE"
    Sql = Sql & " a.proposta_id = " & gPropostaId
     
    Set Rst = rdocn1.OpenResultset(Sql)
   
    While Not Rst.EOF
        Select Case Rst!Item
            Case 1
                gcpoResp1Tit = Rst!resp_titular
                gcpoDescr1Tit = Rst!Descricao
            Case 2
                gcpoResp2Tit = Rst!resp_titular
                gcpoDescr2Tit = Rst!Descricao
            Case 3
                gcpoResp3Tit = Rst!resp_titular
                gcpoDescr3Tit = Rst!Descricao
            Case 4
                gcpoResp4Tit = Rst!resp_titular
                gcpoDescr4Tit = Rst!Descricao
            Case 5
                gcpoResp5Tit = Rst!resp_titular
                gcpoDescr5Tit = Rst!Descricao
        End Select
        
        Rst.MoveNext
        
    Wend
    
    Rst.Close
    
Exit Sub
   
ErroBuscaDadosTitular:
    TrataErroGeral "Dados Decl Titular REL015", Me.name
    TerminaSEGBR
End Sub
Private Sub BuscaDadosDeclComplementar()
    
    On Error GoTo ErroBuscaDadosComplementar
    
    Dim Rst As rdoResultset
    
    gcpoResp1Compl = " "
    gcpoResp2Compl = " "
    gcpoResp3Compl = " "
    gcpoResp4Compl = " "
    gcpoResp5Compl = " "
    gcpoDescr1Compl = Space(60)
    gcpoDescr2Compl = Space(60)
    gcpoDescr3Compl = Space(60)
    gcpoDescr4Compl = Space(60)
    gcpoDescr5Compl = Space(60)
    
    Sql = "SELECT resp_complementar, descricao, item"
    Sql = Sql & " FROM"
    Sql = Sql & " decl_complementar_tb a "
    Sql = Sql & " WHERE"
    Sql = Sql & " a.proposta_id = " & gPropostaId
    
    Set Rst = rdocn1.OpenResultset(Sql)
    
    While Not Rst.EOF
    
        Select Case Rst!Item
        Case 1
              gcpoResp1Compl = Rst!resp_complementar
              gcpoDescr1Compl = Rst!Descricao
        Case 2
              gcpoResp2Compl = Rst!resp_complementar
              gcpoDescr2Compl = Rst!Descricao
        Case 3
              gcpoResp3Compl = Rst!resp_complementar
              gcpoDescr3Compl = Rst!Descricao
        Case 4
              gcpoResp4Compl = Rst!resp_complementar
              gcpoDescr4Compl = Rst!Descricao
        Case 5
              gcpoResp5Compl = Rst!resp_complementar
              gcpoDescr5Compl = Rst!Descricao
        End Select
        
        Rst.MoveNext
        
    Wend
    
    Rst.Close
   
Exit Sub
   
ErroBuscaDadosComplementar:
    TrataErroGeral "Dados Decl Complementar REL015", Me.name
    TerminaSEGBR
End Sub

Private Sub BuscaDtEmissao()
   
    On Error GoTo ErroBuscaDadosConjuge
   
   Dim gcpoCPFConjugeAux As String
   Dim Rst As rdoResultset
   
    Sql = "SELECT dt_avaliacao"
    Sql = Sql & " FROM"
    Sql = Sql & " avaliacao_proposta_tb "
    Sql = Sql & " WHERE"
    Sql = Sql & " proposta_id = " & gPropostaId
    Sql = Sql & " and tp_avaliacao_id in (0,6,8)"
     
    Set Rst = rdocn1.OpenResultset(Sql)
    
    If Not Rst.EOF Then
       gDtEmissao = Format(Rst!dt_avaliacao, "dd/mm/yyyy")
    Else
       gDtEmissao = Data_Sistema
    End If
    
    Rst.Close
   
Exit Sub
   
ErroBuscaDadosConjuge:
    TrataErroGeral "Obtem Dt Emissao REL015", Me.name
    TerminaSEGBR
End Sub


Private Sub AbreArquivo()
  
Dim lPathArquivoKit         As String
Dim lDataHora               As String
Dim lPathCompletoArquivoKit As String
Dim lNomeArqKitProdSel      As String
Dim HeaderAux               As String
  
On Error GoTo ErroAbreArquivo
    
    '# leitura do path no seguros.ini para gravar o arquivo
    
    lPathArquivoKit = LerArquivoIni("relatorios", "remessa_gerado_path")
    'lPathArquivoKit = "c:\jvieira\"
    
    '# auxiliares para composi��o do header
    If Val(cpoProduto) = 155 Then
        lDataHora = Trim(Format(Data_Sistema, "dd/mm/yyyy"))
    Else
        lDataHora = Trim(Format(CDate(Data_Sistema), "dd/mm/yyyy hh:mm AMPM")) & "  "
    End If
    
    Call ObterNumRemessa(gNomeArquivoDocProdutoIdSel, NumRemessa)
    
    HeaderAux = "1" & Left(gNomeArquivoDocProdutoIdSel & Space(25), 25) & _
                      lDataHora & "A4   " & gDestinoProdutoIdSel & NumRemessa & gDiretoriaDestino
    
    '# define o registro header
    '# de acordo com o produto escolhido (garantia ser�
    '# o layout original e outros o unificado)
        
    RegHeader = Left(HeaderAux & Space(cteTamanhoRegistroUnificado), cteTamanhoRegistroUnificado)
    
    NumRemessa = Format(CDbl(NumRemessa) - 1, "0000")
    lPathCompletoArquivoKit = lPathArquivoKit & gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
    
    
    If Dir(lPathCompletoArquivoKit) = "" Then
        wNew = True
        NumRemessa = Format(CDbl(NumRemessa) + 1, "0000")
        lPathCompletoArquivoKit = lPathArquivoKit & gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
        QualRemessa = gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
        Open lPathCompletoArquivoKit For Append As #1
        Print #1, RegHeader
    Else
        wNew = False
        QualRemessa = gNomeArquivoDocProdutoIdSel & "." & Format(NumRemessa, "0000")
        Open lPathCompletoArquivoKit For Input As #1
        Do While Not EOF(1)
            Line Input #1, newline
            Select Case Left(newline, 1)
                Case 2, 3, 4
                    If sAcrescer = "" Then
                        sAcrescer = newline
                    Else
                        sAcrescer = sAcrescer & vbCrLf & newline
                    End If
                    aNroRegArquivo = aNroRegArquivo + 1
            End Select
        Loop
        Close #1
        Open lPathCompletoArquivoKit For Output As #1
        Print #1, RegHeader
        Print #1, sAcrescer
    End If
   
Exit Sub
    
ErroAbreArquivo:
   TrataErroGeral "Abre Arquivo REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub ObtemNumeroCertificado()

On Error GoTo Erro
   
   Dim Rst As rdoResultset
    
    Sql = ""
    Sql = Sql & " SELECT num_certificado"
    Sql = Sql & " FROM chave_certificado_tb"
    Sql = Sql & " WHERE apolice_id = " & gApolice
    Sql = Sql & " AND sucursal_seguradora_id = " & gSucursal
    Sql = Sql & " AND seguradora_cod_susep = " & gSeguradora
    Sql = Sql & " AND ramo_id = " & gRamo
    
    Set Rst = rdocn1.OpenResultset(Sql)
    
    '# obs: essa SP sempre retorna um valor, de modo que n�o � necess�rio
    '# efetuar o teste de retorno de linhas no resultset
    
    gcpoNumeroCertificado = Format(Rst(0), "00000000")
    gcpoDVCertificado = Modulo11(gcpoNumeroCertificado)
    
    Rst.Close

Exit Sub

Erro:
   TrataErroGeral "Numero do Certificado REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub ObtemNumeroCertificadoRsa()

On Error GoTo Erro
   
    Sql = "exec " & Ambiente & ".chave_certificado_rsa_spu "
    Sql = Sql & gApolice & ", "
    Sql = Sql & gSucursal & ", "
    Sql = Sql & gSeguradora & ", "
    Sql = Sql & gRamo & ", "
    Sql = Sql & num_rsa & ", '"
    Sql = Sql & cUserName & "'"
    
    Set rs = rdocn1.OpenResultset(Sql)
    
    '# obs: essa SP sempre retorna um valor, de modo que n�o � necess�rio
    '# efetuar o teste de retorno de linhas no resultset
    
    gcpoNumeroCertificado = Format(rs(0), "00000000")
    gcpoDVCertificado = Modulo11(gcpoNumeroCertificado)
       
    rs.Close
    
    Exit Sub

Erro:
   TrataErroGeral "Numero do Certificado Rsa REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub GravaCertificado(ByVal pDtInicioVigencia, Optional ByVal pDtFimVegencia)

On Error GoTo ErroGravaCertificado
   
    Sql = "exec " & Ambiente & ".certificado_spi "
    Sql = Sql & gcpoNumeroCertificado & ", "
    Sql = Sql & gApolice & ", "
    Sql = Sql & gSucursal & ", "
    Sql = Sql & gSeguradora & ", "
    Sql = Sql & gRamo & ", "
    Sql = Sql & gPropostaId & ", "
    Sql = Sql & "NULL" & ", "
    Sql = Sql & "NULL" & ", '"
    Sql = Sql & Format(pDtInicioVigencia, "yyyymmdd") & "', "
    If Val(cpoProduto) = 155 Then
        Sql = Sql & "'" & Format(pDtFimVegencia, "yyyymmdd") & "', '"
        Sql = Sql & cUserName & "', "
        Sql = Sql & num_rsa
    Else
        Sql = Sql & "NULL" & ", '"
        Sql = Sql & cUserName & "'"
    End If
    
    Set rs = rdocn1.OpenResultset(Sql)
      
    rs.Close
    
    Exit Sub

ErroGravaCertificado:
   TrataErroGeral "Grava Certificado REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub Atualiza_Evento_Impressao(ByVal num_solicitacao As Long, _
                                      ByVal arquivo_remessa As String, _
                                      ByVal usuario As String)
                                      
Dim Sql         As String
Dim rsAtualizacao  As rdoResultset

On Error GoTo Erro

    Sql = ""
    Sql = Sql & "exec evento_seguros_db..evento_impressao_geracao_spu "
    Sql = Sql & num_solicitacao & ", '" & arquivo_remessa & "', '" & usuario & "' "
    
    Set rsAtualizacao = rdocn.OpenResultset(Sql)
    rsAtualizacao.Close

    Exit Sub
    
Erro:
    TrataErroGeral "Atualiza evento_impressao", Me.name
    TerminaSEGBR
End Sub

Private Sub AtualizaSituacaoProposta(ByVal pSituacao As String)

On Error GoTo ErroAtualizaSituacaoProposta

    Sql = "exec " & Ambiente & ".situacao_proposta_spu "
    Sql = Sql & gPropostaId & " , '"
    Sql = Sql & pSituacao & "', '"
    Sql = Sql & cUserName & "'"
    
    Set rs = rdocn1.OpenResultset(Sql)
    
    rs.Close
    
    Exit Sub

ErroAtualizaSituacaoProposta:
   TrataErroGeral "Atualiza Situacao Proposta REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub AtualizaSituacaoPropostaBB(ByVal pSituacao As String, ByVal pPropostaBB)

On Error GoTo ErroAtualizaSituacaoPropostaBB

    If gTpRegistro <> "11" And gTpRegistro <> "12" And gTpRegistro <> "02" And gTpRegistro <> cteTpRegistroRenovacao Then
       gTpRegistro = "01"
    End If
    
    If Trim(gNomeArquivoDocProdutoIdSel) <> "REL011" And Trim(gNomeArquivoDocProdutoIdSel) <> "REL108" Then
        
        Sql = "exec " & Ambiente & ".emi_proposta_spu "
        Sql = Sql & pPropostaBB & ", "
        Sql = Sql & gcpoNumeroCertificado & " , '"
        Sql = Sql & gcpoDVCertificado & "', '"
        Sql = Sql & pSituacao & "', '"
        Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "', '"
        Sql = Sql & cUserName & "', '"
        Sql = Sql & gTpRegistro & "', '"
        Sql = Sql & gArquivoProdutoIdSel & "'"
        
    Else
        
        If Trim(gNomeArquivoDocProdutoIdSel) = "REL108" Then
            Sql = "exec " & Ambiente & ".situacao_re_prop_Pnh_Rural_spu "
            Sql = Sql & emi_re_proposta_id & ", '"
            Sql = Sql & pSituacao & "', '"
            Sql = Sql & gcpoNumeroCertificado & "', '"
            Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "', '"
            Sql = Sql & cUserName & "'"
        Else
            Sql = "exec " & Ambiente & ".situacao_re_propostaBB_spu "
            Sql = Sql & pPropostaBB & ",'"
            Sql = Sql & pSituacao & "','"
            Sql = Sql & Format(Data_Sistema, "yyyymmdd") & "','"
            Sql = Sql & cUserName & "','"
            Sql = Sql & gTpRegistro & "','"
            Sql = Sql & gArquivoProdutoIdSel & "', "
            Sql = Sql & "Null" & ", '"
            Sql = Sql & gcpoNumeroCertificado & "'"
        End If
        
    End If
    
    
    Set rs = rdocn1.OpenResultset(Sql)
    
    rs.Close


    Exit Sub
    
ErroAtualizaSituacaoPropostaBB:
   TrataErroGeral "Atualiza Situacao Proposta BB REL015", Me.name
   TerminaSEGBR
End Sub

Private Sub cmdCanc_Click()
  Unload Me
End Sub

Sub Obtem_Dados_Ouro_Agricola()

Dim Rst As rdoResultset
Dim Imp_Seg_Aux As String

On Error GoTo Erro


'Colhendo dados da proposta''''''''''''''''''''''''''''''''''''''''''''''''

Sql = "      Select "
Sql = Sql & "     nome_sistema_cultivo = d.nome,"
Sql = Sql & "     b.tp_cultura_id, "
Sql = Sql & "     nome_tp_cultura = b.nome, "
Sql = Sql & "     a.area_seg_hectare, "
Sql = Sql & "     a.nome_propriedade, "
Sql = Sql & "     a.producao_esperada, "
Sql = Sql & "     a.ind_lavoura_plant, "
Sql = Sql & "     a.num_insp_previa, "
Sql = Sql & "     a.dt_insp_previa, "
Sql = Sql & "     a.num_contrato, "
Sql = Sql & "     a.dt_fim_financ "
Sql = Sql & "From "
Sql = Sql & "     seguro_agricola_tb a, "
Sql = Sql & "     tp_cultura_tb b, "
Sql = Sql & "     sub_tp_cultura_tb c, "
Sql = Sql & "     sistema_cultivo_tb d "
Sql = Sql & "Where "
Sql = Sql & "     a.tp_cultura_id = b.tp_cultura_id "
Sql = Sql & "     and b.tp_cultura_id = c.tp_cultura_id "
Sql = Sql & "     and a.sub_tp_cultura_id = c.sub_tp_cultura_id "
Sql = Sql & "     and a.sistema_cultivo_id = d.sistema_cultivo_id "

Sql = Sql & "     and a.proposta_id = " & gPropostaId

Set Rst = rdocn1.OpenResultset(Sql)

If Not Rst.EOF Then

    DescSistemaCultivo = Left(Rst!nome_sistema_cultivo & Space(30), 30)
    
    TpCulturaId = Rst!tp_cultura_id
        TpCulturaId = Right(String(9, " ") & TpCulturaId, 9)
    
    DescTpCultura = Left(Rst!nome_tp_cultura & Space(30), 30)
    
    AreaSegHectare = Val(Rst!area_seg_hectare)
        AreaSegHectare = Right(String(9, " ") & AreaSegHectare, 9)
    
    NomePropriedade = Left(Rst!nome_propriedade & Space(50), 50)
    
    ProducaoEsperada = Val(Rst!producao_esperada)
        ProducaoEsperada = Right(String(15, " ") & ProducaoEsperada, 15)
    
    IndLavouraPlant = IIf(IsNull(Rst!ind_lavoura_plant) = False, UCase(Rst!ind_lavoura_plant), " ")
    
    If IsNull(Rst!num_insp_previa) = False Then
       If Val(Rst!num_insp_previa) <> 0 Then
            NumInspPrevia = Right(String(13, " ") & Rst!num_insp_previa, 13)
       Else
            NumInspPrevia = String(13, " ")
       End If
    Else
       NumInspPrevia = String(13, " ")
    End If
    
    DtInspPrevia = IIf(IsNull(Rst!dt_insp_previa) = False, Format(Rst!dt_insp_previa, "dd/mm/yyyy"), "          ")
    
    NumContrato = Format(Rst!num_contrato, "000000000")
    
    DtFimFinanc = Format(Rst!dt_fim_financ, "dd/mm/yyyy")

Else

    Call TrataErroGeral("Dados do Seguro Agr�cola n�o encontrados. ", "SEGP0191")
    Rst.Close
    Call TerminaSEGBR
    
End If

Rst.Close

'Colhendo dados do endere�o de risco''''''''''''''''''''''''''''''''''''''''

    Obtem_Dados_Endereco_Risco


'Obtendo IS e Premio'''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sql = "Select "
Sql = Sql & "   val_is, "
Sql = Sql & "   val_premio "
Sql = Sql & "From "
Sql = Sql & "   escolha_tp_cob_generico_tb "
Sql = Sql & "Where proposta_id = " & gPropostaId

Set Rst = rdocn1.OpenResultset(Sql)

If Not Rst.EOF Then
    
    Imp_Seg_Arq = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Rst(0)))
    Imp_Seg_Arq = Right(String(15, " ") & Imp_Seg_Arq, 15)
    
    Imp_Seg_Hectare_Arq = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Val(Rst(0)) / AreaSegHectare))
    Imp_Seg_Hectare_Arq = Right(String(15, " ") & Imp_Seg_Hectare_Arq, 15)
    
    Val_Premio_Arq = FormataValorParaPadraoBrasil(MudaVirgulaParaPonto(Rst(1)))
    Val_Premio_Arq = Right(String(15, " ") & Val_Premio_Arq, 15)
    
Else

    Rst.Close
    TrataErroGeral "Dados da Cobertura do Seguro Agr�cola n�o encontrados REL015", Me.name
    TerminaSEGBR
    
End If

Rst.Close

Exit Sub
   
Erro:
    TrataErroGeral "Obtem Dados Ouro Agricola REL015", Me.name
    TerminaSEGBR
End Sub

Sub Obtem_Dados_Endereco_Risco()

Dim Rst As rdoResultset

On Error GoTo Erro

'Colhendo dados do endere�o de risco''''''''''''''''''''''''''''''''''''''''

Sql = "Select "
Sql = Sql & "   a.endereco, "
Sql = Sql & "   b.nome, "
Sql = Sql & "   b.municipio_ibge_id "
Sql = Sql & "From "
Sql = Sql & "   endereco_risco_tb a, "
Sql = Sql & "   municipio_tb b "
Sql = Sql & "Where "
Sql = Sql & "   a.municipio_id = b.municipio_id "
Sql = Sql & "   and a.estado = b.estado "
Sql = Sql & "   and proposta_id = " & gPropostaId

Set Rst = rdocn1.OpenResultset(Sql)

If Not Rst.EOF Then
   
    EnderecoRisco = Left(Rst!Endereco & Space(50), 50)
    MunicipioRisco = Left((Rst!nome) & Space(45), 45)
    If cpoProduto <> 155 Then
        MunicipioIBGE = Rst!municipio_ibge_id
        MunicipioIBGE = Right(String(9, " ") & MunicipioIBGE, 9)
    End If
    
Else
    
    Rst.Close
    TrataErroGeral "Dados do Endere�o de Risco n�o encontrados REL015", Me.name
    TerminaSEGBR
    
End If

Rst.Close
   
Exit Sub
   
Erro:
    TrataErroGeral "Obtem Dados Endereco Risco REL015", Me.name
    TerminaSEGBR
End Sub

Function FormataValorParaPadraoBrasil(valor As String) As String

    On Error GoTo Erro
   
   ConfDecimal = LeArquivoIni("WIN.INI", "intl", "sDecimal")
   
   'Convertendo valor do d�bito para o padr�o brasileiro
   
   If ConfDecimal = "." Then
      valor = TrocaValorAmePorBras(Format(Val(valor), "###,###,##0.00"))
   Else
      valor = Format(Val(valor), "###,###,##0.00")
   End If

   FormataValorParaPadraoBrasil = valor
   
   Exit Function
   
Erro:
    TrataErroGeral "FormataValoresParaPadraoBrasil REL015", Me.name
    TerminaSEGBR
   
End Function

Function ObterNumRemessa(nome As String, ByRef NumRemessa As String) As Variant

Dim Sql As String
Dim rcNum As rdoResultset
Dim vObterNumRemessa() As Integer
ReDim vObterNumRemessa(0 To 1)
On Error GoTo Erro
    
    Sql = ""
    Sql = Sql & " SELECT"
    Sql = Sql & "     l.layout_id"
    Sql = Sql & " FROM"
    Sql = Sql & "     controle_proposta_db..layout_tb l"
    Sql = Sql & " WHERE"
    Sql = Sql & "     l.nome = '" & nome & "'"
    Set rcNum = rdocn1.OpenResultset(Sql)
    
        If rcNum.EOF Then
           Error 1000
        Else
        vObterNumRemessa(0) = rcNum(0)
        Sql = ""
        Sql = Sql & " SELECT"
        Sql = Sql & "     isnull(max(a.versao), 0)"
        Sql = Sql & " FROM"
        Sql = Sql & "     controle_proposta_db..arquivo_versao_gerado_tb a"
        Sql = Sql & " WHERE"
        Sql = Sql & "     a.layout_id = " & rcNum(0)
        
        rcNum.Close
        
        Set rcNum = rdocn1.OpenResultset(Sql)
        
            If Not rcNum.EOF Then
               NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "0000"), Format(rcNum(0) + 1, "0000"))
               ObterNumRemessa = NumRemessa
            Else
               ObterNumRemessa = Nothing
            End If
        End If
    
    rcNum.Close

Exit Function

Erro:
    If Err.Number = 1000 Then
      MensagemBatch "O arquivo a ser gerado n�o est� cadastrado! Programa ser� cancelado", vbCritical
    End If
    TrataErroGeral "ObterNumRemessa", nome
    TerminaSEGBR

End Function

Sub Obtem_Proposta_Basica()

    Dim Rst As rdoResultset
   
    On Error GoTo Erro
   
    Sql = "SELECT val_parametro "
    Sql = Sql & "From "
    Sql = Sql & "      ps_parametro_tb    "
    Sql = Sql & "Where "
    Sql = Sql & "     parametro = 'BASICA " & cpoProduto & "'"
   
    Set Rst = rdocn1.OpenResultset(Sql)
   
   If Not Rst.EOF Then
      PropostaBasica = Rst(0)
   Else
      TrataErroGeral "Dados da proposta Basica do produto  REL015", Me.name
      TerminaSEGBR
   End If
   
   Rst.Close
   
   Exit Sub
   
Erro:
    TrataErroGeral "Obtem Proposta Basica REL015", Me.name
    TerminaSEGBR

End Sub


Sub Obtem_Dados_Estipulante()

    Dim Rst As rdoResultset
   
    On Error GoTo Erro
    
    Sql = "SELECT "
    Sql = Sql & "      b.nome "
    Sql = Sql & "From "
    Sql = Sql & "      representacao_proposta_tb a, "
    Sql = Sql & "      cliente_tb b "
    Sql = Sql & "Where "
    Sql = Sql & "         a.est_cliente_id = b.cliente_id "
    Sql = Sql & "     and a.proposta_id  = " & PropostaBasica
    Sql = Sql & "     and a.dt_fim_representacao is null "
    
    Set Rst = rdocn1.OpenResultset(Sql)
    
    If Not Rst.EOF Then
       NomeEstipulante = Left(UCase(Rst(0)) + Space(50), 50)
    Else
       TrataErroGeral "Dados do Estipulante n�o Encontrados REL015", Me.name
       TerminaSEGBR
    End If
    
    Rst.Close
    
    Exit Sub
   
Erro:
    TrataErroGeral "Dados Estipulante REL015", Me.name
    TerminaSEGBR

End Sub

Sub Formata_Coberturas()

    On Error GoTo Erro
    
' # Valor IS para Morte Natural - Titular
   aValorIS = gImportanciaSegurada * gPercCobMorteNaturalTit
   cpoISMorteNaturalTit = Format(aValorIS, "###,###,##0.00")
   cpoISMorteNaturalTit = Right(String(14, " ") & cpoISMorteNaturalTit, 14)
                
'# Valor IS para IEA Morte Acidental - Titular
   aValorIS = gImportanciaSegurada * gPercCobIEATit
   cpoISIEATit = Format(aValorIS, "###,###,##0.00")
   cpoISIEATit = Right(String(14, " ") & cpoISIEATit, 14)
                
'# Valor IS para IPA Invalidez Permanente - Titular
   aValorIS = gImportanciaSegurada * gPercCobIPATit
   cpoISIPATit = Format(aValorIS, "###,###,##0.00")
   cpoISIPATit = Right(String(14, " ") & cpoISIPATit, 14)
                
'# Valor IS para IPD Invalidez por Doen�a - Titular
   aValorIS = gImportanciaSegurada * gPercCobIPDTit
   cpoISIPDTit = Format(aValorIS, "###,###,##0.00")
   cpoISIPDTit = Right(String(14, " ") & cpoISIPDTit, 14)
            
'# Valores IS para Conjuge
   If gComponenteConjuge <> "" Then
      '# Valor IS para Morte Natural - Conjuge
      aValorIS = gImportanciaSegurada * gPercCobMorteNaturalConj
      cpoISMorteNaturalConj = Format(aValorIS, "###,###,##0.00")
      cpoISMorteNaturalConj = Right(String(14, " ") & cpoISMorteNaturalConj, 14)
                   
      '# Valor IS para IEA Morte Acidental - Conjuge
      aValorIS = gImportanciaSegurada * gPercCobIEAConj
      cpoISIEAConj = Format(aValorIS, "###,###,##0.00")
      cpoISIEAConj = Right(String(14, " ") & cpoISIEAConj, 14)
                   
      '# Valor IS para IPA Invalidez Permanente - Conjuge
       aValorIS = gImportanciaSegurada * gPercCobIPAConj
       cpoISIPAConj = Format(aValorIS, "###,###,##0.00")
       cpoISIPAConj = Right(String(14, " ") & cpoISIPAConj, 14)
          
       '# Valor IS para IPD Invalidez por Doen�a - Conjuge
       aValorIS = gImportanciaSegurada * gPercCobIPDConj
       cpoISIPDConj = Format(aValorIS, "###,###,##0.00")
       cpoISIPDConj = Right(String(14, " ") & cpoISIPDConj, 14)
  Else
       cpoISMorteNaturalConj = String(14, "*")
       cpoISIEAConj = String(14, "*")
       cpoISIPAConj = String(14, "*")
       cpoISIPDConj = String(14, "*")
 End If
            
 '# Acerto da formata��o dos valores
    If Not gConfiguracaoBrasil Then
                    
      '# Valores IS para Titular
       cpoISMorteNaturalTit = TrocaValorAmePorBras(cpoISMorteNaturalTit)
       cpoISMorteNaturalTit = Right(String(14, " ") & cpoISMorteNaturalTit, 14)
       cpoISIEATit = TrocaValorAmePorBras(cpoISIEATit)
       cpoISIEATit = Right(String(14, " ") & cpoISIEATit, 14)
       cpoISIPATit = TrocaValorAmePorBras(cpoISIPATit)
       cpoISIPATit = Right(String(14, " ") & cpoISIPATit, 14)
       cpoISIPDTit = TrocaValorAmePorBras(cpoISIPDTit)
       cpoISIPDTit = Right(String(14, " ") & cpoISIPDTit, 14)
          
       '# Valores IS para Conjuge
        cpoISMorteNaturalConj = TrocaValorAmePorBras(cpoISMorteNaturalConj)
        cpoISMorteNaturalConj = Right(String(14, " ") & cpoISMorteNaturalConj, 14)
        cpoISIEAConj = TrocaValorAmePorBras(cpoISIEAConj)
        cpoISIEAConj = Right(String(14, " ") & cpoISIEAConj, 14)
        cpoISIPAConj = TrocaValorAmePorBras(cpoISIPAConj)
        cpoISIPAConj = Right(String(14, " ") & cpoISIPAConj, 14)
        cpoISIPDConj = TrocaValorAmePorBras(cpoISIPDConj)
        cpoISIPDConj = Right(String(14, " ") & cpoISIPDConj, 14)
    End If
    
    Exit Sub
    
Erro:
    TrataErroGeral "Formata_Coberturas REL015", Me.name
    TerminaSEGBR

End Sub
'Sub ObterNumRemessa(nome As String, ByRef NumRemessa As String)
'
'Dim rcNum As ADODB.Recordset
'
'On Error GoTo Erro
'
'SQL = ""
'SQL = SQL & " SELECT"
'SQL = SQL & "     l.layout_id"
'SQL = SQL & " FROM"
'SQL = SQL & "     controle_proposta_db..layout_tb l"
'SQL = SQL & " WHERE"
'SQL = SQL & "     l.nome = '" & nome & "'"
'Set rcNum = rdocn.Execute(SQL)
'
'If rcNum.EOF Then
'   Error 1000
'Else
'
'SQL = "       SELECT"
'SQL = SQL & "     max(a.versao)"
'SQL = SQL & " FROM"
'SQL = SQL & "     controle_proposta_db..arquivo_versao_gerado_tb a"
'SQL = SQL & " WHERE"
'SQL = SQL & "     a.layout_id = " & rcNum(0)
'
'rcNum.Close
'
'Set rcNum = rdocn.Execute(SQL)
'
'If Not rcNum.EOF Then
'   NumRemessa = IIf(IsNull(rcNum(0)), Format(1, "000000"), Format(rcNum(0) + 1, "000000"))
'End If
'
'End If
'
'rcNum.Close
'
'Exit Sub
'
'Erro:
'    If Err.Number = 1000 Then
'      TrataErroGeral "O arquivo a ser gerado n�o est� cadastrado REL015", Me.name
'    End If
'    TrataErroGeral "ObterNumRemessa REL015", Me.name
'    TerminaSEGBR
'
'End Sub

Sub Obtem_Dados_Produto()

Dim Rc_prod As rdoResultset

On Error GoTo Erro

Sql = "SELECT nome FROM produto_tb "
Sql = Sql & "WHERE produto_id = " & Val(cpoProduto)

Set Rc_prod = rdocn1.OpenResultset(Sql)

If Not Rc_prod.EOF Then
   gNomeProduto = Left(UCase(Rc_prod(0)) & Space(50), 50)
End If

Exit Sub

Erro:
    TrataErroGeral "Obtem_Dados_Produto REL015", Me.name
    TerminaSEGBR

End Sub

'Sub InserirArquivoVersaoGerado(nome As String, qReg As Long, qLinhas As Long, NumRemessa As Integer)
'
'    On Error GoTo Erro
'
'    SQL = "exec controle_proposta_db..arquivo_versao_gerado_spi '"
'    SQL = SQL & nome & "'," & NumRemessa & "," & qReg & ",'"
'    SQL = SQL & Format(Data_Sistema, "yyyymmdd") & "'," & qLinhas & ",'"
'    SQL = SQL & cUserName & "'"
'    rdocn.Execute (SQL)
'
'    Exit Sub
'
'Erro:
'    TrataErroGeral "InserirArquivoVersaoGerado", Me.name
'    TerminaSEGBR
'
'End Sub
'
