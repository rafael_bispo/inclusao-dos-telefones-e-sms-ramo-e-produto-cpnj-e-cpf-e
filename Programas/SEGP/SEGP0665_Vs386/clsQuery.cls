VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsQuery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarsql As String 'local copy
Private mvarpropostaId As Long 'local copy
Public Property Let propostaId(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.propostaId = 5
    mvarpropostaId = vData
End Property


Public Property Get propostaId() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.propostaId
    propostaId = mvarpropostaId
End Property



Public Property Let sql(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.sql = 5
    mvarsql = vData
End Property


Public Property Get sql() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.sql
    sql = mvarsql
End Property



